#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2012
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# piwigo.py
#
# Création de la base de données mysql de piwigo
#
###########################################################################
"""
    Config pour piwigo
"""
from eolesql.db_test import db_exists, test_var

PIWIGO_TABLEFILENAMES = ['/usr/share/eole/mysql/piwigo/gen/piwigo-create-0.sql',
                       '/usr/share/eole/mysql/piwigo/gen/piwigo-create-1.sql',
                       '/usr/share/eole/mysql/piwigo/gen/piwigo-create-2.sql']
def test():
    """
        test l'existence de la base piwigo
    """
    return test_var('activer_piwigo') and not db_exists('piwigo')

conf_dict = dict(filenames=PIWIGO_TABLEFILENAMES,
                 test=test)
