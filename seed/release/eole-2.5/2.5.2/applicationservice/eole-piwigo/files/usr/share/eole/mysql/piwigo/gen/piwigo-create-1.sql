-- connexion à la base
\r piwigo


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
--
-- Base de données: `piwigo`
--

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_amm_blocks`
--

CREATE TABLE IF NOT EXISTS `piwigo_amm_blocks` (
  `id` varchar(40) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `users` varchar(1024) NOT NULL,
  `groups` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `byOrder` (`order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_amm_blocks`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_amm_personalised`
--

CREATE TABLE IF NOT EXISTS `piwigo_amm_personalised` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visible` char(1) NOT NULL DEFAULT 'y',
  `nfo` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_amm_personalised`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_amm_personalised_langs`
--

CREATE TABLE IF NOT EXISTS `piwigo_amm_personalised_langs` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `lang` char(5) NOT NULL DEFAULT '*',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  PRIMARY KEY (`id`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_amm_personalised_langs`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_amm_urls`
--

CREATE TABLE IF NOT EXISTS `piwigo_amm_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `mode` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(50) NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `visible` char(1) NOT NULL DEFAULT 'y',
  `accessUsers` varchar(1024) NOT NULL,
  `accessGroups` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_key` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_amm_urls`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_caddie`
--

CREATE TABLE IF NOT EXISTS `piwigo_caddie` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `element_id` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_caddie`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_categories`
--

CREATE TABLE IF NOT EXISTS `piwigo_categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `id_uppercat` smallint(5) unsigned DEFAULT NULL,
  `comment` text,
  `dir` varchar(255) DEFAULT NULL,
  `rank` smallint(5) unsigned DEFAULT NULL,
  `status` enum('public','private') NOT NULL DEFAULT 'public',
  `site_id` tinyint(4) unsigned DEFAULT '1',
  `visible` enum('true','false') NOT NULL DEFAULT 'true',
  `representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
  `uppercats` varchar(255) NOT NULL DEFAULT '',
  `commentable` enum('true','false') NOT NULL DEFAULT 'true',
  `global_rank` varchar(255) DEFAULT NULL,
  `image_order` varchar(128) DEFAULT NULL,
  `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_i3` (`permalink`),
  KEY `categories_i2` (`id_uppercat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `piwigo_categories`
--

INSERT INTO `piwigo_categories` (`id`, `name`, `id_uppercat`, `comment`, `dir`, `rank`, `status`, `site_id`, `visible`, `representative_picture_id`, `uppercats`, `commentable`, `global_rank`, `image_order`, `permalink`) VALUES
(1, 'classes', NULL, 'Album des classes', NULL, 1, 'private', NULL, 'true', NULL, '1', 'false', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_comments`
--

CREATE TABLE IF NOT EXISTS `piwigo_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author` varchar(255) DEFAULT NULL,
  `author_id` smallint(5) DEFAULT NULL,
  `content` longtext,
  `validated` enum('true','false') NOT NULL DEFAULT 'false',
  `validation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_i2` (`validation_date`),
  KEY `comments_i1` (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_comments`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_community_pendings`
--

CREATE TABLE IF NOT EXISTS `piwigo_community_pendings` (
  `image_id` mediumint(8) unsigned NOT NULL,
  `state` varchar(255) NOT NULL,
  `added_on` datetime NOT NULL,
  `validated_by` smallint(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_community_pendings`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_community_permissions`
--

CREATE TABLE IF NOT EXISTS `piwigo_community_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `group_id` smallint(5) unsigned DEFAULT NULL,
  `user_id` smallint(5) DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `recursive` enum('true','false') NOT NULL DEFAULT 'true',
  `create_subcategories` enum('true','false') NOT NULL DEFAULT 'false',
  `moderated` enum('true','false') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_config`
--

CREATE TABLE IF NOT EXISTS `piwigo_config` (
  `param` varchar(40) NOT NULL DEFAULT '',
  `value` text,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`param`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='configuration table';

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_favorites`
--

CREATE TABLE IF NOT EXISTS `piwigo_favorites` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_favorites`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_gpc_request`
--

CREATE TABLE IF NOT EXISTS `piwigo_gpc_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `num_items` int(10) unsigned NOT NULL DEFAULT '0',
  `execution_time` float unsigned NOT NULL DEFAULT '0',
  `connected_plugin` char(255) NOT NULL,
  `filter` text NOT NULL,
  `parameters` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_gpc_request`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_gpc_result_cache`
--

CREATE TABLE IF NOT EXISTS `piwigo_gpc_result_cache` (
  `id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_gpc_result_cache`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_gpc_temp`
--

CREATE TABLE IF NOT EXISTS `piwigo_gpc_temp` (
  `requestId` char(30) NOT NULL,
  `imageId` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`requestId`,`imageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_gpc_temp`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_groups`
--

CREATE TABLE IF NOT EXISTS `piwigo_groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `is_default` enum('true','false') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_ui1` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_groups`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_group_access`
--

CREATE TABLE IF NOT EXISTS `piwigo_group_access` (
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_group_access`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_history`
--

CREATE TABLE IF NOT EXISTS `piwigo_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `IP` varchar(15) NOT NULL DEFAULT '',
  `section` enum('categories','tags','search','list','favorites','most_visited','best_rated','recent_pics','recent_cats') DEFAULT NULL,
  `category_id` smallint(5) DEFAULT NULL,
  `tag_ids` varchar(50) DEFAULT NULL,
  `image_id` mediumint(8) DEFAULT NULL,
  `summarized` enum('true','false') DEFAULT 'false',
  `image_type` enum('picture','high','other') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_i1` (`summarized`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `piwigo_history`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_history_summary`
--

CREATE TABLE IF NOT EXISTS `piwigo_history_summary` (
  `year` smallint(4) NOT NULL DEFAULT '0',
  `month` tinyint(2) DEFAULT NULL,
  `day` tinyint(2) DEFAULT NULL,
  `hour` tinyint(2) DEFAULT NULL,
  `nb_pages` int(11) DEFAULT NULL,
  UNIQUE KEY `history_summary_ymdh` (`year`,`month`,`day`,`hour`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_history_summary`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_images`
--

CREATE TABLE IF NOT EXISTS `piwigo_images` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_available` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_creation` datetime DEFAULT NULL,
  `tn_ext` varchar(4) DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `comment` text,
  `author` varchar(255) DEFAULT NULL,
  `hit` int(10) unsigned NOT NULL DEFAULT '0',
  `filesize` mediumint(9) unsigned DEFAULT NULL,
  `width` smallint(9) unsigned DEFAULT NULL,
  `height` smallint(9) unsigned DEFAULT NULL,
  `representative_ext` varchar(4) DEFAULT NULL,
  `date_metadata_update` date DEFAULT NULL,
  `rating_score` float(5,2) unsigned DEFAULT NULL,
  `has_high` enum('true') DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `storage_category_id` smallint(5) unsigned DEFAULT NULL,
  `high_filesize` mediumint(9) unsigned DEFAULT NULL,
  `high_width` smallint(9) unsigned DEFAULT NULL,
  `high_height` smallint(9) unsigned DEFAULT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `md5sum` char(32) DEFAULT NULL,
  `added_by` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `images_i2` (`date_available`),
  KEY `images_i3` (`rating_score`),
  KEY `images_i4` (`hit`),
  KEY `images_i5` (`date_creation`),
  KEY `images_i1` (`storage_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_images`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_image_category`
--

CREATE TABLE IF NOT EXISTS `piwigo_image_category` (
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `category_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rank` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`image_id`,`category_id`),
  KEY `image_category_i1` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_image_category`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_image_tag`
--

CREATE TABLE IF NOT EXISTS `piwigo_image_tag` (
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tag_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`,`tag_id`),
  KEY `image_tag_i1` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_image_tag`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_languages`
--

CREATE TABLE IF NOT EXISTS `piwigo_languages` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_languages`
--

INSERT INTO `piwigo_languages` (`id`, `version`, `name`) VALUES
('af_ZA', '2.3.0', 'Afrikaans [ZA]'),
('ar_SA', '2.3.0', 'العربية [AR]'),
('bg_BG', '2.3.0', 'Български [BG]'),
('ca_ES', '2.3.0', 'Catalan [CA]'),
('cs_CZ', '2.3.0', 'Česky [CZ]'),
('da_DK', '2.3.0', 'Dansk [DK]'),
('de_DE', '2.3.0', 'Deutsch [DE]'),
('dv_MV', '2.3.0', 'Dhivehi [MV]'),
('el_GR', '2.3.0', 'Ελληνικά [GR]'),
('en_UK', '2.3.0', 'English [UK]'),
('eo_EO', '2.3.0', 'Esperanto [EO]'),
('es_AR', '2.3.0', 'Argentina [AR]'),
('es_ES', '2.3.0', 'Español [ES]'),
('et_EE', '2.3.0', 'Estonian [EE]'),
('fa_IR', '2.3.0', 'فارسی [IR]'),
('fi_FI', '2.4.0', 'Finnish [FI]'),
('fr_CA', '2.3.0', 'Français [QC]'),
('fr_FR', '2.3.0', 'Français [FR]'),
('he_IL', '2.3.0', 'עברית [IL]'),
('hr_HR', '2.3.0', 'Hrvatski [HR]'),
('hu_HU', '2.3.0', 'Magyar [HU]'),
('is_IS', '2.3.0', 'Íslenska [IS]'),
('it_IT', '2.3.0', 'Italiano [IT]'),
('ja_JP', '2.3.0', '日本語 [JP]'),
('ka_GE', '2.3.0', 'ქართული [GE]'),
('km_KH', '2.3.0', 'ភាសាខ្មែរ [KH]'),
('ko_KR', '2.2.0', '한국어 [KR]'),
('lv_LV', '2.3.0', 'Latviešu [LV]'),
('mk_MK', '2.3.0', 'Македонски [MK]'),
('nl_NL', '2.3.0', 'Nederlands [NL]'),
('no_NO', '2.3.0', 'Norwegian [NO]'),
('pl_PL', '2.3.0', 'Polski [PL]'),
('pt_BR', '2.3.0', 'Brasil [BR]'),
('pt_PT', '2.3.0', 'Português [PT]'),
('ro_RO', '2.3.0', 'Română [RO]'),
('ru_RU', '2.3.0', 'Русский [RU]'),
('sh_RS', '2.3.0', 'Srpski [SR]'),
('sk_SK', '2.3.0', 'Slovensky [SK]'),
('sl_SL', '2.3.0', 'Slovenšcina [SL]'),
('sr_RS', '2.3.0', 'Српски [SR]'),
('sv_SE', '2.2.1', 'Svenska [SE]'),
('th_TH', '2.2.0', 'ภาษาไทย [TH]'),
('tr_TR', '2.3.0', 'Türkçe [TR]'),
('uk_UA', '2.3.0', 'Українська [UA]'),
('vi_VN', '2.3.0', 'Tiếng Việt [VN]'),
('zh_CN', '2.3.0', '简体中文 [CN]'),
('zh_TW', '2.3.0', '中文 (繁體) [TW]');

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_old_permalinks`
--

CREATE TABLE IF NOT EXISTS `piwigo_old_permalinks` (
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_deleted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_hit` datetime DEFAULT NULL,
  `hit` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`permalink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_old_permalinks`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_plugins`
--

CREATE TABLE IF NOT EXISTS `piwigo_plugins` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `state` enum('inactive','active') NOT NULL DEFAULT 'inactive',
  `version` varchar(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_plugins`
--

INSERT INTO `piwigo_plugins` (`id`, `state`, `version`) VALUES
('AMenuManager', 'active', '3.1.3'),
('EoleCAS', 'active', '0.1'),
('GrumPluginClasses', 'active', '3.5.1'),
('community', 'active', '2.3.e');

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_rate`
--

CREATE TABLE IF NOT EXISTS `piwigo_rate` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `element_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `anonymous_id` varchar(45) NOT NULL DEFAULT '',
  `rate` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`element_id`,`user_id`,`anonymous_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_rate`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_search`
--

CREATE TABLE IF NOT EXISTS `piwigo_search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `last_seen` date DEFAULT NULL,
  `rules` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_search`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_sessions`
--

CREATE TABLE IF NOT EXISTS `piwigo_sessions` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `data` mediumtext NOT NULL,
  `expiration` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_sites`
--

CREATE TABLE IF NOT EXISTS `piwigo_sites` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `galleries_url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sites_ui1` (`galleries_url`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `piwigo_sites`
--

INSERT INTO `piwigo_sites` (`id`, `galleries_url`) VALUES
(1, './galleries/');

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_tags`
--

CREATE TABLE IF NOT EXISTS `piwigo_tags` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `tags_i1` (`url_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `piwigo_tags`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_themes`
--

CREATE TABLE IF NOT EXISTS `piwigo_themes` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_themes`
--

INSERT INTO `piwigo_themes` (`id`, `version`, `name`) VALUES
('clear', '2.3.0', 'clear'),
('dark', '2.3.0', 'dark'),
('Sylvia', '2.3.0', 'Sylvia');

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_upgrade`
--

CREATE TABLE IF NOT EXISTS `piwigo_upgrade` (
  `id` varchar(20) NOT NULL DEFAULT '',
  `applied` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_upgrade`
--

INSERT INTO `piwigo_upgrade` (`id`, `applied`, `description`) VALUES
('100', '2012-05-16 16:19:17', 'upgrade included in installation'),
('101', '2012-05-16 16:19:17', 'upgrade included in installation'),
('102', '2012-05-16 16:19:17', 'upgrade included in installation'),
('103', '2012-05-16 16:19:17', 'upgrade included in installation'),
('104', '2012-05-16 16:19:17', 'upgrade included in installation'),
('105', '2012-05-16 16:19:17', 'upgrade included in installation'),
('106', '2012-05-16 16:19:17', 'upgrade included in installation'),
('107', '2012-05-16 16:19:17', 'upgrade included in installation'),
('108', '2012-05-16 16:19:17', 'upgrade included in installation'),
('109', '2012-05-16 16:19:17', 'upgrade included in installation'),
('110', '2012-05-16 16:19:17', 'upgrade included in installation'),
('111', '2012-05-16 16:19:17', 'upgrade included in installation'),
('61', '2012-05-16 16:19:17', 'upgrade included in installation'),
('62', '2012-05-16 16:19:17', 'upgrade included in installation'),
('63', '2012-05-16 16:19:17', 'upgrade included in installation'),
('64', '2012-05-16 16:19:17', 'upgrade included in installation'),
('65', '2012-05-16 16:19:17', 'upgrade included in installation'),
('66', '2012-05-16 16:19:17', 'upgrade included in installation'),
('67', '2012-05-16 16:19:17', 'upgrade included in installation'),
('68', '2012-05-16 16:19:17', 'upgrade included in installation'),
('69', '2012-05-16 16:19:17', 'upgrade included in installation'),
('70', '2012-05-16 16:19:17', 'upgrade included in installation'),
('71', '2012-05-16 16:19:17', 'upgrade included in installation'),
('72', '2012-05-16 16:19:17', 'upgrade included in installation'),
('73', '2012-05-16 16:19:17', 'upgrade included in installation'),
('74', '2012-05-16 16:19:17', 'upgrade included in installation'),
('75', '2012-05-16 16:19:17', 'upgrade included in installation'),
('76', '2012-05-16 16:19:17', 'upgrade included in installation'),
('77', '2012-05-16 16:19:17', 'upgrade included in installation'),
('78', '2012-05-16 16:19:17', 'upgrade included in installation'),
('79', '2012-05-16 16:19:17', 'upgrade included in installation'),
('80', '2012-05-16 16:19:17', 'upgrade included in installation'),
('81', '2012-05-16 16:19:17', 'upgrade included in installation'),
('82', '2012-05-16 16:19:17', 'upgrade included in installation'),
('83', '2012-05-16 16:19:17', 'upgrade included in installation'),
('84', '2012-05-16 16:19:17', 'upgrade included in installation'),
('85', '2012-05-16 16:19:17', 'upgrade included in installation'),
('86', '2012-05-16 16:19:17', 'upgrade included in installation'),
('87', '2012-05-16 16:19:17', 'upgrade included in installation'),
('88', '2012-05-16 16:19:17', 'upgrade included in installation'),
('89', '2012-05-16 16:19:17', 'upgrade included in installation'),
('90', '2012-05-16 16:19:17', 'upgrade included in installation'),
('91', '2012-05-16 16:19:17', 'upgrade included in installation'),
('92', '2012-05-16 16:19:17', 'upgrade included in installation'),
('93', '2012-05-16 16:19:17', 'upgrade included in installation'),
('94', '2012-05-16 16:19:17', 'upgrade included in installation'),
('95', '2012-05-16 16:19:17', 'upgrade included in installation'),
('96', '2012-05-16 16:19:17', 'upgrade included in installation'),
('97', '2012-05-16 16:19:17', 'upgrade included in installation'),
('98', '2012-05-16 16:19:17', 'upgrade included in installation'),
('99', '2012-05-16 16:19:17', 'upgrade included in installation');

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_users`
--

CREATE TABLE IF NOT EXISTS `piwigo_users` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  `mail_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_ui1` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_access`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_access` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_user_access`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_cache`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_cache` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `need_update` enum('true','false') NOT NULL DEFAULT 'true',
  `cache_update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `forbidden_categories` mediumtext,
  `nb_total_images` mediumint(8) unsigned DEFAULT NULL,
  `image_access_type` enum('NOT IN','IN') NOT NULL DEFAULT 'NOT IN',
  `image_access_list` mediumtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_user_cache`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_cache_categories`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_cache_categories` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `date_last` datetime DEFAULT NULL,
  `max_date_last` datetime DEFAULT NULL,
  `nb_images` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `count_images` mediumint(8) unsigned DEFAULT '0',
  `count_categories` mediumint(8) unsigned DEFAULT '0',
  `user_representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_user_cache_categories`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_feed`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_feed` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `last_check` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_user_feed`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_group`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_group` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `piwigo_user_group`
--


-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_infos`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_infos` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `nb_image_page` smallint(3) unsigned NOT NULL DEFAULT '15',
  `status` enum('webmaster','admin','normal','generic','guest') NOT NULL DEFAULT 'guest',
  `language` varchar(50) NOT NULL DEFAULT 'en_UK',
  `maxwidth` smallint(6) DEFAULT NULL,
  `maxheight` smallint(6) DEFAULT NULL,
  `expand` enum('true','false') NOT NULL DEFAULT 'false',
  `show_nb_comments` enum('true','false') NOT NULL DEFAULT 'false',
  `show_nb_hits` enum('true','false') NOT NULL DEFAULT 'false',
  `recent_period` tinyint(3) unsigned NOT NULL DEFAULT '7',
  `theme` varchar(255) NOT NULL DEFAULT 'Sylvia',
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enabled_high` enum('true','false') NOT NULL DEFAULT 'true',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `activation_key` char(20) DEFAULT NULL,
  UNIQUE KEY `user_infos_ui1` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `piwigo_user_mail_notification`
--

CREATE TABLE IF NOT EXISTS `piwigo_user_mail_notification` (
  `user_id` smallint(5) NOT NULL DEFAULT '0',
  `check_key` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `enabled` enum('true','false') NOT NULL DEFAULT 'false',
  `last_send` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_mail_notification_ui1` (`check_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
