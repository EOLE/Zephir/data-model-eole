#-*-coding:utf-8-*-

def calc_info(user_infos):
    """
        calcule ENTPersonProfils : Profil d'utilisateur ENT
    """
    dico = {'eleve':['eleve'],
            'responsable':['responsable'],
            'professeur':['professeur'],
            'professeur_principal':['professeur_principal'],
            'admin':['admin'],
            'administratif':['administratif'],
            'default':['autre'],}
    groups = user_infos.get('user_groups', [])
    typeadmin = user_infos.get('typeadmin', [''])[0]
    uid = user_infos.get('uid', [''])[0]
    objectclass = user_infos.get('objectClass', [''])
    if 'eleves' in groups:
        return dico.get('eleve', dico.get('default'))
    elif uid == 'admin' or typeadmin == '1':
        return dico.get('admin', dico.get('default'))
    elif typeadmin == '2':
        return dico['professeur_principal']
    elif typeadmin == '0' or 'professeurs' in groups:
        return dico.get('professeur',  dico.get('default'))
    elif 'responsable' in objectclass:
        return dico.get('responsable', dico.get('default'))
    elif 'administratifs' in groups:
        return dico.get('administratif', dico.get('default'))
    else:
        return dico.get('default')
