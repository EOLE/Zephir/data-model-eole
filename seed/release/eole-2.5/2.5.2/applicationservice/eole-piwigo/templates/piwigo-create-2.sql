-- connexion à la base
\r piwigo


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
--
-- Contenu de la table `piwigo_users`
--

-- mdp admin = admin_eole_piwigo
%if %%is_defined('%%domaine_messagerie_etab')
INSERT INTO `piwigo_users` (`id`, `username`, `password`, `mail_address`) VALUES (1, 'admin', '36f2a799c010e29f0764d2405f865022', 'admin@%%domaine_messagerie_etab'), (2, 'guest', NULL, NULL);
%else
INSERT INTO `piwigo_users` (`id`, `username`, `password`, `mail_address`) VALUES (1, 'admin', '36f2a799c010e29f0764d2405f865022', ''), (2, 'guest', NULL, NULL);
%end if

--
-- Contenu de la table `piwigo_user_infos`
--

INSERT INTO `piwigo_user_infos` (`user_id`, `nb_image_page`, `status`, `language`, `maxwidth`, `maxheight`, `expand`, `show_nb_comments`, `show_nb_hits`, `recent_period`, `theme`, `registration_date`, `enabled_high`, `level`, `activation_key`) VALUES
(1, 15, 'webmaster', 'fr_FR', NULL, NULL, 'false', 'false', 'false', 7, 'dark', '2012-05-16 16:19:17', 'true', 8, NULL),
(2, 15, 'guest', 'fr_FR', NULL, NULL, 'false', 'false', 'false', 7, 'dark', '2012-05-16 16:19:17', 'true', 0, NULL);

--
-- Contenu de la table `piwigo_config`
--

INSERT INTO `piwigo_config` (`param`, `value`, `comment`) VALUES
('admin_theme', 'clear', NULL),
('allow_user_customization', 'true', 'allow users to customize their gallery?'),
('allow_user_registration', 'true', 'allow visitors to register?'),
('amm_config', 'a:15:{s:20:"amm_links_show_icons";s:1:"y";s:15:"amm_links_title";a:47:{s:5:"af_ZA";s:8:"TGlua3M=";s:5:"es_AR";s:8:"TGlua3M=";s:5:"pt_BR";s:8:"TGlua3M=";s:5:"ca_ES";s:8:"TGlua3M=";s:5:"cs_CZ";s:8:"TGlua3M=";s:5:"da_DK";s:8:"TGlua3M=";s:5:"de_DE";s:8:"TGlua3M=";s:5:"dv_MV";s:8:"TGlua3M=";s:5:"en_UK";s:8:"TGlua3M=";s:5:"es_ES";s:8:"TGlua3M=";s:5:"eo_EO";s:8:"TGlua3M=";s:5:"et_EE";s:8:"TGlua3M=";s:5:"fi_FI";s:8:"TGlua3M=";s:5:"fr_FR";s:8:"TGllbnM=";s:5:"fr_CA";s:8:"TGlua3M=";s:5:"hr_HR";s:8:"TGlua3M=";s:5:"is_IS";s:8:"TGlua3M=";s:5:"it_IT";s:8:"TGlua3M=";s:5:"lv_LV";s:8:"TGlua3M=";s:5:"hu_HU";s:8:"TGlua3M=";s:5:"nl_NL";s:8:"TGlua3M=";s:5:"no_NO";s:8:"TGlua3M=";s:5:"pl_PL";s:8:"TGlua3M=";s:5:"pt_PT";s:8:"TGlua3M=";s:5:"ro_RO";s:8:"TGlua3M=";s:5:"sl_SL";s:8:"TGlua3M=";s:5:"sk_SK";s:8:"TGlua3M=";s:5:"sh_RS";s:8:"TGlua3M=";s:5:"sv_SE";s:8:"TGlua3M=";s:5:"vi_VN";s:8:"TGlua3M=";s:5:"tr_TR";s:8:"TGlua3M=";s:5:"el_GR";s:8:"TGlua3M=";s:5:"bg_BG";s:8:"TGlua3M=";s:5:"mk_MK";s:8:"TGlua3M=";s:5:"ru_RU";s:8:"TGlua3M=";s:5:"sr_RS";s:8:"TGlua3M=";s:5:"uk_UA";s:8:"TGlua3M=";s:5:"he_IL";s:8:"TGlua3M=";s:5:"ar_SA";s:8:"TGlua3M=";s:5:"fa_IR";s:8:"TGlua3M=";s:5:"th_TH";s:8:"TGlua3M=";s:5:"ka_GE";s:8:"TGlua3M=";s:5:"km_KH";s:8:"TGlua3M=";s:5:"zh_TW";s:8:"TGlua3M=";s:5:"ja_JP";s:8:"TGlua3M=";s:5:"zh_CN";s:8:"TGlua3M=";s:5:"ko_KR";s:8:"TGlua3M=";}s:25:"amm_randompicture_preload";i:25;s:26:"amm_randompicture_showname";s:1:"n";s:29:"amm_randompicture_showcomment";s:1:"n";s:32:"amm_randompicture_periodicchange";i:0;s:24:"amm_randompicture_height";i:0;s:23:"amm_randompicture_title";a:47:{s:5:"af_ZA";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"es_AR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"pt_BR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ca_ES";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"cs_CZ";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"da_DK";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"de_DE";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"dv_MV";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"en_UK";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"es_ES";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"eo_EO";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"et_EE";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"fi_FI";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"fr_FR";s:28:"VW5lIGltYWdlIGF1IGhhc2FyZA==";s:5:"fr_CA";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"hr_HR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"is_IS";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"it_IT";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"lv_LV";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"hu_HU";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"nl_NL";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"no_NO";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"pl_PL";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"pt_PT";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ro_RO";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"sl_SL";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"sk_SK";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"sh_RS";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"sv_SE";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"vi_VN";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"tr_TR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"el_GR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"bg_BG";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"mk_MK";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ru_RU";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"sr_RS";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"uk_UA";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"he_IL";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ar_SA";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"fa_IR";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"th_TH";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ka_GE";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"km_KH";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"zh_TW";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ja_JP";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"zh_CN";s:24:"QSByYW5kb20gcGljdHVyZQ==";s:5:"ko_KR";s:24:"QSByYW5kb20gcGljdHVyZQ==";}s:28:"amm_randompicture_selectMode";s:1:"a";s:27:"amm_randompicture_selectCat";a:0:{}s:16:"amm_blocks_items";a:13:{s:9:"favorites";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:0;s:11:"translation";s:12:"My favorites";}s:12:"most_visited";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:1;s:11:"translation";s:12:"Most visited";}s:10:"best_rated";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:2;s:11:"translation";s:10:"Best rated";}s:6:"random";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:3;s:11:"translation";s:15:"Random pictures";}s:11:"recent_pics";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:4;s:11:"translation";s:15:"Recent pictures";}s:11:"recent_cats";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:5;s:11:"translation";s:17:"Recent categories";}s:8:"calendar";a:4:{s:9:"container";s:7:"special";s:10:"visibility";s:1:"/";s:5:"order";i:6;s:11:"translation";s:8:"Calendar";}s:7:"qsearch";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:0;s:11:"translation";s:12:"Quick search";}s:4:"tags";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:1;s:11:"translation";s:4:"Tags";}s:6:"search";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:2;s:11:"translation";s:6:"Search";}s:8:"comments";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:3;s:11:"translation";s:8:"Comments";}s:5:"about";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:4;s:11:"translation";s:5:"About";}s:3:"rss";a:4:{s:9:"container";s:4:"menu";s:10:"visibility";s:1:"/";s:5:"order";i:5;s:11:"translation";s:12:"Notification";}}s:18:"amm_albums_to_menu";a:0:{}s:19:"amm_old_blk_menubar";s:0:"";s:10:"newInstall";s:1:"n";s:9:"installed";s:8:"03.01.03";}', ''),
('blk_menubar', '', 'Menubar options'),
('c13y_ignore', 'a:2:{s:7:"version";s:5:"2.3.4";s:4:"list";a:0:{}}', 'List of ignored anomalies'),
('combined_dir_checked', 'true', NULL),
('comments_forall', 'false', 'even guest not registered can post comments'),
('comments_validation', 'false', 'administrators validate users comments before becoming visible'),
('community_cache_key', 'l6o8NMc8ja9iJ8z8Ye09', NULL),
('email_admin_on_comment', 'false', 'Send an email to the administrators when a valid comment is entered'),
('email_admin_on_comment_deletion', 'false', 'Send an email to the administrators when a comment is deleted'),
('email_admin_on_comment_edition', 'false', 'Send an email to the administrators when a comment is modified'),
('email_admin_on_comment_validation', 'false', 'Send an email to the administrators when a comment requires validation'),
('email_admin_on_new_user', 'false', 'Send an email to theadministrators when a user registers'),
('extents_for_templates', 'a:0:{}', 'Actived template-extension(s)'),
('gallery_locked', 'false', 'Lock your gallery temporary for non admin users'),
('gallery_title', '%%to_sql(%%libelle_etab)', 'Title at top of each page and for RSS feed'),
('GPCCore_config', 'a:1:{s:10:"registered";a:1:{s:21:"Advanced Menu Manager";a:4:{s:4:"name";s:21:"Advanced Menu Manager";s:7:"release";s:5:"3.1.3";s:6:"needed";s:5:"3.5.1";s:4:"date";s:10:"2012-06-14";}}}', ''),
('gpc_config', 'a:1:{s:9:"installed";s:8:"03.05.01";}', ''),
('history_admin', 'false', 'keep a history of administrator visits on your website'),
('history_guest', 'true', 'keep a history of guest visits on your website'),
('index_created_date_icon', 'true', 'Display calendar by creation date icon'),
('index_flat_icon', 'true', 'Display flat icon'),
('index_new_icon', 'true', 'Display new icons next albums and pictures'),
('index_posted_date_icon', 'true', 'Display calendar by posted date'),
('index_slideshow_icon', 'true', 'Display slideshow icon'),
('index_sort_order_input', 'true', 'Display image order selection list'),
('local_data_dir_checked', 'true', NULL),
('log', 'true', 'keep an history of visits on your website'),
('menubar_filter_icon', 'true', 'Display filter icon'),
('nbm_complementary_mail_content', '', 'Complementary mail content for notification by mail'),
('nbm_send_detailed_content', 'true', 'Send detailed content for notification by mail'),
('nbm_send_html_mail', 'true', 'Send mail on HTML format for notification by mail'),
('nbm_send_mail_as', '', 'Send mail as param value for notification by mail'),
('nbm_send_recent_post_dates', 'true', 'Send recent post by dates for notification by mail'),
('nb_comment_page', '10', 'number of comments to display on each page'),
('no_photo_yet', 'false', NULL),
('obligatory_user_mail_address', 'false', 'Mail address is obligatory for users'),
('order_by', 'ORDER BY date_available DESC, file ASC, id ASC', 'default photo order'),
('order_by_inside_category', 'ORDER BY date_available DESC, file ASC, id ASC', 'default photo order inside category'),
('page_banner', '<h1>%gallery_title%</h1>\r\n\r\n<p>Bienvenue sur ma galerie photo</p>', 'html displayed on the top each page of your gallery'),
('picture_download_icon', 'true', 'Display download icon on picture page'),
('picture_favorite_icon', 'true', 'Display favorite icon on picture page'),
('picture_informations', 'a:11:{s:6:"author";b:1;s:10:"created_on";b:1;s:9:"posted_on";b:1;s:10:"dimensions";b:1;s:4:"file";b:1;s:8:"filesize";b:1;s:4:"tags";b:1;s:10:"categories";b:1;s:6:"visits";b:1;s:12:"rating_score";b:1;s:13:"privacy_level";b:1;}', 'Information displayed on picture page'),
('picture_menu', 'false', 'Show menubar on picture page'),
('picture_metadata_icon', 'true', 'Display metadata icon on picture page'),
('picture_navigation_icons', 'true', 'Display navigation icons on picture page'),
('picture_navigation_thumb', 'true', 'Display navigation thumbnails on picture page'),
('picture_slideshow_icon', 'true', 'Display slideshow icon on picture page'),
('piwigo_db_version', '2.3', NULL),
('rate', 'true', 'Rating pictures feature is enabled'),
('rate_anonymous', 'true', 'Rating pictures feature is also enabled for visitors'),
('secret_key', 'a0526f25d829e94565cdf40a38b9b286', 'a secret key specific to the gallery for internal use'),
('updates_ignored', 'a:3:{s:7:"plugins";a:0:{}s:6:"themes";a:0:{}s:9:"languages";a:0:{}}', 'Extensions ignored for update'),
('upload_form_hd_keep', 'true', NULL),
('upload_form_hd_maxheight', '2000', NULL),
('upload_form_hd_maxwidth', '2000', NULL),
('upload_form_hd_quality', '95', NULL),
('upload_form_hd_resize', 'false', NULL),
('upload_form_thumb_crop', 'false', NULL),
('upload_form_thumb_follow_orientation', 'true', NULL),
('upload_form_thumb_maxheight', '96', NULL),
('upload_form_thumb_maxwidth', '128', NULL),
('upload_form_thumb_quality', '95', NULL),
('upload_form_websize_maxheight', '600', NULL),
('upload_form_websize_maxwidth', '800', NULL),
('upload_form_websize_quality', '95', NULL),
('upload_form_websize_resize', 'true', NULL),
('user_can_delete_comment', 'false', 'administrators can allow user delete their own comments'),
('user_can_edit_comment', 'false', 'administrators can allow user edit their own comments'),
('week_starts_on', 'monday', 'Monday may not be the first day of the week');
