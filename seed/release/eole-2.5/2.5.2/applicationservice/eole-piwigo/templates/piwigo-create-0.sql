-- création de la base de donnée
CREATE DATABASE piwigo CHARACTER SET latin1;

-- création du user de la base
grant all privileges on piwigo.* to piwigo@%%adresse_ip_web identified by 'piwigo';
flush privileges ;

-- connexion à la base
\r piwigo


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
