-- Idem glpi_maj_ocsplugin.sql
LOCK TABLES `glpi_crontasks` WRITE;
INSERT INTO `glpi_crontasks`(`itemtype`, `name`, `frequency`, `param`, `state`, `mode`, `allowmode`, `hourmin`, `hourmax`, `logs_lifetime`, `lastrun`, `lastcode`, `comment`) VALUES
('PluginOcsinventoryngThread','CleanOldThreads',3600,24,1,1,3,0,24,30,NULL,NULL,NULL),
('PluginOcsinventoryngNotimportedcomputer','SendAlerts',600,24,1,1,3,0,24,30,NULL,NULL,NULL),
('PluginOcsinventoryngOcsServer','ocsng',300,NULL,0,1,3,0,24,30,NULL,NULL,NULL);
UNLOCK TABLES;

LOCK TABLES `glpi_notificationtemplates` WRITE;
INSERT INTO `glpi_notificationtemplates`(`name`, `itemtype`, `date_mod`, `comment`, `css`) VALUES
('Computers not imported','PluginOcsinventoryngNotimportedcomputer','2016-01-28 09:40:04','',NULL);
UNLOCK TABLES;

set @notificationtemplates_id=(SELECT `glpi_notificationtemplates`.`id` FROM `glpi_notificationtemplates` WHERE `glpi_notificationtemplates`.`name`='Computers not imported');

LOCK TABLES `glpi_notifications` WRITE;
INSERT INTO `glpi_notifications`(`name`, `entities_id`, `itemtype`, `event`, `mode`, `notificationtemplates_id`, `comment`, `is_recursive`, `is_active`, `date_mod`) VALUES
('Computers not imported',0,'PluginOcsinventoryngNotimportedcomputer','not_imported','mail',@notificationtemplates_id,'',1,1,'2016-01-28 09:40:04');
UNLOCK TABLES;

LOCK TABLES `glpi_notificationtemplatetranslations` WRITE;
INSERT INTO `glpi_notificationtemplatetranslations`(`notificationtemplates_id`, `language`, `subject`, `content_text`, `content_html`) VALUES
(@notificationtemplates_id,'','##lang.notimported.action## : ##notimported.entity##','\r\n\n##lang.notimported.action## :&#160;##notimported.entity##\n\n##FOREACHnotimported##&#160;\n##lang.notimported.reason## : ##notimported.reason##\n##lang.notimported.name## : ##notimported.name##\n##lang.notimported.deviceid## : ##notimported.deviceid##\n##lang.notimported.tag## : ##notimported.tag##\n##lang.notimported.serial## : ##notimported.serial## \r\n\n ##notimported.url## \n##ENDFOREACHnotimported## \r\n','&lt;p&gt;##lang.notimported.action## :&#160;##notimported.entity##&lt;br /&gt;&lt;br /&gt;##FOREACHnotimported##&#160;&lt;br /&gt;##lang.notimported.reason## : ##notimported.reason##&lt;br /&gt;##lang.notimported.name## : ##notimported.name##&lt;br /&gt;##lang.notimported.deviceid## : ##notimported.deviceid##&lt;br /&gt;##lang.notimported.tag## : ##notimported.tag##&lt;br /&gt;##lang.notimported.serial## : ##notimported.serial##&lt;/p&gt;\r\n&lt;p&gt;&lt;a href=\"##notimported.url##\"&gt;##notimported.url##&lt;/a&gt;&lt;br /&gt;##ENDFOREACHnotimported##&lt;/p&gt;');
UNLOCK TABLES;

LOCK TABLES `glpi_plugins` WRITE;
INSERT INTO `glpi_plugins` (`directory`, `name`, `version`, `state`, `author`, `homepage`, `license`) VALUES
('ocsinventoryng', 'OCS Inventory NG', '1.1.1', 1, 'Remi Collet, Nelly Mahu-Lasson, David Durieux, Xavier Caillaud, Walid Nouh, Arthur Jaouen', 'https://forge.indepnet.net/projects/ocsinventoryng', 'GPLv2+');
UNLOCK TABLES;

LOCK TABLES `glpi_displaypreferences` WRITE;
/*!40000 ALTER TABLE `glpi_displaypreferences` DISABLE KEYS */;
INSERT INTO `glpi_displaypreferences` (`itemtype`, `num`, `rank`, `users_id`) VALUES
('PluginOcsinventoryngOcsServer',3,1,0),
('PluginOcsinventoryngOcsServer',19,2,0),
('PluginOcsinventoryngOcsServer',6,3,0),
('PluginOcsinventoryngNotimportedcomputer',2,1,0),
('PluginOcsinventoryngNotimportedcomputer',3,2,0),
('PluginOcsinventoryngNotimportedcomputer',4,3,0),
('PluginOcsinventoryngNotimportedcomputer',5,4,0),
('PluginOcsinventoryngNotimportedcomputer',6,5,0),
('PluginOcsinventoryngNotimportedcomputer',7,6,0),
('PluginOcsinventoryngNotimportedcomputer',8,7,0),
('PluginOcsinventoryngNotimportedcomputer',9,8,0),
('PluginOcsinventoryngNotimportedcomputer',10,9,0),
('PluginOcsinventoryngDetail',5,1,0),
('PluginOcsinventoryngDetail',2,2,0),
('PluginOcsinventoryngDetail',3,3,0),
('PluginOcsinventoryngDetail',4,4,0),
('PluginOcsinventoryngDetail',6,5,0),
('PluginOcsinventoryngDetail',80,6,0);
UNLOCK TABLES;

LOCK TABLES `glpi_profilerights` WRITE;
INSERT INTO `glpi_profilerights`(`profiles_id`, `name`, `rights`) VALUES
(1,'plugin_ocsinventoryng',0),
(2,'plugin_ocsinventoryng',0),
(3,'plugin_ocsinventoryng',0),
(4,'plugin_ocsinventoryng',23),
(5,'plugin_ocsinventoryng',0),
(6,'plugin_ocsinventoryng',0),
(7,'plugin_ocsinventoryng',0),
(1,'plugin_ocsinventoryng_sync',0),
(2,'plugin_ocsinventoryng_sync',0),
(3,'plugin_ocsinventoryng_sync',0),
(4,'plugin_ocsinventoryng_sync',3),
(5,'plugin_ocsinventoryng_sync',0),
(6,'plugin_ocsinventoryng_sync',0),
(7,'plugin_ocsinventoryng_sync',0),
(1,'plugin_ocsinventoryng_view',0),
(2,'plugin_ocsinventoryng_view',0),
(3,'plugin_ocsinventoryng_view',0),
(4,'plugin_ocsinventoryng_view',1),
(5,'plugin_ocsinventoryng_view',0),
(6,'plugin_ocsinventoryng_view',0),
(7,'plugin_ocsinventoryng_view',0),
(1,'plugin_ocsinventoryng_clean',0),
(2,'plugin_ocsinventoryng_clean',0),
(3,'plugin_ocsinventoryng_clean',0),
(4,'plugin_ocsinventoryng_clean',3),
(5,'plugin_ocsinventoryng_clean',0),
(6,'plugin_ocsinventoryng_clean',0),
(7,'plugin_ocsinventoryng_clean',0),
(1,'plugin_ocsinventoryng_rule',0),
(2,'plugin_ocsinventoryng_rule',0),
(3,'plugin_ocsinventoryng_rule',0),
(4,'plugin_ocsinventoryng_rule',3),
(5,'plugin_ocsinventoryng_rule',0),
(6,'plugin_ocsinventoryng_rule',0),
(7,'plugin_ocsinventoryng_rule',0);
UNLOCK TABLES;

UPDATE `glpi_plugin_ocsinventoryng_ocsservers` SET `ocs_version`='7006';
--
-- Structure de la table `glpi_plugin_ocsinventoryng_configs`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_log_frequency` int(11) NOT NULL DEFAULT '10',
  `is_displayempty` int(1) NOT NULL DEFAULT '1',
  `import_limit` int(11) NOT NULL DEFAULT '0',
  `delay_refresh` int(11) NOT NULL DEFAULT '0',
  `allow_ocs_update` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `glpi_plugin_ocsinventoryng_configs`
--

INSERT INTO `glpi_plugin_ocsinventoryng_configs` (`id`, `thread_log_frequency`, `is_displayempty`, `import_limit`, `delay_refresh`, `allow_ocs_update`, `comment`) VALUES
(1, 2, 1, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_details`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entities_id` int(11) NOT NULL DEFAULT '0',
  `plugin_ocsinventoryng_threads_id` int(11) NOT NULL DEFAULT '0',
  `rules_id` text COLLATE utf8_unicode_ci,
  `threadid` int(11) NOT NULL DEFAULT '0',
  `ocsid` int(11) NOT NULL DEFAULT '0',
  `computers_id` int(11) NOT NULL DEFAULT '0',
  `action` int(11) NOT NULL DEFAULT '0',
  `process_time` datetime DEFAULT NULL,
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `end_time` (`process_time`),
  KEY `process_thread` (`plugin_ocsinventoryng_threads_id`,`threadid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_devicebiosdatas`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_devicebiosdatas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assettag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturers_id` int(11) NOT NULL DEFAULT '0',
  `entities_id` int(11) NOT NULL DEFAULT '0',
  `is_recursive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `manufacturers_id` (`manufacturers_id`),
  KEY `entities_id` (`entities_id`),
  KEY `is_recursive` (`is_recursive`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_items_devicebiosdatas`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_items_devicebiosdatas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) NOT NULL DEFAULT '0',
  `itemtype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin_ocsinventoryng_devicebiosdatas_id` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_dynamic` tinyint(1) NOT NULL DEFAULT '0',
  `entities_id` int(11) NOT NULL DEFAULT '0',
  `is_recursive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `computers_id` (`items_id`),
  KEY `plugin_ocsinventoryng_devicebiosdatas_id` (`plugin_ocsinventoryng_devicebiosdatas_id`),
  KEY `is_deleted` (`is_deleted`),
  KEY `is_dynamic` (`is_dynamic`),
  KEY `entities_id` (`entities_id`),
  KEY `is_recursive` (`is_recursive`),
  KEY `item` (`itemtype`,`items_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_networkports`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_networkports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `networkports_id` int(11) NOT NULL DEFAULT '0',
  `TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPEMIB` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_devicenetworkcards_id` int(11) NOT NULL DEFAULT '0',
  `speed` varchar(255) COLLATE utf8_unicode_ci DEFAULT '10mb/s',
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `networkports_id` (`networkports_id`),
  KEY `TYPE` (`TYPE`),
  KEY `TYPEMIB` (`TYPEMIB`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_networkporttypes`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_networkporttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OCS_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `OCS_TYPEMIB` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `instantiation_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'T, LX, SX',
  `speed` int(11) DEFAULT '10' COMMENT 'Mbit/s: 10, 100, 1000, 10000',
  `version` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'a, a/b, a/b/g, a/b/g/n, a/b/g/n/y',
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `OCS_TYPE` (`OCS_TYPE`),
  KEY `OCS_TYPEMIB` (`OCS_TYPEMIB`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `glpi_plugin_ocsinventoryng_networkporttypes`
--

INSERT INTO `glpi_plugin_ocsinventoryng_networkporttypes` (`id`, `name`, `OCS_TYPE`, `OCS_TYPEMIB`, `instantiation_type`, `type`, `speed`, `version`, `comment`) VALUES
(1, 'Unkown port', '*', '*', 'PluginOcsinventoryngNetworkPort', NULL, NULL, NULL, NULL),
(2, 'Ethernet port', 'Ethernet', '*', 'NetworkPortEthernet', 'T', 10, NULL, NULL),
(3, 'Wifi port', 'Wifi', '*', 'NetworkPortWifi', NULL, NULL, 'a', NULL),
(4, 'Loopback port', 'Local', '*', 'NetworkPortLocal', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_notimportedcomputers`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_notimportedcomputers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entities_id` int(11) NOT NULL DEFAULT '0',
  `rules_id` text COLLATE utf8_unicode_ci,
  `comment` text COLLATE utf8_unicode_ci,
  `ocsid` int(11) NOT NULL DEFAULT '0',
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL,
  `ocs_deviceid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `useragent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_inventory` datetime DEFAULT NULL,
  `reason` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ocs_id` (`plugin_ocsinventoryng_ocsservers_id`,`ocsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_ocsadmininfoslinks`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_ocsadmininfoslinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `glpi_column` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocs_column` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `plugin_ocsinventoryng_ocsservers_id` (`plugin_ocsinventoryng_ocsservers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
LOCK TABLES `glpi_plugin_ocsinventoryng_ocsadmininfoslinks` WRITE;
/*!40000 ALTER TABLE `glpi_plugin_ocsinventoryng_ocsadmininfoslinks` DISABLE KEYS */;
INSERT INTO `glpi_plugin_ocsinventoryng_ocsadmininfoslinks` VALUES (10,'contact_num','HARDWARE_ID',1),(9,'networks_id','HARDWARE_ID',1),(8,'groups_id','0',1),(7,'otherserial','HARDWARE_ID',1),(6,'locations_id','TAG',1);
/*!40000 ALTER TABLE `glpi_plugin_ocsinventoryng_ocsadmininfoslinks` ENABLE KEYS */;
UNLOCK TABLES;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_ocsservers_profiles`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_ocsservers_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL DEFAULT '0',
  `profiles_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `plugin_ocsinventoryng_ocsservers_id` (`plugin_ocsinventoryng_ocsservers_id`),
  KEY `profiles_id` (`profiles_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `glpi_plugin_ocsinventoryng_ocsservers_profiles`
--
set @mon_server_ocs_id=(SELECT MAX(`glpi_plugin_ocsinventoryng_ocsservers`.`id`) FROM `glpi_plugin_ocsinventoryng_ocsservers` WHERE `glpi_plugin_ocsinventoryng_ocsservers`.`name`='localhost');

INSERT INTO `glpi_plugin_ocsinventoryng_ocsservers_profiles` (`plugin_ocsinventoryng_ocsservers_id`, `profiles_id`) VALUES
(@mon_server_ocs_id, 4);


-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_servers`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL DEFAULT '0',
  `max_ocsid` int(11) DEFAULT NULL,
  `max_glpidate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin_ocsinventoryng_ocsservers_id` (`plugin_ocsinventoryng_ocsservers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `glpi_plugin_ocsinventoryng_threads`
--

CREATE TABLE IF NOT EXISTS `glpi_plugin_ocsinventoryng_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `threadid` int(11) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `error_msg` text COLLATE utf8_unicode_ci NOT NULL,
  `imported_machines_number` int(11) NOT NULL DEFAULT '0',
  `synchronized_machines_number` int(11) NOT NULL DEFAULT '0',
  `failed_rules_machines_number` int(11) NOT NULL DEFAULT '0',
  `linked_machines_number` int(11) NOT NULL DEFAULT '0',
  `notupdated_machines_number` int(11) NOT NULL DEFAULT '0',
  `not_unique_machines_number` int(11) NOT NULL DEFAULT '0',
  `link_refused_machines_number` int(11) NOT NULL DEFAULT '0',
  `total_number_machines` int(11) NOT NULL DEFAULT '0',
  `plugin_ocsinventoryng_ocsservers_id` int(11) NOT NULL DEFAULT '1',
  `processid` int(11) NOT NULL DEFAULT '0',
  `entities_id` int(11) NOT NULL DEFAULT '0',
  `rules_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `end_time` (`end_time`),
  KEY `process_thread` (`processid`,`threadid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
