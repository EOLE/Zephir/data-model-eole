#!/bin/bash

container_path_web=$(CreoleGet container_path_web)
container_ip_mysql=$(CreoleGet container_ip_mysql)
adresse_ip_mysql=$(CreoleGet adresse_ip_mysql)
if [ "$adresse_ip_mysql" = "localhost" ]
then
	adresse_ip_mysql="127.0.0.1"
fi

# récupération des paramètres de connexion à la base de données OCS
USEROCS=$(grep -R 'COMPTE_BASE' $container_path_web/usr/share/ocsinventory-reports/dbconfig.inc.php |cut -d'"' -f4)
PWDOCS=$(grep -R 'PSWD_BASE' $container_path_web/usr/share/ocsinventory-reports/dbconfig.inc.php |cut -d'"' -f4)

# récupération des paramètres de connexion à la base de données GLPI
PWDGLPI=$(grep -R 'dbpassword' $container_path_web/var/www/html/glpi/config/config_db.php |cut -d'"' -f2)

# mise à jour du champ TRACE_DELETED à 1 (true) de la table config de la base ocsweb pour fonctionnement du plugin ocsinventoryng
/usr/bin/mysql -u$USEROCS -p$PWDOCS -D ocsweb -h$adresse_ip_mysql -e "UPDATE config SET IVALUE = 1 WHERE config.NAME = 'TRACE_DELETED'"

# mise à jour de la table contenant les paramètres de connexion glpi à la base OCS
/usr/bin/mysql -u admin_glpi -p$PWDGLPI -D glpi -h$adresse_ip_mysql -e "UPDATE glpi_plugin_ocsinventoryng_ocsservers SET ocs_db_passwd = '"$PWDOCS"',ocs_db_user = '"$USEROCS"' WHERE glpi_plugin_ocsinventoryng_ocsservers.name = 'localhost'"
