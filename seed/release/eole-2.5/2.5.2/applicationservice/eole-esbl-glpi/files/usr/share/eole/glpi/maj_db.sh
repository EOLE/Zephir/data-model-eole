#!/bin/bash

container_path_web=$(CreoleGet container_path_web)
container_ip_mysql=$(CreoleGet container_ip_mysql)
adresse_ip_mysql=$(CreoleGet adresse_ip_mysql)

if [ $adresse_ip_mysql = '127.0.0.1' ]
then
	adresse_ip_mysql='localhost'
fi

if [ "$(CreoleGet mode_conteneur_actif)" = "oui" ]; then
    adresse_ip=$(CreoleGet container_ip_web)
else
    adresse_ip=$(CreoleGet adresse_ip_eth0)
fi

PWD=$(grep -R 'dbpassword' $container_path_web/var/www/html/glpi/config/config_db.php |cut -d'"' -f2)

VERSION=$(/usr/bin/mysql -u admin_glpi -p$PWD -h$adresse_ip_mysql -e "select value FROM glpi.glpi_configs where name='version'" 2>/dev/null| tail -1)
if ! echo $VERSION | grep -q "0.85.3"
then
	echo "Mise à jour de la base de données GLPI"
	wget --quiet --delete-after --no-check-certificate --post-data 'from_update=Mise+%C3%A0+jour' http://$adresse_ip/glpi/install/update.php  1>/dev/null
	#wget --post-data 'from_update=Mise+%C3%A0+jour' http://$adresse_ip_eth0/glpi/install/update.php  #1>/dev/null
	/usr/bin/mysql -u admin_glpi -p$PWD -h$adresse_ip_mysql glpi < /usr/share/eole/glpi/glpi_maj_ocsplugin.sql 1>/dev/null
	echo "Mise à jour terminée $?"
fi

/usr/bin/mysql -u admin_glpi -p$PWD -h$adresse_ip_mysql glpi < /usr/share/eole/glpi/glpi-update.sql
exit 0
