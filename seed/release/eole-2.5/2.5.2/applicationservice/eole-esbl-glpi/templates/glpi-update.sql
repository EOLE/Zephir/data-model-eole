-- ------------------------------------------------- --
-- configuration de l'authentification CAS dans GLPI --
-- ------------------------------------------------- --

\r glpi
-- documentation URL
%set %%helpdesk_doc_url = %%getVar('glpi_helpdesk_doc_url', 'http://www.glpi-project.org/spip.php?rubrique3')
%set %%central_doc_url = %%getVar('glpi_central_doc_url', 'http://www.glpi-project.org/spip.php?rubrique3')
LOCK TABLES `glpi_configs` WRITE;
UPDATE `glpi`.`glpi_configs` SET `value` = '%%helpdesk_doc_url' WHERE `glpi_configs`.`name` ='helpdesk_doc_url';
UPDATE `glpi`.`glpi_configs` SET `value` = '%%central_doc_url' WHERE `glpi_configs`.`name` ='central_doc_url';
UNLOCK TABLES;

%if %%glpi_config_email == 'oui'
-- mail admin glpi --
LOCK TABLES `glpi_configs` WRITE;
%if %%glpi_admin_email != ''
UPDATE `glpi`.`glpi_configs` SET `value` = '%%glpi_admin_email' WHERE `glpi_configs`.`name` ='admin_email';
%end if
%if %%system_mail_from != ''
UPDATE `glpi`.`glpi_configs` SET `value` = '%%system_mail_from' WHERE `glpi_configs`.`name` ='admin_reply';
%end if
UNLOCK TABLES;
%end if


%if %%getVar('glpi_ldap_auth', 'ne rien faire') == "oui"
-- LDAP --
-- procedure de MAJ du groupe DomainAdmins si auth_ldap --
DELIMITER //
DROP PROCEDURE IF EXISTS AddDomainAdmins//
CREATE PROCEDURE AddDomainAdmins()
BEGIN
	DECLARE compteur int(5) default 0;
	SELECT count(*) FROM `glpi_groups` WHERE `name`='LDAPAdmins' INTO compteur;
	IF compteur = 0 THEN INSERT INTO `glpi_groups` (`entities_id`, `is_recursive`, `name`, `comment`, `groups_id`, `ldap_field`, `ldap_value`, `ldap_group_dn`, `completename`,`level`,`is_requester`, `is_assign`, `is_notify`, `is_itemgroup`, `is_usergroup`) VALUES (0,0,'LDAPAdmins',NULL,0,NULL,NULL,'%%glpi_domainadmins_dn','LDAPAdmins',1,1,1,1,1,1);
	INSERT INTO `glpi_authldaps` (`id`) VALUES (1);
	INSERT INTO `glpi_rules` (`entities_id`, `sub_type`, `ranking`, `name`, `description`, `match`, `is_active`, `comment`, `is_recursive`) VALUES (0,'RuleRight',2,'LDAPAdmins','','OR',1,'',0);
	set @LDAPAdmins_rule_id=(SELECT `glpi_rules`.`id` FROM `glpi_rules` WHERE `glpi_rules`.`name`='LDAPAdmins');
	INSERT INTO `glpi_rulecriterias` (`rules_id`, `criteria`, `condition`, `pattern`) VALUES (@LDAPAdmins_rule_id,'GROUPS',0,'1');
	INSERT INTO `glpi_ruleactions` (`rules_id`, `action_type`, `field`, `value`) VALUES (@LDAPAdmins_rule_id,'assign','profiles_id','4');
	ELSE UPDATE `glpi`.`glpi_groups` SET `ldap_group_dn`='%%glpi_domainadmins_dn' WHERE name='LDAPAdmins';
	END IF;
END//
DELIMITER ;
-- Fin procedure --

CALL AddDomainAdmins();
DROP PROCEDURE IF EXISTS AddDomainAdmins;
LOCK TABLES `glpi_authldaps` WRITE;
%if %%ldap_tls == 'oui'
-- probleme de certificat pour TLS, desactive (#3936)
-- %set %%use_tls = 1
%set %%use_tls = 0
%else
%set %%use_tls = 0
%end if
UPDATE `glpi`.`glpi_authldaps` SET `name` = 'LDAP', `host` = '%%adresse_ip_ldap', `basedn` = '%%glpi_domainadmins_dn', `port` = 389,
`login_field` = 'mail', `use_tls` = %%use_tls, `group_condition` = '(objectClass=posixGroup)', `group_search_type` = 1,
`group_member_field` = 'memberuid', `email1_field` = 'mail', `realname_field` = 'sn', `firstname_field` = 'givenname', `use_dn` = 0,
`is_default` = 1, `is_active` = 1, `comment_field` = 'departmentnumber' WHERE `glpi_authldaps`.`id` =1 LIMIT 1 ;
UNLOCK TABLES;
%end if

%if %%getVar('glpi_cas_auth', 'ne rien faire') == "oui"
-- CAS --
LOCK TABLES `glpi_configs` WRITE;
UPDATE `glpi`.`glpi_configs` SET `value` = '%%eolesso_adresse' WHERE `glpi_configs`.`name` ='cas_host';
UPDATE `glpi`.`glpi_configs` SET `value` = '%%eolesso_port' WHERE `glpi_configs`.`name` ='cas_port';
UPDATE `glpi`.`glpi_configs` SET `value` = '%%getVar("eolesso_dossier", "")' WHERE `glpi_configs`.`name` ='cas_uri';
UPDATE `glpi`.`glpi_configs` SET `value` = 'https://%%eolesso_adresse:%%eolesso_port/logout?service=https://%%web_url/glpi/front/login.php' WHERE `glpi_configs`.`name` ='cas_logout';
%elif %%glpi_cas_auth == "non"
UPDATE `glpi`.`glpi_configs` SET `value` = '' WHERE `glpi_configs`.`name` ='cas_host';
UPDATE `glpi`.`glpi_configs` SET `value` = '' WHERE `glpi_configs`.`name` ='cas_port';
UPDATE `glpi`.`glpi_configs` SET `value` = '' WHERE `glpi_configs`.`name` ='cas_uri';
UPDATE `glpi`.`glpi_configs` SET `value` = '' WHERE `glpi_configs`.`name` ='cas_logout';
UNLOCK TABLES;
%end if
