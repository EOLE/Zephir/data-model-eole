# -*- coding: utf-8 -*-
###########################################################################
# Eole - 2007 - 2018
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
#  Fonctions de calcul et validation des variables Creole
#
###########################################################################
import subprocess
import stat
from ast import literal_eval
try:
    from pyeole.process import system_out
except:
    pass
#from pyeole.deprecation import deprecated

from creole.config import func_dir, containers_default_network, \
 INSTANCE_LOCKFILE
from creole.error import TypeEoleError, FileNotFound, ConfigError
from creole.utils import classes, gen_random as _gen_random
#from .wpkg_secrets import wcrypt, wdecrypt
import imp
try:
    from tiramisu.error import ValueWarning
except:
    class ValueWarning(Warning):
        def __init__(self, msg, *args, **kwargs):
            super(Warning, self).__init__(msg)
from IPy import IP
try:
    import pyudev
except:
    pass
    # librairie non disponible (Zéphir 2.3)

from creole.i18n import _

# modules utilitaires à rendre accessible dans les fonctions
import os, sys, re, glob, time, hashlib, random, socket, pwd, grp
"""Version variable of EOLE distribution

"""

UBUNTU_VERSION = 'xenial'
"""Ubuntu version used by EOLE.

"""
EOLE_VERSION = '2.6'
"""Current stable EOLE distribution.

"""

EOLE_RELEASE = '{0}.2'.format(EOLE_VERSION)
"""Release version of the current stable EOLE distribution.

"""

ENVOLE_VERSION = '6'
"""Envole version to use.

"""

LAST_RELEASE = u'10'
"""Last stable release for this version

"""
# type paramètre
class Param:
    """paramètre pour les fonctions de contrainte"""
    def __init__(self, value, name=None, typ=None):
        self.value = value
        self.name = name
        self.type = typ


#Tiramisu internal function
func_on_zephir_context = []


def execute_on_zephir_context(func):
    func_on_zephir_context.append(func.__name__)
    return func


@execute_on_zephir_context
def gen_random(*args, **kwargs):
    return _gen_random(*args, **kwargs)

#############################
## Fonctions de vérification
#############################
@execute_on_zephir_context
def calc_container(mode_container, container_info, available_probes='non'):
    """ retourne une variable de conteneur """
    #container_info contient l'ip du conteneur directement (#6240)
    return container_info


@execute_on_zephir_context
def get_version(name):
    try:
        return name
    except AttributeError as err:
        raise ValueError(_(u'Unknown variable {0}').format(name))


@execute_on_zephir_context
def valid_ip(data):
    """fonction de validation de la saisie d'une adresse ip"""

    # variable non obligatoire
    if data == "":
        return True

    # découpage de l'ip
    ip = data.split('.')
    if len(ip) != 4:
        raise ValueError(_(u"Wrong IP address format"))

    # vérification des nombres
    for part in ip:
        try:
            num = int(part)
        except ValueError:
            raise ValueError(_(u"An IP address is a sequence of four numbers separated by dots."))
        else:
            if not (0 <= num < 256):
                raise ValueError(_(u"Each number is in range [0, 255]."))

    # on n'a pas rencontré d'erreur
    return True


@execute_on_zephir_context
def valid_intervalle(data):
    """ fonction de validation d'une plage d'ip """
    if data == "":
        return True

    liste = data.split()
    if len(liste) != 2:
        raise ValueError(_(u"Two IP addresses are required."))
    else:
        # on vérifie la syntaxe des ips
        for ip in liste:
            valid_ip(ip)
    return True


@execute_on_zephir_context
def valid_network(data, ip=None):
    """fonction de validation de la saisie d'un réseau"""
    if data == "":
        return True

    # on utilise la fonction de test d'adresse ip pour la syntaxe
    valid_ip(data)
    if ip is not None:
        # on effectue les tests spécifiques au réseau (fonction de l'ip ?)
        ip_parts = ip.split(".")
        network_parts = data.split(".")
        for i in range(4):
            if (int(network_parts[i]) != 0) and (network_parts[i] != ip_parts[i]):
                raise ValueError(_(u"Specified network does not match IP address {0}").format(ip))

    # aucune erreur rencontrée
    return True


@execute_on_zephir_context
def valid_in_list(data, list_datas, error_msg=_("la valeur '{0}' n'est pas dans la liste disponible ({1})")):
    if list_datas and data not in list_datas:
        raise ValueError(error_msg.format(data, ', '.join(list_datas)))


@execute_on_zephir_context
def valid_netmask(data):
    """fonction de validation de la saisie d'un masque réseau"""
    if data == "":
        return True

    # on utilise la fonction de test d'adresse ip pour la syntaxe
    valid_ip(data)
    # on effectue les tests spécifiques aux masques réseaux
    netmask = []
    for part in data.split("."):
        netmask.append(int(part))
    error = 0
    mask_len = 0
    done = 0
    for part in netmask:
        bitmask = 0x80
        for bit in range(8):
            if not done:
                if part & bitmask:
                    mask_len = mask_len + 1
                else:
                    done = 1
            else:
                if part & bitmask:
                    error = 1
            bitmask = bitmask >> 1

    if error == 1:
        raise ValueError(_(u"Subnet mask format invalid"))
    # pas d'erreur rencontrée
    return True


@execute_on_zephir_context
def valid_booleen(data, type_bool="alpha"):
    """fonction de validation d'une réponse booléenne
       type_bool : alpha ou num (O/N ou 0/1)
    """
    if data == "":
        return True

    if (data[0].upper() != 'O') and (data[0].upper() != 'N'):
        raise ValueError(_(u"You must answer 'O' or 'N'"))
    # pas d'erreurs détectées
    return True


@execute_on_zephir_context
def valid_upper(data):
    """
    validation d'une valeur en majuscules
    """
    if data != data.upper():
        raise ValueError(_(u"Value must be uppercase."))
    return True


@execute_on_zephir_context
def valid_lower(data):
    """
    validation d'une valeur en minuscules
    """
    if data != data.lower():
        raise ValueError(_(u"Value must be lowercase."))
    return True


@execute_on_zephir_context
def valid_enum(data, liste=['oui','non','Oui','Non','OUI','NON','o','n','O','N'], checkval="True"):
    """fonction de validation d'une réponse en fonction
    d'une liste de valeur possibles
    """
    if data == "":
        return True

    try:
        if type(liste) != list:
            liste = literal_eval(liste)
    except:
        raise ValueError(_(u"Invalid potential values list"))
    if data not in liste and checkval != "False":
        raise ValueError(_(u"Choose one of these options: {0}").format(liste))
    return True


@execute_on_zephir_context
def valid_regexp(data, exp_reg, err_msg=_(u"Invalid syntax")):
    """fonction de validation d'une saisie par expression régulière"""
    if data == "":
        return True
    match = re.match(exp_reg, data)
    if match is None:
        raise ValueError(err_msg)
    else:
        return True


@execute_on_zephir_context
def valid_entier(data, mini=None, maxi=None):
    """ fonction de validation d'un nombre (intervalle optionnel)"""
    if data == "":
        return True
    try:
        value = int(data)
    except ValueError:
        raise ValueError(_(u"A number is required."))
    else:
        if mini is not None and value < int(mini):
            raise ValueError(_(u"Give an integer greater than {0}").format(mini))
        if maxi is not None and value > int(maxi):
            raise ValueError(_(u"Give an integer lesser than {0}").format(maxi))
        return True


@execute_on_zephir_context
def valid_len(data, length=None, max_len=None, min_len=None):
    """valide la longueur d'une variable
    * length : longueur fixe de la variable
    * max_len : taille maximum
       et/ou
      min_len : taille minimum

    Attention il faut utiliser soit length soit max_len/min_len mais pas les deux groupes ensemble
    """
    if length == max_len == min_len == None:
        raise ValueError(_(u'valid_len : a length must be given, either an exact length or a maximum or minimum length.'))
    if length is not None and (max_len is not None or min_len is not None):
        raise ValueError(_(u'valid_len : an exact length can not be used with a minimum or a maximum length.'))
    if length is not None:
        if len(data) != int(length):
            raise ValueError(_(u'The exact length of the given variable must be {0}.').format(length))
    else:
        if max_len is not None and len(data) > int(max_len):
            raise ValueError(_(u'The length of the given variable must be lesser than {0}').format(max_len))
        if min_len is not None and len(data) < int(min_len):
            raise ValueError(_(u'The length of the given variable must be greater than {0}').format(min_len))


@execute_on_zephir_context
def valid_yesno(data):
    """Yes ou No pour les fichiers de config"""
    if data not in ["Yes", "No"]:
        raise ValueError(_(u"Value must be either Yes or No."))
    return True


def valid_system(data, cmd, result, *args):
    """ test d'une commande système """
    if data == "":
        return True

    cmd_string = cmd
    for arg in args:
        cmd_string += " "+arg

    # exécution de la fonction
    code_retour = os.system(cmd_string)

    if code_retour == result:
        return True
    else:
        raise ValueError(_(u"Command {0} execution has failed").format(cmd_string))


@execute_on_zephir_context
def valid_differ(data, value):
    """ test d'une valeur indésirable """
    if data == value:
        raise ValueError(_(u"Value must be different from {0}").format(value))
    else:
        return True


@execute_on_zephir_context
def valid_chaine(data, forbidden_chars="[' ']"):
    """
    Recherche de caractères interdits dans une chaine
    """
    forbidden_chars = literal_eval(forbidden_chars)
    for char in forbidden_chars:
        if char in data:
            raise ValueError(_(u"\"{0}\" character is not allowed.").format(char))
    return True


@execute_on_zephir_context
def valid_name(data):
    """
    nom valide
    """
    valid_lower(data)
    valid_chaine(data, forbidden_chars="['@', ' ', '_']")
    return True


@execute_on_zephir_context
def valid_domaine(data):
    """
    nom de domaine valide
    """
    valid_name(data)
    valid_regexp(data, '.+\..+', _(u"Domain name is missing a top-level domain (TLD)."))
    return True


@execute_on_zephir_context
def valid_alias(data):
    """
    alias apache valide
    """
    valid_name(data)
    valid_regexp(data, '^/', _(u"Aliases must start with a slash (\"/\")."))
    return True


@execute_on_zephir_context
def valid_phpmyadmin(data, apache):
    """ phpmyadmin dépend d'apache """
    if data == 'oui' and apache == 'non':
        raise ValueError(_(u"Activating Apache is required in order to activate phpMyAdmin."))
    return True


@execute_on_zephir_context
def valid_addr_sso(data, sso_actif, revprox_actif="non", revprox_sso="", revprox_domainname="", *args):
    """vérifie les conflits entre sso local et reverse proxy"""
    if data in ['localhost', '127.0.0.1']:
        raise ValueError(_(u"SSO service must listen on an interface other than localhost."))
    if revprox_sso and revprox_actif == 'oui':
        if data in args and sso_actif == 'oui':
            raise ValueError(_(u"Local SSO service is in conflict with reverse proxy, use IP address {0} instead.").format(revprox_domainname or revprox_sso))
    return True


@execute_on_zephir_context
def valid_country(data):
    try:
        error_val = not data.isalpha()
    except:
        raise ValueError(_(u'Value must be of type string.'))
    if error_val:
        raise ValueError(_(u'Value must be uppercase letters.'))
    valid_len(data, 2)
    try:
        valid_upper(data)
    except ValueError as err:
        #uniquement warning pour compatibilité #8585
        raise ValueWarning(unicode(str(err), 'utf-8'), None)
    return True


@execute_on_zephir_context
def verif_cas_active(data, var_name, full_cas):
    """ vérifie l'activation du full_cas
    """
    if data == 'oui' and full_cas == 'non':
        raise ValueError(_(u"Please activate \"Utilisation du service sso pour les applications de votre serveur Scribe\" before activating {0}.").format(var_name))
    return True


@execute_on_zephir_context
def obligatoire(data):
    """ fonction qui vérifie que les données ne sont pas vides
    """
    if data == [] or data == "" or data[0].strip() == "":
        raise TypeEoleError(_(u"This parameter is mandatory."))
    else:
        return True

@execute_on_zephir_context
def is_empty(data):
    if str(data) in ['', '""', "''", "[]", "['']", '[""]', "None"]:
        return True
    return False


@execute_on_zephir_context
def check_name_uniq(value, values, index):
    values.pop(index)
    if value in values:
        raise ValueError('le nom {} est déjà attribué à une autre plage'.format(value))


################################################################
## Fonctions de calcul de dépendance entre variables et groupes
################################################################
@execute_on_zephir_context
def hidden_if_not_in(val, *args):
    """ on cache si la valeur n'est pas présente dans celles proposées """
    if val not in args:
        return None
        #raise NoValueReturned("""%s n'est pas dans %s""" % (val, args))


@execute_on_zephir_context
def hidden_if_in(val, *args):
    """ on cache si la valeur est présente dans celles proposées """
    if val in args:
        return None
        #raise NoValueReturned("""%s est dans %s""" % (val, args))

#############################################
## Fonctions de saisie et calcul des valeurs
#############################################
def get_devices():
    cmd = ['/sbin/ip', '-4', '-o', 'address', 'show', 'up', 'primary']
    code, stdout, stderr = system_out(cmd)

    if code != 0:
        raise Exception(_(u'Error running command {0} : {1}').format(' '.join(cmd), stderr))

    devs = {}
    for line in stdout.splitlines():
        dev = line.strip().split()
        if dev[2] != 'inet':
            continue
        #get aliases
        devname = dev[1]
        try:
            if dev[8].startswith(devname) and devname != dev[8]:
                #le 8eme argument comprend le vrai nom de l'interface (notamment pour les alias)
                devname = dev[8][:-1]
        except:
            pass
        if '/' in dev[3]:
            ip, netmask = dev[3].split('/')
        else:
            ip = dev[3]
            netmask = '32'
        if dev[4] == 'brd':
            broadcast = dev[5]
        else:
            broadcast = None
        devs[devname] = (ip, calc_netmask(netmask), broadcast)

    return devs


def get_net_devices():
    devices = []
    PERSISTENT = '/var/lib/eole/config/persistent-net.cfg'
    if os.path.isfile(PERSISTENT):
        fh = file(PERSISTENT, 'r')
        for name in fh.readlines():
            devices.append(name.strip())
    new_devices = []
    for dirname in glob.glob('/sys/class/net/*'):
        if os.path.islink(os.path.join(dirname, 'device', 'driver')):
            name = os.path.split(dirname)[1]
            new_devices.append(name)
    new_devices.sort()
    for device in new_devices:
        if device not in devices:
            devices.append(device)
    return devices


def get_net_devices_name(devices=None):
    if devices is None:
        devices = get_devices()
    names = list(devices.keys())
    names.sort()
    return names


def get_net_devices_ip():
    ips = []
    devices = get_devices()
    for nom_carte in devices:
        ips.append(devices[nom_carte][0])
    return ips


def get_net_devices_netmask():
    ips = []
    devices = get_devices()
    for nom_carte in devices:
        ips.append(devices[nom_carte][1])
    return ips


def get_net_device(index):
    devices = get_net_devices()
    if not index < len(devices):
        return None
    return devices[index]


@execute_on_zephir_context
def calc_broadcast(ip, netmask):
    """ calcule l'adresse de broadcast par défaut à partir d'un masque et d'une ip """
    # si les ip et netmask viennent du dictionnaire, on les récupère
    if is_empty(ip) or is_empty(netmask):
        return None
    broadcast, network = calc_adresses(ip, netmask)
    return broadcast


@execute_on_zephir_context
def create_ip(ip_list):
    """ création d'une adresse ip à partir d'une liste"""
    template = """%i.%i.%i.%i"""
    return template % tuple(ip_list)


@execute_on_zephir_context
def calc_adresses(ip, mask):
    """calcul l'adresse de broadcast à partir de l'ip et du netmask
    """
    if is_empty(mask) or is_empty(ip):
        return None, None
    try:
        ipy = IP('{0}/{1}'.format(ip, mask), make_net=True)
    except ValueError:
        raise ValueError(_(u'IP or subnet mask invalid ({0}, {1})').format(ip, mask))
    network = str(ipy.net())
    broadcast = str(ipy.broadcast())
    return broadcast, network


@execute_on_zephir_context
def calc_classe(netmask):
    """calcule la classe du réseau à partir du netmask"""

    if netmask in classes:
        return classes[netmask]
    else:
        return netmask


@execute_on_zephir_context
def calc_netmask(plage):
    """calcule le reseau a partir de sa classe"""
    if plage in classes.values():
        for mask, plaj in classes.items():
            if plaj == plage:
                return mask
    else:
        return plage


def calc_pg_cache_size():
    """
    Calcule la taille du cache postgreql en fonction de la RAM
    """
    procmem = open('/proc/meminfo')
    meminfo = procmem.read().split('\n')
    procmem.close()
    for line in meminfo:
        if line.startswith('MemTotal'):
            mem_tot = float(line.split()[1])
            break
    return int( (mem_tot * 0.50) / 8 )


def calc_mysql_innodb_size():
    """
    Calcule la taille du cache mysql en fonction de la RAM
    """
    procmem = open('/proc/meminfo')
    meminfo = procmem.read().split('\n')
    procmem.close()
    for line in meminfo:
        if line.startswith('MemTotal'):
            mem_tot = float(line.split()[1])
            break
    mem_cache = int( (mem_tot * 0.72) /1024)
    return mem_cache


@execute_on_zephir_context
def calc_val(valeur=None):
    """ valeur par défaut "en dur" """
    return(valeur)


@execute_on_zephir_context
def calc_val_first_value(*args):
    if args == tuple():
        return None
    return args[0]


@execute_on_zephir_context
def calc_ssl_country_name(valeur):
    """Fonction spécifique à ssl_country_name
    Si la longueur de la valeur copier n'est pas de 2 force la valeur 'FR'
    """
    if valeur == None or len(valeur) != 2 or not valeur.isalpha():
        return u"FR"
    return valeur.upper()


@execute_on_zephir_context
def concat(*args, **kwargs):
    """ concatène deux valeurs """
    sortedkeys = list(kwargs.keys())
    sortedkeys.sort()
    sortedkwvalues = []
    for key in sortedkeys:
        if kwargs[key] == '' or kwargs[key] is None:
            return None
        sortedkwvalues.append(kwargs[key])
    if None in args:
        return None
    return "".join(args)+''.join(sortedkwvalues)


@execute_on_zephir_context
def concat_path(*args):
    """ concaténation de chemins """
    if None in args:
        return None
    values = []
    for val in args:
        if val.startswith('/'):
            values.append(val[1:])
        else:
            values.append(val)
    return '/' + os.path.join(*values)


@execute_on_zephir_context
def calc_multi_val(*args, **kwargs):
    """ valeur par défaut "en dur" avec un nombre non défini de paramètres
    Attention, une valeur vide passée en paramètre à cette fonction force
    le renvoi d'une liste vide ([]) sauf si allow_none vaut True
    Les doublons sont supprimés
    """
    res = []
    for arg in args:
        if arg is None:
            if kwargs.get('allow_none', u'False') == u'True':
                continue
            else:
                return []
        # gestion des valeurs multi
        if isinstance(arg, list):
            for ev_arg in arg:
                if ev_arg is not None and ev_arg not in res:
                    res.append(ev_arg)
        else:
            res.append(arg)
    return res


@execute_on_zephir_context
def get_list_item(lst,
                  index,
                  multi="False"):
    """Retourne une valeur de la liste à l'index indiqué en paramètre
    """
    if lst is None or index == -1 or index >= len(lst):
        if multi == "True":
            return []
        return None
    return lst[index]


@execute_on_zephir_context
def get_list_index(lst, condition):
    if lst is None or condition not in lst:
        return -1
    return lst.index(condition)


@execute_on_zephir_context
def calc_multi_condition(param, match=None, mismatch=None, operator='AND',
                         default_match=None, default_mismatch=None,
                         eval_match='False', eval_mismatch='False',
                         **conditions):
    """param: réponse testée sur toutes les conditions. Si les réponses des
    conditions sont différentes mettre une liste avec chacune des conditions
    a tester
    operator: doit être 'AND' ou 'OR'
    match: valeur de retour si toutes les conditions sont validées (si
           operator est à 'AND') ou si une des conditions sont validée
           (si operator est à 'OR')
    mismatch: valeur de retour si au moins une des conditions n'est pas
              validée (si operator est à 'AND') ou toutes les conditions
              ne sont pas validées (si operator est à 'OR')
    si les variables match ou mismatch sont optional ou hidden, recupère le
    contenu de la variable default_match ou default_mismatch
    conditions: liste des conditions a tester.
    exemple:
        <auto name='calc_multi_condition' target='test_activer_bareos_dir'>
            <param>['oui', 'non']</param>
            <param type='eole' name='condition_1'>activer_bareos</param>
            <param type='eole' name='condition_2'>activer_bareos_dir</param>
            <param name='match'>oui</param>
            <param name='mismatch'>non</param>
        </auto>
    """
    if operator not in ('AND', 'OR'):
        raise ValueError(_(u'Operator must be either "AND" or "OR"'))
    if eval_match not in ('True', 'False'):
        raise ValueError(_(u'eval_match must be either "True" or "False"'))
    if eval_mismatch not in ('True', 'False'):
        raise ValueError(_(u'eval_mismatch must be either "True" or "False"'))
    if match is None:
        if default_match is None:
            match = u'oui'
        else:
            if default_match == 'None':
                match = None
            else:
                match = default_match
    if mismatch is None:
        if default_mismatch is None:
            mismatch = u'non'
        else:
            if default_mismatch == 'None':
                mismatch = None
            else:
                mismatch = default_mismatch
    conditions_keys = list(conditions.keys())
    conditions_keys.sort()
    for condition in conditions_keys:
        if not condition.startswith('condition_'):
            raise ValueError(_(u'Condition keys must start with "condition".'))
    # si le paramètre est une liste...
    if param.startswith('['):
        param = literal_eval(param)
    # si il faut évaluer le résultat à retourner...
    if eval_match == 'True':
        match = literal_eval(match)
    if eval_mismatch == 'True':
        mismatch = literal_eval(mismatch)
    if isinstance(param, list) and len(param) != len(conditions):
        raise ValueError(_(u'Conditions and parameters counts do not match in calc_multi_condition.'))
    for num in range(0, len(conditions)):
        key = conditions_keys[num]
        value = conditions[key]
        if not isinstance(param, list):
            if operator == 'AND' and value != param:
                return mismatch
            if operator == 'OR' and value == param:
                return match
        else:
            if operator == 'AND' and value != param[num]:
                return mismatch
            if operator == 'OR' and value == param[num]:
                return match
    if operator == 'AND':
        return match
    else:
        return mismatch


@execute_on_zephir_context
def list_len_gt(param, max_len=None, match=None, mismatch=None,
                default_match=None, default_mismatch=None,
                eval_match='False', eval_mismatch='False',):
    """retourne la valeur 'match' si la longueur de la liste est supérieure à max_len, 'mismatch' sinon
    * max_len : longueur maximum de la liste à tester
    """
    if match is None:
        if default_match is None:
            match = u'oui'
        else:
            if default_match == 'None':
                match = None
            else:
                match = default_match
    if mismatch is None:
        if default_mismatch is None:
            mismatch = u'non'
        else:
            if default_mismatch == 'None':
                mismatch = None
            else:
                mismatch = default_mismatch
    # si il faut évaluer le résultat à retourner...
    if eval_match == 'True':
        match = literal_eval(match)
    if eval_mismatch == 'True':
        mismatch = literal_eval(mismatch)
    if isinstance(param, list):
        if len(param) > int(max_len):
            return match
        else:
            return mismatch
    else:
        return mismatch


@execute_on_zephir_context
def calc_binary_and(valeur1, valeur2):
    """
        Calcule un 'et' binaire :
        valeur1 & valeur2
    """
    if valeur1 == 'oui' and valeur2 == 'oui':
        return('oui')
    else:
        return('non')


@execute_on_zephir_context
def calc_libelle_annuaire(ldap_host, nom_machine, nom_domaine_local):
    """
    calcule un libellé pour l'annuaire local
    """
    if ldap_host is None:
        return None
    if ldap_host in ['127.0.0.1', 'localhost']:
        return _(u"LDAP directory of {0}.{1}").format(nom_machine, nom_domaine_local)
    try:
        hostname = socket.gethostbyaddr(ldap_host)[0]
    except:
        hostname = ldap_host
    return _(u"LDAP directory of {0}").format(hostname)


@execute_on_zephir_context
def calc_last_interface(nombre_int):
    return 'eth'+str(int(nombre_int)-1)


@execute_on_zephir_context
def extract_ad_domain_from_realm(realm):
    """Return the domain name of a REALM

    Split the realm accros the dots and return the first element.

    :param realm: realm FQDN
    :type realm: `str`
    :return: domain part of the realm
    :rtype: `str`

    """
    if realm is None:
        return None
    return realm.split('.')[0]

#################################################
## Fonctions de calcul des valeurs automatiques
#################################################
def server_mem():
    """renvoie la mémoire du serveur
    """
    procmem = open('/proc/meminfo')
    meminfo = procmem.read().split('\n')
    procmem.close()
    for line in meminfo:
        if line.startswith('MemTotal'):
            mem_tot = float(line.split()[1])
            break
    return mem_tot


def kernel_version():
    """ renvoie la version du kernel
    """
    return_code, output, stderr = system_out(['uname', '-'])
    if return_code == 0:
        return output
    else:
        return ''


@execute_on_zephir_context
def auto_copy_val(value):
    """
        Renvoie la valeur value
        utilisé pour forcer la valeur d'une variable
    """
    return(value)


def auto_eth(nom_carte, **kw):
    """ renvoie l'adresse de la carte nom_carte
    """
    if kw['parametre'].lower() == 'pppoe':
        nom_carte = 'ppp0'
    #if dhcp or pppoe
    if kw['condition'].lower() == kw['parametre'].lower() or nom_carte == 'ppp0':
        eths = get_devices()
        # sur Zéphir, nom_carte est None (non calculable). #19233
        if nom_carte and 'br'+nom_carte in eths.keys():
            nom_carte = 'br'+nom_carte
        #6106
        return eths.get(nom_carte, ('169.254.0.1', None, None))[0]
    else:
        return None


def auto_netmask(nom_carte, parametre, condition):
    """ renvoie le netmask pour la carte nom_carte
    """
    if parametre.lower() == 'pppoe':
        nom_carte = 'ppp0'
    if condition.lower() == parametre.lower() or nom_carte == 'ppp0':
        eths = get_devices()
        #6106
        val = eths.get(nom_carte, (None, '255.255.0.0', None))[1]
        try:
            return(val)
        except TypeError:
            return val
    else:
        return u"255.255.255.0"


@execute_on_zephir_context
def auto_ip_ldap(activer_client_ldap, **kw):
    """ renvoie l'adresse du serveur ldap local
    pas de valeur renvoyée si on choisit un ldap distant
    """
    if activer_client_ldap != 'local':
        return None
        #raise NoValueReturned
    else:
        # récupération de l'adresse du service local
        return kw['ip_annuaire']


def auto_broadcast(nom_carte, parametre, condition):
    """ renvoie le broadcast pour la carte nom_carte
    """
    if parametre.lower() == 'pppoe':
        nom_carte = 'ppp0'
    if condition.lower() == parametre.lower() or nom_carte == 'ppp0':
        eths = get_devices()
        broadcast = str(eths.get(nom_carte, ('', '', '169.254.255.255'))[2])
        return broadcast
    else:
        return None


def auto_defaultroute():
    """ renvoie la route par défaut
    """
    cmd = "ip route list scope global"
    cmd = cmd.split()
    process = subprocess.Popen(args=cmd, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE, shell=False)
    code = process.wait()
    if code != 0:
        return False
    defaultroute = process.stdout.read()
    defaultroute = re.findall('default\s+via\s+([\d\.]+)\s+dev\s+(\w+)\s+', defaultroute)
    if len(defaultroute) == 0:
        cmd = "ip route list"
        cmd = cmd.split()
        process = subprocess.Popen(args=cmd, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE, shell=False)
        code = process.wait()
        if code != 0:
            return False
        defaultroute = process.stdout.read()
        defaultroute = re.findall('default\s+dev\s+(\w+)\s+', defaultroute)
        if len(defaultroute) == 0:
            return [['0.0.0.0', None]]
        else:
            defaultroute = [['0.0.0.0', defaultroute[0]]]
    return defaultroute


def auto_defaultgw_ip():
    """ renvoie la route par défaut
    """

    ip = auto_defaultroute()[0][0]
    if ip == u'0.0.0.0':
        return None
    return ip


def auto_defaultroute_ip(method, calc_ip_gw):
    """ renvoie la route par défaut
    """
    if method == "statique" or calc_ip_gw == "non":
        return None
        #raise NoValueReturned
    #DHCP
    ip = auto_defaultroute()
    return ip[0][0]


def auto_defaultroute_int(method, calc_ip_gw):
    """ renvoie la route par défaut
    """

    if method == "statique" or calc_ip_gw == "non":
        return None
        #raise NoValueReturned
    if method == "pppoe":
        return 'ppp0'
    #DHCP
    rint = auto_defaultroute()
    return rint[0][1]


def get_interface_from_ip(ip):
    try:
        ip = IP(ip).strNormal(0)
        a = system_out(['ip', 'route', 'get', ip])
        dev = a[1].split('dev')[1].split()[0]
        if dev == 'lo':
            return 'eth0'
        return dev
    except:
        return 'eth0'


@execute_on_zephir_context
def get_zone_name_bridge(nom_carte, context):
    #si une interface conteneur est en mode bridge => l'interface est en mode bridge
    is_bridge = u'non'
    try:
        if isinstance(nom_carte, list):
            if len(nom_carte) <= 1:
                nom_zone = nom_carte[0]
            else:
                return is_bridge
        else:
            nom_zone = nom_carte
        for path in context.containers.interfaces.find(byname='linkto', byvalue=nom_zone, type_='path'):
            #remove .linkto to path
            path = path[:-7]
            if getattr(context, path + '.method') == 'bridge':
                is_bridge = u'oui'
                break
    except:
        pass
    return is_bridge


@execute_on_zephir_context
def get_zone_name(nom_carte, method, zone_is_bridge='non', interface_no=None):
    #seulement pour eth0/pppoe (pas de support de bridge sur cette interface)
    if method == "pppoe":
        return u'ppp0'
    if zone_is_bridge == 'oui':
        if nom_carte is not None:
            if isinstance(nom_carte, list):
                return 'br' + nom_carte[0]
            else:
                return 'br' + nom_carte
        else:
            nom_carte = None
    elif isinstance(nom_carte, list):
        if len(nom_carte) > 1:
            return u'bond{0}'.format(str(interface_no))
        elif len(nom_carte) == 1:
            nom_carte = nom_carte[0]
        else:
            nom_carte = None
    return nom_carte

####################################################
# fonctions diverses accessibles dans les templates
####################################################
def calc_or_auto_network(ip, netmask, nom_carte, parametre, condition):
    """ renvoie le network pour la carte nom_carte
    """
    if parametre.lower() == 'pppoe':
        nom_carte = 'ppp0'
    network = None
    if condition.lower() == parametre.lower() or nom_carte == 'ppp0':
        #calcul à partir de l'IP réel
        eths = get_devices()
        #6106
        ip, netmask, broadcast = eths.get(nom_carte, ('169.254.0.1', '255.255.0.0', '169.254.255.255'))
    if is_empty(ip) or is_empty(netmask):
        return None
    broadcast, network = calc_adresses(ip, netmask)
    return network


@execute_on_zephir_context
def calc_if_in_network(ip_addr1, netmask1, ip_addr2, netmask2, ):
    """ renvoie ip_addr1 si ip_addr1 et ip_addr2 sont dans le même réseau
        et "255.255.255.255" sinon
        ip_addr2 et netmask2 peuvent être une liste
    """
    if ip_addr1 is not None and netmask1 is not None:
        try:
            ip1 = IP("{0}/{1}".format(ip_addr1, netmask1), make_net=True)
        except ValueError:
            raise ValueError(_(u'IP or subnet mask invalid ({0}, {1})').format(ip_addr1, netmask1))
        if isinstance(ip_addr2, list):
            res = []
            for ip_addr, netmask in ip_addr2, netmask2:
                if ip_addr is not None and netmask is not None:
                    try:
                        ip = IP("{0}/{1}".format(ip_addr, netmask), make_net=True)
                    except ValueError:
                        raise ValueError(_(u'IP or subnet mask invalid ({0}, {1})').format(ip_addr, netmask))
                    if ip1 == ip:
                        res.append(str(ip_addr1))
                    else:
                        res.append(u'255.255.255.255')
                else:
                    res.append(None)
        else:
            if ip_addr2 is not None and netmask2 is not None:
                try:
                    ip2 = IP("{0}/{1}".format(ip_addr2, netmask2), make_net=True)
                except ValueError:
                    raise ValueError(_(u'IP or subnet mask invalid ({0}, {1})').format(ip_addr2, netmask2))
                if ip1 == ip2:
                    res = str(ip_addr1)
                else:
                    res = u'255.255.255.255'
            else:
                res = None
    else:
        res = None
    return res


@execute_on_zephir_context
def calc_network(ip, netmask):
    """ calcule le réseau par défaut à partir d'un masque et d'une ip """
    # si les ip et netmask viennent du dictionnaire, on les récupère
    if is_empty(ip) or is_empty(netmask):
        return None
    broadcast, network = calc_adresses(ip, netmask)
    return network


def calc_or_auto_broadcast(ip, netmask, nom_carte, parametre, condition):
    #try:
    #    return auto_broadcast(nom_carte, parametre, condition)
    #except NoValueReturned:
    #    return calc_broadcast(ip, netmask)
    ret = auto_broadcast(nom_carte, parametre, condition)
    if ret == None:
        ret = calc_broadcast(ip, netmask)
    return ret


def auto_dns():
    """ renvoie la route par défaut
    """
    if os.path.isfile('/etc/eole/resolv.conf.dhclient'):
        dhclient = open('/etc/eole/resolv.conf.dhclient')
        ips = set(dhclient.read().strip().split(' '))
        for ip_ in ips.copy():
            if ip_.startswith('192.0.2.'):
                ips.remove(ip_)
        return list(ips)
    elif os.path.isfile('/etc/resolv.conf'):
        dhclient = open('/etc/resolv.conf', 'r')
        ret = []
        for line in dhclient:
            if 'nameserver' in line:
                nameserv = line.split(' ')
                ret.append(nameserv[1].rstrip())
                return ret
    return []


def list_files(rep, wildcards='*', stripext=False, basename=True, default=[], exception=[], substring=True):
    """
    Liste des fichiers dans un répertoire
    @rep: répertoire de recherche
    @wildcards: expression régulière sur les fichiers à rechercher
    @stripext: supprimer l'extension des fichiers
    @basename: ne conserver que le nom court des fichiers
    @default: liste à renvoyer dans le cas où aucun fichier n'est trouvé
    @exception: les fichiers contenant cette chaîne sont ignorés
    @substring: l'exception peut-être une sous-chaîne du nom du fichier
    """
    if stripext == 'True':
        stripext = True
    if substring == 'False':
        substring = False
    if not isinstance(default, list):
        default = default.split(',')
    if not isinstance(default, list):
        exception = exception.split(',')
    res = []
    if os.path.isdir(rep):
        for fic in glob.glob(rep+'/%s' % wildcards):
            if exception:
                found = False
                for exc in exception:
                    if substring:
                        if exc in os.path.basename(fic):
                            found = True
                            break
                    elif exc == os.path.basename(fic):
                        found = True
                        break
                if found:
                    continue
            if basename:
                fic = os.path.basename(fic)
            if stripext:
                fic = os.path.splitext(fic)[0]
            res.append(fic)
    if res == []:
        res = default
    res.sort()
    return res

def is_file(filename):
    """ filtre permettant de verifier l'existence
    d'un fichier avant de l'inclure par exemple """
    return os.path.isfile(filename)


@execute_on_zephir_context
def to_list(lst, sep=' '):
    """retourne une liste concatenee de donnees de la liste"""
    return sep.join(lst)


@execute_on_zephir_context
def purge_list(lst):
    """supprime les doublons dans une liste"""
    res = []
    for elt in lst:
        if elt not in res:
            res.append(elt)
    return res


@execute_on_zephir_context
def to_iso(data):
    """encode une chaine en iso"""
    try:
        return unicode(data, "UTF-8").encode("ISO-8859-1")
    except:
        return data


@execute_on_zephir_context
def to_sql(data):
    """echape les caractères ' et " d'une chaine"""
    try:
        return data.replace("'", "\\\'").replace("\"", "\\\"")
    except:
        return data


def is_installed(package, chroot=None):
    """ vérifie si un paquet est installé ou pas"""
    """FIXME: obsolete, utiliser plutot pyeole.process.is_installed"""
    cmd = ["/usr/bin/dpkg", "-l", package.strip()]
    if chroot != None:
        cmd = ['chroot', chroot] + cmd
    process = subprocess.Popen(args=cmd,
              stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    code = process.wait()
    if code != 0:
        # paquet non installé
        return False
    dpkg_info = process.stdout.read()
    pkg_status = dpkg_info.strip().split('\n')[-1].split()
    if pkg_status[0].lower() in ['pn', 'un']:
        return False
    return True


def installed_version(package):
    """renvoie la version d'un paquet"""
    process = subprocess.Popen(args=["dpkg", "-l", package.strip()],
              stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    code = process.wait()
    if code != 0:
        # paquet non installé
        return ''
    dpkg_info = process.stdout.read()
    pkg_status = dpkg_info.strip().split('\n')[-1].split()
    if pkg_status[0].lower() == 'pn':
        # ancien paquet purgé (status = pn)
        return ''
    return pkg_status[2]


@execute_on_zephir_context
def custom_join(multivar, separator=' '):
    """ mise à plat d'une variable multiple"""
    return separator.join([unicode(i) for i in multivar])


@execute_on_zephir_context
def is_ip(data):
    """vérifie si data décrit une adresse IP"""
    if data == '':
        return False
    try:
        return valid_ip(data)
    except:
        return False


def is_instanciate():
    """
    teste la présence du fichier spécial .instance (#7051)
    """
    return {True: 'oui', False: 'non'}.get(os.path.isfile(INSTANCE_LOCKFILE))


def certs_nuauth():
    """ liste des certificats pour nuauth """
    namecert = ''
    nuauth_tls_key = ''
    rep = '/etc/ipsec.d'
    repdefault = '/etc/nufw/certs/'
    wildcards = '*.pem'
    if os.path.isdir(rep):
        if len(glob.glob(rep+'/%s' % wildcards)) > 0:
            namecert = os.path.basename(glob.glob(rep+'/%s' % wildcards)[0])
            nuauth_tls_key = os.path.join(rep, 'private', 'priv%s' % namecert)
            if not os.path.isfile(nuauth_tls_key):
                nuauth_tls_key = ''
            else:
                nuauth_tls_cert = os.path.join(rep, namecert)
                if not os.path.isfile(nuauth_tls_cert):
                    nuauth_tls_key = ''
                else:
                    nuauth_tls_cacert = os.path.join(rep, 'cacerts', 'CertifCa.pem')
                    if not os.path.isfile(nuauth_tls_cacert):
                        nuauth_tls_key = 'none'
    if nuauth_tls_key == '':
        nuauth_tls_key = os.path.join(repdefault,'nuauth-eole.key')
        nuauth_tls_cert = os.path.join(repdefault,'nuauth-eole.crt')
        nuauth_tls_cacert = '/etc/ssl/certs/ca.crt'
    res = [nuauth_tls_key, nuauth_tls_cert, nuauth_tls_cacert]
    return res


def key_nuauth():
    """retourne la clef privee pour nuauth"""
    return certs_nuauth()[0]


def cert_nuauth():
    """retourne le certificat pour nuauth"""
    return certs_nuauth()[1]


def cacert_nuauth():
    """retourne l'authorité de certification pour nuauth"""
    return certs_nuauth()[2]


@execute_on_zephir_context
def upper(string):
    """ texte en majuscules """
    return string.upper()


@execute_on_zephir_context
def lower(string):
    """ texte en minuscules """
    return string.lower()

#def wpkg_crypt(string):
#    if string is None:
#        return ''
#    return wcrypt(string)
#
#def wpkg_decrypt(string):
#    if string is None:
#        return ''
#    return wdecrypt(string)


def get_file_list(string):
    return glob.glob(string)


@execute_on_zephir_context
def escape_regexp(string):
    return string.replace('.', '\.')

DAY_TO_BAREOS = {1: 'mon', 2: 'tue', 3: 'wed', 4: 'thu', 5: 'fri',
                 6: 'sat', 7: 'sun'}
def gen_bareos_schedule(jobs, schedule):
    ret = []
    def write_job(index, day, monthly=False):
        level = jobs['level'][index]
        hour = jobs['hour'][index]
        day = int(day)
        #weeknumber must be 1st for monthly jobs
        #weeknumber must be 2nd-5th if monthly job set same day
        if monthly:
            weeknumber = '1st '
        else:
            if str(day) in monthjobs:
                weeknumber = '2nd-5th '
            else:
                weeknumber = ''
        #if 12 < day < 24: day = yesterday
        #if 0 < day 12: day = today

        schehour = schedule['hour']
        if int(hour) > 12:
            if day == 1:
                newday = 7
            else:
                newday = day-1
        else:
            newday = day
            if schehour < hour:
                schehour = hour

        #could be day after (if hour in [12-24])
        tday = DAY_TO_BAREOS[newday]
        #always value "day"
        scheday = DAY_TO_BAREOS[day]
        schemin = str(schedule['minute'])
        #time must be 4:01 not 4:1
        if len(schemin) == 1:
            schemin = '0{0}'.format(schemin)
        ret.append((level, weeknumber, tday, hour, scheday, schehour, schemin))

    #get all monthly job
    monthjobs = []
    for index in range(0, len(jobs['job_type'])):
        if jobs['job_type'][index] == 'monthly':
            monthjobs.append(str(jobs['day'][index]))

    for index in range(0, len(jobs['job_type'])):
        if jobs['job_type'][index] == 'daily':
            for i in range(int(jobs['day'][index]),
                    int(jobs['end_day'][index])+1):
                write_job(index, i)
        elif jobs['job_type'][index] == 'weekly':
            write_job(index, jobs['day'][index])
        elif jobs['job_type'][index] == 'monthly':
            write_job(index, jobs['day'][index], True)
        else:
            raise Exception(_(u'Unknown job type'))
    return ret


@execute_on_zephir_context
def random_int(vmin, vmax, exclude=None):
    values = list(range(int(vmin), int(vmax)+1))
    if exclude != None and exclude in values:
        values.remove(exclude)
    return random.choice(values)


def list_cdrom_devices(available_probes='non'):
    # TODO: execute this function on server, not on zephir
    return []


def cdrom_minormajor(typ, device):
    stats = os.stat(device)
    if stat.S_ISBLK(stats.st_mode):
        device = stats.st_rdev
#    elif stat.S_ISCHR(stats.st_mode):
#        device = stats.st_rdev
    elif stat.S_ISDIR(stats.st_mode):
        device = stats.st_dev
    else:
        raise ValueError(_(u"Disknod '{0}' is not a directory or"
                         u" a device.").format(device))
    if typ == 'minor':
        return os.minor(device)
    else:
        return os.major(device)


def get_mount_point_device(mount_point):
    """Lookup device informations for a mount point

    :param mount_point: absolute path of the mount point
    :type mount_point: `str`
    :return: device informations: `name`, `uid`, `gid` and `mode`
    :rtype: `dict`
    :raise: `Exception` if :data:`mount_point` is not a directory or
            not mounted.

    """
    if not os.path.isdir(mount_point):
        msg = u'{0} must be a directory.'
        raise Exception(msg.format(mount_point))

    mounts = open(u'/proc/mounts', 'r')
    for line in mounts:
        items = line.split()
        if items[1] == mount_point:
            device = {'name' : items[0]}
            stats = os.stat(os.path.realpath(device['name']))
            device['uid'] = pwd.getpwuid(stats.st_uid).pw_name
            device['gid'] = grp.getgrgid(stats.st_gid).gr_name
            device['mode'] = oct(stats.st_mode & 0o777)
            return device

    msg = u'No device found for mount point {0}.'
    raise Exception(msg.format(mount_point))


def device_type(device):
    stats = os.stat(device)
    if stat.S_ISBLK(stats.st_mode):
        dev_type = u'b'
    elif stat.S_ISCHR(stats.st_mode):
        dev_type = u'c'
    elif stat.S_ISDIR(stats.st_mode):
        dev_type = u'b'
    return dev_type


@execute_on_zephir_context
def calc_webredirection(roundcube="non", force_envole="non"):
    if force_envole=="oui":
        return "/envole"
    elif roundcube=="oui":
        return "/roundcube"
    else:
        return "/"


@execute_on_zephir_context
def activate_master_only_web_app(mode_conteneur_actif=None, activer_nginx_web=None, activer_apache=None):
    """Check if a master only web application can be enabled

    Check if service can be enabled on master only:
    - When container is enabled, activate if `activer_nginx_web` is `oui`
    - All other cases must activate if either `activer_nginx_web` or `apache` is `oui`.

    :param str mode_conteneur_actif: is container mode enabled (`oui` or `non`)
    :param str activer_nginx_web: is publication by Nginx enabled (`oui` or `non`)
    :param str activer_apache: is apache enabled (`oui` or `non`)
    :return str: `oui` if the service can be activated, `non` otherwise

    """
    without_container = (mode_conteneur_actif == 'non' and (activer_apache == 'oui' or activer_nginx_web == 'oui'))
    with_container = (mode_conteneur_actif == 'oui' and activer_nginx_web == 'oui')
    if without_container or with_container:
        return 'oui'
    else:
        return 'non'


# those functions are tiramisu buildin
@execute_on_zephir_context
def valid_networknetmask():
    pass


@execute_on_zephir_context
def valid_ipnetmask():
    pass


@execute_on_zephir_context
def valid_broadcast():
    pass


@execute_on_zephir_context
def valid_in_network():
    pass
# end tiramisu buildin
