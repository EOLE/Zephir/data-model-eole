#!/bin/sh

#------------------------------------------------------------------------
# forteresse.sh - Put the current server in fortress mode
# Everything is rejected except SSH for authorized networks
#
# Copyright © 2014 Pôle de compétences EOLE <eole@ac-dijon.fr>
#
# License CeCILL:
#  * in french: http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
#  * in english http://www.cecill.info/licences/Licence_CeCILL_V2-en.html

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.2-14) to ensure that this file is present
# and status_of_proc is working.
. /lib/lsb/init-functions

%set %%enabled = {u'oui': 'true', u'non': 'false'}
FIREWALL_ENABLED=%%enabled[%%getVar(u'activer_firewall', u'non')]

# Check if parameter is current interface
# If ${IFACE} is undef then match all interfaces
is_iface() {
    [  -z "${IFACE}" -o "${IFACE}" = "${1}" ]
}

forteresse_start() {
    if [ -n "${IFACE}" -a "${MODE}" != 'start' ]
    then
        # From if-down script
        exit 0
    fi

    if [ -n "${IFACE}" -a  "${FIREWALL_ENABLED}" = 'false' ]
    then
        # From if-up script with firewall disabled
        log_warning_msg "Pare-feu désactivé, mode forteresse inactif."
        exit 0
    fi

    if [ -n "${IFACE}" -a "$(runlevel)" != "unknown" ]
    then
        # From if-up script in non booting mode
        exit 0
    fi

    # Either from ifup with firewall enabled or direct call
    if is_iface "lo"
    then
        log_begin_msg "Réinitialisation du pare-feu"
        ## Reinitialisation des chaines
        /sbin/iptables -F
        /sbin/iptables -t nat -F
        ## on vide les regles utilisateurs
        /sbin/iptables -t nat -X
        /sbin/iptables -X
        ## mise en place de la politique par defaut
        /sbin/iptables -P INPUT DROP
        /sbin/iptables -P OUTPUT ACCEPT
        /sbin/iptables -P FORWARD DROP
        ## lo ok
        /sbin/iptables -A INPUT -i lo -j ACCEPT
        #Supprime les set ipset
        if [ $(command -v ipset) ]
        then
            ipset -n list | while read setname; do
                ipset flush "$setname";
                ipset destroy "$setname";
            done
        fi
    fi

    µµµµµµµµµµ range(3) => 0, 1, 2
    %for %%id_interface in %%range(%%int(%%nombre_interfaces))
        %set %%phy_interface = u'eth{0}'.format(%%id_interface)
        %set %%interface_name = %%getVar(u'nom_zone_{0}'.format(%%phy_interface))
    if is_iface "%%interface_name"
    then
        log_begin_msg "Activation du mode forteresse sur %%interface_name"
    %if %%activer_firewall == u'non'
        /sbin/iptables -A INPUT -i %%interface_name -p tcp --syn -s 0/0 --dport ssh -m state --state NEW -j ACCEPT
    %else
        %if %%getVar(u'ssh_{0}'.format(%%phy_interface), u'non') == u'oui'
            %set %%mask_attribute_name = u'netmask_ssh_{0}'.format(%%phy_interface)
            %for %%res_ssh in %%getVar(u'ip_ssh_{0}'.format(%%phy_interface))
                µµµµµµµµµµ Get netmask attribute
                %set %%res_mask = %%getattr(%%res_ssh,%%mask_attribute_name)
        /sbin/iptables -A INPUT -i %%interface_name -p tcp --syn -s %%res_ssh/%%res_mask --dport ssh -m state --state NEW -j ACCEPT
            %end for
        %end if
    %end if
        /sbin/iptables -A INPUT -i %%interface_name -m state --state ESTABLISHED,RELATED -j ACCEPT
    fi
    %end for

    if is_iface "%%getVar('nom_zone_eth0', 'eth0')"
    then
        # Manage LXC if any
        if [ -x /usr/share/eole/bastion/data/90-lxc_rules ]
        then
            /usr/share/eole/bastion/data/90-lxc_rules
        fi
    fi
}

close_container() {
    local container=$1
    tcpcheck 2 $container:22 &>/dev/null || return 1
    ssh -q -o LogLevel=ERROR -oStrictHostKeyChecking=no root@${container} /usr/share/eole/firewall.stop
}

close_all_containers() {
  %if %%mode_conteneur_actif == 'non'
    # Container mode is disabled => do nothing
    :
  %else
    %for %%group in %%creole_client.get_groups()
      %if %%group in [u'root', u'all']
        %continue
      %end if
    close_container %%getVar('adresse_ip_{0}'.format(%%group)) # %%group
    %end for
  %end if
}
