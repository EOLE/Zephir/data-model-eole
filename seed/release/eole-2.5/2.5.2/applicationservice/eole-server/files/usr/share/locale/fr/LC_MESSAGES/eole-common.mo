��    	      d      �       �   #   �            &  4   E  '   z     �     �  '   �  �  �  -     %   �  )   �  <   �  2   :     m     �  +   �           	                                   ! Abandoning container generation ! Containers generation terminated Installing additional packages LXC cache already exists, do you want to remove it ? System may be in an incoherent state.

 container {0} available container {0} not started container {0} started but not available Project-Id-Version: eole-common
Report-Msgid-Bugs-To: eole@ac-dijon.fr
POT-Creation-Date: 2016-02-04 15:51+0100
PO-Revision-Date: 2014-11-10 14:35+0100
Last-Translator: gnunux <eole@ac-dijon.fr>
Language-Team: Equipe EOLE <eole@ac-dijon.fr>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 ! Abandon de la génération des conteneurs ! Génération des conteneurs terminée Installation des paquets supplémentaires Le cache LXC est déjà présent, voulez-vous le supprimer ? Le système peut être dans un état incohérent

 conteneur {0} accessible conteneur {0} non démarré conteneur {0} démarré mais non accessible 