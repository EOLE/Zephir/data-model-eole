#!/bin/bash

# Création de liens pour les modules eSBL
# dans le cadre d'une installation
# ou d'une migration
# les données ont normalement été copiées dans
# /home
# Pour la bonne marche des services, il faut
# rétablir, si besoin, les liens vers /home

PERSISTENT_PARTITION="/home"

COMPAT_FOLDER="/data"

create_folder() {
    [ ! -d "$1" ] && /bin/mkdir -p "$1"
}

move_data () {
    cp -a $1 $2 && rm -rf $1
}

create_link () {
    [ ! -L "$1" ] && /bin/ln -s "${DEST_FOLDER}$1" "$1"
}

move_to_persistent_partition () {

    if [ "${COMPAT_FOLDER}" = "$1" ]; then
        DEST_FOLDER="${PERSISTENT_PARTITION}"
    else
        DEST_FOLDER="${PERSISTENT_PARTITION}${COMPAT_FOLDER}"
    fi

    location=$(/bin/readlink -e "$1")

    if [[ ! ${location} == ${PERSISTENT_PARTITION}* ]] && [[ -d "$1" ]]
    then

        create_folder $(dirname "${DEST_FOLDER}$1")
        move_data $1 $(dirname "${DEST_FOLDER}$1")
        create_link "$1"
    fi
}

if [ $(/bin/mount | /bin/grep " ${PERSISTENT_PARTITION} "| /usr/bin/wc -l) -eq 1 ]; then

    CreoleService apache2 stop
    CreoleService mysql stop
    CreoleService postgresql stop

    move_to_persistent_partition "${COMPAT_FOLDER}"
    move_to_persistent_partition /bureautique
    move_to_persistent_partition /var/www/html
    move_to_persistent_partition /var/lib/mysql
    move_to_persistent_partition /var/lib/postgresql
    move_to_persistent_partition /var/lib/mlocate
    move_to_persistent_partition /opt/arkeia

    CreoleService apache2 start
    CreoleService mysql start
    CreoleService postgresql start
fi

