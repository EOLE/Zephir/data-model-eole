<?php
/* $Id: default_config.php,v 1.68.2.12 2011/08/09 03:27:56 cknudsen Exp $
 *
 * The file contains a listing of all the current WebCalendar config settings
 * and their default values.
 */
$webcalConfig = array (
  'ADD_LINK_IN_VIEWS' => 'N',
  'ADMIN_OVERRIDE_UAC' => 'Y',
  'ALLOW_ATTACH' => 'N',
  'ALLOW_ATTACH_ANY' => 'N',
  'ALLOW_ATTACH_PART' => 'N',
  'ALLOW_COLOR_CUSTOMIZATION' => 'Y',
  'ALLOW_COMMENTS' => 'N',
  'ALLOW_COMMENTS_ANY' => 'N',
  'ALLOW_COMMENTS_PART' => 'N',
  'ALLOW_CONFLICT_OVERRIDE' => 'Y',
  'ALLOW_CONFLICTS' => 'N',
  'ALLOW_EXTERNAL_HEADER' => 'N',
  'ALLOW_EXTERNAL_USERS' => 'N',
  'ALLOW_HTML_DESCRIPTION' => 'Y',
  'ALLOW_SELF_REGISTRATION' => 'N',
  'ALLOW_USER_HEADER' => 'N',
  'ALLOW_USER_THEMES' => 'Y',
  'ALLOW_VIEW_OTHER' => 'Y',
  'APPLICATION_NAME' => 'WebCalendar', //envole : au lieu de "Titre"
  'APPROVE_ASSISTANT_EVENT' => 'Y',
  'AUTO_REFRESH' => 'N',
  'AUTO_REFRESH_TIME' => '0',
  'BGCOLOR' => '#FFFFDD', //envole : au lieu de "#FFFFFF"
  'BGIMAGE' => '',
  'BGREPEAT' => 'repeat fixed center',
  'BOLD_DAYS_IN_YEAR' => 'Y',
  'CAPTIONS' => '#B04040',
  'CATEGORIES_ENABLED' => 'Y',
  'CELLBG' => '#FFFFFF', //envole : au lieu de "#C0C0C0"
  'CONFLICT_REPEAT_MONTHS' => '6',
  'CUSTOM_HEADER' => 'N',
  'CUSTOM_SCRIPT' => 'N',
  'CUSTOM_TRAILER' => 'N',
  'DATE_FORMAT' => '__dd__ __month__ __yyyy__', //envole : au lieu de 'LANGUAGE_DEFINED'
  'DATE_FORMAT_MD' => '__dd__ __month__', //envole : au lieu de 'LANGUAGE_DEFINED'
  'DATE_FORMAT_MY' => '__month__ __yyyy__', //envole : au lieu de 'LANGUAGE_DEFINED'
  'DATE_FORMAT_TASK' => '__dd__/__mm__/__yy__', //envole : au lieu de 'LANGUAGE_DEFINED'
  'DEMO_MODE' => 'N',
  'DISABLE_ACCESS_FIELD' => 'N',
  'DISABLE_CROSSDAY_EVENTS' => 'N',
  'DISABLE_LOCATION_FIELD' => 'N',
  'DISABLE_PARTICIPANTS_FIELD' => 'N',
  'DISABLE_POPUPS' => 'N',
  'DISABLE_PRIORITY_FIELD' => 'N',
  'DISABLE_REMINDER_FIELD' => 'N',
  'DISABLE_REPEATING_FIELD' => 'N',
  'DISABLE_URL_FIELD' => 'Y',
  'DISPLAY_ALL_DAYS_IN_MONTH' => 'N',
  'DISPLAY_CREATED_BYPROXY' => 'Y',
  'DISPLAY_DESC_PRINT_DAY' => 'Y',
  'DISPLAY_END_TIMES' => 'N',
  'DISPLAY_LOCATION' => 'N',
  'DISPLAY_LONG_DAYS' => 'N',
  'DISPLAY_MINUTES' => 'N',
  'DISPLAY_MOON_PHASES' => 'N',
  'DISPLAY_SM_MONTH' => 'Y',
  'DISPLAY_TASKS' => 'N',
  'DISPLAY_TASKS_IN_GRID' => 'N',
  'DISPLAY_UNAPPROVED' => 'Y',
  'DISPLAY_WEEKENDS' => 'N', //envole : au lieu de 'Y'
  'DISPLAY_WEEKNUMBER' => 'Y',
  'EMAIL_ASSISTANT_EVENTS' => 'Y',
  'EMAIL_EVENT_ADDED' => 'Y',
  'EMAIL_EVENT_CREATE' => 'N',
  'EMAIL_EVENT_DELETED' => 'Y',
  'EMAIL_EVENT_REJECTED' => 'Y',
  'EMAIL_EVENT_UPDATED' => 'Y',
  'EMAIL_FALLBACK_FROM' => 'youremailhere',
  'EMAIL_HTML' => 'N',
  'EMAIL_MAILER' => 'mail',
  'EMAIL_REMINDER' => 'Y',
  'ENABLE_CAPTCHA' => 'N',
  'ENABLE_GRADIENTS' => 'N',
  'ENABLE_ICON_UPLOADS' => 'N',
  'ENTRY_SLOTS' => '144',
  'EXTERNAL_NOTIFICATIONS' => 'N',
  'EXTERNAL_REMINDERS' => 'N',
  'FONTS' => 'Arial, Helvetica, sans-serif',
  'FREEBUSY_ENABLED' => 'N',
  'GENERAL_USE_GMT' => 'Y',
  'GROUPS_ENABLED' => 'N',
  'H2COLOR' => '#0F5080', //envole : au lieu de "#000000"
  'HASEVENTSBG' => '#BBCEDE', //envole : au lieu de "#FFFF33"
  'IMPORT_CATEGORIES' => 'Y',
  'LANGUAGE' => 'French', //envole : au lieu de 'none'
  'LIMIT_APPTS' => 'N',
  'LIMIT_APPTS_NUMBER' => '6',
  'LIMIT_DESCRIPTION_SIZE' => 'N',
  'MENU_DATE_TOP' => 'Y',
  'MENU_ENABLED' => 'Y',
  'MENU_THEME' => 'default',
  'MYEVENTS' => '#006000',
  'NONUSER_AT_TOP' => 'Y',
  'NONUSER_ENABLED' => 'N', //envole : au lieu de 'Y'
  'OTHERMONTHBG' => '#D0D0D0',
  'OVERRIDE_PUBLIC' => 'N',
  'OVERRIDE_PUBLIC_TEXT' => 'Not available',
  'PARTICIPANTS_IN_POPUP' => 'N',
  'PLUGINS_ENABLED' => 'N',
  'POPUP_BG' => '#FFFFFF',
  'POPUP_FG' => '#000000',
  'PUBLIC_ACCESS' => 'N',
  'PUBLIC_ACCESS_ADD_NEEDS_APPROVAL' => 'N',
  'PUBLIC_ACCESS_CAN_ADD' => 'N',
  'PUBLIC_ACCESS_DEFAULT_SELECTED' => 'N',
  'PUBLIC_ACCESS_DEFAULT_VISIBLE' => 'N',
  'PUBLIC_ACCESS_OTHERS' => 'Y',
  'PUBLIC_ACCESS_VIEW_PART' => 'N',
  'PUBLISH_ENABLED' => 'Y',
  'PULLDOWN_WEEKNUMBER' => 'N',
  'REMEMBER_LAST_LOGIN' => 'N',
  'REMINDER_DEFAULT' => 'N',
  'REMINDER_OFFSET' => '240',
  'REMINDER_WITH_DATE' => 'N',
  'REMOTES_ENABLED' => 'N',
  'REPORTS_ENABLED' => 'N',
  'REQUIRE_APPROVALS' => 'Y',
  'RSS_ENABLED' => 'N',
  'SELF_REGISTRATION_BLACKLIST' => 'N',
  'SELF_REGISTRATION_FULL' => 'Y',
  'SEND_EMAIL' => 'N',
  'SERVER_TIMEZONE' => 'Europe/Paris', //envole : au lieu de 'America/New_York'
  'SITE_EXTRAS_IN_POPUP' => 'N',
  'SMTP_AUTH' => 'N',
%if %%is_defined('adresse_ip_mail')
  'SMTP_HOST' => '%%adresse_ip_mail',
%else
  'SMTP_HOST' => '%%passerelle_smtp',
%end if
  'SMTP_PASSWORD' => '',
  'SMTP_PORT' => '25',
  'SMTP_USERNAME' => '',
  'STARTVIEW' => 'week.php', //envole : au lieu de "month.php"
  'SUMMARY_LENGTH' => '80',
  'TABLEBG' => '#0F5080', //envole : au lieu de "#000000"
  'TEXTCOLOR' => '#0F5080', //envole : au lieu de "#000000"
  'THBG' => '#FFFFFF',
  'THFG' => '#0F5080', //envole : au lieu de "#000000"
  'TIME_FORMAT' => '24', //envole : au lieu de '12'
  'TIME_SLOTS' => '24',
  'TIME_SPACER' => '&raquo;&nbsp;',
  'TIMED_EVT_LEN' => 'D',
  'TIMEZONE' => 'Europe/Paris', //envole : au lieu de 'America/New_York'
  'TODAYCELLBG' => '#BBCEDE', //envole : au lieu de "#FFFF33"
  'UAC_ENABLED' => 'N',
  'USER_PUBLISH_ENABLED' => 'Y',
  'USER_PUBLISH_RW_ENABLED' => 'Y',
  'USER_RSS_ENABLED' => 'N',
  'USER_SEES_ONLY_HIS_GROUPS' => 'N', //envole : au lieu de 'Y'
  'USER_SORT_ORDER' => 'cal_lastname, cal_firstname',
  'WEBCAL_PROGRAM_VERSION' => 'v1.2.4',
  'WEEK_START' => '1', //envole : pour lundi au lieu du dimanche ('0') car sinon lundi non visible + décalage si non affichage des week-end
  'WEEKEND_START' => '6',
  'WEEKENDBG' => '#D0D0D0',
  'WEEKNUMBER' => '#FF6633',
  'WORK_DAY_END_HOUR' => '17',
  'WORK_DAY_START_HOUR' => '8',
  'SERVER_URL' => '/webcalendar/', //ajout envole (demandé à la main lors d'une installation classique)
  'ALLOW_VIEW_TEACHERS'=>'N', //ajout envole
  'C_COLOR'=>'#990099', //ajout envole
  'D_COLOR'=>'#FF3333', //ajout envole
  'DISPLAY_CDT'=>'Y' //ajout envole
  );

/* This function is defined here because admin.php calls it during startup.
 */
function db_load_config () {
  global $webcalConfig;

  while ( list ( $key, $val ) = each ( $webcalConfig ) ) {
    $res = dbi_execute ( 'SELECT cal_value FROM webcal_config
      WHERE cal_setting = ?', array ( $key ), false, false );
    $sql = 'INSERT INTO webcal_config ( cal_setting, cal_value ) VALUES (?,?)';
    if ( ! $res )
      dbi_execute ( $sql, array ( $key, $val ) );
    else { // SQLite returns $res always.
      $row = dbi_fetch_row ( $res );
      if ( ! isset ( $row[0] ) )
        dbi_execute ( $sql, array ( $key, $val ) );

      dbi_free_result ( $res );
    }
  }
}

?>
