<?php
//envole : adaptation du fichier user-ldap.php

//envole : importation de la librairie CAS et des paramètres du serveur CAS
include_once "CAS-1.3.1/eoleCAS.php";
include_once("configCAS/cas.inc.php"); //variables globales
$envole_cas = null;
if(__CAS_DEBUG) EolephpCAS::setDebug('/var/log/posh/webcalendar-cas.log');

//vérification de constantes spécifiques au scribe 2.3 pour une version unique du fichier sur 2.2 et 2.3
if(!defined("__CAS_VALIDER_CA")) define("__CAS_VALIDER_CA",false);
if(!defined("__CAS_CA_LOCATION")) define("__CAS_CA_LOCATION","/etc/ssl/certs/ca.crt");

defined ( '_ISVALID' ) or die ( 'You cannot access this file directly!' );
/*
 * $Id: user-ldap.php,v 1.42.2.1 2007/08/17 14:39:00 umcesrjones Exp $
 * LDAP user functions.
 * This file is intended to be used instead of the standard user.php file.
 * I have not tested this yet (I do not have an LDAP server running yet),
 * so please provide feedback.
 *
 * This file contains all the functions for getting information about users.
 * So, if you want to use an authentication scheme other than the webcal_user
 * table, you can just create a new version of each function found below.
 *
 * Note: this application assumes that usernames (logins) are unique.
 *
 * Note #2: If you are using HTTP-based authentication, then you still need
 * these functions and you will still need to add users to webcal_user.
 */

/***************************** Config *******************************/
// Set some global config variables about your system.
// Next three are NOT yet implemented for LDAP
$user_can_update_password = false;
$admin_can_add_user = false;

// Allow admin to delete user from webcal tables
$admin_can_delete_user = true;
$admin_can_disable_user = false;

//------ LDAP General Server Settings ------//
//
// Name or address of the LDAP server
//  For SSL/TLS use 'ldaps://localhost'
$ldap_server = '%%adresse_ip_ldap';

// Port LDAP listens on (default 389)
%if %%is_defined('eolesso_port_ldap')
$ldap_port = '%%eolesso_port_ldap';
%else
$ldap_port = '%%eolesso_ldap.eolesso_port_ldap[0]';
%end if

// Use TLS for the connection (not the same as ldaps://)
$ldap_start_tls = false;

// If you need to set LDAP_OPT_PROTOCOL_VERSION
$set_ldap_version = false;
$ldap_version = '3'; // (usually 3)

// base DN to search for users
$ldap_base_dn = '%%ldap_base_dn';

// The ldap attribute used to find a user (login).
// E.g., if you use cn,  your login might be "Jane Smith"
//       if you use uid, your login might be "jsmith"
$ldap_login_attr = 'uid';

// Account used to bind to the server and search for information.
// This user must have the correct rights to perform search.
// If left empty the search will be made in anonymous.
//
// *** We do NOT recommend storing the root LDAP account info here ***
$ldap_admin_dn = '';  // user DN
$ldap_admin_pwd = ''; // user password

//------ Admin Group Settings ------//
//
// A group name (complete DN) to find users with admin rights
$ldap_admin_group_name = 'cn=DomainAdmins,o=gouv,c=fr';

// What type of group do we want (posixgroup, groupofnames, groupofuniquenames)
$ldap_admin_group_type = 'posixgroup';

// The LDAP attribute used to store member of a group
$ldap_admin_group_attr = 'memberuid';

//------ LDAP Filter Settings ------//
//
// LDAP filter used to limit search results and login authentication
$ldap_user_filter = '(objectclass=ENTPerson)';
//ce filtre plus précis mais ne renvoit pas les infos attendues, pourquoi ???
//$ldap_user_filter = '(objectclass=sambaSamAccount)(objectclass=inetOrgPerson)(!(description=Computer))';

// Attributes to fetch from LDAP and corresponding user variables in the
// application. Do change according to your LDAP Schema
$ldap_user_attr = array(
  // LDAP attribute   //WebCalendar variable
  'divcod',           //pour pouvoir ranger les élèves par classe
  'entpersonprofils'  //pour trier dans user_get_users() en fonction du profil
);

$ldap_civilite = array(0=>"M./Mme",1=>"M.",2=>"Mme",3=>"Mme","."=>"M.","M"=>"Mme","L"=>"Mme");

//utilisateurs à ne pas afficher
$ldap_uid_exclusions = array(
	'meta_prof',
	// 'meta_parent',
	'meta_eleve',
	'meta_perdir',
	'visiteur',
	'admin'
);

//profils autorisés à se connecter
$ldap_profils = array(
	'National_1',    //élèves (doit obligatoirement être en position 0 pour user_stop_access)
	'National_2',    //parents (doit obligatoirement rester en position 1)
	'National_3',    //enseignants
	'National_6',    //administratifs
	'administrateur', //admin
    'autre' //Lucas Francavilla *PIA* Dijon
);

/*************************** End Config *****************************/

// Convert group name to lower case to prevent problems
$ldap_admin_group_attr = strtolower($ldap_admin_group_attr);
$ldap_admin_group_type = strtolower($ldap_admin_group_type);

//envole : pour vérifier l'existence d'une variable extraite du fichier "includes/settings.php"
function requiredSetting($setting,$type="str")
	{
	global $settings; //tableau généré par le fichier "includes/config.php"
	if(!array_key_exists($setting,$settings)) die("La variable \"<i>".$setting."</i>\" est manquante. Le processus d&rsquo;authentification est impossible.");
	if($type=="int") $value = intval($settings[$setting]);
	else $value = $settings[$setting];
	if(empty($value)) die("La variable \"<i>".$setting."</i>\" est invalide. Le processus d&rsquo;authentification est impossible.");
	return $value;
	}

//envole : pour générer un mot de passe
function genere_pwd()
	{
	$chaine = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$pass = "";
	$chaine_length = strlen($chaine);
	$pass_length = 10;
	for($i=1;$i<=$pass_length;$i++)
		{
		$char = mt_rand(0,$chaine_length-1);
		$pass.=$chaine[$char];
		}
	return $pass;
	}

//envole : instanciation du client CAS
function user_cas_instance()
	{
	global $envole_cas;
	if(empty($envole_cas))
		{
		$envole_cas = new eolephpCAS();
		$envole_cas->client(__CAS_VERSION,__CAS_SERVER,__CAS_PORT,__CAS_FOLDER,false); //session démarrée par login.php
		if(__CAS_VALIDER_CA) $envole_cas->setCasServerCACert(__CAS_CA_LOCATION);
		elseif(method_exists($envole_cas,'setNoCasServerValidation')) $envole_cas->setNoCasServerValidation();
		if(method_exists($envole_cas,'EoleLogoutRequests')) $envole_cas->EoleLogoutRequests(false);
		$envole_cas->setNoClearTicketsFromUrl();
		}
	}

//envole : vérification de l'authentification par WebCalendar.class pour permettre la déconnexion globale (Single Sign Out)
function user_cas_logged_in($encoded_login)
	{
	global $login,$envole_cas,$ldap_profils;

	if(empty($encoded_login)) return false;

	//l'authentification classique se base sur le cookie "webcalendar_session"
	//si le cookie est présent, on applique ce que fait WebCalendar.class lignes 517/518 pour décoder le login
	$login_pw = explode('|',decode_string($encoded_login));
	$login = $login_pw[0];

	@session_start(); //nécessaire car non réalisé encore au moment de l'utilisation de cette procédure

	if(empty($envole_cas)) user_cas_instance();

	if($envole_cas->isAuthenticated())
		{
		$attributs = $envole_cas->getDetails();
		$profil = $attributs['utilisateur']['profil'][0];
		$estUnUtilisateur = $login==$envole_cas->getUser();
		$estUnParent = ($profil==$ldap_profils[1] && $login=="Meta_parent");
		if($estUnUtilisateur || $estUnParent) return true;
		}

	//échec de l'authentification
	user_delete_session();
	return false;
	}

//envole : suppression des cookies d'authentification et de la session
function user_delete_session()
	{
	global $cookie_path;

	SetCookie ("webcalendar_session","",0,$cookie_path);
	SetCookie ("webcalendar_login","",0,$cookie_path);
	// In older versions the cookie path had no trailing slash and NS 4.78
	// thinks "path/" and "path" are different, so the line above does not
	// delete the "old" cookie. This prohibits the login. So we delete the
	// cookie with the trailing slash removed
	if(substr($cookie_path,-1)=='/')
		{
		SetCookie("webcalendar_session","",0,substr($cookie_path,0,-1));
		SetCookie("webcalendar_login","",0,substr($cookie_path,0,-1));
		}
	session_destroy();
	unset($_SESSION);
	}

//envole : authentification et récupération des informations sur l'utilisateur
//attention au client CAS : isAuthenticated() provoque une redirection si il est utilisé lors de la requête d'authentification dû à la présence du ticket
//comme user_valid_login() vérifie l'authentification CAS, il faut prévenir cette procédure que l'authentification a réussi
$force_authentication = false;
function user_cas_auth($create=true)
	{
	global $envole_cas,$cookie_path,$logout,$ldap_profils,$error,$force_authentication;

	if(empty($envole_cas)) user_cas_instance();

	if(isset($_REQUEST['envole_logout'])) //retour après déconnexion du serveur cas pour permettre l'affichage de la page de déconnexion
		{
		$logout = true;
		return "";
		}
	elseif(isset($_REQUEST['action']) && $_REQUEST['action']=="logout") //procédure pour déconnexion du serveur CAS
		{
		user_delete_session();

	  	//procédure de déconnexion du serveur CAS
	  	$domaine = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? "https://" : "http://";
	  	if(empty($_SERVER['SERVER_NAME'])) $domaine .= $_SERVER['HTTP_HOST'];
	  	else $domaine .= $_SERVER['SERVER_NAME'];
	  	$url = $domaine."/calendar/login.php?envole_logout";
	  	$service = $domaine."/calendar/login.php";
	  	$envole_cas->logout(array("service"=>$service));
		}
	else
		{
		//forcer à redemander les infos auprès du serveur CAS (au lieu de les lire en session alors que ce serait les infos d'une autre appli...)
		if(isset($_SESSION["phpCAS"])) unset($_SESSION["phpCAS"]);

		$envole_cas->forceAuthentication();
		$attributs = array();
		$attributs = $envole_cas->getDetails();
		$force_authentication = true;

		//test sur un attribut particulier pour vérifier la bonne application du filtre par le serveur CAS
		if(!isset($attributs['utilisateur']['profil'])) include("../envole/echecCAS.php");

		//test du profil
		$profil = $attributs['utilisateur']['profil'][0];
		if(!in_array($profil,$ldap_profils))
			{
			$error = "Le profil \"".$profil."\" n&rsquo;est pas autoris&eacute; &agrave; se connecter sur l&rsquo;agenda.";
			return false;
			}

		//l'utilisateur est authentifié, on procède à la création de son compte (sauf pour les parents)
		if($create && $profil!=$ldap_profils[1]) user_cas_add($attributs);

		//on met en session les données non prévues par la base
		$_SESSION["webcalendar_profil"] = $profil;
		$_SESSION["webcalendar_classe"] = $attributs['utilisateur']['classe'][0];

		if($profil!=$ldap_profils[1]) return $attributs['utilisateur']['user'][0];
		else return "Meta_parent";
		}
	}

//envole : création d'un compte s'il n'existe pas
function user_cas_add($attributs)
	{
	global $error,$ldap_civilite;

	//compte déjà présent ?
	$login = $attributs['utilisateur']['user'][0];
	$query_verif_user = "SELECT COUNT(*) FROM `webcal_user` WHERE `cal_login`='".$login."';";
	$result_verif_user = dbi_query($query_verif_user);
	$row_verif_user = dbi_fetch_row ($result_verif_user);
	if ($row_verif_user[0]>0) return true;

	//création du compte
	$password = md5(genere_pwd()); //mot de passe aléatoire pour ne pas laisser le champ vide dans la table
	$profil = $attributs['utilisateur']['profil'][0];
	$classe = $attributs['utilisateur']['classe'][0];
	$civilite = $attributs['utilisateur']['civil'][0];
	if($profil=="National_1")
		{
		$firstname = $attributs['utilisateur']['firstname'][0];
		$lastname = strtoupper($attributs['utilisateur']['lastname'][0]);
		}
	elseif(preg_match("/^meta_/i",$login)) //pour l'agenda d'une classe
		{
		$firstname = "";
		$lastname = strtoupper($attributs['utilisateur']['lastname'][0]);
		}
	else //adultes : civilité pas comme prénom si changement d'affichage par l'admin sous la form "nom prénom"...
		{
		$firstname = "";
		$lastname = $ldap_civilite[$civilite]." ".strtoupper($attributs['utilisateur']['lastname'][0]);
		}
	$email = $attributs['utilisateur']['email'][0];
	$cal_is_admin = ($profil=="administrateur") ? 'Y' : 'N';

	$query_insert_entry_user = "INSERT INTO `webcal_user` (cal_login,cal_passwd,cal_lastname,cal_firstname,cal_is_admin,cal_email) VALUES (?,?,?,?,?,?)";
	$result_insert_entry_user = dbi_execute($query_insert_entry_user,array($login,$password,$lastname,$firstname,$cal_is_admin,$email));
	if (!$result_insert_entry_user)
		{
		$error = db_error();
		return false;
		}

	return true;
	}

/**
 * Function to search the dn of a given user
 * the error message will be placed in $error.
 *
 * @param string $login User login
 * @return string $ret  complete dn for the user or FALSE if the user is not found
 */
function user_search_dn($login)
	{
	global $error, $ds, $ldap_base_dn, $ldap_login_attr, $ldap_user_attr, $ldap_user_filter;

	$ret = false;
	if($r=connect_and_bind())
		{
		$sr = @ldap_search ($ds,$ldap_base_dn,"(&($ldap_login_attr=$login)$ldap_user_filter )",$ldap_user_attr);
		if(!$sr)
			{
			$error = 'Error searching LDAP server: '.ldap_error($ds);
			}
		else
			{
			$info = @ldap_get_entries($ds,$sr);
			if($info['count']!=1)
				{
				$error = 'Invalid login';
				}
			else
				{
				$dn = $info[0]['dn'];
				$ret = $dn;
				}
			@ldap_free_result($sr);
			}
		@ldap_close($ds);
		}
	return $ret;
	}

 //envole : adaptation de user_search_dn() pour identifier prof/élève (utilisé dans la fonction user_stop_access() qui limite l'accès des élèves aux autres agendas)
 //renvoit false en cas de problème, la classe si élève et "autre" dans les autre cas
function search_classe($login)
	{
	global $error, $ds, $ldap_base_dn, $ldap_login_attr, $ldap_user_attr, $ldap_user_filter;

	$ret = false;
	if(preg_match("/^meta_classe_(.+)$/i",$login,$matches)) $ret = $matches[1];
	elseif($r=connect_and_bind())
		{
		$sr = @ldap_search ($ds,$ldap_base_dn,"(&($ldap_login_attr=$login)$ldap_user_filter )",$ldap_user_attr);
		if(!$sr)
			{
			$error = 'Error searching LDAP server: '.ldap_error($ds);
			}
		else
			{
			$info = @ldap_get_entries($ds,$sr);
			if($info['count']!=1)
				{
				$error = 'Invalid login';
				}
			else
				{
				$dn = strtolower($info[0]['dn']);
				if(preg_match("/ou=eleves/i",$dn)) $ret = $info[0][$ldap_user_attr[0]][0];
				else $ret = "autre";
				}
			@ldap_free_result($sr);
			}
		@ldap_close($ds);
		}
	return $ret;
	}

/**
 * Check to see if a given login/password is valid.
 *
 * If invalid, the error message will be placed in $error.
 *
 * @param string $login    User login
 * @param string $password User password
 *
 * @return bool True on success
 *
 * @global string Error message
 */
//envole : l'authentification CAS a déjà été effectuée par "user_cas_auth()", on la vérifie et on teste l'existence du login (password inutile)
function user_valid_login($login,$password="")
	{
  	global $error,$envole_cas,$force_authentication;
  	$ret = false;
  	if(empty($envole_cas)) user_cas_instance();

	if($force_authentication || $envole_cas->isAuthenticated())
		{
		$res = dbi_execute('SELECT cal_login FROM webcal_user WHERE cal_login = ?',array($login));
		if($res)
			{
			$row = dbi_fetch_row ($res);
			// MySQL seems to do case insensitive matching, so double-check the login.
			if($row && $row[0]!='' && $row[0]==$login) $ret = true;
			else $error = translate('Invalid login',true).':'.translate('no such user',true);
			}
		else $error = translate('Invalid login',true).':'.translate('no such user',true);
		}
	else $error = 'Vous n&rsquo;&ecirc;tes pas authentifi&eacute;. Impossible de continuer.';

	return $ret;
	}

/**
 * Check to see if a given login/crypted password is valid.
 *
 * If invalid, the error message will be placed in $error.
 *
 * @param string $login          User login
 * @param string $crypt_password Encrypted user password
 *
 * @return bool True on success
 *
 * @global string Error message
 */
//envole : utilité du mot de passe présent dans la base -> s'assurer que celui mis en cookie à la connexion correspond à celui présent en base
function user_valid_crypt ( $login, $crypt_password )
	{
	global $error;
	$ret = false;

	$sql = 'SELECT cal_login, cal_passwd FROM webcal_user WHERE cal_login = ?';
	$res = dbi_execute($sql,array($login));
	if($res)
		{
		$row = dbi_fetch_row($res);
		if($row && $row[0]!='')
			{
			// MySQL seems to do case insensitive matching, so double-check
			// the login.
			// also check if password matches
			if ($row[0]==$login && crypt($row[1], $crypt_password)==$crypt_password) $ret = true; // found login/password
			else $error = 'Invalid login';
			}
		else $error = 'Invalid login';
		dbi_free_result($res);
		}
	else $error = 'Database error: '.dbi_error();
	return $ret;
	}

/**
 * Load info about a user (first name, last name, admin) and set globally.
 *
 * @param string $user User login
 * @param string $prefix Variable prefix to use
 *
 * @return bool True on success
 */
//envole : classe et profil mis en session pour l'utilisateur connecté
function user_load_variables ( $login, $prefix )
	{
	global $PUBLIC_ACCESS_FULLNAME,$NONUSER_PREFIX,$cached_user_var,$DISPLAY_CDT;
	$ret = false;

	if(!empty($cached_user_var[$login][$prefix])) return $cached_user_var[$login][$prefix];
	$cached_user_var = array ();

	//help prevent spoofed username attempts from disclosing fullpath
	$GLOBALS[$prefix.'fullname'] = '';

	if($NONUSER_PREFIX && substr($login,0,strlen($NONUSER_PREFIX))==$NONUSER_PREFIX)
		{
		nonuser_load_variables($login,$prefix);
		return true;
		}

	if($login=='__public__' || $login=='__default__')
		{
		$GLOBALS[$prefix.'login'] = $login;
		$GLOBALS[$prefix.'firstname'] = '';
		$GLOBALS[$prefix.'lastname'] = '';
		$GLOBALS[$prefix.'is_admin'] = 'N';
		$GLOBALS[$prefix.'email'] = '';
		$GLOBALS[$prefix.'fullname'] = ($login=='__public__' ? $PUBLIC_ACCESS_FULLNAME : translate('DEFAULT CONFIGURATION'));
		$GLOBALS[$prefix.'password'] = '';
		return true;
		}

	$sql = 'SELECT cal_firstname,cal_lastname,cal_is_admin,cal_email,cal_passwd,cal_enabled FROM webcal_user WHERE cal_login = ?';
	$rows = dbi_get_cached_rows($sql,array($login));
	if($rows)
		{
		$row = $rows[0];
		$GLOBALS[$prefix.'login'] = $login;
		$GLOBALS[$prefix.'firstname'] = $row[0];
		$GLOBALS[$prefix.'lastname'] = $row[1];
		$GLOBALS[$prefix.'is_admin'] = $row[2];
		$GLOBALS[$prefix.'email'] = empty($row[3]) ? '' : $row[3];
		if(strlen($row[0]) && strlen($row[1])) $GLOBALS[$prefix.'fullname'] = $row[0]." ".$row[1];
		elseif(strlen($row[1])) $GLOBALS[$prefix.'fullname'] = $row[1]; //pas de cal_firstname pour un enseignant et cal_lastname en civilité + nom par défaut
		else $GLOBALS[$prefix.'fullname'] = $login;
		$GLOBALS[$prefix.'password'] = $row[4];
		$GLOBALS[$prefix.'enabled'] = $row[5];
		$ret = true;
		}
	else
		{
		if($DISPLAY_CDT="Y" && preg_match("/^meta_classe_(.+)$/i",$login,$matches)) $GLOBALS[$prefix.'fullname'] = $matches[1]; //ajout pour affichage si accès à des classes sans compte existant
		return false;
		}

	//save these results
	$cached_user_var[$login][$prefix] = $ret;

	return $ret;
	}

/**
 * Add a new user.
 *
 * @param string $user      User login
 * @param string $password  User password
 * @param string $firstname User first name
 * @param string $lastname  User last name
 * @param string $email     User email address
 * @param string $admin     Is the user an administrator? ('Y' or 'N')
 *
 * @return bool True on success
 *
 * @global string Error message
 */
function user_add_user ( $user, $password, $firstname,
  $lastname, $email, $admin, $enabled='Y' ) {
  global $error;

  $error = 'Not yet supported.';
  return false;
}

/**
 * Update a user.
 *
 * @param string $user      User login
 * @param string $firstname User first name
 * @param string $lastname  User last name
 * @param string $mail      User email address
 * @param string $admin     Is the user an administrator? ('Y' or 'N')
 * @param string $enabled   Is the user account enabled? ('Y' or 'N')
 *
 * @return bool True on success
 *
 * @global string Error message
 */
//envole : fonction d'origine
function user_update_user ( $user, $firstname, $lastname, $email,
  $admin, $enabled='Y' ) {
  global $error;

  if ( $user == '__public__' ) {
    $error = translate ( 'Invalid user login' );
    return false;
  }
  if ( strlen ( $email ) )
    $uemail = $email;
  else
    $uemail = NULL;
  if ( strlen ( $firstname ) )
    $ufirstname = $firstname;
  else
    $ufirstname = NULL;
  if ( strlen ( $lastname ) )
    $ulastname = $lastname;
  else
    $ulastname = NULL;
  if ( $admin != 'Y' )
    $admin = 'N';
  if ( $enabled != 'Y' )
    $enabled = 'N';

  $sql = 'UPDATE webcal_user SET cal_lastname = ?, ' .
    'cal_firstname = ?, cal_email = ?,' .
    'cal_is_admin = ?, cal_enabled = ? WHERE cal_login = ?';
  if ( ! dbi_execute ( $sql,
    array ( $ulastname, $ufirstname, $uemail, $admin, $enabled, $user  ) ) ) {
    $error = db_error ();
    return false;
  }
  return true;
}

/**
 * Update user password.
 *
 * @param string $user     User login
 * @param string $password User password
 *
 * @return bool True on success
 *
 * @global string Error message
 */
function user_update_user_password ( $user, $password ) {
  global $error;

  $error = 'Not yet supported';
  return false;
}

/**
 * Delete a user from the webcalendar tables. (NOT from LDAP)
 *
 * This will also delete any of the user's events in the system that have
 * no other participants. Any layers that point to this user
 * will be deleted. Any views that include this user will be updated.
 *
 * @param string $user User to delete
 */
//envole : fonction d'origine
function user_delete_user ( $user ) {
  // Get event ids for all events this user is a participant
  $events = get_users_event_ids ( $user );

  // Now count number of participants in each event...
  // If just 1, then save id to be deleted
  $delete_em = array ();
  for ( $i = 0; $i < count ( $events ); $i++ ) {
    $res = dbi_execute ( 'SELECT COUNT(*) FROM webcal_entry_user ' .
      'WHERE cal_id = ?', array ( $events[$i] ) );
    if ( $res ) {
      if ( $row = dbi_fetch_row ( $res ) ) {
        if ( $row[0] == 1 )
   $delete_em[] = $events[$i];
      }
      dbi_free_result ( $res );
    }
  }
  // Now delete events that were just for this user
  for ( $i = 0; $i < count ( $delete_em ); $i++ ) {
    dbi_execute ( 'DELETE FROM webcal_entry_repeats WHERE cal_id = ?',
      array ( $delete_em[$i] ) );
    dbi_execute ( 'DELETE FROM webcal_entry_repeats_not WHERE cal_id = ?',
      array ( $delete_em[$i] ) );
    dbi_execute ( 'DELETE FROM webcal_entry_log WHERE cal_entry_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_import_data WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_site_extras WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_entry_ext_user WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_reminders WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_blob WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
    dbi_execute ( 'DELETE FROM webcal_entry WHERE cal_id = ?',
      array ( $delete_em[$i] )  );
  }

  // Delete user participation from events
  dbi_execute ( 'DELETE FROM webcal_entry_user WHERE cal_login = ?',
    array ( $user ) );
  // Delete preferences
  dbi_execute ( 'DELETE FROM webcal_user_pref WHERE cal_login = ?',
    array ( $user ) );
  // Delete from groups
  dbi_execute ( 'DELETE FROM webcal_group_user WHERE cal_login = ?',
    array ( $user ) );
  // Delete bosses & assistants
  dbi_execute ( 'DELETE FROM webcal_asst WHERE cal_boss = ?',
    array ( $user ) );
  dbi_execute ( 'DELETE FROM webcal_asst WHERE cal_assistant = ?',
    array ( $user ) );
  // Delete user's views
  $delete_em = array ();
  $res = dbi_execute ( 'SELECT cal_view_id FROM webcal_view WHERE cal_owner = ?',
    array ( $user ) );
  if ( $res ) {
    while ( $row = dbi_fetch_row ( $res ) ) {
      $delete_em[] = $row[0];
    }
    dbi_free_result ( $res );
  }
  for ( $i = 0; $i < count ( $delete_em ); $i++ ) {
    dbi_execute ( 'DELETE FROM webcal_view_user WHERE cal_view_id = ?',
      array ( $delete_em[$i] ) );
  }
  dbi_execute ( 'DELETE FROM webcal_view WHERE cal_owner = ?',
    array ( $user ) );
  //Delete them from any other user's views
  dbi_execute ( 'DELETE FROM webcal_view_user WHERE cal_login = ?',
    array ( $user ) );
  // Delete layers
  dbi_execute ( 'DELETE FROM webcal_user_layers WHERE cal_login = ?',
    array ( $user ) );
  // Delete any layers other users may have that point to this user.
  dbi_execute ( 'DELETE FROM webcal_user_layers WHERE cal_layeruser = ?',
    array ( $user ) );
  // Delete user
  dbi_execute ( 'DELETE FROM webcal_user WHERE cal_login = ?',
    array ( $user ) );
  // Delete function access
  dbi_execute ( 'DELETE FROM webcal_access_function WHERE cal_login = ?',
    array ( $user ) );
  // Delete user access
  dbi_execute ( 'DELETE FROM webcal_access_user WHERE cal_login = ?',
    array ( $user ) );
  dbi_execute ( 'DELETE FROM webcal_access_user WHERE cal_other_user = ?',
    array ( $user ) );
  // Delete user's categories
  dbi_execute ( 'DELETE FROM webcal_categories WHERE cat_owner = ?',
    array ( $user ) );
  dbi_execute ( 'DELETE FROM webcal_entry_categories WHERE cat_owner = ?',
    array ( $user ) );
  // Delete user's reports
  $delete_em = array ();
  $res = dbi_execute ( 'SELECT cal_report_id FROM webcal_report WHERE cal_login = ?',
    array ( $user ) );
  if ( $res ) {
    while ( $row = dbi_fetch_row ( $res ) ) {
      $delete_em[] = $row[0];
    }
    dbi_free_result ( $res );
  }
  for ( $i = 0; $i < count ( $delete_em ); $i++ ) {
    dbi_execute ( 'DELETE FROM webcal_report_template WHERE cal_report_id = ?',
      array ( $delete_em[$i] ) );
  }
  dbi_execute ( 'DELETE FROM webcal_report WHERE cal_login = ?',
    array ( $user ) );
    //not sure about this one???
  dbi_execute ( 'DELETE FROM webcal_report WHERE cal_user = ?',
    array ( $user ) );
  // Delete user templates
  dbi_execute ( 'DELETE FROM webcal_user_template WHERE cal_login = ?',
    array ( $user ) );
}

/**
 * Get a list of users and return info in an array.
 *
 * @param bool  $publicOnly  return only public data
 * @return array Array of user info
 */
//envole :
//choix pour des raisons d'effets de bord de lister les utilisateurs depuis la base du webcalendar
//et non de l'annuaire donc une importation globale est à prévoir
//par contre, pour pouvoir trier la liste, une connexion ldap permet de récupérer la classe
function user_get_users($publicOnly=false)
	{
	global $PUBLIC_ACCESS,$PUBLIC_ACCESS_FULLNAME,$USER_SORT_ORDER,$error,
	       $ds,$ldap_base_dn,$ldap_user_attr,$ldap_user_filter,$ldap_uid_exclusions,$ALLOW_VIEW_TEACHERS,$DISPLAY_CDT;
	$count = 0;
	$ret = array();
	$ar_groupes = array(); //tableau transitoire pour trier les utilisateurs par groupe (classes, métas ou profs)
	$ar_classes = array(); //pour mémoriser les classes et les inclure dans la liste

	if($PUBLIC_ACCESS=='Y')
	$ret[$count++] = array
		(
		'cal_login'=>'__public__',
		'cal_lastname'=>'',
		'cal_firstname'=>'',
		'cal_is_admin'=>'N',
		'cal_email'=>'',
		'cal_password'=>'',
		'cal_fullname'=>$PUBLIC_ACCESS_FULLNAME
		);
	if($publicOnly) return $ret;

	$r=connect_and_bind();
	$order1 = empty($USER_SORT_ORDER) ? 'cal_firstname,cal_lastname' : $USER_SORT_ORDER;
	$res = dbi_execute ('SELECT cal_login,cal_lastname,cal_firstname,cal_is_admin,cal_email,cal_passwd FROM webcal_user ORDER BY '.$order1.',cal_login');
	if($res && $r)
		{
		while($row=dbi_fetch_row($res))
			{
			$login = $row[0];
			if(in_array(strtolower($login),$ldap_uid_exclusions)) continue; //utilisateurs à ne pas lister

			//classes : comptes facultatifs, elles sont donc gérées plus bas pour apparaître tous si $DISPLAY_CDT
			if(preg_match("/^meta_classe_(.+)$/i",$login,$matches))
				{
				$classe = $matches[1];
				if(!in_array($classe,$ar_classes)) $ar_classes[] = $classe;
				continue;
				}

			//on recherche son groupe
			$groupe = "4_autres"; //par défaut, à la fin
			$classe = "autre";
			$sr = @ldap_search($ds,$ldap_base_dn,"(uid=".$login.")",$ldap_user_attr);
			if(!$sr) $error = 'Error searching LDAP server: '.@ldap_error($ds);
			else
				{
				$info = @ldap_get_entries($ds,$sr);
				if($info['count']>0)
					{
					$profil = $info[0][$ldap_user_attr[1]][0];
					if(preg_match("/ou=eleves/i",$info[0]["dn"]) || $profil=="eleve")
						{
						$classe = $info[0][$ldap_user_attr[0]][0];
						if($DISPLAY_CDT=="Y" && !in_array($classe,$ar_classes)) $ar_classes[] = $classe;
						$groupe = "2_".$classe;
						}
					elseif($profil=="enseignant")
						{
						$groupe = "3_enseignants";
						}
					}
				@ldap_free_result($sr);
				}

			//possibilité laissée à l'administrateur de ne lister aux élèves que les autres élèves et les classes
			//on applique cette règle de droits seulement si les ACL (appelés UAC dans Webcalendar) sont désactivés
			if(!access_is_enabled() && user_stop_access($login,$classe)) continue;

			//on ajoute l'utilisateur
			if (strlen($row[1])) $fullname = $row[2]." ".$row[1];
			else $fullname = $row[0];
			$ar_groupes[$groupe][] = array
				(
				'cal_login'=>$row[0],
				'cal_lastname'=>$row[1],
				'cal_firstname'=>$row[2],
				'cal_is_admin'=>$row[3],
				'cal_email'=>empty($row[4]) ? '' : $row[4],
				'cal_password'=>$row[5],
				'cal_fullname'=>$fullname,
				'cal_groupe' =>substr($groupe,2)
				);
			}
		@ldap_close ($ds);
		dbi_free_result($res);

		//ajout des classes identifiées
		sort($ar_classes);
		foreach($ar_classes as $classe)
			{
			$ar_groupes["1_classes"][] = array
				(
				'cal_login'=>'meta_classe_'.$classe,
				'cal_lastname'=>$classe,
				'cal_firstname'=>'',
				'cal_is_admin'=>'N',
				'cal_email'=>'',
				'cal_password'=>'',
				'cal_fullname'=>$classe,
				'cal_groupe' =>'classes'
				);
			}

		//tri en fonction du groupe
		ksort($ar_groupes);
		foreach($ar_groupes as $groupe=>$individus) {foreach($individus as $individu) $ret[$count++] = $individu;}
		}
	/*
	echo "<pre>";
	print_r($ret);
	die("</pre>");
	*/
	return $ret;
	}

//envole :  aucun administrateur n'est récupéré, la variable $ldap_admin_group_name semble incorrect
// Searches $ldap_admin_group_name and returns an array of the group members.
// Do this search only once per request.
// returns: array of admins
function get_admins () {
  global $error, $ds, $cached_admins;
  global $ldap_admin_group_name,$ldap_admin_group_attr,$ldap_admin_group_type;

  if ( ! empty ( $cached_admins ) ) return $cached_admins;
  $cached_admins = array ();

  if ($r = connect_and_bind ()) {
    $search_filter = "($ldap_admin_group_attr=*)";
    $sr = @ldap_search ( $ds, $ldap_admin_group_name, $search_filter, array ($ldap_admin_group_attr) );
    if (!$sr) {
      $error = 'Error searching LDAP server: ' . ldap_error( $ds );
    } else {
      $admins = ldap_get_entries( $ds, $sr );
      for( $x = 0; $x < $admins[0][$ldap_admin_group_attr]['count']; $x ++ ) {
       if ($ldap_admin_group_type != 'posixgroup') {
          $cached_admins[] = stripdn($admins[0][$ldap_admin_group_attr][$x]);
        } else {
          $cached_admins[] = $admins[0][$ldap_admin_group_attr][$x];
        }
      }
      @ldap_free_result($sr);
    }
    @ldap_close ( $ds );
  }
  return $cached_admins;
}

//envole : fonction laissée intacte spécifique à la connexion ldap et non utilisée
// Strip everything but the username (uid) from a dn.
//  params:
//    $dn - the dn you want to strip the uid from.
//  returns: string - userid
//
//  ex: stripdn(uid=jeffh,ou=people,dc=example,dc=com) returns jeffh
function stripdn($dn){
  list ($uid,$trash) = split (',', $dn, 2);
  list ($trash,$user) = split ('=', $uid);
  return($user);
}

//envole : fonction laissée intacte spécifique à la connexion ldap et utilisée dans user_get_users()
// Connects and binds to the LDAP server
// Tries to connect as $ldap_admin_dn if we set it.
//  returns: bind result or false
function connect_and_bind () {
  global $ds, $error, $ldap_server, $ldap_port, $ldap_version;
  global $ldap_admin_dn, $ldap_admin_pwd, $ldap_start_tls, $set_ldap_version;

  if ( ! function_exists ( 'ldap_connect' ) ) {
    die_miserable_death ( 'Your installation of PHP does not support LDAP' );
  }

  $ret = false;
  $ds = @ldap_connect ( $ldap_server, $ldap_port );
  if ( $ds ) {
    if ($set_ldap_version || $ldap_start_tls)
      ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, $ldap_version);

    if ($ldap_start_tls) {
      if (!ldap_start_tls($ds)) {
        $error = 'Could not start TLS for LDAP connection';
        return $ret;
      }
    }

    if ( $ldap_admin_dn != '') {
      $r = @ldap_bind ( $ds, $ldap_admin_dn, $ldap_admin_pwd );
    } else {
      $r = @ldap_bind ( $ds );
    }

    if (!$r) {
      $error = 'Invalid Admin login for LDAP Server';
    } else {
      $ret = $r;
    }
  } else {
    $error = 'Error connecting to LDAP server';
    $ret = false;
  }
  return $ret;
}

/**
 * Ajout envole
 * Vérifier l'autorisation d'afficher l'agenda d'un autre utilisateur
 * Utilisé dans la fonction access_user_calendar() du script access.php et rendu inefficace si les ACL (appelés UAC dans Webcalendar) sont activés
 *
 * @return bool True on success
 */
function user_valid_access()
	{
	global $user;
	if(!isset($user) || empty($user)) return true; //pas de demande pour un autre agenda
	return !user_stop_access($user);
	}

/**
 * Ajout envole
 * Vérifier l'autorisation d'afficher le calque d'un autre utilisateur
 * Utilisé dans la fonction load_user_layers() du script functions.php et rendu inefficace si les ACL (appelés UAC dans Webcalendar) sont activés
 *
 * @return bool True on success
 */
function calque_valid_access($user)
	{
	return !user_stop_access($user);
	}

/**
 * Ajout envole
 * Bloquer l'accès à l'agenda de {$user}
 *
 * @param string $user login de l'agenda à visualiser
 * @param string $classe classe éventuelle de {$user}
 *
 * @return bool True si l'accès doit être bloqué
 */
function user_stop_access($user="",$classe="")
	{
	global $ALLOW_VIEW_TEACHERS,$ldap_profils,$ldap_uid_exclusions;

	if(empty($user)) return true;

	if(in_array(strtolower($user),$ldap_uid_exclusions)) return true; //cas à exclure automatiquement

	if(!isset($_SESSION["webcalendar_profil"]) || empty($_SESSION["webcalendar_profil"])) return true; //identification impossible du profil

	switch($_SESSION["webcalendar_profil"])
		{
		case $ldap_profils[0] : //élève
		if(empty($classe)) $classe = search_classe($user); //ne doit pas être fait si utilisé depuis user_get_users (double connexion ldap...)
		if($ALLOW_VIEW_TEACHERS == 'N' && (!$classe || $classe=="autre")) return true; //seul voir les élèves et classes est autorisé
		break;

		case $ldap_profils[1] : //responsable
		if(!preg_match("/^meta_classe_(.+)$/i",$user)) return true; //interdiction totale à part les classes
		break;
		}

	return false; //on autorise l'accès
	}

/**
 * Ajout envole
 * Non affichage du menu et de l'édition pour les responsables
 * utilisé dans init.php et week.php
 *
 * @return bool True si l'affichage est autorisé
 */
function user_show_html($boolean=true)
	{
	global $ldap_profils;

	$can_view = !isset($_SESSION["webcalendar_profil"]) || $_SESSION["webcalendar_profil"]!=$ldap_profils[1]; //responsable

	if($can_view) return $boolean;
	else return false;
	}

/********************* début plugin cdt *****************************/
/*
Le plugin est activée dans l'application à deux conditions :
    - si le paramètre "DISPLAY_CDT" de l'utilisateur est à "Y"
    - si la variable $cdt_plugin existe (c'est le cas seulement si le plugin est correctement initialisé)
*/
$plugin_cdt_file = "includes/plugin_cdt.php";
if(is_file($plugin_cdt_file))
	{
	include_once($plugin_cdt_file);
	$cdt_plugin = new readCDT();
	if(!$cdt_plugin->is_ok) unset($cdt_plugin);
	}
/*********************** fin plugin cdt *****************************/
?>
