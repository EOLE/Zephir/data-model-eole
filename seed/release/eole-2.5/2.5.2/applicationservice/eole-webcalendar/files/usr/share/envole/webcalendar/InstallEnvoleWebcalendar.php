#!/usr/bin/env php
<?php
// -*- coding: UTF-8 -*-

//ajout spécifique WebCalendar : session utilisée par la fonction get_installed_version() dans install_webcalendar.php
session_start();

//permettre de forcer l'affichage en direct lors de l'exécution
if(in_array("--help",$argv) || in_array("-h",$argv)) die("option unique pour obtenir l'affichage complet, absent par défaut : -v ou -V (verbose)\n");
elseif(in_array("-v",$argv) || in_array("-V",$argv)) $verbose = true;
else $verbose = false;

//paramètres généraux --> obligatoires car existence vérifiée par EnvoleTools
define('APPLI_NAME','WebCalendar'); //pour affichage
define('APPLI_MODULE','webcalendar'); //pour noms de fichiers ou répertoires
define('APPLI_DICO','activer_webcalendar'); //nom de la variable dans le gen_config

//paramètres de connexion qui seront affectés à l'application (DB_PASS sera généré par la librairie) --> obligatoires car existence vérifiée par EnvoleTools
define('DB_NAME','webcalendar');
define('DB_USER','webcalendar');

//classe générique qui gère les opérations utiles à l'installation et présente dans l'include_path du php.ini (/usr/share/php)
if(!is_file("/usr/share/php/envole-php/EnvoleTools.class.php"))
die("---- INSTALLATION IMPOSSIBLE ----\nCause : le fichier \"/usr/share/php/envole-php/EnvoleTools.class.php\" est introuvable. Réinstaller le paquet \"envole-php-apps\".\n\n");
require_once("envole-php/EnvoleTools.class.php");

//fonction à redéfinir pour chaque application et gérant la vérification des paramètres du fichier de connexion
function connexion_analyse()
	{
	global $install,$db_host,$db_login,$db_password,$db_database;

	if(!is_file(FILE_CONNEXION))
		{
		$install->afficher("Le fichier de connexion est introuvable... il va être recréé\n");
		$install->create_user = true;
		}
	else
		{	
		//récupération des paramètres de connexion
		//méthode de lecture reprise du fichier "webcalendar/includes/config.php"
		$install->afficher("Lecture des paramètres de connexion... ");
		$webcalendar_file = file_get_contents(FILE_CONNEXION);
		$webcalendar_parametres = array("db_type","db_host","db_database","db_login","db_password","user_inc"); //paramètres à vérifier
		$webcalendar_file = preg_replace("/[\r\n]+/","\n",$webcalendar_file);
		$webcalendar_content = explode("\n",$webcalendar_file);
		for($n=0; $n<count($webcalendar_content); $n++)
			{
			$buffer = $webcalendar_content[$n];
			$buffer = trim($buffer,"\r\n ");
			if(preg_match("/^(#|\/\*|<\?|\?>)/",$buffer)) continue;
			if(preg_match("/(\S+):\s*(\S+)/",$buffer,$matches))
				{
				if(in_array($matches[1],$webcalendar_parametres))
					{
					$$matches[1] = $matches[2];
					unset($webcalendar_parametres[array_search($matches[1],$webcalendar_parametres)]);
					}
				}
			}	
		$install->afficher("OK\n");

		//tester la validité des paramètres obtenus
		if(count($webcalendar_parametres)!=0) //paramètre absent
			{
			$install->afficher("Le fichier de connexion est incomplet... il va être réécrit\n");
			$install->create_user = true;
			}
		elseif($user_inc!="user-cas.php" || !$install->test($db_host,$db_login,$db_password,$db_database)) //test des paramètres
			{
			$install->afficher("Les paramètres sont incorrects... ils vont être mis à jour\n");
			$install->create_user = true;
			}
		else //précaution sur les droits du fichier
			{
			exec("chown root:www-data ".FILE_CONNEXION);
			exec("chmod 640 ".FILE_CONNEXION);
			}
		}
	}

//fonction à redéfinir pour chaque application et gérant la création du fichier de connexion
function connexion_creation()
	{
	global $install,$db_password;
	
	$install->afficher("Création du fichier de connexion... ");
	$db_password = DB_PASS; //indispensable de remettre la bonne valeur à cette variable
	$install->sed("/usr/share/envole/webcalendar/settings.php.ori",FILE_CONNEXION,"HOST_DATABASE",$install->db_host);
	$install->sed(FILE_CONNEXION,FILE_CONNEXION,"PASS_WEBCALENDAR",$db_password);
	$install->resultat(is_file(FILE_CONNEXION));

	//droits sur le fichier
	exec("chown root:www-data ".FILE_CONNEXION);
	exec("chmod 640 ".FILE_CONNEXION);
	}

//démarrage réel de l'installation qui ouvre notamment le fichier de log et récupère certaines variables du Scribe
$install = new EnvoleTools($verbose);

define('ABSPATH', CONTAINER_PATH_WEB.'/var/www/html/'.APPLI_MODULE.'/');
define('FILE_CONNEXION',ABSPATH.'includes/settings.php');
define('DB_HOST', $install->db_host);

//ajout spécifique WebCalendar : variables utilisées pour mettre à jour les tables (dans install_webcalendar.php)
$db_type = "mysql";
$db_persistent = true;
$db_host = $install->db_host;
$db_login = DB_USER;
$db_password = DB_PASS; //généré par l'instanciation de EnvoleTools
$db_database = DB_NAME;

$install->echo_sstitre("Analyse de la base \"".DB_NAME."\" et des paramètres de connexion");

//vérification de la base de données
if($install->bdd_verification()) connexion_analyse();

//traitement de création éventuelle base/user
$install->bdd_traitement();

//création éventuelle du fichier de connexion
if($install->create_user) connexion_creation();

//simple vérification de la présence des fichiers de configuration nécessaires pour assurer le SSO
$install->echo_sstitre("Vérification du paramétrage du serveur CAS");
$install->afficher("Présence du fichier \"".APPLI_MODULE.".ini\"... ");
if(is_file(DIR_FILTER.APPLI_MODULE.".ini")) $install->afficher("OK\n");
else $install->echec("Echec\nAVERTISSEMENT : Le fichier \"".DIR_FILTER.APPLI_MODULE.".ini\" est introuvable -> Installation interrompue.\nLe paquet aurait dû le fournir.\n\n");
$install->afficher("Présence du fichier \"profil_".APPLI_MODULE.".py\"... ");
if(is_file(DIR_INFOS."profil_".APPLI_MODULE.".py")) $install->afficher("OK\n");
else $install->echec("Echec\nAVERTISSEMENT : Le fichier \"".DIR_INFOS."profil_".APPLI_MODULE.".py\" est introuvable -> Installation interrompue.\nLe paquet aurait dû le fournir.\n\n");

//sauvegarde de la base avant toute mise à jour (si la base existait déjà sinon on obtient un fichier vide...)
if(!$install->create_base)
	{
	$install->echo_sstitre("Sauvegarde de la base avant toute mise à jour");
	$install->dump(DB_NAME);
	}

//installation/maj par le processus propre à l'application mais automatisé dans install_appli.php
$install->echo_titre("Procédure d'installation/maj propre à l'application");
if(!is_file($install->rep_exec."install_".APPLI_MODULE.".php")) $install->echec("\nLe fichier \"".$install->rep_exec."install_".APPLI_MODULE.".php\" est introuvable -> Installation interrompue.\nLe paquet aurait dû le fournir.\n\n");
include_once($install->rep_exec."install_".APPLI_MODULE.".php");
?>
