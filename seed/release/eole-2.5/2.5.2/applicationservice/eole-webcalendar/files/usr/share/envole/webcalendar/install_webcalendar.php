<?php
/*******************************************
en s'appuyant sur "calendar/install/index.php"
installation/mise à jour des tables
variables requises avant l'inclusion de ce fichier :
$db_type
$db_host
$db_database
$db_login
$db_password
$install
*******************************************/

$install->afficher("Base de données... ");

$show_all_errors = false; //évite de s'inquiéter pour des messages d'erreurs qui sont en réalité normaux car test sur la base de données pour identifier la maj à effectuer
$install_filename = "";

//nécessaire pour indiquer le chemin correct utilisé par "translate.php", de même que la modification du chemin dans la variable $eng_file dans le fichier lui-même
$lang_file = ABSPATH."translations/French.txt";
$ar_files = array
	(
	ABSPATH.'includes/translate.php', //nécessaire car fonction translate() appelée par certaines fonctions
	ABSPATH.'includes/dbi4php.php', //pour les échanges avec la base de données
	ABSPATH.'includes/config.php', //fonctions utilisées
	ABSPATH.'includes/formvars.php', //à priori inutile, à vérifier...
	ABSPATH.'install/default_config.php', //variables par défaut de la table "webcal_config"
	ABSPATH.'install/install_functions.php', //fonctions utilisées
	ABSPATH.'install/sql/upgrade_matrix.php' //pour chercher le niveau de mise à jour à effectuer
	);
foreach($ar_files as $nom_fichier)
	{
	if(!is_file($nom_fichier)) $install->echec("Echec\nMAJ impossible : absence du fichier \"".$nom_fichier."\" -> Installation interrompue.\n\n");
	else include_once($nom_fichier);
	}

$c = dbi_connect ( $db_host, $db_login, $db_password, $db_database, false );
if(!$c) $install->echec("Echec\nMAJ impossible : problème de connexion à la base de données -> Installation interrompue.\n\n");

//Connaître l'état de l'installation (utilise upgrade_matrix.php pour faire une série de requêtes)
//autre opérations : vérification que tous les settings sont en majuscules dans la base puis maj de ceux non présents (voir default_config.php)
//$_SESSION['install_file'] vaut 'tables' puis 'upgrade_...' et enfin '' si aucune maj à faire
//$_SESSION['old_program_version'] vaut 'new_install' puis la valeur de la version actuelle si déjà installé;
get_installed_version();

if(empty($_SESSION['old_program_version']))
$install->echec("Echec\nMAJ impossible : échec de la recherche de l'upgrade à effectuer -> Installation interrompue.\n\n");

$reload = false;
if($_SESSION['old_program_version']=='new_install' && $_SESSION['install_file']=='tables')
	{
	$install->afficher("installation des tables... ");
	$install_filename = 'tables-mysql.sql';
	$reload = true;
	}
elseif(preg_match("/^upgrade_/",$_SESSION['install_file'])) //$_SESSION['install_file'] vaut "upgrade_v1.1.0-CVS" dans la maj vers la version 1.2;
	{
	$install->afficher("MAJ à partir de \"".$_SESSION['install_file']."\"... ");
	$install_filename = 'upgrade-mysql.sql';	
	}
else $install->afficher("aucun changement de version... ");
	
if(!empty($install_filename))
	{
	db_populate($install_filename,"");
	if($reload) get_installed_version(true); //si primo-install, nécessité d'importer les paramètres de configuration donc de rejouer cette fonction
	}

//autre étapes prévues par "install/index.php" :

// If new install, run 0 GMT offset
//just to set webcal_config.WEBCAL_TZ_CONVERSION
// + besoin de convertir les dates paris->greenwich lors de la mise à jour vers 1.2.0
if($_SESSION['old_program_version']=='new_install' || $_SESSION['install_file']=="upgrade_v1.1.0-CVS") convert_server_to_GMT();

//for upgrade to v1.1b we need to convert existing categories
//and repeating events
do_v11b_updates(); //utile de le laisser ?

//v1.1e requires converting webcal_site_extras to webcal_reminders
do_v11e_updates(); //utile de le laisser ?
	
//inutile de conserver la session utilisée
$_SESSION = array();
session_destroy();

$install->afficher("OK\n");

//SERVER_URL est calculé et enregistré par l'application si absent lors de l'appel à "load_global_settings()" (voir "includes/functions.php") sous la forme "http://..."
//comme ça ne convient pas à toutes les situations (voir demande #2743), on force la valeur à "/calendar/".
//si le paramètre est absent, il aura été ajouté par "get_installed_version()" (exécuté ci-dessus) d'après le fichier "install/default_config.php"
//par précaution, on effectue tout de même une vérification
$server_url = "/webcalendar/"; //"http://".ENVOLE_URL."/webcalendar/";
$install->afficher("Vérification de l'existence du paramètre SERVER_URL... ");
$install->setRows("SELECT `cal_value` FROM `webcalendar`.`webcal_config` WHERE `cal_setting`='SERVER_URL' LIMIT 1;",DB_NAME);
if(count($install->rows)===0)
	{
	$install->afficher("création avec pour valeur \"".$server_url."\"... ");
	$install->query("INSERT INTO `webcalendar`.`webcal_config` (`cal_setting`,`cal_value`) VALUES ('SERVER_URL','".$server_url."');",DB_NAME);
	}
$install->afficher("OK\n");

//possibilité d'accès des parents sur un compte commun pour permettre la visualisation des agendas des classes (essentiellement les emplois du temps)
$install->afficher("Compte commun des responsables (Meta_parent)... ");
$install->setRows("SELECT * FROM `webcal_user` WHERE `cal_login`='Meta_parent' LIMIT 1;",DB_NAME);
if(count($install->rows)===0)
	{
	$install->afficher("il n'existe pas... création... ");
	$query_parent = "INSERT INTO `webcalendar`.`webcal_user` (`cal_login`,`cal_passwd`,`cal_lastname`,`cal_firstname`,`cal_is_admin`,`cal_email`,`cal_enabled`,`cal_telephone`,`cal_address`,`cal_title`,`cal_birthday`,`cal_last_login`) VALUES 
	('Meta_parent',MD5('Meta_parent'),'compte parent',NULL ,'N',NULL ,'Y',NULL ,NULL ,NULL ,NULL ,NULL);";
	$install->query($query_parent,DB_NAME);
	}
else $install->afficher("il existe déjà... ");
$install->afficher("OK\n");
?>
