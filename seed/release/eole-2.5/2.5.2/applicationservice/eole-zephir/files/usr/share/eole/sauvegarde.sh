#!/bin/bash
###########################################################################
# Eole NG - 2007
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# sauvegarde.sh
#
# script de création d'archives de sauvegarde Zéphir
#
###########################################################################

Lock="sauvegarde"
Excl="maj"

. /usr/lib/eole/ihm.sh

pg_version=9.3
day=`date +%d-%m-%Y-%Hh%M`
backup_dir=/var/lib/zephir_backups
tmp_backup=$backup_dir/$day
pg_data=/var/lib/postgresql/$pg_version/main
pg_dir=/etc/postgresql/$pg_version/main
pg_perms=$pg_dir/pg_hba.conf
pg_service=postgresql

if [ ! -d $backup_dir ]
then
	mkdir $backup_dir
fi
rm -rf $tmp_backup
mkdir $tmp_backup
# fichier de version
echo "$(CreoleGet eole_release)" > $tmp_backup/version

# on arrête temporairement le backend zephir (cohérence des données)
CreoleService zephir stop &> /dev/null

# postgres : se mettre en mode sans mot de passe
mv -f $pg_perms $pg_perms.savezephir
echo -e -e "local\tall\tall\ttrust" > $pg_perms
chown postgres.postgres $pg_perms
chmod 600 $pg_perms
CreoleService $pg_service reload &> /dev/null

# sauvegarde des données de la base PostgreSQL
echo -e
EchoCyan "Sauvegarde en cours, veuillez patienter ..."

echo -e " - base PostgreSQL"
su - postgres -c "pg_dumpall > /tmp/backup_db.sql"
mv /tmp/backup_db.sql ${tmp_backup}/backup_db.sql

# postgres : se remettre en mode mot de passe
rm -f $pg_perms
mv -f $pg_perms.savezephir $pg_perms
chown postgres.postgres $pg_perms
chmod 600 $pg_perms
CreoleService $pg_service reload &> /dev/null

# sauvegarde des répertoires et fichiers importants

pushd /var/lib > /dev/null 2>&1

echo -e " - base LDAP"
# sauvegarde de la base ldap
# tar -cpf ${tmp_backup}/var_lib_ldap.tar ldap
CreoleRun "slapcat -f /etc/ldap/slapd.conf -l /tmp/ldap.ldif" annuaire
mv -f $container_path_annuaire/tmp/ldap.ldif ${tmp_backup}/ldap.ldif
echo -e " - configuration des serveurs"
# /var/lib/zephir
tar -cpf ${tmp_backup}/var_lib_zephir.tar zephir

# /etc/uucp/
cd /etc
tar -cpf ${tmp_backup}/etc_uucp.tar uucp
echo -e " - clés de connexion"
# /var/spool/uucp/
cd /var/spool
tar -cpf ${tmp_backup}/var_spool_uucp.tar uucp

# on relance le service
CreoleService zephir start &> /dev/null

echo -e " - configuration EOLE"
mkdir -p ${tmp_backup}/etc_eole/dicos
cp -rf /usr/share/eole/creole/dicos/local ${tmp_backup}/etc_eole/dicos/
cp -rf /usr/share/eole/creole/dicos/variante ${tmp_backup}/etc_eole/dicos/
cp -rf /usr/share/eole/creole/patch ${tmp_backup}/etc_eole/
mkdir -p ${tmp_backup}/etc_eole/distrib
# templates non installés par un paquet
for TMPL in `ls /usr/share/eole/creole/distrib/*`
do
    dpkg -S $TMPL >/dev/null 2>&1
    if [ $? -ne 0 ];then
        /bin/cp -rf $TMPL ${tmp_backup}/etc_eole/distrib/
    fi
done
cp /etc/eole/config.eol ${tmp_backup}/etc_eole/
cd ${tmp_backup}/etc_eole/
tar -cpf ${tmp_backup}/etc_eole.tar config.eol dicos patch distrib
cd ..
rm -rf etc_eole
echo -e " - configuration EAD et certificats SSL"
# configurations diverses (EAD/certificats/sso/...)
mkdir -p ${tmp_backup}/config
ead_config=/usr/share/ead2/backend/config
[ -f "$ead_config/perm_local.ini" ] && cp -f "$ead_config/perm_local.ini" "${tmp_backup}/config"
[ -f "$ead_config/roles_local.ini" ] && cp -f "$ead_config/roles_local.ini" "${tmp_backup}/config"
ead_tmp=/usr/share/ead2/backend/tmp
eole_config=/var/lib/eole/config
[ -f "$ead_tmp/cron.txt" ] && cp -f "$ead_tmp/cron.txt" "${tmp_backup}/config/cron.txt"
if [ -f "$eole_config/bp_server.conf" ];then
    cp -f "$eole_config/bp_server.conf" "${tmp_backup}/config/bp_server.conf"
else
    cp -f "$ead_tmp/bp_server.txt" "${tmp_backup}/config/bp_server.conf"
fi
cp -f $eole_config/dhcp.conf "${tmp_backup}/config/" 2>/dev/null
tar -cpf ${tmp_backup}/config.tar config
rm -rf config
# certificats SSL
mkdir -p ${tmp_backup}/ssl
cp -rf /etc/ssl/* "${tmp_backup}/ssl"
# suppression des liens symbolique et certificats 'système'
find "${tmp_backup}/ssl" -type l -delete
rm -f ${tmp_backup}/ssl/certs/ca-certificates.crt 2>/dev/null
rm -f ${tmp_backup}/ssl/certs/*snakeoil*.* 2>/dev/null
rm -f ${tmp_backup}/ssl/private/*snakeoil*.* 2>/dev/null
cd ${tmp_backup}/ssl
tar -cpf ${tmp_backup}/ssl.tar *
cd ..
rm -rf ssl
# configuration EoleSSO
sso_dir=/usr/share/sso
if [ -d $sso_dir ];then
    echo -e " - configuration d'EoleSSO"
    mkdir -p ${tmp_backup}/sso
    for sso_conf_dir in app_filters attribute_sets external_attrs user_infos metadata interface securid_users
    do
        [ -d ${sso_dir}/${sso_conf_dir} ] && /bin/cp -rf ${sso_dir}/${sso_conf_dir} ${tmp_backup}/sso
    done
fi
cd ${tmp_backup}/sso
# purge des fichiers ~ et .pyc
find -name "*.pyc" -exec rm -f {} \;;find -name "*~" -exec rm -f {} \;
tar -cpf ${tmp_backup}/sso.tar *
cd ..
rm -rf sso
echo -e " - dictionnaires personnalisés"
mkdir -p "${tmp_backup}/usr_dictionnaires"
dict_dir=/usr/share/zephir/dictionnaires
for dir_local in `ls -d $dict_dir/*/local`
do
    dir_local=`dirname $dir_local`
    version=`basename $dir_local`
    mkdir -p "${tmp_backup}/usr_dictionnaires/$version"
    cp -rf "${dir_local}/local" "${tmp_backup}/usr_dictionnaires/$version/"
done
cd ${tmp_backup}/usr_dictionnaires
tar -cpf "${tmp_backup}/usr_dictionnaires.tar" *
cd ..
rm -rf usr_dictionnaires

mkdir -p "${tmp_backup}/creole_funcs/creole2"
funcs_dir=/usr/share/creole/funcs_creole2
if [ -d $funcs_dir ]
then
    echo -e " - fonctions CREOLE personnalisées (serveurs 2.0 à 2.3)"
    for func_file in ${funcs_dir}/*.py
    do
        dpkg -S $func_file >/dev/null 2>&1
        if [ $? -ne 0 ]
        then
            # on ne conserve que les fichiers ne provenant pas d'un paquet
            echo "   $(basename $func_file)"
            cp $func_file ${tmp_backup}/creole_funcs/creole2/
        fi
    done
fi
mkdir -p "${tmp_backup}/creole_funcs/creole3"
funcs_dir=/usr/share/creole/funcs
if [ -d $funcs_dir ]
then
    echo -e " - fonctions CREOLE personnalisées (serveurs 2.4 et >)"
    for func_file in ${funcs_dir}/*.py
    do
        dpkg -S $func_file >/dev/null 2>&1
        if [ $? -ne 0 ]
        then
            # on ne conserve que les fichiers ne provenant pas d'un paquet
            echo "   $(basename $func_file)"
            cp $func_file ${tmp_backup}/creole_funcs/creole3/
        fi
    done
fi
cd ${tmp_backup}/creole_funcs
tar -cpf ${tmp_backup}/creole_funcs.tar creole2 creole3
cd ..
rm -rf creole_funcs

echo -e " - modules personnalisés"
mkdir -p ${tmp_backup}/usr_default_modules
mod_dir=/usr/share/zephir/default_modules
for modfile in $mod_dir/*/*
do
    [ -f "${modfile}" ] || continue
    if ! dpkg -S $modfile 2&> /dev/null
    then
        dist_dir=`dirname $modfile`
        version=`basename $dist_dir`
        mkdir -p "${tmp_backup}/usr_default_modules/${version}"
        cp -f "${modfile}" "${tmp_backup}/usr_default_modules/${version}/"
    fi
done
cd "${tmp_backup}/usr_default_modules"
if [ "$(ls -A ${tmp_backup}/usr_default_modules)" ]
then
    tar -cpf "${tmp_backup}/usr_default_modules.tar" *
fi
cd ..
rm -rf usr_default_modules

# création du fichier de sauvegarde compressé
EchoCyan "Compression de l'archive..."
cd $backup_dir
tar -czf $day.tar.gz $day
echo -e
EchoVert "Archive créée : $backup_dir/$day.tar.gz"
echo -e

# suppression du répertoire temporaire
rm -rf $tmp_backup
# retour au répertoire d'origine
popd >/dev/null 2>&1
