#!/usr/bin/env python
# -*- coding: UTF-8 -*-
###########################################################################
# Eole NG - 2007
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# script de mise à jour des anciennes configurations uucp pour Zéphir 2.3
#
###########################################################################

from glob import glob
import os
conf_dir = "/etc/uucp/serveurs"
new_line = "protocol t"

if os.path.isdir(conf_dir):
    for client_conf_f in glob(os.path.join(conf_dir, '*.sys')):
        new_conf = []
        for line in open(client_conf_f).read().split('\n'):
            new_conf.append(line)
            if line.strip() ==  "port tcp":
                new_conf.append(new_line)
        # écriture du nouveau fichier
        print "écrasement %s" % client_conf_f
        f_conf = open(client_conf_f, 'w')
        f_conf.write("\n".join(new_conf))
        f_conf.close()

