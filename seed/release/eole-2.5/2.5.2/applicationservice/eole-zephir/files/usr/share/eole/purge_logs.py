#!/usr/bin/env python
# -*- coding: UTF-8 -*-
###########################################################################
# Eole NG - 2007
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# purge_logs.py
#
# script de purge des logs serveur pour zephir (--help pour les options)
#
###########################################################################

import sys, time, os
if '/usr/share' not in sys.path:
    sys.path.append('/usr/share')

import psycopg2 as PgSQL

__version__ = '%prog v0.1'

# date limite par défaut : logs datant de plus de 30 jours
DATE_DEFAULT = time.ctime(time.time() - (3600 * 24 * 30))
TYPE_DEFAULT = 'SURVEILLANCE, RVP'

def parse_command_line():
    """
    Construit le parser d'options de la ligne de commande,
    parse la ligne de commande, et retourne les options
    """

    from optparse import OptionParser
    parser = OptionParser("""\n%prog [options]""", version = __version__)

    parser.add_option("-d", "--date", dest="date",
                      default='',
                      help=u"date avant laquelle les logs seront supprimés (JJ/MM/AAAA) -30 jours par défaut",
                      metavar="DATE")

    parser.add_option("-t", "--typelog", dest="type_log",
                      default='',
                      help=u"type de logs à supprimer: SURVEILLANCE, RVP par défaut, all pour tous",
                      metavar="TYPE_LOG")

    parser.add_option("-a", "--archive_name", dest="archive",
                      default='',
                      help=u"Archive de sauvegarde des logs, pas de sauvegarde si vide (défaut)",
                      metavar="ARCHIVE")

    parser.add_option("-s", "--stats",
                      action="store_true", dest="stats_mode", default=False,
                      help=u"afficher des statistiques sur les logs par type")

    options, args = parser.parse_args()
    if options.archive != '':
        options.archive = os.path.abspath(options.archive)
        res = os.system("""su - postgres -c \"touch %s\" > /dev/null 2>&1""" % options.archive)
        if res != 0:
            print "erreur, le répertoire %s n'est pas accessible" % os.path.dirname(options.archive)
            sys.exit(1)
    return options.date, options.type_log, options.archive, options.stats_mode

def do_purge(date, type_log, archive='', stats_mode=False):
    """
    effectue la purge des types de logs du type spécifié avant 'date'
    """
    # calcul type de logs
    if type_log == '':
        type_log = TYPE_DEFAULT
    if type_log == 'all':
        type_clause = ""
    else:
        type_clause = " and type in ('%s')" % "', '".join([t.strip() for t in type_log.split(',')])
    # calcul date limite
    if date == '':
        date = DATE_DEFAULT
    else:
        date = date.split('/')
        try:
            date = str(PgSQL.Timestamp(int(date[2]),int(date[1]),int(date[0]),0,0,0).adapted)
        except Exception, e:
            print "erreur de conversion de la date : %s" % str(e)
            sys.exit(1)

    print "\ntraitement des logs de type %s antérieurs à %s (cette opération peut prendre du temps)\n" % (type_log, date)

    query = "from log_serveur where date < '%s'%s" % (date, type_clause)
    if archive != '':
        # archivage des logs avant suppression
        print "archivage .."
        cmd = """su - postgres -c \"psql zephir -A -c \\"select * %s order by id_serveur, date\\" -o %s\" """ % (query, archive)
        res = os.system(cmd)
        res = os.system("/bin/gzip %s" % archive)
        if res != 0:
            print "erreur lors de l'archivage des logs dans %s !!" % archive
            sys.exit(1)
    # connexion à  la base
    if stats_mode == True:
        print "nombre de logs par type ...\n"
        cmd = """su - postgres -c \"psql zephir -c \\"select count(id), type %s group by type\\"\" """ % query
        os.system(cmd)
    else:
        print "suppression ...\n"
        cmd = """su - postgres -c \"psql zephir -c \\"delete %s\\"\" """ % query
        os.system(cmd)
        print "réindexation de la table ...\n"
        cmd = """su - postgres -c \"psql zephir -c \\"REINDEX TABLE log_serveur\\"\" """
        os.system(cmd)
    print "terminé \n"

if __name__ == '__main__':
    date, type_log , archive, stats_mode = parse_command_line()
    do_purge(date, type_log, archive, stats_mode)
