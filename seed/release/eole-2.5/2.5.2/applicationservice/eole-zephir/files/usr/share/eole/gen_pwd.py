#!/usr/bin/env python
# -*- coding: UTF-8 -*-
###########################################################################
# Eole NG - 2007  
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr 
#  
# gen_pwd.py
#  
# Outil de mise à jour des mots de passe LDAP et potgresql pour Zephir
# option --nopgsql pour changer seulement LDAP.
#       
###########################################################################
import os, sys
from pyeole.process import system_out
from creole import config

def gen_password():
    cmd = ["/usr/bin/pwgen", "-1"]
    sortie = system_out(cmd)
    return sortie[1].strip()

def apply_passwd(passwd,modifs):

    for file_path in modifs.keys():
        # pour chaque fichier de conf à modifier
        conf_file = open(file_path,"r")
        print "\nMise à jour du fichier "+file_path+".."
        buffer_in=conf_file.readlines()
        conf_file.close()
        chaine_recherche = modifs[file_path]
        buffer_out = ""

        # on parcourt le fichier
        for line in buffer_in:
            # si la ligne actuelle contient la chaine recherchée
            if line.startswith(chaine_recherche):
                # print "définition du mot de passe trouvée : "+line.strip()
                buffer_out += chaine_recherche
                buffer_out += passwd
                if chaine_recherche.endswith('"'):
                    # si le mot de passe est entre guillemets, on ajoute le guillemet fermant
                    buffer_out += '"'
                if file_path.endswith('.pl') or file_path.endswith('.pm'):
                    buffer_out += ';'
                buffer_out += "\n"
            else:
                # sinon, on recopie la ligne d'origine
                buffer_out+=line
            
        # sauvegarde du fichier
        conf_file = open(file_path,"w")
        conf_file.write(buffer_out)
        conf_file.close()

def main():
    pgsql=1
    try:
        if '--nopgsql' in sys.argv:
            pgsql=0
    except:
        pass
    if pgsql == 1:
        # changement du mot de passe de zephir et postgres pour postgresql
        print "\n mise à jour du mot de passe postgresql ..."
        pwd_zephir = gen_password()
        pwd_postgres = gen_password()
        # changement dans la base
        cmd_sql = """su postgres -c "psql -d template1 -c \\"ALTER USER zephir WITH PASSWORD '%s'\\"" 2>&1 >/dev/null """ % pwd_zephir
        os.system(cmd_sql)
        cmd_sql = """su postgres -c "psql -d template1 -c \\"ALTER USER postgres WITH PASSWORD '%s'\\"" 2>&1 >/dev/null """ % pwd_postgres
        os.system(cmd_sql)
        # sauvegarde dans l'appli
        import zephir
        lib_dir = os.path.dirname(zephir.__file__)
        modifs = {'/usr/share/zephir/utils/conf_db':'DB_PASSWD = "'}
        apply_passwd(pwd_zephir,modifs)

if __name__ == '__main__':
	main()
