#!/bin/bash
###########################################################################
# Eole NG - 2007
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# restauration.sh
#
# script de restauration d'une archive de sauvegarde Zéphir
#
###########################################################################

Lock="sauvegarde"
Excl="maj"

. /usr/lib/eole/ihm.sh

pg_version=9.3
backup_dir=/var/lib/zephir_backups
pg_data=/var/lib/postgresql/$pg_version
pg_dir=/etc/postgresql/$pg_version/main
pg_perms=$pg_dir/pg_hba.conf
pg_service=postgresql
initdb=/usr/lib/postgresql/$pg_version/bin/initdb


if [ ! -d $backup_dir ]
then
    EchoRouge "Erreur, répertoire des sauvegardes non trouvé"
    exit 1
fi
pushd $backup_dir &>/dev/null 2>&1

if [ ! -z $1 ]
then
    archive=$1
    basename=`echo -e $archive | sed -e s/"\.tar\.gz"/""/`
else
    archive=/tmp
fi

while [ ! -f $archive ]
do
    # pas de fichier passé en paramètre
    save_files=`ls *.tar.gz | sed -e s/".tar.gz"/""/`
    echo -e "     Utilitaire de restauration Zéphir"
    echo -e
    EchoRouge "!! Attention : toutes les modifications effectuées"
    EchoRouge "   après la sauvegarde restaurée seront perdues !!"
    echo -e
    EchoCyan "Liste des sauvegardes présentes :"
    echo -e
    echo -e "$save_files"
    echo -e
    read -p "Sauvegarde à restaurer (rien pour sortir): " basename
    if [ -z $basename ]
    then
        exit 0
    fi
    if [ -f $basename.tar.gz ]
    then
        archive=$basename.tar.gz
    else
        echo -e "Fichier invalide : $basename.tar.gz"
    fi
done

echo -e "Arrêt du service Zéphir..."
CreoleService zephir stop > /dev/null 2>&1
echo -e "Décompression en cours..."
tar xzf $archive
cd $basename
ARCHIVE_VERSION=$(cat version)
if [ "$ARCHIVE_VERSION" == "2.3" ]
then
    # migration des données d'un serveur 2.3
    echo -e
    EchoOrange "Attention, La sauvegarde provient d'un serveur Zéphir 2.3"
    read -p "Restaurer les données (o/n) ? " migr_ok
    echo -e
    if [ "$migr_ok" != "o" ] && [ "$migr_ok" != "O" ]
    then
        EchoRouge "Abandon ...\n"
        exit 1
    fi
fi

echo -e "Vérification des données..."
if [ ! -f backup_db.sql ]
then
    EchoRouge "Sauvegarde de la base de données non trouvée"
    exit 1
fi
if [ ! -f ldap.ldif ]
then
    EchoRouge "Sauvegarde de la base ldap non trouvée"
    exit 1
fi
if [ ! -f var_lib_zephir.tar ]
then
    EchoRouge "Sauvegarde des configurations des clients non trouvée"
    exit 1
fi
if [ ! -f etc_uucp.tar ]
then
    EchoRouge "Sauvegarde des configurations uucp non trouvée"
    exit 1
fi
if [ ! -f var_spool_uucp.tar ]
then
    EchoRouge "Sauvegarde des clés de connexion des clients non trouvée"
    exit 1
fi
if [ ! -f etc_eole.tar ]
then
    EchoRouge "Sauvegarde de la configuration du serveur non trouvée"
    exit 1
fi

rep="N"
# restauration des données de la base postgresql
read -p "Restaurer la base de données (o/n) ? " rep
echo -e
if [ "$rep" == "o" ] || [ "$rep" == "O" ]
then
    echo -e " - base PostgreSQL"
    cp -f $pg_perms /tmp/pg_hba.savezephir
    cp -f $pg_dir/postgresql.conf /tmp/postgresql.conf
    CreoleService $pg_service stop &> /dev/null
    rm -rf $pg_data/main/*
    echo -e " - initialisation de la base"
    su - postgres -c "$initdb -D $pg_data/main/" &> /dev/null
    # remise en place des certificats
    # postgres : se mettre en mode sans mot de passe
    echo -e -e "local\tall\tall\ttrust" > $pg_perms
    chown postgres.postgres $pg_perms
    chmod 600 $pg_perms
    mv -f /tmp/postgresql.conf $pg_dir/postgresql.conf
    chown postgres.postgres $pg_dir/postgresql.conf
    chmod 600 $pg_dir/postgresql.conf
    CreoleService $pg_service start &> /dev/null
    sleep 2
    echo -e " - injection des données"
    su postgres -c "psql template1 -f backup_db.sql" > /dev/null 2>&1
    # recréation du mot de passe
    echo -e " - régénération du mot de passe"
    /usr/share/eole/gen_pwd.py &> /dev/null
    echo -e " - mise à jour du schéma de la base"
    su postgres -c "psql -d zephir -U zephir -f /usr/share/zephir/sql/maj.sql" > /dev/null 2>&1
    # postgres : se remettre en mode mot de passe
    rm -f $pg_perms
    mv -f /tmp/pg_hba.savezephir $pg_perms
    chown postgres.postgres $pg_perms
    chmod 600 $pg_perms
    CreoleService $pg_service stop &> /dev/null
    CreoleService $pg_service start &> /dev/null
fi

# restauration des répertoires et fichiers importants
# pushd /var/lib > /dev/null 2>&1

echo -e " - base LDAP"
# restauration de la base ldap
CreoleService slapd stop >/dev/null 2>&1
rm -rf /var/lib/ldap/*.*
# envoi du fichier ldap.ldif dans le conteneur annuaire
container_path_annuaire=$(CreoleGet container_path_annuaire)
cp -r ldap.ldif $container_path_annuaire/tmp/ >/dev/null 2>&1
CreoleRun "slapadd -f /etc/ldap/slapd.conf -l /tmp/ldap.ldif >/dev/null 2>&1" annuaire
rm -f $container_path_annuaire/tmp/ldap.ldif >/dev/null 2>&1
CreoleRun "chown -R openldap.openldap /var/lib/ldap" annuaire
CreoleService slapd start >/dev/null 2>&1
echo -e " - configuration des serveurs"
# /var/lib/zephir/
rm -rf /var/lib/zephir
tar -C /var/lib/ -xpf var_lib_zephir.tar >/dev/null 2>&1
# /etc/uucp/
rm -rf /etc/uucp
tar -C /etc/ -xpf etc_uucp.tar --no-overwrite-dir >/dev/null 2>&1
# /var/spool/uucp/
rm -rf /var/spool/uucp
tar -C /var/spool -xpf var_spool_uucp.tar --no-overwrite-dir >/dev/null 2>&1
echo -e " - dictionnaires personnalisés"
rm -rf /usr/share/zephir/dictionnaires/*/local
tar -C /usr/share/zephir/dictionnaires -xpf usr_dictionnaires.tar 2>&1
if [ -f usr_default_modules.tar ]
then
    echo -e " - modules personnalisés"
    tar -C /usr/share/zephir/default_modules -xpf usr_default_modules.tar 2>&1
fi

# /usr/share/creole/funcs (/func_creole3)
if [ -f creole_funcs.tar ]
then
    mkdir creole_funcs
    tar -C creole_funcs -xpf creole_funcs.tar >/dev/null 2>&1
    ls creole_funcs/creole2/*.py >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        echo -e " - fonctions CREOLE personnalisées (2.0 à 2.3)"
        mkdir -p /usr/share/creole/funcs_creole2
        cp -f creole_funcs/creole2/*.py /usr/share/creole/funcs_creole2/
    fi
    ls creole_funcs/creole3/*.py >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        echo -e " - fonctions CREOLE personnalisées (2.4 et >)"
        mkdir -p /usr/share/creole/funcs
        cp -f creole_funcs/creole3/*.py /usr/share/creole/funcs/
    fi
fi

# configurations diverses (EAD/certificats/sso/...)
if  [ -f config.tar ]
then
    echo -e " - configuration EAD"
    tar -xpf config.tar >/dev/null 2>&1
    ead_config=/usr/share/ead2/backend/config/
    [ -f "config/perm_local.ini" ] && cp -f "config/perm_local.ini" "$ead_config"
    [ -f "config/roles_local.ini" ] && cp -f "config/roles_local.ini" "$ead_config"
    cp -f "config/bp_server.conf" /var/lib/eole/config/ 2>/dev/null
    # désactivation de la maj hebdomadaire
    if [ -f "config/cron.txt" ] && [ $(cat "config/cron.txt" | wc -w) -eq 0 ];then
        /usr/share/eole/schedule/manage_schedule post majauto weekly del >/dev/null
    fi
    cp -f "config/dhcp.conf" /var/lib/eole/config/dhcp.conf 2>/dev/null
fi
if  [ -f ssl.tar ]
then
    echo -e " - certificats SSL"
    tar -C /etc/ssl/ -xpf ssl.tar --no-overwrite-dir >/dev/null 2>&1
fi
if  [ -f sso.tar ]
then
    echo -e " - configuration EoleSSO"
    tar -C /usr/share/sso/ -xpf sso.tar >/dev/null 2>&1
fi

echo -e " - configuration EOLE"
apply_patches="o"
# on regarde si des fichiers de personnalisation existent (hors config.eol et répertoires vides)
nb_files=`tar -tf etc_eole.tar | grep -v "^config.eol$" | grep -v ".*/$" | wc -l`
if [ "$ARCHIVE_VERSION" == "2.3" -a "$nb_files" != "0" ]
then
    echo -e
    # migration des données d'un serveur 2.3
    EchoOrange "Voulez vous appliquer les adaptations faites sur la version 2.3"
    EchoOrange "(patchs, dictionnaires et templates locaux du serveur Zéphir)"
    EchoOrange "Attention, ces modifications peuvent être non fonctionnalles sur Zéphir 2.5"
    read -p "Restaurer les données (o/n) ? " apply_patches
fi
if [ "$apply_patches" == "o" ] || [ "$apply_patches" == "O" ]
then
    rm -rf /usr/share/eole/creole/dicos/local /usr/share/eole/creole/dicos/variante /usr/share/eole/creole/patch
    tar -C /usr/share/eole/creole -xpf etc_eole.tar >/dev/null 2>&1
fi
mv /etc/eole/config.eol /etc/eole/config.old >/dev/null 2>&1
mv -f /usr/share/eole/creole/config.eol /etc/eole/config.eol >/dev/null 2>&1
# vérification du dictionnaire
echo -e
if [ "$ARCHIVE_VERSION" == "2.3" ]
then
    EchoOrange "La configuration actuelle a été sauvegardée sous /etc/eole/config.old"
    EchoOrange "Pour reprendre cette version, copier ce fichier sur /etc/eole/config.eol"
    echo -e
    EchoCyan "Lancez gen_config pour migrer la configuration (2.3) de l'archive"
    EchoCyan "Puis Utilisez la commande 'instance' sans écraser la base de données"
else
    diff -q /etc/eole/config.old /etc/eole/config.eol &>/dev/null
    if [ ! $? -eq 0 ]
    then
        EchoOrange "Le fichier /etc/eole/config.eol est différent de la version restaurée"
        EchoOrange "Le fichier présent avant restauration a été sauvegardé sous /etc/eole/config.old"
        EchoOrange "Pour reprendre cette version, copier ce fichier sur /etc/eole/config.eol"
        echo -e
        EchoCyan "Utilisez la commande instance sans écraser la base de données"
        echo -e
    else
        echo -e
        EchoCyan "Reconfigurez le serveur après la fin de la restauration"
        echo -e
    fi
fi

echo -e
rm -rf $backup_dir/$basename
EchoVert "Système restauré"
echo -e
# retour au répertoire d'origine
popd >/dev/null 2>&1
