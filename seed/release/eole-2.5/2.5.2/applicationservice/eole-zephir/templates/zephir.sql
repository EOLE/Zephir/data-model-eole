/****************************************************/
/*              tables annexes                      */
/****************************************************/

/*-- types d etablissements (college, lycee...) */
create table types_etab (
  id		serial		PRIMARY KEY,
  libelle	varchar(80)	not null
);

/*-- modules disponibles */
create table modules (
  id		serial		PRIMARY KEY,
  libelle	varchar(40)	not null,
  version	integer		default 2
);

/*-- services disponibles selon le module */
create table services (
  id		serial		PRIMARY KEY,
  module	integer		not null,
  libelle	varchar(40)	not null,
  FOREIGN KEY(module) REFERENCES modules(id) ON DELETE CASCADE
);

/*-- variantes disponibles selon le module */
create table variantes (
  id			serial		PRIMARY KEY,
  module		integer		not null,
  libelle		varchar(40)	not null,
  owner			varchar(30)	,
  passmd5		varchar(50)	,
  FOREIGN KEY(module) REFERENCES modules(id)
);

/****************************************************/
/*              tables principales                  */
/****************************************************/

/*-- etablissements */
/*-- type en dernier pour gestion specifique */
create table etablissements (
  rne			varchar(8)	PRIMARY KEY,
  libelle		varchar(200)	not null,
  adresse		varchar(100),
  tel			varchar(20),
  fax			varchar(20),
  mail			varchar(100),
  responsable		varchar(30),
  remarques		text,
  type			integer		not null,
  ville			varchar(50)	not null,
  cp			varchar(5)	not null,
  FOREIGN KEY(type) REFERENCES types_etab(id)
);

/*-- serveurs installes */
create table serveurs (
  id			serial		PRIMARY KEY,
  rne			varchar(8)	not null,
  libelle		varchar(150)	not null,
  materiel		text,
  processeur		varchar(100),
  disque_dur		varchar(100),
  date_install		date		not null,
  installateur		varchar(50)	not null,
  tel			varchar(20),
  remarques		text,
  module_initial	integer		not null,
  module_actuel		integer		not null,
  variante		integer		not null,
  timestamp		varchar(20)	not null,
  timeout		integer,
  etat			integer,
  last_contact		varchar(20),
  params		text,
  maj			integer,
  md5s			integer,
  ip_publique		varchar(20),
  no_alert		integer		default 0,
  inactive_packages	text,
  FOREIGN KEY(rne) REFERENCES etablissements(rne),
  FOREIGN KEY(module_initial) REFERENCES modules(id),
  FOREIGN KEY(module_actuel) REFERENCES modules(id),
  FOREIGN KEY(variante) REFERENCES variantes(id)
);

/*-- groupes de droits */
create table groupes_droits (
  id		serial		PRIMARY KEY,
  libelle	varchar(100)	not null,
  droits	text		not null
);

/*-- groupes de serveurs */
create table groupes_serveurs (
  id		serial		PRIMARY KEY,
  libelle	varchar(50)	not null,
  serveurs	text		not null
);


/*-- services installes */
create table services_installes (
  id_serveur	integer,
  id_service	integer,
  FOREIGN KEY(id_serveur) REFERENCES serveurs(id) ON DELETE CASCADE,
  FOREIGN KEY(id_service) REFERENCES services(id),
  PRIMARY KEY(id_serveur,id_service)
);

/*****************************/
/* table de log des serveurs */
/*****************************/

create table log_serveur (
  id		serial		PRIMARY KEY,
  id_serveur	integer		not null,
  date		timestamp	not null,
  type		varchar(20),
  message	text		not null,
  etat		integer		not null,
  FOREIGN KEY(id_serveur) REFERENCES serveurs(id) ON DELETE CASCADE
);

/**********************************************************************/
/* version light de la table de log (seulement un log de chaque type) */
/**********************************************************************/

create table last_log_serveur (
  id		serial		PRIMARY KEY,
  id_serveur	integer		not null,
  date		timestamp	not null,
  type		varchar(20),
  message	text		not null,
  etat		integer		not null,
  FOREIGN KEY(id_serveur) REFERENCES serveurs(id) ON DELETE CASCADE
);

/**************************/
/* table des utilisateurs */
/**************************/

create table users (
  login			varchar(100)		PRIMARY KEY,
  mail			varchar(100),
  nom			varchar(100),
  prenom		varchar(40),
  sms			varchar(20),
  mail_actif		integer			not null,
  sms_actif		integer			not null,
  droits		text			not null,
  groupes		text,
  cle			text
);

/*****************************************/
/* table d'autorisation de connexion ssh */
/*****************************************/
create table serveur_auth (
  id_serveur	integer,
  login			varchar(40),
  FOREIGN KEY(id_serveur)	REFERENCES serveurs(id) ON DELETE CASCADE,
  FOREIGN KEY(login)		REFERENCES users(login) ON DELETE CASCADE,
  PRIMARY KEY(id_serveur,login)
);


/*************************/
/* table de log generale */
/*************************/
create table log_zephir (
  id		serial		PRIMARY KEY,
  id_user	varchar(20),
  date		timestamp	not null,
  type		varchar(20),
  message	text		not null
);


/*********************************/
/* tables de procedures et locks */
/*********************************/

create table procedures (
  tag		varchar(20)	PRIMARY KEY,
  libelle	varchar(100)
);

create table lock_serveur (
  id_serveur	integer		not null,
  tag		varchar(20)	not null,
  FOREIGN KEY(id_serveur) REFERENCES serveurs(id) ON DELETE CASCADE,
  FOREIGN KEY(tag) REFERENCES procedures(tag) ON DELETE CASCADE
);


/****************************/
/* tables de gestion du vpn */
/****************************/

create table conf_vpn (
  id_sphynx     integer not null,
  id_amon       integer not null,
  etat          integer,
  FOREIGN KEY(id_sphynx)        REFERENCES serveurs(id) ON DELETE CASCADE,
  FOREIGN KEY(id_amon)          REFERENCES serveurs(id) ON DELETE CASCADE,
  PRIMARY KEY(id_sphynx,id_amon)
);

/***********************************************/
/* Table de restriction d'acces aux ressources */
/***********************************************/

create table restrictions (
  login		varchar(40)	not null,
  type_res	varchar(20)	not null,
  id_res	varchar(50)	not null,
  FOREIGN KEY(login) REFERENCES users(login) ON DELETE CASCADE
);

/*********************************************************************/
/* Tables de gestion des dictionnaires par module/variante/serveur   */
/* dict_type : eole / local                                          */
/*                                                                   */
/* rqe: les dictionnaires de base d'un module sont listés dans le    */
/* fichier d'activation du module                                    */
/* (/usr/share/zephir/default_module/version/nom_module)             */
/*                                                                   */
/* en cas de maj des dictionnaires, on supprime tous les liens au    */
/* niveau des modules et on recrée les liens au démarrage du backend */
/*********************************************************************/

create table dict_module (
  id_resource 	integer		not null,
  dict_type	varchar(10)	default 'local',
  dict_name	varchar(256)	not null,
  FOREIGN KEY(id_resource) REFERENCES modules(id) on DELETE CASCADE,
  PRIMARY KEY(id_resource, dict_type, dict_name)
);

create table dict_variante (
  id_resource 	integer		not null,
  dict_type	varchar(10)	default 'local',
  dict_name	varchar(256)	not null,
  FOREIGN KEY(id_resource) REFERENCES variantes(id) on DELETE CASCADE,
  PRIMARY KEY(id_resource, dict_type, dict_name)
);

create table dict_serveur (
  id_resource	integer		not null,
  dict_type	varchar(10)	default 'local',
  dict_name	varchar(256)	not null,
  FOREIGN KEY(id_resource) REFERENCES serveurs(id) on DELETE CASCADE,
  PRIMARY KEY(id_resource, dict_type, dict_name)
);

/*****************************************************************************/
/* Table de correspondance des variantes copiées à l'upgrade de distribution */
/*****************************************************************************/

create table migration_variantes (
  id_source	integer		not null,
  id_dest	integer		not null,
  FOREIGN KEY(id_source) REFERENCES variantes(id) ON DELETE CASCADE,
  FOREIGN KEY(id_dest) REFERENCES variantes(id) ON DELETE CASCADE
);

/********************************/
/* Table des tâches programmées */
/********************************/

create table tasks (
  id_task	serial			PRIMARY KEY,
  name		varchar(100)	not null,
  cmd		text		not null,
  month		integer			,
  day		integer			,
  hour		integer			,
  min		integer			,
  week_day	integer			,
  periodicity	integer			default 0,
  exec_date	timestamp		default null,
  CHECK (hour < 25),
  CHECK (min < 61),
  CHECK (month < 13),
  CHECK (day < 32),
  CHECK (week_day < 7)
);

/*****************************************/
/* Table de liaison tâche/serveur-groupe */
/*****************************************/

create table task_targets (
  id_res	integer		not null,
  type_res	varchar(20)	not null,
  id_task   integer     not null,
  FOREIGN KEY(id_task) REFERENCES tasks(id_task) ON DELETE CASCADE,
  PRIMARY KEY(id_res, type_res, id_task)
);

create table ent_id_ranges (
  code_ent	char(2)		not null,
  serveur	integer		not null,
  date_valid	timestamp	not null,
  min		integer		not null,
  max		integer		not null,
  PRIMARY KEY(code_ent, min, max)
);

create table import_aaf (
  fichier	varchar(50)	PRIMARY KEY,
  id_serveur	integer		not null,
  hash		char(64)	,
  FOREIGN KEY(id_serveur) REFERENCES serveurs(id) ON DELETE CASCADE
);

/**********************************************/
/* insertion des groupes de droits par defaut */
/**********************************************/
insert into groupes_droits values (1,'Lecture', '[''aaf.get_list'',''etabs.get_types'',''etabs.get_etab'',''etabs.get_libelle_etab'',''etabs.rech_etab'',''services.get_service_installe'',''services.get_service'',''services.get_service_module'',''serveurs.fichiers_zephir'',''serveurs.get_serveur_perms'',''modules.get_variante_perms'',''serveurs.global_status'',''serveurs.get_dico'',''serveurs.get_config'',''serveurs.get_serveur'',''serveurs.creole_version'',''serveurs.module_version'',''serveurs.get_log'',''serveurs.serveurs_etab'',''serveurs.agents_status'',''serveurs.get_measure'',''serveurs.get_status'',''serveurs.groupe_serveur'',''serveurs.get_conf_uucp'',''serveurs.get_file_content'',''modules.get_module'',''modules.get_mod_dict'',''modules.get_vars'',''modules.get_variante'',''modules.fichiers_variante'',''modules.get_var_file'',''modules.get_dico'',''modules.modules_distrib'',''uucp.get_checksum'',''serveurs.get_groupes'',''get_user'',''serveurs.get_groups'',''get_allowed_servers'',''get_permissions'',''get_restrictions'',''user_group'',''edit_user'',''get_stats'',''serveurs.groupe_reload'',''save_key'',''serveurs.get_bastion'',''serveurs.save_group'',''serveurs.del_group'',''serveurs.edit_group'',''serveurs.check_backup'',''serveurs.check_groupes'',''serveurs.check_serveurs'',''serveurs.get_locks'',''serveurs.get_alertes'',''serveurs.get_migration_status'',''maj_client'',''uucp.get_actions'',''serveurs.get_maj_infos'',''serveurs.check_min_version'',''serveurs.variante_migration'',''uucp.sphynx_list'',''uucp.check_replication'',''list_client'',''scheduler.get_tasks'',''sentinelle.get_all'',''sentinelle.get_all_conf'',''sentinelle.get_all_for_sentinelle'',''sentinelle.get_serveur_etab'',''prelude.gen_certif'',''dicos.get_dict_resources'',''dicos.get_dict_links'',''dicos.get_paq_dict'',''dicos.get_module_defaults'',''dicos.list_local_serveur'',''dicos.list_module'',''dicos.list_variante'',''dicos.list_serveur'',''dicos.list_available'',''dicos.check_module'',''dicos.check_serveur'',''dicos.managed_modules'',''dicos.update_dicts'',''dicos.get_dict'',''dicos.get_inactive'',''dicos.check_inactive'']');
insert into groupes_droits values (2,'Ecriture', '[''etabs.edit_etab'',''etabs.del_etab'',''etabs.add_etab'',''etabs.import_etab'',''etabs.add_type'',''etabs.del_type'',''services.del_service_installe'',''services.add_service'',''services.del_service'',''services.add_service_installe'',''services.edit_service'',''serveurs.del_serveur'',''serveurs.conf_ssh'',''serveurs.edit_serveur'',''serveurs.add_serveur'',''serveurs.del_log'',''serveurs.add_files'',''serveurs.set_serveur_perms'',''serveurs.del_serveur_perms'',''modules.set_variante_perms'',''modules.del_variante_perms'',''serveurs.del_files'',''serveurs.groupe_extend'',''modules.del_module'',''modules.del_mod_dict'',''modules.del_variante'',''modules.add_variante'',''modules.copy_variante'',''modules.edit_module'',''modules.save_dico'',''modules.edit_variante'',''modules.add_module'',''modules.add_files'',''modules.del_files'',''modules.import_variante'',''modules.get_migration_infos'',''modules.variantes_upgrade'',''modules.upgrade_modules'',''prelude.get_managers'',''prelude.register_server'',''prelude.register_logger'',''serveurs.migrate_serveur'',''serveurs.migrate_data'',''serveurs.migrate_conf'',''serveurs.revert_migration'',''serveurs.regen_key'',''serveurs.purge_key'',''serveurs.download_upgrade'',''serveurs.download_upgrade_groupe'',''prelude.gen_certif'',''dicos.add_dict'',''dicos.remove_dict'',''dicos.add_module'',''dicos.add_variante'',''dicos.add_serveur'',''dicos.del_module'',''dicos.del_variante'',''dicos.del_serveur'']');
insert into groupes_droits values (3,'Configuration et actions sur les serveurs', '[''serveurs.save_conf'',''serveurs.save_bastion'',''serveurs.save_bastion_groupe'',''serveurs.copy_conf'',''serveurs.copy_perms'',''serveurs.set_groupe_var'',''serveurs.groupe_params'',''serveurs.get_groupe_vars'',''serveurs.maj_locks'',''uucp.reconfigure'',''uucp.reconfigure_groupe'',''uucp.install_variante'',''uucp.configure'',''uucp.confirm_transfer'',''uucp.configure_groupe'',''uucp.service_restart'',''uucp.service_restart_groupe'',''uucp.save_conf_groupe'',''uucp.log_serveur'',''uucp.maj_site'',''uucp.maj_groupe'',''uucp.reboot_groupe'',''uucp.reboot'',''uucp.release_lock'',''uucp.release_lock_groupe'',''uucp.save_files'',''uucp.install_module'',''uucp.maj'',''uucp.save_conf'',''uucp.purge_actions'',''uucp.maj_client'',''uucp.maj_client_groupe'',''uucp.exec_script'',''scheduler.add_single'',''scheduler.add_loop'',''scheduler.add_daily'',''scheduler.add_weekly'',''scheduler.add_monthly'',''scheduler.del_task'',''scheduler.assign_task'',''scheduler.unassign_task'',''dicos.add_dict'',''dicos.remove_dict'',''dicos.add_serveur'',''dicos.del_serveur'']');
insert into groupes_droits values (4,'Gestion des permissions', '[''get_permissions'',''save_permissions'',''get_user'',''get_rights'',''user_group'',''serveurs.authorize_user'',''serveurs.deny_user'',''get_restrictions'',''add_restriction'',''del_restriction'',''serveurs.save_group'',''modules.add_module'',''modules.update_modules'',''modules.edit_module'',''modules.get_migration_infos'',''modules.variantes_upgrade'',''modules.edit_variante'',''etabs.add_type'',''uucp.install_module'',''del_user'',''list_users'',''update_client'',''remove_client'']');
insert into groupes_droits values (5,'Fonction des clients','[''aaf.confirm_transfer'',''aaf.get_list'',''uucp.confirm_transfer'',''uucp.get_checksum'',''uucp.maj_site'',''uucp.log_serveur'',''uucp.save_files'',''uucp.scan_timeouts'',''uucp.unlock'',''serveurs.get_locks'',''maj_client'',''serveurs.get_timeout'',''uucp.check_queue'',''serveurs.update_key'',''serveurs.get_key'',''entid.get_id_range'',''entid.validate_id_range'',''prelude.get_rsyslog_conf'',''dicos.check_serveur'']');
insert into groupes_droits values (6,'Export de variantes','[''modules.export_variante'',''modules.get_variante'']');
insert into groupes_droits values (7,'Configuration vpn','[''uucp.sphynx_add'',''uucp.sphynx_del'',''uucp.sphynx_get'']');
insert into groupes_droits values (8,'Enregistrement', '[''get_permissions'',''maj_client'',''serveurs.get_status'',''serveurs.serveurs_etab'',''serveurs.get_serveur'',''serveurs.get_conf_uucp'',''etabs.get_etab'',''modules.get_module'',''modules.get_variante'',''modules.modules_distrib'',''uucp.configure'']');
insert into groupes_droits values (9,'Ajout/Modification de serveur (enregistrement)', '[''serveurs.add_serveur'',''serveurs.edit_serveur'']');
/* XXX FIXME: fonctions prelude : à intégrer dans enregistrement ?? */
insert into groupes_droits values (10,'Enregistrement des sondes prelude', '[''prelude.register_server'',''prelude.get_managers'',''prelude.register_logger'']');
insert into groupes_droits values (11,'Migration de serveur (enregistrement)', '[''serveurs.migrate_serveur'',''serveurs.migrate_data'',''serveurs.add_serveur'']');
insert into groupes_droits values (12,'Gestion des identifiants ENT','[''entid.get_id_pools'',''entid.get_code_ent'',''entid.reserve_range'']');
insert into groupes_droits values (13,'Gestion de la réplication LDAP','[''uucp.add_replication'',''uucp.del_replication'',''uucp.get_replication'',''uucp.update_replication'',''uucp.get_replication_info'']');
insert into groupes_droits values (14,'Gestion de la synchronisation AAF','[''aaf.get_list'',''aaf.add_file'',''aaf.del_file'',''aaf.notify_upload'']');
/* droits d'ecriture limités */
insert into groupes_droits values (21,'Ecriture (serveurs)', '[''serveurs.del_serveur'',''serveurs.conf_ssh'',''serveurs.edit_serveur'',''serveurs.add_serveur'',''serveurs.del_log'',''serveurs.add_files'',''serveurs.del_files'',''serveurs.set_serveur_perms'',''serveurs.del_serveur_perms'',''serveurs.migrate_serveur'',''serveurs.migrate_data'',''serveurs.migrate_conf'',''serveurs.revert_migration'',''serveurs.groupe_extend'',''serveurs.regen_key'',''serveurs.purge_key'',''serveurs.download_upgrade'',''serveurs.download_upgrade_groupe'',''dicos.add_serveur'',''dicos.del_serveur'',''dicos.add_dict'',''dicos.remove_dict'']');
insert into groupes_droits values (22,'Ecriture (modules)', '[''modules.del_module'',''modules.del_mod_dict'',''modules.del_variante'',''modules.add_variante'',''modules.copy_variante'',''modules.edit_module'',''modules.edit_variante'',''modules.add_module'',''modules.add_files'',''modules.del_files'',''modules.import_variante'',''modules.get_migration_infos'',''modules.variantes_upgrade'',''modules.upgrade_modules'',''modules.save_dico'',''modules.set_variante_perms'',''modules.del_variante_perms'',''dicos.add_module'',''dicos.add_variante'',''dicos.del_module'',''dicos.del_variante'',''dicos.add_dict'',''dicos.remove_dict'']');
insert into groupes_droits values (23,'Ecriture (etablissements)', '[''etabs.edit_etab'',''etabs.del_etab'',''etabs.add_etab'',''etabs.import_etab'',''etabs.add_type'',''etabs.del_type'']');
/* actions sans modification de configuration */
insert into groupes_droits values (31,'Actions sans modification de configuration', '[''uucp.reconfigure'',''uucp.reconfigure_groupe'',''uucp.configure'',''uucp.confirm_transfer'',''uucp.configure_groupe'',''uucp.service_restart'',''uucp.service_restart_groupe'',''uucp.log_serveur'',''uucp.maj_groupe'',''uucp.reboot_groupe'',  ''uucp.reboot'',''uucp.release_lock'',''uucp.release_lock_groupe'',''uucp.maj'',''uucp.purge_actions'',''uucp.maj_client'',''uucp.maj_client_groupe'']');
/* modification du mot de passe utilisateur */
insert into groupes_droits values (40,'Mise à jour du mot de passe (annuaire local)','[''update_passwd'']');

/**********************************************/
/* insertion des utilisateurs par defaut      */
/**********************************************/

insert into users (login,mail_actif,sms_actif,droits) values ('%%admin_zephir',0,0,'[1,4]');
insert into users (login,mail_actif,sms_actif,droits) values ('zephir',0,0,'[5]');

/**********************************************/
/* insertion des procedures par defaut        */
/**********************************************/

insert into procedures values ('MAJ','Mise à jour (réseau ou cd)');
insert into procedures values ('RECONFIGURE','Reconfiguration du serveur');
insert into procedures values ('INSTANCE','Instanciation du serveur');
insert into procedures values ('PERSO','scripts additionnels');


/*********************/
/* ajout des indexs */
/*********************/

create index log_id_index ON log_serveur(id_serveur);
create index last_log_type_index ON last_log_serveur(type);
create index last_log_date_index ON last_log_serveur(date);
create index serveurs_rne_index ON serveurs(rne);
create index serveurs_module_index ON serveurs(module_actuel);
create index serveur_etat_index ON serveurs(etat);
create index serveur_timeout_index ON serveurs(timeout);
create index variante_module_index ON variantes(module);
