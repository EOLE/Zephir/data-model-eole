<?php


$path="/var/www/html/grr";

include "$path/include/connect.inc.php";
include "$path/include/config.inc.php";
include "$path/include/misc.inc.php";
include "$path/include/functions.inc.php";
include "$path/include/$dbsys.inc.php";
$grr_script_name = "admin_maj.php";

// Settings
require_once("$path/include/settings.inc.php");
//Chargement des valeurs de la table settingS
if (!loadSettings())
    die("Erreur chargement settings");

// Session related functions
require_once("$path/include/session.inc.php");

// Paramètres langage
include "$path/include/language.inc.php";

function traite_requete($requete="") {
    $retour="";
    $res = mysql_query($requete);
    $erreur_no = mysql_errno();
    if (!$erreur_no) {
        $retour = "";
    } else {
        switch ($erreur_no) {
        case "1060":
        // le champ existe déja : pas de problème
        $retour = "";
        break;
        case "1061":
        // La cléf existe déja : pas de problème
        $retour = "";
        break;
        case "1062":
        // Présence d'un doublon : création de la cléf impossible
        $retour = "<span style=\"color:#FF0000;\">Erreur (<b>non critique</b>) sur la requête : <i>".$requete."</i> (".mysql_errno()." : ".mysql_error().")</span><br />\n";
        break;
        case "1068":
        // Des cléfs existent déja : pas de problème
        $retour = "";
        break;
        case "1091":
        // Déja supprimé : pas de problème
        $retour = "";
        break;
        default:
        $retour = "<span style=\"color:#FF0000;\">Erreur sur la requête : <i>".$requete."</i> (".mysql_errno()." : ".mysql_error().")</span><br />\n";
        break;
        }
    }
    return $retour;
}

// Si nécessaire, ajout automatique de l'auth CAS 
$req1 = grr_sql_query1("SELECT VALUE FROM ".TABLE_PREFIX."_setting WHERE NAME='sso_statut'");
if ($req1== -1) $result_inter = traite_requete("INSERT INTO ".TABLE_PREFIX."_setting VALUES ('sso_statut', 'cas_utilisateur');");
        

