#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2009
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# grr.py
#
# Configuration de base de données mysql de grr
#
###########################################################################
"""
    Config pour grr
"""
from os.path import isfile
from eolesql.config import dico_creole
GRR_TABLEFILENAMES = ['/usr/share/eole/applications/gen/0_eole-grr.sql']
def test():
    """test l'existence de la table "grr"
    """
    return not isfile('/var/lib/mysql/grr/grr_area.frm')

conf_dict = dict(filenames=GRR_TABLEFILENAMES,
                 test=test)

