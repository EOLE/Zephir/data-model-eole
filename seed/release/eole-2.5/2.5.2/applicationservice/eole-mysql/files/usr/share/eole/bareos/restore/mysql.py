#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module mysql"""
from os.path import join
from shutil import copyfile
from eolesql.db_cmd import dump_sql_file
import sys
from glob import glob
sys.path.append('/usr/share/eole/sbin')
from mysql_pwd import mysql_root_passwd, gen_random_passwds
from pyeole.bareosrestore import bareos_restore_one_folder, \
 bareos_restore_one_file, exit_if_running_jobs
from creole.client import CreoleClient

mysqlsavdir = '/home/backup/sql/'
container_path_mysql = CreoleClient().get_creole('container_path_mysql')
debian_cnf = '/etc/mysql/debian.cnf'

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """ldap helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)

def pre():
    print "pre mysql"

def post():
    print "post mysql"
    password = gen_random_passwds()[0]
    # utilisation du debian.cnf d'origine
    mysql_root_passwd(password, debian_cnf+'.bak')
    mysql_bdd = join(mysqlsavdir, 'mysql.sql')
    for files in glob("{0}/*.sql".format(mysqlsavdir)):
        if files == mysql_bdd:
            continue
        print "restauration de {0}".format(files)
        dump_sql_file(password, files)
    # bdd mysql restaurée en dernier
    print "restauration de {0}".format(mysql_bdd)
    dump_sql_file(password, mysql_bdd)

def job(jobid):
    print "Restauration mysql"
    # sauvegarde de l'actuel fichier debian.cnf
    copyfile(container_path_mysql+debian_cnf, container_path_mysql+debian_cnf+'.bak')
    # restauration du fichier debian.cnf
    bareos_restore_one_file(container_path_mysql+debian_cnf, jobid)
    # restauration des dumps sql
    bareos_restore_one_folder(mysqlsavdir, jobid)

priority = 30
