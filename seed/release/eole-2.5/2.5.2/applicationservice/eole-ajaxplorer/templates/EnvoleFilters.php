<?php
function UserFolderftpCas(&$value){
    if(AuthService::getLoggedUser() != null){

        $loggedUser = AuthService::getLoggedUser();

        // Répertoire par défaut d'un utilisateur de scribe
        $user_folder = "/home/". substr($loggedUser->getId(), 0, 1) . '/' . $loggedUser->getId() .'/';
        
        // C'est peut-être un horus
        if (! is_dir($user_folder)) 
            $user_folder = "/home/".$loggedUser->getId() .'/';
        
        // Sinon on est en amonecole donc normal que l'on ne voit pas le rep = remettre l'initiale
        if (! is_dir($user_folder)) // C'est peut-être un horus ou un amonecole
            $user_folder = "/home/". substr($loggedUser->getId(), 0, 1) . '/' . $loggedUser->getId() .'/';
        
        $value = str_replace("USER_FTPCAS_PATH", $user_folder.".ftp/", $value);
    }
    else {
        $value = str_replace("USER_FTPCAS_PATH", "", $value);
    }
}
?>
