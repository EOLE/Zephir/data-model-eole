<?php
/**
 * service to access
 */
%if %%ajaxplorer_ftp != '127.0.0.1'
define('__FTP_SERVICE', 'ftp://%%ajaxplorer_ftp');
%else
define('__FTP_SERVICE', 'ftp://%%adresse_ip_eth0');
%end if

define('__CAS_URL', '');

define('__LOG_FILE', '/var/log/posh/ajaxplorer-cas.log');

/**
 * if gateway =true, CAS authentication will not be forced
 * (standard authentication will proceed)
 */
define('__GATEWAY', false);
?>
