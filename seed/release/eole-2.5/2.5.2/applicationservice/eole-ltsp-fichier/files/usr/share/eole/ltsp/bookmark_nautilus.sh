#!/bin/bash

SHARE_DIRS_NAME="Partages"
FTP_DIR=`echo $HOME| sed 's/perso/.ftp/'`
EXCLUDE="perso"

if [ ! -d ${HOME}/${SHARE_DIRS_NAME} ];then
	mkdir ${HOME}/${SHARE_DIRS_NAME}
fi

for d in `ls ${FTP_DIR}`;do
	if [ ${d} != ${EXCLUDE} ] && [ ! -h ${HOME}/$SHARE_DIRS_NAME/${d} ];then
		ln -s ${FTP_DIR}/$d ${HOME}/${SHARE_DIRS_NAME}/${d}
	fi
done
grep ${HOME}/${SHARE_DIRS_NAME} ${HOME}/.gtk-bookmarks 2>&1 > /dev/null
if [ $? != 0 ];then
	echo "file:///${HOME}/${SHARE_DIRS_NAME}" >> ${HOME}/.gtk-bookmarks
fi
