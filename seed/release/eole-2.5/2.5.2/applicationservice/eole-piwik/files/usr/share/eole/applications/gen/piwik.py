#-*-coding:utf-8-*-
###########################################################################
#
# piwik.py
#
###########################################################################
"""
    Configuration pour la création de la base de données de piwik
"""
from eolesql.db_test import db_exists, test_var

piwik_TABLEFILENAMES = ['/usr/share/eole/mysql/piwik/gen/piwik-create-0.sql']

def test():
    """
        test l'existence de la base de donnée piwik
    """
    return test_var('activer_piwik') and not db_exists('piwik')

conf_dict = dict(filenames=piwik_TABLEFILENAMES,
                 test=test)

