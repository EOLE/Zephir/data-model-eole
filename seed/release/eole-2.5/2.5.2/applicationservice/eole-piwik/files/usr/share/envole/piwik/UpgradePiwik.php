#!/usr/bin/env php
<?php
// -*- coding: UTF-8 -*-

// Permettre de forcer l'affichage en direct lors de l'exécution : php UpgradePiwik.php -v
if(in_array("--help",$argv) || in_array("-h",$argv)) die("option unique pour obtenir l'affichage complet, absent par défaut : -v ou -V (verbose)\n");
elseif(in_array("-v",$argv) || in_array("-V",$argv)) $verbose = true;
else $verbose = false;

/***** Paramètres obligatoires et vérifiés lors de l'instanciation de la classe EnvoleTools **************/
// Paramètres généraux 
define('APPLI_NAME','Piwik');		// pour affichage
define('APPLI_MODULE','piwik'); 	// pour noms de fichiers ou répertoires
define('APPLI_DICO','activer_piwik'); 	// nom de la variable dans le gen_config
// Paramètres de connexion qui seront affectés à l'application (DB_PASS sera généré par la librairie) 
define('DB_NAME','piwik');
define('DB_USER','piwik');

/***** Vérifications avant d'executer que le contenu du paquet envole-php est bien installé **************/
// Classe EnvoleTools qui gère les opérations utiles à la mise à jour et est présente dans l'include_path du php.ini (/usr/share/php)
if(!is_file("/usr/share/php/envole-php/EnvoleTools.class.php"))
die("---- INSTALLATION IMPOSSIBLE ----\nCause : le fichier \"/usr/share/php/envole-php/EnvoleTools.class.php\" est introuvable. Réinstaller le paquet \"eole-envole-php\".\n\n");
require_once("envole-php/EnvoleTools.class.php");

/***** Démarrage de la mise à jour ************************************************************************/
//--> Création d'un objet EnvoleTools qui ouvre notamment le fichier de log et récupère certaines variables du Scribe
$tools = new EnvoleTools($verbose);

//--> Résolution des chemins nécessaires au travail de mise à jour
// Host de base de données : calculé par EnvoleTools qui détecte si le serveur SQL est contenu dans un container ou pas.
//define('DB_HOST', $tools->db_host);
// Chemin vers le container WEB
//define('ABSPATH', CONTAINER_PATH_WEB.'/var/www/html/'.APPLI_MODULE.'/');
//define('FILE_CONNEXION',ABSPATH.'config/config.ini.php');

//--> Début réel de la mise à jour
// NOTE : les mises à jour doivent être dans l'ordre chronologique afin qu'elles puissent être réalisées à la suite l'une de l'autre si plusieurs versions à modifier.
$tools->echo_titre("Début du script de mise à jour piwik");
$tools->setRows("SELECT `option_value` FROM `piwik_option` WHERE `option_name`='version_Installation'", DB_NAME);
if(count($tools->rows)==0) $tools->afficher("ATTENTION : version non identifiée mais aucune mise à jour possible, ce n'est pas normal !!!!!\n");
else
{
	/********************************************************************************************************************************************************/
	/* Vérification des versions non prises en charge par ce script												*/
	/********************************************************************************************************************************************************/
	// A chaque nouvel ajout de mise à jour, ajouter le nouveau numéro de version supporté par ce script
	if (	   $tools->rows[0]["option_value"]!="1.6"  
		&& $tools->rows[0]["option_value"]!="1.12"
		//&& $tools->rows[0]["option_value"]!="1.xx"
		) $tools->afficher("ATTENTION : version identifiée (" . $tools->rows[0]["option_value"] . ") mais aucune mise à jour disponible, ce n'est pas normal !!!!!\n");

	/********************************************************************************************************************************************************/
	/* Affiche le message si la base est bien à jour et assure une sauvegarde avant maj sinon								*/
	/********************************************************************************************************************************************************/
	// Toujours placer ici le dernier numéro de version
	if ($tools->rows[0]["option_value"]=="1.12") $tools->afficher("Piwik est déjà à jour.\n");

	/********************************************************************************************************************************************************/
	/* Mise à jour 1.6 vers 1.12																*/
	/********************************************************************************************************************************************************/
	// Il est indispensable de relire le numéro de version à chaque vérification si la mise à jour précédente est terminée
	$tools->setRows("SELECT `option_value` FROM `piwik_option` WHERE `option_name`='version_Installation'", DB_NAME);
	if(count($tools->rows)!=0 && $tools->rows[0]["option_value"]=="1.6") maj_16_vers_112();

	/********************************************************************************************************************************************************/
	/* Mise à jour 1.12 vers 1.xx																*/
	/********************************************************************************************************************************************************/
	/*// Il est indispensable de relire le numéro de version à chaque vérification si la mise à jour précédente est terminée
	$tools->setRows("SELECT `option_value` FROM `piwik_option` WHERE `option_name`='version_Installation')", DB_NAME);
	if(count($tools->rows)!=0 && $tools->rows[0]["option_value"]=="1.12") maj_112_vers_1xx();
	*/

}
$tools->echo_titre("fin du script de mise à jour piwik");
//--> L'execution se termine ici

/************************************************** Scripts de mise à jour ci-dessous ***************************************************************************/

/****************************************************************************************************************************************************************/
/* Prise en charge de la mise à jour de la version 1.6 à 1.12 de Piwik												*/
/****************************************************************************************************************************************************************/
function maj_16_vers_112()
{
	// Récupération de l'objet $tools
	global $tools;

	// Affichage
	$tools->echo_titre("Mise à jour de la version 1.6 à 1.12 de PIWIK");
	
	/********************************** Nettoyage préventif du create view et dump **************************************************************************/
	//--> Suppression de la vue qui n'est pas utile et pose problème lors du dump
	$tools->query("DROP TABLE IF EXISTS view_piwik_visit_action", DB_NAME);
	//--> Sauvegarde préventive de la base
	$tools->afficher("* : Sauvegarde de la base avant toute mise à jour\n");
	$tools->dump(DB_NAME);

	/********************************** Migration de structure de la base de données ************************************************************************/
	//--> Passe la base en UTF8
	$tools->afficher("0: Changement encodage de la base de données (pas des tables)\n");
	$tools->query("ALTER DATABASE `piwik` CHARSET=utf8", DB_NAME);
	//--> Ajout d'élèments temporaires destinés à manipuler les données
	$tools->afficher("1: Préparation de la table piwik_log_action à être reconvertie\n");
	// On renomme la table afin de réaliser plus loin la migration des données. 
	// Ce renommage est sécurisé : on peut le relancer sans crash si l'action a déjà été faite.
	// Si la table avait déjà été renommée tmp_piwik_log_action, et qu'elle existe, on ne recommence pas 
	renommerTable("piwik_log_action", "tmp_piwik_log_action");

	// Création de la nouvelle table destinée à récupérer les données de la table tmp_piwik_log_action
	$tools->query("DROP TABLE IF EXISTS `piwik_log_action`", DB_NAME);
	$tools->query("CREATE TABLE `piwik_log_action` (
	  	`idaction` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  	`name` text,
		  `hash` int(10) unsigned NOT NULL,
		  `type` tinyint(3) unsigned DEFAULT NULL,
		  `url_prefix` tinyint(2) DEFAULT NULL,
		  PRIMARY KEY (`idaction`),
		  KEY `index_type_hash` (`type`,`hash`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");

	//--> Modification des élèments existants
	$tools->afficher("2: Modification des champs dans les tables\n");
	$tools->query("ALTER TABLE `piwik_log_link_visit_action` MODIFY COLUMN `idlink_va` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
							MODIFY COLUMN `idaction_url` INT(10) UNSIGNED DEFAULT NULL NULL, 
							MODIFY COLUMN `idaction_url_ref` INT(10) UNSIGNED DEFAULT 0 NULL", DB_NAME);

	$tools->query("ALTER TABLE `piwik_log_visit` MODIFY COLUMN `visit_exit_idaction_url` INT(11) UNSIGNED DEFAULT 0 NULL", DB_NAME);

	$tools->query("ALTER TABLE `piwik_site` MODIFY COLUMN `excluded_parameters` TEXT NOT NULL", DB_NAME);

	//--> Ajout des champs supplémentaires (avec recréation si déjà existant)
	$tools->afficher("3a: Ajout des champs supplémentaires dans les tables (si pas déjà fait)\n");
	ajouterColonne("piwik_log_conversion", "location_region", "CHAR(2) DEFAULT NULL NULL COMMENT '' AFTER `location_country`");
	ajouterColonne("piwik_log_conversion", "location_city", "VARCHAR(255) DEFAULT NULL NULL COMMENT '' AFTER location_region");
	ajouterColonne("piwik_log_conversion", "location_latitude", "FLOAT(10, 6) DEFAULT NULL NULL COMMENT '' AFTER location_city");
	ajouterColonne("piwik_log_conversion", "location_longitude", "FLOAT(10, 6) DEFAULT NULL NULL COMMENT '' AFTER location_latitude");

	// La table ci-dessous n'a pas besoin d'être altérée car elle est renommée préfixées tmp_ et recréées vides dans la nouvelle version
	//ajouterColonne("piwik_log_action","url_prefix", "TINYINT(2) DEFAULT NULL NULL COMMENT '' AFTER type");

	ajouterColonne("piwik_log_link_visit_action", "custom_float", "FLOAT DEFAULT NULL NULL COMMENT '' AFTER custom_var_v5");
	ajouterColonne("piwik_log_visit", "visit_total_searches", "SMALLINT(5) UNSIGNED NOT NULL COMMENT '' AFTER visit_total_actions");

	ajouterColonne("piwik_log_visit", "location_region", "CHAR(2) DEFAULT NULL NULL COMMENT '' AFTER location_country");
	ajouterColonne("piwik_log_visit", "location_city", "VARCHAR(255) DEFAULT NULL NULL COMMENT '' AFTER location_region");
	ajouterColonne("piwik_log_visit", "location_latitude", "FLOAT(10, 6) DEFAULT NULL NULL COMMENT '' AFTER location_city");
	ajouterColonne("piwik_log_visit", "location_longitude", "FLOAT(10, 6) DEFAULT NULL NULL COMMENT '' AFTER location_latitude");

	ajouterColonne("piwik_site", "excluded_user_agents", "TEXT NOT NULL COMMENT '' AFTER excluded_parameters");
	ajouterColonne("piwik_site", "sitesearch", "TINYINT(4) DEFAULT 1 NULL COMMENT '' AFTER ecommerce");
	ajouterColonne("piwik_site", "sitesearch_keyword_parameters", "TEXT NOT NULL COMMENT '' AFTER sitesearch");
	ajouterColonne("piwik_site", "sitesearch_category_parameters", "TEXT NOT NULL COMMENT '' AFTER sitesearch_keyword_parameters");
	ajouterColonne("piwik_site", "keep_url_fragment", "TINYINT(4) NOT NULL DEFAULT 0 COMMENT '' AFTER `group`");

	ajouterColonne("piwik_user_dashboard", "name", "VARCHAR(100) DEFAULT NULL NULL COMMENT '' AFTER iddashboard");

	//--> Ajout des champs supplémentaires pour l'augmentation des variables perso (avec recréation si déjà existant)
	$tools->afficher("3b: Ajout des champs supplémentaires pour passage à 10 variables personnalisées\n");
	ajouterColonne("piwik_log_visit", "custom_var_k6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v5`");
	ajouterColonne("piwik_log_visit", "custom_var_v6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k6`");
	ajouterColonne("piwik_log_visit", "custom_var_k7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v6`");
	ajouterColonne("piwik_log_visit", "custom_var_v7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k7`");
	ajouterColonne("piwik_log_visit", "custom_var_k8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v7`");
	ajouterColonne("piwik_log_visit", "custom_var_v8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k8`");
	ajouterColonne("piwik_log_visit", "custom_var_k9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v8`");
	ajouterColonne("piwik_log_visit", "custom_var_v9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k9`");
	ajouterColonne("piwik_log_visit", "custom_var_k10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v9`");
	ajouterColonne("piwik_log_visit", "custom_var_v10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k10`");

	ajouterColonne("piwik_log_link_visit_action", "custom_var_k6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v5`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_v6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k6`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_k7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v6`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_v7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k7`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_k8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v7`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_v8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k8`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_k9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v8`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_v9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k9`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_k10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v9`");
	ajouterColonne("piwik_log_link_visit_action", "custom_var_v10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k10`");

	ajouterColonne("piwik_log_conversion", "custom_var_k6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v5`");
	ajouterColonne("piwik_log_conversion", "custom_var_v6", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k6`");
	ajouterColonne("piwik_log_conversion", "custom_var_k7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v6`");
	ajouterColonne("piwik_log_conversion", "custom_var_v7", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k7`");
	ajouterColonne("piwik_log_conversion", "custom_var_k8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v7`");
	ajouterColonne("piwik_log_conversion", "custom_var_v8", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k8`");
	ajouterColonne("piwik_log_conversion", "custom_var_k9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v8`");
	ajouterColonne("piwik_log_conversion", "custom_var_v9", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k9`");
	ajouterColonne("piwik_log_conversion", "custom_var_k10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_v9`");
	ajouterColonne("piwik_log_conversion", "custom_var_v10", "VARCHAR(200) NULL DEFAULT NULL AFTER `custom_var_k10`");

	//--> Ajout des tables supplémentaires
	$tools->afficher("4: Ajout des tables supplémentaires (éffacement et recréation si existe)\n");
	$tools->query("DROP TABLE IF EXISTS piwik_report", DB_NAME);
	$tools->query("CREATE TABLE piwik_report (
		    idreport INT(11) AUTO_INCREMENT NOT NULL,
		    idsite INT(11) NOT NULL,
		    login VARCHAR(100) NOT NULL,
		    description VARCHAR(255) NOT NULL,
		    idsegment INT(11) DEFAULT NULL NULL,
		    period VARCHAR(10) NOT NULL,
		    hour TINYINT(4) NOT NULL DEFAULT 0,
		    type VARCHAR(10) NOT NULL,
		    format VARCHAR(10) NOT NULL,
		    reports TEXT NOT NULL,
		    parameters TEXT,
		    ts_created TIMESTAMP NULL DEFAULT NULL,
		    ts_last_sent TIMESTAMP NULL DEFAULT NULL,
		    deleted TINYINT(4) NOT NULL DEFAULT 0,
		    PRIMARY KEY (idreport)
		) ENGINE=MyISAM CHARACTER SET=utf8 AUTO_INCREMENT=1", DB_NAME);

	$tools->query("DROP TABLE IF EXISTS piwik_segment", DB_NAME);
	$tools->query("CREATE TABLE piwik_segment (
		    idsegment INT(11) AUTO_INCREMENT NOT NULL,
		    name VARCHAR(255) NOT NULL,
		    definition TEXT NOT NULL,
		    login VARCHAR(100) NOT NULL,
		    enable_all_users TINYINT(4) NOT NULL DEFAULT 0,
		    enable_only_idsite INT(11) DEFAULT NULL NULL,
		    auto_archive TINYINT(4) NOT NULL DEFAULT 0,
		    ts_created TIMESTAMP NULL DEFAULT NULL,
		    ts_last_edit TIMESTAMP NULL DEFAULT NULL,
		    deleted TINYINT(4) NOT NULL DEFAULT 0,
		    PRIMARY KEY (idsegment)
		) ENGINE=MyISAM CHARACTER SET=utf8 AUTO_INCREMENT=1", DB_NAME);

	/********************************** Migration des données la base de données ****************************************************************************/
	// ************ Manipulation des données simples (SQL)
	//--> Correction des entrées de la table piwik_option
	// Attention : il ne faut ni supprimer, ni modifier l'entrée 'version_Installation' avant la fin du script
	$tools->afficher("5: Modifications des données dans la table piwik_option\n");
	$tools->query("DELETE FROM piwik_option WHERE option_name LIKE 'version_%' AND option_name != 'version_Installation'", DB_NAME);
	$tools->query("INSERT INTO `piwik_option` VALUES
			('version_core', '1.12', 1),
			('version_CorePluginsAdmin', '1.12', 1),
			('version_CoreAdminHome', '1.12', 1),
			('version_CoreHome', '1.12', 1),
			('version_Proxy', '1.12', 1),
			('version_API', '1.12', 1),
			('version_Widgetize', '1.12', 1),
			('version_LanguagesManager', '1.12', 1),
			('version_Actions', '1.12', 1),
			('version_Dashboard', '1.12', 1),
			('version_MultiSites', '1.12', 1),
			('version_Referers', '1.12', 1),
			('version_UserSettings', '1.12', 1),
			('version_Goals', '1.12', 1),
			('version_SEO', '1.12', 1),
			('version_UserCountry', '1.12', 1),
			('version_VisitsSummary', '1.12', 1),
			('version_VisitFrequency', '1.12', 1),
			('version_VisitTime', '1.12', 1),
			('version_VisitorInterest', '1.12', 1),
			('version_ExampleAPI', '0.1', 1),
			('version_ExamplePlugin', '0.1', 1),
			('version_ExampleRssWidget', '0.1', 1),
			('version_ExampleFeedburner', '0.1', 1),
			('version_Provider', '1.12', 1),
			('version_Feedback', '1.12', 1),
			('version_Login', '1.12', 1),
			('version_UsersManager', '1.12', 1),
			('version_SitesManager', '1.12', 1),
			('version_CoreUpdater', '1.12', 1),
			('version_PDFReports', '1.12', 1),
			('version_UserCountryMap', '1.12', 1),
			('version_Live', '1.12', 1),
			('version_CustomVariables', '1.12', 1),
			('version_PrivacyManager', '1.12', 1),
			('version_ImageGraph', '1.12', 1),
			('version_Transitions', '1.12', 1),
			('version_Annotations', '1.12', 1),
			('version_MobileMessaging', '1.12', 1),
			('version_Overlay', '1.12', 1),
			('version_SegmentEditor', '1.12', 1),
			('version_AnonymizeIP', '1.12', 1)", DB_NAME);

	// Ajout de nouvelles options : (ON DUPLICATE KEY UPDATE permet de garantir d éviter de ré-insérer une clé primaire existante)
	$tools->query("INSERT INTO `piwik_option` VALUES('MobileMessaging_DelegatedManagement', 'false', 0),
			('delete_logs_enable', '0', 0),
			('delete_logs_schedule_lowest_interval', '7', 0),
			('delete_logs_older_than', '180', 0),
			('delete_logs_max_rows_per_query', '100000', 0),
			('delete_reports_enable', '0', 0),
			('delete_reports_older_than', '12', 0),
			('delete_reports_keep_basic_metrics', '1', 0),
			('delete_reports_keep_day_reports', '0', 0),
			('delete_reports_keep_week_reports', '0', 0),
			('delete_reports_keep_month_reports', '1', 0),
			('delete_reports_keep_year_reports', '1', 0),
			('delete_reports_keep_range_reports', '0', 0),
			('delete_reports_keep_segment_reports', '0', 0) ON DUPLICATE KEY UPDATE option_name=option_name", DB_NAME);


	// ************  Manipulation de données complexes (Nécéssite du codage)
	//--> Reconversion de la forme d'enregistrement des URL sans la table piwik_log_action
	//	et correction des index dans 2 tables qui sont liées.
	$tools->afficher("6a: Conversion des données de la table tmp_piwik_log_action --> piwik_log_action\n");
	// NOTES :
	// Dans le fichier src/piwik-1.12/core/Tracker/Action.php, on trouve :
	// 	- lg 104 : la définitions des 4 états du nouveau champs `url_prefix` ('http://www.' => 1, 'http://' => 0, 'https://www.' => 3, 'https://' => 2)
	//	- lg 556 et 567 : le calcul du champ `hash` est un CRC32() du champ `name`

	//-> Lire la table temporaire `tmp_piwik_log_action` et traiter la conversion
	$tools->setRows("SELECT * FROM `tmp_piwik_log_action` WHERE 1 ORDER BY idaction ASC", DB_NAME);
	if(count($tools->rows)==0) $tools->afficher("La table tmp_piwik_action est vide, aucune action à réaliser\n");
	else
	{
		// Création d'un tableau mémorisant les résultats de $tools->setRows() afin de pouvoir relancer des requêtes à l'objet tools
		$tabLogAction = $tools->rows;
		// Tableau de conversion des identifiants idaction
		$tabConvert = array();
		// Scruter le tableau
		for ($x=0; $x < count($tabLogAction) ; $x++)
		{
			// On va d'abord vérifier si le prefixe de l'url correspond à un des 4 préfixes définis dans la note un peu plus haut.
			// et on récupère le nom sans préfixe et le numéro du champ url_prefix
			$isUrl = false;
			$urlPrefixe = -1;
			$nomSansPrefixe = "";
			// si URL type http://www.
			if (preg_match('/^http:\/\/www\./', $tabLogAction[$x]["name"]))
			{ 
				$isUrl = true;
				$urlPrefixe = 1;
				$nomSansPrefixe = preg_replace('/^http:\/\/www\./', "", $tabLogAction[$x]["name"]);
			}
			// si URL type http://
			if (!$isUrl && preg_match('/^http:\/\//', $tabLogAction[$x]["name"]))
			{ 
				$isUrl = true;
				$urlPrefixe = 0;
				$nomSansPrefixe = preg_replace('/^http:\/\//', "", $tabLogAction[$x]["name"]);
			}
			// si URL type https://www.
			if (!$isUrl && preg_match('/^https:\/\/www\./', $tabLogAction[$x]["name"]))
			{ 
				$isUrl = true;
				$urlPrefixe = 3;
				$nomSansPrefixe = preg_replace('/^https:\/\/www\./', "", $tabLogAction[$x]["name"]);
			}
			// si URL type http://
			if (!$isUrl && preg_match('/^https:\/\//', $tabLogAction[$x]["name"]))
			{ 
				$isUrl = true;
				$urlPrefixe = 2;
				$nomSansPrefixe = preg_replace('/^https:\/\//', "", $tabLogAction[$x]["name"]);
			}
			
			// Si c'est une URL : 
			if ($isUrl && $urlPrefixe != -1 && $nomSansPrefixe != "")
			{
				// Rechercher dans la nouvelle table piwik_log_action un name identique
				$tools->setRows("SELECT * FROM `piwik_log_action` WHERE name='" . $nomSansPrefixe . "'", DB_NAME);
				// si il y en a un idnetique, on n'enregistre pas mais on prépare la table de conversion
				if(count($tools->rows)!=0) $tabConvert[$tabLogAction[$x]["idaction"]] = $tools->rows[0]["idaction"];
				// Si on n'a pas trouvé d'entrée identique, enregistrer :
				else $tools->query("INSERT INTO `piwik_log_action` (`idaction`, `name`, `hash`, `type`, `url_prefix`) VALUES ('" . $tabLogAction[$x]["idaction"] . "', '" . $nomSansPrefixe . "', CRC32('" . $nomSansPrefixe . "'), '" . $tabLogAction[$x]["type"] . "', '" . $urlPrefixe . "')", DB_NAME);

			}
			// Sinon (si ce n'est pas une URL), on enregistre le champ en le recopiant et (on laisse url_prefix à NULL dans la table)
			else 
			{
				// Enregistre la nouvelle entrée sans aucune transformation
				$tools->query("INSERT INTO `piwik_log_action` (`idaction`, `name`, `hash`, `type`) VALUES ('" . $tabLogAction[$x]["idaction"] . "', '" . $tabLogAction[$x]["name"] . "', '" . $tabLogAction[$x]["hash"] . "', '" . $tabLogAction[$x]["type"] . "')", DB_NAME);			
			}
		}

		// Traiter la maj des cles des 2 tables piwik_log_link_visit_action et piwik_log_visit à partir du tableau $tabConvert
		foreach (array_keys($tabConvert) as $k ) 
		{ 
			$tools->afficher(" * Traitement modification des champs idaction (la clé $k devient " . $tabConvert[$k] ."\n"); 

			$tools->afficher(" *--> Table piwik_log_link_visit_action\n");
			$tools->query("UPDATE piwik_log_link_visit_action SET `idaction_url`='" . $tabConvert[$k] . "' WHERE `idaction_url`='" . $k . "'", DB_NAME);
			$tools->query("UPDATE piwik_log_link_visit_action SET `idaction_url_ref`='" . $tabConvert[$k] . "' WHERE `idaction_url_ref`='" . $k . "'", DB_NAME);

			$tools->afficher(" *--> Table piwik_log_visit\n");
			$tools->query("UPDATE piwik_log_visit SET `visit_exit_idaction_url`='" . $tabConvert[$k] . "' WHERE `visit_exit_idaction_url`='" . $k . "'", DB_NAME);
			$tools->query("UPDATE piwik_log_visit SET `visit_entry_idaction_url`='" . $tabConvert[$k] . "' WHERE `visit_entry_idaction_url`='" . $k . "'", DB_NAME);
		}
	}

	//--> Conversion des anciens tags personnalisés dans les 5 variables standard de piwik
	$tools->afficher("6b: reconversion des 4 champs Envole_* de la table piwik_log_visit pour adapter au nouveau process d'enregistrement des logs\n");
	$tools->setRows("SELECT * FROM `piwik_log_visit` WHERE 1 ORDER BY idvisit ASC", DB_NAME);
	if(count($tools->rows)==0) $tools->afficher("La table piwik_log_visit est vide, aucune action à réaliser\n");
	else
	{
		// Création d'un tableau mémorisant les résultats de $tools->setRows() afin de pouvoir relancer des requêtes à l'objet tools
		$tabLogVisit = $tools->rows;
		// Scruter le tableau et enregistrer les champs personnalisés
		for ($x=0; $x < count($tabLogVisit) ; $x++)
		{
			$tools->query("UPDATE `piwik_log_visit` 
					SET 	`custom_var_k1`='Rne', `custom_var_v1`='" . addslashes($tabLogVisit[$x]["Envole_rne"]) . "',
						`custom_var_k4`='Profil Utilisateur', `custom_var_v4`='" . addslashes($tabLogVisit[$x]["Envole_group"]) . "',
						`custom_var_k5`='Provenance', `custom_var_v5`='" . addslashes($tabLogVisit[$x]["Envole_provenance"]) . "'
					WHERE `idvisit`='" . $tabLogVisit[$x]["idvisit"] . "'", DB_NAME);
		}
		// Compte rendu
		$tools->afficher(" *--> " . count($tabLogVisit) . " enregistrements ont été modifiés dans la table piwik_log_visit\n");
	}

	/********************************** Nettoyage de la structure de la base de données ********************************************************/
	//--> Suppression des tables en trop
	$tools->afficher("7: Suppression des tables en trop (si elles existent)\n");
	$tools->query("DROP TABLE IF EXISTS piwik_pdf", DB_NAME);

	//--> Supression des colonnes en trop
	$tools->afficher("8: Suppression des colonnes en trop (si elles existent)\n");
	supprimerColonne("piwik_log_conversion", "location_continent");
	supprimerColonne("piwik_log_visit", "location_continent");
	supprimerColonne("piwik_site", "feedburnerName");
	// Suppression des 4 champs de l'ancien système de sondage pour Envole sous piwik 1.6.
	//	En étape 6b : Les champs utilisés dans la nouvelle version de piwik ont préalablement été reconvertis au système standard de variables piwik (custom_var_*)
	supprimerColonne("piwik_log_visit", "Envole_rne");
	supprimerColonne("piwik_log_visit", "Envole_group");
	supprimerColonne("piwik_log_visit", "Envole_user");
	supprimerColonne("piwik_log_visit", "Envole_provenance");

	//--> A compter d'ici, ce morceau de script de maj ne pourra plus être relancé
	$tools->afficher("9: Finalisation de la mise à jour\n");
	$tools->query("UPDATE `piwik_option` SET  `option_value`='1.12' WHERE `option_name`='version_Installation'", DB_NAME);

	//--> Suppression des tables ou champs temporaires
	// On supprime tmp_piwik_log_action après finalisation ce qui évite de ré-écraser ce qui aurait déjà été migré dans la table piwik_log_action
	$tools->afficher("10: Suppression des tables ou champs temporaires\n");
	$tools->query("DROP TABLE IF EXISTS `tmp_piwik_log_action`", DB_NAME);
	
}


/****************************************************************************************************************************************************************/
/* Fonctions réalisant des actions sécurisées (test d'existance ou d'inexistance) afin de rendre possible la relance d'une partie du script alors que l'action 	*/
/* 	a déjà eu lieu																		*/
/****************************************************************************************************************************************************************/
/**
 * Renommer une base SQL en vérifiant si pas de soucis de renommage
 *
 * @param $nomAncien Nom de la table avant renommage
 * @param $nomNouveau Nom de la table après renommage
 *
 * @return boolean true si renommée, false si déja renommée
 */
function renommerTable ($nomAncien, $nomNouveau)
{
	// Récupère l'objet EnvoleTools déjà créé
	global $tools;

	// On vérifie d'abord que la nouvelle table n'éxiste pas déjà
	$tools->setRows("SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA='". DB_NAME ."' AND TABLE_NAME='$nomNouveau'", "information_schema");
	if(count($tools->rows)==0) 
	{
		$tools->query("RENAME TABLE `$nomAncien` TO `$nomNouveau`", DB_NAME);
		return (true);
	}
	// Si on arrive ici, c'est que le renommage n'a pas été nécessaire
	return (false);
}

/**
 * Ajouter une colonne dans une table en vérifiant si elle n'existe pas déja (dans ce cas, on la drop et on la recréé)
 *
 * @param $table nom de la table
 * @param $champ nom du champs
 * @param $paramatres paramètres de la requête : ALTER TABLE $table ADD COLUMN $champ $parametres
 */
function ajouterColonne($table, $champ, $parametres)
{		
	// Récupération de l'objet $tools
	global $tools;

	// On vérifie d'abord que la nouvelle table n'éxiste pas déjà
	$tools->setRows("SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA='". DB_NAME ."' AND TABLE_NAME='$table' AND COLUMN_NAME='$champ'", "information_schema");
	if(count($tools->rows)!=0) $tools->query("ALTER TABLE `".$table."` DROP COLUMN `".$champ."`", DB_NAME);
	$tools->query("ALTER TABLE `".$table."` ADD COLUMN `".$champ."` ".$parametres, DB_NAME);
}

/**
 * Sipprimer une colonne dans une table en vérifiant si elle existe avant
 *
 * @param $table nom de la table
 * @param $champ nom du champs
 */
function supprimerColonne($table, $champ)
{		
	// Récupération de l'objet $tools
	global $tools;

	// On vérifie d'abord que la nouvelle table n'éxiste pas déjà
	$tools->setRows("SELECT * FROM `COLUMNS` WHERE TABLE_SCHEMA='". DB_NAME ."' AND TABLE_NAME='$table' AND COLUMN_NAME='$champ'", "information_schema");
	if(count($tools->rows)!=0) $tools->query("ALTER TABLE `".$table."` DROP COLUMN `".$champ."`", DB_NAME);
}


?>
