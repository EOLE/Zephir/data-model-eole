#-*- coding: utf-8 -*-
##########################################################################
# Eole NG - 2013
# Copyright Pole de competence Eole (Ministere Education - Academie Dijon)
# Licence CeCill cd /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# aaf.py
#
# Creation de la base de donnees pour eole-aaf
##########################################################################
"""
    Creation de la base aaf
"""
from eolesql.db_test import db_exists

TABLEFILENAMES = ['/usr/share/eole/mysql/eole-aaf/gen/eoleaaf.sql']

def test():
    """
        test l'existence de la base aaf
    """
    return not db_exists('eoleaaf')

conf_dict = dict(filenames=TABLEFILENAMES, test=test)
