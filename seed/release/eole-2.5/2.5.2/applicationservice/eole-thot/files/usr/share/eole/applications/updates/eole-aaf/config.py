#-*-coding:utf-8-*-

FILENAMES = ['/usr/share/eole/mysql/eole-aaf/updates/eoleaaf-update-0.sql']
# existance du champ ENTPersonAdresseDiffusion
QUERY = "select * from information_schema.columns where column_name='ENTPersonAdresseDiffusion' and table_name='responsable'"

def test_response(result):
    """
    """
    return result == []

conf_dict = dict(db='eoleaaf',
                 condition_query=QUERY,
                 expected_response=test_response,
                 filenames=FILENAMES,
                 version='Mise à jour SDET 5.0')
