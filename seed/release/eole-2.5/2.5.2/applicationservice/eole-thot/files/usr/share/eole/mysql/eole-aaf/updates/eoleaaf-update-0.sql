\r eoleaaf

-- modify types #16910
ALTER TABLE enseignant MODIFY ENTAuxEnsMatiereEnseignEtab text;
ALTER TABLE enseignant MODIFY ENTAuxEnsClasses text;
ALTER TABLE enseignant MODIFY ENTAuxEnsClassesMatieres text;
ALTER TABLE enseignant MODIFY ENTAuxEnsGroupesMatieres text;

-- new columns #16908
ALTER TABLE responsable ADD ENTPersonAdresseDiffusion VARCHAR(1);
ALTER TABLE responsable ADD mail VARCHAR(50);
ALTER TABLE responsable ADD ENTPersonMailDiffusion VARCHAR(50);
ALTER TABLE responsable ADD mobile VARCHAR(50);
ALTER TABLE responsable ADD ENTPersonMobileSMS VARCHAR(50);
ALTER TABLE responsable ADD ENTPersonAutresMobiles VARCHAR(50);
ALTER TABLE responsable ADD ENTPersonAutresMails VARCHAR(50);
