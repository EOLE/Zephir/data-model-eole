-- DROP DATABASE IF EXISTS eoleaaf;
CREATE DATABASE eoleaaf CHARACTER SET utf8 ;
GRANT ALL PRIVILEGES ON eoleaaf.* to 'eoleaaf'@'localhost' identified by 'eoleaaf';
USE eoleaaf ;

-- classe

create table classe (
        description VARCHAR(50) NOT NULL PRIMARY KEY,
        member VARCHAR(500),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- groupe

create table groupe (
        description VARCHAR(50) NOT NULL PRIMARY KEY,
        member VARCHAR(500),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- eleve

create table eleve (
        id VARCHAR(50) NOT NULL PRIMARY KEY,
        uid VARCHAR(8),
        userPassword VARBINARY(8),
        ENTPersonLogin VARCHAR(50),
        ENTEleveFiliere VARCHAR(50),
        ENTEleveTransport VARCHAR(50),
        ENTEleveEnseignements VARCHAR(500),
        ENTEleveStructRattachId VARCHAR(50),
        ENTEleveClasses VARCHAR(50),
        ENTEleveMEF VARCHAR(50),
        ENTPersonJointure VARCHAR(50),
        ENTEleveNivFormation VARCHAR(50),
        ENTEleveStatutEleve VARCHAR(50),
        ENTEleveAutoriteParentale VARCHAR(50),
        ENTElevePere VARCHAR(50),
        ENTEleveQualitePersRelEleve2 VARCHAR(50),
        ENTEleveQualitePersRelEleve1 VARCHAR(50),
        ENTEleveMere VARCHAR(50),
        ENTEleveGroupes VARCHAR(250),
        ENTPersonDateNaissance VARCHAR(50),
        ENTElevePersRelEleve2 VARCHAR(250),
        ENTElevePersRelEleve1 VARCHAR(250),
        ENTEleveParents VARCHAR(50),
        ENTEleveBoursier VARCHAR(50),
        ENTEleveLibelleMEF VARCHAR(50),
        personalTitle VARCHAR(10),
        ENTPersonNomPatro VARCHAR(50),
        sn VARCHAR(50),
        ENTPersonStructRattach VARCHAR(50),
        ENTPersonAutresPrenoms VARCHAR(50),
        givenName VARCHAR(50),
        ENTEleveRegime VARCHAR(50),
        ENTEleveCodeEnseignements VARCHAR(250),
        ENTElevePersRelEleve VARCHAR(250),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- responsable

create table responsable (
        id VARCHAR(50) NOT NULL PRIMARY KEY,
        uid VARCHAR(8),
        ENTPersonLogin VARCHAR(50),
        userPassword  VARBINARY(8),
        telephoneNumber VARCHAR(50),
        ENTPersonPays VARCHAR(50),
        ENTPersonAdresse VARCHAR(150),
        personalTitle VARCHAR(10),
        ENTPersonVille VARCHAR(50),
        ENTPersonCodePostal VARCHAR(50),
        ENTPersonAdresseDiffusion VARCHAR(1),
        ENTPersonJointure VARCHAR(50),
        ENTPersonNomPatro VARCHAR(50),
        sn VARCHAR(50),
        ENTPersonDateNaissance VARCHAR(50),
        givenName VARCHAR(50),
        homePhone VARCHAR(50),
        mobile VARCHAR(50),
        ENTPersonAutresMobiles VARCHAR(50),
        ENTPersonMobileSMS VARCHAR(50),
        mail VARCHAR(50),
        ENTPersonAutresMails VARCHAR(50),
        ENTPersonMailDiffusion VARCHAR(50),
        ENTAuxPersRelEleveEleve VARCHAR(100),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- enseignant

create table enseignant (
        id VARCHAR(50) NOT NULL PRIMARY KEY,
        uid VARCHAR(8),
        userPassword  VARBINARY(8),
        ENTPersonLogin VARCHAR(50),
        ENTAuxEnsGroupes VARCHAR(500),
        ENTPersonFonctions VARCHAR(800),
        personalTitle VARCHAR(10),
        ENTAuxEnsMEF VARCHAR(550),
        ENTAuxEnsCategoDiscipline VARCHAR(500),
        ENTPersonJointure VARCHAR(50),
        ENTAuxEnsDisciplinesPoste VARCHAR(50),
        ENTPersonNomPatro VARCHAR(50),
        PersEducNatPresenceDevantEleves VARCHAR(50),
        ENTAuxEnsMatiereEnseignEtab TEXT,
        sn VARCHAR(50),
        ENTPersonStructRattach VARCHAR(50),
        mail VARCHAR(50),
        ENTPersonDateNaissance VARCHAR(50),
        ENTAuxEnsClassesPrincipal VARCHAR(250),
        givenName VARCHAR(50),
        ENTAuxEnsClasses TEXT,
        ENTAuxEnsClassesMatieres TEXT,
        ENTAuxEnsGroupesMatieres TEXT,
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- administratif

create table administratif(

        id VARCHAR(50) NOT NULL PRIMARY KEY,
        uid VARCHAR(8),
        userPassword  VARBINARY(8),
        ENTPersonLogin VARCHAR(50),
        ENTPersonFonctions VARCHAR(800),
        personalTitle VARCHAR(10),
        ENTPersonJointure VARCHAR(50),
        ENTPersonNomPatro VARCHAR(50),
        PersEducNatPresenceDevantEleves VARCHAR(50),
        sn VARCHAR(50),
        ENTPersonStructRattach VARCHAR(50),
        mail VARCHAR(50),
        ENTPersonDateNaissance VARCHAR(50),
        givenName VARCHAR(50),
        ENTAuxEnsMEF VARCHAR(550),
        ENTAuxEnsClassesMatieres VARCHAR(500),
        ENTAuxEnsGroupesMatieres VARCHAR(500),
        ENTAuxEnsMatiereEnseignEtab VARCHAR(250),
        ENTAuxEnsClasses VARCHAR(250),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- etablissement

create table etablissement(
        id INTEGER NOT NULL PRIMARY KEY,
        postOfficeBox VARCHAR(50),
        ENTEtablissementMinistereTutelle VARCHAR(50),
        ENTEtablissementContrat VARCHAR(50),
        ENTStructureJointure VARCHAR(50),
        telephoneNumber VARCHAR(50),
        ENTServAcAcademie VARCHAR(50),
        facsimileTelephoneNumber VARCHAR(50),
        l VARCHAR(50),
        ENTEtablissementStructRattachFctl VARCHAR(50),
        ENTStructureSIREN VARCHAR(50),
        street VARCHAR(50),
        ENTEtablissementUAI VARCHAR(50),
        ENTEtablissementBassin VARCHAR(50),
        ENTStructureTypeStruct VARCHAR(50),
        postalCode VARCHAR(50),
        ENTStructureNomCourant VARCHAR(150),
        ENTStructureUAI VARCHAR(50),
        ENTStructureGroupes VARCHAR(1550),
        ENTStructureClasses VARCHAR(1550),
        FieldModifiedStatus BOOLEAN,
        FieldActionType VARCHAR(50),
        FieldFromXMLImportActionDate DATETIME,
        FieldToLDIFFExportActionDate DATETIME
        )
        engine = InnoDB;

-- journal

create table journal(
        ActionDate DATETIME NOT NULL,
        TableName VARCHAR(50),
        ActionId VARCHAR(50),
        Level VARCHAR(10),
        FieldActionType VARCHAR(50),
        -- the whole entry saved as a dict
        FieldDeletedContent VARCHAR(2500)
        )
        engine = InnoDB;

-- unique random id table

create table randomids(
        id VARCHAR(50) NOT NULL PRIMARY KEY
        )
        engine = InnoDB;

-- unique ENTPersonLogin  table

create table personlogin(
        ENTPersonLoginAlpha VARCHAR(50),
        ENTPersonLoginNum VARCHAR(10),
        DateCreation DATETIME NOT NULL,
        uid VARCHAR(8),
        usertype VARCHAR(50),
        PRIMARY KEY (ENTPersonLoginAlpha, ENTPersonLoginNum)
        )
        engine = InnoDB;
