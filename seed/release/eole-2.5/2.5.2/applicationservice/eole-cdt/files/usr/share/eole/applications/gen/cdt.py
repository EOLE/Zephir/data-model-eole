#-*-coding:utf-8-*-
###########################################################################
#
# cdt.py
#
###########################################################################
"""
    Configuration pour la création de la base de données de cdt
"""
from eolesql.db_test import db_exists, test_var

cdt_TABLEFILENAMES = ['/usr/share/eole/mysql/cdt/gen/cdt-create-0.sql']

def test():
    """
        test l'existence de la base de donnée cdt
    """
    return test_var('activer_cdt') and not db_exists('cdt')

conf_dict = dict(filenames=cdt_TABLEFILENAMES,
                 test=test)

