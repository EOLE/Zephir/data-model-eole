#!/usr/bin/perl
#
##########################################################################
# GB_Actualise.pl v1.1 (C) MAAPAR/SDSI/CERI/DIG 2004 [MW-Projet GéoMAP]
# Actualisation de la base de consultation avec la base de production
# dans la GéoBASE : doit tourner une fois par jour (lancement par cron)
# Devrait utiliser l'outil TAB2TAB de copie de fichers MapInfo
##########################################################################

# Quelques initialisations

$dirbase="%%geobase_homedir";
$dirlog="/var/log/geobase";
$dir_prod="$dirbase/PRODUCTION";
$dir_cons="$dirbase/CONSULTATION";
$dir_ref="$dirbase/REF_EXT";
$cougra="$dir_ref/COUGRA.XML";

# date du jour
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$date=sprintf("%04d%02d%02d",$year+1900, $mon+1, $mday);
$err=$dirlog . "/Err" . $date;
$log=$dirlog . "/gb_actualise.log";
open(STDERR, ">> $err") || die "Impossible de créer le fichier d'erreurs $!";
open(STDOUT, ">> $log") || die "Impossible de créer le fichier de log $!";
open(FC, "< $cougra") || die "COUGRA.XML est innaccessible !";
$line=0;
$pos=0;

printf STDOUT "INFO\t%s\tActualisation de PROD vers CONS.\n", $date;

# parcours des entrées de COUGRA

while (<FC>) {
	$line++;
	$ligne=$_;
	if (/<ENTRY/) {
		$nomtable="";
		$chemintable="";
		$base="";
		$valide=0;
		$actualise=0;
		$frequence="";
		$ipos=index( $ligne, "ACTU_BL") + $pos + 9;
		split / /;	# On sépare les entrées pour récupérer les bonnes
		foreach (@_) {
			chop;			# élimination du " de fin
			if (/COUGRA_RFA/) {	# nom de la table
				s/.*\"//;	# on ne garde que la valeur
				$nomtable=$_;
			} elsif (/CHDESC_LB/) {	# quelle base ?
				s/.*\"//;
				$chdesc=$_;
				if ($chdesc =~ /P/) {
					$base=$dir_prod;
				} else {
					last;
				}
			} elsif (/CHEMIN_LB/) {	# quel répertoire ?
				s/.*\"//;
				$chemintable=$_;
			} elsif (/ACTU_BL/) {	# actualisation forcée ?
				s/.*\"//;
				$actualise=1 if (/OUI/);
			} elsif (/FREQ_LB/) {	# fréquence de mise à jour ?
				s/.*\"//;
				$frequence=$_;
			} elsif (/VALID_BL/) {	# couche valide ?
				s/.*\"//;
				$valide=1 if (/OUI/); 
			}
		}
		# mise à jour seulement si la couche est valide ou si forcée
		if ($valide == 1 && $actualise == 0) {
			if ($frequence =~ /J/) {
				$actualise=2;	# tous les jours
			} elsif ($frequence =~ /S/ && $wday == 5) {
				$actualise=2;	# tous les vendredi
			} elsif ($frequence =~ /M/ && $wday == 5 && $mday < 8) {
				$actualise=2;	# le premier vendredi du mois
			}
		}
		$pos += length($ligne);
		next if ($actualise == 0);
		push @tpos, $ipos if ($actualise == 1);

		# nom de fichier correct ?
		if (length($base) == 0 || length($nomtable) == 0
		 || length($chemintable) == 0) {	# pas normal !
			printf STDERR "Entrée invalide ligne %d :\n %s", $line, $ligne;
			next;
		}
		# le fichier existe-t-il ?
		$chemintable =~ s/\\/\//g;	# évite les "\"
		$table=$base."/".$chemintable."/".$nomtable;
		$table_maj = $table.".TAB";	# fin en .TAB ou .tab
		$table_min = $table.".tab";
		$hnoms{$nomtable} = $table_maj;	# liste des tables
		# Vérification de son existence
		if (! -e $table_maj and ! -e $table_min ) {
			printf STDERR "Table $nomtable ($table) non trouvé dans la GéoBASE (COUGRA.XML, ligne %d)\n", $line;
			next;
		}
		$srce=$base."/".$chemintable."/".$nomtable.".*";
		$dest=$dir_cons."/".$chemintable;
		@args=( "cp $srce $dest");
		if (system(@args) == 0) {
			printf "Table $nomtable copiée (%s)\n", $table;
		} else {
			printf STDERR "Erreur de copie de $nomtable (Exit=%d, SigNum=%d, Dump=%d)\n", $?>>8, $?&127,$?&128;
		}
	} else {
		$pos += length($ligne);
	}
}
# remise à "NON" des flags de demande d'actualisation
close(FC);
open(FC, "+< $cougra");
foreach $curpos (@tpos) {
	seek(FC, $curpos, 0);
	# pas top le perl pour le remplacement en accès direct => on blinde un peu
	read(FC, $rep, 3);
	if ($rep =~ /OUI/) {
		seek (FC, $curpos, 0);
		print FC "NON";
	} else {
		printf STDERR "Erreur de mise à jour COUGRA (Offset %d, trouvé \"%s\" au lieu de \"OUI\")\n", $curpos, $rep;
	}
}
close(FC);	# fermeture de cougra

printf STDOUT "INFO\t%s\tActualisation de PROD vers CONS terminée.\n", $date;

close(STDOUT) || die "Impossible de fermer le fichier de log : $!";
close(STDERR) || die "Impossible de fermer le fichier d'erreur : $!";
unlink($err) if ((stat($err))[7] == 0) ;
unlink($log) if ((stat($log))[7] == 0) ;
