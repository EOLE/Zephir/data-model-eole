#!/bin/bash

if [ "%%inst_pallier12" == "non" ]; then
GEOIDE_FTP_SERVER=%%geoide_base_replic_ftp_server
LOGDIR=/var/log/geobase
LOGFILE=$LOGDIR/replication.log
script=`pwd`
ftpuser=%%geoide_base_replic_ftp_user
ftppassword=%%geoide_base_replic_ftp_pass
serialnb=%%geoide_base_replic_sn

if [ "$ftpuser" == "" ]; then
echo "La réplication vers l'entrepôt central n'est pas configurée";
exit 0;
fi

# chemin à vérifier
cd %%geobase_homedir

# Evitons les messages intempestifs
exec >/dev/null 2>&1

# déclenchement échelonné de 150 sites toutes les 120 secondes
day=`date +%j`
delay=`expr '(' '(' $serialnb + '(' 29 '*' $day ')' ')' '%' 150 ')' '*' 120`
sleep $delay

du -h --max-depth 1 %%geobase_homedir > $LOGFILE
date >> $LOGFILE
nohup lftp -e "mirror -R -e -v -X LOCAL/* -X *.old -X *.OLD -X .* PRODUCTION" -u $ftpuser,$ftppassword $GEOIDE_FTP_SERVER 2>&1 > $LOGFILE
date >> $LOGFILE
nohup lftp -e "mirror -R -e -v -X LOCAL/* -X *.old -X *.OLD -X .* CONSULTATION" -u $ftpuser,$ftppassword $GEOIDE_FTP_SERVER 2>&1 > $LOGFILE
date >> $LOGFILE
fi
