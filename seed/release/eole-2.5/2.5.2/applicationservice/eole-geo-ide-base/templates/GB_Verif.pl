#!/usr/bin/perl -w
#
##########################################################################
# GB_Verif.pl v2.1 (C) MAP/SG/SM/SDSI/CERI/DIG 2005 [MW-Projet GéoMAP]
# Vérification de l'arborescence par rapport à la définition GéoREPERTOIRE
# et de la cohérence entre COUGRA.XML et la GéoBASE (fichiers non présents
# sur le disque ou non référencés dans COUGRA.
# Compactage éventuel des tables MapInfo.
# Cette procédure est lancée une fois par jour avec cron
# A améliorer en vérifiant le contenu des fichiers ?
##########################################################################
#
# Usage : perl GB_Verif.pl [-c] [-v] [-d]
#  -c si on désire le compactage des fichiers MapInfo
#  -v si on veut afficher (fichier log) les actions effectuées
#  -d (implique -v) si on veut juste visualiser les actions sans les effectuer
#
#  Chaque modification provoque l'écriture d'un enregistrement dans le fichier
#  ErrAAAAMMJJ (AAAA = année, MM = mois, JJ =jour) puis copie ce fichier dans
#  le fichier LastErr.log, qui sera récupéré par GéoREPERTOIRE.  Chaque entrée
#  de LastErr.log est composée de 2 champs séparé par une tabulation.
#  Le premier est un code sur 2 caractère, dont le codage est le suivant :
#     CR = Un répertoire manquant localement a été automatiquement créé
#     CE = La création de ce répertoire a échoué
#     SR = Un répertoires invalide (non connu dans GéoREPERTOIRE) a été supprimé
#     SE = La suppression de ce répertoire a échoué (en général car non vide)
#     EM = Une couche référencée dans COUGRA a été trouvée avec un autre
#          chemin que celui indiqué => l'entrée (base et/ou chemin) a été
#          modifiée dans COUGRA (on ne déplace pas le fichier...)
#     EI = Une entrée invalide de COUGRA a été effacées (nom vide)
#     EE = Une entrée de COUGRA ne correspondant à aucun fichier a été effacée
#     EA = Une entrée a été ajoutée à COUGRA (on a trouvé un fichiers ".tab"
#          dans la GéoBASE non référencé dans COUGRA)
#     TC = La table MapInfo correspondant à cette entrée a été compactée
#     XM = Autre Problème (fichier geobase_tree.txt ou COUGRA.XML illisible, etc.)
#     OK = La vérification du COUGRA s'est terminée sans incident
#
#  La second champ de la ligne donne les informations complémentaires
#  (nom, base, chemin, ligne du fichier COUGRA, etc.)
#
#  Les gestionnaires locaux (demandeurs) peuvent aussi voir le fichier
#  correpondant à leur site dans l'onglet "Option" de GéoREPERTOIRE, sous
#  la forme d'un tableau plus facile à lire et interpréter

# Pour lire COUGRA.XML, on utilise le module perl XML::Parser

use strict;
use LWP::Simple;
use XML::Parser;

# Quelques initialisations

my $georepertoire = "http://georepertoire.national.agri";
my $dirlog     = "/var/log/geobase";
my $tab2tab    = "tab2tab";
my $dirbase    = "%%geobase_homedir";
my $dir_prod   = "PRODUCTION";
my $dir_cons   = "CONSULTATION";
my $dir_ref    = "REF_EXT";
my $cougra     = "$dirbase/$dir_ref/COUGRA.XML";
my $cougra_new = "$dirbase/$dir_ref/COUGRA.NEW";
my $cougra_bak = "$dirbase/$dir_ref/COUGRA.BAK";
# copie locale du fichier équivalent de GéoREPERTOIRE (mise à jour chaque nuit)
my $gb_tree_file = "geobase_tree.txt";
my $gb_tree    = "/usr/lib/geobase/$gb_tree_file";

# Date du jour
(my $sec,my $min,my $hour,my $mday,my $mon,my $year,my $wday,my $yday,my $isdst) = localtime(time);
my $date=sprintf("%04d%02d%02d",$year+1900, $mon+1, $mday);
# Nom du fichier d'erreur
my $err=$dirlog . "/gb_verif.log";

open( STDERR, ">> $err") || die "Impossible de creer le fichier d'erreurs $!";

# Lecture des paramètres
my $verbose = 0; # par défaut, rien d'affiché (-v => verbeux)
my $nodebug = 1; # par défaut, on exécute (-d => mode debug)
my $compact = 0; # par défaut, pas de compactage... (-c => compactage)
if ( $#ARGV >= 0 ) {
	foreach (@ARGV) {
		if (/^-v/) {
			$verbose = 1;
		} elsif (/^-d/) {
			$nodebug = 0;
			$verbose = 1;
		} elsif (/^-c/) {
			$compact = 1;
		}
	}
}

# Vérification de l'arborescence et mise en mémoire des entrées corrrectes
# open( DL,"< $gb_tree") || termine( "XM\tPas de fichier de description de l'arborescence !");

getstore("$georepertoire/$gb_tree_file", "/tmp/$gb_tree_file");
open( DL,"< /tmp/$gb_tree_file") || open( DL,"< $gb_tree") || termine( "XM\tPas de fichier de description de l'arborescence !");

if ($verbose) {
	print "GB_Verif.pl version 2.1 du 8/11/2005\n\n" ;
	print "Création éventuelle des répertoires manquants :\n" ;
	print "-----------------------------------------------\n" ;
}
chdir( $dirbase);
umask( 002 );	# par défaut, création des répertoire en drwxrwxr-x
my %tb_ok;
while (<DL>) {
	chop;	# on vire la fin de ligne
	if (! -d $_ ) {
		print "création du répertoire '$_'\n" if ($verbose);
		if ($nodebug) {
			mkdir( $_ );
			if (! -d $_ ) {
				print STDERR "CE\t$_\n";
			} else {
				print STDERR "CR\t$_\n";
			}
		}
}
# Même traitement pour la base de PRODUCTION et de CONSULTATION
	if (/^CONSULTATION/) {
		$tb_ok{$_} = 'c';
		s/CONSULTATION/PRODUCTION/;
		$tb_ok{$_} = 'p';
		if (! -d $_ ) {
			print "création du répertoire '$_'\n" if ($verbose);
			if ($nodebug) {
				mkdir( $_ );
				if (! -d $_ ) {
					print STDERR "CE\t$_\n";
				} else {
					print STDERR "CR\t$_\n";
				}
			}
		}
	} else {
		$tb_ok{$_} = 'r';
	}
}
close( DL);

# recherche des répertoires non nécessaires...
# ...en base de production
if ($verbose) {
	print "\nEffacement des éventuels répertoires non valides :\n" ;
	print "--------------------------------------------------\n" ;
}

open( DL,"find PRODUCTION -depth -type d -print |");
while(<DL>) {
	chop;
	if ( ! exists $tb_ok{$_} || $tb_ok{$_} !~ /p/ ) {
		print "effacement du répertoire '$_'\n" if ($verbose);
		if ($nodebug) {
			rmdir( $_ );
			if ( -d $_ ) {
				print STDERR "SE\t$_\n";
			} else {
				print STDERR "SR\t$_\n";
			}
		}
	}
}
close( DL);

# ...en base de consultation
open( DL,"find CONSULTATION -depth -type d -print |");
while(<DL>) {
	chop;
	if ( ! exists $tb_ok{$_} || $tb_ok{$_} !~ /c/ ) {
		print "effacement du répertoire '$_'\n" if ($verbose);
		if ($nodebug) {
			rmdir( $_ );
			if ( -d $_ ) {
				print STDERR "SE\t$_\n";
			} else {
				print STDERR "SR\t$_\n";
			}
		}
	}
}
close( DL);

# ...et en base de référence, où il faut ne pas tenir compte des
# sous-répertoires des répertoires se terminant pas "_DALLES" !
open( DL,"find REF_EXT -depth -type d -print |");
while(<DL>) {
	chop;
	if ( ! exists $tb_ok{$_} || $tb_ok{$_} !~ /r/ ) {
		if (! /_DALLES\/.*/) {
			print "effacement du répertoire '$_'\n" if ($verbose);
			if ($nodebug) {
				rmdir( $_ );
				if ( -d $_ ) {
					print STDERR "SE\t$_\n";
				} else {
					print STDERR "SR\t$_\n";
				}
			}
		}
	}
}
close( DL);

# Vérification des entrées de COUGRA.XML

if ($verbose) {
	print "\nVérification des entrées de COUGRA.XML :\n" ;
	print "----------------------------------------\n" ;
}

my $modif_cg = 0;	# au départ, pas de modification nécessaire
my %t_chem;		# déclaration des tables utilisées
my %t_noms;
my %t_trouve;

# on cherche tous les .TAB de la GéoBASE pour en faire une table
# et on vérifiera ensuite leur présence dans COUGRA.XML
# l'ordre des répertoire est obligatoire, car un fichier trouvé
# dans PRODUCTION doit effacer l'entrée de CONSULTATION puisque dans
# COUGRA.XML c'est dans PRODUCTION qu'elle est définie...

# On exclut les fichiers qui commencent par un . :
# ces fichiers sont créés par l'antivirus ou l'agent de sauvegarde

open( FL, "find $dir_cons $dir_prod $dir_ref -name '*.[Tt][Aa][Bb]' -not -name '.*' -print |");
while(<FL>) {
	chomp;			# élimination de la fin de ligne
	my $chem=$_;		# on garde le nom réel du fichier
	s/\.[Tt][Aa][Bb]$//;	# nom dans COUGRA sans .TAB
	s/.*\///;
	my $index = uc;		# on passe le nom en majuscule
	$t_chem{$index} = $chem;# liste des chemins
	if ($chem =~ /_DALLES\//) {	# on note les dalles images
		$t_trouve{$index} = 1;
	} else {
		$t_trouve{$index} = 0;
	}
}
close( FL);

termine( "XM\tImpossible de trouver $cougra !!!" ) if (! -e $cougra );

# Lecture de COUGRA.XML
my $cg = new XML::Parser( Style => 'Objects');
my $tree = $cg->parsefile( $cougra);

# le tag principal doit être <TABLE NOM="COUGRA">
my ($head) = $tree->[0];
my $typ_xml = ref $head;
my $attrs = {%$head};
delete $attrs->{Kids};
my $nom = ${$attrs}{'NOM'};

if ($typ_xml ne 'main::TABLE' || $nom ne 'COUGRA') {
	print STDERR "XM\tCOUGRA.XML mal configuré = $typ_xml [";
	print STDERR join(', ', map { "$_: $attrs->{$_}" } keys %{$attrs});
	print STDERR "] - Vérifier avec la sauvegarde COUGRA.BAK\n";
	termine( "" );
}

# table pour les données en sortie.
my @cougra_out = ();

# parcours des entrées de la table...
#
foreach my $entry (@{$head->{Kids}}) {
	my ($node) = $entry;
	my $type = ref $node;
	$type =~ s/^.*:://;
	next if ($type eq 'Characters');
	
	$attrs = {%$node};
	my $base = ${$attrs}{CHDESC_LB};
	if ($base eq 'C' ) {
		$base = 'CONSULTATION/';
	} elsif ($base eq 'P' ) {
		$base = 'PRODUCTION/';
	} elsif ($base eq 'R' || $base eq 'L' ) {
		$base = 'REF_EXT/';
	} else {
		$base = '?_inconnu_?/';
	}
	my $chemin = ${$attrs}{CHEMIN_LB};
	$chemin = '?_inconnu_?' if (length($chemin) == 0 );
	
	my $couche = ${$attrs}{COUGRA_RFA};
	my $fichier = $base . $chemin . "/" .$couche;
	$fichier =~ s/\\/\//g;		# évite les "\"
	$fichier =~ s/\/\.\//\//g;	# pour POSCOM...
	$fichier =~ s/\/\//\//g;	# évite les "//"
	
	if (length($couche) == 0) {	# impossible à corriger !
		$fichier .= '?_inconnu_?';
		print "Entrée invalide effacée : '$fichier'\n" if ($verbose);
		print STDERR "EI\t$fichier\n";
		$modif_cg++;
		next;
	}
	my $index = uc $couche;			# indice de table en majuscules...
	if ( exists $t_trouve{$index} ) {	# Fichier trouvé
		$t_trouve{$index} = 1;		# on le marque
		my $filepath = $t_chem{$index};
		$filepath =~s/\.[Tt][Aa][Bb]$//;
		
		# nom dans COUGRA sans .TAB
		if ($filepath ne $fichier ) {
			# nom, base ou chemin mal notés... on corrige
			print "Entrée modifiée : '$fichier' -> '$filepath'\n" if ($verbose);
			print STDERR "EM\t$fichier -> $filepath\n";
			$modif_cg++;
			if ($filepath =~ /^CONSULTATION\//) {
				${$attrs}{CHDESC_LB} = 'C';
			} elsif ($filepath =~ /^PRODUCTION\//) {
				${$attrs}{CHDESC_LB} = 'P';
			} elsif ( ${$attrs}{CHDESC_LB} ne 'L' ) {
				${$attrs}{CHDESC_LB} = 'R';
			}
			$filepath =~ s/^[A-Z_]+\///;
			$couche = $filepath;
			$couche =~ s/.*\///;
			$filepath =~ s/\/*[^\/]*$//;
			if (length( $filepath ) > 0) {
				$filepath =~ s/\//\\/g;
				${$attrs}{CHEMIN_LB} = $filepath;
			} else {
				${$attrs}{CHEMIN_LB} = '.';
			}
			${$attrs}{COUGRA_RFA} = $couche;
		}

		# Entrée pour une dalle (à supprimer)
		my $content = '';
		foreach my $subnode (@{$node->{Kids}}) {
			my $type = ref ($subnode);
			$type =~ s/^.*:://;
			if ($type eq 'Characters') {
				$content = $subnode->{Text};
				$content =~ s/\n/ /g;
				$content =~ s/^[\t\s]+//;
				$content =~ s/[\t\s]+$//;
				${$attrs}{Desc} = $content;
			}
		}
		if ( $filepath =~ /_DALLES\//  && $content =~ /automatiquement =>/ ) {
			$modif_cg++;
			print STDERR "EE\t$fichier\n";
			print "Entrée supprimée : '$fichier'\n" if ($verbose);
		} else {
			push @cougra_out, { %{$attrs} };
		}
	} else {
		$modif_cg++;	# On se contente de compter...
		print STDERR "EE\t$fichier\n";
		print "Entrée supprimée : '$fichier'\n" if ($verbose);
	}
}

# Les fichiers pour lesquels la valeur dans %t_trouve est 0 sont à créer...
#
foreach my $index ( keys %t_trouve ) {
	next if ( $t_trouve{$index} );
	my $filepath = $t_chem{$index};
	$filepath =~s/\.[Tt][Aa][Bb]$//;	# nom dans COUGRA sans .TAB
	print "Entrée ajoutée : '$filepath'\n" if ($verbose);
	print STDERR "EA\t$filepath\n";
	$modif_cg++;
	my $date_deb = sprintf("%02d/%02d/%04d", $mday, $mon+1, $year+1900);
	my %new_attrs = (
		DEB_DT    => $date_deb,
		FIN_DT    => '',
		ECHMIN_NB => '',
		ECHMAX_NB => '',
		ACTU_BL   => 'NON',
		VALID_BL  => 'OUI',
		FREQ_LB   => '',
		HISTO_BL  => 'NON',
	);

	# On remplit le chemin...
	if ($filepath =~ /^CONSULTATION\//) {
		$new_attrs{CHDESC_LB} = 'C';
	} elsif ($filepath =~ /^PRODUCTION\//) {
		$new_attrs{CHDESC_LB} = 'P';
	} else {
		$new_attrs{CHDESC_LB} = 'R';
	}
	$filepath =~ s/^[A-Z_]+\///;
	my $couche = $filepath;
	$couche =~ s/.*\///;
	$filepath =~ s/\/*[^\/]*$//;
	if (length( $filepath ) > 0) {
		$filepath =~ s/\//\\/g;
		$new_attrs{CHEMIN_LB} = $filepath;
	} else {
		$new_attrs{CHEMIN_LB} = '.';
	}
	$new_attrs{COUGRA_RFA} = $couche;
	
	# Il reste à trouver le type d'objets de la couche...
	
	$new_attrs{TYPE_CDA} = lire_type ( $t_chem{$index} );

	# Description par défaut
	$new_attrs{Desc} = "Entrée ajoutée automatiquement => à vérifier !";
	# Et il reste à remplir la structure XML
	push @cougra_out, { %new_attrs };

}

# mise à jour éventuelle du fichier COUGRA.XML
maj_cougra() if ($modif_cg);

termine( "OK\tVérification du COUGRA terminée" );

# Determination du type du fichier .TAB
sub lire_type {
	my $filename = shift;
	open( TAB, "< $filename" ) || return '';
	while( <TAB> ) {
		return 'R' if /IsSeamless.*TRUE/; # pas vrai dans tous les cas !
		return 'R' if /Type.*RASTER/;
	}
	close( TAB);
	
	my $filetemp = '/tmp/test.mif';
	system( $tab2tab, $filename, $filetemp );
	return '' if $?;	# pas de réponse possible...
	
	open( TMP, "< $filetemp" ) || return '';

	my $point = 0;
	my $line = 0;
	my $surf = 0;
	my $text = 0;
	while( <TMP> ){
		$point++ if /^Point/;
		$line++  if /^Pline/;
		$line++  if /^Line/;
		$line++  if /^Arc/;
		$surf++  if /^Region/;
		$surf++  if /^Ellipse/;
		$surf++  if /^Rect/;
		$surf++  if /^Roundrect/;
		$text++  if /^Text/;
	}
	close( TMP);
	unlink $filetemp, '/tmp/test.mid';
	
	# retourne '' si aucun objet et 'M' si couche mixte
	my $type = '';
	$type = 'P' if $point > 0;
	if ( $line > 0 ) {
		return 'M' if $type ne '';
		$type = 'L';
	}
	if ($surf > 0) {
		return 'M' if $type ne '';
		$type = 'S';
	}
	if ($text > 0 ) {
		return 'M' if $type ne '';
		$type = 'T'
	}
	# Pour renvoyer le type majoritaire, decomenter les 4 lignes suivantes
	# et commenter les lignes précédentes...
	# my $type = '';
	# $type = 'P' if $point > 0;
	# $type = 'L' if $line > $point;
	# $type = 'S' if $surf > $point && $surf > $line;
	# $type = 'T' if $text > $point && $text > $line && $text > $surf;
	
	return $type;
}

# Mise à jour du fichier COUGRA.XML
sub maj_cougra {

	# création du nouveau fichier XML
	open( CF, "> $cougra_new") || termine( "XM\tImpossible de modifier COUGRA.XML");
	print CF "<?xml version=\"1.0\" encoding=\"iso-8859-1\" standalone=\"yes\"?>\n";
	print CF "<?xml-stylesheet href=\"COUGRA.XSL\" type=\"text/xsl\" ?>\n";
	print CF "<!-- Contenu de la GéoBASE -->\n";
	print CF "<TABLE NOM=\"COUGRA\">\n";

	for my $entry (@cougra_out) {
		print CF "\t<ENTRY";
		for my $key ( 'COUGRA_RFA', 'TYPE_CDA', 'CHDESC_LB', 'CHEMIN_LB', 'DEB_DT', 'FIN_DT', 'ECHMIN_NB', 'ECHMAX_NB', 'ACTU_BL', 'VALID_BL', 'FREQ_LB', 'HISTO_BL') {
		print CF " $key=\"", $$entry{$key}, "\"" ;
		}
		if ( exists $$entry{Desc} ) {
			print CF ">\n", $$entry{Desc}, "</ENTRY>\n";
		} else {
			print CF "></ENTRY>\n";
		}
	}

	print CF "</TABLE>\n";
	close( CF);

	# sauvegarde de l'ancien fichier (pas d'historique)
	print "Mise à jour de $cougra après sauvegarde dans $cougra_bak\n" if $verbose ;
	system("mv", "-f", "$cougra", "$cougra_bak" );
	termine ("XM\tImpossible de créer COUGRA.BAK ($?)") if $?;
	# remplacement de cougra par la nouvelle version
	system("mv", "-f", "$cougra_new", "$cougra" );
	termine ("XM\tImpossible de remplacer COUGRA.XML ($?)") if $?;
}

# A faire à la fin du processus...
sub termine {
    my $msg = shift;
    print STDERR "$msg\n" if ( length( $msg ) > 0 );
    close(STDERR) || die "Impossible de fermer le fichier d'erreur : $!";
    unlink($err) if ((stat($err))[7] == 0) ;
    
    exit;
}

exit;
