#! /usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################################
#
# Eole NG
# Copyright Pole de Competence Eole (Ministere Education - Academie Dijon)
# Licence CeCill  http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
# eole@ac-dijon.fr
#
###########################################################################
import sys
from os.path import isfile
from pyeole.ihm import question_ouinon
from creole.client import CreoleClient
from creole.utils import print_red, print_green, print_orange
from zephir import lib_zephir

def format_current_date(format="%Y%m%d"):
    """
    renvoie la date du jour formatée
    repris de scribe/eoletools.py
    """
    from datetime import datetime
    now = datetime.today()
    return datetime.strftime(now, format)

def err_msg(message):
    """
    Sortie avec message d'erreur
    """
    print_red("Erreur : ", newline=False)
    print message
    print
    sys.exit(1)

def readdefault(message, valeur):
    """
    Question avec valeur proposée
    """
    print message
    res = raw_input('[%s] : ' % valeur)
    if not res:
        return valeur
    else:
        return res

def get_protocole(ldap_ssl):
    """
    Définition du protocole à utiliser
    ldap vs ldaps
    """
    if ldap_ssl == 'non':
        print_orange("LDAP n'est pas activé sur le port SSL : utilisation du protocole ldap (port 389)")
        proto = ('ldap', '389')
    elif ldap_ssl == 'uniquement':
        print_orange("LDAP est activé uniquement sur le port SSL : utilisation du protocole ldaps (port 636)")
        proto = ('ldaps', '636')
    elif question_ouinon("Utiliser le protole ldaps (port 636) pour la réplication", default='oui') == 'oui':
        proto = ('ldaps', '636')
    else:
        proto = ('ldap', '389')
    return proto

def send_zephir(conf_data):
    """
    Envoi de la configuration à Zephir pour prise en compte par le serveur de réplication
    """
    from zephir.zephir_conf.zephir_conf import id_serveur, adresse_zephir
    print_green("\nEnvoi de la configuration sur Zephir")
    import xmlrpclib, getpass, base64
    try:
        login = ""
        con_ok = False
        zephir_proxy = None
        while not con_ok:
            # saisie des informations de connexion
            login = raw_input("\nVeuillez saisir votre identifiant Zéphir (rien pour annuler l'envoi) : ")
            if login == "":
                return False
            passwd = getpass.getpass('Mot de passe pour %s : ' % login)
            zephir_proxy = xmlrpclib.ServerProxy("https://%s:%s@%s:7080" % (login, passwd, adresse_zephir), transport=lib_zephir.TransportEole())
            # test des identifiants
            try:
                res = lib_zephir.convert(zephir_proxy.get_permissions(login))
                assert res[0] == 1
                con_ok = True
            except:
                print_red('Erreur de connexion au serveur ou indentifiants incorrects')
        # saisie du serveur de réplication
        num_serv = ""
        serv_ok = False
        while not serv_ok:
            num_serv = raw_input("\nIdentifiant Zéphir du serveur de réplication (rien pour annuler l'envoi) : ")
            if num_serv == "":
                return False
            # vérification des données
            try:
                res = lib_zephir.convert(zephir_proxy.uucp.add_replication(int(num_serv), id_serveur, base64.encodestring(conf_data)))
                # Envoi des données de configuration
                if res[0] != 1:
                    print_red("Erreur lors de l'envoi à zephir : %s" % str(res[1]))
                else:
                    serv_ok = True
                print_green("""Cette configuration sera prise en compte par le serveur
de réplication lors de sa prochaine connexion à Zéphir""")

            except xmlrpclib.ProtocolError:
                print_red('Serveur %s non retrouvé ou permissions insuffisantes' % num_serv)
            except Exception, e:
                print_red("Erreur lors de l'envoi de la configuration à Zéphir : %s" % str(e))
    except:
        # erreur de récupération
        sys.exit("Erreur d'envoi de la configuration sur zephir")

dico = CreoleClient().get_creole()
ldif = "/root/annuaire-%s.ldif" % format_current_date()
conf = "/root/replication-%(numero_etab)s.conf" % dico
replicator = "/root/.reader"
module = dico['eole_module']
print
## test de la vestion d'openldap
#cmd = "dpkg-query -W -f='${Version}' 'slapd'"
#slapd = Popen(cmd, shell=True, stdout=PIPE).stdout.read()
#if 'eole' not in slapd:
#    err_msg("Mise à jour du paquet slapd nécessaire")

# test du dictionnaire
if dico.get('ldap_replication', '') != 'oui':
    err_msg("""Vous devez d'abord activer la réplication ldap
dans l'interface de configuration du module (mode expert)
et reconfigurer votre serveur""")

# test de la configuration réelle
if file('/etc/ldap/slapd.conf').read().find('syncprov') == -1:
    err_msg("""Votre serveur est mal paramétré, lancez la commande reconfigure""")

if not isfile(replicator):
    replicator_pwd = ''
else:
    replicator_pwd = file(replicator).read().strip()
if replicator_pwd != '':
    print_green("Utilisation du compte de réplication existant")
    print
else:
    err_msg("impossible de récupérer le mot de passe du compte de réplication")

print_green("Génération de la configuration client")

# objectClass concernées par la réplication
if module == 'scribe':
    group = 'ENTGroupe'
    person = 'ENTPerson'
else:
    group = 'posixGroup'
    person = 'inetOrgPerson'
if question_ouinon("Répliquer également les groupes", 'non') == 'oui':
    group_filter = '(|(objectClass={0})(objectClass={1}))'.format(person, group)
else:
    group_filter = '(objectClass={0})'.format(person)

# uid concernées par la réplication
user_filter = '(!(uid=admin))'
if question_ouinon("Ajouter des uid à exclure de la réplication", 'non') == 'oui':
    while True:
        user = raw_input("uid à exclure (entrée pour terminer la saisie) : ")
        if user == '':
            break
        user_filter += '(!(uid=%s))' % user

msg = "Adresse utilisée pour accéder au module {0} depuis le client"
adresse_scribe = readdefault(msg.format(module.capitalize()), dico['adresse_ip_eth0'])
protocole, port = get_protocole(dico['ldap_ssl'])
libelle = dico['libelle_etab']
numero = dico['numero_etab']
acad = dico['nom_academie']
dn = dico['ldap_base_dn']
search_filter = "(&%s%s)" % (group_filter, user_filter)
if protocole == 'ldaps':
    tls_reqcert = "\n        tls_reqcert=never"
else:
    tls_reqcert = ""
print
print_green("Ecriture du fichier %s" % conf)
tmpl = """# {0} ({1})
syncrepl rid=%i
        provider={2}://{3}:{4}{9}
        type=refreshAndPersist
        interval=00:01:00:00
        retry="60 10 300 +"
        searchbase="ou={1},ou={5},ou=education,{6}"
        filter="{8}"
        scope=sub
        schemachecking=off
        bindmethod=simple
        binddn="cn=reader,{6}"
        credentials={7}

""".format(libelle, numero, protocole, adresse_scribe, port,
           acad, dn, replicator_pwd, search_filter, tls_reqcert)
fic = file(conf, 'w')
fic.write(tmpl)
fic.close()
# La configuration est générée localement, on l'envoie via zephir si possible
if lib_zephir.registered == 1:
    send_zephir(tmpl)
print
print "fin"
