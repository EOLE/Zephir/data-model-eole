#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Mise à jour du mot de passe LDAP
# Equipe Eole eole@ac-dijon.fr

import sys
from glob import glob
from os.path import join
from pyeole.process import system_code
from creole.client import CreoleClient
from pyeole.service import service_code
from pyeole.ssha import ssha_encode
from pyeole.dict4ini import DictIni

dico = CreoleClient()

# on demande le nouveau mot de passe
# si il n'est pas passé comme paramètre
try:
    new_pass = sys.argv[1]
    #reindex = True
except:
    new_pass = ""
    while len(new_pass) < 5:
        if new_pass != "":
            print "le mot de passe est trop court (5 caractères au moins)"
        new_pass = raw_input("\nNouveau mot de passe pour l'admin LDAP ? ")
    #reindex = False

# récupération du mot de passe chiffré
crypt_pwd = ssha_encode(new_pass)

container_mode = dico.get_creole('mode_conteneur_actif', 'non') == 'oui'

# recupération de la liste des fichiers à modifier
modifs = {}
password_files = glob("/usr/share/eole/annuaire/password_files/*.ini")
for fic in password_files:
    for cle, opts in DictIni(fic):
        # Work on container group
        container = opts.get('container', 'root')
        group = dico.get_container(container)['group']
        opts['container'] = group
        if cle not in modifs:
            modifs[cle] = opts
            modifs[cle]['container'] = [opts['container']]
        elif container_mode:
            # hack pour les fichier multi-conteneurs
            if group not in modifs[cle]['container']:
                modifs[cle]['container'].append(group)

samba = False
# parcours du dictionnaire des fichiers
for name, option in modifs.items():

    # lecture des options
    chaines_recherche = option['string']
    container_list = option.get('container', ['root'])
    short_path = join(option['path'], name)
    if not isinstance(chaines_recherche, list):
        chaines_recherche = [chaines_recherche]

    smb = dico.get_creole('smb_netbios_name', False)
    if smb:
        samba = True

    for container in container_list:
        prefix = dico.get_creole('container_path_{0}'.format(container))
        file_path = prefix+short_path

        print "\nMise à jour du fichier %s..." % str(file_path)
        for chaine_recherche in chaines_recherche:

            # lecture du fichier de conf
            conf_file = open(file_path)
            buffer_in = conf_file.readlines()
            conf_file.close()

            buffer_out = ""
            # on parcourt le fichier
            for line in buffer_in:
                # si la ligne actuelle contient la chaine recherchée
                if line.startswith(chaine_recherche):
                    print "définition du mot de passe trouvée : %s" % line.strip()
                    # on ajoute les possibles tabulations ou espaces en début de ligne
                    buffer_out += line[0:line.index(chaine_recherche)]
                    buffer_out += chaine_recherche
                    if name == 'slapd.conf':
                        buffer_out += crypt_pwd
                    else:
                        buffer_out += new_pass
                    if chaine_recherche.endswith('"'):
                        # si le mot de passe est entre guillemets, on ajoute le guillemet fermant
                        buffer_out += '"'
                    if name.endswith('.pl') or name.endswith('.pm') or name.endswith('.php'):
                        buffer_out += ';'
                    elif name == 'lsc.xml':
                        #FIXME: hack for lsc.xml (eole-ad)
                        buffer_out += '</password>'
                    buffer_out += "\n"
                else:
                    # sinon, on recopie la ligne d'origine
                    buffer_out += line

            # sauvegarde du fichier de conf
            conf_file = open(file_path,"w")
            conf_file.write(buffer_out)
            conf_file.close()

        rights = option.get('rights')
        if rights:
            cmd = ['chown', rights, short_path]
            system_code(cmd, container=container)
        mode = option.get('mode')
        if mode:
            system_code(['chmod', mode, file_path])

# mise à jour du mot de passe LDAP pour samba
if samba:
    print "\nMise à jour de Samba .."
    cmd = ["smbpasswd", '-w', new_pass]
    ret = system_code(cmd, container='fichier')
    if ret != 0:
        raise Exception, 'Erreur à la mise à jour Samba : %s' % str(ret[1:])

print "\nredémarrage des services, veuillez patienter ..."
# visiblement il vaut mieux le faire à chaque fois surtout pour ldap !
service_code('slapd', 'restart', 'annuaire')
#if reindex:
#    print "réindexation de l'annuaire..."
#    cmd = ['/usr/bin/sudo', '-u', 'openldap', '/usr/sbin/slapindex']
#    system_res(cmd)
if samba:
    service_code('smbd', 'restart', 'fichier')
if dico.get_creole('activer_courier', 'non') != 'non':
    service_code('courier-authdaemon', 'restart', 'mail')
