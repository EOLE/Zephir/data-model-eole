#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2011
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# fluxbb.py
#
# Création de la base de données mysql de fluxbb
#
###########################################################################
"""
    Config pour fluxbb
"""
from eolesql.db_test import db_exists, test_var

FLUXBB_TABLEFILENAMES = ['/usr/share/eole/mysql/fluxbb/gen/fluxbb-create-0.sql',
                         '/usr/share/eole/mysql/fluxbb/gen/fluxbb-create-1.sql']
def test():
    """
        test l'existence de la base fluxbb
    """
    return test_var('activer_fluxbb') and not db_exists('fluxbb')

conf_dict = dict(filenames=FLUXBB_TABLEFILENAMES,
                 test=test)
