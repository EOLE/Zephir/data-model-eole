#-*-coding:utf-8-*-
"""
    Update systématique de la base fluxbb (à chaque reconfigure)
"""
from eolesql.db_test import test_var

DB = "fluxbb"
FILENAMES = ['/usr/share/eole/mysql/fluxbb/updates/fluxbb-update.sql']

def test_response(result=None):
    """
        renvoie True si fluxbb est activé
    """
    return test_var('activer_fluxbb')

conf_dict = dict(db=DB,
                 expected_response=test_response,
                 filenames=FILENAMES,
                 version='Mise à jour de fluxbb'
                 )
