-- création de la base de donnée
CREATE DATABASE fluxbb DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

-- création du user de la base
grant all privileges on fluxbb.* to fluxbb@%%adresse_ip_web identified by 'fluxbb';
flush privileges ;
