\r fluxbb

%if %%mode_conteneur_actif != "non"
grant all privileges on fluxbb.* to 'fluxbb'@'%%adresse_ip_br0' identified by 'fluxbb';
%end if

UPDATE `flux_config` SET `conf_value` =  'admin@%%domaine_messagerie_etab' WHERE conf_name = 'o_mailing_list';
UPDATE `flux_config` SET `conf_value` =  'admin@%%domaine_messagerie_etab' WHERE conf_name = 'o_admin_email';
UPDATE `flux_config` SET `conf_value` =  'admin@%%domaine_messagerie_etab' WHERE conf_name = 'o_webmaster_email';
UPDATE `flux_config` SET `conf_value` =  'https://%%web_url/fluxbb' WHERE conf_name = 'o_base_url';


-- UPGRADE 1.4.3 TO 1.5.3 ------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS 143to153;
DELIMITER |
CREATE PROCEDURE 143to153()
BEGIN
	DECLARE version VARCHAR(64);
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE curversion CURSOR FOR SELECT conf_value FROM flux_config WHERE conf_name='o_cur_version';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN curversion;
	read_loop: LOOP
		FETCH curversion INTO version;
		IF done THEN
			LEAVE read_loop;
		END IF;

	END LOOP;

	CLOSE curversion;

	IF version < "1.5.3" THEN
		UPDATE flux_config SET conf_value = '1.5.3' WHERE conf_name = 'o_cur_version';
		UPDATE flux_config SET conf_value = '18' WHERE conf_name = 'o_database_revision';
		UPDATE flux_config SET conf_value = '2' WHERE conf_name = 'o_parser_revision';
		UPDATE flux_config SET conf_value = '2' WHERE conf_name = 'o_searchindex_revision';

		INSERT IGNORE INTO flux_config (conf_name, conf_value) VALUES ('o_feed_ttl', '0');
		DELETE FROM flux_config WHERE conf_name='o_ranks';


		ALTER TABLE flux_groups ADD g_promote_min_posts INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER g_user_title;
		ALTER TABLE flux_groups ADD g_promote_next_group INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER g_promote_min_posts;
		ALTER TABLE flux_groups ADD g_post_links TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER g_delete_topics;
		ALTER TABLE flux_groups ADD g_report_flood SMALLINT( 6 ) NOT NULL DEFAULT '60' AFTER g_email_flood;

		ALTER TABLE flux_users ADD  last_report_sent INT( 10 ) UNSIGNED DEFAULT NULL AFTER last_email_sent;

		UPDATE flux_groups SET g_send_email = 0 WHERE g_id = 3;
		UPDATE flux_groups SET g_email_flood = 0, g_report_flood = 0 WHERE g_id IN (1,2,3);

		DROP TABLE flux_ranks;
	END IF;

END|
DELIMITER ;

CALL 143to153();
DROP PROCEDURE 143to153;
