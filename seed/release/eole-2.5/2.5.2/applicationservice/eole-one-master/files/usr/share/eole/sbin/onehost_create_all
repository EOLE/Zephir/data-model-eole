#!/usr/bin/env bash

#
# Register all Hâpy Cluster Nodes
#

. /usr/lib/eole/ihm.sh

#
# NAME: copy_ssh_id 
# AIM: Copy the ssh key on the host
# PARAM: the hostname of the node 
#
function copy_ssh_id()
{
    local ip=${1}
    su - ${ONEUSER} -c "ssh-copy-id ${ip}"
}

#
# NAME: register_node
# AIM: register the node in OpenNebula master
# PARAM: the node hostname
#
function register_node()
{
    cmd="onehost create -i kvm -v kvm -n ovswitch -c \"$(CreoleGet one_cluster_name)\" ${1}"
    ret=$(su - ${ONEUSER} -c -- "${cmd}")
    return $?
}

#
# NAME: wait_node_ok
# AIM: Wait until the node is OK or ERROR
# PARAM: The node name 
#
function wait_node_ok()
{
    local RET=0
    local cmd="onehost show ${1} | awk '/^STATE/ {print \$3}'"
    local spinstr='|/-\'
    local delay=0.75

    while [ 1 ]
    do
        st=$(su - ${ONEUSER} -c "${cmd}")
        [[ ${st} == "MONITORED" ]] && break
        [[ ${st} == '' ]] && break
        if [[ ${st} == "ERROR" ]]
        then
            RET=2
            break
        fi

        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"

    done
    printf "    \b\b\b\b"
    return ${RET}
}

#
# MAIN
#
HAPY_ACTIF=$(echo $(CreoleGet activer_onesinglenode))
if [[ $HAPY_ACTIF == "non" ]]
then
	EchoRouge "Le serveur de virtualisation n'est pas activé dans l'interface de configuration du module"
	exit 1
fi

HAPY_NODE_SUPPORT=$(echo $(CreoleGet activer_multinode))
if [[ $HAPY_NODE_SUPPORT == "non" ]]
then
	EchoRouge "Le mode multi-noeuds n'est pas activé dans l'interface de configuration du module"
	exit 1
fi
declare -a HAPY_SLV=('')
declare -a HAPY_SLV_IP=('')
ONEUSER=$(CreoleGet virt_user)
HAPY_SLV=$(echo $(CreoleGet one_nodes) | sed -e "s/\n/ /g")
HAPY_SLV_IP=$(echo $(CreoleGet node_ip) | sed -e "s/\n/ /g")

echo -e "\n"
EchoBleu "Vous allez inscrire un noeud dans une grappe Hâpy"
EchoBleu "Pour ce faire vous devez vous munir du mot de passe de l'utilisateur 'oneadmin' de chacun des noeuds"
Question_ouinon  "Voulez-vous commencer ?" 'True' "oui"
if [[ $? -ne 0 ]]
then
	EchoOrange "Abandon de l'enregistrement"
	exit 1
fi

for ((i = 0; i < ${#HAPY_SLV[*]}; i +=1))
do
    echo -e "\n"
    EchoOrange "Traitement du noeud ${HAPY_SLV[${i}]}"
    echo
    EchoVert " * Gestion des clés SSH"
    echo
    copy_ssh_id ${HAPY_SLV_IP[${i}]}
    EchoVert " * Enregistrement du noeud"
    register_node ${HAPY_SLV[${i}]}
    wait_node_ok ${HAPY_SLV[${i}]}
    if [[ ${?} -ne 0 ]]
    then
        EchoRouge "Erreur lors de l'enregistrement du noeud ${HAPY_SLV[${i}]} !"
    fi
done

echo -e "\n"
EchoVert "Enregistrement des noeuds terminé"
