#!/usr/bin/env python
"""Module jabber"""
import sys
from shutil import copyfile
from os.path import isfile
from os import remove
from creole.client import CreoleClient
from pyeole.process import system_code
from pyeole.service import manage_service
from pyeole.bareosrestore import bareos_restore_one_file, exit_if_running_jobs

jabfile = '/home/backup/ejabberd'
filename = "/tmp/ejabberd.tmp"
dico_eole = CreoleClient()

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """jabber helper"""
    if dico_eole.get_creole('activer_ejabberd') == 'non':
        sys.exit(0)
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)

def pre():
    print "pre jabber"

def post():
    """
    Backup of jabber database : ejabberdctl is in container while database is in
    /home/backup on host.
    ejabberdctl cannot reach database in /home/backup on host. Database must be
    copied first in relevant container to be processed.
    """
    print "post jabber"
    path = dico_eole.get_creole('container_path_jabber')
    destdir = path + filename
    copyfile(jabfile, destdir)
    system_code(["/bin/chown", "ejabberd", filename], container='jabber')
    system_code(["/usr/sbin/ejabberdctl", "install_fallback", filename], container='jabber')
    remove(destdir)
    manage_service('restart', 'ejabberd', container='jabber')

def job(jobid):
    print "Restauration de la base ejabberd"
    bareos_restore_one_file(jabfile, jobid)

priority = 20
