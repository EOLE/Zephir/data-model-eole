<?xml version="1.0" encoding="utf-8"?>

<creole>
    <files>
        <!-- le service nginx est déclaré dans 28_eoleflask.xml -->
        <service_access service='nginx'>
            <port service_accesslist="nginx">80</port>
            <port service_accesslist="nginx">443</port>
        </service_access>
        <file name='/etc/nginx/sites-enabled/default' source='nginx.default'/>
        <file name='/usr/share/nginx/html/nginx.html'/>
        <file name='/var/www/index.html' source='nginx.no_proxy.html' mkdir='True'/>
        <file name='/etc/logrotate.d/nginx' source='nginx.logrotate'/>
        <!--<file name="/etc/nginx/sites-enabled/access_proxy"/>-->
    </files>

    <variables>

        <family name='services'>
            <!-- Nginx fait serveur HTTP même si la fonctionnalité reverse proxy est désactivée -->
            <variable name='activer_revprox' type='oui/non' description='Activer le reverse proxy Nginx'>
                <value>non</value>
            </variable>
            <variable name='activer_apache' type='oui/non' exists='False' hidden='True'>
                <value>non</value>
            </variable>
        </family>

        <family name='Reverse proxy' icon='nginx'>
            <variable name='revprox_default' type="domain_strict" description="Nom de domaine par défaut" />
            <variable name='revprox_sso' type='domain' description="Nom de domaine du serveur SSO" />
            <variable name='revprox_eop' type='domain' description="Nom de domaine du serveur EOP" />
            <variable name='activer_revprox_ead' type='oui/non' description="Activer la redirection de l'EAD Scribe">
                <value>non</value>
            </variable>
            <variable name='revprox_ead' type='ip' description="IP du Scribe pour la redirection EAD" mandatory="True"/>
            <variable name='revprox_ead_port' type='port' description="Port de l'EAD sur le Scribe" mandatory="True">
                <value>4203</value>
            </variable>
            <variable name='revprox_auto_config_local_web' type='oui/non' description='Activer la configuration automatique pour les applications locales'>
                <value>non</value>
            </variable>
            <variable name='revprox_activate_http' type='oui/non' description='Activer le reverse proxy Nginx pour http/https'>
                <value>non</value>
            </variable>

            <variable name='revprox_domainname' type='domain' description="Nom de domaine ou IP à rediriger" multi='True' mandatory='True'/>
            <variable name='revprox_rep' type='string' description="Répertoire ou nom de la page à rediriger">
                <value>/</value>
            </variable>
            <variable name='revprox_http' type='string' description="Reverse proxy HTTP">
                <value>redirige vers https</value>
            </variable>
            <variable name='revprox_https' type='oui/non' description="Reverse proxy HTTPS">
                <value>oui</value>
            </variable>
            <variable name='revprox_url' type='web_address' description="IP ou domaine de destination (avec http:// ou https://) ou URI complète" mandatory='True'/>

            <variable name="activer_revprox_rewrite" type="oui/non" description="Activer la réécriture d'URL" mode="expert">
                <value>non</value>
            </variable>
            <variable name="revprox_rewrite_domaine" type="domain" description="Nom de domaine concerné par la réécriture" mode="expert" multi="True" mandatory='True'/>
            <variable name="revprox_rewrite_proto" type="string" description="Protocole" mode="expert">
                <value>http/https</value>
            </variable>
            <variable name='revprox_rewrite_location' type='string' description="Répertoire de la réécriture" mode="expert" mandatory='True'/>
            <variable name="revprox_rewrite_regex" type="string" description="Regex de la réécriture" mode="expert" mandatory='True'/>
            <variable name="revprox_rewrite_replacement" type="string" description="La valeur de remplacement" mode="expert" mandatory='True'/>
            <variable name='revprox_hash_bucket_size' type='string' description="Longueur maximum pour un nom de domaine" mode="expert">
                <value>128</value>
            </variable>
            <variable name='php_post_max_size' type='number' description="Taille maximale des données reçues par la méthode POST (en Mo)" exists="False" mode='expert' mandatory="True">
                <value>32</value>
            </variable>
        </family>
        <family name='applications web' hidden='True'>
            <!-- creer si n'existe pas et cache-->
            <variable name='activer_web_behind_revproxy' type='oui/non' exists='False' hidden='True'>
                <value>oui</value>
            </variable>
            <variable name='web_behind_revproxy_ip' type='ip' exists='False' hidden='True'/>
        </family>

        <separators>
            <separator name='revprox_sso' never_hidden='True'>Redirection de services particuliers</separator>
            <separator name='revprox_activate_http'>Redirection HTTP et HTTPS</separator>
        </separators>

    </variables>

    <constraints>
        <!-- **** Listes deroulantes **** -->
        <check name='valid_enum' target='revprox_http'>
            <param>['oui','non', 'redirige vers https']</param>
        </check>
        <check name='valid_enum' target='revprox_rewrite_proto'>
            <param>['http/https','http', 'https']</param>
        </check>
        <check name='valid_enum' target='revprox_hash_bucket_size'>
            <param>['128', '64', '32']</param>
        </check>
        <!-- **** Conditions **** -->
        <condition name='disabled_if_in' source='activer_revprox'>
            <param>non</param>
            <target type='family'>Reverse proxy</target>
            <target type='filelist'>revprox</target>
        </condition>
        <condition name='disabled_if_in' source='activer_sso' fallback='True'>
            <param>local</param>
            <target type='variable'>revprox_sso</target>
        </condition>
        <condition name='disabled_if_in' source='activer_apache'>
            <param>non</param>
            <target type='variable'>revprox_auto_config_local_web</target>
        </condition>
        <condition name='disabled_if_in' source='activer_revprox_rewrite'>
            <param>non</param>
            <target type='variable'>revprox_rewrite_proto</target>
            <target type='variable'>revprox_rewrite_domaine</target>
            <target type='variable'>revprox_rewrite_location</target>
            <target type='variable'>revprox_rewrite_regex</target>
            <target type='variable'>revprox_rewrite_replacement</target>
        </condition>
        <condition name='disabled_if_in' source='activer_revprox_ead'>
            <param>non</param>
            <target type='variable'>revprox_ead</target>
            <target type='variable'>revprox_ead_port</target>
        </condition>
        <group master='revprox_domainname'>
            <slave>revprox_rep</slave>
            <slave>revprox_http</slave>
            <slave>revprox_https</slave>
            <slave>revprox_url</slave>
        </group>
        <group master='revprox_rewrite_domaine'>
            <slave>revprox_rewrite_proto</slave>
            <slave>revprox_rewrite_location</slave>
            <slave>revprox_rewrite_regex</slave>
            <slave>revprox_rewrite_replacement</slave>
        </group>
        <condition name='disabled_if_in' source='revprox_activate_http'>
            <param>non</param>
            <target type='variable'>revprox_domainname</target>
            <target type='variable'>revprox_rep</target>
            <target type='variable'>revprox_http</target>
            <target type='variable'>revprox_https</target>
            <target type='variable'>revprox_url</target>
            <target type='service_accesslist'>nginx</target>
        </condition>
        <condition name='disabled_if_in' source='revprox_auto_config_local_web'>
            <param>non</param>
            <target type='service_accesslist'>nginx</target>
        </condition>
        <condition name='disabled_if_in' source='activer_web_behind_revproxy'>
            <param>non</param>
            <target type='variable'>web_behind_revproxy_ip</target>
        </condition>
        <fill name='calc_val' target='revprox_ead'>
            <param type='eole' name='valeur' optional='True' hidden='False'>ip_serveur_scribe_dmz</param>
        </fill>
        <auto name='calc_container' target='web_behind_revproxy_ip'>
            <param type='eole'>mode_conteneur_actif</param>
            <param type='eole'>adresse_ip_br0</param>
            <param type='eole' optional='True'>available_probes</param>
        </auto>
    </constraints>
    <!--************************************************************************************************* -->
    <help>
        <family name='Reverse proxy'>Paramètrage du proxy inverse</family>
        <variable name='revprox_default'>Si un client accède au serveur avec un nom de domaine non déclaré, le flux est redirigé vers ce domaine</variable>
        <variable name='revprox_sso'>Pour rediriger vers le SSO de la machine "domainelocal", taper "domainelocal"</variable>
        <variable name='revprox_eop'>Pour rediriger vers EOP de la machine "domainelocal", taper "domainelocal"</variable>
        <variable name='revprox_auto_config_local_web'>Configure automatiquement le reverse proxy afin de faire pointer web_url sur le conteneur web</variable>
        <variable name='revprox_domainname'>Exemple : pour rediriger "http://domaine/" saisir "domaine"</variable>
        <variable name='revprox_rep'>URL relative (sans le nom de domaine) redirigée pour l'adresse définie dans la variable ci-dessus (exemple "/mail")</variable>
        <variable name='revprox_url'>Nom de domaine ou IP de destination, par exemple "http://domainelocal" ou URI, par exemple "http://domainelocal/dir/"</variable>
        <variable name='activer_revprox'>Le Reverse Proxy permet de relayer des accès extérieurs vers des serveurs situés derrière le pare-feu</variable>
    </help>
</creole>
<!-- vim: ts=4 sw=4 expandtab
-->
