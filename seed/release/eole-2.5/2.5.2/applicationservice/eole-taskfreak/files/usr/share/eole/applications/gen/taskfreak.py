#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2011
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# taskfreak.py
#
# Création de la base de données mysql de taskfreak
#
###########################################################################
"""
    Config pour taskfreak
"""
from eolesql.db_test import db_exists, test_var

TASKFREAK_TABLEFILENAMES = ['/usr/share/eole/mysql/taskfreak/gen/taskfreak-create.sql']
def test():
    """
        test l'existence de la base taskfreak
    """
    return test_var('activer_taskfreak') and not db_exists('taskfreak')

conf_dict = dict(filenames=TASKFREAK_TABLEFILENAMES,
                 test=test)
