#-*-coding:utf-8-*-

def calc_info(user_infos):
    """
        calcule ENTPersonProfils : Profil d'utilisateur ENT
    """
    # Nomenclature nationale des profils utilisateurs
    # National_1 : Élève
    # National_2 : Responsable d'un élève (parent, tuteur légal)
    # National_3 : Enseignant
    # National_4 : Personnel de direction de l'établissement
    # National_5 : Personnel de vie scolaire travaillant dans l'établissement
    # National_6 : Personnel administratif, technique ou d'encadrement
    #              travaillant dans l'établissement
    # National_7 : Personnel de rectorat, de DRAF, de collectivité locale,
    #              d'inspection académique
    dico = {'eleve':['National_1'],
            'responsable':['National_2'],
            'professeur':['National_3'],
            'professeur_principal':['National_3'],
            'admin':['administrateur'],
            'administratif':['National_6'],
            'default':['autre'],}
    groups = user_infos.get('user_groups', [])
    typeadmin = user_infos.get('typeadmin', [''])[0]
    uid = user_infos.get('uid', [''])[0]
    objectclass = user_infos.get('objectClass', [''])
    if 'eleves' in groups:
        return dico.get('eleve', dico.get('default'))
    elif uid == 'admin' or typeadmin == '1':
        return dico.get('admin', dico.get('default'))
    elif typeadmin == '2':
        return dico['professeur_principal']
    elif typeadmin == '0' or 'professeurs' in groups:
        return dico.get('professeur',  dico.get('default'))
    elif 'responsable' in objectclass:
        return dico.get('responsable', dico.get('default'))
    elif 'administratifs' in groups:
        return dico.get('administratif', dico.get('default'))
    else:
        return dico.get('default')
