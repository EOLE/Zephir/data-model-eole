-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 24 Novembre 2009 à 02:11
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.4-2ubuntu5.7


--
-- Base de données: `taskfreak`
--

-- --------------------------------------------------------

-- Création de la base Mysql pour taskfreak
-- Equipe EOLE 2009

-- création de la base de donnée
CREATE DATABASE taskfreak;

-- création du user de la base
grant all privileges on taskfreak.* to taskfreak@%%adresse_ip_web identified by 'taskfreak';
flush privileges ;

-- connexion à la base
\r taskfreak


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Structure de la table `frk_country`
--

CREATE TABLE IF NOT EXISTS `frk_country` (
  `countryId` char(2) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`countryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `frk_country`
--

INSERT INTO `frk_country` (`countryId`, `name`) VALUES
('AF', 'Afghanistan'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and herzegovina'),
('BW', 'Botswana'),
('BV', 'Bouvet island'),
('BR', 'Brazil'),
('IO', 'British indian ocean territory'),
('BN', 'Brunei darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape verde'),
('KY', 'Cayman islands'),
('CF', 'Central african republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas island'),
('CC', 'Cocos (keeling) islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo'),
('CD', 'Congo'),
('CK', 'Cook islands'),
('CR', 'Costa rica'),
('CI', 'Cote d''ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican republic'),
('TP', 'East timor'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El salvador'),
('GQ', 'Equatorial guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FK', 'Falkland islands (malvinas)'),
('FO', 'Faroe islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GF', 'French guiana'),
('PF', 'French polynesia'),
('TF', 'French southern territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GN', 'Guinea'),
('GW', 'Guinea-bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard and mcdonald islands'),
('VA', 'Holy see (vatican city state)'),
('HN', 'Honduras'),
('HK', 'Hong kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran, islamic republic of'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JO', 'Jordan'),
('KZ', 'Kazakstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, democratic'),
('KR', 'Korea, republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Laos'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libyan arab jamahiriya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macau'),
('MK', 'Macedonia'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia'),
('MD', 'Moldova, republic of'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands antilles'),
('NC', 'New caledonia'),
('NZ', 'New zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk island'),
('MP', 'Northern mariana islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PS', 'Palestinian territory'),
('PA', 'Panama'),
('PG', 'Papua new guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian federation'),
('RW', 'Rwanda'),
('SH', 'Saint helena'),
('KN', 'Saint kitts and nevis'),
('LC', 'Saint lucia'),
('PM', 'Saint pierre and miquelon'),
('VC', 'Saint vincent and the grenadines'),
('WS', 'Samoa'),
('SM', 'San marino'),
('ST', 'Sao tome and principe'),
('SA', 'Saudi arabia'),
('SN', 'Senegal'),
('SC', 'Seychelles'),
('SL', 'Sierra leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon islands'),
('SO', 'Somalia'),
('ZA', 'South africa'),
('GS', 'South georgia'),
('ES', 'Spain'),
('LK', 'Sri lanka'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and jan mayen'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian arab republic'),
('TW', 'Taiwan, province of china'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, united republic of'),
('TH', 'Thailand'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and caicos islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UM', 'United States minor islands'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VE', 'Venezuela'),
('VN', 'Viet nam'),
('VG', 'Virgin islands, british'),
('VI', 'Virgin islands, u.s.'),
('WF', 'Wallis and futuna'),
('EH', 'Western sahara'),
('YE', 'Yemen'),
('YU', 'Yugoslavia'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Structure de la table `frk_item`
--

CREATE TABLE IF NOT EXISTS `frk_item` (
  `itemId` int(10) unsigned NOT NULL auto_increment,
  `projectId` mediumint(8) unsigned NOT NULL default '0',
  `itemParentId` int(10) unsigned NOT NULL default '0',
  `priority` tinyint(3) unsigned NOT NULL default '0',
  `context` varchar(80) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `description` text NOT NULL,
  `deadlineDate` date NOT NULL default '0000-00-00',
  `expectedDuration` smallint(5) unsigned NOT NULL default '0',
  `showInCalendar` tinyint(1) unsigned NOT NULL default '0',
  `showPrivate` tinyint(1) unsigned NOT NULL default '0',
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  `authorId` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`itemId`),
  KEY `projectId` (`projectId`),
  KEY `memberId` (`memberId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `frk_item`
--

INSERT INTO `frk_item` (`itemId`, `projectId`, `itemParentId`, `priority`, `context`, `title`, `description`, `deadlineDate`, `expectedDuration`, `showInCalendar`, `showPrivate`, `memberId`, `authorId`) VALUES
(1, 1, 0, 3, '1', 'Bravo ! Ceci est votre première tâche', '', '9999-00-00', 0, 0, 0, 1, 1),
(2, 1, 0, 5, '1', 'Comment créer un utilisateur', 'Pour créer un nouvel utilisateur, aller dans le menu <i>Gérer > Utilisateurs</i> \n\nCliquer sur l''icône <img src="skins/redfreak/images/b_new.png" />', '9999-00-00', 0, 0, 2, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `frk_itemComment`
--

CREATE TABLE IF NOT EXISTS `frk_itemComment` (
  `itemCommentId` bigint(20) unsigned NOT NULL auto_increment,
  `itemId` int(10) unsigned NOT NULL default '0',
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  `postDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `body` text NOT NULL,
  `lastChangeDate` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`itemCommentId`),
  KEY `taskId` (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `frk_itemComment`
--


-- --------------------------------------------------------

--
-- Structure de la table `frk_itemFile`
--

CREATE TABLE IF NOT EXISTS `frk_itemFile` (
  `itemFileId` bigint(20) unsigned NOT NULL auto_increment,
  `itemId` int(10) unsigned NOT NULL default '0',
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  `fileTitle` varchar(200) NOT NULL default '',
  `filename` varchar(127) NOT NULL default '',
  `filetype` varchar(30) NOT NULL default '',
  `filesize` bigint(20) NOT NULL default '0',
  `postDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastChangeDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fileTags` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`itemFileId`),
  KEY `taskId` (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `frk_itemFile`
--


-- --------------------------------------------------------

--
-- Structure de la table `frk_itemStatus`
--

CREATE TABLE IF NOT EXISTS `frk_itemStatus` (
  `itemStatusId` bigint(20) unsigned NOT NULL auto_increment,
  `itemId` int(10) unsigned NOT NULL default '0',
  `statusDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `statusKey` tinyint(3) unsigned NOT NULL default '0',
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`itemStatusId`),
  KEY `itemId` (`itemId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `frk_itemStatus`
--

INSERT INTO `frk_itemStatus` (`itemStatusId`, `itemId`, `statusDate`, `statusKey`, `memberId`) VALUES
(1, 1, '2006-06-01 00:00:00', 0, 1),
(2, 2, '2006-06-01 00:00:00', 0, 1),
(3, 3, '2006-06-01 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `frk_member`
--

CREATE TABLE IF NOT EXISTS `frk_member` (
  `memberId` mediumint(8) unsigned NOT NULL auto_increment,
  `email` varchar(120) NOT NULL default '',
  `title` varchar(20) NOT NULL default '',
  `firstName` varchar(50) NOT NULL default '',
  `middleName` varchar(50) NOT NULL default '',
  `lastName` varchar(50) NOT NULL default '',
  `zipCode` varchar(20) NOT NULL default '',
  `city` varchar(60) NOT NULL default '',
  `stateCode` char(2) NOT NULL default '',
  `countryId` char(2) NOT NULL default '',
  `phone` varchar(30) NOT NULL default '',
  `mobile` varchar(30) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `username` varchar(20) NOT NULL default '',
  `password` varchar(60) NOT NULL default '',
  `salt` varchar(8) NOT NULL default '',
  `autoLogin` tinyint(1) NOT NULL default '0',
  `timeZone` smallint(6) NOT NULL default '0',
  `expirationDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastLoginDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastLoginAddress` varchar(60) NOT NULL default '',
  `creationDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastChangeDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `visits` mediumint(8) unsigned NOT NULL default '0',
  `badAccess` tinyint(3) unsigned NOT NULL default '0',
  `level` tinyint(3) unsigned NOT NULL default '0',
  `activation` varchar(16) NOT NULL default '',
  `authorId` mediumint(8) unsigned NOT NULL default '0',
  `enabled` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`memberId`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `frk_member`
--


-- --------------------------------------------------------

--
-- Structure de la table `frk_memberProject`
--

CREATE TABLE IF NOT EXISTS `frk_memberProject` (
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  `projectId` mediumint(8) unsigned NOT NULL default '0',
  `position` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`memberId`,`projectId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `frk_memberProject`
--

INSERT INTO `frk_memberProject` (`memberId`, `projectId`, `position`) VALUES
(1, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `frk_project`
--

CREATE TABLE IF NOT EXISTS `frk_project` (
  `projectId` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(120) NOT NULL default '',
  `description` text NOT NULL,
  PRIMARY KEY  (`projectId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `frk_project`
--

INSERT INTO `frk_project` (`projectId`, `name`, `description`) VALUES
(1, 'Votre premier projet', '');

-- --------------------------------------------------------

--
-- Structure de la table `frk_projectStatus`
--

CREATE TABLE IF NOT EXISTS `frk_projectStatus` (
  `projectStatusId` int(10) unsigned NOT NULL auto_increment,
  `projectId` mediumint(10) unsigned NOT NULL default '0',
  `statusDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `statusKey` tinyint(3) unsigned NOT NULL default '0',
  `memberId` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`projectStatusId`),
  KEY `projectId` (`projectId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `frk_projectStatus`
--

INSERT INTO `frk_projectStatus` (`projectStatusId`, `projectId`, `statusDate`, `statusKey`, `memberId`) VALUES
(1, 1, '2009-11-24 00:00:00', 0, 1);
