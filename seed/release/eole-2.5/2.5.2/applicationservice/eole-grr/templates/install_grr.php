<?php
/*
* mise à jour manuelle possible par l'administrateur (un lien dans son menu pointant vers "admin_maj.php")
*
* mise à jour automatique :
* les version "RC" n'étant pas utilisés en production, on ne se base que sur la version stable
* donc conformément à ce qui est prévu par "admin_maj.php", la version à fournir doit être suffixer de ".9"
*/

$install->afficher("Base de données... ");

$sqlFile = ABSPATH."tables.my.sql"; //fichier original servant à installer la base
$messages = array(); //messages provenant de la mise à jour
$erreurs = array(); //messages d'erreurs potentiels

//requêtes lues dans le fichier 'tables.my.sql'
function install_tables($sqlFile)
	{
	global $install;

	//méthode de lecture du fichier des requêtes à effectuer reprise de celle utilisée par Webcalendar pour son fichier de connexion
	$grr_file = file_get_contents($sqlFile);
	$grr_file = preg_replace("/[\r\n]+/","\n",$grr_file);
	$grr_content = explode("\n",$grr_file);
	for($n=0; $n<count($grr_content); $n++)
		{
		$query = $grr_content[$n];
		$query = trim($query,"\r\n ");
		//par précaution, on n'exécute pas les suppression de tables car inutile et en plus il ne faudrait pas réinstaller une base déjà présente...
		if(!empty($query) && !preg_match("/drop table/i",$query)) $install->query($query,DB_NAME);
		}

	$nom_etab = $install->escape(LIBELLE_ETAB);
	$url_etab = $install->escape("https://".WEB_URL."/grr/");
	$escaped = "admin@".$install->escape(DOMAINE_MESSAGERIE_ETAB);
	$adminMail=(DOMAINE_MESSAGERIE_ETAB != "") ? $escaped : SYSTEM_MAIL_TO;

	$supportMail = (DOMAINE_MESSAGERIE_ETAB !="") ? $escaped : SYSTEM_MAIL_TO;

	$install->query("UPDATE `grr_setting` SET VALUE='".$nom_etab."' WHERE NAME='company';",DB_NAME);
	$install->query("UPDATE `grr_setting` SET VALUE='".$adminMail."' WHERE NAME='webmaster_email';",DB_NAME);
	$install->query("UPDATE `grr_setting` SET VALUE='".$supportMail."' WHERE NAME='technical_support_email';",DB_NAME);
	$install->query("UPDATE `grr_setting` SET VALUE='".$url_etab."' WHERE NAME='grr_url';",DB_NAME);
	$install->query("INSERT INTO `grr_setting` (`NAME`,`VALUE`) VALUES ('sso_statut','cas_envole');",DB_NAME);
	}

//pour insérer un paramètre absent de la table grr_setting
function check_setting($name,$value)
	{
	global $install;

	$name = $install->escape($name);
	$value = $install->escape($value);

	$install->setRows("SELECT `VALUE` FROM `grr_setting` WHERE `NAME`='".$name."';",DB_NAME);
	if(count($install->rows)===0) $install->query("INSERT INTO `grr_setting` (`NAME`,`VALUE`) VALUES ('".$name."','".$value."');",DB_NAME);
	}

function requeteHTTP($post=array(),$https=false)
	{
	global $install,$erreurs;
	$url  = $https ? 'https://' : 'http://';
	$url .= ENVOLE_URL.'/grr/admin_maj.php';

	$curl = curl_init();
	if(curl_errno($curl)) { $erreurs[] = "Echec d'une requête cURL : ".curl_error($curl); return false; }
	curl_setopt($curl,CURLOPT_URL,$url);
	curl_setopt($curl,CURLOPT_POST,true);
	curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
	curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($curl,CURLOPT_FOLLOWLOCATION,false);
	$resultat = curl_exec($curl);
	if(curl_errno($curl)) { $erreurs[] = "Echec d'une requête cURL : ".curl_error($curl); return false; }
	$code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
	curl_close($curl);

	//utiliser CURLOPT_FOLLOWLOCATION ne résoud pas le problème de redirection possible HTTP vers HTTPS : le résultat retourné est vide et la mise à jour non faite
	//voir ce qui est fait là : http://www.php.net/manual/fr/function.curl-setopt.php#102121
	//ici, on relance la requête en HTTPS si redirection déclarée
	if(!$https && $code=="302")
		{
		$install->afficher("redirection https... ");
		return requeteHTTP($post,true);
		}
	else return $resultat;
	}

$test = "SHOW TABLES";
$install->setRows($test,DB_NAME);
if(count($install->rows)===0)
	{
	if(!is_file($sqlFile)) $install->echec("Erreur > Fichier SQL \"".$sqlFile."\" servant à créer la base non présent\n\n");

	$install->afficher("installation des tables... ");
	install_tables($sqlFile);
	}
else
	{
	$install->afficher("tables présentes... ");

	//recherche de mise à jour : on fait au mieux pour la détecter et l'appliquer sinon elle sera à faire manuellement
	$install->setRows("SELECT `VALUE` FROM `grr_setting` WHERE `NAME`='version';",DB_NAME);
	$config_file = ABSPATH."include/misc.inc.php";
	$test_maj = false;
	if(count($install->rows)>0 && is_file($config_file))
		{
		include_once($config_file); //pour récupérer $version_grr
		if(isset($version_grr))
			{
			$version_old = $install->rows[0]["VALUE"];
			if($version_old!='')
				{
				$test_maj = true;
				if($version_grr_RC!="") $erreurs[] = "L'application est en version RC : mise à jour automatique non prévue";
				elseif($version_grr>$version_old) //vérification faite habituellement dans "admin_maj.php" grâce à la fonction "verif_version()" de "include/functions.inc.php"
					{
					$post = array
						(
						"maj_envole"  => "1",
						"maj"         => "yes",
						"valid"       => "yes",
						"version_old" => $version_old.".9" //important !!!
						);
					$install->afficher("mise à jour de ".$version_old." vers ".$version_grr."... ");
					if($maj = requeteHTTP($post))
						{
						$maj = preg_replace("#<br\s*/?>#i","|",$maj);
						$messages = explode("|",$maj);
						}
					}
				else $install->afficher("version ".$version_old." à jour... ");
				}
			}
		}
	if(!$test_maj) $erreurs[] = "Impossible de vérifier la version de l'application";
	}

$install->afficher("vérification des paramètres... ");
//l'absence de "sso_statut" est possible donc on ne peut imposer la valeur (se récupère manuellement en réactivant la connexion CAS par exemple)
//renseigner "default_site" et "default_room" permet d'éviter les urls par défaut incomplètes du type "grr/week.php?area=1&room=" qui affiche un message "Accès refusé".
$liste_settings = array
	(
	"envole_statut_prof"          => "utilisateur",
	"envole_statut_eleve"         => "visiteur",
	"envole_statut_administratif" => "utilisateur",
	"envole_statut_responsable"   => "visiteur",
	"envole_statut_autre"         => "visiteur",
	"default_site"                => "-1",
	"default_room"                => "-1"
	);
foreach($liste_settings as $name=>$value) check_setting($name,$value);

$resultat = empty($erreurs) ? "OK" : "ERREUR";
$install->afficher($resultat."\n");

if(!empty($messages))
	{
	$install->afficher("\nRésultats de la mise à jour (format HTML) :\n");
	$i = 0;
	foreach($messages as $k=>$message) if(!empty($message) && !preg_match("/^\s+$/",$message)) $install->afficher((++$i)." - ".trim($message)."\n");
	$install->afficher("\n");
	}

if(!empty($erreurs))
	{
	$install->afficher("\nErreurs survenues :\n");
	foreach($erreurs as $k=>$erreur) $install->afficher(($k+1)." - ".$erreur."\n");
	$install->afficher("\n");
	}
?>
