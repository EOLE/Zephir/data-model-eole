<?php
/**
 * Répertoire contenant les scripts servant à l'installation
 */
define('DIR_EXEC','/usr/share/envole/');

/**
 * Répertoire contenant les fichiers à traiter durant l'installation
 */
define('DIR_FILES','/etc/sysconfig/envole/');

/**
 * Répertoire contenant les fichiers de log
 */
define('DIR_LOG','/var/log/envole/');

/**
 * Répertoire contenant les fichiers de sauvegarde (mysql)
 */
define('DIR_BACKUP','/var/backups/envole/');

/**
 * Répertoires contenant les fichiers de configuration du SSO
 */
define('DIR_FILTER','/usr/share/sso/app_filters/');
define('DIR_INFOS','/usr/share/sso/user_infos/');

/**
 * @class EnvoleTools
 * Ensemble d'outils génériques pour installer une application sur Scribe
 */
class EnvoleTools
	{
	/**
	 * Nombre max de sauvegardes SQL
	 */
	private $dump_count = 10;

	/**
	 * Nom des variables à récupérer du Scribe
	 * Tableau vidé à l'instanciation, constantes du même nom à utiliser
	 */
	private $scribe_vars = array
		(
		"activation", //valeur oui/non du gen_config pour l'appli concernée
		"numero_etab",
		"libelle_etab",
		"domaine_messagerie_etab",
		"nom_academie",
		"eolesso_adresse",
		"web_url",
		"adresse_ip_eth0",
		"adresse_ip_mysql",
        "adresse_ip_web",
        "system_mail_to"
		);

	/**
	 * Nom des variables liées aux conteneurs à récupérer du Scribe
	 * Tableau vidé à l'instanciation, constantes du même nom à utiliser
	 */
	private $container_vars = array
		(
		"container_path_web",
		"container_path_mysql",
		"adresse_ip_br0"
		);

	/**
	 * Nom des constantes devant avoir été définies obligatoirement à l'instanciation de la classe
	 */
	private $liste_const = array
		(
		"APPLI_NAME",
		"APPLI_MODULE",
		"APPLI_DICO",
		"DB_NAME",
		"DB_USER",
		"DB_PASS"
		);

	/**
	 * Affichage complet uniquement dans le fichier de log
	 * Si true, un message court sera tout de même affiché sur la console
	 */
	private $only_log;

	/**
	 * Encodage des scripts d'installation pour les échanges avec le serveur MySQL
	 * "utf8" par défaut et modifiable lors de l'instanciation
	 */
	private $charset = "utf8";

	/**
	 * Par précaution, encodages autorisés pour les échanges avec le serveur MySQL
	 * liste complète : http://dev.mysql.com/doc/refman/5.0/fr/charset-charsets.html
	 */
	private $liste_charset = array("utf8","latin1");

	/**
	 * Informations sur la base de données en cours d'utilisation
	 */
	public $db_host = ""; // db_host est réécrit par la suite
	private $db_user = "root";
	private $db_name = "";
	public $db_conn = false; //identifiant de la connexion
	public $result = false; //résultat de la requête
	public $rows = array(); //liste des résultats (n-uplets)

	/**
	 * Le processus d'installation vérifie s'il faut ou non créer la base et/ou l'utilisateur
	 */
	public $create_user = false;
	public $create_base = false;

	/**
	 * Pointeur du fichier de log
	 */
	private $file_log = "";

	/**
	 * Message console terminant l'installation
	 * Devient "ERREUR" en cas d'échec intermédiaire
	 */
	private $stop_text = "OK";

	/**
	 * Répertoires utilisés propres à l'application
	 * (de la forme "repertoire/appli)
	 */
	public $rep_exec = "";
	public $rep_files = "";
	public $rep_log = "";
	public $rep_backup = "";

	/**
	 * Actions lors de l'instanciation
	 */
	function __construct($verbose=false,$charset="")
		{
		$this->only_log = !$verbose;
		if(!empty($charset) && in_array($charset,$this->liste_charset)) $this->charset = $charset;
		$this->preinst(); //vérifie l'existence des constantes
		$this->rep_exec = DIR_EXEC.APPLI_MODULE."/"; //créé par le paquet et indispensable
		$this->rep_files = DIR_FILES.APPLI_MODULE."/"; //créé par le paquet et non indispensable
		$this->rep_log = DIR_LOG.APPLI_MODULE."/"; //à créer si non présent
		$this->rep_backup = DIR_BACKUP.APPLI_MODULE."/"; //à créer si non présent
		if(!is_dir(DIR_LOG)) mkdir(DIR_LOG);
		if(!is_dir($this->rep_log)) mkdir($this->rep_log);
		if(!is_dir(DIR_BACKUP)) mkdir(DIR_BACKUP);
		if(!is_dir($this->rep_backup)) mkdir($this->rep_backup);
		$this->file_log = fopen($this->rep_log."install-".APPLI_MODULE."-".date("dmy-His").".log","a"); //démarre l'affichage
		if(!is_dir($this->rep_exec)) $this->echec("\nRépertoire d'installation \"".$this->rep_exec."\" inexistant -> Installation abandonnée.\n\n");
		if(!is_dir($this->rep_files)) $this->afficher("\nRemarque : le répertoire \"".$this->rep_files."\" pour les fichiers à traiter n'est pas prévu par le paquet.\n");
		if($this->only_log) echo "Configuration de ".APPLI_NAME."... ";
		$this->echo_titre("Installation du module ".APPLI_NAME);
		$this->parse_dico();
		$this->parse_container();
		$this->check_activation();
		$this->connect();
		}

	/**
	 * Actions à l'arrêt de l'installation
	 */
	function __destruct()
		{
		$this->close();
		if($this->only_log) echo $this->stop_text."\n";
		$this->echo_titre("Installation du module ".APPLI_MODULE." terminée");
		fclose($this->file_log);
		}

	/**
	 * Vérifications des variables devant être définies par le script d'installation
	 */
	function preinst()
		{
		define('DB_PASS',$this->genere_pwd()); //exception car nécessite une méthode de cette classe
		foreach($this->liste_const as $const)
			{
			if(!defined($const)) $this->echec("\nConstante ".$const." non définie par l'application -> Installation abandonnée.\n\n");
			}
		}

	/**
	 * Récupération des variables utiles du dico eole sous forme de constantes
	 * (repris de "/var/www/ead/ParseDico.php")
	 */
	function parse_dico()
		{
		$this->echo_sstitre("Récupération des variables du Scribe");
		if (is_file("/usr/share/creole/parsedico.py")) $dico_script="/usr/share/creole/parsedico.py";
		else $dico_script="/usr/share/php/envole-php/parsedico24.py";
		$fp = popen($dico_script,"r");
		while(!feof($fp))
			{
			$ligne = fgets($fp,4096);
			if($pos=strpos($ligne,'='))
				{
				$libelle = substr($ligne,0,$pos);
				if($libelle==APPLI_DICO) $libelle = "activation";
				if(in_array($libelle,$this->scribe_vars))
					{
					$value = substr($ligne,$pos+2,-2);
					if($libelle=="web_url") $value = strtolower($value); //problème de majuscules et le serveur CAS est sensible à la casse...
					define(strtoupper($libelle),$value);
					$this->afficher("  * ".strtoupper($libelle).": ".$value."\n");
					$keys = array_keys($this->scribe_vars,$libelle);
					foreach($keys as $key) unset($this->scribe_vars[$key]);
					}
				}
			}
		if(count($this->scribe_vars)>0)
			{
			$this->afficher("\nVariables du Scribe non récupérées :\n");
			foreach($this->scribe_vars as $var) $this->afficher("  * ".$var."\n");
			$this->echec("Impossible de poursuivre -> Installation abandonnée.\n\n");
			}
		pclose($fp);

		//dernière variable à déterminer en fonction de celles trouvées
		$url = $this->get_url();
		if(empty($url)) $this->echec("\nImpossible de récupérer l'url d'accès -> Installation abandonnée.\n\n");
		else
			{
			define("ENVOLE_URL",$url);
			$this->afficher("  * ENVOLE_URL : ".$url."\n");
			}

		//utilisation de la base définie dans le dictionnaire
		$this->db_host=ADRESSE_IP_MYSQL;

		}

	/**
	 * Récupération des variables utiles liées aux conteneurs
	 */
	function parse_container()
		{
		$this->echo_sstitre("Récupération des variables liées aux conteneurs");
		if (is_file("/usr/share/creole/parsedico.py")) {
			$fp = popen("cat /etc/eole/network_containers.conf /etc/eole/containers.conf","r");
		}else{
			$fp = popen("/usr/share/php/envole-php/parsedico24.py","r");
		}
		while(!feof($fp))
			{
			$ligne = fgets($fp,4096);
			if($pos=strpos($ligne,'='))
				{
				$libelle = substr($ligne,0,$pos);
				if(in_array($libelle,$this->container_vars))
					{
					$value = substr($ligne,$pos+2,-2);
          $value=preg_replace("/^\/var\/lib\//", "/opt/", $value, 1);
					define(strtoupper($libelle),$value);
					$this->afficher("  * ".strtoupper($libelle).": ".$value."\n");
					$keys = array_keys($this->container_vars,$libelle);
					foreach($keys as $key) unset($this->container_vars[$key]);
					}
				}
			}
		if(count($this->container_vars)>0)
			{
			$this->afficher("\nVariables de Conteneur non récupérées :\n");
			foreach($this->container_vars as $var) $this->afficher("  * ".$var."\n");
			$this->echec("Impossible de poursuivre -> Installation abandonnée.\n\n");
			}
		pclose($fp);
		}

	/**
	 * Récupération de l'url d'accès au portail en fonction du EnvOLE activé
	 *
	 * @return string
	 */
	function get_url()
		{
		$url = "";

		if(defined("WEB_URL")) $url = WEB_URL;
		elseif(defined("ADRESSE_IP_WEB")) $url = ADRESSE_IP_WEB;

		return $url;
		}

	/**
	 * Vérification que l'application a bien été activée dans le gen_config
	 * A utiliser après la métode parse_dico()
	 */
	function check_activation()
		{
		if(!defined("ACTIVATION") || ACTIVATION!="oui")
			{
			$this->echo_sstitre("INSTALLATION IMPOSSIBLE");
			$this->echec("Cause : l'application \"".APPLI_NAME."\" doit être activée dans le \"gen_config\".\n\n");
			}
		}

	/**
	 * Connexion MySQL en tant que root qui servira aux enregistrements et mises à jour
	 *
	 * On prévient le serveur MySQL du charset du client (encodage des scripts d'installation) sachant que SET NAMES 'x' équivaut aux 3 requêtes suivantes :
	 *	- SET character_set_client = 'x';
	 *	- SET character_set_results = 'x';
	 *	- SET character_set_connection = 'x';
	 */
	function connect()
		{
		if(!$this->db_conn)
			{
			$this->echo_sstitre("Connexion MySQL en tant que \"".$this->db_user."\"");

			$this->afficher("Mise à jour du mot de passe... ");
			$password = $this->genere_pwd();
			if (is_file("/usr/share/eole/mysql_pwd.py")) $mysql_script="/usr/share/eole/mysql_pwd.py";
			else $mysql_script="/usr/share/eole/sbin/mysql_pwd.py";
			system("$mysql_script $password rootonly >/dev/null");
			$this->afficher("OK\n");

			$this->afficher("Connexion... ");
			$this->db_conn = @mysql_connect($this->db_host,$this->db_user,$password);
			@mysql_query("SET NAMES '".$this->charset."';",$this->db_conn);
			if($this->db_conn) $this->afficher("OK\n");
			else $this->echec("Impossible de se connecter à la base de données -> Installation abandonnée.\n\n");
			}
		}

	/**
	 * Sélection d'une base de données
	 *
	 * @param $dbname
	 */
	function select($dbname)
		{
		if(!$this->db_conn) $this->echec("Impossible de sélectionner la base de données car la connexion MySQL est inexistante -> Installation abandonnée.\n\n");
		if(!@mysql_select_db($dbname,$this->db_conn)) $this->echec("Impossible de sélectionner la base de données -> Installation abandonnée.\n\n");
		else $this->db_name = $dbname;
		}

	/**
	 * Demande de requête
	 *
	 * @param $sql
	 * @param $dbname si changement de base nécessaire
	 * @param $forcing requête ne nécessitant pas qu'une base de données soit sélectionnée
	 */
	function query($sql,$dbname="",$forcing=false)
		{
		if(!$this->db_conn) $this->echec("Impossible d'effectuer une requête car la connexion MySQL est inexistante -> Installation abandonnée.\n\n");
		if(!$forcing && empty($dbname) && empty($this->db_name)) $this->echec("Impossible d'effectuer une requête car aucune base n'est sélectionnée -> Installation abandonnée.\n\n");
		if(!empty($dbname) && $dbname!=$this->db_name) $this->select($dbname);
		$this->result = @mysql_query($sql,$this->db_conn);
		if(!$this->result) $this->echec("La requête suivante a échoué :\n".$sql."\n\n");
		else return true;
		}

	/**
	 * Demande de requête type SELECT et enregistrement des résultats dans $this->rows
	 *
	 * @param $sql
	 * @param $dbname si changement de base nécessaire
	 */
	function setRows($sql,$dbname="")
		{
		if($this->query($sql,$dbname) && $this->result)
			{
			$n = 0;
			$this->rows = array();
			while($row = @mysql_fetch_array($this->result,MYSQL_BOTH))
				{
				foreach($row as $key=>$value) $this->rows[$n][$key] = $value;
				$n++;
				}
			@mysql_free_result($this->result);
			return true;
			}
		else return false;
		}

	/**
	 * Pour préparer une chaîne dans une requête sql
	 *
	 * @param $string chaîne de caractères
	 */
	function escape($str)
		{
		if(!$this->db_conn) $this->echec("Impossible de traiter la chaîne de caractères \"".$str."\" car la connexion MySQL est inexistante -> Installation abandonnée.\n\n");
		if(get_magic_quotes_gpc()) $str = stripslashes($str);
		$str = mysql_real_escape_string($str,$this->db_conn);
		return $str;
		}

	/**
	 * Fermeture de la connexion MySQL
	 */
	function close()
		{
		//$this->afficher("Fermeture de la connexion mysql en tant que \"".$this->db_user."\"... ");
		if(@mysql_close($this->db_conn))
			{
			$this->db_conn = false;
			$this->db_name = "";
			//$this->afficher("OK\n");
			}
		//else $this->echec("Echec de la fermeture de connexion\n\n");
		}

	/**
	 * Vérification de la présence de la base de données
	 *
	 * @return boolean
	 */
	function bdd_verification()
		{
		if(is_dir(CONTAINER_PATH_MYSQL."/var/lib/mysql/".DB_NAME))
			{
			$this->afficher("La base \"".DB_NAME."\" est présente sur ce serveur.\n");
			$bdd_exist = true;
			}
		else
			{
			$this->afficher("La base \"".DB_NAME."\" n'est pas présente sur ce serveur... elle va être installée\n");
			$this->create_user = true;
			$this->create_base = true;
			$bdd_exist = false;
			}
		return $bdd_exist;
		}

	/**
	 * Création éventuelle de la base et de l'utilisateur associé
	 */
	function bdd_traitement()
		{
		if($this->create_base)
			{
			$this->afficher("Création de la base \"".DB_NAME."\"... ");
			$this->query("CREATE DATABASE `".DB_NAME."`;","",true);
			$this->afficher("OK\n");
			}

		if($this->create_user)
			{
			$this->afficher("Création de l'utilisateur \"".DB_USER."\"... ");
			// les applications se connectent à mysql depuis le conteneur web
			$this->query("GRANT ALL PRIVILEGES ON ".DB_NAME.".* TO '".DB_USER."'@'".ADRESSE_IP_WEB."' IDENTIFIED BY '".DB_PASS."' WITH GRANT OPTION;","",true);
			// et les tests se font depuis le maître
			$this->query("GRANT ALL PRIVILEGES ON ".DB_NAME.".* TO '".DB_USER."'@'".ADRESSE_IP_BR0."' IDENTIFIED BY '".DB_PASS."' WITH GRANT OPTION;","",true);
			$this->afficher("OK\n");

			$this->afficher("Rechargement des privilèges... ");
			$this->query("flush privileges;","",true);
			$this->afficher("OK\n");
			}
		}

	/**
	 * vérifie les identifiants de connexion à une base de données
	 *
	 * @return boolean
	 */
	function test($dbhost,$dbuser,$dbpassword,$dbname)
		{
		$this->afficher("Tentative de connexion mysql ".$dbhost." à la base \"".$dbname."\" en tant que \"".$dbuser."\"... ");
		$link_test = @mysql_connect($dbhost,$dbuser,$dbpassword);
		if ($link_test)
			{
			if(@mysql_select_db($dbname,$link_test))
				{
				$this->afficher("OK\n");
				@mysql_close($link_test);
				return true;
				}
			else
				{
				$this->afficher("Echec\n");
				@mysql_close($link_test);
				}
			}
		else $this->afficher("Echec\n");
		return false;
		}

	/**
	 * Obtenir un mot de passe aléatoire
	 *
	 * @param $pass_length
	 *
	 * @return string
	 */
	function genere_pwd($pass_length=12)
	  {
	  $chaine = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	  $pass = "";
	  $chaine_length = strlen($chaine);
	  for($i=1;$i<=$pass_length;$i++)
		  {
		  $char = mt_rand(0,$chaine_length-1);
		  $pass.=$chaine[$char];
		  }
	  return $pass;
	  }

	/**
	 * Affichage des étapes de l'installation
	 *
	 * @param $text
	 */
	function echo_titre($text)
		{
		//un caractère accentué compte pour 2 en utf-8...
		$longueur = strlen(utf8_decode($text));
		$cadre = "\n+";
		for($i=0;$i<($longueur+4);$i++) $cadre .= "-";
		$cadre .= "+\n";
		$this->afficher($cadre."|  ".$text."  |".$cadre);
		}

	/**
	 * Affichage des sous-étapes de l'installation
	 *
	 * @param $text
	 */
	function echo_sstitre($text)
		{
		$this->afficher("\n---- ".$text." ----\n");
		}

	/**
	 * Test sur le résultat d'une action qui renvoit true/false, l'action ayant été annoncée par la méthode "afficher"
	 *
	 * @param $bool
	 */
	function resultat($bool)
		{
		if($bool) $this->afficher("OK\n");
		else $this->echec("Echec\n");
		}

	/**
	 * Traiter les commentaires de l'installation
	 *
	 * @param $text
	 */
	function afficher($text)
		{
		fwrite($this->file_log,$text);
		if(!$this->only_log) echo $text;
		}

	/**
	 * Interruption du script
	 *
	 * @param $text
	 */
	function echec($text="")
		{
		if(!empty($text)) $this->afficher($text);
		$this->stop_text = "ERREUR";
		exit();
		}

	/**
	 * Remplacer un mot par un autre dans un fichier
	 *
	 * @param $ori_file chemin complet du fichier originale
	 * @param $dest_file chemin complet du fichier finale
	 * @param $avant string à trouver dans le fichier originale
	 * @param $apres string de remplacement
	 * @param $ecraser true pour réécrire le fichier finale, sinon on ajoute à la fin
	 */
	function sed($ori_file,$dest_file,$avant,$apres,$ecraser=true)
		{
		$texte = @file_get_contents($ori_file);
		if(!preg_match("/[[:alnum:]]/",$texte)) $this->echec("\nLa lecture du fichier \"".$ori_file."\" a échoué -> Installation interrompue.\n\n");
		$texte = str_replace($avant,$apres,$texte);
		if($ecraser) @file_put_contents($dest_file,$texte);
		else @file_put_contents($dest_file,$texte,FILE_APPEND);
		if(!is_file($dest_file)) $this->echec("\nLe traitement du fichier \"".$dest_file."\" a échoué -> Installation interrompue.\n\n");
		}

	/**
	 * Copier un fichier
	 *
	 * @param $ori_file chemin complet du fichier originale
	 * @param $dest_file chemin complet du fichier finale
	 */
	function copier($ori_file,$dest_file)
		{
		if (!@copy($ori_file,$dest_file))
		$this->echec("\nLe traitement du fichier \"".$dest_file."\" a échoué -> Installation interrompue.\n\n");
		}

	/**
	 * Nom du fichier dump
	 * Si le nombre maximum est atteint, le 1 est supprimé, les 2 à max sont renommés de 1 à (max-1) et on renvoit le max
	 *
	 * @param  $base
	 * @return string nom du fichier à utiliser
	 */
	function getBackupFileName($base)
		{
		$n = 1;
		$name = $base."-dump-";
		$extension = ".sql";
		while(@is_file($this->rep_backup.$name.$n.$extension)) $n++;
		if($n<=$this->dump_count) $filename = $this->rep_backup.$name.$n.$extension;
		else
			{
			@unlink($this->rep_backup.$name."1".$extension);
			for($i=2; $i<=$this->dump_count; $i++) @rename($this->rep_backup.$name.$i.$extension,$this->rep_backup.$name.($i-1).$extension);
			$filename = $this->rep_backup.$name.$this->dump_count.$extension;
			}
		return $filename;
		}

	/**
	 * Extraction du contenu d'une base de données (tables-vues)
	 * Extraction facultative des éventuelles fonctions (à l'utilisation, déconnexion systématique observée dans un seul bahut, cause inconnue...)
	 * Aucune prise en compte des procédures
	 *
	 * @param $base
	 * @param $get_functions boolean
	 */
	function dump($base,$get_functions=false)
		{
		$this->afficher("Création dans \"".$this->rep_backup."\" d'un fichier de sauvegarde de la base \"".$base."\"... ");
		$this->file_backup = fopen($this->getBackupFileName($base),"w");
		$this->select($base);
		$sql = "SHOW TABLE STATUS;";
		$result = mysql_query($sql,$this->db_conn);
		if($result)
			{
			while($row=mysql_fetch_assoc($result))
				{
				$this->dump_table_structure($row["Name"],$row["Comment"]);
				if($row["Comment"]!="VIEW") $this->dump_table_data($row["Name"]);
				}
			}
		else fwrite($this->file_backup,"/*** Aucune table présente dans ".$data." ***/\n\n");
		mysql_free_result($result);
		if($get_functions)
			{
			$sql = "SHOW FUNCTION STATUS;";
			$result = mysql_query($sql,$this->db_conn);
			if($result)
				{
				while($row=mysql_fetch_assoc($result))
					{
					if($row["Db"]==$base) $this->dump_function_structure($row["Name"]);
					}
				}
			else fwrite($this->file_backup,"/*** Aucune fonction présente dans ".$data." ***/\n\n");
			mysql_free_result($result);
			}
		fclose($this->file_backup);
		$this->afficher("OK\n");
		}

	/**
	 * Récupérer la structure d'une fonction donnée
	 *
	 * @param $function
	 */
	function dump_function_structure($function)
		{
		fwrite($this->file_backup,"/*** Structure de la fonction `".$function."` ***/\n");
		$sql = "SHOW CREATE FUNCTION `".$function."`;";
		$result = mysql_query($sql,$this->db_conn);
		if($result)
			{
			if($row=mysql_fetch_assoc($result))
				{
				if($row['Create Function']) fwrite($this->file_backup,$row['Create Function'].";\n\n");
				}
			}
		mysql_free_result($result);
		}

	/**
	 * Récupérer la structure d'une table/vue donnée
	 *
	 * @param $table
	 * @param $type sert juste à afficher qu'il s'agit d'une vue si besoin
	 */
	function dump_table_structure($table,$type)
		{
		if($type=="VIEW") fwrite($this->file_backup,"/*** Structure de la vue `".$table."` ***/\n");
		else fwrite($this->file_backup,"/*** Structure de la table `".$table."` ***/\n");
		$sql = "SHOW CREATE TABLE `".$table."`;";
		$result = mysql_query($sql,$this->db_conn);
		if($result)
			{
			if($row=mysql_fetch_assoc($result))
				{
				if($row['Create Table']) fwrite($this->file_backup,$row['Create Table'].";\n\n");
				elseif($row['Create View']) fwrite($this->file_backup,$row['Create View'].";\n\n");
				}
			}
		mysql_free_result($result);
		}

	/**
	 * Récupérer le contenu d'une table donnée
	 *
	 * @param $table
	 */
	function dump_table_data($table)
		{
		$sql = "SELECT * FROM `".$table."`;";
		$result = mysql_query($sql,$this->db_conn);
		if($result)
			{
			$num_rows = mysql_num_rows($result);
			$num_fields = mysql_num_fields($result);
			if($num_rows>0)
				{
				fwrite($this->file_backup,"/*** Contenu de la table `".$table."` ***/\n");
				$field_type = array();
				$i = 0;
				while($i<$num_fields)
					{
					$meta = mysql_fetch_field($result, $i);
					array_push($field_type, $meta->type);
					$i++;
					}
				//print_r($field_type);
				fwrite($this->file_backup,"INSERT INTO `".$table."` VALUES\n");
				$index = 0;
				while($row=mysql_fetch_row($result))
					{
					fwrite($this->file_backup,"(");
					for($i=0; $i<$num_fields; $i++)
						{
						if(is_null($row[$i])) fwrite($this->file_backup,"null");
						else
							{
							switch($field_type[$i])
								{
								case 'int':
									fwrite($this->file_backup,$row[$i]);
									break;
								case 'string':
								case 'blob' :
								default:
									fwrite($this->file_backup,"'".mysql_real_escape_string($row[$i],$this->db_conn)."'");
								}
							}
						if($i<$num_fields-1) fwrite($this->file_backup,",");
						}
					fwrite($this->file_backup,")");
					if($index<$num_rows-1) fwrite($this->file_backup,",");
					else fwrite($this->file_backup,";");
					fwrite($this->file_backup,"\n");
					$index++;
					}
				}
			else fwrite($this->file_backup,"/*** La table `".$table."` est vide de contenu ***/\n\n");
			}
		mysql_free_result($result);
		fwrite($this->file_backup,"\n\n");
		}
	}
?>
