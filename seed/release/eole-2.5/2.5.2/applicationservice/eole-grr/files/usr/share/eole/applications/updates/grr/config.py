#-*-coding:utf-8-*-
###########################################################################
#
#
###########################################################################
"""
    Configuration pour la création de la base de données de mon appli
"""
from eolesql.db_test import test_var, db_exists

sap_TABLEFILENAMES = ['/usr/share/eole/mysql/grr/updates/grr-update1.sql']

def test_reponse(result=None):
    """
        test l'existence posh-profil
    """
    return test_var('activer_grr') and db_exists('grr')

conf_dict = dict(db='grr',
                 test_active=test_reponse,
                 filenames=sap_TABLEFILENAMES,
                 version='Mise à jour de Grr')
