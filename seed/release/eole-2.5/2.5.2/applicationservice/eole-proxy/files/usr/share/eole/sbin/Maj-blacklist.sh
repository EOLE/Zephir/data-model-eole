#!/bin/bash

if [ $(CreoleGet activer_filtrage_proxy) = "non" ];then
    echo "Le filtrage n'est pas activé sur le proxy"
    exit 0
fi

. /usr/lib/eole/diagnose.sh

FatalError() {
    # alerte Zéphir
    . /usr/lib/eole/zephir.sh
    Zephir "ERR" "$1" "Maj-blacklist" 2>&1 | tee -a $F_LOG
    # erreur en 1ère position pour vue EAD
    sed -i 1i"Erreur : $1" $F_LOG
    exit 1
}

container_path_proxy=$(CreoleGet container_path_proxy)
url_maj_blacklist=$(CreoleGet url_maj_blacklist)

# lieu de stockage des bases
SHORT_META_PATH="/var/lib/blacklists/meta/"
META_PATH="$container_path_proxy$SHORT_META_PATH"
SHORT_DB_PATH="/var/lib/blacklists"
DB_PATH="$container_path_proxy$SHORT_DB_PATH"
# fichier de log spécifique
F_LOG="/var/lib/eole/reports/maj-blacklist.txt"

echo -n "Mise à jour le " > $F_LOG
date '+%d.%m.%Y à %H:%M :' >> $F_LOG

ServBlacklist=`echo "$url_maj_blacklist" |awk -F "/" '{print $3}'`

TestWeb "Test de $url_maj_blacklist" "$url_maj_blacklist" 2>&1
if [ $? -ne 0 ];then
    FatalError "Impossible d'accéder au site de mise à jour : $ServBlacklist"
fi

## on se pose dans /tmp ##
mkdir -p $DB_PATH/tmp
cd $DB_PATH/tmp
WGET_OPTIONS="--timestamping"

echo "Téléchargement des bases"

res=`wget $WGET_OPTIONS $url_maj_blacklist/blacklists.tar.gz 2>&1`
if [ $? -ne 0 ];then
    FatalError "Le fichier blacklists.tar.gz n'a pas été trouvé !"
fi
echo "$res" | grep -q -E "non récupéré|not retrieving"
if [ $? -eq 0 ];then
    blacklists="0"
else
    blacklists="1"
fi

## Fichier weighted ##
res=`wget $WGET_OPTIONS $url_maj_blacklist/weighted 2>&1`
if [ $? -ne 0 ];then
    FatalError "Le fichier weighted n'a pas été trouvé !"
fi
echo "$res" | grep -q -E "non récupéré|not retrieving"
if [ $? -eq 0 ];then
    weighted="0"
else
    weighted="1"
fi

## Base blacklists (si nécessaire) ##
if [ "$blacklists" == "1" ];then
    echo "Intégration des bases"
    tar -xzf blacklists.tar.gz
    if [ $? -ne 0 ];then
        FatalError "L'archive blacklists.tar.gz n'a pas pu être décompressée !"
    fi

    ## Filtres obligatoires (dans db) ##
    for base in "adult" "redirector"
    do
        [ ! -d $DB_PATH/db/$base ] && mkdir -p $DB_PATH/db/$base
        for file in "$DB_PATH/tmp/blacklists/$base/domains" "$DB_PATH/tmp/blacklists/$base/urls"
        do
            if [ -f $file ];then
                count=`wc -l $file | awk -F " " '{print $1}'`
                if [ ! "$count" -eq "0" ];then
                    cp $file $DB_PATH/db/$base
                else
                    echo 'le fichier' $file 'est vide !'
                fi
            fi
        done
    done
    #CreoleRun "chown -R proxy.proxy $SHORT_DB_PATH/db/" proxy

    ## Filtres optionnels (dans eole) ##
    for base in `cut -d '#' -f1 /usr/share/ead2/backend/config/filtres-opt`
    do
        [ ! -d $DB_PATH/eole/$base ] && mkdir -p $DB_PATH/eole/$base
        [ ! -e $DB_PATH/eole/$base/domains ] && touch $DB_PATH/eole/$base/domains
        [ ! -e $DB_PATH/eole/$base/urls ] && touch $DB_PATH/eole/$base/urls

        for file in "$DB_PATH/tmp/blacklists/$base/domains" "$DB_PATH/tmp/blacklists/$base/urls"
        do
            if [ -f $file ];then
                count=`wc -l $file | awk -F " " '{print $1}'`
                if [ ! "$count" -eq "0" ];then
                    cp $file $DB_PATH/eole/$base
                else
                    echo 'le fichier' $file 'est vide !'
                fi
            fi
        done
    done
    #CreoleRun "chown -R proxy.proxy $SHORT_DB_PATH/eole/" proxy

else
    echo "Rien à faire pour blacklists.tar.gz"
fi

## Fichier weighted (si nécessaire) ##
if [ $weighted == "1" ]
then
    echo "Copie du fichier weighted"
    cp -f $DB_PATH/tmp/weighted $META_PATH
else
    echo "Rien à faire pour le fichier weighted"
fi


# formatage de la date du fichier pour EAD
bdate=`ls -l --time-style="+%d.%m.%Y %H:%M" blacklists.tar.gz`
echo -n "- bases du " >> $F_LOG
echo -n `echo -n $bdate | awk -F' ' '{print $6} {print $7}'` >> $F_LOG
echo >> $F_LOG
wdate=`ls -l --time-style="+%d.%m.%Y %H:%M" weighted`
echo -n "- fichier weighted du " >> $F_LOG
echo -n `echo -n $bdate | awk -F' ' '{print $6} {print $7}'` >> $F_LOG
echo >> $F_LOG

## Suppression des fichiers
# on laisse le tar.gz pour utiliser l'option --timestamping de wget
rm -rf $DB_PATH/tmp/adult
rm -rf $DB_PATH/tmp/blacklists
#rm -f $DB_PATH/tmp/blacklists.tar.gz
#rm -f $DB_PATH/tmp/weighted

# redémarrage si au moins une modification
if [ "$blacklists" == "1" -o $weighted == "1" ];then
        CreoleService eole-guardian restart
fi
