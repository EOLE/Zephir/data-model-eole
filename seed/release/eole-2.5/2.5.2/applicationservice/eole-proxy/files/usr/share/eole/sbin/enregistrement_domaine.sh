#!/bin/bash
#################################################
# enregistrement_domaine.sh
#################################################
. /usr/lib/eole/ihm.sh

if [ "$(CreoleGet type_squid_auth aucun)" != 'NTLM/KERBEROS' ]; then
    EchoRouge "L'authentification du proxy n'est pas de type NTLM/KERBEROS"
    exit 1
fi

ip_serveur_krb=$(CreoleGet ip_serveur_krb)
CreoleRun "/usr/bin/wbinfo -t" proxy &>/dev/null
if [ $? -eq 0 ];then
    QUESTION="Le serveur est déjà intégré à un domaine\nRelancer l'intégration ?"
    Question_ouinon "$QUESTION" True non warn
    [ $? -ne 0 ] && exit 0
fi

#redemarrage de samba
echo "*** Redémarrage des services pour l'enregistrement au domaine ***"
CreoleService winbind stop -c proxy
CreoleService smbd restart -c proxy
CreoleService winbind start -c proxy

#inscription de la station dans un domaine
echo
echo "Entrer le nom de l'administrateur du serveur Windows :"
read user_admin
echo "Entrer le mot de passe de l'administrateur du serveur Windows :"
read -s mdp_admin
CreoleRun "/usr/bin/net ads join -I $ip_serveur_krb -U $user_admin%$mdp_admin" proxy
echo

#redemarrage de samba
echo "*** Redémarrage des services pour confirmer l'enregistrement au domaine ***"
CreoleService winbind stop -c proxy
CreoleService smbd restart -c proxy
CreoleService winbind start -c proxy

#test de l'intégration
CreoleRun "/usr/bin/wbinfo -t" proxy &>/dev/null
if [ $? -eq 1 ]; then
    EchoRouge "L'intégration au domaine a échoué"
    exit 1
fi
EchoVert "L'intégration au domaine a réussi"
exit 0
