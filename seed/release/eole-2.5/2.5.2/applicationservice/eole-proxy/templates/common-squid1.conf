#######################################
##Librairie de fonctions
####ACL###

%def set_acl_interfaces(%%interface, %%nom_machine_eth, %%adresse_network_eth, %%adresse_netmask_eth, %%adresse_ip_eth, %%vlan_eth)
 %set %%interface_key = "eth"+%%interface
# definition des acls pour l'interface %%interface_key
acl to_localhost dst %%adresse_ip_eth/32
#empêche le fonctionnement de cntlm (#10130)
#acl srcnoauth src %%adresse_ip_eth/32
acl %%nom_machine_eth src %%adresse_network_eth/%%calc_classe(%%adresse_netmask_eth)
 %if %%is_defined('route_adresse')
  %for %%i_route_adresse in %%route_adresse
   %if %%i_route_adresse.route_int == %%interface_key
acl %%nom_machine_eth src %%i_route_adresse/%%calc_classe(%%i_route_adresse.route_netmask)
   %end if
  %end for
 %end if
 %if %%is_defined('sites_dist_ip')
  %for %%lans in %%sites_dist_ip
acl %%nom_machine_eth src %%lans/%%calc_classe(%%lans.sites_dist_netmask)
  %end for
 %end if

#FIXME: pas bonne ip !
acl localhosteth%%{interface} dst %%adresse_ip_eth/32
acl reseaueth%%{interface} dst %%adresse_network_eth/%%calc_classe(%%adresse_netmask_eth)
 %if %%is_defined('route_adresse')
  %for %%i_route_adresse in %%route_adresse
   %if %%i_route_adresse.route_int == %%interface_key
acl reseaueth%%{interface} dst %%i_route_adresse/%%calc_classe(%%i_route_adresse.route_netmask)
   %end if
  %end for
 %end if
 %if %%is_defined('sites_dist_ip')
  %for %%lans in %%sites_dist_ip
acl reseaueth%%{interface} dst %%lans/%%calc_classe(%%lans.sites_dist_netmask)
  %end for
 %end if
 %if %%vlan_eth == "oui"
  %for %%vlans_iter in %%getVar('vlan_id_eth'+%%interface)
   %set %%network_key = "vlan_network_eth" + %%interface
   %set %%netmask_key = "vlan_netmask_eth" + %%interface
   %set %%ip_key = "vlan_ip_eth" + %%interface
   %set %%ip_vlan = %%getattr(%%vlans_iter, %%ip_key)
acl to_localhost dst %%ip_vlan/32
#empêche le fonctionnement de cntlm (#10130)
#acl srcnoauth src %%ip_vlan/32
acl vlan_eth%%{interface}_%%vlans_iter src %%getattr(%%vlans_iter, %%network_key)/%%calc_classe(%%getattr(vlans_iter, %%netmask_key))
acl reseaueth%%{interface}_%%vlans_iter dst %%getattr(%%vlans_iter, %%network_key)/%%calc_classe(%%getattr(%%vlans_iter, %%netmask_key))
acl localhosteth%%{interface}_%%vlans_iter dst %%ip_vlan/32
  %end for
 %end if
%end def

%def set_acl_ead(%%interface, %%admin_eth)
 %if %%admin_eth == "oui"
#acl admin ead pour l'interface %%interface
  %for %%idx, %%res_ssh in %%enumerate(%%getVar('ip_admin_eth'+%%interface))
   %if %%res_ssh == '0.0.0.0'
acl eth%%{interface}admin%%idx src all
   %else
    %set %%netmask_key = "netmask_admin_eth"+%%interface
acl eth%%{interface}admin%%idx src %%res_ssh/%%calc_classe(%%getattr(%%res_ssh, %%netmask_key))
   %end if
  %end for
 %end if
%end def

%def set_acl_alias(%%interface, %%alias_eth)
 %if %%alias_eth == "oui"
#acl alias IP pour l'interface %%interface
  %for %%idx, %%res_alias in %%enumerate(%%getVar('alias_ip_eth'+%%interface))
    %set %%network_val = %%getattr(%%res_alias, "alias_network_eth"+%%interface)
    %set %%netmask_val = %%getattr(%%res_alias, "alias_netmask_eth"+%%interface)
acl to_localhost dst %%res_alias/32
#empêche le fonctionnement de cntlm (#10130)
#acl srcnoauth src %%res_alias/32
acl eth%%{interface}alias%%idx src %%network_val/%%calc_classe(%%netmask_val)
  %end for
 %end if
%end def

###REGLES ###
%def set_eth_ead(%%interface, %%admin_eth, %%vlan_eth)
## Gestion des autorisations de connexion a l'ead de l'amon pour chaque interface
 %if %%admin_eth =="non"
http_access deny Reserved localhosteth%%{interface}
  %if %%vlan_eth == "oui"
   %for %%vlans_iter in %%getVar('vlan_id_eth'+%%interface)
http_access deny Reserved localhosteth%%{interface}_%%vlans_iter
   %end for
  %end if
 %elif %%admin_eth =="oui"
  %set %%res_ssh = ''
  %for %%idx, %%res_sshx in %%enumerate(%%getVar('ip_admin_eth'+%%interface))
   %set %%res_ssh += ' !eth' + %%interface + 'admin' + str(%%idx)
  %end for
http_access deny Reserved localhosteth%%{interface} %%res_ssh
  %if %%vlan_eth == "oui"
   %for %%vlans_iter in %%getVar('vlan_id_eth'+%%interface)
    %set %%res_ssh = ''
    %for %%idx, %%res_sshx in %%enumerate(%%getVar('ip_admin_eth'+%%interface))
     %set %%res_ssh += ' !eth' + %%interface + 'admin' + str(%%idx)
    %end for
http_access deny Reserved localhosteth%%{interface}_%%vlans_iter %%res_ssh
   %end for
  %end if
 %end if
%end def

# NETWORK OPTIONS
# -----------------------------------------------------------------------------

#  TAG: https_port
# Note: This option is only available if Squid is rebuilt with the
#       --enable-ssl option
#
#	Usage:  [ip:]port cert=certificate.pem [key=key.pem] [options...]
#
#	The socket address where Squid will listen for HTTPS client
#	requests.
#
#	This is really only useful for situations where you are running
#	squid in accelerator mode and you want to do the SSL work at the
#	accelerator level.
#
#	You may specify multiple socket addresses on multiple lines,
#	each with their own SSL certificate and/or options.
#
#	Options:
#
#	   accel	Accelerator mode. Also needs at least one of
#			defaultsite or vhost.
#
#	   defaultsite=	The name of the https site presented on
#	   		this port. Implies accel.
#
#	   vhost	Accelerator mode using Host header for virtual
#			domain support. Requires a wildcard certificate
#			or other certificate valid for more than one domain.
#			Implies accel.
#
#	   protocol=	Protocol to reconstruct accelerated requests with.
#			Defaults to https.
#
#	   cert=	Path to SSL certificate (PEM format).
#
#	   key=		Path to SSL private key file (PEM format)
#			if not specified, the certificate file is
#			assumed to be a combined certificate and
#			key file.
#
#	   version=	The version of SSL/TLS supported
#			    1	automatic (default)
#			    2	SSLv2 only
#			    3	SSLv3 only
#			    4	TLSv1 only
#
#	   cipher=	Colon separated list of supported ciphers.
#
#	   options=	Various SSL engine options. The most important
#			being:
#			    NO_SSLv2  Disallow the use of SSLv2
#			    NO_SSLv3  Disallow the use of SSLv3
#			    NO_TLSv1  Disallow the use of TLSv1
#			    SINGLE_DH_USE Always create a new key when using
#				      temporary/ephemeral DH key exchanges
#			See src/ssl_support.c or OpenSSL SSL_CTX_set_options
#			documentation for a complete list of options.
#
#	   clientca=	File containing the list of CAs to use when
#			requesting a client certificate.
#
#	   cafile=	File containing additional CA certificates to
#			use when verifying client certificates. If unset
#			clientca will be used.
#
#	   capath=	Directory containing additional CA certificates
#			and CRL lists to use when verifying client certificates.
#
#	   crlfile=	File of additional CRL lists to use when verifying
#			the client certificate, in addition to CRLs stored in
#			the capath. Implies VERIFY_CRL flag below.
#
#	   dhparams=	File containing DH parameters for temporary/ephemeral
#			DH key exchanges.
#
#	   sslflags=	Various flags modifying the use of SSL:
#			    DELAYED_AUTH
#				Don't request client certificates
#				immediately, but wait until acl processing
#				requires a certificate (not yet implemented).
#			    NO_DEFAULT_CA
#				Don't use the default CA lists built in
#				to OpenSSL.
#			    NO_SESSION_REUSE
#				Don't allow for session reuse. Each connection
#				will result in a new SSL session.
#			    VERIFY_CRL
#				Verify CRL lists when accepting client
#				certificates.
#			    VERIFY_CRL_ALL
#				Verify CRL lists for all certificates in the
#				client certificate chain.
#
#	   sslcontext=	SSL session ID context identifier.
#
#	   vport	Accelerator with IP based virtual host support.
#
#	   vport=NN	As above, but uses specified port number rather
#			than the https_port number. Implies accel.
#
#	   name=	Specifies a internal name for the port. Defaults to
#			the port specification (port or addr:port)
#
#Default:
# none
%if not %%is_empty(%%https_port)
https_port %%https_port transparent
%end if

#  TAG: tcp_outgoing_tos
#	Allows you to select a TOS/Diffserv value to mark outgoing
#	connections with, based on the username or source address
#	making the request.
#
#	tcp_outgoing_tos ds-field [!]aclname ...
#
#	Example where normal_service_net uses the TOS value 0x00
#	and good_service_net uses 0x20
#
#	acl normal_service_net src 10.0.0.0/255.255.255.0
#	acl good_service_net src 10.0.1.0/255.255.255.0
#	tcp_outgoing_tos 0x00 normal_service_net
#	tcp_outgoing_tos 0x20 good_service_net
#
#	TOS/DSCP values really only have local significance - so you should
#	know what you're specifying. For more information, see RFC2474,
#	RFC2475, and RFC3260.
#
#	The TOS/DSCP byte must be exactly that - a octet value  0 - 255, or
#	"default" to use whatever default your host has. Note that in
#	practice often only values 0 - 63 is usable as the two highest bits
#	have been redefined for use by ECN (RFC3168).
#
#	Processing proceeds in the order specified, and stops at first fully
#	matching line.
#
#	Note: The use of this directive using client dependent ACLs is
#	incompatible with the use of server side persistent connections. To
#	ensure correct results it is best to set server_persisten_connections
#	to off when using this directive in such configurations.
#Default:
# none

#  TAG: clientside_tos
#	Allows you to select a TOS/Diffserv value to mark client-side
#	connections with, based on the username or source address
#	making the request.
#Default:
# none

#  TAG: qos_flows
# Note: This option is only available if Squid is rebuilt with the
#       --enable-zph-qos option
#
#	Allows you to select a TOS/DSCP value to mark outgoing
#	connections with, based on where the reply was sourced.
#
#	TOS values really only have local significance - so you should
#	know what you're specifying. For more information, see RFC2474,
#	RFC2475, and RFC3260.
#
#	The TOS/DSCP byte must be exactly that - octet value 0x00-0xFF.
#	Note that in practice often only values up to 0x3F are usable
#	as the two highest bits have been redefined for use by ECN
#	(RFC3168).
#
#	This setting is configured by setting the source TOS values:
#
#	local-hit=0xFF		Value to mark local cache hits.
#
#	sibling-hit=0xFF	Value to mark hits from sibling peers.
#
#	parent-hit=0xFF		Value to mark hits from parent peers.
#
#
#	NOTE: 'miss' preserve feature is only possible on Linux at this time.
#
#	For the following to work correctly, you will need to patch your
#	linux kernel with the TOS preserving ZPH patch.
#	The kernel patch can be downloaded from http://zph.bratcheda.org
#
#	disable-preserve-miss
#		If set, any HTTP response towards clients will
#		have the TOS value of the response comming from the
#		remote server masked with the value of miss-mask.
#
#	miss-mask=0xFF
#		Allows you to mask certain bits in the TOS received from the
#		remote server, before copying the value to the TOS sent
#		towards clients.
#		Default: 0xFF (TOS from server is not changed).
#
#Default:
# none

#  TAG: tcp_outgoing_address
#	Allows you to map requests to different outgoing IP addresses
#	based on the username or source address of the user making
#	the request.
#
#	tcp_outgoing_address ipaddr [[!]aclname] ...
#
#	Example where requests from 10.0.0.0/24 will be forwarded
#	with source address 10.1.0.1, 10.0.2.0/24 forwarded with
#	source address 10.1.0.2 and the rest will be forwarded with
#	source address 10.1.0.3.
#
#	acl normal_service_net src 10.0.0.0/24
#	acl good_service_net src 10.0.2.0/24
#	tcp_outgoing_address 10.1.0.1 normal_service_net
#	tcp_outgoing_address 10.1.0.2 good_service_net
#	tcp_outgoing_address 10.1.0.3
#
#	Processing proceeds in the order specified, and stops at first fully
#	matching line.
#
#	Note: The use of this directive using client dependent ACLs is
#	incompatible with the use of server side persistent connections. To
#	ensure correct results it is best to set server_persistent_connections
#	to off when using this directive in such configurations.
#
#
#        IPv6 Magic:
#
#	Squid is built with a capability of bridging the IPv4 and IPv6
#	internets.
#	tcp_outgoing_address as exampled above breaks this bridging by forcing
#	all outbound traffic through a certain IPv4 which may be on the wrong
#	side of the IPv4/IPv6 boundary.
#
#	To operate with tcp_outgoing_address and keep the bridging benefits
#	an additional ACL needs to be used which ensures the IPv6-bound traffic
#	is never forced or permitted out the IPv4 interface.
#
#	acl to_ipv6 dst ipv6
#	tcp_outgoing_address 2002::c001 good_service_net to_ipv6
#	tcp_outgoing_address 10.1.0.2 good_service_net !to_ipv6
#
#	tcp_outgoing_address 2002::beef normal_service_net to_ipv6
#	tcp_outgoing_address 10.1.0.1 normal_service_net !to_ipv6
#
#	tcp_outgoing_address 2002::1 to_ipv6
#	tcp_outgoing_address 10.1.0.3 !to_ipv6
#
#	WARNING:
#	  'dst ipv6' bases its selection assuming DIRECT access.
#	  If peers are used the peername ACL are needed to select outgoing
#	  address which can link to the peer.
#
#	  'dst ipv6' is a slow ACL. It will only work here if 'dst' is used
#	  previously in the http_access rules to locate the destination IP.
#	  Some more magic may be needed for that:
#	    http_access allow to_ipv6 !all
#	  (meaning, allow if to IPv6 but not from anywhere ;)
#
#Default:
# none
#tcp_outgoing_address

#Evite les warnings de l'ipv6
#acl all src all
#acl manager proto cache_object #FIXME 2.5 : #10729
acl PURGE method purge
acl localdest dst 127.0.0.1/32
#ATTENTION a reporter dans 00_proxy.fw
acl SSL_ports port 443 563 631 4000-5000 6080 8062 8070 8090 8443 8753 7070
%for %%ssl_iter in %%ssl_ports
acl SSL_ports port %%ssl_iter
%end for
#ATTENTION a reporter dans 00_proxy.fw
acl Safe_ports port 80 21 443 563 70 210 631 1025-65535
acl Safe_ports port 280
acl Safe_ports port 488
acl Safe_ports port 591
acl Safe_ports port 777
%for %%safe_iter in %%safe_ports
acl Safe_ports port %%safe_iter
%end for
acl Reserved port 4200
acl CONNECT method CONNECT
#windows update
##Déporté dans acl noauth ...
#acl winupdate dstdomain .windowsupdate.microsoft.com
#acl winupdate dstdomain .update.microsoft.com
#acl winupdate dstdomain .c.microsoft.com
#acl winupdate dstdomain .windowsupdate.com
#
#MAJ Eole
# déplacé dans les templates domaines_noauth et domaines_nocache
#domaines accessibles sans authentification
acl noauth dstdomain "/etc/squid3/domaines_noauth"
acl noauth dstdomain "/etc/squid3/domaines_noauth_user"
acl noauth dstdomain "/etc/squid3/domaines_noauth_acad"
#domaines pas caches
acl nocache dstdomain "/etc/squid3/domaines_nocache"
acl nocache dstdomain "/etc/squid3/domaines_nocache_user"
acl nocache dstdomain "/etc/squid3/domaines_nocache_acad"
# sources à ne pas authentifier
acl srcnoauth src "/etc/squid3/src_noauth"
acl srcnoauth src "/etc/squid3/src_noauth_user"
acl srcnoauth src "/etc/squid3/src_noauth_acad"
# source sans cache
acl srcnocache src "/etc/squid3/src_nocache"
acl srcnocache src "/etc/squid3/src_nocache_user"
acl srcnocache src "/etc/squid3/src_nocache_acad"
#domaines auquels on peut accéder directement (pas passer par un proxy père)
acl nopeerproxy dstdomain "/etc/squid3/domaines_nopeerproxy"
%if %%is_defined('wan_route_adresse')
acl nopeerproxip dst "/etc/squid3/domaines_nopeerproxy_ip"
%end if

%if %%squid_nopeerproxy_url_regex == 'oui'
acl nopeerproxy url_regex "/etc/squid3/domaines_nopeerproxy_regex"
%end if
##
%if %%activer_cache_pere_zone == 'oui'
 %set %%list_nom_zone_dns_cache = []
 %for %%idx, %%zones_iter in %%enumerate(%%nom_cache_pere_zone)
  %if %%zones_iter.nom_zone_dns_cache not in %%list_nom_zone_dns_cache
   %if %%zones_iter.type_nom_zone_dns_cache == 'DNS'
acl intradom%%idx dstdomain %%zones_iter.nom_zone_dns_cache
   %else
acl intradom%%idx dstdomain "%%zones_iter.nom_zone_dns_cache"
   %end if
   %%list_nom_zone_dns_cache.append(%%zones_iter.nom_zone_dns_cache)
  %end if
 %end for
%end if
##Rvp
%if %%getVar('acces_proxy_zone_rvp', 'non') == 'oui'
 %for %%idx, %%net_iter in %%enumerate(%%adresse_network_zone_rvp)
acl rvpdom%%idx dst %%net_iter/%%calc_classe(%%net_iter.adresse_netmask_zone_rvp)
 %end for
%end if
##

## déf des acls pour eth0
%if %%nombre_interfaces == '1'
 %if %%mode_conteneur_actif == 'non'
  %set %%adresse_ip = %%adresse_ip_eth0
 %else
  %set %%adresse_ip = %%adresse_ip_eth0_proxy_link
 %end if
%%set_acl_interfaces('0', %%nom_machine, %%adresse_network_eth0, %%adresse_netmask_eth0, %%adresse_ip, %%vlan_eth0)
%end if

%if %%is_defined('proxy_eth0_ip') and not %%is_empty(%%proxy_eth0_ip)
# Adresse IP ou réseau supplémentaire autorisé à se connecter sur le proxy
 %for %%proxy_eth0 in %%proxy_eth0_ip
acl %%nom_machine src %%proxy_eth0/%%calc_classe(%%proxy_eth0.proxy_eth0_netmask)
acl reseaueth0 dst %%proxy_eth0/%%calc_classe(%%proxy_eth0.proxy_eth0_netmask)
 %end for
%end if

%%set_acl_ead('0', %%admin_eth0)
%%set_acl_alias('0', %%alias_eth0)

%if %%nombre_interfaces >= "2"
## déf des acls pour eth1
 %if %%mode_conteneur_actif == 'non'
  %set %%adresse_ip = %%adresse_ip_eth1
 %else
  %set %%adresse_ip = %%adresse_ip_eth1_proxy_link
 %end if
%%set_acl_interfaces('1', %%nom_machine_eth1, %%adresse_network_eth1, %%adresse_netmask_eth1, %%adresse_ip, %%vlan_eth1)
%%set_acl_ead('1', %%admin_eth1)
%%set_acl_alias('1', %%alias_eth1)
%end if

%if %%nombre_interfaces >= "3"
## déf des acls pour eth2
 %if %%mode_conteneur_actif == 'non'
  %set %%adresse_ip = %%adresse_ip_eth2
 %else
  %set %%adresse_ip = %%adresse_ip_eth2_proxy_link
 %end if
%%set_acl_interfaces('2', %%nom_machine_eth2, %%adresse_network_eth2, %%adresse_netmask_eth2, %%adresse_ip, %%vlan_eth2)
%%set_acl_ead('2', %%admin_eth2)
%%set_acl_alias('2', %%alias_eth2)
%end if

%if %%nombre_interfaces >= "4"
## déf des acls pour eth3
%%set_acl_interfaces('3', %%nom_machine_eth3, %%adresse_network_eth3, %%adresse_netmask_eth3, %%adresse_ip_eth3, %%vlan_eth3)
%%set_acl_ead('3', %%admin_eth3)
%%set_acl_alias('3', %%alias_eth3)
%end if

%if %%nombre_interfaces == "5"
## déf des acls pour eth4
%%set_acl_interfaces('4', %%nom_machine_eth4, %%adresse_network_eth4, %%adresse_netmask_eth4, %%adresse_ip_eth4, %%vlan_eth4)
%%set_acl_ead('4', %%admin_eth4)
%%set_acl_alias('4', %%alias_eth4)
%end if

acl proxy_port port 3128
%if %%is_defined('%dansguardian_port3')
acl proxy_port port %%dansguardian_port3
%end if
# On interdit de boucler sur lui-même
http_access deny to_localhost proxy_port

acl localdom dstdomain .%%nom_domaine_local
# acl for request coming from root container (used for snmp querying)
acl lxcroot src %%adresse_ip_br0/32

%if %%activer_proxy_sibling == 'oui'
#sibling
 %set %%squid_sibling = ''
 %for %%proxy_sibling in %%proxy_sibling_ip
  %set %%squid_sibling += ' ' + str(%%proxy_sibling) + '/32'
 %end for
acl src_sibling src %%squid_sibling
%end if
#  TAG: follow_x_forwarded_for
#	Allowing or Denying the X-Forwarded-For header to be followed to
#	find the original source of a request.
#
#	Requests may pass through a chain of several other proxies
#	before reaching us.  The X-Forwarded-For header will contain a
#	comma-separated list of the IP addresses in the chain, with the
#	rightmost address being the most recent.
#
#	If a request reaches us from a source that is allowed by this
#	configuration item, then we consult the X-Forwarded-For header
#	to see where that host received the request from.  If the
#	X-Forwarded-For header contains multiple addresses, we continue
#	backtracking until we reach an address for which we are not allowed
#	to follow the X-Forwarded-For header, or until we reach the first
#	address in the list. For the purpose of ACL used in the
#	follow_x_forwarded_for directive the src ACL type always matches
#	the address we are testing and srcdomain matches its rDNS.
#
#	The end result of this process is an IP address that we will
#	refer to as the indirect client address.  This address may
#	be treated as the client address for access control, ICAP, delay
#	pools and logging, depending on the acl_uses_indirect_client,
#	icap_uses_indirect_client, delay_pool_uses_indirect_client and
#	log_uses_indirect_client options.
#
#	This clause only supports fast acl types.
#	See http://wiki.squid-cache.org/SquidFaq/SquidAcl for details.
#
#	SECURITY CONSIDERATIONS:
#
#		Any host for which we follow the X-Forwarded-For header
#		can place incorrect information in the header, and Squid
#		will use the incorrect information as if it were the
#		source address of the request.  This may enable remote
#		hosts to bypass any access control restrictions that are
#		based on the client's source addresses.
#
#	For example:
#
#		acl localhost src 127.0.0.1
#		acl my_other_proxy srcdomain .proxy.example.com
#		follow_x_forwarded_for allow localhost
#		follow_x_forwarded_for allow my_other_proxy
#Default:
# follow_x_forwarded_for deny all
##EOLE
# DansGuardian
follow_x_forwarded_for allow localhost

#  TAG: acl_uses_indirect_client	on|off
#	Controls whether the indirect client address
#	(see follow_x_forwarded_for) is used instead of the
#	direct client address in acl matching.
#Default:
# acl_uses_indirect_client on
##EOLE
acl_uses_indirect_client on

#  TAG: delay_pool_uses_indirect_client	on|off
#	Controls whether the indirect client address
#	(see follow_x_forwarded_for) is used instead of the
#	direct client address in delay pools.
#Default:
# delay_pool_uses_indirect_client on
##EOLE
delay_pool_uses_indirect_client on

#  TAG: log_uses_indirect_client	on|off
#	Controls whether the indirect client address
#	(see follow_x_forwarded_for) is used instead of the
#	direct client address in the access log.
#Default:
# log_uses_indirect_client on
##EOLE
log_uses_indirect_client on

#  TAG: http_access
#	Allowing or Denying access based on defined access lists
#
#	Access to the HTTP port:
#	http_access allow|deny [!]aclname ...
#
#	NOTE on default values:
#
#	If there are no "access" lines present, the default is to deny
#	the request.
#
#	If none of the "access" lines cause a match, the default is the
#	opposite of the last line in the list.  If the last line was
#	deny, the default is allow.  Conversely, if the last line
#	is allow, the default will be deny.  For these reasons, it is a
#	good idea to have an "deny all" entry at the end of your access
#	lists to avoid potential confusion.
#
#	This clause supports both fast and slow acl types.
#	See http://wiki.squid-cache.org/SquidFaq/SquidAcl for details.
#
#Default:
# http_access deny all
#

#
# Recommended minimum Access Permission configuration:
#
# Only allow cachemgr access from localhost
http_access allow manager localhost
http_access deny manager
# only allow purge from localhost
http_access allow purge localhost
http_access deny purge
# Deny requests to unknown ports
http_access deny !Safe_ports
# Deny CONNECT to other than SSL ports
http_access deny CONNECT !SSL_ports

# We strongly recommend the following be uncommented to protect innocent
# web applications running on the proxy server who think the only
# one who can access services on "localhost" is a local user
#http_access deny to_localhost

#
# INSERT YOUR OWN RULE(S) HERE TO ALLOW ACCESS FROM YOUR CLIENTS
#

# Example rule allowing access from your local networks.
# Adapt localnet in the ACL section to list your (internal) IP networks
# from where browsing should be allowed
#http_access allow localnet
http_access allow localhost

## connexion à l'ead de l'amon pour les interfaces (a cette hauteur #2729)
%if %%nombre_interfaces == '1'
%%set_eth_ead('0', %%admin_eth0, %%vlan_eth0)
%end if

%if %%nombre_interfaces >= "2"
%%set_eth_ead('1', %%admin_eth1, %%vlan_eth1)
%end if

%if %%nombre_interfaces >= "3"
%%set_eth_ead('2', %%admin_eth2, %%vlan_eth2)
%end if

%if %%nombre_interfaces >= "4"
%%set_eth_ead('3', %%admin_eth3, %%vlan_eth3)
%end if

%if %%nombre_interfaces == "5"
%%set_eth_ead('4', %%admin_eth4, %%vlan_eth4)
%end if

# Autoriser reseau eth0 uniquement pour les stations de ce reseau
%if %%nombre_interfaces == '1'
 %if %%vlan_eth0 == "oui"
  %set %%http_vlans = ''
  %for %%vlans_0 in %%vlan_id_eth0
   %set %%http_vlans += ' !vlan_eth0_' + str(%%vlans_0)
  %end for
http_access deny reseaueth0 !%%nom_machine %%http_vlans
 %else
http_access deny reseaueth0 !%%nom_machine
 %end if
# Intradom
 %if %%activer_cache_pere_zone == 'oui'
  %for %%idx, %%zones_iter in %%enumerate(%%nom_cache_pere_zone)
   %if %%zones_iter.autoriser_proxy_autres == 'non'
    %set %%http_vlans = ''
    %if %%vlan_eth0 == "oui"
     %for %%vlans_0 in %%vlan_id_eth0
      %set %%http_vlans += ' !vlan_eth0_' + str(%%vlans_0)
     %end for
    %end if
http_access deny intradom%%idx !%%nom_machine %%http_vlans
   %end if
  %end for
 %end if
%end if

# Autoriser reseau eth1 uniquement pour les stations de ce reseau
%if %%nombre_interfaces >= "2"
 %set %%http_vlans = ''
 %if %%vlan_eth1 == "oui"
  %for %%vlans_1 in %%vlan_id_eth1
   %set %%http_vlans += ' !vlan_eth1_' + str(%%vlans_1)
  %end for
 %end if
http_access deny reseaueth1 !%%nom_machine_eth1 %%http_vlans
# Intradom
 %if %%activer_cache_pere_zone == 'oui'
  %for %%idx, %%zones_iter in %%enumerate(%%nom_cache_pere_zone)
   %if %%zones_iter.autoriser_proxy_autres == 'non'
    %set %%http_vlans = ''
    %if %%vlan_eth1 == "oui"
     %for %%vlans_1 in %%vlan_id_eth1
      %set %%http_vlans += ' !vlan_eth1_' + str(%%vlans_1)
     %end for
    %end if
http_access deny intradom%%idx !%%nom_machine_eth1 %%http_vlans
   %end if
  %end for
 %end if
%end if

## connexion aux reseaux internes
%if %%nombre_interfaces >= "3"
http_access allow reseaueth2
%end if
%if %%nombre_interfaces >= "4"
http_access allow reseaueth3
%if %%vlan_eth3 == "oui"
%for %%vlans3 in %%vlan_id_eth3
http_access allow reseaueth3_%%vlans3
%end for
%end if
%end if
%if %%nombre_interfaces >= "5"
http_access allow reseaueth4
%if %%vlan_eth4 == "oui"
%for %%vlans4 in %%vlan_id_eth4
http_access allow reseaueth4_%%vlans4
%end for
%end if
%end if
%if %%is_defined('nom_dmz')
http_access allow reseaudmz
%end if

#rvp
%if %%getVar('acces_proxy_zone_rvp', 'non') == 'oui'
 %for %%idx, %%net_iter in %%enumerate(%%adresse_network_zone_rvp)
  %if %%net_iter.autoriser_rvp_autres == 'oui'
http_access allow rvpdom%%idx
  %end if
 %end for
 %for %%idx, %%net_iter in %%enumerate(%%adresse_network_zone_rvp)
  %if %%net_iter.autoriser_rvp_autres == 'non'
   %if %%vlan_eth1 == "oui"
http_access allow rvpdom%%idx %%nom_machine_eth1
    %for %%idx2, %%vlans_1 in %%enumerate(%%vlan_id_eth1)
http_access allow rvpdom%%idx vlan_eth1_%%vlans_1
    %end for
http_access deny rvpdom%%idx all
   %else
http_access deny rvpdom%%idx !%%nom_machine_eth1
   %end if
  %end if
 %end for
%end if

#domaines accessibles sans auth
http_access allow noauth
http_access allow srcnoauth

