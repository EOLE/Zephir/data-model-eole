#! /bin/bash
### BEGIN INIT INFO
# Provides:          eqos
# Required-Start:    $local_fs $remote_fs
# Required-Stop:     $local_fs $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      S 0 1 6
# Short-Description: eqos initscript
# Description:       demarrage/arret du service eqosd
### END INIT INFO
#
# Author:    Equipe Eole <eole@ac-dijon.fr>.
#

set -e

#PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DESC="sondes EQOS"
NAME=eqos
DAEMON="/usr/share/eqos/eqosd.pl"
PIDFILE=/var/run/$NAME.pid
LOGFILE=/var/log/eqos.log
SCRIPTNAME=/etc/init.d/$NAME

. /lib/lsb/init-functions

# Gracefully exit if the package has been removed.
test -e $DAEMON || exit 0

cd /usr/share/eqos

#
#    Function that starts the daemon/service.
#
d_start() {
    log_begin_msg "Starting $DESC"
    if [ -f $PIDFILE ]
    then
        echo -n "$PIDFILE existe déjà"
        log_end_msg 1
    else
        # FIXME
        #start-stop-daemon -b --start --exec $DAEMON --pidfile $PIDFILE
        su eqos -s /bin/bash -c "$DAEMON" &>$LOGFILE &
        echo $! > $PIDFILE
        log_end_msg 0
    fi
}

#
#    Function that stops the daemon/service.
#
d_stop() {
    log_begin_msg "Stopping $DESC"
    if [ -f $PIDFILE ]
    then
        for count in `seq 1 10`
        do
            if ps h `cat $PIDFILE` > /dev/null
            then
                kill `cat $PIDFILE`
                sleep 1
            else
                rm $PIDFILE
                log_end_msg 0
                break
            fi
        done
    else
        echo -n "$DESC est déjà arrêté"
        # pour le restart
        log_end_msg 0
    fi
}

case "$1" in
    start)
        d_start
        ;;
    stop)
        d_stop
        ;;
    restart|force-reload)
        d_stop
        sleep 4
        d_start
        ;;
    status)
        if ! [ -f $PIDFILE ]
        then
             echo "$DESC est arrêté"
             #log_end_msg 1
             exit 0
        fi
        pid=`cat $PIDFILE`
        kill -0 $pid >/dev/null 2>&1
        if [ $? == 0 ]
        then
             echo "$DESC (pid $pid) est actif ..."
             #log_end_msg 0
             exit 0
        fi
        echo "$DESC est arrêté"
        #log_end_msg 1
        exit 0
        ;;
    *)
        echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
        exit 3
        ;;
esac

exit 0
