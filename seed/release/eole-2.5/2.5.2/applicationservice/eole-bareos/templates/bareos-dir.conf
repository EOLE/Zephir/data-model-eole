##############################################################################r
#                               Directeur                                     #
###############################################################################

Director {
  Name = %%bareos_dir_name
  DIRport = 9101
  QueryFile = "/etc/bareos/query.sql"
  WorkingDirectory = "/var/lib/bareos"
  PidDirectory = "/var/run/bareos"
  Maximum Concurrent Jobs = 1
  Heartbeat Interval = 10
  Password = "%%bareos_dir_password"         # Console password
  Messages = MessagesDir
}

# Generic catalog service
Catalog {
  Name = MyCatalog
  dbdriver = "%%bareos_db_type"
  dbname = "bareos"
  user = "bareos"
%if %%bareos_db_type == 'mysql'
  dbaddress = "%%adresse_ip_mysql"
  password = "%%bareos_db_mysql_password"
%else
  password = ""
%end if
}

Console {
  Name = bareos-%%{nom_machine}-mon
  Password = %%creole_client.get('bareos.monitor.bareos_mon_password')
  CommandACL = status, .status
}

%if %%getVar('activer_bareoswebui', 'non') == 'oui'
%for %%user in %%bareoswebui_user
Console {
  Name = "%%user"
  Password = "%%user.bareoswebui_password"
  CommandACL = cancel, messages, rerun, restore, run, show, status, version
  Job ACL = *all*
  Schedule ACL = *all*
  Catalog ACL = *all*
  Pool ACL = *all*
  Storage ACL = *all*
  Client ACL = *all*
  FileSet ACL = *all*
}
%end for
%end if

# FileDescriptor (toujours local)
Client {
  Name = %%{nom_machine}-fd
  Address = 127.0.0.1
  FDPort = 9102
  Catalog = MyCatalog
  Password = "%%bareos_fd_password"          # password for FileDaemon
  #AutoPrune = yes                     # Prune expired Jobs/Files
# On garde la liste des fichiers aussi longtemps que nécessaire
# La durée maximale est la rétention des volumes de sauvegarde complet
# ou la retention de base si on n'utilise pas des pool séparés
# File Retention = %%bareos_full_retention %%bareos_full_retention_unit
# Job Retention = %%bareos_full_retention %%bareos_full_retention_unit
}

Messages {
  Name = "MessagesDir"
  console = all, !skipped, !saved
  syslog = all, !skipped, !saved
}

###############################################################################
#                               Default                                       #
###############################################################################

# importe l'ensemble des programmations des sauvegardes
# le catalogue sera sauvegarde après chaque sauvegarde

JobDefs {
  Name = "JobDefsDefault"
  Type = Backup
  Level = Full
  Client = "%%{nom_machine}-fd"
  Storage = "%%{nom_machine}-sd" #CHANGER LE NOM
  Messages = "MessagesDefault"
%if %%bareos_sd_local == 'oui'
  # L'option "Mount Command" dans bareos-sd.conf est exécutée à chaque fois donc on mount ici
  # https://dev-eole.ac-dijon.fr/issues/8510
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosmount.py --mount"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
%end if
}

Schedule {
  Name = "ScheduleDefaultPre"
  @/etc/bareos/bareosschedule.conf
}

Schedule {
  Name = "ScheduleDefaultPost"
  @/etc/bareos/bareosschedulepost.conf
}

%if %%system_mail_from != ''
%set adresse_expediteur=%%system_mail_from
%else
%set adresse_expediteur="bareos-" + %%nom_machine + "@" + %%nom_domaine_local
%end if

Messages {
  Name = "MessagesDefault"
  append = "/var/lib/eole/reports/rapport-bareos.txt" = all, !skipped, !notsaved
  console = all, !skipped, !saved
  syslog = all, !skipped, !saved
  mailcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \'%e : %%numero_etab sauvegarde \"%n\" %%nom_machine.%%nom_domaine_local\' -- %r"
%set %%mail_error = %%custom_join(%%creole_client.get('/bareos/mail/mail_error'), ',')
%set %%mail_ok = %%custom_join(%%creole_client.get('/bareos/mail/mail_ok'), ',')
%if %%mail_error != '' and %%mail_ok != ''
  MailOnError = %%mail_error, %%mail_ok = all
  operatorcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \"%e : %%numero_etab sauvegarde %%nom_machine.%%nom_domaine_local\" -- %r"
  operator = %%mail_error = mount
%elif %%mail_error != ''
  MailOnError = %%mail_error = all
  operatorcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \"%e : %%numero_etab sauvegarde %%nom_machine.%%nom_domaine_local\" -- %r"
  operator = %%mail_error = mount
%elif %%mail_ok != ''
  MailOnError = %%mail_ok = all
%end if
}

# pour l'EAD, le rapport doit être écrasé en PRE backup pour ne contenir que la sauvegarde actuelle (Pre, Sauv, Catalogue, Post)
Messages {
  Name = "MessagesDefaultOverwrite"
  file = "/var/lib/eole/reports/rapport-bareos.txt" = all, !skipped, !notsaved
  console = all, !skipped, !saved
  syslog = all, !skipped, !saved
  mailcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \'%e : %%numero_etab sauvegarde \"%n\" %%nom_machine.%%nom_domaine_local\' -- %r"
%set %%mail_error = %%custom_join(%%creole_client.get('/bareos/mail/mail_error'), ',')
%set %%mail_ok = %%custom_join(%%creole_client.get('/bareos/mail/mail_ok'), ',')
%if %%mail_error != '' and %%mail_ok != ''
  MailOnError = %%mail_error, %%mail_ok = all
  operatorcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \"%e : %%numero_etab sauvegarde %%nom_machine.%%nom_domaine_local\" -- %r"
  operator = %%mail_error = mount
%elif %%mail_error != ''
  MailOnError = %%mail_error = all
  operatorcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \"%e : %%numero_etab sauvegarde %%nom_machine.%%nom_domaine_local\" -- %r"
  operator = %%mail_error = mount
%elif %%mail_ok != ''
  MailOnError = %%mail_ok = all
%end if
}

# Default pool definition
Pool {
  Name = "PoolDefault"
  Pool Type = Backup
  AutoPrune = yes
  Recycle = yes                       # Bareos can automatically recycle Volumes
  Maximum Volume Jobs = 1
  Maximum Volumes = 1
#  Accept Any Volume = yes             # write on any volume in the pool
  LabelFormat = "%%{bareos_dir_name}-volume-"
  Volume Retention = 1 minutes
  Recycle Oldest Volume = yes
  Recycle Current Volume = yes
  Maximum Volume Bytes = 1 gb
}

# StorageDescriptor (peut être local ou distant)
Storage {
  Name = "%%{nom_machine}-sd"
# Do not use "localhost" here
%if %%bareos_sd_local == 'oui'
  Address = "127.0.0.1"
%else
  Address = "%%bareos_sd_adresse"
%end if
  SDPort = 9103
  Password = "%%bareos_sd_password"
  Device = FileStorage
  Media Type = File
}

FileSet {
  Name = "FileSetDefault"
}

###############################################################################
#                               Schedule                                      #
###############################################################################

Job {
  Name = "JobSchedulePre"
  JobDefs = "JobDefsDefault"
  Messages = "MessagesDefaultOverwrite"
  Schedule = "ScheduleDefaultPre"
  FileSet = "FileSetDefault"
  Pool = "PoolDefault"
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --daemon=%d --jobID=%i --backup_progress --jobType=cronpre"
  ClientRunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --dir=%D --daemon=%d --jobID=%i --backup_progress --jobType=cronpre"
  ClientRunBeforeJob = "/usr/share/eole/schedule/schedule_bareos pre"
  ClientRunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --dir=%D --daemon=%d --jobID=%i --backup_ok --jobType=cronpre"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_ok --jobType=cronpre"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_err --jobType=cronpre"
  Priority = 10
  Max Wait Time = 600
}

Job {
  Name = "JobSchedulePost"
  JobDefs = "JobDefsDefault"
  Schedule = "ScheduleDefaultPost"
  FileSet = "FileSetDefault"
  Pool = "PoolDefault"
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --daemon=%d --jobID=%i --backup_progress --jobType=cronpost"
  ClientRunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --dir=%D --daemon=%d --jobID=%i --backup_progress --jobType=cronpost"
  ClientRunAfterJob = "/usr/share/eole/schedule/schedule_bareos post"
  ClientRunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --dir=%D --daemon=%d --jobID=%i --backup_ok --jobType=cronpost"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_ok --jobType=cronpost"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_err --jobType=cronpost"
  Priority = 99
}

###############################################################################
#                               Sauvegarde                                    #
###############################################################################

Messages {
  Name = "MessagesSauvegarde"
  append = "/var/lib/eole/reports/rapport-bareos.txt" = all, !skipped, !notsaved
  console = all, !skipped, !saved
  syslog = all, !skipped, !saved
  mailcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \'%e : %%numero_etab sauvegarde \"%n\" %%nom_machine.%%nom_domaine_local\' -- %r"
%if %%mail_error != ''
  MailOnError = %%mail_error = all
  operatorcommand = "/usr/bin/mutt -n -e \"set from=%%adresse_expediteur copy=no use_envelope_from=yes send_charset=utf-8\" -s \"%e : %%numero_etab sauvegarde %%nom_machine.%%nom_domaine_local\" -- %r"
  operator = %%mail_error = mount
%end if
%if %%mail_ok != ''
  Mail = %%mail_ok = all
%end if
}

Job {
  Name = "JobSauvegarde"
  Schedule = "ScheduleDefaultPre"
  FileSet = "FileSetSauvegarde"
  JobDefs = "JobDefsSauvegarde"
  Pool = "Full-Pool"
  Full Backup Pool = "Full-Pool"
  Differential Backup Pool = "Diff-Pool"
  Incremental Backup Pool = "Inc-Pool"
  Priority = 20
%if not %%is_empty(%%bareos_max_run_time)
  Max Run Time = %%bareos_max_run_time
%end if
}

JobDefs {
  Name = "JobDefsSauvegarde"
  Type = Backup
  Storage = "%%{nom_machine}-sd"
  Client = "%%{nom_machine}-fd"
  Messages = "MessagesSauvegarde"
  Write Bootstrap = "/var/lib/bareos/%%{bareos_dir_name}-JobDefsSauvegarde.bsr"
  # Debut Sauvegarde
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --daemon=%d --jobID=%i --backup_progress --jobType=sauvegarde"
  ClientRunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --dir=%D --daemon=%d --jobID=%i --backup_progress --jobType=sauvegarde"
  # bareosconfig effectue un test de montage, ce test démonte le support donc à effectuer AVANT le montage réel
%if %%bareos_sd_local == 'oui'
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosmount.py --mount"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
%end if
  ClientRunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --dir=%D --daemon=%d --jobID=%i --backup_ok --jobType=sauvegarde"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_ok --jobType=sauvegarde"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_err --jobType=sauvegarde"
}

# List of files to be backed up
FileSet {
  Name = "FileSetSauvegarde"
  Exclude {
    file = /proc
    file = /tmp
    file = /.journal
    file = /.fsck
  }
@|"sh -c 'for f in /etc/bareos/bareosfichiers.d/*.conf; do echo @${f}; done'"
}

# Pool for full backup
Pool {
  Name = Full-Pool
  Pool Type = Backup
  Recycle = yes
  AutoPrune = yes
  Volume Retention = %%bareos_full_retention %%bareos_full_retention_unit
#  Accept Any Volume = yes             # write on any volume in the pool
  LabelFormat = "%%{bareos_dir_name}-full-"
  Recycle Oldest Volume = yes
  # Max volume use is 2gb or 1 unit
  Maximum Volume Bytes = 2 gb
  Volume Use Duration = 1 %%bareos_full_retention_unit
}

# Pool for Differential backup
Pool {
  Name = Diff-Pool
  Pool Type = Backup
  Recycle = yes
  AutoPrune = yes
  Volume Retention = %%bareos_diff_retention %%bareos_diff_retention_unit
#  Accept Any Volume = yes             # write on any volume in the pool
  LabelFormat = "%%{bareos_dir_name}-diff-"
  Recycle Oldest Volume = yes
  # Max volume use is 2gb or 1 unit
  Maximum Volume Bytes = 2 gb
  Volume Use Duration = 1 %%bareos_diff_retention_unit
}

# Pool for incremental backup
Pool {
  Name = Inc-Pool
  Pool Type = Backup
  Recycle = yes
  AutoPrune = yes
  Volume Retention = %%bareos_inc_retention %%bareos_inc_retention_unit
#  Accept Any Volume = yes             # write on any volume in the pool
  LabelFormat = "%%{bareos_dir_name}-inc-"
  Recycle Oldest Volume = yes
  # Max volume use is 2gb or retention -1 (avoid very little volumes)
  Maximum Volume Bytes = 2 gb
  Volume Use Duration = 1 %%bareos_inc_retention_unit
}


###############################################################################
#                               Catalog                                       #
###############################################################################


### Catalog Bareos ###
### le catalog est sur bareos-dir ###
#
Pool {
  Name = "PoolCatalog"
  Pool Type = Backup
  Recycle = yes                       # Bareos can automatically recycle Volumes
  AutoPrune = no                     # Prune expired volumes
  Volume Retention = 1 seconds
  Recycle Current Volume = yes
  Maximum Volumes = 1
#  Accept Any Volume = yes             # write on any volume in the pool
  LabelFormat = "%%{bareos_dir_name}-catalog-"
  Recycle Oldest Volume = yes
  Maximum Volume Bytes = 2 gb
  Maximum Volume Jobs = 1
}

JobDefs {
  Name = "JobDefsCatalog"
  Type = Backup
  Storage = "%%{nom_machine}-sd"
  Client = "%%{nom_machine}-fd"
  Messages = "MessagesDefault"
  Write Bootstrap = "/var/lib/bareos/%%{bareos_dir_name}-JobDefsCatalog.bsr"
  # Debut Sauvegarde
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --daemon=%d --jobID=%i --backup_progress --jobType=catalogue"
  ClientRunBeforeJob = "sudo /usr/share/eole/sbin/bareosconfig.py --lock --dir=%D --daemon=%d --jobID=%i --backup_progress --jobType=catalogue"
%if %%bareos_sd_local == 'oui'
  # bareosconfig effectue un test de montage, ce test démonte le support donc à effectuer AVANT le montage réel
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosmount.py --mount"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
%end if
%if %%bareos_db_type == 'mysql'
  RunBeforeJob = "/usr/lib/bareos/scripts/make_catalog_backup bareos bareos %%bareos_db_mysql_password %%adresse_ip_mysql mysql"
%else
  RunBeforeJob = "/usr/lib/bareos/scripts/make_catalog_backup bareos bareos \"\" localhost sqlite3"
%end if
  RunAfterJob = "/usr/lib/bareos/scripts/delete_catalog_backup"
  ClientRunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --dir=%D --daemon=%d --jobID=%i --backup_ok --jobType=catalogue"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_ok --jobType=catalogue"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosconfig.py --unlock --daemon=%d --jobID=%i --backup_err --jobType=catalogue"
}

Job {
  Name = "BackupCatalog"
  JobDefs = "JobDefsCatalog"
  FileSet = "FileSetCatalog"
  Schedule = "ScheduleDefaultPost"
#  Prune Jobs = "yes"
  Pool = "PoolCatalog"
  Priority = 98
}

# This is the backup of the catalog
FileSet {
  Name = "FileSetCatalog"
  Include {
    Options {
      signature = MD5
    }
    # le catalog est sauvegarde avec RunBeforeJob
    # on sauvegarde un petit fichier
    File = /etc/eole/bareos.conf # fichier pour personnaliser la commande de
                                 # montage, ...
    File = /etc/eole/config.eol
    File = /var/lib/bareos/bareos.sql
    File = /var/lib/bareos/%%{bareos_dir_name}-JobDefsSauvegarde.bsr
    File = /var/lib/bareos/%%{bareos_dir_name}-JobDefsCatalog.bsr
  }
}

###############################################################################
#                               Restauration                                  #
###############################################################################

Messages {
  Name = "MessagesRestore"
  file = "/var/log/bareos/restore.txt" = all, !skipped, !notsaved
  console = all, !skipped, !saved
  syslog = all, !skipped
}

JobDefs {
  Name = "JobDefsRestore"
  Type = Restore
  Client = %%{nom_machine}-fd
  FileSet="FileSetSauvegarde"
  Storage = %%{nom_machine}-sd
  Pool = Full-Pool
  Full Backup Pool = Full-Pool
  Differential Backup Pool = Diff-Pool
  Incremental Backup Pool = Inc-Pool
  Messages = MessagesRestore
  Where = /
%if %%bareos_sd_local == 'oui'
  RunBeforeJob = "sudo /usr/share/eole/sbin/bareosmount.py --mount"
  RunAfterJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
  RunAfterFailedJob = "sudo /usr/share/eole/sbin/bareosmount.py --umount"
%end if
}

@/etc/bareos/bareos-restore.conf
