#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################################
#                                                                          #
#  Utilitaire de configuration du support pour bareos                      #
#                                                                          #
############################################################################

from optparse import OptionParser, OptionGroup
import sys

from pyeole.bareos import save_bareos_support_usb, save_bareos_support_smb, \
        save_bareos_support_manual, save_bareos_mail, load_bareos_mail,\
        load_bareos_support, pprint_bareos_jobs, \
        add_job, del_job, \
        run_backup, Disabled, bareos_rapport_in_progress, bareos_rapport_err, \
        bareos_rapport_ok, test_bareos_support, write_bareos_schedule
import pyeole.lock
from pyeole.ansiprint import print_red
from pyeole.lock import acquire, release, get_system_lock_name, is_name_locked

def main():
    def conf_support():
        support = options.support
        usb_path = options.usb_path
        smb_machine = options.smb_machine
        smb_ip = options.smb_ip
        smb_partage = options.smb_partage
        smb_login = options.smb_login
        smb_password = options.smb_password
        no_reload = options.no_reload
        try:
            if support == 'usb':
                if usb_path is None:
                    raise Exception("L'option --usb_path est obligatoire pour une sauvegarde sur support USB")
                save_bareos_support_usb(usb_path=usb_path, no_reload=no_reload)
            elif support == 'smb':
                save_bareos_support_smb(smb_machine=smb_machine,
                    smb_ip=smb_ip, smb_partage=smb_partage,
                    smb_login=smb_login, smb_password=smb_password,
                    no_reload=no_reload)
            elif support == 'manual':
                save_bareos_support_manual(no_reload=no_reload)
            else:
                raise Exception("Support {} inconnu".format(support))
        except Disabled:
            pass
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)

    def conf_mail():
        no_reload = options.no_reload
        try:
            save_bareos_mail(mail_ok=options.mail_ok,
                    mail_error=options.mail_error, no_reload=no_reload)
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)

    def conf_job():
        no_reload = options.no_reload
        try:
            add_job(job=options.job, level=options.job_level,
                    day=options.job_day, hour=options.job_hour,
                    end_day=options.job_end_day, no_reload=no_reload)
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)

    def display_infos():
        begin = "Support : {0}"
        try:
            ret = str(load_bareos_support())
            print begin.format(ret)
        except Disabled:
            pass
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)

        begin = "Mail : {0}"
        try:
            ret = load_bareos_mail()
            if ret == False:
                print begin.format('non configuré')
            else:
                print begin.format(str(ret))
        except Disabled:
            pass
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)

        msg = "Programmation : "
        try:
            jobs = pprint_bareos_jobs()
            if jobs == None:
                print msg + 'non configuré'
            else:
                print msg
                print jobs
        except Disabled:
            pass
        except Exception as err:
            import traceback
            traceback.print_exc()
            print_red(unicode(err))
            sys.exit(1)


    def run_back():
        try:
            status, comment = test_bareos_support()
            print comment
        except Exception as err:
            print_red(u'Erreur au test de montage : ' + unicode(err))
            sys.exit(1)
        if status == False:
            print_red(u'Erreur au test de montage')
            sys.exit(1)
        try:
            print run_backup(options.level)
        except Exception as err:
            print_red(unicode(err))
            sys.exit(1)


    #get options
    parser = OptionParser(usage="Seulement une option \n%prog [option]")
    parser.add_option("-d", "--display", action="store_true", dest="display",
            default=False, help=u"Liste les configurations")
    parser.add_option("--no-reload", action="store_true", dest="no_reload",
            default=False, help=u"Ne pas recharger Bareos lors de l'application")
    parser.add_option("-a", "--apply", action="store_true", dest="apply_c",
            default=False, help=u"Force l'aplication des configurations")

    group = OptionGroup(parser, "Configuration du support de sauvegarde")
    group.add_option("-s", "--support", dest="support", help=u"Type du support de sauvegarde (usb, smb ou manual)")
    group.add_option("--usb_path", dest="usb_path", help=u"Support usb : chemin du disque usb")
    group.add_option("--smb_machine", dest="smb_machine", help=u"Support smb : nom machine avec le partage")
    group.add_option("--smb_ip", dest="smb_ip", help=u"Support smb : ip de la machine avec le partage")
    group.add_option("--smb_partage", dest="smb_partage", help=u"Support smb : nom du partage")
    group.add_option("--smb_login", dest="smb_login", help=u"Support smb : nom de l'utilisateur smb (facultatif)")
    group.add_option("--smb_password", dest="smb_password", help=u"Support smb : mot de passe smb (facultatif)")
    parser.add_option_group(group)

    group2 = OptionGroup(parser, "Configuration des adresses mails")
    group2.add_option("-m", "--mail", dest="mail", action="store_true", default=False,
            help=u"Configuration des adresses mails de sauvegarde (sans autre option, suppression)")
    group2.add_option("--mail_ok", dest="mail_ok", help=u"    Mail admin sauvegarde (facultatif)")
    group2.add_option("--mail_error", dest="mail_error", help=u"    Mail admin sauvegarde pour les erreurs (facultatif)")
    parser.add_option_group(group2)

    group3 = OptionGroup(parser, "Programmation des sauvegardes")
    group3.add_option("-j", "--job", dest="job", help=u"Type de programmation (daily, weekly ou monthly)")
    group3.add_option("--job_level", dest="job_level", help=u"Niveau de programmation (Full, Incremental ou Differential)")
    group3.add_option("--job_day", dest="job_day", help=u"Jour de programmation (1 pour la nuit du dimanche au lundi, ..., 7 du samedi au dimanche). La nuit commence à 12h")
    group3.add_option("--job_end_day", dest="job_end_day", help=u"Si programmation daily, nuit de la fin de la plage")
    group3.add_option("--job_hour", dest="job_hour", help=u"Heure de programmation (chiffre de 0 à 23), la nuit commence à 12h et finit à 11h le lendemain")
    group3.add_option("-x", "--del_job", dest="del_job", help=u"Suppression du job (numéro")
    parser.add_option_group(group3)

    group4 = OptionGroup(parser, "Lancer une sauvegarde")
    group4.add_option("-n", "--now", dest="now", help=u"Lancer une sauvegarde immédiate", action="store_true", default=False)
    group4.add_option("--level", dest="level", help=u"Niveau de sauvegarde (Full, Incremental ou Differential)")
    parser.add_option_group(group4)

    group5 = OptionGroup(parser, "Gestion des verrous")
    group5.add_option("--lock", dest="lock", help=u"Mettre un verrou de sauvegarde", action="store_true", default=False)
    group5.add_option("--unlock", dest="unlock", help=u"Supprimer le verrou de sauvegarde", action="store_true", default=False)
    group5.add_option("--daemon", dest="daemon", help=u"Identifiant du démon", default='eolesauvegarde')
    group5.add_option("--dir", dest="director", help=u"Identifiant du directeur")
    group5.add_option("--fd", dest="client", help=u"Identifiant du client")
    group5.add_option("--jobID", dest="job_id", help=u"Identifiant du job")
    group5.add_option("--backup_progress", dest="backup_progress", help=u"Rapport : la sauvegarde est en cours", action="store_true", default=False)
    group5.add_option("--backup_ok", dest="backup_ok", help=u"Rapport : la sauvegarde s'est terminée avec succès", action="store_true", default=False)
    group5.add_option("--backup_err", dest="backup_err", help=u"Rapport : la sauvegarde s'est terminée par une erreur", action="store_true", default=False)
    group5.add_option("--jobType", dest="job_type", help=u"Indiquer le type de tâche (cronpre, cronpost, sauvegarde ou catalogue)")
    parser.add_option_group(group5)


    (options, args) = parser.parse_args()

    if options.support == None and options.mail == False and options.job == None \
            and options.display == False and options.now == False \
            and options.lock == False and options.del_job == None \
            and options.unlock == False and options.backup_err == False \
            and options.apply_c == False:
        parser.print_help()
        sys.exit(1)


    if options.support != None:
        conf_support()

    if options.mail != False:
        conf_mail()

    if options.job != None:
        conf_job()

    if options.del_job != None:
        del_job(options.del_job, no_reload=options.no_reload)

    if options.display != False:
        display_infos()

    if options.now != False:
        run_back()

    if options.backup_progress != False:
        bareos_rapport_in_progress(options.job_type)

    if options.backup_ok != False:
        bareos_rapport_ok(options.job_type)

    if options.backup_err != False:
        bareos_rapport_err(options.job_type)

    if options.lock != False:
        locks = get_system_lock_name()
        if options.job_id is None:
            print_red(u"Erreur : l'option --lock nécessite l'identifiant du job")
            sys.exit(1)
        pyeole.lock.PID = options.job_id
        if options.director != None:
            #remplacer "." par "_" #8481
            lock_name = options.director.replace('.', '_')
            if len(locks) == 1 and is_name_locked(lock_name, level='system'):
                print u'Lock système déjà placé : {}'.format(locks).encode("utf-8")
                print u'Inutile de placer un lock supplémentaire'.encode('utf-8')
            elif not locks:
                #remplacer "." par "_" #8481
                lock_name = options.daemon.replace('.', '_')
                acquire(lock_name, level='system')
            else:
                print_red(u'Lock système déjà placé : {}'.format(locks).encode("utf-8"))
                sys.exit(1)
        elif not locks:
            try:
                status, comment = test_bareos_support()
            except Exception as err:
                print_red(u'Erreur au test de montage : ' + unicode(err))
                sys.exit(1)
            #remplacer "." par "_" #8481
            lock_name = options.daemon.replace('.', '_')
            acquire(lock_name, level='system')
        else:
            for lock in locks:
                print_red(u'Lock déjà présent : ' + unicode(lock))
            sys.exit(1)

    if options.unlock != False:
        if options.job_id is None:
            print u"Erreur : l'option --unlock nécessite l'identifiant du job"
            sys.exit(1)
        pyeole.lock.PID = options.job_id
        #remplacer "." par "_" #8481
        lock_name = options.daemon.replace('.', '_')

        if is_name_locked(lock_name, level='system'):
            release(lock_name, level='system')

    if options.apply_c:
        write_bareos_schedule()


if __name__ == "__main__":
    main()
