#!/usr/bin/env python
"""Restauration de tous les fichiers"""
from pyeole.bareosrestore import bareos_restore_all_files, exit_if_running_jobs
import sys

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """all_files helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)

def pre():
    print "pre all_files"

def post():
    print "post all_files"

def job(jobid):
    print "Restauration des fichiers"
    bareos_restore_all_files(jobid)


priority=80

