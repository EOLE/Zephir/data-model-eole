#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Lanceur

"""

import sys
from optparse import OptionParser
#from operator import itemgetter, attrgetter
from creole.config import bareos_restore_root, bareos_restore
sys.path.insert(0, bareos_restore_root)
sys.path.insert(0, bareos_restore)
import restore

from pyeole.bareosrestore import exit_if_running_jobs
from pyeole.bareosrestoreplugins import list_bareos_restore, filter_built
# ____________________________________________________________

class Option(object):
    """
    Objet de creation de l'import
    """

    def __init__(self, name, doc, jobid):
        self.name = name
        self.doc = doc
        self.jobid = jobid

    def add_optparse_option(self, parser):
        """
        bind entre le parser et la fonction
        """
        parser.add_option("--"+self.name, help=self.doc,
                        action='callback',
                        callback=self.callback,
                        callback_args=(self.jobid,))

# ____________________________________________________________

def utils_imp(module, plug_options):
    """Utilitaire d'import du module plugin"""
    return __import__(module, globals(), locals(), plug_options)

def add_options(option_names):
    """Ajout des fonctions a partir des plugins"""
    descr = []
    #on recharge le module plugin pour pouvoir importer les modules necessaires
    plugin_imp = utils_imp('restore', option_names)
    option = Option('all', 'Toutes les options', 0)
    setattr(option, 'callback', opt_all)
    descr.append(option)
    list_bareos = list_bareos_restore()
    for name in option_names:
        func = sys.modules['restore.'+name]
        jobid = list_bareos.index(name)+1
        option = Option(name, func.__doc__, jobid)
        setattr(option, 'callback', func.execute)
        descr.append(option)
    return descr

def opt_all(option, opt_str, value, parser, fake_jobid):
    """Excution des fonction pour toutes les options"""
    exit_if_running_jobs()
    plug_options = filter_built(restore)
    plugins = utils_imp('restore', plug_options)
    plug_options_ordered = []
    list_bareos = list_bareos_restore()
    #gestion des priorites lors de l'appel du module
    for plug in plug_options:
        func = sys.modules['restore.'+plug]
        jobid = list_bareos.index(plug)+1
        if not dir(func).__contains__('priority'):
            print "need priority for {0}".format(str(func))
        #exclude script with priority 0
        if func.priority != 0:
            plug_options_ordered.append((func.priority, func.execute, jobid))

    plug_options_ordered = sorted(plug_options_ordered)
    for plug in plug_options_ordered:
        plug[1](option, opt_str, value, parser, plug[2], False)
        print ""

def main():
    """Fonction principal"""
    parser = OptionParser()
    plug_options = filter_built(restore)
    descr = add_options(plug_options)
    #config = Config(descr)
    for opt in descr:
        opt.add_optparse_option(parser)
    parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

if __name__ == '__main__':
    main()
