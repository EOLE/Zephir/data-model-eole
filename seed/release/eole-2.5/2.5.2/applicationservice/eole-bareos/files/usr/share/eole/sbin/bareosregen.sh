#!/bin/bash

MOUNT_DIR='/mnt/sauvegardes'
BAREOS_DB='/var/lib/bareos/bareos.db'
DDL_MYSQL='/usr/lib/bareos/scripts/ddl/grants/mysql.sql'

#if database is already exists and noquestion set, do nothing
if [ ! -z $1 ]; then
    noquestion=1
    if [ "$1" = "noquestion" ]; then
        restart_mysql=1
    else
        extract_file=$1
    fi
fi

regen_user_bdd=0

function regen_mysql_pwd() {
    /usr/share/eole/sbin/mysql_pwd.py $1 nomodif
}

if [ ! -z $2 ]; then
    bareos_db_type=$2
else
    bareos_db_type=$(CreoleGet bareos_db_type)
fi
if [ "$bareos_db_type" = "mysql" ]; then
    passwd=$(pwgen 40 1)
    options="-uroot -p$passwd -h $(CreoleGet adresse_ip_mysql)"
fi

if [ "$noquestion" = "1" -o "$(CreoleGet activer_bareos_dir)" = "oui" ]; then
    . /usr/lib/eole/ihm.sh
    #(re)generation du catalog
    rep=0
    if [ "$bareos_db_type" = "sqlite3" ]; then
        [ -e $BAREOS_DB ] && allready_generate=1
    elif [ "$bareos_db_type" = "mysql" ]; then
        if [ $(python -c "from eolesql.db_test import db_exists; print db_exists('bareos');") = "True" ]; then
            allready_generate=1
        fi
    else
        echo "FIXME a faire"
        exit 1
    fi
    if [ ! -z $allready_generate ]; then
        if [ "$noquestion" = "1" ]; then
            if [ ! -z $extract_file ]; then
                rep=0
            else
                rep=1
            fi
        else
            Question_ouinon "La régénération du catalogue de la sauvegarde va écraser l'ancienne base, confirmez-vous ?" "$interactive" 'non' 'warn'
            rep=$?
        fi
    fi
    if [ "$rep" = "0" ]; then
        echo
        echo "## Régénération du catalogue Bareos##"
        if [ -z $restart_mysql ]; then
            CreoleService bareos-dir stop
            CreoleService bareos-sd stop
        elif [ "$bareos_db_type" = "mysql" ]; then
            CreoleService mysql start
        fi
        if [ "$bareos_db_type" = "mysql" ]; then
            regen_mysql_pwd $passwd
        fi
        [ ! -z $allready_generate ] && /usr/lib/bareos/scripts/drop_bareos_database $options
        /usr/lib/bareos/scripts/create_bareos_database $options
        if [ "$bareos_db_type" = "mysql" ] || [ -z $extract_file ]; then
            /usr/lib/bareos/scripts/make_bareos_tables $options
        fi
        if [ ! -z $extract_file ]; then
            if [ "$bareos_db_type" = "sqlite3" ]; then
                /usr/bin/sqlite3 $BAREOS_DB < $extract_file
            elif [ "$bareos_db_type" = "mysql" ]; then
                mysql $options bareos < $extract_file
            fi
        fi
        regen_user_bdd=1
        need_startstop=1
    else
        if [ -z $restart_mysql ]; then
            exit 0
        fi
        #regarde s'il y a un mot de passe à l'utilisateur bareos
        if [ "$bareos_db_type" = "mysql" ]; then
            if [ ! -z $restart_mysql ]; then
                CreoleService mysql start
            fi
            echo "exit" | mysql -ubareos bareos &> /dev/null
            [ $? -eq 0 ] && regen_user_bdd=1
            need_startstop=1
        fi
    fi
fi

if [ "$regen_user_bdd" = "1" ] && [ "$bareos_db_type" = "mysql" ]; then
    if [ -z $restart_mysql ] && [ ! "$need_startstop" = "1" ]; then
        CreoleService mysql start
    fi
    regen_mysql_pwd $passwd
    mode_conteneur_actif=$(CreoleGet mode_conteneur_actif)
    if [ $mode_conteneur_actif = 'oui' ]; then
        cp -a $DDL_MYSQL /tmp/mysql.sql
        echo "USE mysql
GRANT ALL PRIVILEGES ON TABLE @DB_NAME@.* TO @DB_USER@@$(CreoleGet adresse_ip_br0) @DB_PASS@;
FLUSH PRIVILEGES;" > $DDL_MYSQL
    fi
    /usr/lib/bareos/scripts/grant_bareos_privileges $options
    if [ "$bareos_db_type" = "mysql" -a $mode_conteneur_actif = 'oui' ]; then
        cp -a /tmp/mysql.sql $DDL_MYSQL
    fi
    [ -z $restart_mysql ] && [ ! "$need_startstop" = "1" ] && CreoleService mysql stop
fi

if [ "$need_startstop" = "1" ]; then
    echo "Régénération du catalogue terminée"
    echo "Suppression des anciens rapports d'état"
    sleep 5
    rm -f /var/lib/bareos/*.state
    if [ -z $restart_mysql ]; then
        CreoleService bareos-dir start
        CreoleService bareos-sd start
    elif [ "$bareos_db_type" = "mysql" ]; then
        CreoleService mysql stop
    fi
fi
