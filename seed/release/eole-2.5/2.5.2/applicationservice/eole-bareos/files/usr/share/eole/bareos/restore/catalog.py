#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Restauration du catalogue de bareos (attention il faut savoir ce que l'on fait !)"""
from pyeole.bareosrestore import extract_bareos_files, bareos_restore_dump, \
        restore_file
from creole.loader import load_store
from sys import exit

catalogfile = '/var/lib/bareos/bareos.sql'
class fake_config:
    def impl_set_information(*args):
        pass

def execute(option, opt_str, value, parser, jobid, test_jobs=False):
    """catalog helper
    never test jobs
    """
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            job(option, jobid)
    else:
        print "Il faut spécifier le nom du directeur Bareos"
        exit(1)

def pre():
    print "pre catalog"

def post():
    print "post catalog"

def job(bareos_dir_name, jobid):
    print "Restauration du catalogue"
    try:
        extract_bareos_files(bareos_dir_name=bareos_dir_name)
        store = load_store(fake_config(), restore_file)
        catalog_db_type = store.get('bareos_db_type', {'val': None})['val']
        bareos_db_mysql_password = store.get('bareos_db_mysql_password', {'val': None})['val']
        bareos_restore_dump(catalog_db_type=catalog_db_type, password=bareos_db_mysql_password)
    except Exception,e:
        print e
        exit(1)

priority=0

