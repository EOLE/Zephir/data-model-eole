#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Restauration du fichier passé en argument"""
from pyeole.bareosrestore import bareos_restore_one_file, exit_if_running_jobs
import sys

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """file helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            if test_jobs:
                exit_if_running_jobs()
            job(option, jobid)

    else:
        print "Il faut sprécifier le nom du fichier à restaurer"
        sys.exit(1)

def pre():
    print "pre file"

def post():
    print "post file"

def job(filename, jobid):
    print "Restauration du fichier {0}".format(filename)
    bareos_restore_one_file(filename, jobid)


priority=0
