-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: 127.0.0.1
-- Généré le : Mar 25 Juin 2013 à 10:12
-- Version du serveur: 5.1.69
-- Version de PHP: 5.3.2-1ubuntu4.19


CREATE DATABASE spip;

-- création du user de la base
GRANT ALL PRIVILEGES ON spip.* to 'spip'@'%%adresse_ip_web' identified by 'spip';
%if %%mode_conteneur_actif != "non"
GRANT ALL PRIVILEGES ON spip.* to 'spip'@'%%adresse_ip_br0' identified by 'spip';
%end if
flush privileges ;

-- connexion à la base
\r spip

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET character_set_client = utf8;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `spip`
--

-- --------------------------------------------------------

--
-- Structure de la table `spip_articles`
--

CREATE TABLE IF NOT EXISTS `spip_articles` (
  `id_article` bigint(21) NOT NULL AUTO_INCREMENT,
  `surtitre` text NOT NULL,
  `titre` text NOT NULL,
  `soustitre` text NOT NULL,
  `id_rubrique` bigint(21) NOT NULL DEFAULT '0',
  `descriptif` text NOT NULL,
  `chapo` mediumtext NOT NULL,
  `texte` longtext NOT NULL,
  `ps` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `id_secteur` bigint(21) NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `export` varchar(10) DEFAULT 'oui',
  `date_redac` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visites` int(11) NOT NULL DEFAULT '0',
  `referers` int(11) NOT NULL DEFAULT '0',
  `popularite` double NOT NULL DEFAULT '0',
  `accepter_forum` char(3) NOT NULL DEFAULT '',
  `date_modif` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lang` varchar(10) NOT NULL DEFAULT '',
  `langue_choisie` varchar(3) DEFAULT 'non',
  `id_trad` bigint(21) NOT NULL DEFAULT '0',
  `nom_site` tinytext NOT NULL,
  `url_site` varchar(255) NOT NULL DEFAULT '',
  `virtuel` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_article`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `id_trad` (`id_trad`),
  KEY `lang` (`lang`),
  KEY `statut` (`statut`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_articles`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_auteurs`
--

CREATE TABLE IF NOT EXISTS `spip_auteurs` (
  `id_auteur` bigint(21) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `bio` text NOT NULL,
  `email` tinytext NOT NULL,
  `nom_site` tinytext NOT NULL,
  `url_site` text NOT NULL,
  `login` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pass` tinytext NOT NULL,
  `low_sec` tinytext NOT NULL,
  `statut` varchar(255) NOT NULL DEFAULT '0',
  `webmestre` varchar(3) NOT NULL DEFAULT 'non',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pgp` text NOT NULL,
  `htpass` tinytext NOT NULL,
  `en_ligne` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alea_actuel` tinytext,
  `alea_futur` tinytext,
  `prefs` tinytext,
  `cookie_oubli` tinytext,
  `source` varchar(10) NOT NULL DEFAULT 'spip',
  `lang` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_auteur`),
  KEY `login` (`login`),
  KEY `statut` (`statut`),
  KEY `en_ligne` (`en_ligne`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `spip_auteurs`
--

INSERT INTO `spip_auteurs` (`id_auteur`, `nom`, `bio`, `email`, `nom_site`, `url_site`, `login`, `pass`, `low_sec`, `statut`, `webmestre`, `maj`, `pgp`, `htpass`, `en_ligne`, `alea_actuel`, `alea_futur`, `prefs`, `cookie_oubli`, `source`, `lang`) VALUES
%if %%is_defined('domaine_messagerie_etab')
(1, 'admin', '', 'admin@%%domaine_messagerie_etab', '', '', 'admin', '16cd931847f168d33ce3bd2400f7a5a90d3a37dab93f7ddfe454b60c2630aedf', '', '0minirezo', 'oui', '2013-06-25 09:25:08', '', '$1$NyLJ3fWT$/dLdYXuA1peqZnFsr7ecm/', '2013-06-25 09:25:08', '117989252651c941bb996458.51533289', '99416821651c941bba640d5.60830976', 'a:5:{s:7:"couleur";i:1;s:7:"display";i:2;s:18:"display_navigation";s:22:"navigation_avec_icones";s:14:"display_outils";s:3:"oui";s:3:"cnx";s:0:"";}', NULL, 'spip', '');
%else
(1, 'admin', '', '%%system_mail_to', '', '', 'admin', '16cd931847f168d33ce3bd2400f7a5a90d3a37dab93f7ddfe454b60c2630aedf', '', '0minirezo', 'oui', '2013-06-25 09:25:08', '', '$1$NyLJ3fWT$/dLdYXuA1peqZnFsr7ecm/', '2013-06-25 09:25:08', '117989252651c941bb996458.51533289', '99416821651c941bba640d5.60830976', 'a:5:{s:7:"couleur";i:1;s:7:"display";i:2;s:18:"display_navigation";s:22:"navigation_avec_icones";s:14:"display_outils";s:3:"oui";s:3:"cnx";s:0:"";}', NULL, 'spip', '');
%end if
-- --------------------------------------------------------

--
-- Structure de la table `spip_auteurs_liens`
--

CREATE TABLE IF NOT EXISTS `spip_auteurs_liens` (
  `id_auteur` bigint(21) NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  `vu` varchar(6) NOT NULL DEFAULT 'non',
  PRIMARY KEY (`id_auteur`,`id_objet`,`objet`),
  KEY `id_auteur` (`id_auteur`),
  KEY `id_objet` (`id_objet`),
  KEY `objet` (`objet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_auteurs_liens`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_breves`
--

CREATE TABLE IF NOT EXISTS `spip_breves` (
  `id_breve` bigint(21) NOT NULL AUTO_INCREMENT,
  `date_heure` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` longtext NOT NULL,
  `lien_titre` text NOT NULL,
  `lien_url` text NOT NULL,
  `statut` varchar(6) NOT NULL DEFAULT '0',
  `id_rubrique` bigint(21) NOT NULL DEFAULT '0',
  `lang` varchar(10) NOT NULL DEFAULT '',
  `langue_choisie` varchar(3) DEFAULT 'non',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_breve`),
  KEY `id_rubrique` (`id_rubrique`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_breves`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_depots`
--

CREATE TABLE IF NOT EXISTS `spip_depots` (
  `id_depot` bigint(21) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT '',
  `url_serveur` varchar(255) NOT NULL DEFAULT '',
  `url_brouteur` varchar(255) NOT NULL DEFAULT '',
  `url_archives` varchar(255) NOT NULL DEFAULT '',
  `url_commits` varchar(255) NOT NULL DEFAULT '',
  `xml_paquets` varchar(255) NOT NULL DEFAULT '',
  `sha_paquets` varchar(40) NOT NULL DEFAULT '',
  `nbr_paquets` int(11) NOT NULL DEFAULT '0',
  `nbr_plugins` int(11) NOT NULL DEFAULT '0',
  `nbr_autres` int(11) NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_depot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_depots`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_depots_plugins`
--

CREATE TABLE IF NOT EXISTS `spip_depots_plugins` (
  `id_depot` bigint(21) NOT NULL,
  `id_plugin` bigint(21) NOT NULL,
  PRIMARY KEY (`id_depot`,`id_plugin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_depots_plugins`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_documents`
--

CREATE TABLE IF NOT EXISTS `spip_documents` (
  `id_document` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_vignette` bigint(21) NOT NULL DEFAULT '0',
  `extension` varchar(10) NOT NULL DEFAULT '',
  `titre` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descriptif` text NOT NULL,
  `fichier` text NOT NULL,
  `taille` bigint(20) DEFAULT NULL,
  `largeur` int(11) DEFAULT NULL,
  `hauteur` int(11) DEFAULT NULL,
  `media` varchar(10) NOT NULL DEFAULT 'file',
  `mode` varchar(10) NOT NULL DEFAULT 'document',
  `distant` varchar(3) DEFAULT 'non',
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `credits` varchar(255) NOT NULL DEFAULT '',
  `date_publication` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `brise` tinyint(4) DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_document`),
  KEY `id_vignette` (`id_vignette`),
  KEY `mode` (`mode`),
  KEY `extension` (`extension`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_documents`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_documents_liens`
--

CREATE TABLE IF NOT EXISTS `spip_documents_liens` (
  `id_document` bigint(21) NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  `vu` enum('non','oui') NOT NULL DEFAULT 'non',
  PRIMARY KEY (`id_document`,`id_objet`,`objet`),
  KEY `id_document` (`id_document`),
  KEY `id_objet` (`id_objet`),
  KEY `objet` (`objet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_documents_liens`
--

-- Structure de la table `spip_eva_habillage`
--

CREATE TABLE IF NOT EXISTS `spip_eva_habillage` (
  `id_habillage` int(11) NOT NULL AUTO_INCREMENT,
  `habillage` text NOT NULL,
  `sauvegarde` text NOT NULL,
  PRIMARY KEY (`id_habillage`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `spip_eva_habillage`
--

INSERT INTO `spip_eva_habillage` (`id_habillage`, `habillage`, `sauvegarde`) VALUES
(1, 'eva4_menu_gauche.css', 'Defaut');

-- --------------------------------------------------------


--
-- Structure de la table `spip_eva_habillage_images`
--

CREATE TABLE IF NOT EXISTS `spip_eva_habillage_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `nom_habillage` text NOT NULL,
  `nom_div` text NOT NULL,
  `nom_image` text NOT NULL,
  `pos_x` text NOT NULL,
  `pos_y` text NOT NULL,
  `repetition` text NOT NULL,
  `attach` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Contenu de la table `spip_eva_habillage_images`
--

INSERT INTO `spip_eva_habillage_images` (`id`, `type`, `nom_habillage`, `nom_div`, `nom_image`, `pos_x`, `pos_y`, `repetition`, `attach`) VALUES
(1, 'bloc', 'Defaut', 'sommaire_navigation', 'gauche', '1', '', '', 'sommaire'),
(2, 'bloc', 'Defaut', 'sommaire_menu_depliable', 'non', '1', '', '', 'sommaire'),
(3, 'bloc', 'Defaut', 'sommaire_edito', 'centre', '1', '', '', 'sommaire'),
(4, 'bloc', 'Defaut', 'sommaire_articles', 'centre', '2', '', '', 'sommaire'),
(5, 'bloc', 'Defaut', 'sommaire_ajax_articles', 'non', '2', '', '', 'sommaire'),
(6, 'bloc', 'Defaut', 'sommaire_mini_calendrier', 'gauche', '2', '', '', 'sommaire'),
(7, 'bloc', 'Defaut', 'sommaire_connexion', 'gauche', '3', '', '', 'sommaire'),
(8, 'bloc', 'Defaut', 'sommaire_breve', 'gauche', '4', '', '', 'sommaire'),
(9, 'bloc', 'Defaut', 'sommaire_site', 'gauche', '5', '', '', 'sommaire'),
(10, 'bloc', 'Defaut', 'sommaire_podcast', 'gauche', '6', '', '', 'sommaire'),
(11, 'bloc', 'Defaut', 'sommaire_logo', 'gauche', '7', '', '', 'sommaire'),
(12, 'bloc', 'Defaut', 'sommaire_syndic', 'gauche', '8', '', '', 'sommaire'),
(13, 'bloc', 'Defaut', 'sommaire_compteur', 'gauche', '9', '', '', 'sommaire'),
(14, 'bloc', 'Defaut', 'article_navigation', 'gauche', '1', '', '', 'article'),
(15, 'bloc', 'Defaut', 'article_menu_depliable', 'non', '1', '', '', 'article'),
(16, 'bloc', 'Defaut', 'article_contenu', 'centre', '1', '', '', 'article'),
(17, 'bloc', 'Defaut', 'article_forum', 'centre', '2', '', '', 'article'),
(18, 'bloc', 'Defaut', 'article_signature', 'centre', '3', '', '', 'article'),
(19, 'bloc', 'Defaut', 'article_petition', 'gauche', '2', '', '', 'article'),
(20, 'bloc', 'Defaut', 'article_meme_rubrique', 'gauche', '3', '', '', 'article'),
(21, 'bloc', 'Defaut', 'article_mot', 'gauche', '4', '', '', 'article'),
(22, 'bloc', 'Defaut', 'article_compteur', 'gauche', '5', '', '', 'article'),
(23, 'bloc', 'Defaut', 'auteur_navigation', 'gauche', '1', '', '', 'auteur'),
(24, 'bloc', 'Defaut', 'auteur_menu_depliable', 'non', '1', '', '', 'auteur'),
(25, 'bloc', 'Defaut', 'auteur_contenu', 'centre', '1', '', '', 'auteur'),
(26, 'bloc', 'Defaut', 'auteur_auteurs', 'gauche', '2', '', '', 'auteur'),
(27, 'bloc', 'Defaut', 'auteur_articles', 'gauche', '3', '', '', 'auteur'),
(28, 'bloc', 'Defaut', 'breve_navigation', 'gauche', '1', '', '', 'breve'),
(29, 'bloc', 'Defaut', 'breve_menu_depliable', 'non', '1', '', '', 'breve'),
(30, 'bloc', 'Defaut', 'breve_contenu', 'centre', '1', '', '', 'breve'),
(31, 'bloc', 'Defaut', 'breve_breves', 'gauche', '2', '', '', 'breve'),
(32, 'bloc', 'Defaut', 'rubrique_navigation', 'gauche', '1', '', '', 'rubrique'),
(33, 'bloc', 'Defaut', 'rubrique_menu_depliable', 'non', '1', '', '', 'rubrique'),
(34, 'bloc', 'Defaut', 'rubrique_contenu', 'centre', '1', '', '', 'rubrique'),
(35, 'bloc', 'Defaut', 'rubrique_sous_rubriques', 'centre', '2', '', '', 'rubrique'),
(36, 'bloc', 'Defaut', 'rubrique_articles', 'centre', '3', '', '', 'rubrique'),
(37, 'bloc', 'Defaut', 'rubrique_podcast', 'centre', '4', '', '', 'rubrique'),
(38, 'bloc', 'Defaut', 'rubrique_documents', 'centre', '5', '', '', 'rubrique'),
(39, 'bloc', 'Defaut', 'rubrique_breve', 'gauche', '2', '', '', 'rubrique'),
(40, 'bloc', 'Defaut', 'rubrique_site', 'gauche', '3', '', '', 'rubrique'),
(41, 'bloc', 'Defaut', 'rubrique_site_syndic', 'gauche', '4', '', '', 'rubrique'),
(42, 'bloc', 'Defaut', 'rubrique_mot', 'gauche', '5', '', '', 'rubrique'),
(43, 'bloc', 'Defaut', 'rubrique_syndic', 'gauche', '6', '', '', 'rubrique'),
(44, 'bloc', 'Defaut', 'site_navigation', 'gauche', '1', '', '', 'site'),
(45, 'bloc', 'Defaut', 'site_menu_depliable', 'non', '1', '', '', 'site'),
(46, 'bloc', 'Defaut', 'site_contenu', 'centre', '1', '', '', 'site'),
(47, 'bloc', 'Defaut', 'site_syndic', 'centre', '2', '', '', 'site'),
(48, 'bloc', 'Defaut', 'site_podcast', 'gauche', '2', '', '', 'site'),
(49, 'bloc', 'Defaut', 'site_sites', 'gauche', '3', '', '', 'site'),
(50, 'bloc', 'Defaut', 'entete_classique', 'oui', '1', '', '', 'entete'),
(51, 'bloc', 'Defaut', 'entete_arborescence', 'oui', '2', '', '', 'entete'),
(52, 'bloc', 'Defaut', 'entete_sans_liens', 'non', '1', '', '', 'entete'),
(53, 'bloc', 'Defaut', 'entete_sans_titre', 'non', '1', '', '', 'entete'),
(54, 'bloc', 'Defaut', 'pied_logos', 'oui', '1', '', '', 'pied'),
(55, 'bloc', 'Defaut', 'pied_classique', 'oui', '2', '', '', 'pied'),
(56, 'bloc', 'Defaut', 'headers_classiques', 'oui', '1', '', '', 'headers'),
(57, 'bloc', 'Defaut', 'headers_menu_dynamique', 'non', '2', '', '', 'headers');

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Structure de la table `spip_eva_habillage_themes`
--

CREATE TABLE IF NOT EXISTS `spip_eva_habillage_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `fond_ecran` text NOT NULL,
  `fond_page` text NOT NULL,
  `fond_bloc` text NOT NULL,
  `fond_edito` text NOT NULL,
  `fond_titre_edito` text NOT NULL,
  `fond_entete_pages` text NOT NULL,
  `fond_barres_entete` text NOT NULL,
  `fond_barres_entete_arborescence` text NOT NULL,
  `fond_barres_entete_auteur` text NOT NULL,
  `fond_barres_entete_texte` text NOT NULL,
  `fond_titre_article` text NOT NULL,
  `fond_pied_pages` text NOT NULL,
  `fond_pied_logos` text NOT NULL,
  `fond_titres` text NOT NULL,
  `fond_menu_fond` text NOT NULL,
  `fond_menu_off` text NOT NULL,
  `fond_menu_on` text NOT NULL,
  `fond_liste_elements_impairs` text NOT NULL,
  `fond_liste_elements_pairs` text NOT NULL,
  `texte_police_type` text NOT NULL,
  `texte_principal` text NOT NULL,
  `texte_liste_paire` text NOT NULL,
  `texte_liste_impaire` text NOT NULL,
  `texte_entetes` text NOT NULL,
  `texte_surtitre` text NOT NULL,
  `texte_titre` text NOT NULL,
  `texte_soustitre` text NOT NULL,
  `texte_couleur_liens_menu` text NOT NULL,
  `texte_couleur_liens_menu_actif` text NOT NULL,
  `texte_couleur_liens_menu_survol` text NOT NULL,
  `texte_barres_entete` text NOT NULL,
  `texte_premiere_lettre_entete` text NOT NULL,
  `texte_edito` text NOT NULL,
  `texte_edito_titre` text NOT NULL,
  `texte_edito_titre_premier` text NOT NULL,
  `texte_entete_arborescence` text NOT NULL,
  `texte_auteur` text NOT NULL,
  `texte_pied` text NOT NULL,
  `lien_couleur_lien` text NOT NULL,
  `lien_couleur_lien_survol` text NOT NULL,
  `lien_couleur_lien_impair` text NOT NULL,
  `lien_couleur_lien_impair_survol` text NOT NULL,
  `lien_couleur_lien_pair` text NOT NULL,
  `lien_couleur_lien_pair_survol` text NOT NULL,
  `bordure_page` text NOT NULL,
  `bordure_largeur_page` text NOT NULL,
  `bordure_style_page` text NOT NULL,
  `bordure_couleur_bordure` text NOT NULL,
  `bordure_largeur_couleur_bordure` text NOT NULL,
  `bordure_style_couleur_bordure` text NOT NULL,
  `bordure_couleur_bordure_entete` text NOT NULL,
  `bordure_largeur_couleur_bordure_entete` text NOT NULL,
  `bordure_style_couleur_bordure_entete` text NOT NULL,
  `bordure_couleur_titre` text NOT NULL,
  `bordure_largeur_titre` text NOT NULL,
  `bordure_style_titre` text NOT NULL,
  `bordure_couleur_bordure_auteur` text NOT NULL,
  `bordure_largeur_couleur_bordure_auteur` text NOT NULL,
  `bordure_style_couleur_bordure_auteur` text NOT NULL,
  `bordure_couleur_bordure_edito` text NOT NULL,
  `bordure_largeur_couleur_bordure_edito` text NOT NULL,
  `bordure_style_couleur_bordure_edito` text NOT NULL,
  `bordure_couleur_bordure_pied` text NOT NULL,
  `bordure_largeur_couleur_bordure_pied` text NOT NULL,
  `bordure_style_couleur_bordure_pied` text NOT NULL,
  `bordure_couleur_bordure_menu` text NOT NULL,
  `bordure_largeur_couleur_bordure_menu` text NOT NULL,
  `bordure_style_couleur_bordure_menu` text NOT NULL,
  `bordure_couleur_bordure_menu_secteurs` text NOT NULL,
  `bordure_largeur_couleur_bordure_menu_secteurs` text NOT NULL,
  `bordure_style_couleur_bordure_menu_secteurs` text NOT NULL,
  `taille_largeur_page` text NOT NULL,
  `taille_largeur_menu` text NOT NULL,
  `taille_largeur_menudroite` text NOT NULL,
  `taille_largeur_contenu` text NOT NULL,
  `taille_hauteur_entete` text NOT NULL,
  `admin_deplacement_horizontal_bouton_admin` text NOT NULL,
  `admin_deplacement_vertical_bouton_admin` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `spip_eva_habillage_themes`
--

INSERT INTO `spip_eva_habillage_themes` (`id`, `nom`, `fond_ecran`, `fond_page`, `fond_bloc`, `fond_edito`, `fond_titre_edito`, `fond_entete_pages`, `fond_barres_entete`, `fond_barres_entete_arborescence`, `fond_barres_entete_auteur`, `fond_barres_entete_texte`, `fond_titre_article`, `fond_pied_pages`, `fond_pied_logos`, `fond_titres`, `fond_menu_fond`, `fond_menu_off`, `fond_menu_on`, `fond_liste_elements_impairs`, `fond_liste_elements_pairs`, `texte_police_type`, `texte_principal`, `texte_liste_paire`, `texte_liste_impaire`, `texte_entetes`, `texte_surtitre`, `texte_titre`, `texte_soustitre`, `texte_couleur_liens_menu`, `texte_couleur_liens_menu_actif`, `texte_couleur_liens_menu_survol`, `texte_barres_entete`, `texte_premiere_lettre_entete`, `texte_edito`, `texte_edito_titre`, `texte_edito_titre_premier`, `texte_entete_arborescence`, `texte_auteur`, `texte_pied`, `lien_couleur_lien`, `lien_couleur_lien_survol`, `lien_couleur_lien_impair`, `lien_couleur_lien_impair_survol`, `lien_couleur_lien_pair`, `lien_couleur_lien_pair_survol`, `bordure_page`, `bordure_largeur_page`, `bordure_style_page`, `bordure_couleur_bordure`, `bordure_largeur_couleur_bordure`, `bordure_style_couleur_bordure`, `bordure_couleur_bordure_entete`, `bordure_largeur_couleur_bordure_entete`, `bordure_style_couleur_bordure_entete`, `bordure_couleur_titre`, `bordure_largeur_titre`, `bordure_style_titre`, `bordure_couleur_bordure_auteur`, `bordure_largeur_couleur_bordure_auteur`, `bordure_style_couleur_bordure_auteur`, `bordure_couleur_bordure_edito`, `bordure_largeur_couleur_bordure_edito`, `bordure_style_couleur_bordure_edito`, `bordure_couleur_bordure_pied`, `bordure_largeur_couleur_bordure_pied`, `bordure_style_couleur_bordure_pied`, `bordure_couleur_bordure_menu`, `bordure_largeur_couleur_bordure_menu`, `bordure_style_couleur_bordure_menu`, `bordure_couleur_bordure_menu_secteurs`, `bordure_largeur_couleur_bordure_menu_secteurs`, `bordure_style_couleur_bordure_menu_secteurs`, `taille_largeur_page`, `taille_largeur_menu`, `taille_largeur_menudroite`, `taille_largeur_contenu`, `taille_hauteur_entete`, `admin_deplacement_horizontal_bouton_admin`, `admin_deplacement_vertical_bouton_admin`) VALUES
(1, 'Defaut', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------


-- --------------------------------------------------------

--
-- Structure de la table `spip_forum`
--

CREATE TABLE IF NOT EXISTS `spip_forum` (
  `id_forum` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  `id_parent` bigint(21) NOT NULL DEFAULT '0',
  `id_thread` bigint(21) NOT NULL DEFAULT '0',
  `date_heure` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_thread` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titre` text NOT NULL,
  `texte` mediumtext NOT NULL,
  `auteur` text NOT NULL,
  `email_auteur` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `statut` varchar(8) NOT NULL DEFAULT '0',
  `ip` varchar(40) NOT NULL DEFAULT '',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_auteur` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_forum`),
  KEY `id_auteur` (`id_auteur`),
  KEY `id_parent` (`id_parent`),
  KEY `id_thread` (`id_thread`),
  KEY `optimal` (`statut`,`id_parent`,`id_objet`,`objet`,`date_heure`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_forum`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_groupes_mots`
--

CREATE TABLE IF NOT EXISTS `spip_groupes_mots` (
  `id_groupe` bigint(21) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longtext NOT NULL,
  `unseul` varchar(3) NOT NULL DEFAULT '',
  `obligatoire` varchar(3) NOT NULL DEFAULT '',
  `tables_liees` text NOT NULL,
  `minirezo` varchar(3) NOT NULL DEFAULT '',
  `comite` varchar(3) NOT NULL DEFAULT '',
  `forum` varchar(3) NOT NULL DEFAULT '',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_groupes_mots`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_jobs`
--

CREATE TABLE IF NOT EXISTS `spip_jobs` (
  `id_job` bigint(21) NOT NULL AUTO_INCREMENT,
  `descriptif` text NOT NULL,
  `fonction` varchar(255) NOT NULL,
  `args` longblob NOT NULL,
  `md5args` char(32) NOT NULL DEFAULT '',
  `inclure` varchar(255) NOT NULL,
  `priorite` smallint(6) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_job`),
  KEY `date` (`date`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `spip_jobs`
--

INSERT INTO `spip_jobs` (`id_job`, `descriptif`, `fonction`, `args`, `md5args`, `inclure`, `priorite`, `date`, `status`) VALUES
(9, 'Tâche CRON optimiser (toutes les 172800 s)', 'optimiser', 0x613a313a7b693a303b693a313337323132373630343b7d, 'f0622b205c8d2f6519ed4dc442e7e366', 'genie/', -1, '2013-06-27 04:33:24', 1),
(11, 'Tâche CRON maintenance (toutes les 7200 s)', 'maintenance', 0x613a313a7b693a303b693a313337323134343038373b7d, 'ecde31b21fc6948545347f12d955367f', 'genie/', 0, '2013-06-25 11:08:07', 1),
(12, 'Tâche CRON mise_a_jour (toutes les 259200 s)', 'mise_a_jour', 0x613a313a7b693a303b693a313337323134343130383b7d, 'fd3f16bff9b0c15299eb04575348f6b2', 'genie/', 0, '2013-06-28 09:08:28', 1),
(16, 'Tâche CRON invalideur (toutes les 600 s)', 'invalideur', 0x613a313a7b693a303b693a313337323134353034363b7d, '00587846ea5d2986aa6ea3522c2e3f69', 'genie/', 0, '2013-06-25 09:34:06', 1),
(14, 'Tâche CRON svp_actualiser_depots (toutes les 21600 s)', 'svp_actualiser_depots', 0x613a313a7b693a303b693a313337323134343139343b7d, 'dbc5debff00593e1ac5de82c51384fc9', 'genie/', 0, '2013-06-25 15:09:54', 1),
(15, 'Tâche CRON syndic (toutes les 90 s)', 'syndic', 0x613a313a7b693a303b693a313337323134353034353b7d, 'cee1502f3eaa111a93351390178070c1', 'genie/', 0, '2013-06-25 09:25:35', 1),
(8, 'Tâche CRON queue_watch (toutes les 86400 s)', 'queue_watch', 0x613a313a7b693a303b693a313337323134343038363b7d, '37101327fb5ede32605be16065fe5f72', 'genie/', 0, '2013-06-26 09:08:06', 1);

-- --------------------------------------------------------

--
-- Structure de la table `spip_jobs_liens`
--

CREATE TABLE IF NOT EXISTS `spip_jobs_liens` (
  `id_job` bigint(21) NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_job`,`id_objet`,`objet`),
  KEY `id_job` (`id_job`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_jobs_liens`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_messages`
--

CREATE TABLE IF NOT EXISTS `spip_messages` (
  `id_message` bigint(21) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `texte` longtext NOT NULL,
  `type` varchar(6) NOT NULL DEFAULT '',
  `date_heure` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_fin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rv` varchar(3) NOT NULL DEFAULT '',
  `statut` varchar(6) NOT NULL DEFAULT '0',
  `id_auteur` bigint(21) NOT NULL DEFAULT '0',
  `destinataires` text NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_message`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_messages`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_meta`
--

CREATE TABLE IF NOT EXISTS `spip_meta` (
  `nom` varchar(255) NOT NULL,
  `valeur` text,
  `impt` enum('non','oui') NOT NULL DEFAULT 'oui',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nom`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_meta`
--

INSERT INTO `spip_meta` (`nom`, `valeur`, `impt`, `maj`) VALUES
('charset_sql_base', 'utf8', 'non', '2013-06-25 09:06:38'),
('charset_collation_sql_base', 'utf8_general_ci', 'non', '2013-06-25 09:06:38'),
('charset_sql_connexion', 'utf8', 'non', '2013-06-25 09:06:38'),
('version_installee', '19268', 'non', '2013-06-25 09:06:38'),
('nouvelle_install', '1', 'non', '2013-06-25 09:06:38'),
('pcre_u', 'u', 'oui', '2013-06-25 09:07:33'),
('plugin', 'a:36:{s:4:"SPIP";a:5:{s:3:"nom";s:4:"SPIP";s:4:"etat";s:6:"stable";s:7:"version";s:6:"3.0.10";s:8:"dir_type";s:14:"_DIR_RESTREINT";s:3:"dir";s:0:"";}s:3:"CFG";a:5:{s:3:"nom";s:3:"CFG";s:4:"etat";s:6:"stable";s:7:"version";s:5:"3.0.0";s:3:"dir";s:3:"cfg";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:9:"COMPAGNON";a:5:{s:3:"nom";s:9:"Compagnon";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.4.1";s:3:"dir";s:9:"compagnon";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:14:"COUTEAU_SUISSE";a:5:{s:3:"nom";s:265:"<multi>[fr]Le Couteau Suisse[en]Swiss Knife[ca]El Ganivet Suís[nl]Het Zwitserland Mes[gl]A navalla suíza[es]La Navaja Suiza[de]Schweizer Taschenmesser[br]Ar Gontell Suis[pt_br]Canivete Suíço[ast]La Navaya Suiza[gl]A navalla suíza[it]Coltellino Svizzero</multi>";s:4:"etat";s:6:"stable";s:7:"version";s:7:"1.8.133";s:3:"dir";s:14:"couteau_suisse";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:4:"DUMP";a:5:{s:3:"nom";s:4:"Dump";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.6.7";s:3:"dir";s:4:"dump";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:7:"EOLECAS";a:5:{s:3:"nom";s:7:"EoleCas";s:4:"etat";s:6:"stable";s:7:"version";s:3:"1.0";s:3:"dir";s:7:"eolecas";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:6:"IMAGES";a:5:{s:3:"nom";s:6:"Images";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.1.5";s:3:"dir";s:14:"filtres_images";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:5:"FORUM";a:5:{s:3:"nom";s:5:"Forum";s:4:"etat";s:6:"stable";s:7:"version";s:6:"1.8.24";s:3:"dir";s:5:"forum";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:8:"JQUERYUI";a:5:{s:3:"nom";s:9:"jQuery UI";s:4:"etat";s:6:"stable";s:7:"version";s:6:"1.8.21";s:3:"dir";s:9:"jquery_ui";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:8:"MEDIABOX";a:5:{s:3:"nom";s:8:"MediaBox";s:4:"etat";s:6:"stable";s:7:"version";s:5:"0.8.4";s:3:"dir";s:8:"mediabox";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:6:"MEDIAS";a:5:{s:3:"nom";s:6:"Medias";s:4:"etat";s:6:"stable";s:7:"version";s:6:"2.7.45";s:3:"dir";s:6:"medias";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:4:"MOTS";a:5:{s:3:"nom";s:4:"Mots";s:4:"etat";s:6:"stable";s:7:"version";s:6:"2.4.10";s:3:"dir";s:4:"mots";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:11:"MSIE_COMPAT";a:5:{s:3:"nom";s:25:"Support vieux navigateurs";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.2.0";s:3:"dir";s:11:"msie_compat";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:10:"ORGANISEUR";a:5:{s:3:"nom";s:10:"Organiseur";s:4:"etat";s:6:"stable";s:7:"version";s:6:"0.8.10";s:3:"dir";s:10:"organiseur";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:9:"PETITIONS";a:5:{s:3:"nom";s:10:"Pétitions";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.4.3";s:3:"dir";s:9:"petitions";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:11:"PORTE_PLUME";a:5:{s:3:"nom";s:11:"Porte plume";s:4:"etat";s:6:"stable";s:7:"version";s:6:"1.12.2";s:3:"dir";s:11:"porte_plume";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:9:"REVISIONS";a:5:{s:3:"nom";s:10:"Révisions";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.7.5";s:3:"dir";s:9:"revisions";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:8:"SAFEHTML";a:5:{s:3:"nom";s:8:"SafeHTML";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.4.0";s:3:"dir";s:8:"safehtml";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:5:"SITES";a:5:{s:3:"nom";s:5:"Sites";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.7.8";s:3:"dir";s:5:"sites";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:23:"SQUELETTES_PAR_RUBRIQUE";a:5:{s:3:"nom";s:23:"Squelettes par Rubrique";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.1.1";s:3:"dir";s:23:"squelettes_par_rubrique";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:5:"STATS";a:5:{s:3:"nom";s:12:"Statistiques";s:4:"etat";s:6:"stable";s:7:"version";s:6:"0.4.15";s:3:"dir";s:12:"statistiques";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:3:"SVP";a:5:{s:3:"nom";s:3:"SVP";s:4:"etat";s:6:"stable";s:7:"version";s:7:"0.80.14";s:3:"dir";s:3:"svp";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:2:"TW";a:5:{s:3:"nom";s:19:"TextWheel pour SPIP";s:4:"etat";s:6:"stable";s:7:"version";s:6:"0.8.17";s:3:"dir";s:9:"textwheel";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:4:"URLS";a:5:{s:3:"nom";s:13:"Urls Etendues";s:4:"etat";s:6:"stable";s:7:"version";s:6:"1.4.14";s:3:"dir";s:13:"urls_etendues";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:9:"VERTEBRES";a:5:{s:3:"nom";s:10:"Vertèbres";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.2.2";s:3:"dir";s:9:"vertebres";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:11:"EVAMENTIONS";a:5:{s:3:"nom";s:29:"Gestion des mentions légales";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.0";s:3:"dir";s:31:"auto/eva_mentions_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:13:"EVASQUELETTES";a:5:{s:3:"nom";s:11:"EVA-WEB 4.2";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.4";s:3:"dir";s:33:"auto/eva_squelettes_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:14:"EVA_CALENDRIER";a:5:{s:3:"nom";s:32:"Rubrique calendrier pour EVA-web";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.1";s:3:"dir";s:33:"auto/eva_calendrier_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:13:"EVA_HABILLAGE";a:5:{s:3:"nom";s:39:"Gestion des habillages pour EVA-Web 4.2";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.6";s:3:"dir";s:32:"auto/eva_habillage_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:10:"EVA_AGENDA";a:5:{s:3:"nom";s:49:"Affichage d’une rubrique sous forme d’agenda.";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.1";s:3:"dir";s:29:"auto/eva_agenda_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:11:"EVA_INSTALL";a:5:{s:3:"nom";s:36:"Création des mots clés d’eva-web";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.3";s:3:"dir";s:30:"auto/eva_install_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:8:"EVABONUS";a:5:{s:3:"nom";s:26:"EVA-bonus pour EVA-web 4.2";s:4:"etat";s:6:"stable";s:7:"version";s:5:"4.2.5";s:3:"dir";s:28:"auto/eva_bonus_pour_spip_3_0";s:8:"dir_type";s:12:"_DIR_PLUGINS";}s:10:"ITERATEURS";a:5:{s:3:"nom";s:10:"iterateurs";s:7:"version";s:5:"0.6.1";s:4:"etat";s:1:"?";s:8:"dir_type";s:14:"_DIR_RESTREINT";s:3:"dir";s:0:"";}s:5:"QUEUE";a:5:{s:3:"nom";s:5:"queue";s:7:"version";s:5:"0.6.6";s:4:"etat";s:1:"?";s:8:"dir_type";s:14:"_DIR_RESTREINT";s:3:"dir";s:0:"";}s:6:"BREVES";a:5:{s:3:"nom";s:7:"Brèves";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.3.5";s:3:"dir";s:6:"breves";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}s:11:"COMPRESSEUR";a:5:{s:3:"nom";s:11:"Compresseur";s:4:"etat";s:6:"stable";s:7:"version";s:5:"1.8.6";s:3:"dir";s:11:"compresseur";s:8:"dir_type";s:17:"_DIR_PLUGINS_DIST";}}', 'non', '2013-07-09 10:32:01'),
('plugin_attente', 'a:0:{}', 'oui', '2013-06-25 09:07:34'),
('plugin_header', 'spip(3.0.10),cfg(3.0.0),compagnon(1.4.1),couteau_suisse(1.8.133),dump(1.6.7),eolecas(1.0),images(1.1.5),forum(1.8.24),jqueryui(1.8.21),mediabox(0.8.4),medias(2.7.45),mots(2.4.10),msie_compat(1.2.0),organiseur(0.8.10),petitions(1.4.3),porte_plume(1.12.2),revisions(1.7.5),safehtml(1.4.0),sites(1.7.8),squelettes_par_rubrique(1.1.1),stats(0.4.15),svp(0.80.14),tw(0.8.17),urls(1.4.14),vertebres(1.2.2),evamentions(4.2.0),evasquelettes(4.2.4),eva_calendrier(4.2.1),eva_habillage(4.2.6),eva_agenda(4.2.1),eva_install(4.2.3),evabonus(4.2.5),iterateurs(0.6.1),queue(0.6.6),breves(1.3.5),compresseur(1.8.6)', 'non', '2013-07-09 10:32:01'),
('charset', 'utf-8', 'oui', '2013-06-25 09:07:34'),
('langues_proposees', 'ar,ast,ay,bg,br,bs,ca,co,cpf,cpf_hat,cs,da,de,en,eo,es,eu,fa,fon,fr,gl,he,hu,id,it,it_fem,ja,km,lb,my,nl,oc_auv,oc_gsc,oc_lms,oc_lnc,oc_ni,oc_ni_la,oc_prv,oc_va,pl,pt,pt_br,ro,ru,sk,sv,tr,vi,zh', 'non', '2013-06-25 09:07:39'),
('langue_site', 'fr', 'non', '2013-06-25 09:07:39'),
('tweaks_actifs', 'a:29:{s:16:"supprimer_numero";a:1:{s:5:"actif";i:0;}s:11:"set_options";a:1:{s:5:"actif";i:0;}s:16:"previsualisation";a:2:{s:5:"actif";i:0;s:11:"maj_distant";i:1;}s:12:"suivi_forums";a:1:{s:5:"actif";i:0;}s:14:"boites_privees";a:1:{s:7:"contrib";i:2564;}s:7:"decoupe";a:1:{s:7:"contrib";i:2135;}s:8:"sommaire";a:1:{s:7:"contrib";i:2378;}s:10:"SPIP_liens";a:1:{s:7:"contrib";i:2443;}s:19:"visiteurs_connectes";a:1:{s:7:"contrib";i:3412;}s:10:"decoration";a:1:{s:7:"contrib";i:2427;}s:8:"couleurs";a:1:{s:7:"contrib";i:2427;}s:14:"typo_exposants";a:1:{s:7:"contrib";i:1564;}s:15:"liens_orphelins";a:1:{s:7:"contrib";i:2443;}s:10:"filets_sep";a:1:{s:7:"contrib";i:1563;}s:7:"smileys";a:1:{s:7:"contrib";i:1561;}s:9:"glossaire";a:1:{s:7:"contrib";i:2206;}s:9:"mailcrypt";a:1:{s:7:"contrib";i:2443;}s:14:"liens_en_clair";a:1:{s:7:"contrib";i:2443;}s:7:"jcorner";a:2:{s:7:"contrib";i:2987;s:11:"maj_distant";i:1;}s:12:"titre_parent";a:1:{s:7:"contrib";i:2900;}s:15:"trousse_balises";a:1:{s:7:"contrib";i:3005;}s:7:"horloge";a:1:{s:7:"contrib";i:2998;}s:8:"maj_auto";a:2:{s:7:"contrib";i:3223;s:11:"maj_distant";i:1;}s:17:"sessions_anonymes";a:1:{s:5:"actif";i:0;}s:5:"blocs";a:1:{s:7:"contrib";i:2583;}s:8:"devdebug";a:1:{s:7:"contrib";i:3572;}s:14:"ecran_securite";a:1:{s:11:"maj_distant";i:1;}s:7:"masquer";a:1:{s:11:"maj_distant";i:1;}s:10:"balise_set";a:1:{s:7:"contrib";i:4336;}}', 'oui', '2013-07-09 10:30:31'),
('tweaks_variables', 'a:2:{s:8:"_chaines";a:60:{i:0;s:18:"dossier_squelettes";i:1;s:10:"webmestres";i:2;s:18:"suite_introduction";i:3;s:18:"radio_set_options4";i:4;s:11:"mot_masquer";i:5;s:19:"radio_suivi_forums3";i:6;s:9:"spam_mots";i:7;s:8:"spam_ips";i:8;s:7:"dir_log";i:9;s:8:"file_log";i:10;s:15:"file_log_suffix";i:11;s:15:"message_travaux";i:12;s:9:"i_couleur";i:13;s:8:"i_police";i:14;s:22:"url_glossaire_externe2";i:15;s:17:"decoration_styles";i:16;s:14:"couleurs_perso";i:17;s:17:"glossaire_groupes";i:18;s:18:"fonds_demailcrypt2";i:19;s:15:"jcorner_classes";i:20;s:10:"insertions";i:21;s:8:"timezone";i:22;s:10:"spip_ecran";i:23;s:14:"alerte_message";i:24;s:12:"tri_articles";i:25;s:9:"tri_perso";i:26;s:11:"tri_groupes";i:27;s:17:"tri_perso_groupes";i:28;s:19:"autorisations_alias";i:29;s:7:"bloc_h4";i:30;s:11:"blocs_slide";i:31;s:7:"style_p";i:32;s:7:"style_h";i:33;s:7:"racc_hr";i:34;s:7:"racc_h1";i:35;s:7:"racc_h2";i:36;s:7:"racc_i1";i:37;s:7:"racc_i2";i:38;s:7:"racc_g1";i:39;s:7:"racc_g2";i:40;s:9:"ouvre_ref";i:41;s:9:"ferme_ref";i:42;s:10:"ouvre_note";i:43;s:10:"ferme_note";i:44;s:4:"puce";i:45;s:15:"devdebug_espace";i:46;s:15:"devdebug_niveau";i:47;s:16:"radio_type_urls3";i:48;s:11:"spip_script";i:49;s:21:"terminaison_urls_page";i:50;s:20:"separateur_urls_page";i:51;s:15:"url_arbo_sep_id";i:52;s:21:"terminaison_urls_arbo";i:53;s:24:"terminaison_urls_propres";i:54;s:18:"debut_urls_propres";i:55;s:19:"debut_urls_propres2";i:56;s:23:"terminaison_urls_libres";i:57;s:17:"debut_urls_libres";i:58;s:27:"terminaison_urls_propres_qs";i:59;s:18:"urls_id_sauf_liste";}s:8:"_nombres";a:123:{i:0;s:6:"alinea";i:1;s:7:"alinea2";i:2;s:11:"paragrapher";i:3;s:16:"lgr_introduction";i:4;s:17:"lien_introduction";i:5;s:25:"radio_filtrer_javascript3";i:6;s:13:"forum_lgrmaxi";i:7;s:9:"logo_Hmax";i:8;s:9:"logo_Wmax";i:9;s:9:"logo_Smax";i:10;s:8:"img_Hmax";i:11;s:8:"img_Wmax";i:12;s:8:"img_Smax";i:13;s:8:"doc_Smax";i:14;s:9:"img_GDmax";i:15;s:10:"img_GDqual";i:16;s:10:"copie_Smax";i:17;s:16:"auteur_forum_nom";i:18;s:18:"auteur_forum_email";i:19;s:17:"auteur_forum_deux";i:20;s:11:"ecran_actif";i:21;s:10:"ecran_load";i:22;s:18:"log_couteau_suisse";i:23;s:15:"spip_options_on";i:24;s:11:"distant_off";i:25;s:18:"distant_outils_off";i:26;s:7:"log_non";i:27;s:14:"filtre_gravite";i:28;s:20:"filtre_gravite_trace";i:29;s:8:"log_brut";i:30;s:7:"max_log";i:31;s:12:"log_fileline";i:32;s:15:"taille_des_logs";i:33;s:14:"nombre_de_logs";i:34;s:13:"prive_travaux";i:35;s:13:"admin_travaux";i:36;s:15:"avertir_travaux";i:37;s:13:"titre_travaux";i:38;s:13:"cache_travaux";i:39;s:14:"bp_tri_auteurs";i:40;s:15:"bp_urls_propres";i:41;s:6:"cs_rss";i:42;s:11:"format_spip";i:43;s:12:"stat_auteurs";i:44;s:14:"qui_webmasters";i:45;s:16:"max_auteurs_page";i:46;s:9:"auteurs_0";i:47;s:9:"auteurs_1";i:48;s:9:"auteurs_5";i:49;s:9:"auteurs_6";i:50;s:9:"auteurs_n";i:51;s:17:"auteurs_tout_voir";i:52;s:14:"balise_decoupe";i:53;s:13:"prof_sommaire";i:54;s:12:"lgr_sommaire";i:55;s:13:"auto_sommaire";i:56;s:13:"jolies_ancres";i:57;s:15:"balise_sommaire";i:58;s:9:"i_padding";i:59;s:9:"i_hauteur";i:60;s:9:"i_largeur";i:61;s:8:"i_taille";i:62;s:19:"radio_target_blank3";i:63;s:15:"enveloppe_mails";i:64;s:8:"tout_rub";i:65;s:8:"tout_aut";i:66;s:8:"puceSPIP";i:67;s:14:"couleurs_fonds";i:68;s:12:"set_couleurs";i:69;s:11:"expo_bofbof";i:70;s:19:"liens_interrogation";i:71;s:15:"liens_orphelins";i:72;s:8:"long_url";i:73;s:9:"coupe_url";i:74;s:16:"glossaire_limite";i:75;s:12:"glossaire_js";i:76;s:14:"glossaire_abbr";i:77;s:12:"balise_email";i:78;s:17:"fonds_demailcrypt";i:79;s:8:"scrollTo";i:80;s:11:"LocalScroll";i:81;s:14:"jcorner_plugin";i:82;s:16:"moderation_admin";i:83;s:16:"moderation_redac";i:84;s:16:"moderation_visit";i:85;s:14:"titres_etendus";i:86;s:18:"arret_optimisation";i:87;s:17:"rubrique_brouteur";i:88;s:17:"select_mots_clefs";i:89;s:18:"select_min_auteurs";i:90;s:18:"select_max_auteurs";i:91;s:10:"tres_large";i:92;s:13:"meme_rubrique";i:93;s:19:"autorisations_debug";i:94;s:11:"bloc_unique";i:95;s:12:"blocs_cookie";i:96;s:14:"blocs_millisec";i:97;s:13:"devdebug_mode";i:98;s:22:"radio_desactive_cache3";i:99;s:22:"radio_desactive_cache4";i:100;s:11:"duree_cache";i:101;s:16:"duree_cache_mutu";i:102;s:11:"quota_cache";i:103;s:23:"derniere_modif_invalide";i:104;s:13:"compacte_tout";i:105;s:14:"compacte_prive";i:106;s:12:"compacte_css";i:107;s:11:"compacte_js";i:108;s:19:"url_arbo_minuscules";i:109;s:19:"urls_arbo_sans_type";i:110;s:12:"url_max_arbo";i:111;s:15:"url_max_propres";i:112;s:22:"marqueurs_urls_propres";i:113;s:16:"url_max_propres2";i:114;s:23:"marqueurs_urls_propres2";i:115;s:14:"url_max_libres";i:116;s:18:"url_max_propres_qs";i:117;s:25:"marqueurs_urls_propres_qs";i:118;s:15:"urls_minuscules";i:119;s:12:"urls_avec_id";i:120;s:13:"urls_avec_id2";i:121;s:18:"urls_id_3_chiffres";i:122;s:22:"urls_id_sauf_rubriques";}}', 'oui', '2013-07-09 10:30:31'),
('tweaks_pipelines', 'a:4:{s:15:"header_js_prive";s:352:"<script type="text/javascript"><!--\nvar cs_prive=window.location.pathname.match(/\\/ecrire\\/$/)!=null;\njQuery.fn.cs_todo=function(){return this.not(''.cs_done'').addClass(''cs_done'');};\nif(window.jQuery) {\nvar cs_sel_jQuery=typeof jQuery(document).selector==''undefined''?''@'':'''';\nvar cs_CookiePlugin="../prive/javascript/jquery.cookie.js";\n}\n// --></script>\n";s:9:"header_js";s:349:"<script type="text/javascript"><!--\nvar cs_prive=window.location.pathname.match(/\\/ecrire\\/$/)!=null;\njQuery.fn.cs_todo=function(){return this.not(''.cs_done'').addClass(''cs_done'');};\nif(window.jQuery) {\nvar cs_sel_jQuery=typeof jQuery(document).selector==''undefined''?''@'':'''';\nvar cs_CookiePlugin="prive/javascript/jquery.cookie.js";\n}\n// --></script>\n";s:21:"pre_description_outil";s:3662:"\n# Copie du code utilise en eval() pour le pipeline ''pre_description_outil($flux)''\ninclude_spip(''outils/ecran_securite'');\ninclude_spip(''outils/sommaire'');\ninclude_spip(''outils/couleurs'');\ninclude_spip(''outils/sessions_anonymes'');\nif($id=="autobr"){if(defined("_SPIP30000")) $texte=str_replace("@BALISES@",cs_balises_traitees("autobr"),$texte);\n                $texte=str_replace(array("@ARTICLES@","@RUBRIQUES@","@FORUMS@"),\n                      array(cs_raccourcis_presents(array("article/texte", "article/descriptif", "article/chapo"),"%<alinea>%"), cs_raccourcis_presents(array("rubrique/texte"),"%<alinea>%"), cs_raccourcis_presents(array("forum/texte"),"%<alinea>%")), $texte);\n}\nif($id=="webmestres")\n                $texte=str_replace(array("@_CS_LISTE_WEBMESTRES@","@_CS_LISTE_ADMINS@"),get_liste_administrateurs(),$texte);\nif($id=="masquer"){include_spip("lib/masquer/distant_pipelines_masquer_pipelines");\n     $texte=str_replace(array("@_RUB@","@_ART@"),\n          array((function_exists("masquer_liste_rubriques") && $x=masquer_liste_rubriques(true))?"[->rub".join("], [->rub", $x)."]":couteauprive_T("variable_vide"),\n                    (function_exists("masquer_liste_articles") && $x=masquer_liste_articles(true))?"[->art".join("], [->art", $x)."]":couteauprive_T("variable_vide"))\n    ,$texte); }\nif($id=="auteur_forum") $texte=str_replace(array("@_CS_FORUM_NOM@","@_CS_FORUM_EMAIL@"),\n array(preg_replace('',:$,'',"",_T("forum:forum_votre_nom")),preg_replace('',:$,'',"",_T("forum:forum_votre_email"))),$texte);\nif($id=="ecran_securite") $flux["non"] = !1 || !$flux["actif"];\nif($id=="cs_comportement"){$tmp=(!0||!$flux["actif"]||defined("_CS_SPIP_OPTIONS_OK"))?"":"<spanred>"._T("couteauprive:cs_spip_options_erreur")."</span>";\n$texte=str_replace(\n        array("@_CS_FILE_OPTIONS_ERR@","@_CS_DIR_TMP@","@_CS_DIR_LOG@","@_CS_FILE_OPTIONS@"),\n array($tmp,cs_root_canonicalize(_DIR_TMP),cs_root_canonicalize(defined("_DIR_LOG")?_DIR_LOG:_DIR_TMP),show_file_options()),\n   $texte);\n}\nif($id=="spip_log")\n      $texte=str_replace(array("@DIR_LOG@","@SPIP_OPTIONS@"),\n       array("<code>".cs_root_canonicalize(_DIR_LOG)."</code>",!defined("_CS_SPIP_OPTIONS_OK")?"<br/>"._T("couteauprive:detail_spip_options2"):""),$texte);\nif($id=="test_i18n") $texte.=_T("Lorem_ipsum_dolor");\nif($id=="en_travaux") $texte=str_replace(array("@_CS_TRAVAUX_TITRE@","@_CS_NOM_SITE@"),\n  array("["._T("info_travaux_titre")."]","[".$GLOBALS["meta"]["nom_site"]."]"),$texte);\nif($id=="titres_typo")\n         $texte=str_replace("@_CS_FONTS@",join(" - ",get_liste_fonts()),$texte);\nif($id=="visiteurs_connectes") if($GLOBALS["meta"]["activer_statistiques"]!="oui")\n           $texte.="\\n\\n<spanred>"._T("couteauprive:visiteurs_connectes:inactif")."</span>";\nif($id=="timezone")\n              $texte=str_replace("@_CS_TZ@","<b>".(!(function_exists("date_default_timezone_get") && date_default_timezone_get())?"<span style=\\"color: red;\\">??</span>":@date_default_timezone_get())."</b> (PHP ".phpversion().")",$texte);\nif($id=="autorisations")\n$texte=str_replace(array("@_CS_DIR_TMP@","@_CS_DIR_LOG@"), array(cs_root_canonicalize(_DIR_CS_TMP), cs_root_canonicalize(defined("_DIR_LOG")?_DIR_LOG:_DIR_TMP)), $texte);\nfunction_exists(''ecran_securite_pre_description_outil'')?$flux=ecran_securite_pre_description_outil($flux):cs_deferr(''ecran_securite_pre_description_outil'');\nfunction_exists(''sommaire_description_outil'')?$flux=sommaire_description_outil($flux):cs_deferr(''sommaire_description_outil'');\nfunction_exists(''couleurs_pre_description_outil'')?$flux=couleurs_pre_description_outil($flux):cs_deferr(''couleurs_pre_description_outil'');\nfunction_exists(''sessions_anonymes_pre_description_outil'')?$flux=sessions_anonymes_pre_description_outil($flux):cs_deferr(''sessions_anonymes_pre_description_outil'');\n";s:15:"fichier_distant";s:707:"\n# Copie du code utilise en eval() pour le pipeline ''fichier_distant($flux)''\ninclude_spip(''outils/ecran_securite'');\n// rajeunissement pour SPIP3 (2e appel du pipeline)\nif($flux["outil"]=="maj_auto" && isset($flux["texte"]) && strpos($flux["fichier_distant"],"action/charger_plugin.php")!==false)\n    $flux["texte"] = str_replace(array("''icon''","include_spip(''inc/install'');"), array("''logo''", "if(_request(''cs_retour'')) return array(''nom''=>\\$retour, ''suite''=>\\$suite, ''fichier''=>\\$fichier, ''tmp''=>\\$status[''tmpname'']);\\n\\tinclude_spip(''inc/install'');"), $flux["texte"]);\nfunction_exists(''ecran_securite_fichier_distant'')?$flux=ecran_securite_fichier_distant($flux):cs_deferr(''ecran_securite_fichier_distant'');\n";}', 'oui', '2013-07-09 10:31:07'),
('alea_ephemere_ancien', '33069cd7874564ba2576790abed5647c', 'non', '2013-07-09 10:31:07'),
('alea_ephemere', '075c1202e1e589ab38ed1b1985b1fb94', 'non', '2013-07-09 10:31:07'),
('alea_ephemere_date', '1373358667', 'non', '2013-07-09 10:31:07'),
('email_webmaster', 'admin@%%web_url', 'oui', '2013-06-25 09:07:39'),
('nom_site', '%to_sql(%%libelle_etab)', 'oui', '2013-06-25 09:07:39'),
('adresse_site', 'https://%%web_url/spip', 'non', '2013-06-25 09:07:40'),
('descriptif_site', '', 'oui', '2013-06-25 09:07:39'),
('activer_logos', 'oui', 'oui', '2013-06-25 09:07:39'),
('activer_logos_survol', 'non', 'oui', '2013-06-25 09:07:39'),
('articles_surtitre', 'non', 'oui', '2013-06-25 09:07:39'),
('articles_soustitre', 'non', 'oui', '2013-06-25 09:07:39'),
('articles_descriptif', 'non', 'oui', '2013-06-25 09:07:39'),
('articles_chapeau', 'non', 'oui', '2013-06-25 09:07:39'),
('articles_texte', 'oui', 'oui', '2013-06-25 09:07:40'),
('articles_ps', 'non', 'oui', '2013-06-25 09:07:40'),
('articles_redac', 'non', 'oui', '2013-06-25 09:07:40'),
('post_dates', 'non', 'oui', '2013-06-25 09:07:40'),
('articles_urlref', 'non', 'oui', '2013-06-25 09:07:40'),
('articles_redirection', 'non', 'oui', '2013-06-25 09:07:40'),
('creer_preview', 'non', 'non', '2013-06-25 09:07:40'),
('taille_preview', '150', 'non', '2013-06-25 09:07:40'),
('articles_modif', 'non', 'oui', '2013-06-25 09:07:40'),
('rubriques_descriptif', 'non', 'oui', '2013-06-25 09:07:40'),
('rubriques_texte', 'oui', 'oui', '2013-06-25 09:07:40'),
('accepter_inscriptions', 'non', 'oui', '2013-06-25 09:07:40'),
('accepter_visiteurs', 'non', 'oui', '2013-06-25 09:07:40'),
('prevenir_auteurs', 'non', 'oui', '2013-06-25 09:07:40'),
('suivi_edito', 'non', 'oui', '2013-06-25 09:07:40'),
('adresse_suivi', '', 'oui', '2013-06-25 09:07:40'),
('adresse_suivi_inscription', '', 'oui', '2013-06-25 09:07:40'),
('adresse_neuf', '', 'oui', '2013-06-25 09:07:40'),
('jours_neuf', '', 'oui', '2013-06-25 09:07:40'),
('quoi_de_neuf', 'non', 'oui', '2013-06-25 09:07:40'),
('preview', ',0minirezo,1comite,', 'oui', '2013-06-25 09:07:40'),
('syndication_integrale', 'oui', 'oui', '2013-06-25 09:07:40'),
('dir_img', 'IMG/', 'oui', '2013-06-25 09:07:40'),
('multi_rubriques', 'non', 'oui', '2013-06-25 09:07:40'),
('multi_secteurs', 'non', 'oui', '2013-06-25 09:07:40'),
('gerer_trad', 'non', 'oui', '2013-06-25 09:07:40'),
('langues_multilingue', '', 'oui', '2013-06-25 09:07:40'),
('version_html_max', 'html4', 'oui', '2013-06-25 09:07:40'),
('type_urls', 'page', 'oui', '2013-06-25 09:07:40'),
('email_envoi', '', 'oui', '2013-06-25 09:07:40'),
('auto_compress_http', 'non', 'oui', '2013-06-25 09:07:40'),
('mots_cles_forums', 'non', 'oui', '2013-06-25 09:07:40'),
('forums_titre', 'oui', 'oui', '2013-06-25 09:07:40'),
('forums_texte', 'oui', 'oui', '2013-06-25 09:07:40'),
('forums_urlref', 'non', 'oui', '2013-06-25 09:07:40'),
('forums_afficher_barre', 'oui', 'oui', '2013-06-25 09:07:40'),
('formats_documents_forum', '', 'oui', '2013-06-25 09:07:40'),
('forums_publics', 'posteriori', 'oui', '2013-06-25 09:07:40'),
('forum_prive', 'oui', 'oui', '2013-06-25 09:07:40'),
('forum_prive_objets', 'oui', 'oui', '2013-06-25 09:07:40'),
('forum_prive_admin', 'non', 'oui', '2013-06-25 09:07:40'),
('articles_mots', 'non', 'oui', '2013-06-25 09:07:40'),
('config_precise_groupes', 'non', 'oui', '2013-06-25 09:07:40'),
('iecompat', 'non', 'oui', '2013-06-25 09:07:40'),
('messagerie_agenda', 'oui', 'oui', '2013-06-25 09:07:40'),
('barre_outils_public', 'oui', 'oui', '2013-06-25 09:07:40'),
('objets_versions', 'a:0:{}', 'oui', '2013-06-25 09:07:42'),
('activer_sites', 'non', 'oui', '2013-06-25 09:07:40'),
('proposer_sites', '0', 'oui', '2013-06-25 09:07:40'),
('activer_syndic', 'oui', 'oui', '2013-06-25 09:07:40'),
('moderation_sites', 'non', 'oui', '2013-06-25 09:07:40'),
('activer_statistiques', 'non', 'oui', '2013-06-25 09:07:40'),
('activer_captures_referers', 'non', 'oui', '2013-06-25 09:07:40'),
('activer_breves', 'non', 'oui', '2013-06-25 09:07:40'),
('auto_compress_js', 'non', 'oui', '2013-06-25 09:07:40'),
('auto_compress_closure', 'non', 'oui', '2013-06-25 09:07:40'),
('auto_compress_css', 'non', 'oui', '2013-06-25 09:07:40'),
('langues_utilisees', 'fr', 'oui', '2013-06-25 09:07:40'),
('compagnon', 'a:2:{s:6:"config";a:1:{s:7:"activer";s:3:"oui";}i:1;a:3:{s:7:"accueil";i:1;s:18:"accueil_configurer";i:1;s:19:"accueil_publication";i:1;}}', 'oui', '2013-07-09 10:31:19'),
('compagnon_base_version', '1.0.0', 'oui', '2013-06-25 09:07:41'),
('couteau_suisse_base_version', '1.11', 'oui', '2013-07-09 10:31:23'),
('forum_base_version', '1.2.1', 'oui', '2013-06-25 09:07:41'),
('medias_base_version', '1.2.4', 'oui', '2013-06-25 09:07:41'),
('mots_base_version', '2.1.1', 'oui', '2013-06-25 09:07:41'),
('organiseur_base_version', '1.1.1', 'oui', '2013-06-25 09:07:41'),
('petitions_base_version', '1.1.6', 'oui', '2013-06-25 09:07:42'),
('revisions_base_version', '1.1.4', 'oui', '2013-06-25 09:07:42'),
('sites_base_version', '1.0.0', 'oui', '2013-06-25 09:07:42'),
('stats_base_version', '1.0.0', 'oui', '2013-06-25 09:07:42'),
('svp_base_version', '0.4.0', 'oui', '2013-06-25 09:07:42'),
('urls_base_version', '1.1.3', 'oui', '2013-06-25 09:07:42'),
('breves_base_version', '1.0.0', 'oui', '2013-06-25 09:07:42'),
('plugin_installes', 'a:14:{i:0;s:9:"compagnon";i:1;s:14:"couteau_suisse";i:2;s:5:"forum";i:3;s:6:"medias";i:4;s:4:"mots";i:5;s:10:"organiseur";i:6;s:9:"petitions";i:7;s:9:"revisions";i:8;s:5:"sites";i:9;s:12:"statistiques";i:10;s:3:"svp";i:11;s:13:"urls_etendues";i:12;s:32:"auto/eva_habillage_pour_spip_3_0";i:13;s:6:"breves";}', 'non', '2013-07-09 10:32:03'),
('secret_du_site', '8f25a090d9eeeb3247543adb91b3a33ae8726250f3e857a5ba3a67ff15e35f03', 'oui', '2013-06-25 09:07:59'),
('optimiser_table', '1', 'oui', '2013-06-25 09:08:06'),
('tour_quota_cache', '2', 'oui', '2013-06-25 09:24:06'),
('plugins_interessants', 'a:33:{s:31:"auto/eva_mentions_pour_spip_3_0";i:23;s:9:"textwheel";i:30;s:13:"urls_etendues";i:30;s:3:"svp";i:30;s:12:"statistiques";i:30;s:23:"squelettes_par_rubrique";i:30;s:5:"sites";i:30;s:8:"safehtml";i:30;s:9:"revisions";i:30;s:11:"porte_plume";i:30;s:9:"petitions";i:30;s:10:"organiseur";i:30;s:6:"medias";i:30;s:4:"mots";i:30;s:11:"msie_compat";i:30;s:8:"mediabox";i:30;s:4:"dump";i:30;s:7:"eolecas";i:30;s:14:"filtres_images";i:30;s:5:"forum";i:30;s:9:"jquery_ui";i:30;s:14:"couteau_suisse";i:30;s:9:"compagnon";i:30;s:11:"compresseur";i:30;s:3:"cfg";i:30;s:6:"breves";i:30;s:9:"vertebres";i:30;s:33:"auto/eva_squelettes_pour_spip_3_0";i:24;s:33:"auto/eva_calendrier_pour_spip_3_0";i:25;s:32:"auto/eva_habillage_pour_spip_3_0";i:26;s:29:"auto/eva_agenda_pour_spip_3_0";i:27;s:30:"auto/eva_install_pour_spip_3_0";i:28;s:28:"auto/eva_bonus_pour_spip_3_0";i:29;}', 'oui', '2013-07-09 10:32:01'),
('eva_habillage_base_version', '0.6', 'oui', '2013-07-09 10:32:31'),
('info_maj_spip', '', 'oui', '2013-06-25 09:08:28');

%if %%is_defined('eolesso_adresse') and %%is_defined('eolesso_port')
%set %%longueurdom=len(%%eolesso_adresse)
%set %%longueurport=len(str(%%eolesso_port))
INSERT INTO `spip_meta` (`nom`, `valeur`, `impt`)
VALUES ('eolecas', 'a:5:{s:7:"eolecas";s:3:"oui";s:10:"eolecasuid";s:5:"login";s:16:"eolecasurldefaut";s:%%longueurdom:"%%eolesso_adresse";s:17:"eolecasrepertoire";s:1:"/";s:11:"eolecasport";s:%%longueurport:"%%eolesso_port";}', 'oui');
%else
INSERT INTO `spip_meta` (`nom`, `valeur`, `impt`) VALUES('eolecas', 'a:5:{s:7:"eolecas";s:3:"oui";s:10:"eolecasuid";s:5:"login";s:16:"eolecasurldefaut";s:15:"%%adresse_ip_web";s:17:"eolecasrepertoire";s:1:"/";s:11:"eolecasport";s:4:"8443";}', 'oui');
%end if

-- --------------------------------------------------------

--
-- Structure de la table `spip_mots`
--

CREATE TABLE IF NOT EXISTS `spip_mots` (
  `id_mot` bigint(21) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longtext NOT NULL,
  `id_groupe` bigint(21) NOT NULL DEFAULT '0',
  `type` text NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mot`),
  KEY `id_groupe` (`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_mots`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_mots_liens`
--

CREATE TABLE IF NOT EXISTS `spip_mots_liens` (
  `id_mot` bigint(21) NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_mot`,`id_objet`,`objet`),
  KEY `id_mot` (`id_mot`),
  KEY `id_objet` (`id_objet`),
  KEY `objet` (`objet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_mots_liens`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_paquets`
--

CREATE TABLE IF NOT EXISTS `spip_paquets` (
  `id_paquet` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_plugin` bigint(21) NOT NULL,
  `prefixe` varchar(30) NOT NULL DEFAULT '',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(24) NOT NULL DEFAULT '',
  `version_base` varchar(24) NOT NULL DEFAULT '',
  `compatibilite_spip` varchar(24) NOT NULL DEFAULT '',
  `branches_spip` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `auteur` text NOT NULL,
  `credit` text NOT NULL,
  `licence` text NOT NULL,
  `copyright` text NOT NULL,
  `lien_doc` text NOT NULL,
  `lien_demo` text NOT NULL,
  `lien_dev` text NOT NULL,
  `etat` varchar(16) NOT NULL DEFAULT '',
  `etatnum` int(1) NOT NULL DEFAULT '0',
  `dependances` text NOT NULL,
  `date_crea` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modif` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_depot` bigint(21) NOT NULL DEFAULT '0',
  `nom_archive` varchar(255) NOT NULL DEFAULT '',
  `nbo_archive` int(11) NOT NULL DEFAULT '0',
  `maj_archive` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `src_archive` varchar(255) NOT NULL DEFAULT '',
  `traductions` text NOT NULL,
  `actif` varchar(3) NOT NULL DEFAULT 'non',
  `installe` varchar(3) NOT NULL DEFAULT 'non',
  `recent` int(2) NOT NULL DEFAULT '0',
  `maj_version` varchar(255) NOT NULL DEFAULT '',
  `superieur` varchar(3) NOT NULL DEFAULT 'non',
  `obsolete` varchar(3) NOT NULL DEFAULT 'non',
  `attente` varchar(3) NOT NULL DEFAULT 'non',
  `constante` varchar(30) NOT NULL DEFAULT '',
  `signature` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_paquet`),
  KEY `id_plugin` (`id_plugin`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Contenu de la table `spip_paquets`
--

INSERT INTO `spip_paquets` (`id_paquet`, `id_plugin`, `prefixe`, `logo`, `version`, `version_base`, `compatibilite_spip`, `branches_spip`, `description`, `auteur`, `credit`, `licence`, `copyright`, `lien_doc`, `lien_demo`, `lien_dev`, `etat`, `etatnum`, `dependances`, `date_crea`, `date_modif`, `id_depot`, `nom_archive`, `nbo_archive`, `maj_archive`, `src_archive`, `traductions`, `actif`, `installe`, `recent`, `maj_version`, `superieur`, `obsolete`, `attente`, `constante`, `signature`) VALUES
(46, 46, 'SPIP_BONUX', 'img_pack/spip-bonux.png', '003.000.005', '', '[3.0.0-dev;3.1.*]', '3.0,3.1', '<multi>[ar]الملحق الذي يجعل SPIP أكثر بياضاً من SPIP، ويقدم هدايا ممتعة!\nجداول، عدادات، الخ.[en]The plugin that make SPIP better than SPIP, with nice gifts inside !\n	arrays, counter, etc.[es]El plugin que lava más SPIP que SPIP, ¡con bonitos regalos dentro!\n	Tableros, contador, etcétera.[fr]Le plugin qui lave plus SPIP que SPIP, avec des chouettes cadeaux dedans !\n	Tableaux, compteur, etc.[ru]SPIP Bonux необходим для работы многих других плагинов. Он существенно расширяет возможности SPIP, делая его лучше чем SPIP :)))[sk]Zásuvný modul, ktorý prečistí SPIP ako SPIP s peknými darčekmi vnútri!\n	Tabuľky, počítadlo, a i.</multi>', 'a:3:{i:1;a:3:{s:3:"nom";s:12:"Cedric Morin";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:2;a:3:{s:3:"nom";s:19:"Matthieu Marcillaud";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:3;a:3:{s:3:"nom";s:14:"romy.tetue.net";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', 'a:1:{i:1;s:9:"2008-2013";}', 'http://www.spip-contrib.net/SPIP-Bonux', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/spip-bonux-3', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '874c72b154469a6de4530e452ea6a5ef'),
(45, 45, 'PLAYER', 'prive/themes/spip/images/player-32.png', '002.001.002', '0.2.4', '[3.0.0;3.0.*]', '3.0', '<multi>[de]Dieses Plugin erlaubt, Töne und Videos auszugeben. Es stellt Flash-Player für das .mp3 Format bereit.\nDie Player werden für alle <code><docXX|player></code> Tags in Artikeltexten und SKeletten eingesetzt.\n\n-* In einen Text fügt der Code  <code><docXX|player></code> einen von mehreren Flash-Audioplayern ein.\n-* In ein Skelett fügt der Code  <code>#MODELE{playliste}</code> eine Abspielliste der letzten MP3-Datein ein.[en]This plugin allows to read and display sounds and videos.\nIt adds flash player suitable the formats .mp3.\nIt works on all <code><docXX|player></code> inserted in the texts, as well as in the templates.\n\n-* In a text, <code><docXX|player></code>, displays a flash audio player (several players to choose from);\n-* In a template <code>#MODELE{playliste}</code> displays a playlist of the latest mp3[fa]اين پلاگين به شما اجازه مي‌دهد صدا و ويدئو را بخوانيد و نمايش دهيد. \nهمچنين فلاش پلي‌ير مناسب فرمت ام.پي.3 (.mp3) را اضافه مي‌كند. \n\nاين پلاگين روي تمام <code><docXX|player></code> كه در متن‌ها و نيز در تمپلست‌ها گنجاده شده باشد، كار مي‌كند \n-* در متن،  <code><docXX|player></code>,  يك فلاش اوديو پلي‌ير (چند پلي‌ير براي انتخاب از ميان آن‌ها) را نشان مي‌‌دهد\n-* در يك تمپليت، <code>#MODELE{playliste}</code> يك فهرست پخش  از آخرين ام‌.پي.3 را نشان مي‌دهد[fr]Ce plugin permet la lecture et l''affichage de sons ou de vidéos.\nIl ajoute des lecteurs flash adaptés aux formats .mp3.\nIl agit sur tous les <code><docXX|player></code> insérés dans les textes, aussi bien que dans les squelettes.\n\n-* Dans un texte <code><docXX|player></code> affiche un lecteur flash audio (plusieurs lecteurs au choix);\n-* Dans un squelette <code>#MODELE{playliste}</code> permet d''afficher une playliste des derniers mp3[ru]Плагин позволяет проигрывать аудио и видео файлы на сайте. Он добавляет флеш плеер поддерживающий формат mp3.\n\nПлеер выводится при выводе документа кодом <code><docXX|player></code> как в статье, так и в шаблоне.\n\n-* В тексте на сайте <code><docXX|player></code> выводится как flash плеер (есть несколько вариантов плеер на выбор);\n-* В шаблоне <code>#MODELE{playliste}</code> отображает список последних mp3 файлов[sk]Tento zásuvný modul umožňuje prehrávanie a zobrazenie zvukov alebo videí.\nPridáva údaje zo zariadení USB vo formáte .mp3.\nFunguje na všetkých <code><docXX|player></code> vložených v texte, ako aj v šablónach.\n\n-* v texte <code><docXX|player></code> zobrazí údaje zo zariadení USB (môžete si vybrať z viacerých zariadení),\n-* v šablóne <code>#MODELE{playliste}</code> umožňuje zobraziť zoznam skladieb z posledných mp3</multi>', 'a:4:{i:1;a:3:{s:3:"nom";s:4:"BoOz";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:2;a:3:{s:3:"nom";s:7:"Cédric";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:3;a:3:{s:3:"nom";s:9:"erational";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:4;a:3:{s:3:"nom";s:3:"CP.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', 'http://www.spip-contrib.net/Lecteur-Multimedia', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/lecteur_multimedia-2', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', 'daa2ac11cbcbd59342e053be5713d448'),
(43, 43, 'EVAMENTIONS', 'prive/themes/spip/images/evamentions-32.png', '004.002.000', '', '[2.9.9;3.0.99]', '', '<multi>[fr]Le contenu des mentions légales sera affiché en page mentions (et éventuellement dans le pied de page).</multi>', 'a:3:{i:1;a:3:{s:3:"nom";s:38:"Olivier Gautier, développeur EVA-web.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:2;a:3:{s:3:"nom";s:50:"Samuel Bocharel, équipe de développement Eva-WEB";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:3;a:3:{s:3:"nom";s:13:"Johan Pustoch";s:3:"url";s:39:"http://www.spip-contrib.net/?auteur7949";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.evaweb.fr', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_mentions_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '07814a3d694a9c40ac1d414e513636c2'),
(44, 44, 'EVASQUELETTES', 'images/logo_eva3_fb.png', '004.002.004', '', '[3.0.0;3.0.99]', '3.0', '<multi>[fr]Un squelette de portail d’Etablissement, destinée à une école, un établissement du second degré, mais aussi une association, un individuel afin de mettre en place un site web collaboratif proposant différents modèles de publication (article, livre-album, portfolio, activités, fichiers ...).</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:96:"Equipe Eva-dev, Aide à la traduction : Eric Le Jan. Mise à jour pour SPIP 3 : Olivier Gautier.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.evaweb.fr', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_squelettes_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', 'e0f940144d2a12e6d565838c3d2d6694'),
(42, 42, 'EVA_INSTALL', 'prive/themes/spip/images/logo_eva3_fb.png', '004.002.003', '', '[2.9.9;3.0.99]', '', '<multi>[fr]Permet de créer les mots clés nécessaires à l’utilisation du squelette eva-web\r\net de gérer les informations liées à la page de mentions légales.\r\n\r\nCe plugin créé d’abord les groupes puis les mots clés tout en vérifiant que ceux-ci n’existe pas avant. L’utilisation de chaque mot clé est précisé dans chaque descriptif.\r\n\r\nUne fois activée et les mots clés créés, ce plugin peut être désinstallé.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:114:"Samuel Bocharel, Olivier Gautier, équipe de développement Eva-WEB. Mise à jour pour SPIP 3 par Olivier Gautier.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.evaweb.fr', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:13:"evasquelettes";a:2:{s:3:"nom";s:13:"evasquelettes";s:13:"compatibilite";s:6:"[4.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_install_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', 'd7a03a3f3d7a017b9162391ba9924033'),
(38, 38, 'EVA_AGENDA', 'prive/themes/spip/images/logo_eva3_fb.png', '004.002.001', '', '[2.9.9;3.0.99]', '', '<multi>[fr]La rubrique s’affiche sous forme d’agenda lorsqu’on lui joint le mot clé {{agenda}}.\r\nLes articles sont inscrits dans l’agenda en leurs attribuant une date de rédaction antérieure correspondant à la date de l’évènement concerné.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:50:"Olivier Gautier, équipe de développement EVA-web";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://spip.ac-rouen.fr/?Agenda-calendrier-et-mini', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:13:"evasquelettes";a:2:{s:3:"nom";s:13:"evasquelettes";s:13:"compatibilite";s:6:"[4.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_agenda_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '9b36fbaf507fee1dc5b2499adfa35658'),
(39, 39, 'EVABONUS', 'prive/themes/spip/images/logo_eva_bonus.png', '004.002.005', '', '[2.9.9;3.0.99]', '', '<multi>[fr]Noisettes supplémentaires pour les squelettes EVA-web,...</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:38:"Olivier Gautier, développeur EVA-web.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.evaweb.fr', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:2:{s:13:"evasquelettes";a:2:{s:3:"nom";s:13:"evasquelettes";s:13:"compatibilite";s:6:"[4.2;[";}s:13:"eva_habillage";a:2:{s:3:"nom";s:13:"eva_habillage";s:13:"compatibilite";s:6:"[4.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_bonus_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '441f756041379618aaafb92b056238e1'),
(40, 40, 'EVA_CALENDRIER', 'prive/themes/spip/images/logo_eva3_fb.png', '004.002.001', '', '[2.9.9;3.0.99]', '', '<multi>[fr]Affichage d’une rubrique sous forme de calendrier mensuel avec système de navigation vers les mois précédents et suivants.\r\nLa rubrique s’affiche sous forme de calendrier dés lors qu’on lui joint le mot clé {{calendrier}}.\r\nLes articles sont inscrits dans le calendrier en leurs attribuant une date de rédaction antérieure correspondant à la date de l’évènement concerné.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:50:"Olivier Gautier, équipe de développement EVA-web";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://spip.ac-rouen.fr/?Agenda-calendrier-et-mini', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:13:"evasquelettes";a:2:{s:3:"nom";s:13:"evasquelettes";s:13:"compatibilite";s:6:"[4.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_calendrier_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '74b2007eded97598d67acf8a5ff1237e'),
(41, 41, 'EVA_HABILLAGE', 'prive/themes/spip/images/logo_eva3_fb.png', '004.002.006', '2012.1.1', '[2.9.9;3.0.99]', '', '<multi>[fr]Gestion automatique des habillages d’EVA-web 4.2 par un administrateur du site.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:182:"Olivier Gautier, Samuel Bocharel, équipe de développement EVA-web.\nThomas Delhomenie pour l’amélioration de la page des règles CSS.\n Mise à jour pour SPIP 3 : Olivier Gautier.";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://spip.ac-rouen.fr/?-Gestion-des-habillages-sous-EVA-', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:13:"evasquelettes";a:2:{s:3:"nom";s:13:"evasquelettes";s:13:"compatibilite";s:6:"[4.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/eva_habillage_pour_spip_3_0', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', 'ec11a2c164e808c74e0b40096c012335'),
(37, 37, 'ACCESRESTREINT', 'prive/themes/spip/images/zone-32.png', '003.008.005', '0.4.2', '[3.0.0;3.0.*]', '3.0', '<multi>[de]_ Jeder Bereich enthält Rubriken.\n_ Einzelnen Autoren können Zugangsrechte für Bereiche zugeordnet werden.\n_ ALle Standardschleifen (Abfragen) von SPIP werden überladen, und um die Überprüfung der Rechte des jeweiligen Sitebesuchers ergänzt.[en]_ Each area contains sections.\n_ Authors can be associated with these areas to gain access to them.\n_ All SPIP''s loops are loaded with the necessary tools to filter the results according to the visitors'' access priviliges.[es]_ Cada zona contiene secciones.\n_ Los autores podrán ser asociados a ciertas zonas restringidas para tener derechos de acceso a ellas.\n_ Todos los bucles nativos de SPIP quedan modificados para filtrar los resultados en función de los derechos del visitante.[fr]_ Chaque zone contient des rubriques.\n\n_ Les auteurs peuvent etre associés à des zones pour avoir le droit d''y accéder.\n\n_ Toutes les boucles natives de SPIP sont surchargées pour en filtrer les résultats en fonction des droits du visiteur.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:12:"Cedric Morin";s:3:"url";s:22:"http://www.yterium.net";s:4:"mail";s:27:"cedric.morin AT yterium.com";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', 'a:1:{i:1;s:9:"2007-2008";}', 'http://www.spip-contrib.net/Acces-Restreint-3-0', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:1:{s:6:"medias";a:2:{s:3:"nom";s:6:"medias";s:13:"compatibilite";s:3:"[;]";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/accesrestreint_3_5', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '2c60266747ae0ee79768c75f348e3bdc'),
(11, 11, 'BREVES', 'prive/themes/spip/images/breve-32.png', '001.003.005', '1.0.0', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]الأخبار هي معلومات قصيرة بدون مؤلف.[de]Meldungen sind kurze Informationen, deren Autor nicht gespeichert wird.[en]News items are short informations, without authors.[eo]La fulm-informoj estas mallongaj enhavo, sen aŭtoro.[es]Los breves son informaciones cortas sin autor.[fa]اين خبرها اطلاعات كوتاه و بدون مؤلف هستند[fr]Les brèves sont des informations courtes, sans auteur.[fr_tu]Les brèves sont des informations courtes, sans auteur.[it]Le brevi sono piccole fonti di informazioni, senza autori.[lb]Kuerzmeldunge si kléng Informatiounen ouni Auteur.[nl]Nieuwsberiechten zijn kortnieuws zonder auteur.[pt_br]As notas são informações curtas, sem autor.[ru]Новость - это краткая статья без указания автора.[sk]Novinky sú krátke informácie bez autora.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:1:{s:4:"mots";a:1:{s:3:"nom";s:4:"mots";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'breves', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '7c99bde1ac1abea7b967e561f000ed9d'),
(12, 12, 'CFG', 'prive/themes/spip/images/cfg-128.png', '003.000.000', '', '[3.0.0-beta;3.0.*]', '3.0', '<multi>[fr]Augmente les possibilités de gestion de configuration pour d''autres plugins\n		en fournissant un mode de stockage PHP.\n\n		Attention : une partie du fonctionnement de CFG pour SPIP 2 a été intégrée dans SPIP 3 de façon\n		légèrement différente. Il est nécessaire de migrer les plugins qui utilisaient CFG pour SPIP 2.\n		La plupart n''auront plus besoin de ce plugin pour gérer leur configurations.</multi>', 'a:2:{i:1;a:3:{s:3:"nom";s:15:"Bertrand Gugger";s:3:"url";s:0:"";s:4:"mail";s:0:"";}i:2;a:3:{s:3:"nom";s:19:"Matthieu Marcillaud";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', 'a:1:{i:1;s:9:"2008-2011";}', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'cfg', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'b893ce1472495381296cdfb2f4701134'),
(13, 13, 'COMPAGNON', 'prive/themes/spip/images/compagnon-32.png', '001.004.001', '1.0.0', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]يقدم الرفيق مساعدة للمستخدمين\n		لدى أول زيارة لهم الى المجال الخاص في SPIP.[de]Der Begleiter hilft neuen Benutzern bei ihren ersten Schritten im SPIP Redaktionssystem.[en]The companion provides assistance to users\nduring their first visit to the backoffice of SPIP.[eo]La kompano proponas helpon por uzantoj dum liaj unuaj vizitoj al SPIP privata spaco.[es]El compañero ofrece ayuda a los usuarios durante su primera visita al espacio privado de SPIP.[fa]همراه به كاربران در جريان نخستين ازديدشان از قسمت خصوصي اسپيپ كمك ارايه مي‌كند.[fr]Le compagnon offre une aide aux utilisateurs\n		lors de leur première visite dans l''espace privé de SPIP.[it]L''assistente fornisce una guida agli utenti durante la loro prima visita all''area riservata di SPIP.[lb]De Compagnon bitt de Benotzer Hëllef beim éischte Benotze vum privaten Deel vu SPIP.[nl]De metgezel helpt de gebruikers tijdens\n		hun eerste bezoek in het privé-ruimte van SPIP[pt_br]O Companheiro oferece ajuda aos usuários, na primeira visita à área privada do SPIP.[ru]Плагин выводит справку о системе управления при первом входе пользователя в админку.[sk]Compagnon ponúka pomoc používateľom \n		pri ich prvej návšteve v súkromnej zóne SPIPu.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:19:"Matthieu Marcillaud";s:3:"url";s:20:"http://magraine.net/";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:26:"Paul Viluda (Logo, icones)";s:3:"url";s:52:"http://www.cruzine.com/2010/10/06/animals-icon-sets/";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:0:"";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'compagnon', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '65d5dc96bb38f1c8d447a262ae06cf1e'),
(14, 14, 'COMPRESSEUR', 'images/compresseur-32.png', '001.008.006', '', '[3.0.0;3.1.*]', '3.0,3.1', '<multi>[ar]ضغط أوراق الأنماط ورموز جافاسكريبت في ترويسة الصفحات في <code>ecrire/</code> و/او الموقع العمومي[de]Kompression von CSS und Javascript-Code im Kopf der Seiten unter <code>ecrire/</code> und/oder auf der öffentlichen Website[en]Compression of css and javascript in the header of the HTML pages of <code>ecrire/</code> and/or of the public site[eo]Densigo de la css kaj javascript en la kaplinio de HTML-paĝoj pri <code>ecrire/</code> kaj / aŭ de la publika retejo[es]Compresión de css y javascript en los encabezados de las páginas HTML de <code>ecrire/</code> y/o del sitio público[fa]و/يا سايت همگاني <code>ecrire/</code> فشرده‌سازي سي.اس.اس‌ها و جاوا اسكريپت در سرصفحه‌ي صفحات اچ.تي.ام.ال‌[fr]Compression des css et javascript dans l''en-tête des pages html de <code>ecrire/</code> et/ou du site public[it]Compressione di CSS e javascript \nnell''intestazione delle pagine html di <code>ecrire/</code> e/o del sito pubblico pubblico.[lb]Compressioun vun CSS a Javascript am Header vun den HTML-Säiten vun <code>ecrire/</code> an/oder dem ëffentleche Site[nl]Compressie van de css en javascript in de hoofding van html paginas uit <code>ecrire/</code> en/of van het publiek site.[pt_br]Compressão dos css e javascript nos cabeçalhos das páginas html de <code>ecrire/</code> e/ou do site público[ru]Сжимает css и javascript файлы для уменьшения времени загрузки. Используется как на основном сайте, так и в его администраторской части <code>ecrire/</code>[sk]Kompresia css a javascriptu v hlavičke html stránok <code>ecrire/</code> a/lebo na verejne prístupnej stránke</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:1:{s:11:"porte_plume";a:2:{s:3:"nom";s:11:"porte_plume";s:13:"compatibilite";s:8:"[1.6.3;[";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'compresseur', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '82b6e53c6cd642be541b8ff4e06b60dd'),
(15, 15, 'COUTEAU_SUISSE', 'img/couteau-50.gif', '001.008.098', '1.8', '[1.9.2;3.0.99]', '1.9,2.0,2.1,3.0', '<multi>[en]Combines in one plugin a list of small and useful new features improving the management of your SPIP site.\n\nEach of these tools can be enabled or disabled by the user on [the plugin administration page->./?exec=admin_couteau_suisse] and manage a number of variables: click {{Configuration}}, then select the tab Couteau Suisse.\n\nThe categories are: Administration, Security, Private interface, text enhancements, Typographical shortcuts, Display public, Tags, filters, criteria.\n\nCheck out this plugin in your favorite tools: {Removes the number}, {URLs Format}, {Typographical superscript}, {Smart Quotes}, {Beautiful bullets}, {Fight against SPAM}, {Mailcrypt}, {Beautiful URLs} , {SPIP and external links...}, {Smileys}, {A summary for your articles}, {cut in pages and tabs}, etc.., etc..\n\nFeel free to consult the articles of plugin documentation that are published on: [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].\n\nCompatibility: SPIP v1.92x, v2.x.x and v3.0[fr]Réunit en un seul plugin une liste de petites fonctionnalités nouvelles et utiles améliorant la gestion de votre site SPIP.\n\nChacun de ces outils peut être activé ou non par l''utilisateur sur [la page d''administration du plugin->./?exec=admin_couteau_suisse] et gérer un certain nombre de variables : cliquer sur {{Configuration}}, puis choisir l''onglet {{Le Couteau Suisse}}.\n\nLes catégories disponibles sont : Administration, Sécurité, Interface privée, Améliorations des textes, Raccourcis typographiques, Affichage public, Balises, filtres, critères.\n\nDécouvrez dans ce plugin vos outils favoris : {Supprime le numéro}, {Format des URLs}, {Exposants typographiques}, {Guillemets typographiques}, {Belles puces}, {Lutte contre le SPAM}, {Mailcrypt}, {Belles URLs}, {SPIP et les liens... externes}, {Smileys}, {Un sommaire pour vos articles}, {Découpe en pages et onglets}, etc., etc.\n\nN''hésitez pas à consulter les articles de documentation du plugin publiés sur : [spip-contrib.net->http://www.spip-contrib.net/Le-Couteau-Suisse].\n\nCompatibilité : SPIP v1.92x, v2.x.x et v3.0[sk]V jednom zásuvnom module spája veľa malých nových a užitočných funkcií zlepšujúcich riadenie vašej stránky v SPIPe.\n\nKaždý z týchto nástrojov môže používateľ aktivovať alebo deaktivovať [na riadiacej stránke zásuvného modulu->./?exec=admin_couteau_suisse] a vyberať si z určitého počtu premenných: kliknite na {{Konfiguráciu,}} potom vyberte kartu {{Vreckový nožík.}}\n\nK dispozícii sú tieto kategórie: Riadenie, Zabezpečenie, Súkromné rozhranie, Vylepšenie textov, Klávesové skratky, Zobrazenie na verejne prístupnej stránke, Tagy, filtre, kritériá.\n\nObjavte svoje obľúbené nástroje v tomto zásuvnom module: {vymazanie čísla}, {formát internetových adries}, {klávesové indexy}, {inteligentné úvodzovky}, {pekné odrážky}, {boj proti SPAMU}, {šifrovanie pošty}, {pekné internetové adresy}, {SPIP a externé odkazy...}, {smajlíky}, {zhrnutie článku}, {vystrihovanie na stránkach a kartách} a tak ďalej, a tak ďalej.\n\nPokojne si prečítajte dokumentáciu k zásuvnému modulu publikovanú na stránke: [spip-contrib.net.->http://www.spip-contrib.net/Le-Couteau-Suisse]\n\nKompatibilita: SPIP v1.92x, v2.x.x a v3.0</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:33:"Patrice Vanneufville~©~2007-2012";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:301:"Collectif SPIP, Vincent Ramos, Maïeul Rouquette, Fil, Arnaud Ventre, Joseph Larmarange, Cerdic, Pierre Andrews (Mortimer), IZO, Phil, Jérôme Combaz, Bertrand Marne, Têtue, Aurélien Pierard, FredoMkb, Sylvain, BoOz, Alexis Roussel, Paolo, PatV, Yohann(potter64), Nicolas Hoizey, Piero Wbmstr, etc.";s:3:"url";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', 'a:1:{i:1;s:9:"2007-2012";}', 'http://www.spip-contrib.net/Le-Couteau-Suisse', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'couteau_suisse', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '04be585fab957069aeff1a5653a88383'),
(16, 16, 'DUMP', 'prive/themes/spip/images/base-backup-32.png', '001.006.007', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]نسخ احتياطي لقاعدة البيانات بتنسيق SQLite واسترجاعها[de]Sicherung und Wiederherstellung der Datenbank im SQLite-Format[en]Backup and restoration of the database in SQLite format[eo]Savkopio de la datumbazo en SQLite kaj restaŭro[es]Copia de seguridad en SQLite y recuperación[fa]بك‌آپ و ذخيره‌سازي پايگاه داده‌ها در فرمت اس.كيو.لايت[fr]Sauvegarde de la base en SQLite et restauration[it]Backup e ripristino della base dati SQLite[lb]Backup vun der Datebank als SQLite an Zeréckspillen[nl]Backup van de database in SQLite en restauratie[pt_br]Cópia de segurança da base em SQLite e restauro[ru]Резервная копия в SQLite[sk]Záloha databázy v SQLite a jej obnovenie</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'dump', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'b33f73edac59b64f2fd403f79ecf3ca8'),
(17, 17, 'EOLECAS', './eolecas.gif', '001.000', '', '[3.0.0;3.0.*]', '3.0', '', 'a:1:{i:1;a:3:{s:3:"nom";s:11:"Equipe EOLE";s:3:"url";s:27:"http://dev-eole.ac-dijon.fr";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:4:"spip";a:2:{s:3:"nom";s:4:"SPIP";s:13:"compatibilite";s:14:"[3.0.0 ;3.0.*]";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'eolecas', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '0c26b2d614351b7937b3e88e76cf00a6'),
(18, 18, 'IMAGES', 'images/image_filtre-32.png', '001.001.005', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]مرشحات معالجة الصور والألوان[de]Bild- und Farbfilter[en]Images processing and colors filters[es]Filtros de transformación de imágenes y de colores[fa]فيلتر‌هاي پردازش تصوير‌ها و رنگ‌ها[fr]Filtres de transformation d''images et de couleurs[it]Filtri di trasformazione delle immagini e dei colori[lb]Ännerungs-Filteren fir d''Biller an d''Faarwen[nl]Beeld en kleur transformatie filters[pt_br]Filtros de transformação de imagens e de cores[ru]Набор фильтров по работе с изображениями[sk]Filtre na transformáciu obrázkov a farieb</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'filtres_images', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '43f1ddd1628a770daebd9318ce0bf625'),
(19, 19, 'FORUM', 'prive/themes/spip/images/forum-32.png', '001.008.024', '1.2.1', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]منتدى SPIP (الخاص والعمومي)[de]Foren in SPIP (Redaktionsbereich und öffentliche Website)[en]SPIP''s forum (private and public)[es]Foros de SPIP (privado y público)[fa]سخنگاه اسپيپ (خصوصي همگاني)؟[fr]Forum de SPIP (privé et public)[it]Forum di SPIP (privati e pubblici)[lb]SPIP-Forum (privat an ëffentlech)[nl]Forum van SPIP (private en openbaar)[pt_br]Fórum do SPIP (privado e público)[ru]Форумы и комментарии в SPIP[sk]Diskusné fóra SPIPu (súkromné a verejné)</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'forum', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'eb2237450586676652543fe7d2817a2e'),
(20, 20, 'JQUERYUI', 'images/jqueryui-128.png', '001.008.021', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]يثبت هذا الملحق مكتبة jQuery UI في SPIP مما يتيح إنشاء مكونات رسومية ديناميكية: ألسنة صفحات، السحب والرمي، شريط سير تنفيذ العمليات الخ.\nوتنقسم وظائف هذه المكتبة الى ثلاث فئات:\n\n-* {{التفاعلية}}: تتيح إضفاء خصائص شيقة على عناصر HTML كإمكان مسكها ونقلها من مكان الى آخر بواسطة الفأرة (السحب والرمي) او تغيير أحجامها كالإطارات والنوافذ او حتى اختيارها كأيقونات توضع على سطح مكتب نظام التشغيل.\n-* {{الأدوات}}: انها عناصر قائمة بنفسها تقدم وظائف متطورة مثل منتقي التواريخ (DatePicker) الذي يعرض روزنامة لاختيار تاريخ منها.\n-* {{المؤثرات}}: تأتي لإغناء المؤثرات القياسية التي يوفرها jQuery (كالتلاشي والإزاحة الى الأعلى او الأسفل...). وبفضل jQuery UI، يصبح من الممكن مثلاً إضفاء الحركة على خصائص الأنماط (CSS) مثل لون الخلفية من خلال خاصية ().animate او إضافة مؤثرات انتقال في خصائص addClass وremoveClass...[de]Dieses Plugin implementiert die Bibliothel jQuery UI in SPIP und ermöglicht so die Nutzung dynamischer Grafiken für Reiter, Ziehen und Ablegen, Fortschrittsbalken, u.v.m.\n	Die Bibliothek enthält drei Kategorien von Funktionen:\n\n	-* {{Interaktionen}} ermöglichen HTML-Elementen mit zusätzlichen Eigenschaften auszustatten, etwa dem Verschieben mit der Maus, der Größenänderung von Fenstern oder einer Auswahl wie bei Desktop-Icons.\n	-* {{Widgets}} sind gebrauchsfertige komplexe Objekte wie z.B. der DatePicker, welcher einen Kalender zur Datumsauswahl einblendet.\n	-* {{Effekte}} erweitern die Standardeffekte (fadeIn, slideUp...) von jQuery. Mit jQuery UI kann z.B. die Eigenschaft CSS background-color mit .animate() dynamisiert werden, oder Übergänge bei addClass/removeClass eingefügt werden.[en]This plugin implements the jQuery UI library in SPIP allowing the creation of dynamic graphical components: tabs, drag & drop, progress bars...\n\nThe features of this library are divided into three categories:\n-* {{The interactions}}. They can provide very interesting properties to HTML elements, such as being able to be grabbed and moved with the mouse (drag & drop), to be resized as a window, or to be selected as icons on the desktop of your OS.\n-* {{The Widgets}}. They are "turnkey" objects offering high-level features such as DatePicker for example, which displays a calendar to select a date.\n-* {{The effects}}. They enrich the effects offered by default by jQuery (fadeIn, slideUp...). Thanks to jQuery UI, it''s possible for example to animate the CSS property background-color with .animate() or to add transitions for the actions addClass/removeClass...[es]Este plugin implementa en SPIP la la librería complementaria jQuery UI autorizando la creación de componentes gráficos dinámicos: péstañas, drag & drop, barras de progresión...\n	Las funcionalidades de esta librería se dividen en tres categorías:\n\n	-* {{Les interacciones}}. permiten dar propiedades muy interesantes a elementos HTML, como el hecho de poder ser agarrado y desplazado con el mouse (drag & drop), de ser redimensionados como una ventana, o de ser seleccionados como iconos en el escritorio del entorno de ventanas.\n	-* {{Los Widgets}}. Son objetos «llave en mano» qie proponen funcionalidades de alto nivel como el DatePicker por ejemplo, qu muestra un calendario en el que se puede seleccionar una fecha.\n    -* {{Los efectos}}. Enriquecen los efectos por omisión propuestos por jQuery (fadeIn, slideUp...). Gracias a jQuery UI, es posible, por ejemplo, animar la propiedad CSS background-color con .animate() o o también de agregar transiciones a los addClass/removeClass...[fr]Ce plugin implémente dans SPIP la librairie complémentaire jQuery UI autorisant la création de composants graphiques dynamiques : onglets, drag & drop, barres de progression...\n	Les fonctionnalités de cette librairie se divisent en 3 catégories :\n\n	-* {{Les interactions}}. Elles permettent de donner des propriétés très intéressantes à des éléments HTML, comme le fait de pouvoir être attrapé et déplacé avec la souris (drag & drop), d''être redimensionné comme une fenêtre, ou encore d''être sélectionné comme des icônes sur le bureau de votre OS.\n	-* {{Les Widgets}}. Ce sont des objets « clé en main » qui proposent des fonctionnalités de haut niveau comme le DatePicker par exemple, qui affiche un calendrier permettant de sélectionner une date.\n    -* {{Les effects}}. Ils enrichissement les effets par défaut proposés par jQuery (fadeIn, slideUp...). Grâce à jQuery UI, il est par exemple possible d''animer la propriété CSS background-color avec .animate() ou encore d''ajouter des transitions lors des addClass/removeClass...[it]Questo plugin implementa la libreria jQuery UI all''interno di SPIP consentendo la creazione di componenti grafiche dinamiche: tab, drag & drop, barre di progresso ...\n\nLe funzionalità di questa libreria sono divise in tre categorie:\n\n-* {{Le interazioni}}. Queste aggiungono interessanti proprietà agli elementi HTML, come la possibilità di essere prese e trascinate con il mouse (drag & drop), di essere ridimensionate come una finestra, oppure di essere selezionate come le icone del desktop del vostro sistema operativo.\n-* {{I Widgets}}. Questi sono oggetti che offrono caratteristiche di alto livello come il DatePicker per esempio, che mostra un calendario per selezionare le date.\n-* {{Gli Effetti}}. Questi arrichiscono gli effetti già offerti da jQuery (fadeIn, slideUp). Grazie a jQuery UI, è possibile per esempio animare la propriety CSS background-color con .animate() oppure aggiungere una transizione per l''azione addClass/removeClass...[lb]Dëse Plugin installéiert d''jQuery UI Library am SPIP. Si erlaabt dynamesch grafesch Erstelle vun Tabs, Drag&Drop, Progression-Bars, asw.\nD''Funktioune vun dëser Library ginn a 3 Kategorien opgedeelt:\n\n-* {{Interaktiounen}}. Si erlaben HTML-Elementer interessant Méiglechkeeten ze ginn, z.B. kënne mat der Maus gepaakt a geréckelt ginn (Drag & Drop), an der Gréisst wéi eng Fenster geännert ginn, oder wéi Ikone um Büro vun ärem Operating System ausgewielt ginn.\n-* {{Widgets}}. Dat sinn "schlësselfäerdeg" Objekter déi Funktioune vun héigem Niveau erméiglechen, z.B DatePicker deen e Kalenner weist wou e kann en Datum auswielen.\n-* {{Effekter}}. Si beräicheren d''Default-Effekter vun jQuery (fadeIn, slideUp, asw.). Mat jQuery UI ass et z.B. méiglech d''CSS-Proprietéit background-color mat .animate() ze änneren oder Transitiounen mat  addClass/removeClass ze erméiglechen.[nl]Deze plugin implementeert het extra bibliotheek jQuery UI in SPIP waardoor de creatie van dynamische grafische tabs, drag & drop, vooruitgang bars mogelijk wordt...\nKenmerken van dit bibliotheek bestaan uit drie categorieën:\n\n-* {{Interacties}}. Zij kunnen zeer interessante eigenschappen aan HTML-elementen geven, zoals de mogelijkheid om ze te kunnen grijpen en verplaatsen met de muis (drag & drop), aanpassen als een venster, of nog geselecteerd te worden als pictogrammen op het bureau van uw OS.\n-¨* {{Widgets}}. Het zijn objecten "klaar om te gebruiken" die hoog niveau functionaliteit aanbieden, zoals bijvoorbeeld  datepicker die een kalender aanmaakt om het selecteren van een datum te toelaten.\n     - * {{De effecten}}. Ze verrijken  de effecten aangeboden door jQuery (FadeIn, slideUp ...). Dankzij jQuery UI is het bijvoorbeeld mogelijk om de CSS background-color met .animate() te animeren of nog overgangen tijdens addClass / removeClass toe te voegen ...[ru]Данный плагин подключает к сайту функционал библиотеки JQuery UI.[sk]Tento zásuvný modul do SPIPu zavádza doplnkovú knižnicu  jQuery UI umožňujúcu vytváranie dynamických grafických prvkov: tabulátory, drag & drop, grafy zobrazujúce postup pri dokončení niečoho atď.\n	Funkcie tejto knižnice sa rozdeľujú do 3 kategórií:\n\n	-* {{Interakcie:}} dávajú vašim HTML objektom zaujímavé vlastnosti, napríklad môžete na objekty kliknúť myšou, a tak ich presúvať (drag&drop), dá sa meniť ich veľkosť tak ako veľkosť okna, alebo si ich môžete zvoliť ako ikony na pracovnú plochu svojho operačného systému.\n	-* {{Widgety:}} to sú objekty s "prepínačmi", ktoré ponúkajú funkcie vysokej úrovne, ako napríklad DatePicker, ktorý zobrazí kalendár umožňujúci zadanie dátumu.\n    -* {{Efekty:}} obohacujú predvolené efekty, ktoré ponúka jQuery (fadeIn, slideUp...). Napríklad vďaka jQuery UI je možné doplniť vlastnosť CSS background-color o .animate() alebo pridať prechody k addClass/removeClass, atď.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:14:"jQuery UI Team";s:3:"url";s:25:"http://jqueryui.com/about";}}', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'jquery_ui', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'ad0d95cf21f3eb91541e8c4ccf6fdc01');
INSERT INTO `spip_paquets` (`id_paquet`, `id_plugin`, `prefixe`, `logo`, `version`, `version_base`, `compatibilite_spip`, `branches_spip`, `description`, `auteur`, `credit`, `licence`, `copyright`, `lien_doc`, `lien_demo`, `lien_dev`, `etat`, `etatnum`, `dependances`, `date_crea`, `date_modif`, `id_depot`, `nom_archive`, `nbo_archive`, `maj_archive`, `src_archive`, `traductions`, `actif`, `installe`, `recent`, `maj_version`, `superieur`, `obsolete`, `attente`, `constante`, `signature`) VALUES
(21, 21, 'MEDIABOX', 'prive/themes/spip/images/mediabox-32.png', '000.008.004', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]افتراضياً، يتم إضفاء صندوق الفرجة على كل الوصلات الى الصور (لها نوع خاصية يصف mime/type الصورة) إضافة الى الوصلات المزودة بنمط &lt;code&gt;.mediabox&lt;/code&gt;.\n\n\n\nمن الممكن إعداد كل وصلة على حدة من خلال أنماط إضافية:\n\n-* يتيح &lt;code&gt;boxIframe&lt;/code&gt;  فتح الوصلة في إطار iframe\n\n-* يتيج &lt;code&gt;boxWidth-350px&lt;/code&gt; تحديد عرض ٣٥٠ نقطة للإطار\n\n-* يتيح &lt;code&gt;boxHeight-90pc&lt;/code&gt; تحديد ارتفاع ٩٠٪ للإطار\n\n\n\nوتتيح لوحة تحكم تعديل الإعدادات العامة حسب الرغبة كما تتيح اختيار شكل الصندوق من بين الأشكال المتاحة.\n\nيعمل هذا الملحق في الصفحات النموذجية التي تحتوي على علامة <code>#INSERT_HEAD</code>[de]Alle Links zu Bildern (mit einem mime/type für Bilder) sowie Links mit der Klasse <code>.mediabox</code>werden in einer Multimedia-Box angezeigt.\n\nJeder Link kann einzeln um folgende Klassen ergänzt werden:\n\n-* <code>boxIframe</code> öffnet den Link als iFrame in einer Box;\n\n-* <code>boxWidth-350px</code>öffnet eine 350px breite Box;\n\n-* <code>boxHeight-90pc</code> öffnet eine 90% hohe Box;\n\nSie können die Voreinstellungen in einem Konfigurationsdialog einstellen, und das Standarddesign der Boxen auswählen.\n\nDieses Plugin benötigt den Tag <code>#INSERT_HEAD</code> im Kopf des Skeletts.[en]By default, all links to pictures (with a type attribute describing the mime/type of the picture) and\nlinks with the CSS class <code>.mediabox</code> are enriched by multimedia box.\n\nYou can configure each link on a case by case basis with additional classes:\n-* <code>boxIframe</code> enables to open link in iframe box; \n-* <code>boxWidth-350px</code> enables to specify a width of 350px for the box;\n-* <code>boxHeight-90pc</code> enables to specify a height of 90% for the box\n\nA configuration panel lets you edit the general settings to your liking, and the appearance of the box among the available skins.\n\nThis plugin works on skeletons which have the <code>#INSERT_HEAD</code> tag.[es]Por defecto, todos los enlaces definidos sobre imágenes (con un atributo tipo que describa el mime/type de la imafen) así como los enlaces con la clase <code>.mediabox</code> son enriquecidos por la caja multimedia.\n	\nEs posible configurar cada enlace, caso por caso, con clases adicionales:\n\n-* <code>boxIframe</code> permite abrir el enlace en una caja en un iframe ;\n\n-* <code>boxWidth-350px</code> permite definir un ancho de 350px para la caja;\n\n-* <code>boxHeight-90pc</code> perite definir un alto de 90% para la caja.\n\nUn panel de configuración permite modificar tanto las especificaciones generales como el aspecto de la caja, a partir de un conjunto de opciones disponibles. \n	\nEste plugin funciona en los esqueletos que incluyen la baliza <code>#INSERT_HEAD</code>[fa]Par défaut, tous les liens vers des images (avec un attribut type décrivant le mime/type de l''image) ainsi que les liens avec la classe <code>.mediabox</code> sont enrichis par la boîte multimédia.\n	\n	Il est possible de configurer chaque lien au cas par cas avec des classes supplémentaires :\n\n-* <code>boxIframe</code> permet de demander à ouvrir le lien dans une boîte en iframe ;\n\n-* <code>boxWidth-350px</code> permet de spécifier une largeur de 350px pour la boîte ;\n\n-* <code>boxHeight-90pc</code> permet de spécifier une hauteur de 90% pour la boîte.\n\n	Un panneau de configuration vous permet de modifier les réglages généraux à votre convenance, ainsi que l''aspect de la boîte parmi les habillages disponibles.\n	\n	Ce plugin fonctionne sur les squelettes disposant de la balise <code>#INSERT_HEAD</code>[fr]Par défaut, tous les liens vers des images (avec un attribut type décrivant le mime/type de l''image) ainsi que les liens avec la classe <code>.mediabox</code> sont enrichis par la boîte multimédia.\n	\n	Il est possible de configurer chaque lien au cas par cas avec des classes supplémentaires :\n\n-* <code>boxIframe</code> permet de demander à ouvrir le lien dans une boîte en iframe ;\n\n-* <code>boxWidth-350px</code> permet de spécifier une largeur de 350px pour la boîte ;\n\n-* <code>boxHeight-90pc</code> permet de spécifier une hauteur de 90% pour la boîte.\n\n	Un panneau de configuration vous permet de modifier les réglages généraux à votre convenance, ainsi que l''aspect de la boîte parmi les habillages disponibles.\n	\n	Ce plugin fonctionne sur les squelettes disposant de la balise <code>#INSERT_HEAD</code>[it]Per impostazione predefinita, tutti i collegamenti alle immagini (con un attributo che descrive il tipo mime/tipo di foto) e\ncollegamenti con il <code> classe CSS. mediabox </ code> sono arricchite da box multimediale.\n	\nÈ possibile configurare ogni collegamento in una caso per caso con le classi aggiuntive:\n- * <code> BoxIframe </ code> permette di aprire il collegamento nella casella iframe;\n- * <code> Boxwidth-350 pixel </ code> consente di specificare una larghezza di 350 pixel per il box;\n- * <code> BoxHeight-90pc </ code> consente di specificare una altezza di 90% per il box\n\nUn pannello di configurazione permette di modificare le impostazioni generali a proprio piacimento, e l''aspetto tra le skin disponibili.\n\nQuesto plugin funziona su scheletri che hanno la <code> INSERT_HEAD # </ code> tag.[lb]Par défaut hunn all Linken op Biller (mat Attribut mime/type) a Linke mat der Klass <code>.mediabox</code> Zougrëff op d''Mediabox.\n\nAll Link ka mat zousätzleche Klassen agestallt ginn:\n-* <code>boxIframe</code> de Link soll an engem Iframe opgemach ginn;\n-* <code>boxWidth-350px</code> d''Box soll eng Breet vun 350 Pixel hun;\n-* <code>boxHeight-90pc</code> d''Box soll eng Héicht vun 90% hun.\n\nD''Astellung erlaabt de generelle Layout festzeleen, op Basis vun e puer Virlagen.\nDëse Plugin fonktionnéiert mat Skeletter déi den Tag <code>#INSERT_HEAD</code> hun.[nl]Standaard worden alle links naar beelden (met een type attribuut beschrijving van de beeld mime/type) en de links met een class <code>.mediabox </ code> verrijkt door de mediabox.\n\nHet is mogelijk om elke link telkens met extra klassen te configureren :\n\n- * <code>boxIframe</code> om de link in een iframe doos te tonen;\n\n- * <code>boxWidth-350px</code> om een boxbreedte van 350px te specificeren;\n\n- <code>boxHeight-90pc</code>  om een hoogte van 90% voor de box te specificeren.\n\nIn een configuratiepaneel kunt u de algemene instellingen voor uw gemak bewerken, en ook het uiterlijk van de doos vervangen uit de beschikbare skins.\n\nDeze plugin werkt op de skeletten die de <code>#INSERT_HEAD</ code> SPIP tag gebruiken.[ru]По умолчанию все ссылки на картинки (файлы с соответствующим mime/type атрибутом) и ссылки с CSS классом <code>.mediabox</code> выводятся в сплывающем окне.\n	\n	Вы можете настроить класс ссылки для каждого конкретного случая :\n\n-* <code>boxIframe</code> позволяет открывать ссылки в iframe ;\n\n-* <code>boxWidth-350px</code> задает ширину всплывающего окна в 350px ;\n\n-* <code>boxHeight-90pc</code> задает высоту окна в 90%.\n\nВ настройках модуля вы можете изменять 	внешний вид блоков и прочие настройки.\n	Модуль работает только в случае, если вы добавляете <code>#INSERT_HEAD</code> в ваши шаблоны.[sk]Podľa predvolených nastavení všetky odkazy k obrázkom (s typom v parametre, ktorý uvádza mime/typ obrázka) a odkazy v triede <code>.mediabox</code> obohacuje multimediálny box.\n\nKaždý odkaz môžete osobitne nastaviť pomocou doplnkových tried:\n\n-* <code>boxIframe</code> umožňuje otvárať odkaz v ráme iframe,\n\n-* <code>boxWidth-350px</code> umožňuje definovať šírku poľa 350px,\n\n-* <code>boxHeight-90pc</code> umožňuje zadať výšku poľa 90%.\n\nOvládací panel vám umožňuje všeobecné nastavenia podľa vašich želaní vybrať dizajn boxu z dostupných vzhľadov.\n\nTento zásuvný modul spolupracuje so šablónami, ktoré obsahujú tag <code>#INSERT_HEAD</code></multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:12:"ColorPowered";s:3:"url";s:33:"http://colorpowered.com/colorbox/";}}', '', '', 'http://www.spip-contrib.net/MediaBox', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'mediabox', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '12c1bc818981e1ad344ca50ba837e31a'),
(22, 22, 'MEDIAS', 'prive/themes/spip/images/portfolio-32.png', '002.007.045', '1.2.4', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]أدارة الوسائط المتعددة في SPIP[de]Medienverwaltung in  SPIP[en]SPIP''s media management[es]Gestión de documentos multimedia de SPIP[fa]مديريت رسانه‌‌هاي اسپيپ[fr]Gestion des médias de SPIP[it]Gestione dei media di SPIP[lb]Gestioun vun de Medien vu SPIP[nl]Beheer van de medias in SPIP[pt_br]Gerenciamento de mídias do SPIP[ru]Управление медиа файлами[sk]Správa multimédií v SPIPe</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:57:"Cédric Morin, Romy Duhem-Verdière pour la médiathèque";s:3:"url";s:0:"";}}', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:2:{s:1:"z";a:2:{s:3:"nom";s:1:"Z";s:13:"compatibilite";s:8:"[1.2.1;[";}s:8:"mediabox";a:2:{s:3:"nom";s:8:"mediabox";s:13:"compatibilite";s:8:"[0.5.0;[";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'medias', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'ee9f14cd6ec14006a4f7bef66235f138'),
(23, 23, 'MOTS', 'prive/themes/spip/images/mot-32.png', '002.004.010', '2.1.1', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]المفاتيح ومجموعات المفاتيح[de]Schlagworte und Schlagwortgruppen[en]Keywords and keywords groups[es]Palabras clave y grupos de palabras clave[fa]واژه‌ها و گروه‌ واژه‌ها[fr]Mots et Groupes de mots[it]Parole e Gruppi di parole[lb]Schlësselwierder a Gruppen[nl]Terfwoorden en groepen van trefwoorden[pt_br]Palavras-chave e seus grupos[ru]Ключи и группы ключей[sk]Kľúčové slová a skupiny kľúčových slov</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'mots', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '2051218f7a26845f28ddba47ee6e87e6'),
(24, 24, 'MSIE_COMPAT', 'images/msiecompat-32.png', '001.002.000', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]يتيح اختيار وسائل متنوعة\n\n-* لتأمين سلامة عرض ملفات PNG الشفافة في إنترنت إكسبلورر ٦\n\n-* و/او تفعيل علامات أوراق الأنماط في إنترنت إكسبلورر ٦ و٧[de]Bietet verschiedene Methoden für:\n\n-* transparente PNG Dateien unter MSIE~6 \n\n-* und /oder diverse CSS Selektoren für MSIE~6 und~7.[en]Offers different javascript methods to add support for:\n-* transparent PNG files to MSIE~6 \n-* and/or various CSS selectors to MSIE~6 and~7.[es]Permite seleccionar diferentes métodos \n\n-* para garantizar la visualización de archivos PNG transparentes en MSIE 6 ~\n\n-* y/o activar algunos selectores CSS en MSIE~6 y ~7.[fa]مجوز گزينش روش‌هاي مختلف\n\n-* براي اطمينان از نمايش پرونده‌هاي پي.ان.گي پشفاب‌ در ام.اس.آي.اي~ 6\n\n-*و/يا فعال كردن بعضي از گزينشگر‌هاي اس.اس.سي در ام.اس.آي.اي~ 6 و ~7[fr]Permet de sélectionner différentes méthodes \n\n-* pour assurer l''affichage des fichiers PNG transparents sous MSIE~6 \n\n-* et/ou activer certains sélecteurs CSS dans MSIE~6 et~7.[it]Offre diversi metodi javasciprt per aggoiungere il supporto per \n-* immagini PNG trasparenti per  MSIE~6 \n-* e/o attivare alcuni selettori CSS per MSIE~6 e ~7.[lb]Erlaabt verschidde Methoden ze wielen\n\n-* fir déi transparent PNG Biller ënner MSIE~6 ze gesinn\n-* an/oder verschidde CSS Selektoren am MSIE~6 a ~7 anzeschalten.[nl]Geeft de mogelijkheid om verschillenden methodes te kiezen om:\n\n-* transparante PNG''s onder MSIE~6 goed te tonen\n\n-* en/of een aantal CSS-selectors in MSIE~6 en 7 te activeren[pt_br]Permite selecionar métodos diferentes\n\n-* para garantir a exibição de arquivos PNG transparentes em MSIE~6 \n\n-* e/ou ativar certos seletores CSS em MSIE~6 et~7.[ru]Несколько разных javascript библиотек, которые обеспечивают:\n\n-* поддержку прозрачности PNG  для MSIE~6 \n\n-* поддержку ряда CSS селекторов для MSIE~6 et~7.[sk]Umožňuje vám zvoliť si iné metódy:\n\n-* na zabezpečenie zobrazenia priehľadných súborov PNG na MSIE~6,\n\n-* a/lebo aktivovať určité voliče CSS v~MSIE~6 a~7.</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:2:{i:1;a:2:{s:3:"nom";s:27:"Kush M. (jquery.ifixpng.js)";s:3:"url";s:26:"http://jquery.khurshid.com";}i:2;a:2:{s:3:"nom";s:27:"Dean Edwards et al (IE7.js)";s:3:"url";s:32:"http://code.google.com/p/ie7-js/";}}', 'a:2:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}i:2;a:2:{s:3:"nom";s:3:"MIT";s:3:"url";s:46:"http://opensource.org/licenses/mit-license.php";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'msie_compat', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '0ce5f5e60b8255ce8369b7d687890c7a'),
(25, 25, 'ORGANISEUR', 'prive/themes/spip/images/calendrier-32.png', '000.008.010', '1.1.1', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]أدوات عمل تحريري جماعي[de]Werkzeuge für eine Online-Redaktion[en]Collaborative editorial working tools[es]Herramientas de trabajo editorial en grupo[fa]ابزارهاي گروه سردبيري[fr]Outils de travail éditorial en groupe[it]Strumenti per il lavoro editoriale in gruppo[lb]Gruppenaarbecht[nl]Samenwerking- en werkinstrument voor de redacteurs[ru]Организация совместной работы[sk]Nástroje na redakčnú prácu v skupine</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:24:"Adam Shaw (FullCalendar)";s:3:"url";s:31:"http://arshaw.com/fullcalendar/";}}', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'organiseur', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'e649b005303e9382dfe434ca5d00caf1'),
(26, 26, 'PETITIONS', 'prive/themes/spip/images/petition-32.png', '001.004.003', '1.1.6', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]العرائض في SPIP[de]Petitionen mit SPIP[en]Petitions in SPIP[es]Peticiones en SPIP[fa]طومارها در اسپيپ[fr]Pétitions dans SPIP[it]Petizioni di SPIP[nl]Petities in SPIP[pt_br]Petições do SPIP[ru]Модуль сбора подписей для SPIP[sk]Petície v SPIPe</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'petitions', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '680de13b47f0cb401be54b82d26e7227'),
(27, 27, 'PORTE_PLUME', 'images/porte-plume-32.png', '001.012.002', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]الريشة هي شريط أدوات موسع لنظام SPIP يستخدم مكتبة جافاسكريبت [MarkItUp-&gt;http://markitup.jaysalvat.com/home/][de]Der Federhalter ist eine erweiterbare Werkzeugleiste für SPIP auf Grundlage der Javascript-Bibiliothek [MarkItUp->http://markitup.jaysalvat.com/home/][en]The Quill is a SPIP extensible toolbar which uses the [MarkItUp->http://markitup.jaysalvat.com/home/] javascript library.[es]Porta pluma es una barra de herramientas extensible para SPIP que utiliza la librería javascript [MarkItUp->http://markitup.jaysalvat.com/home/][fa]چوب قلم يك نوار ابزار قابل گسترش براي اسپيپ است كه آرشيو جاوا اسكريپت [MarkItUp->http://markitup.jaysalvat.com/home/] را مورد استفاده قرار مي‌دهد.[fr]Porte plume est une barre d''outil extensible pour SPIP qui utilise la librairie javascript [MarkItUp->http://markitup.jaysalvat.com/home/][it]Portapenne è una barra degli strumenti estensibile per SPIP che utilizza la libreria javascript [MarkItUp->http://markitup.jaysalvat.com/home/][nl]Penhouder is een rekbare werkbalk voor SPIP die gebruik van [MarkItUp->http://markitup.jaysalvat.com/home/] javascript library  maakt.[ru]Porte plume - расширение возможностей стандартного текстового редактора SPIP. Использует javascript библиотеку  [MarkItUp->http://markitup.jaysalvat.com/home/].[sk]Porte plume je rozšíriteľný panel s nástrojmi pre SPIP, ktorý využíva javascriptovú knižnicu [MarkItUp->http://markitup.jaysalvat.com/home/]</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:19:"Matthieu Marcillaud";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:2:{i:1;a:2:{s:3:"nom";s:21:"Jay Salvat (MarkitUp)";s:3:"url";s:30:"http://markitup.jaysalvat.com/";}i:2;a:2:{s:3:"nom";s:18:"FamFamFam (Icones)";s:3:"url";s:25:"http://www.famfamfam.com/";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.spip-contrib.net/Porte-plume,3117', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'porte_plume', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '6097f7a4ed383f2d52bf9057b91f2c0f'),
(28, 28, 'REVISIONS', 'prive/themes/spip/images/revision-32.png', '001.007.005', '1.1.4', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]متابعة التغييرات في العناصر التحريرية[de]Änderungen von redaktionellen Inhalten nachvollziehen[en]Tracking changes of editorial objects[es]Seguimiento de las modificaciones de los objetos editoriales[fa]پيگيري اصلاحات موضوع‌هاي سردبيري[fr]Suivi des modifications des objets éditoriaux[it]Il rilevamento delle revisioni sugli oggetti editoriali[lb]Verwaltung vun de Versiounen vun den Objekter[nl]De revisies van de editoriale objecten volgen[pt_br]Acompanhamento das alterações dos objetos editoriais[ru]История изменений[sk]Sledujte zmeny redakčných objektov</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'revisions', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '8267b1b6e67f9f129c4658810acca44b'),
(29, 29, 'SAFEHTML', 'images/safehtml-32.png', '001.004.000', '', '[3.0.0;3.0.*]', '3.0', '<multi>[en]Forums protection against cross-site scripting attacks[es]Protección de los foros contra ataques de type cross-site scripting[fr]Protection des forums contre les attaques de type cross-site scripting[ru]Защищает форму комментариев от межсайтового скриптинга (XSS)</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:3:{i:1;a:2:{s:3:"nom";s:12:"Roman Ivanov";s:3:"url";s:0:"";}i:2;a:2:{s:3:"nom";s:10:"Pixel-Apes";s:3:"url";s:0:"";}i:3;a:2:{s:3:"nom";s:8:"JetStyle";s:3:"url";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:3:"GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://www.ohloh.net/p/safehtml', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'safehtml', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'd5b71754c776f203bc720efd9bf638af'),
(30, 30, 'SITES', 'prive/themes/spip/images/site-32.png', '001.007.008', '1.0.0', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]مواقع وترخيص في SPIP (خاص وعمومي)[de]Verwaltung verlinkter und syndizierter Websites mit  SPIP (im öffentlichen und Redaktionsbereich)[en]Sites and syndication in SPIP (private and public)[es]Sitios y sindicación en SPIP (privado y público)[fa]سايت‌ها و مشترك‌سازي‌ها در اسپسپ (خصوصي و همگاني)[fr]Sites et syndication dans SPIP (privé et public)[it]Siti e syndication in SPIP (privato e pubblica)[lb]Websäiten a Syndicatioun am SPIP (privat an ëffentlech)[nl]Sites en Websyndicatie in SPIP (privé en publiek)[pt_br]Sites e sindicação do SPIP (privado e público)[ru]Подключение других сайтов по RSS[sk]Stránky a syndikácia v SPIPe (súkromná aj verejná)</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'sites', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'd514bbfcf3bba62554cbd99e15ad6a3b'),
(31, 31, 'SQUELETTES_PAR_RUBRIQUE', 'squelettes_par_rubrique-32.png', '001.001.001', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]دعم الصفحات النموذجية التي تملك لاحقة رقمية و/او لاحقة رمز لغة[de]Unterstützung der Skelette mit Rubriknummer und/oder Sprachcode als Namenszusatz: (-23.html, =23.html, et .en.html)[en]Support of suffixed templates by section number and/or by language code : (-23.html, =23.html, and .en.html)[es]Implementación de esqueletos con sufijo por numero de sección y/o por código de idioma: (-23.html, =23.html, y .en.html)[fa]پشتيبان اسكلت‌هاي پسوندي توسط تعدادي بخش و/يا توسط كدر زبان :  (-23.html, =23.html, et .en.html)[fr]Support des squelettes suffixés par numéro de rubrique et/ou par code de langue : (-23.html, =23.html, et .en.html)[it]Supporto dei modelli con suffisso numero di sezione e/o il codice della lingua: (-23.html, =23.html, et .en.html)[lb]Ënnerstëtzung vun de Skeletter mat engem Suffix fir d''Rubrik an/oder d''Sprooch: (-23.html, =23.html, an .en.html)[nl]Ondersteuning van de suffix ingewijden skeletten (met rubrieknummers en / of taal-code: -23.html, =23.html   en .en.html)[pt_br]Suporte aos gabaritos com sufixo do númeor da seção e/ou por código de idioma: (-23.html, =23.html, et .en.html)[ru]Позволяет задавать отдельные шаблоны по номеру раздела, статьи, а так же языковой версии : (-23.html, =23.html, и .en.html)[sk]Podpora šablón pripojených vo forme prípony podľa čísla rubriky a/lebo kódu jazyka: (-23.html, =23.html a .en.html)</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'squelettes_par_rubrique', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'ac1763b47df50a486f60aedb28ade07f'),
(32, 32, 'STATS', 'prive/themes/spip/images/statistique-32.png', '000.004.015', '1.0.0', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]إحصاءات SPIP[de]Estadísticas de SPIP[en]Statistics of SPIP[es]Estadísticas de SPIP[fa]آمارهاي اسپيپ[fr]Statistiques de SPIP[it]Statistiche di SPIP[lb]SPIP-Statistiken[nl]Statistieken van SPIP[pt_br]Estatísticas do SPIP[ru]Статистика посещений сайта[sk]Štatistiky SPIPu</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'statistiques', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '4d7d69487713e8209b19d5c2eba5fbac'),
(33, 33, 'SVP', 'svp-64.png', '000.080.014', '0.4.0', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]يقدم هذا الملحق، من جهة، دارة تتيح تنفيذ بحث متعدد المعايير حول الملحقات وتجميع معلومات عنها (نماذج وظيفية، نماذج تصاميمن صفحات نموذجية) ومن جهة أخرى، يقدم واجهة لإدارة الملحقات والعلاقات بينها.[de]SVP stellt ein API bereit, mit dem SPIP-Plugins (Zusatzfunktionen, Themen und  Skelette) nach mehreren Kriterien gesucht  und ausgewählt werden können, und bietet eine Oberfläche zur Verwaltung der  Plugins sowie ihrer Abhängigkeiten.[en]On one hand, this plugin provides an API to perform multi-criteria searches, to collect and present information on SPIP plugins (functional modules, themes, and skeletons) and on the other hand, proposes a new administration interface to manage the dependencies between plugins.[es]Este plugin proporciona, por un lado, una API que permite efectuar búsquedas multi-criterio, recopilar y presentar información sobre los plugins (módulos funcionales, temas y esqueletos) y, por otro lado, una nueva interfaz de administración de los plugins que gestiona las dependencias entre ellos.[fa]اين پلاگينن،‌ از يك طرف يك آي.پي.يي براي جستجو‌‌هاي چند معياره  براي گردآوري و ارايه‌ي اطلاعات در مورد پلاگين‌هاي اسپيپ (مادوول‌هاي كاركردي، تم‌ها و اسكلث‌ها) در اختيار مي‌گذارد، و از طرف ديگر، يك واسط مديريتي جديد براي مديريت وابستگي‌هاي بين پلاگين‌ها را پيشنهاد مي‌كند.[fr]Ce plugin fournit, d''une part,  une API permettant d''effectuer des recherches multi-critères, de collecter, et de présenter les informations sur les plugins SPIP (modules fonctionnels, thèmes et squelettes) et d''autre part, une nouvelle interface d''administration des plugins gérant les dépendances entre plugins.[it]Da un lato, questo plugin fornisce un''API per eseguire ricerche multi-criteri, per raccogliere e presentare informazioni su SPIP plugin (moduli funzionali, temi e scheletri) e, dall''altro, propone una nuova interfaccia di amministrazione per gestire le dipendenze tra i plugin.[nl]Enerzijds geeft deze plugin een API aan voor het multi-criteria zoeken, het bijeenbregen en het voorstellen van informaties over SPIP Plugins (functionele modules, thema''s en skeletten) en anderzijds, geeft het een nieuwe interface voor het beheer van plugins dat ook de  afhankelijkheden tussen hen in acht neemt.[ru]Этот модуль предоставляет API и интерфейс для управления плагинами SPIP.[sk]Tento zásuvný modul poskytuje, po prvé, aplikáciu umožňujúcu vykonávať vyhľadávania na základe viacerých kritérií, vyhľadať a zobraziť informácie o zásuvných moduloch SPIPu (funkčných moduloch, farebných motívoch i šablónach) a po druhé, nové rozhranie nastavení zásuvných modulov riadi závislosti medzi zásuvnými modulmi.</multi>', 'a:2:{i:1;a:3:{s:3:"nom";s:14:"Eric Lupinacci";s:3:"url";s:23:"http://blog.smellup.net";s:4:"mail";s:0:"";}i:2;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:5:"GPL 3";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://blog.smellup.net/spip.php?rubrique1', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'svp', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '109cc51a6c5655be204c230c97c6b1c0'),
(34, 34, 'TW', 'textwheel-32.png', '000.008.017', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]دمج TextWheel في SPIP[de]Integration von TextWheel in SPIP[en]Integrate TextWheel in SPIP[es]Integrar TextWheel en SPIP[fa]ادغام چرخ‌نويس (TextWheel) در اسپيپ[fr]Intégrer TextWheel dans SPIP[it]Integra TextWheel in SPIP[lb]TextWheel a SPIP integréieren[nl]TextWheel in SPIP integreren[ru]Интегрировать TextWheel в SPIP[sk]Integrovať TextWheel do SPIPu</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:8:"GNU/LGPL";s:3:"url";s:37:"http://www.gnu.org/licenses/lgpl.html";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:2:{s:4:"yaml";a:2:{s:3:"nom";s:4:"yaml";s:13:"compatibilite";s:6:"[1.3;[";}s:11:"memoization";a:2:{s:3:"nom";s:11:"memoization";s:13:"compatibilite";s:6:"[0.9;[";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'textwheel', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '9999b20cfc1a6d81c653f952466a6e21'),
(35, 35, 'URLS', 'prive/themes/spip/images/url-32.png', '001.004.014', '1.1.3', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]إدارة تنوعات عناوين URL ذات المعنى ام لا[de]Verwaltung von URL-Varianten[en]Management of the URL variants, meaningful or not[es]Gestión de las variantes de URL significativas o no[fa]مديريت تنوع يو.آر.ال‌هاي مهم يا غيرمهم[fr]Gestion des variantes d''URL signifiantes ou non[it]Gestione delle varianti significative URL, o non[nl]Beheer van de URL''s varianten (betekenisdragend of niet)[pt_br]Gerenciamento das variantes de URLs significantes ou não[ru]SEO-friendly URLs (ЧПУ ссылки)[sk]Riadenie variantov URL, či už sémantických, alebo nie</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', '', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'urls_etendues', '', 'oui', 'oui', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', '7ab583c9c8a5dac09f700fd9034c89c9'),
(36, 36, 'VERTEBRES', 'images/vertebres-32.png', '001.002.002', '', '[3.0.0;3.0.*]', '3.0', '<multi>[ar]تقدم إضافة الفقرات وسيلة عرض جداول SQL في قاعدة البيانات وذلك بإدخال متغير في عنوان الصفحة يحدد الجدول المطلوب عرضه تسبقه عبارة &quot;table:&quot;، مثلاً:\n<code>ecrire/?exec=vertebres&table=spip_articles</code>[de]Das Plugin Wirbelsäule ermöglicht SQL Tabellen zu lesen, indem im URL der Parameter &quot;table:&quot; angegeben wird. Beispiel: <code>ecrire/?exec=vertebres&table=spip_articles</code>[en]Vertebrae provides a way to read a SQL table indicating\n as an argument of the page the database table to read, preceded of\n"table", for example: <code>ecrire/?exec=vertebres&table=spip_articles</code>[es]Vértebras propone una forma de leer una tabla SQL indicando como argumento de página el nombre de la tabla a leer precedido de &quot;table:&quot;, por ejemplo : <code>ecrire/?exec=vertebres&table=spip_articles</code>[fa]فقرات روشي براي خواندن يك جدول اس.كيو.ال كه به يك آرگومان صفحه‌ي جدول پايگاه داده‌ةا دلالت دارد، ارايه مي‌كند، پيش از «جدول»، به عنوان نمونه:\n<code>ecrire/?exec=vertebres&table=spip_articles</code>[fr]Vertèbres propose un moyen de lire une table SQL en indiquant comme argument de page la table à lire, précédé de &quot;table:&quot;, par exemple : <code>ecrire/?exec=vertebres&table=spip_articles</code>[it]Vertèbres offre un modo per leggere una tabella SQL passata come parametro, preceduta da  &quot;table:&quot;, per esempio : <code>ecrire/?exec=vertebres&table=spip_articles</code>[lb]Vertèbres erlaabt eng SQL-Tafel ze liesen andeem en als Argument de Numm vun der Tafel ugett, mat virdrun "table:", z.B.: <code>ecrire/?exec=vertebres&table=spip_articles</code>[nl]Wervels geeft je de mogelijkheid om SQL-tabels te lezen. Schrijf "table=naam_van_de_tabel" als argument van de pagina, bijvoorbeeld: <code>ecrire/?exec=vertebres&table=spip_articles</code>[ru]Это плагин (Vertèbres) позволяет получать информацию из таблиц базы данных (SQL) передавая в адресной строке название таблицы, например : <code>ecrire/?exec=vertebres&table=spip_articles</code>[sk]Vertebres ponúka spôsob na čítanie tabuľky SQL ako parameter, ktorý požaduje od stránky, aby prečítala tabuľku, pred ktorou je "table:", napríklad: <code>ecrire/?exec=vertebres&table=spip_articles</code></multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:14:"Collectif SPIP";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:19:"FatCow pour l''icone";s:3:"url";s:33:"http://www.fatcow.com/free-icons/";}}', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', '', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:0:{}s:9:"librairie";a:0:{}s:7:"utilise";a:0:{}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'vertebres', '', 'oui', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS_DIST', 'b6ac0d43495d7f632b84c4df5e74fde5'),
(47, 47, 'ESCAL', 'images/escal32.png', '003.071.012', '', '[2.10.0;[', '2.1,3.0,3.1', '<multi>[fr]Adapté pour la version 3 de SPIP, il propose :\r\n-* une configuration poussée dans l''espace privé\r\n-* une mise en page paramétrable en 2 ou 3 colonnes par le choix d''une feuille de style\r\n-* une gestion du multilinguisme\r\n-* un forum simple style phpBB\r\n-* un large choix de noisettes à garder ou pas, certaines étant redondantes : identification, menu horizontal et/ou vertical.\r\n-* place, couleur et contenu des blocs latéraux faciles à changer\r\n-* un menu horizontal et/ou vertical (2 au choix) déroulants avec mise en valeur de la rubrique courante\r\n-* une redirection automatique vers l''article s''il est seul dans sa rubrique\r\n-* une navigation par mots-clés\r\n-* un calendrier et/ou une liste d''évènements\r\n-* un affichage des derniers articles ou des articles dans la même rubrique\r\n-* un affichage des sous-rubriques et des articles dans chaque page rubrique\r\n-* un affichage des forums des articles\r\n-* un formulaire de contact des auteurs s''ils ont indiqué leur e-mail\r\n-* une page contact élaborée\r\n-* un plan du site\r\n-* un fichier backend pour la syndication du site\r\n-* une feuille de style spéciale pour l''impression des articles\r\n-* une zone de connexion en page d''accueil (2 choix possibles d''affichage)\r\n-* un ensemble valide XHTML Strict 1.0</multi>', 'a:1:{i:1;a:3:{s:3:"nom";s:26:"Jean-Christophe Villeneuve";s:3:"url";s:0:"";s:4:"mail";s:0:"";}}', '', 'a:1:{i:1;a:2:{s:3:"nom";s:7:"GNU/GPL";s:3:"url";s:40:"http://www.gnu.org/licenses/gpl-3.0.html";}}', '', 'http://projetice.crdp.ac-lyon.fr/escal/', '', '', 'stable', 4, 'a:3:{s:9:"necessite";a:1:{i:0;a:1:{s:6:"agenda";a:2:{s:3:"nom";s:6:"agenda";s:13:"compatibilite";s:9:"[3.11.2;[";}}}s:9:"librairie";a:0:{}s:7:"utilise";a:1:{i:0;a:2:{s:7:"palette";a:2:{s:3:"nom";s:7:"palette";s:13:"compatibilite";s:8:"[2.0.0;[";}s:8:"spip_400";a:1:{s:3:"nom";s:8:"spip_400";}}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 'auto/escal_V3', '', 'non', 'non', 0, '', 'non', 'non', 'non', '_DIR_PLUGINS', '49c4e83d7196f5ec9b543a229968bc58');

-- --------------------------------------------------------

--
-- Structure de la table `spip_petitions`
--

CREATE TABLE IF NOT EXISTS `spip_petitions` (
  `id_petition` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_article` bigint(21) NOT NULL DEFAULT '0',
  `email_unique` char(3) NOT NULL DEFAULT '',
  `site_obli` char(3) NOT NULL DEFAULT '',
  `site_unique` char(3) NOT NULL DEFAULT '',
  `message` char(3) NOT NULL DEFAULT '',
  `texte` longtext NOT NULL,
  `statut` varchar(10) NOT NULL DEFAULT 'publie',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_petition`),
  UNIQUE KEY `id_article` (`id_article`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_petitions`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_plugins`
--

CREATE TABLE IF NOT EXISTS `spip_plugins` (
  `id_plugin` bigint(21) NOT NULL AUTO_INCREMENT,
  `prefixe` varchar(30) NOT NULL DEFAULT '',
  `nom` text NOT NULL,
  `slogan` text NOT NULL,
  `categorie` varchar(100) NOT NULL DEFAULT '',
  `tags` text NOT NULL,
  `vmax` varchar(24) NOT NULL DEFAULT '',
  `date_crea` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modif` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `compatibilite_spip` varchar(24) NOT NULL DEFAULT '',
  `branches_spip` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_plugin`),
  KEY `prefixe` (`prefixe`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Contenu de la table `spip_plugins`
--

INSERT INTO `spip_plugins` (`id_plugin`, `prefixe`, `nom`, `slogan`, `categorie`, `tags`, `vmax`, `date_crea`, `date_modif`, `compatibilite_spip`, `branches_spip`) VALUES
(46, 'SPIP_BONUX', 'SPIP Bonux', '<multi>[ar]الملحق الذي يجعل SPIP أكثر بياضاً من.[en]The plugin that make SPIP better than SPIP.[es]El plugin que lava más SPIP que SPIP.[fr]Le plugin qui lave plus SPIP que SPIP.[ru]SPIP Bonux существенно расширяет возможности SPIP, делая его лучше чем SPIP :)))[sk]Zásuvný modul, ktorý prečistí SPIP ako SPIP.</multi>', 'outil', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(45, 'PLAYER', 'Lecteur Multimédia', '<multi>[de]Lire des sons et des vidéos[en]Read sounds and videos[fa]خواندن صدا و ويديو[fr]Lire des sons et des vidéos[ru]Аудио и видео плеер[sk]Prehrávanie zvukov a videí</multi>', 'multimedia', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(44, 'EVASQUELETTES', 'EVA-WEB 4.2', '<multi>[fr]Distribution des squelettes EVA-WEB 4.2 pour SPIP 3</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(43, 'EVAMENTIONS', 'Gestion des mentions légales', '<multi>[fr]Gérer la page des mentions légales du site</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(42, 'EVA_INSTALL', 'Création des mots clés d’eva-web', '<multi>[fr]Les mots-clé nécessaires à l’utilisation d’eva-web</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(41, 'EVA_HABILLAGE', 'Gestion des habillages pour EVA-Web 4.2', '<multi>[fr]Gérer les habillages d’EVA</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(40, 'EVA_CALENDRIER', 'Rubrique calendrier pour EVA-web', '<multi>[fr]Affichage d’une rubrique sous forme de calendrier</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(39, 'EVABONUS', 'EVA-bonus pour EVA-web 4.2', '<multi>[fr]Des ajouts bonus pour EVA-web</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(37, 'ACCESRESTREINT', '<multi>[de]Zugangskontrolle[en]Restricted Access[es]Acceso restringido[fr]Accès Restreint</multi>', '<multi>[de]Verwaltung von Bereichen mit Zugangskontrolle[en]Management of areas with restricted access[es]Gestión de zonas de acceso restringido[fr]Gestion de zones d''accès restreint</multi>', 'auteur', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(38, 'EVA_AGENDA', 'Affichage d’une rubrique sous forme d’agenda.', '<multi>[fr]Affichage d’une rubrique sous forme d’agenda.</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(11, 'BREVES', 'Brèves', '<multi>[ar]إدارة الأخبار في SPIP[de]Meldungen in  SPIP verwalten[en]Management of news items in SPIP[eo]Mastrumado de fulm-informoj en SPIP[es]Gestión de los breves en SPIP[fa]مديريت خبرها در اسپيپ[fr]Gestion des brèves dans SPIP[fr_tu]Gestion des brèves dans SPIP[it]Gestione delle brevi in SPIP[lb]Gestioun vun de Kuerzmeldungen am SPIP[nl]Beheer van nieuwsberichten in SPIP[pt_br]Gerenciamento de notas no SPIP[ru]Настройки ленты новостей[sk]Správa noviniek v SPIPe</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(12, 'CFG', 'CFG', '<multi>[fr]Gestion de configurations.</multi>', 'maintenance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(13, 'COMPAGNON', '<multi>[ar]الرفيق[de]Begleiter[en]Companion[eo]Kompano[es]Compañero[fa]همراه[fr]Compagnon[it]Assistente[lb]Compagnon[nl]Metgezel[pt_br]Companheiro[ru]Помощник (Companion)[sk]Compagnon</multi>', '<multi>[ar]مرافق الخطوات الأولى في SPIP[de]Assistent für die ersten Schritte mit SPIP[en]First steps wizard with SPIP[eo]Helpanto por SPIP-komencantoj[es]Asistente para dar los primeros pasos con SPIP[fa]همراه اول با اسپيپ نيست[fr]Assistant de premiers pas avec SPIP[it]Assistente per il primo utilizzo di SPIP[lb]Assistent fir déi éischt Schrëtt mat SPIP[nl]Assistent van de eerste steppen met SPIP[pt_br]Assistente de primeiros passos do SPIP[ru]Начальная помощь при работе со SPIP[sk]Sprievodca pri prvých krokoch so SPIPom</multi>', 'divers', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(14, 'COMPRESSEUR', 'Compresseur', '<multi>[ar]ضغط أوراق الأنماط ورموز جافاسكريبت[de]Kompression von CSS und Javascript-Code[en]CSS and javascript compression[eo]Densigo css kaj javascript[es]Compresión de css y javascript[fa]فشرده سازي سي.اس.اس‌ها و جاوااسكريبت[fr]Compression des css et javascript[it]Compressione di CSS e javascript[lb]Compressioun vun CSS a Javascript[nl]Compressie van css en javascript[pt_br]Compressão dos css e javascript[ru]Плагин для сжатия CSS и Javascript файлов[sk]Kompresia css a javascriptu</multi>', 'performance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(15, 'COUTEAU_SUISSE', '<multi>[en]Swiss Knife[fr]Le Couteau Suisse[ca]El Ganivet Suís[ar]سكين الجيب[nl]Het Zwitserland Mes[gl]A navalla suíza[es]La Navaja Suiza[de]Schweizer Taschenmesser[tr]İsviçre Çakısı[br]Ar Gontell Suis[pt_br]Canivete Suíço[ast]La Navaya Suiza[gl]A navalla suíza[ro]Cuţitul Elveţian[it]Coltellino Svizzero</multi>', '<multi>[en]Lots of new and useful features for your site![fr]Plein de petites fonctionnalités nouvelles et utiles à votre site ![sk]Veľa malých nových a užitočných funkcií pre vašu stránku!</multi>', 'maintenance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(16, 'DUMP', 'Dump', '<multi>[ar]نسخ احتياطي لقاعدة بيانات SPIP واسترجاعها[de]Sicherung und Wiederherstellung der  SPIP-Datenbank[en]Backup and restore database SPIP[eo]Savkopio kaj restaŭro de la datumbazo SPIP[es]Copia de seguridad y recuperación de la base de datos de SPIP[fa]بك‌آپ و ذخيره سازي پايگاه داده‌‌هاي اسپيپ[fr]Sauvegarde et restauration de la base SPIP[it]Backup e ripristino del database di SPIP[lb]Backup an Zeréckspillen vun der SPIP-Datebank[nl]Back-up en restauratie van de SPIP database[pt_br]Cópia de segurança e restauro da base SPIP[ru]Резервное копирование[sk]Záloha a obnovenie databázy SPIPu</multi>', 'maintenance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(17, 'EOLECAS', 'EoleCas', '', 'maintenance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(18, 'IMAGES', 'Images', '<multi>[ar]مرشحات معالجة الصور والألوان[de]Bild- und Farbfilter[en]Images processing and colors filters[es]Filtros de transformación de imágenes y de colores[fa]فيلترهاي پردازش تصوير‌ها و رنگ‌ها[fr]Filtres de transformation d''images et de couleurs[it]Filtri di trasformazione delle immagini e dei colori[lb]Ännerungs-Filteren fir d''Biller an d''Faarwen[nl]Beeld en kleur transformatie filters[pt_br]Filtros de transformação de imagens e de cores[ru]Набор фильтров по работе с изображениями[sk]Filtre na transformáciu obrázkov a farieb</multi>', 'multimedia', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(19, 'FORUM', 'Forum', '<multi>[ar]إدارة المنتديات الخاصة والعمومي في SPIP[de]Verwaltung der privaten und öffentlichen SPIP-Foren[en]Management of private and public forums in SPIP[es]Administración de los foros privados y públicos en SPIP[fa]مديريت سخنگاه‌هاي خصوصي و همگاني در اسپيپ[fr]Gestion des forums privés et publics dans SPIP[it]Gestione dei forum privati e pubblici di SPIP[lb]Beaarbechte vun de privaten an ëffentleche Forumen am SPIP[nl]Beheer van de private en openbare SPIP fora[pt_br]Gerenciamento dos fóruns privados e públicos do SPIP[ru]Управление форумами и комментариями[sk]Riadenie súkromných aj verejných diskusných fór v SPIPe</multi>', 'communication', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(20, 'JQUERYUI', 'jQuery UI', '<multi>[ar]إنتاج الحركة والمؤثرات وتوسيع أدواة jQuery[de]Animationen, Effekte und Widgets für jQuery[en]Animations, effects and jQuery widgets[es]Animaciones, efetos y widgets jQuery[fr]Animations, effets et widgets jQuery[it]Animazioni, effetti e widget jQuery[lb]jQuery Animatiounen, Effekter a Widgets[nl]jQuery Animatie, effecten en widgets[ru]Анимация, эффекты и плагины jQuery[sk]Animácie, efekty a widgety jQuery</multi>', 'outil', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(21, 'MEDIABOX', '<multi>[ar]صندوق الفرجة[de]MediaBox[en]MediaBox[es]MediaBox[fa]مدياباكس[fr]MediaBox[it]MediaBox[lb]MediaBox[nl]MediaBox[ru]MediaBox[sk]Multimediálny box</multi>', '<multi>[ar]صندوق الفرجة[de]Multimedia-Box[en]Media box[es]Caja multimedia[fa]صندوق چندرسانه‌اي[fr]Boîte multimédia[it]Media box[lb]Mediabox[nl]Multimedia Box[ru]Медиабокс[sk]Multimediálny box</multi>', 'multimedia', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(22, 'MEDIAS', 'Medias', '<multi>[ar]أدارة الوسائط المتعددة في SPIP[de]Medienverwaltung in  SPIP[en]SPIP''s media management[es]Gestión de documentos multimedia en SPIP[fa]مديريت رسانه‌ها دراسپيپ[fr]Gestion des médias dans SPIP[it]Gestione dei media di SPIP[lb]Gestioun vun de Medien am SPIP[nl]Beheer van de digitale documenten in SPIP[pt_br]Gerenciamento de mídias do SPIP[ru]Управление медиа файлами[sk]Správa multimédií v SPIPe</multi>', 'multimedia', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(23, 'MOTS', 'Mots', '<multi>[ar]إدارة المفاتيح ومجموعات المفاتيح في SPIP[de]Verwaltung von Schlagworten und Schlagwortgruppen mit  SPIP[en]Management of keywords and keywords groups in SPIP[es]Administración de las palabras clave y los grupos de palabras clave en SPIP[fa]مديريت واژه‌ها و گروه‌واژه‌ها در اسپيپ[fr]Gestion des mots et groupes de mots dans SPIP[it]Gestione delle parole e dei gruppi di parole di SPIP[lb]Gestioun vun de Schlësselwierder a Gruppen am SPIP[nl]Beheer van terfwoorden en groepen van trefwoorden in SPIP[pt_br]Gerenciamento de palavras-chave e grupos de palavras chave do SPIP[ru]Настройка групп ключей и ключей в SPIP[sk]Riadenie kľúčových slov a skupín kľúčových slov v SPIPe</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(24, 'MSIE_COMPAT', '<multi>[ar]دعم برامج التصفح القديمة[de]Unterstützung älterer Webbrowser[en]Support of older browsers[es]Soporte para navegadores antiguos[fa]پشتيباني از مرورگر‌هاي قديمي[fr]Support vieux navigateurs[it]Supporto ai vecchi browser[lb]Ënnerstëtzung vun ale Browseren[nl]Support voor oudere browsers[pt_br]Suporte a navegadores antigos[ru]Поддержка старых броузеров[sk]Podpora starších prehliadačov</multi>', '<multi>[ar]ملفات PNG وعلامات أوراق الأنماط في برامج التصفح القديمة[de]PNG und CSS-Selektoren für alte Browser[en]PNG files and various CSS selectors for old browsers[es]PNG y selectores CSS para los navegadores antiguos[fa]پي.ان.جي و گزينشگر‌هاي سي.اس.اس براي مرورگرهاي قديمي[fr]PNG et sélecteurs CSS pour les vieux navigateurs[it]Immagini in formato PNG e selettori CSS per i vecchi browser[lb]PNG an CSS Selektoren fir déi al Browseren[nl]PNG en CSS-selectors voor ouder browsers[pt_br]PNG e seletores CSS para os navegadores antigos[ru]Поддержка прозрачности PNG и расширенная поддержка селекторов CSS для старых броузеров[sk]PNG a voliče CSS pre staršie prehliadače</multi>', 'outil', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(25, 'ORGANISEUR', 'Organiseur', '<multi>[ar]أدوات عمل تحريري جماعي[de]Werkzeuge für eine Online-Redaktion[en]Collaborative editorial working tools[es]Herramientas de trabajo editorial en grupo[fa]ابزارهاي كار گروه سردبيري[fr]Outils de travail éditorial en groupe[it]Strumenti per il lavoro editoriale in gruppo[lb]Gruppenaarbecht[nl]Samenwerking- en werkinstrument voor de redacteurs[ru]Организация совместной работы[sk]Nástroje na redakčnú prácu v skupine</multi>', 'date', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(26, 'PETITIONS', 'Pétitions', '<multi>[ar]إدارة العرائض في SPIP[de]Verwaltung von Petitionen mit SPIP[en]Petitions management in SPIP[es]Administración de las peticiones en SPIP[fa]مديريت طومارها در اسپيپ[fr]Gestion des pétitions dans SPIP[it]Gestione delle petizioni di SPIP[nl]Beheer van petities in SPIP[pt_br]Gerenciamento de petições do SPIP[ru]Модуль сбора подписей для SPIP[sk]Správa petícií v SPIPe</multi>', 'communication', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(27, 'PORTE_PLUME', '<multi>[ar]الريشة[de]Federhalter[en]Quill[es]Porta pluma[fa]چوب قلم[fr]Porte plume[it]Portapenne[nl]Penhouder[ru]Панель инструментов (Porte plume)[sk]Porte plume</multi>', '<multi>[ar]شريط أدوات لتحسين الكتابة[de]Eine Menüleiste zum Formatieren der Texte[en]A toolbar to enhance your texts[es]Una barra de herramientas para escribir bien[fa]نوار ابزاري براي بهترنويسي[fr]Une barre d''outil pour bien écrire[it]Una barra degli strumenti per scrivere bene[nl]Een penhouder om mooi te schrijven[ru]Дополнительные возможности для текстового редактора[sk]Panel s nástrojmi, vďaka ktorému môžete vylepšiť svoje texty</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(28, 'REVISIONS', 'Révisions', '<multi>[ar]متابعة التغييرات في العناصر التحريرية[de]Änderungen von redaktionellen Inhalten nachvollziehen[en]Tracking changes of editorial objects[es]Seguimiento de las modificaciones de los objetos editoriales[fa]پيگيري اصلاحات موضوع‌هاي سردبيري[fr]Suivi des modifications des objets éditoriaux[it]Il rilevamento delle revisioni sugli oggetti editoriali[lb]Verwaltung vun de Versiounen vun den Objekter[nl]Revisies laat je toe de  veranderingen van de editorialen objecten zien en volgen.[pt_br]Acompanhamento das alterações dos objetos editoriais[ru]История изменений[sk]Sledovanie zmien redakčných objektov</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(29, 'SAFEHTML', 'SafeHTML', '<multi>[en]Forums protection against cross-site scripting[es]Protección de los foros contra el cross-site scripting[fr]Protection des forums contre le cross-site scripting[ru]Защищает форму комментариев от межсайтового скриптинга (XSS)</multi>', 'performance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(30, 'SITES', 'Sites', '<multi>[ar]إدارة المواقع والترخيص في SPIP[de]Verwaltung verlinkter und syndizierter Websites mit  SPIP[en]Management of sites and syndication in SPIP[es]Administración de los sitios y de la sindicación en SPIP[fa]مديريت سايت‌ها و مشترك‌سازي در اسپيپ[fr]Gestion des sites et de la syndication dans SPIP[it]Gestione dei siti e syndication in SPIP[lb]Gestioun vun de Websäiten a Syndicatioun am SPIP[nl]Beheer van de sites en van de Websyndicatie in SPIP[pt_br]Gerenciamento de sites e da sindicação do SPIP[ru]Подключение других сайтов по RSS[sk]Riadenie stránok a syndikácie v SPIPe</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(31, 'SQUELETTES_PAR_RUBRIQUE', 'Squelettes par Rubrique', '<multi>[ar]دعم الصفحات النموذجية ذات اللواحق في SPIP[de]Unterstützung von Skeletten mit Namenszusatz in SPIP[en]Support of suffixed templates in SPIP[es]Implementación de esqueletos con sufijos de SPIP[fa]پشتيباني از اسكليت‌هاي  پسوندي در اسپيپ[fr]Support des squelettes suffixés dans SPIP[it]Supporto di modelli con suffisso in SPIP[lb]Ënnerstëtzung fir Skeletter mat Suffix am SPIP[nl]Ondersteuning van suffix ingewijden skeletten in SPIP[pt_br]Suporte aos gabaritos com sufixo do SPIP[ru]Номерные шаблоны для SPIP[sk]Podpora pripojených šablón v SPIPe</multi>', 'outil', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(32, 'STATS', '<multi>[ar]الإحصاءات[de]Estadísticas[en]Statistics[es]Estadísticas[fa]آمارها[fr]Statistiques[it]Statistiche[lb]Statistiken[nl]Statistieken[pt_br]Estatísticas[ru]Статистика посещений сайта[sk]Štatistiky</multi>', '<multi>[ar]إدارة الإحصاءات في SPIP[de]Administración de las estadísticas de SPIP[en]Statistics management in SPIP[es]Administración de las estadísticas de SPIP[fa]مديريت آمارها در اسپيپ[fr]Gestion des statistiques dans SPIP[it]Gestione delle statistiche di SPIP[lb]Verwalte vun de SPIP-Statistiken[nl]Beheer van de bezoekenstatistieken in SPIP[pt_br]Gerenciamento das estatísticas do SPIP[ru]Плагин по учету статистики посещений вашего сайта[sk]Správa štatistík v SPIPe</multi>', 'statistique', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(33, 'SVP', 'SVP', '<multi>[ar]خادم معلومات الملحقات وتحميلها[de]Informations- und Download-SerVer für Plugins[en]Information and download server of plugins[es]Servidor de información y descarga de plugins[fa]سرور اطلاعات و بارگذاري پلاگين‌ها[fr]SerVeur d''information et de téléchargement des Plugins[it]Informazioni e server per il download dei plugin[nl]SerVer voor het informatie en het downloaden van Plugins[ru]Управление плагинами[sk]Server na zisťovanie informácií o zásuvných moduloch a ich sťahovanie</multi>', 'maintenance', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(34, 'TW', 'TextWheel pour SPIP', '<multi>[ar]إدارة الكتابة في SPIP بواسطة TextWheel[de]Typographie in SPIP mit TextWheel steuern[en]Management of SPIP typography with TextWheel[es]Administración de la tipografía SPIP con TextWheel[fa]مديريت حروف‌نگاري در اسپيپ با چرخ‌نويس[fr]Gestion de la typographie SPIP avec TextWheel[it]Gestione della tipografia di SPIP con TextWheel[lb]Verwalte vun der SPIP-Typografie mat TextWheel[nl]Beheer van de SPIP typografie dank zij TextWheel[ru]Управлять оформлением SPIP c помощью TextWheel[sk]Správa typografie SPIPu s modulom TextWheel</multi>', 'edition', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(35, 'URLS', 'Urls Etendues', '<multi>[ar]إدارة تنوعات عناوين URL ذات المعنى ام لا[de]Verwaltung von URL-Varianten[en]Management of the URL variants, meaningful or not[es]Gestión de las variantes de URL significativas o no[fa]مديريت يو.آر.ال‌هاي مهم يا غيرمهم[fr]Gestion des variantes d''URL signifiantes ou non[it]Gestione delle varianti significative URL, o non[nl]Beheer van de URL varianten (betekenisdragend of niet)[pt_br]Gerenciamento das variantes de URLs significantes ou não[ru]SEO-friendly URLs (ЧПУ ссылки)[sk]Riadenie variantov URL, či už sémantických, alebo nie</multi>', 'statistique', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(36, 'VERTEBRES', '<multi>[ar]الفقرات[de]Wirbelsäule[en]Vertebrae[es]Vértebras[fa]فقرات[fr]Vertèbres[it]Vertèbres[lb]Vertèbres[nl]Wervels[ru]Просмотр SQL таблиц[sk]Vertèbres</multi>', '<multi>[ar]قارئ جداول SQL[de]SQL Tabellen lesen[en]SQL tables reader[es]Lector de tablas SQL[fa]خواندن جدول‌هاي اس.كيو.ال[fr]Lecteur de tables SQL[it]Lettore di tabelle SQL[lb]Lieser vir SQL-Tafelen[nl]Een SQL-tabels lezer[ru]Просмотр SQL таблиц[sk]Čítanie tabuliek SQL</multi>', 'outil', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(47, 'ESCAL', 'Escal', '<multi>[fr]Squelette 2 ou 3 colonnes composé de nombreuses noisettes</multi>', 'squelette', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `spip_referers`
--

CREATE TABLE IF NOT EXISTS `spip_referers` (
  `referer_md5` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `referer` varchar(255) DEFAULT NULL,
  `visites` int(10) unsigned NOT NULL,
  `visites_jour` int(10) unsigned NOT NULL,
  `visites_veille` int(10) unsigned NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`referer_md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_referers`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_referers_articles`
--

CREATE TABLE IF NOT EXISTS `spip_referers_articles` (
  `id_article` int(10) unsigned NOT NULL,
  `referer_md5` bigint(20) unsigned NOT NULL,
  `referer` varchar(255) NOT NULL DEFAULT '',
  `visites` int(10) unsigned NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_article`,`referer_md5`),
  KEY `referer_md5` (`referer_md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_referers_articles`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_resultats`
--

CREATE TABLE IF NOT EXISTS `spip_resultats` (
  `recherche` char(16) NOT NULL DEFAULT '',
  `id` int(10) unsigned NOT NULL,
  `points` int(10) unsigned NOT NULL DEFAULT '0',
  `table_objet` varchar(30) NOT NULL DEFAULT '',
  `serveur` char(16) NOT NULL DEFAULT '',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_resultats`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_rubriques`
--

CREATE TABLE IF NOT EXISTS `spip_rubriques` (
  `id_rubrique` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_parent` bigint(21) NOT NULL DEFAULT '0',
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `texte` longtext NOT NULL,
  `id_secteur` bigint(21) NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lang` varchar(10) NOT NULL DEFAULT '',
  `langue_choisie` varchar(3) DEFAULT 'non',
  `statut_tmp` varchar(10) NOT NULL DEFAULT '0',
  `date_tmp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `profondeur` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_rubrique`),
  KEY `lang` (`lang`),
  KEY `id_parent` (`id_parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_rubriques`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_signatures`
--

CREATE TABLE IF NOT EXISTS `spip_signatures` (
  `id_signature` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_petition` bigint(21) NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nom_email` text NOT NULL,
  `ad_email` text NOT NULL,
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `message` mediumtext NOT NULL,
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_signature`),
  KEY `id_petition` (`id_petition`),
  KEY `statut` (`statut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_signatures`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_syndic`
--

CREATE TABLE IF NOT EXISTS `spip_syndic` (
  `id_syndic` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_rubrique` bigint(21) NOT NULL DEFAULT '0',
  `id_secteur` bigint(21) NOT NULL DEFAULT '0',
  `nom_site` text NOT NULL,
  `url_site` text NOT NULL,
  `url_syndic` text NOT NULL,
  `descriptif` text NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `syndication` varchar(3) NOT NULL DEFAULT '',
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_syndic` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_index` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `moderation` varchar(3) DEFAULT 'non',
  `miroir` varchar(3) DEFAULT 'non',
  `oubli` varchar(3) DEFAULT 'non',
  `resume` varchar(3) DEFAULT 'oui',
  PRIMARY KEY (`id_syndic`),
  KEY `id_rubrique` (`id_rubrique`),
  KEY `id_secteur` (`id_secteur`),
  KEY `statut` (`statut`,`date_syndic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_syndic`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_syndic_articles`
--

CREATE TABLE IF NOT EXISTS `spip_syndic_articles` (
  `id_syndic_article` bigint(21) NOT NULL AUTO_INCREMENT,
  `id_syndic` bigint(21) NOT NULL DEFAULT '0',
  `titre` text NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lesauteurs` text NOT NULL,
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `statut` varchar(10) NOT NULL DEFAULT '0',
  `descriptif` text NOT NULL,
  `lang` varchar(10) NOT NULL DEFAULT '',
  `url_source` tinytext NOT NULL,
  `source` tinytext NOT NULL,
  `tags` text NOT NULL,
  PRIMARY KEY (`id_syndic_article`),
  KEY `id_syndic` (`id_syndic`),
  KEY `statut` (`statut`),
  KEY `url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `spip_syndic_articles`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_types_documents`
--

CREATE TABLE IF NOT EXISTS `spip_types_documents` (
  `extension` varchar(10) NOT NULL DEFAULT '',
  `titre` text NOT NULL,
  `descriptif` text NOT NULL,
  `mime_type` varchar(100) NOT NULL DEFAULT '',
  `inclus` enum('non','image','embed') NOT NULL DEFAULT 'non',
  `upload` enum('oui','non') NOT NULL DEFAULT 'oui',
  `media_defaut` varchar(10) NOT NULL DEFAULT 'file',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`extension`),
  KEY `inclus` (`inclus`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_types_documents`
--

INSERT INTO `spip_types_documents` (`extension`, `titre`, `descriptif`, `mime_type`, `inclus`, `upload`, `media_defaut`, `maj`) VALUES
('jpg', 'JPEG', '', 'image/jpeg', 'image', 'oui', 'image', '2013-06-25 09:07:41'),
('png', 'PNG', '', 'image/png', 'image', 'oui', 'image', '2013-06-25 09:07:41'),
('gif', 'GIF', '', 'image/gif', 'image', 'oui', 'image', '2013-06-25 09:07:41'),
('bmp', 'BMP', '', 'image/x-ms-bmp', 'image', 'oui', 'image', '2013-06-25 09:07:41'),
('tif', 'TIFF', '', 'image/tiff', 'embed', 'oui', 'image', '2013-06-25 09:07:41'),
('aac', 'Advanced Audio Coding', '', 'audio/mp4a-latm', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('ac3', 'AC-3 Compressed Audio', '', 'audio/x-aac', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('aifc', 'Compressed AIFF Audio', '', 'audio/x-aifc', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('aiff', 'AIFF', '', 'audio/x-aiff', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('amr', 'Adaptive Multi-Rate Audio', '', 'audio/amr', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('ape', 'Monkey''s Audio File', '', 'audio/x-monkeys-audio', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('asf', 'Windows Media', '', 'video/x-ms-asf', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('avi', 'AVI', '', 'video/x-msvideo', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('anx', 'Annodex', '', 'application/annodex', 'embed', 'oui', 'file', '2013-06-25 09:07:41'),
('axa', 'Annodex Audio', '', 'audio/annodex', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('axv', 'Annodex Video', '', 'video/annodex', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('dv', 'Digital Video', '', 'video/x-dv', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('f4a', 'Audio for Adobe Flash Player', '', 'audio/mp4', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('f4b', 'Audio Book for Adobe Flash Player', '', 'audio/mp4', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('f4p', 'Protected Video for Adobe Flash Player', '', 'video/mp4', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('f4v', 'Video for Adobe Flash Player', '', 'video/mp4', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('flac', 'Free Lossless Audio Codec', '', 'audio/x-flac', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('flv', 'Flash Video', '', 'video/x-flv', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('m2p', 'MPEG-PS', '', 'video/MP2P', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('m2ts', 'BDAV MPEG-2 Transport Stream', '', 'video/MP2T', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('m4a', 'MPEG4 Audio', '', 'audio/mp4a-latm', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('m4b', 'MPEG4 Audio', '', 'audio/mp4a-latm', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('m4p', 'MPEG4 Audio', '', 'audio/mp4a-latm', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('m4r', 'iPhone Ringtone', '', 'audio/aac', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('m4u', 'MPEG4 Playlist', '', 'video/vnd.mpegurl', 'non', 'oui', 'video', '2013-06-25 09:07:41'),
('m4v', 'MPEG4 Video', '', 'video/x-m4v', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mid', 'Midi', '', 'audio/midi', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('mka', 'Matroska Audio', '', 'audio/mka', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('mkv', 'Matroska Video', '', 'video/mkv', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mng', 'MNG', '', 'video/x-mng', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mov', 'QuickTime', '', 'video/quicktime', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mp3', 'MP3', '', 'audio/mpeg', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('mp4', 'MPEG4', '', 'application/mp4', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mpc', 'Musepack', '', 'audio/x-musepack', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('mpg', 'MPEG', '', 'video/mpeg', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('mts', 'AVCHD MPEG-2 transport stream', '', 'video/MP2T', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('oga', 'Ogg Audio', '', 'audio/ogg', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('ogg', 'Ogg Vorbis', '', 'audio/ogg', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('ogv', 'Ogg Video', '', 'video/ogg', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('ogx', 'Ogg Multiplex', '', 'application/ogg', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('qt', 'QuickTime', '', 'video/quicktime', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('ra', 'RealAudio', '', 'audio/x-pn-realaudio', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('ram', 'RealAudio', '', 'audio/x-pn-realaudio', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('rm', 'RealAudio', '', 'audio/x-pn-realaudio', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('spx', 'Ogg Speex', '', 'audio/ogg', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('svg', 'Scalable Vector Graphics', '', 'image/svg+xml', 'embed', 'oui', 'image', '2013-06-25 09:07:41'),
('svgz', 'Compressed Scalable Vector Graphic', '', 'image/svg+xml', 'embed', 'oui', 'image', '2013-06-25 09:07:41'),
('swf', 'Flash', '', 'application/x-shockwave-flash', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('ts', 'MPEG transport stream', '', 'video/MP2T', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('wav', 'WAV', '', 'audio/x-wav', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('webm', 'WebM', '', 'video/webm', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('wma', 'Windows Media Audio', '', 'audio/x-ms-wma', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('wmv', 'Windows Media Video', '', 'video/x-ms-wmv', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('y4m', 'YUV4MPEG2', '', 'video/x-raw-yuv', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('3gp', '3rd Generation Partnership Project', '', 'video/3gpp', 'embed', 'oui', 'video', '2013-06-25 09:07:41'),
('3ga', '3GP Audio File', '', 'audio/3ga', 'embed', 'oui', 'audio', '2013-06-25 09:07:41'),
('7z', '7 Zip', '', 'application/x-7z-compressed', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ai', 'Adobe Illustrator', '', 'application/illustrator', 'non', 'oui', 'image', '2013-06-25 09:07:41'),
('abw', 'Abiword', '', 'application/abiword', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('asx', 'Advanced Stream Redirector', '', 'video/x-ms-asf', 'non', 'oui', 'video', '2013-06-25 09:07:41'),
('bib', 'BibTeX', '', 'application/x-bibtex', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('bin', 'Binary Data', '', 'application/octet-stream', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('blend', 'Blender', '', 'application/x-blender', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('bz2', 'BZip', '', 'application/x-bzip2', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('c', 'C source', '', 'text/x-csrc', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('csl', 'Citation Style Language', '', 'application/xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('css', 'Cascading Style Sheet', '', 'text/css', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('csv', 'Comma Separated Values', '', 'text/csv', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('deb', 'Debian', '', 'application/x-debian-package', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('doc', 'Word', '', 'application/msword', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('dot', 'Word Template', '', 'application/msword', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('djvu', 'DjVu', '', 'image/vnd.djvu', 'non', 'oui', 'image', '2013-06-25 09:07:41'),
('dvi', 'LaTeX DVI', '', 'application/x-dvi', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('emf', 'Enhanced Metafile', '', 'image/x-emf', 'non', 'oui', 'image', '2013-06-25 09:07:41'),
('enl', 'EndNote Library', '', 'application/octet-stream', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ens', 'EndNote Style', '', 'application/octet-stream', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('eps', 'PostScript', '', 'application/postscript', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('epub', 'EPUB', '', 'application/epub+zip', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('gpx', 'GPS eXchange Format', '', 'application/gpx+xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('gz', 'GZ', '', 'application/x-gzip', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('h', 'C header', '', 'text/x-chdr', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('html', 'HTML', '', 'text/html', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('jar', 'Java Archive', '', 'application/java-archive', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('kml', 'Keyhole Markup Language', '', 'application/vnd.google-earth.kml+xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('kmz', 'Google Earth Placemark File', '', 'application/vnd.google-earth.kmz', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('lyx', 'Lyx file', '', 'application/x-lyx', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('m3u', 'M3U Playlist', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('m3u8', 'M3U8 Playlist', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('mathml', 'MathML', '', 'application/mathml+xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('mbtiles', 'MBTiles', '', 'application/x-sqlite3', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pas', 'Pascal', '', 'text/x-pascal', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pdf', 'PDF', '', 'application/pdf', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pgn', 'Portable Game Notation', '', 'application/x-chess-pgn', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pls', 'Playlist', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ppt', 'PowerPoint', '', 'application/vnd.ms-powerpoint', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pot', 'PowerPoint Template', '', 'application/vnd.ms-powerpoint', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ps', 'PostScript', '', 'application/postscript', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('psd', 'Photoshop', '', 'image/x-photoshop', 'non', 'oui', 'image', '2013-06-25 09:07:41'),
('rar', 'WinRAR', '', 'application/x-rar-compressed', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('rdf', 'Resource Description Framework', '', 'application/rdf+xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ris', 'RIS', '', 'application/x-research-info-systems', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('rpm', 'RedHat/Mandrake/SuSE', '', 'application/x-redhat-package-manager', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('rtf', 'RTF', '', 'application/rtf', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sdc', 'StarOffice Spreadsheet', '', 'application/vnd.stardivision.calc', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sdd', 'StarOffice Presentation', '', 'application/vnd.stardivision.impress', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sdw', 'StarOffice Writer document', '', 'application/vnd.stardivision.writer', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sit', 'Stuffit', '', 'application/x-stuffit', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sla', 'Scribus', '', 'application/x-scribus', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('srt', 'SubRip Subtitle', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ssa', 'SubStation Alpha Subtitle', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sxc', 'OpenOffice.org Calc', '', 'application/vnd.sun.xml.calc', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sxi', 'OpenOffice.org Impress', '', 'application/vnd.sun.xml.impress', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sxw', 'OpenOffice.org', '', 'application/vnd.sun.xml.writer', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('tar', 'Tar', '', 'application/x-tar', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('tex', 'LaTeX', '', 'text/x-tex', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('tgz', 'TGZ', '', 'application/x-gtar', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('torrent', 'BitTorrent', '', 'application/x-bittorrent', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ttf', 'TTF Font', '', 'application/x-font-ttf', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('txt', 'Texte', '', 'text/plain', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('usf', 'Universal Subtitle Format', '', 'application/xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xcf', 'GIMP multi-layer', '', 'application/x-xcf', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xls', 'Excel', '', 'application/vnd.ms-excel', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xlt', 'Excel Template', '', 'application/vnd.ms-excel', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('wmf', 'Windows Metafile', '', 'image/x-emf', 'non', 'oui', 'image', '2013-06-25 09:07:41'),
('wpl', 'Windows Media Player Playlist', '', 'application/vnd.ms-wpl', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xspf', 'XSPF', '', 'application/xspf+xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xml', 'XML', '', 'application/xml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('yaml', 'YAML', '', 'text/yaml', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('zip', 'Zip', '', 'application/zip', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odt', 'OpenDocument Text', '', 'application/vnd.oasis.opendocument.text', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ods', 'OpenDocument Spreadsheet', '', 'application/vnd.oasis.opendocument.spreadsheet', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odp', 'OpenDocument Presentation', '', 'application/vnd.oasis.opendocument.presentation', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odg', 'OpenDocument Graphics', '', 'application/vnd.oasis.opendocument.graphics', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odc', 'OpenDocument Chart', '', 'application/vnd.oasis.opendocument.chart', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odf', 'OpenDocument Formula', '', 'application/vnd.oasis.opendocument.formula', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odb', 'OpenDocument Database', '', 'application/vnd.oasis.opendocument.database', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odi', 'OpenDocument Image', '', 'application/vnd.oasis.opendocument.image', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('odm', 'OpenDocument Text-master', '', 'application/vnd.oasis.opendocument.text-master', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ott', 'OpenDocument Text-template', '', 'application/vnd.oasis.opendocument.text-template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ots', 'OpenDocument Spreadsheet-template', '', 'application/vnd.oasis.opendocument.spreadsheet-template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('otp', 'OpenDocument Presentation-template', '', 'application/vnd.oasis.opendocument.presentation-template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('otg', 'OpenDocument Graphics-template', '', 'application/vnd.oasis.opendocument.graphics-template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('cls', 'LaTeX Class', '', 'text/x-tex', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('sty', 'LaTeX Style Sheet', '', 'text/x-tex', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('docm', 'Word', '', 'application/vnd.ms-word.document.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('docx', 'Word', '', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('dotm', 'Word template', '', 'application/vnd.ms-word.template.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('dotx', 'Word template', '', 'application/vnd.openxmlformats-officedocument.wordprocessingml.template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('potm', 'Powerpoint template', '', 'application/vnd.ms-powerpoint.template.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('potx', 'Powerpoint template', '', 'application/vnd.openxmlformats-officedocument.presentationml.template', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ppam', 'Powerpoint addin', '', 'application/vnd.ms-powerpoint.addin.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ppsm', 'Powerpoint slideshow', '', 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('ppsx', 'Powerpoint slideshow', '', 'application/vnd.openxmlformats-officedocument.presentationml.slideshow', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pptm', 'Powerpoint', '', 'application/vnd.ms-powerpoint.presentation.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('pptx', 'Powerpoint', '', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xlam', 'Excel', '', 'application/vnd.ms-excel.addin.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xlsb', 'Excel binary', '', 'application/vnd.ms-excel.sheet.binary.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xlsm', 'Excel', '', 'application/vnd.ms-excel.sheet.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xlsx', 'Excel', '', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xltm', 'Excel template', '', 'application/vnd.ms-excel.template.macroEnabled.12', 'non', 'oui', 'file', '2013-06-25 09:07:41'),
('xltx', 'Excel template', '', 'application/vnd.openxmlformats-officedocument.spreadsheetml.template', 'non', 'oui', 'file', '2013-06-25 09:07:41');

-- --------------------------------------------------------

--
-- Structure de la table `spip_urls`
--

CREATE TABLE IF NOT EXISTS `spip_urls` (
  `id_parent` bigint(21) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `type` varchar(15) NOT NULL DEFAULT 'article',
  `id_objet` bigint(21) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `segments` smallint(3) NOT NULL DEFAULT '1',
  `perma` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_parent`,`url`),
  KEY `type` (`type`,`id_objet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_urls`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_versions`
--

CREATE TABLE IF NOT EXISTS `spip_versions` (
  `id_version` bigint(21) NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL DEFAULT '0',
  `objet` varchar(25) NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_auteur` varchar(23) NOT NULL DEFAULT '',
  `titre_version` text NOT NULL,
  `permanent` char(3) NOT NULL DEFAULT '',
  `champs` text NOT NULL,
  PRIMARY KEY (`id_version`,`id_objet`,`objet`),
  KEY `id_version` (`id_version`),
  KEY `objet` (`objet`),
  KEY `id_objet` (`id_objet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_versions`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_versions_fragments`
--

CREATE TABLE IF NOT EXISTS `spip_versions_fragments` (
  `id_fragment` int(10) unsigned NOT NULL DEFAULT '0',
  `version_min` int(10) unsigned NOT NULL DEFAULT '0',
  `version_max` int(10) unsigned NOT NULL DEFAULT '0',
  `id_objet` bigint(21) NOT NULL,
  `objet` varchar(25) NOT NULL DEFAULT '',
  `compress` tinyint(4) NOT NULL,
  `fragment` longblob,
  PRIMARY KEY (`id_objet`,`objet`,`id_fragment`,`version_min`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_versions_fragments`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_visites`
--

CREATE TABLE IF NOT EXISTS `spip_visites` (
  `date` date NOT NULL,
  `visites` int(10) unsigned NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_visites`
--


-- --------------------------------------------------------

--
-- Structure de la table `spip_visites_articles`
--

CREATE TABLE IF NOT EXISTS `spip_visites_articles` (
  `date` date NOT NULL,
  `id_article` int(10) unsigned NOT NULL,
  `visites` int(10) unsigned NOT NULL DEFAULT '0',
  `maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date`,`id_article`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `spip_visites_articles`
--

