#-*- coding: utf-8 -*-
##########################################################################
# Eole NG - 2013
# Copyright Pole de competence Eole (Ministere Education - Academie Dijon)
# Licence CeCill cd /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# spip.py
#
# Creation de la base de donnees de spip
##########################################################################

"""
    Creation de la base spip
"""
from eolesql.db_test import db_exists, test_var

SPIP_TABLEFILENAMES = ['/usr/share/eole/mysql/spip/gen/spip-create.sql']

def test():
    """
        test  l'existence de la base spip
    """
    return test_var('activer_spip') and not db_exists('spip')

conf_dict = dict(filenames=SPIP_TABLEFILENAMES, test=test)
