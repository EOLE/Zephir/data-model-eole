# -*- coding: utf-8 -*-
"""
    Update de la base spip à chaque reconfigure
"""

from eolesql.db_test import test_var

DB = "spip"
FILENAMES = ['/usr/share/eole/mysql/spip/updates/spip-update.sql']

def test_response(result=None):
    """
        Renvoie True si spip est activé
    """
    return test_var('activer_spip')

conf_dict = dict(db=DB, expected_response=test_response,filenames=FILENAMES, version='Mise à jour de spip')
