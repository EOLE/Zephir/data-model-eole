#!/bin/bash

##################################################################################
#
# VARIABLES GLOBALES
#
returnFile=/var/run/odaim
pidfile=/var/run/odaim.pid
mainDir='/var/www/html/odaim'
logDir='/var/log/rsyslog/local/odaim'

usage(){
  echo "Usage : "
  echo "   $(basename ${0}): -t prevalidation||diffusion  [-i immediat||crontab]"
  exit 127
}


temporisation() {
	# permet d'introduire une temporisation aléatoire suivant le principe :
	# démarrage possible de 21h à 5h du matin soit une période de 8h convertie ici en minutes
	periode=$((8*60))
	# on génère une durée aléatoire d'attente inférieure à 8h
	delais=$(($RANDOM%${periode}))
	# on lance la temporisation
	sleep ${delais}m
}

verifEspace() {
	# on vérifie que l'espace restant couvre la valeur Zephir
	# espace déjà occupé par odaim
	OCCUPE=$(du -k /var/www/html/odaim | awk -F ' ' '{print $1}'	)
	# espace disque restant
	RESTE=$(df -k | grep "^/home"| awk -F' ' '{print $4}' | cut -d'%' -f1)
	# espace accepté pour ODAIM
	VOLMAX=%%wapt_volumetrie

	# on vérifie si on peut lancer la synchronisation
	if [ $((${VOLMAX}-${OCCUPE})) -lt ${RESTE} ]
	then
		# pas assez de place restante pour couvrir la volumétrie
		exit 0
	fi
}

synchronise() {
	echo "****************************************" >> ${logDir}/${logfile}
	echo "Début de synchronisation RSYNC en mode ${typeSynchro}" >> ${logDir}/${logfile}
  date >> ${logDir}/${logfile}

	# on lance le rsync
	synchro=$(rsync -avL ${serveur}::depot_odaim_%%wapt_suffixe_service ${destDir})
	retour=$?
	if [ ${retour} -ne "0" ]
	then
		echo "Erreur de synchronisation RSYNC" >> ${logDir}/${logfile}
		exit 1
	fi
	echo "Fin de synchronisation RSYNC en mode ${typeSynchro}" >> ${logDir}/${logfile}
  date >> ${logDir}/${logfile}
	echo "****************************************" >> ${logDir}/${logfile}
}

unicite() {
	# on vérifie qu'il n'y a pas de processus de synchronisation en cours
	processus=$(cat ${pidfile})

	oldsynchro=$(ps -edf|grep ${processus})
	oldsynchro=$?
	if [ ${oldsynchro} -ne "0" ]
	then
		# un processus de synchro est toujours en cours
		kill -9 ${processus}
  fi
}


#######################################################
## main

while getopts ":t:i:" opt; do
	case $opt in
    t)
        typeSynchro="${OPTARG}"
    ;;
   	i)
				lancement="${OPTARG}"
    ;;
    *)
      echo "Option invalide: -$OPTARG" >&2
      exit 1
      ;;
  esac
done
[[ -z ${typeSynchro} ]] && usage
[[ -z ${lancement} ]] && usage



case ${typeSynchro} in
	diffusion)
		serveur=%%wapt_serveur_national_diffusion
		logfile=%%wapt_diffusion_log
		destDir="${mainDir}/depot_diff/"
	;;
	prevalidation)
		serveur=%%wapt_serveur_national_prevalidation
		logfile=%%wapt_prevalidation_log
		destDir="${mainDir}/depot_preval/"
	;;
esac

if [ ${lancement} == "immediat" ]
then
	echo "----------------------------- Début de synchronisation du dépot ${typeSynchro} -------------------------"
	cmd=$(synchronise)
	if [ $? -ne "0" ]
	then
		echo "-------------------- ERREUR de synchronisation du dépôt ${typeSynchro} -------------------------"
		echo "Consulter les logs sous ${logDir}/${logfile}"
		exit 1
	else
		echo "----------------------------- Fin de synchronisation du dépot ${typeSynchro} -------------------------"
	fi
elif [ ${lancement} == "crontab" ]
then
	# mode crontab
	cmd=$(synchronise)
else
	echo "option ${lancement} inconnue" >> ${logDir}/${logfile}
fi
