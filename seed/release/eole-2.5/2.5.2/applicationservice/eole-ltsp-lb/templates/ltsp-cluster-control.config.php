<?php
    $CONFIG['save'] = "Save";
    $CONFIG['lang'] = "fr"; #Language for the interface (en and fr are supported"
    $CONFIG['charset'] = "UTF-8";
    $CONFIG['use_https'] = "false"; #Force https
    $CONFIG['terminal_auth'] = "false";
    $CONFIG['db_server'] = "localhost"; #Hostname of the database server
    $CONFIG['db_user'] = "eclair"; #Username to access the database
    $CONFIG['db_password'] = "=PASSWORD="; #Password to access the database
    $CONFIG['db_name'] = "eclair"; #Database name
    $CONFIG['db_type'] = "postgres"; #Database type (only postgres is supported)
    %if %%activer_client_ldap != "non"
        $CONFIG['auth_name'] = "LDAPAuth";
        $CONFIG['ldap_host'] = "ldap://%%adresse_ip_ldap";
        $CONFIG['ldap_basedn'] = "o=gouv,c=fr";
        $CONFIG['ldap_group'] = "gidnumber";
        $CONFIG['ldap_binddn'] = "";
        $CONFIG['ldap_bindpwd'] = "";
        $CONFIG['ldap_uidAttribute'] = "uid";
        $CONFIG['ldap_version'] = "3";
    %else
        $CONFIG['auth_name'] = "EmptyAuth";
    %end if
    $CONFIG['loadbalancer'] = "%%adresse_ip_ltsp_link"; #Hostname of the loadbalancer
    $CONFIG['first_setup_lock'] = "TRUE";
    $CONFIG['printer_servers'] = array("cups.yourdomain.com"); #Hostname(s) of your print servers
    $CONFIG['rootInstall'] = "/usr/share/ltsp-cluster-control/Admin/";
?>
