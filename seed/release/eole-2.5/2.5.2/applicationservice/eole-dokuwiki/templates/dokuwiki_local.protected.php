<?php


$conf['savedir']     = '/home/www-data/var/www/html/dokuwiki/data';         //where to store all the files

/* Authentication Options - read http://www.splitbrain.org/dokuwiki/wiki:acl */

$conf['useacl']      = 1;                //Use Access Control Lists to restrict access?
$conf['autopasswd']  = 1;                //autogenerate passwords and email them to user
$conf['authtype']    = 'cas';            //which authentication backend should be used
$conf['passcrypt']   = 'smd5';           //Used crypt method (smd5,md5,sha1,ssha,crypt,mysql,my411)
$conf['defaultgroup']= 'user';           //Default groups new Users are added to
$conf['superuser']   = 'admin';          //The admin can be user or @group or comma separated list user1,@group1,user2
$conf['manager']     = '!!not set!!';    //The manager can be user or @group or comma separated list user1,@group1,user2
$conf['profileconfirm'] = 1;             //Require current password to confirm changes to user profile
$conf['disableactions'] = '';            //comma separated list of actions to disable
$conf['sneaky_index']   = 0;             //check for namespace read permission in index view (0|1) (1 might cause unexpected behavior)
$conf['auth_security_timeout'] = 900;    //time (seconds) auth data is considered valid, set to 0 to recheck on every page view
$conf['securecookie'] = 1;               //never send HTTPS cookies via HTTP

$conf['xmlrpc']      = 0;                //Enable/disable XML-RPC interface
$conf['xmlrpcuser']  = '!!not set!!';    //Restrict XML-RPC access to this groups/users


/* CAS specific configuration */
%if %%is_defined('eolesso_adresse')
$conf['auth']['cas']['server'] = '%%eolesso_adresse';
%else
$conf['auth']['cas']['server'] = 'localhost';
%end if
%if %%is_defined('eolesso_port')
$conf['auth']['cas']['port'] = %%eolesso_port;
%else
$conf['auth']['cas']['port'] = 8443;
%end if
// CAS server root parameter
$conf['auth']['cas']['rootcas'] = '';
// automatically log the user when there is already a CAS session opened
$conf['auth']['cas']['autologin'] = 1;
// validation par la CA : chemin vers la CA ('' => pas de validation)
%if %%is_defined('activer_web_valider_ca') and %%activer_web_valider_ca == 'oui'
%if %%eolesso_ca_location == ''
$conf['auth']['cas']['cacert']='/etc/ssl/certs/ca.crt';
%else
$conf['auth']['cas']['cacert']='%%eolesso_ca_location';
%end if
%else
$conf['auth']['cas']['cacert']='';
%end if
// log out from the CAS server when loggin out from dokuwiki
$conf['auth']['cas']['caslogout'] = 1;
// log out from dokuwiki when loggin out from the CAS server (should work with CASv3, experimental)
%if %%is_defined('cas_send_logout') and %%cas_send_logout == 'oui'
$conf['auth']['cas']['handlelogoutrequest'] = true;
%else
$conf['auth']['cas']['handlelogoutrequest'] = false;
%end if
/* LDAP usual configuration */
$conf['auth']['ldap']['server']      = 'ldap://%%adresse_ip_ldap:389';
%if %%is_defined('nom_academie')
%if %%is_defined('numero_etab')
$conf['auth']['ldap']['usertree']    = 'ou=utilisateurs,ou=%%numero_etab,ou=%%nom_academie,ou=education,o=gouv,c=fr';
$conf['auth']['ldap']['grouptree'] = 'ou=groupes,ou=%%numero_etab,ou=%%nom_academie,ou=education,o=gouv,c=fr';
%else
$conf['auth']['ldap']['usertree']    = 'ou=%%nom_academie,ou=education,o=gouv,c=fr';
$conf['auth']['ldap']['grouptree'] = 'ou=%%nom_academie,ou=education,o=gouv,c=fr';
%end if
%else
$conf['auth']['ldap']['usertree']    = 'ou=education,o=gouv,c=fr';
$conf['auth']['ldap']['grouptree'] = 'ou=education,o=gouv,c=fr';
%end if
$conf['auth']['ldap']['userfilter']  = '(&(objectClass=*)(uid=%{user}))';
$conf['auth']['ldap']['groupfilter'] = '(&(objectClass=posixGroup)(|(cn=%{gid})(memberUid=%{user})))';
%if %%is_defined('activer_themes') and %%activer_themes == 'oui'
$conf['template'] = %%nom_theme;
%else
$conf['template'] = 'eole';
%end if

