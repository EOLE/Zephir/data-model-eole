#!/bin/sh
LC_ALL=fr_FR.UTF-8
export LC_ALL

echo >> %%miroir_log/miroir-mcafee.log
echo "-------------------------------------------------" >> %%miroir_log/miroir-mcafee.log
echo "Mise à jour du "$(date) >> %%miroir_log/miroir-mcafee.log
%if %%type_miroir == 'miroir McAfee DDT'
/usr/bin/lftp -u anonymous,mcafee -e "mirror -e -vvv /%%path_ddt %%miroir_homedir/%%path_ddt; quit" %%source_maj >> %%miroir_log/miroir-mcafee.log 
%else
/usr/bin/lftp -u anonymous,mcafee -e "mirror -e -vvv /%%path_ministere %%miroir_homedir/%%path_ministere; quit" %%source_maj >> %%miroir_log/miroir-mcafee.log 
%end if
exit 0
