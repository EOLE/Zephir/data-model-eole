
# -*- coding: utf-8 -*-

"""Helper for /etc/hosts in LXC containers
"""

def calc_ldap_admin_dn(uid=None, group=None, base_dn=None):
    """
    Return ldap admin dn based on given uid, samba group and base dn.

    :param uid: admin's uid, beginning by uid=
    :type uid: `str`
    :param group: samba workgroup
    :type group: `str`
    :param base_dn: base dn
    :type base_dn: `str`
    :return: full admin dn
    :rtype: `str`

    """

    admin_dn_tmpl = u'{0}{1},ou={1},{2}'
    if uid is None or group is None or base_dn is None:
        admin_dn = None
    else:
        admin_dn = admin_dn_tmpl.format(uid, group.upper(), base_dn)
    return admin_dn

def calc_ldap_suffix(group=None, base_dn=None, valeur=None):
    """
    Return ldap suffix based on samba group and base dn.

    :param group: samba workgroup
    :type group: `str`
    :param base_dn: base dn
    :type base_dn: `str`
    :return: ldap suffix
    :rtype: `str`

    """

    ldap_suffix_tmpl = u'ou={0},{1}'
    if group is None or base_dn is None:
        ldap_suffix = None
    else:
        ldap_suffix = ldap_suffix_tmpl.format(group.upper(), base_dn)
    return ldap_suffix
