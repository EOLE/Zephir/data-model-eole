#!/bin/bash

ldapsearch -x -H ldaps://$(CreoleGet adresse_ip_ldap_distant):636/ -a never -b $(CreoleGet smb_ldap_suffix) ipHostNumber=$1 dn|grep dn:|cut -d "," -f3|cut -d "=" -f 2

exit 0
