#!/bin/bash
#echo retablit les privileges pour le groupe domainadmins

echo "Saisissez le compte administrateur du domaine:"
read u
echo "Saisissez le mot de passe du compte $u : "
read -s a
net  SAM CREATEBUILTINGROUP Administrators 2>/dev/null
net  SAM CREATEBUILTINGROUP Users 2>/dev/null
net rpc group addmem administrators domainadmins  -U $u%$a 2>/dev/null
net rpc rights grant domainadmins SeMachineAccountPrivilege SeAddUsersPrivilege SePrintOperatorPrivilege SeDiskOperatorPrivilege SeTakeOwnershipPrivilege SeBackupPrivilege SeRestorePrivilege -U $u%$a
