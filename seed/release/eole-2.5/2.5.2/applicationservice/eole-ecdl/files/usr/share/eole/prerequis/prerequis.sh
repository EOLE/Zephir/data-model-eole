#!/bin/bash

##########################################################################################
### scripts de vérification des pré-requis avant instanciation d'un eCDL
##########################################################################################
script=$0
scriptPath="/usr/share/eole/prerequis"

[ -e "$scriptPath/.prerequisOk" ] && rm -f $scriptPath/.prerequisOk

# hack parseDico
if [ ! -s '/etc/eole/config.eol' ]; then
        echo "$script:Il faut récupérer ou générer le fichier de configuration .eol"
        exit 1
fi

cd /root

##################################################################################################
##
## fonctions
##

log ()
{
        logger -t EQDOMINST -s -f $LOG "$1" >> $LOG 2>&1
}

test_reseau()
{

#initialisation des variables
#IP du DNS national à pinger
p_ipdnsnat="10.167.160.3"

#Quelques couleurs (rouge test pas glop - vert test réussi)
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"

#fin initialisation des variables

if [ ! -s $scriptPath/exxl_test_reseau ];then
    echo -e "$p_nom: $ROUGE Le fichier de tests réseau $scriptPath/exxl_test_reseau n'existe pas ou est vide=> arrêt de l'installation$NORMAL"
    exit 1
fi

adresse_serveur=$(ifconfig eth0 | grep "inet adr" | cut -d: -f2 | awk '{print $1}')

if [ "$adresse_serveur" != "$(CreoleGet adresse_ip_eth0)" ];then
    echo -e "$p_nom: $ROUGE l'adresse IP du serveur: $adresse_serveur\nne correspond pas à l'adresse IP définie\npour la configuration récupérée du zephir pour ce contrôleur: $(CreoleGet adresse_ip_eth0) $NORMAL"
    exit 1
fi

#Début des tests réseau
. $scriptPath/exxl_test_reseau
#Fin des tests réseau

}


function par_feu_ldap_port
{
#$1 nom du serveur ldap et $2 port utilisé
#option de nc: w timeout de 5s, pas d'E/S de données (scanne)

if `nc -w 5 -z -v $1 $2 >> $LOG 2>&1`;then
    echo "$p_nom:Le pare-feu de la centrale est ouvert de ip:${eq_serv_ip} -> $1 port $2" >> $LOG 2>&1
else
    echo "$p_nom:ATTENTION:Le pare-feu de la Centrale est fermé pour $1 (port $2) à partir de:${eq_serv_ip}" >> $LOG 2>&1
    echo -e "$ROUGE$p_nom: ATTENTION:Le pare-feu de la Centrale est fermé pour $1 (port $2) à partir de:${eq_serv_ip}.$NORMAL"

    PARFEU="KO"
fi
}

test_parefeu()
{

PARFEU="OK" #Par-feu de la Centrale ouvert
# LDAP_MAITRE="ldapma.csac.melanie2.i2" #... :-) LDAP Maître
LDAP_MAITRE="$(CreoleGet adresse_ip_ldap_maitre)"

eq_serv_ldap=$(CreoleGet adresse_ip_ldap_distant)
eq_serv_ip=$(CreoleGet adresse_ip_eth0)

# récupération de l'@IP du ldap Maître

ADR_MAITRE="10.167.128.9"
NOM_LONG_MAITRE="ldapma.m2.e2.rie.gouv.fr"
NOM_COURT_MAITRE="ldapma"

for ip_host in $(CreoleGet adresse_ip_hosts)
  do
      if [ "$(CreoleGet nom_long_hosts)" = "$(CreoleGet adresse_ip_ldap_maitre)" ]; then
                ADR_MAITRE=$ip_host
                NOM_LONG_MAITRE=$(CreoleGet nom_long_hosts)
                NOM_COURT_MAITRE=$(CreoleGet nom_court_hosts)
        fi
  done


# Ajout du serveur ldap maître
[ $(cat /etc/hosts|grep "^$ADR_MAITRE $NOM_LONG_MAITRE $NOM_COURT_MAITRE"|wc -l) -eq 0 ] && echo "$ADR_MAITRE $NOM_LONG_MAITRE $NOM_COURT_MAITRE" >> /etc/hosts

cd /usr/share/eole

echo "$p_nom:Test de l'ouverture du pare-feu de la Centrale (timeout de 5s si pb)"  >> $LOG 2>&1
echo "$p_nom:Test de l'ouverture du pare-feu de la Centrale (timeout de 5s si pb)"

PARFEU=$PARFEU #Par-feu de la Centrale ouvert

#Test l'accès au ldaps smb et master sur le 636
par_feu_ldap_port ${eq_serv_ldap} 636
par_feu_ldap_port $LDAP_MAITRE 636

if [ $PARFEU = "OK" ] ; then
    echo "$p_nom:Le pare-feu de la centrale est ouvert de ${eq_serv_ip} vers l'annuaire LDAP" >> $LOG 2>&1
    echo "$p_nom:Le pare-feu de la centrale est ouvert de ${eq_serv_ip} vers l'annuaire LDAP"
else
   echo "$p_nom: Arrêt de l'instanciation."
   exit 1 #sortie du script avec erreur
fi

}

test_lit_ldap()
{

#Ajout du certificat pour pouvoir accéder à l'annuaire en ldaps
[ $(cat /etc/ldap/ldap.conf|grep -i $(basename $(CreoleGet ldap_ca_cert))|wc -l) -eq 0 ] && echo "TLS_CACERT $(CreoleGet ldap_ca_cert)" >> /etc/ldap/ldap.conf

LIMITETEMPS=30        # Nbre de secondes d'attente avant validation
# eq_serv_ip=`hostname -i`
eq_serv_ip=$(CreoleGet adresse_ip_eth0)

DOM=`$scriptPath/dom_ip_ac.pl $eq_serv_ip`
if [ -n "$DOM" ] ;then # n'est pas nul
    echo "$p_nom:Le domaine $DOM existe, on sort du script et on continue." >> $LOG 2>&1
    echo "$p_nom:Le domaine $DOM existe, on continue."
else
    echo "$p_nom:Aucun domaine trouvé=> sortie avec erreur (arrêt des scripts)" >> $LOG 2>&1
    echo -e "$p_nom: $ROUGE Aucun domaine trouvé $NORMAL => sortie avec erreur (arrêt des scripts)"
    exit 1 #Aucun domaine trouvé=> sortie avec erreur
fi
}


test_ecrit_ldap()
{

eq_pass=$(CreoleGet ecdl_adm_pass_default)

SIG=$(CreoleGet ecdl_sig)
DOM=$eq_serv_domsamba


cd /usr/share/eole

cat > /tmp/test_ecrit_tmp.ldif  << EOF
###################################
# Test d'écriture dans l'annuaire LDAP
###################################
## test_ecrit
dn: ou=test_ecrit,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
objectClass: organizationalUnit
objectClass: top
ou: test_ecrit
EOF

cat > /tmp/test_ecrit_del_tmp.ldif  << EOF
ou=test_ecrit,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
EOF

echo "$p_nom: essai d'écriture dans l'annuaire ldap avec le mot de passe par défaut puis avec le nouveau mot de passe">> $LOG 2>&1
echo "$p_nom: essai d'écriture dans l'annuaire ldap avec le mot de passe par défaut puis avec le nouveau mot de passe"


#Test avec le mot de passe par défaut lors de la 1er boucle
MotDePasse=$eq_pass
#puis avec le nouveau mot de passe dans la 2eme boucle
#Resultat de l'écriture ldaps: OK ça fonctionne (resultat_ecrit_defaut avec le mot de passe par défaut amedee)
resultat_ecrit="OK"
resultat_ecrit_defaut="OK"

for (( i=1; i <= 2 ; i++ ))
do
    #echo "debut de boucle i vaut $i"

   echo "$p_nom:Suppression de ou=test_ecrit au cas où elle existe (ne pas en tenir compte)">> $LOG 2>&1
   ldapdelete -c -v -x -w $MotDePasse -D uid=SambaAdm.$DOM,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://$(CreoleGet adresse_ip_ldap_maitre):636/ -f /tmp/test_ecrit_del_tmp.ldif>> $LOG 2>&1

   #ajout de ou=test_ecrit
   echo "$p_nom:Ajout de ou=test_ecrit">> $LOG 2>&1
   ldapmodify -a -v -x -w $MotDePasse -D uid=SambaAdm.$DOM,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://$(CreoleGet adresse_ip_ldap_maitre):636/ -f /tmp/test_ecrit_tmp.ldif>> $LOG 2>&1

   if [ $? != 0 ] ;then
       echo "$p_nom: l'écriture de ou=test_ecrit dans $DOM a échoué avec un des mdp: les ACL ne sont pas appliquées:arrêt des scripts">> $LOG 2>&1
       resultat_ecrit="KO"
       if [ "$resultat_ecrit_defaut" == "KO" ]; then
           # sortie de l'instanciation si resultat_ecrit_defaut="KO" et resultat_ecrit="KO" (lors de l'exécution de la deuxième boucle)
           echo "$p_nom: l'écriture de ou=test_ecrit dans $DOM a échoué avec un des mdp: les ACL ne sont pas appliquées:arrêt des scripts"
           exit 1
       fi
   else
        #Suppression de ou=test_ecrit
        ldapdelete -c -v -x -w $MotDePasse -D uid=SambaAdm.$DOM,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://$(CreoleGet adresse_ip_ldap_maitre):636/ -f /tmp/test_ecrit_del_tmp.ldif>> $LOG 2>&1

        if [ $? != 0 ] ;then
            echo "$p_nom: la suppression de ou=test_ecrit dans $DOM a échoué avec un des mdp: les ACL ne sont pas appliquées:arrêt des scripts">> $LOG 2>&1
            resultat_ecrit="KO"
            if [ "$resultat_ecrit_defaut" == "KO" ]; then
                # sortie de l'instanciation si resultat_ecrit_defaut="KO" et resultat_ecrit="KO" (lors de l'exécution de la deuxième boucle)
                echo "$p_nom: la suppression de ou=test_ecrit dans $DOM a échoué avec un des mdp: les ACL ne sont pas appliquées:arrêt des scripts"
                exit 1
            fi
        else
            echo "$p_nom: l'écriture de ou=test_ecrit dans $DOM et sa suppression ont réussi avec un des mdp: les ACL sont bien positionnées">> $LOG 2>&1
            resultat_ecrit="OK"
        fi
    fi
    if  [ $MotDePasse = $eq_pass ]; then
        resultat_ecrit_defaut=$resultat_ecrit
        resultat_ecrit="OK"
    fi

    #Debut de récup du nouveau mot de passe ---------------------
    echo $SIG> cle_equidom
    wget --no-proxy http://${eq_site}/diff_cle/recup_cle_enc.php -O cle.enc -o /dev/null
    wget --no-proxy http://${eq_site}/diff_cle/recup_pass_enc.php -O pass.enc -o /dev/null

    #echo "La cle de crypt. est `cat cle.enc` le pass crypt `cat pass.enc` "
    if [ -r cle.enc ]; then
        openssl enc -d -aes256 -in cle.enc -out .cle -kfile cle_equidom
        openssl enc -d -aes256 -in pass.enc -out .pass -kfile .cle
    fi

    echo "$p_nom: on reéssaye avec le nouveau mot de passe">> $LOG 2>&1
    #Si .pass existe et n'est pas vide
    if [ -s .pass ]; then
        MotDePasse=`cat .pass`
    else
        MotDePasse='xyz'
    fi

    rm cle.enc pass.enc cle_equidom .cle .pass

done

    rm -f /tmp/test_ecrit_tmp.ldif>> $LOG 2>&1
    rm -f /tmp/test_ecrit_del_tmp.ldif>> $LOG 2>&1

}



gestion_sid()
{
        echo "$p_nom: Recherche SID du domaine dans LDAP"
        ldap_eq_sambaSID=`ldapsearch -x  -s one -H ldaps://$(CreoleGet adresse_ip_ldap_distant):636/ -b ou=$(CreoleGet smb_workgroup),ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr objectClass=sambaDomain sambaSID | grep -v "^#" | grep "^sambaSID:" | tail -1 | cut -d ' ' -f 2`
        echo "$p_nom: Fin de recherche (${ldap_eq_sambaSID})"

        if [ "${ldap_eq_sambaSID}" == "" ] ; then

                echo "$p_nom:le SID du domaine ${eq_serv_domsamba}, n'existe pas encore dans ldap."

                if [ "$(CreoleGet ecdl_smb_sid)" == "" ] ; then

                                echo "$p_nom:Celui-ci sera généré"
                                eq_sambaSID=`net getlocalsid |cut -d ':' -f 2|tr -d ' '`

                else

                    echo "$p_nom:le SID utilisé sera celui saisi via Zephir : $(CreoleGet ecdl_smb_sid)"
                                eq_sambaSID=$(CreoleGet ecdl_smb_sid)
                fi

                eq_DOM_exist=0

        else

                echo "$p_nom:On utilise le SID inscrit dans LDAP"
                eq_sambaSID=$ldap_eq_sambaSID
                eq_DOM_exist=1
        fi


        echo "$p_nom:Le SID utilisé est ${eq_sambaSID}"
}


gestion_mdp()
{

echo "$p_nom version $p_version du $p_date : Initialisation des mots de passe "
echo "$p_nom version $p_version du $p_date : Initialisation des mots de passe " >> $LOG 2>&1

[ ! -d "/etc/sysconfig/" ] && mkdir /etc/sysconfig


cd /usr/share/eole

if [ ! -r /etc/sysconfig/.pass ] ;  then

	# echo "Creation .pass"
	echo "${eq_pass:-amedee}" > /etc/sysconfig/.pass

fi

echo $SIG> cle_equidom

wget --no-proxy http://${eq_site}/diff_cle/recup_cle_enc.php -O cle.enc -o /dev/null
wget --no-proxy http://${eq_site}/diff_cle/recup_pass_enc.php -O pass.enc -o /dev/null

#echo "La cle de crypt. est `cat cle.enc` le pass crypt `cat pass.enc` "

if [ -r cle.enc ]

then
	openssl enc -d -aes256 -in cle.enc -out .cle -kfile cle_equidom
	openssl enc -d -aes256 -in pass.enc -out .pass -kfile .cle

fi

if [ -r /etc/sysconfig/.pass ]

then

	cp -f /etc/sysconfig/.pass /etc/sysconfig/.pass_old

fi

#echo "la cle apres decryp est `cat .pass`"

mv -f .pass .cle /etc/sysconfig/
rm cle_equidom cle.enc pass.enc

echo "`cat /etc/sysconfig/.pass`" > param.tmp
eq_pass_adm=`cat /etc/sysconfig/.pass`


# c'est comme si la modif avait réussi
MOD=0

if [ `cat /etc/sysconfig/.pass` == `cat /etc/sysconfig/.pass_old` ]

then

	echo "$p_nom: ancien et nouveau mot de passe identique pour SambaAdm.$eq_serv_domsamba, rien à faire"
	echo "$p_nom: ancien et nouveau mot de passe identique pour SambaAdm.$eq_serv_domsamba, rien à faire">> $LOG 2>&1

elif [ "$eq_DOM_exist" == "1" ]

then

	#le domaine existe, c'est un BDC, on ne force pas le mdp ldap
	echo  "$p_nom: le domaine existe, mot de passe ldap déjà changé"

else

	echo "$p_nom: mot de passe en cours de changement"

	DOM=$eq_serv_domsamba


cat > pass_samba_adm.ldif  << EOF
dn: uid=SambaAdm.${eq_serv_domsamba},ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
changetype: modify
replace: userPassword
userPassword: $(cat param.tmp)
EOF


	echo "$p_nom: essai de modification du mot de passe Ldap"


    ldapmodify -a -v -x -w `cat /etc/sysconfig/.pass_old` -D uid=SambaAdm.$DOM,ou=$DOM,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://$(CreoleGet adresse_ip_ldap_maitre):636/ -f pass_samba_adm.ldif 2>> $LOG > /dev/null

	MOD=$?

	echo "$p_nom: code retour ldappmodify $MOD"
	echo "$p_nom: code retour ldappmodify $MOD"  >> $LOG 2>&1

	rm pass_samba_adm.ldif param.tmp

fi

##ici on change les mots de passe samba et ldap tool

if [ "$MOD" == "0" ]

then

      echo "$p_nom: mise a jour dans samba"

      smbpasswd -w `cat /etc/sysconfig/.pass` >> $LOG 2>&1

else

      echo "$p_nom: *** ERREUR Impossible de modifier ldap ***"

      cp /etc/sysconfig/.pass /etc/sysconfig/.pass_tmp
      rm /etc/sysconfig/.pass
      mv /etc/sysconfig/.pass_old /etc/sysconfig/.pass

fi

#Protection du fichier pass
chmod 600 /etc/sysconfig/.pass*
chown root /etc/sysconfig/.pass*

}

##################################################################################################

#############################################
# variables historiques
#############################################
p_version="2.0.0"
p_date="2012.05.09"
p_nom=${0##*/}
p_projet=utilitaires

eq_rep_log=/var/log/ecdl
eq_serv_domsamba=$(echo $(CreoleGet smb_workgroup)|tr [:lower:] [:upper:])
eq_site=$(CreoleGet serveur_metier)
eq_DOM_exist=$(CreoleGet ecdl_smb_DOM_exist)
SIG=$(CreoleGet ecdl_sig)
eq_pass=$(CreoleGet ecdl_adm_pass_default)

LOG=${eq_rep_log}/install_${p_projet}.log

mkdir -p ${eq_rep_log}
touch $LOG


#############################################
# déroulement des pré-requis
#############################################
m="DEBUT_:PGM=$p_nom:VERSION=$p_version:DATE=$p_date"
log "$m"

# tests réseaux pare-feu AC et accès lecture/écriture annuaire
test_reseau
test_parefeu
test_lit_ldap
test_ecrit_ldap

# recupération ou création du sid.
gestion_sid

gestion_mdp

# archivage du fichier /etc/eole/config avant traitement
cp /etc/eole/config.eol  /etc/eole/config.eol.old

# On injecte les trois valeurs dans le fichier.eol pour l'instanciation
CreoleSet ecdl_smb_sid $eq_sambaSID
CreoleSet ecdl_smb_DOM_exist $eq_DOM_exist

#protection du mdp
chmod 600 /etc/eole/config.eol /etc/eole/config.eol.old

touch $scriptPath/.prerequisOk

m="FIN___:PGM=$p_nom:VERSION=$p_version:DATE=$p_date"
log "$m"

exit 0




