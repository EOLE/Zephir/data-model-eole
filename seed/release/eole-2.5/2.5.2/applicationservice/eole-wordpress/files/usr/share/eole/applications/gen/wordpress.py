#-*- coding: utf-8 -*-
##########################################################################
# Eole NG - 2013
# Copyright Pole de competence Eole (Ministere Education - Academie Dijon)
# Licence CeCill cd /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# wordpress.py
#
# Creation de la base de donnees de wordpress
##########################################################################

"""
    Creation de la abse wordpress
"""
from eolesql.db_test import db_exists, test_var

WORDPRESS_TABLEFILENAMES = ['/usr/share/eole/mysql/wordpress/gen/wordpress-create.sql']

def test():
    """
        test  l'existence de la base wordpress
    """
    return test_var('activer_wordpress') and not db_exists('wordpress')

conf_dict = dict(filenames=WORDPRESS_TABLEFILENAMES, test=test)