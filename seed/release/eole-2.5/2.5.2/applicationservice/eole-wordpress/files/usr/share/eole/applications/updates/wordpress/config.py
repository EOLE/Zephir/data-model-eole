# -*- coding: utf-8 -*-
"""
    Update de la base wordpress à chaque reconfigure
"""

from eolesql.db_test import test_var

DB = "wordpress"
FILENAMES = ['/usr/share/eole/mysql/wordpress/updates/wordpress-update.sql',
	     '/usr/share/eole/mysql/wordpress/updates/wordpress-update-1.sql']

def test_response(result=None):
    """
        Renvoie True si wordpress est activé
    """
    return test_var('activer_wordpress')

conf_dict = dict(db=DB, test_active=test_response, filenames=FILENAMES,
                 version='Mise à jour de wordpress')
