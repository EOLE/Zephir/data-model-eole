\r wordpress
SET character_set_client = utf8;

DELIMITER |
DROP PROCEDURE IF EXISTS do_updates|
DROP PROCEDURE IF EXISTS do_wordpress_update_to351|
DROP PROCEDURE IF EXISTS do_wordpress_update_to361|
DROP PROCEDURE IF EXISTS update_db_version|

-- Creation of a procedure upgrading the wordpress database from v3.3.2 to v3.5.1
CREATE PROCEDURE do_updates()
BEGIN
	IF((SELECT meta_value FROM wp_sitemeta WHERE meta_key='wpmu_upgrade_site')<22441) THEN
		CALL do_wordpress_update_to351();
	END IF;
	
	CALL do_wordpress_update_to361();
	CALL update_db_version();
END|

CREATE PROCEDURE update_db_version()
BEGIN
	SET @db_version = '24448';

	versions: BEGIN
		DECLARE done_ids INT DEFAULT FALSE;
		DECLARE idb BIGINT;

		-- as we can have multiple blogs, we need to know the id wordpress gave them
		-- we create and use a cursor to do that

		DECLARE blog_ids CURSOR FOR SELECT blog_id FROM wp_blogs;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done_ids=TRUE;

		OPEN blog_ids;

		-- we fetch `blog_ids` cursor in a loop

		read_ids: LOOP
		FETCH blog_ids INTO idb;
		IF done_ids THEN
			LEAVE read_ids;
		END IF;


		IF((SELECT COUNT(*) FROM wp_blog_versions WHERE blog_id=idb)>0) THEN

		-- if we got a record for the current id in `wp_blogs_versions` table, we update the db_version recorded

			UPDATE wp_blog_versions SET db_version=@db_version, last_updated=NOW();
		ELSE

		-- else, we insert a new record
			INSERT INTO wp_blog_versions (blog_id, db_version, last_updated) VALUES (idb, @db_version, NOW());
		END IF;
		END LOOP read_ids;
		CLOSE blog_ids;
	END versions;
	
	dbv: BEGIN
		DECLARE done INT DEFAULT FALSE;
		DECLARE a VARCHAR(64);
		DECLARE blog CURSOR FOR SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema='wordpress' AND table_name like 'wp%_options';
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

		OPEN blog;

		read_loop: LOOP
			FETCH blog INTO a;
			IF done THEN
				LEAVE read_loop;
			END IF;
		
		SET @prefix = SUBSTRING_INDEX(a, '_options',1);
		SET @cct2 = CONCAT('UPDATE ', a, ' SET option_value=',@db_version, ' WHERE option_name="db_version"');
		PREPARE stmt2 FROM @cct2;
		EXECUTE stmt2;
		DEALLOCATE PREPARE stmt2;

		END LOOP read_loop;
		CLOSE blog;
	END dbv;

	

	UPDATE wp_sitemeta SET meta_value=@db_version WHERE meta_key='wpmu_upgrade_site';
	UPDATE wp_options SET option_value=@db_version WHERE option_name='db_version';
END|


CREATE PROCEDURE do_wordpress_update_to361()
BEGIN
	SET @db_version = '24448';
        

		-- We drop the embed_autourls option
		DELETE FROM wp_options WHERE option_name='embed_autourls';
		-- Finally we update the `wpmu_upgrade_site` meta
		UPDATE wp_sitemeta SET meta_value=@db_version WHERE meta_key='wpmu_upgrade_site';
		UPDATE wp_options SET option_value=@db_version WHERE option_name='db_version';

		-- Changes for theming
		UPDATE wp_sitemeta SET meta_value='a:5:{s:6:"envole";s:7:"/themes";s:12:"twentyeleven";s:7:"/themes";s:9:"twentyten";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}' WHERE meta_key='_site_transient_theme_roots';
		UPDATE wp_sitemeta SET meta_value='a:6:{s:7:"classic";b:1;s:12:"twentyeleven";b:1;s:6:"envole";b:1;s:9:"twentyten";b:1;s:14:"twentythirteen";b:1;s:12:"twentytwelve";b:1;}' WHERE meta_key='allowedthemes';

		UPDATE wp_options SET option_value='Twenty Thirteen' WHERE (option_name='current_theme' AND option_value='Wordpress Classic');
END|
	
	

CREATE PROCEDURE do_wordpress_update_to351()
BEGIN
	IF((SELECT meta_value FROM wp_sitemeta WHERE meta_key='wpmu_upgrade_site')<22441) THEN
		-- The  new revision of the database is 22441

		SET @db_version ='22441';

		-- Here we start the block that will alter columns and keys of tables

		alters: BEGIN

			DECLARE done INT DEFAULT FALSE;

			-- a will hold the current value of our cursor 

			DECLARE a VARCHAR(64);

			-- as we can have multiple blogs on the same wordpress, we need to know how many we have
			-- and what is the prefix for each blog
			-- so we search for table wp*_options in information_schema.tables

			DECLARE blog CURSOR FOR SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema='wordpress' AND table_name like 'wp%_options';

			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


			OPEN blog;

			-- we fetch `blog` cursor in a loop

			read_loop: LOOP
				FETCH blog INTO a;
				IF done THEN
					LEAVE read_loop;
				END IF;

				-- extraction of the prefix of the table (i.e : wp, wp_2, wp_3, etc...)

				SET @prefix = SUBSTRING_INDEX(a, '_options',1);

				-- we search if our current wp*_options have a `blog_id` in information schema

				IF((SELECT COUNT(*) FROM information_schema.columns WHERE table_name=a AND column_name='blog_id')>0) THEN

					-- if so, we drop that column

					SET @cct = CONCAT("ALTER TABLE ", a, " DROP COLUMN blog_id");
					PREPARE stmt FROM @cct;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;

				END IF;

				-- we update the db_version option in current wp*_option tables

				SET @cct2 = CONCAT('UPDATE ', a, ' SET option_value=',@db_version, ' WHERE option_name="db_version"');
				PREPARE stmt2 FROM @cct2;
				EXECUTE stmt2;
				DEALLOCATE PREPARE stmt2;
			    
				-- we have to drop the `comment_approved` key if it exists in the wp*_comments tables
			    
				SET @comments_table = CONCAT(@prefix,'_comments');
				IF((SELECT COUNT(*) FROM information_schema.statistics WHERE index_schema='wordpress' AND table_name=@comments_table AND index_name='comment_approved')>0) THEN
					SET @cct_comments = CONCAT('ALTER TABLE ', @comments_table,' DROP KEY comment_approved');
					PREPARE stmt_comments FROM @cct_comments;
					EXECUTE stmt_comments;
					DEALLOCATE PREPARE stmt_comments;
				END IF;

				-- we modify the type of `post_content_filtered` field in the wp*_posts tables

				SET @posts_table = CONCAT(@prefix,'_posts');
				SET @cct_posts = CONCAT('ALTER TABLE ', @posts_table,' MODIFY post_content_filtered longtext');
				PREPARE stmt_posts FROM @cct_posts;
				EXECUTE stmt_posts;
				DEALLOCATE PREPARE stmt_posts;

			END LOOP read_loop;
			CLOSE blog;

		END alters;

		-- HERE WE ARE DONE WITH ALTERING TABLES


		-- Let's update the db_version everywhere else

		versions: BEGIN
			DECLARE done_ids INT DEFAULT FALSE;
			DECLARE idb BIGINT;

			-- as we can have multiple blogs, we need to know the id wordpress gave them
			-- we create and use a cursor to do that

			DECLARE blog_ids CURSOR FOR SELECT blog_id FROM wp_blogs;
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET done_ids=TRUE;

			OPEN blog_ids;

			-- we fetch `blog_ids` cursor in a loop

			read_ids: LOOP
				FETCH blog_ids INTO idb;
				IF done_ids THEN
					LEAVE read_ids;
				END IF;


				IF((SELECT COUNT(*) FROM wp_blog_versions WHERE blog_id=idb)>0) THEN

					-- if we got a record for the current id in `wp_blogs_versions` table, we update the db_version recorded
				
					UPDATE wp_blog_versions SET db_version=@db_version, last_updated=NOW();
				ELSE
				
					-- else, we insert a new record
					INSERT INTO wp_blog_versions (blog_id, db_version, last_updated) VALUES (idb, @db_version, NOW());
				END IF;
			END LOOP read_ids;
			CLOSE blog_ids;
		END versions;
		
		-- Finally we update the `wpmu_upgrade_site` meta
		
		UPDATE wp_sitemeta SET meta_value=@db_version WHERE meta_key='wpmu_upgrade_site';
	END IF;
END|

CALL do_updates()|

DROP PROCEDURE IF EXISTS do_wordpress_update_to351|
DROP PROCEDURE IF EXISTS do_wordpress_update_to361|
DROP PROCEDURE IF EXISTS do_updates|
