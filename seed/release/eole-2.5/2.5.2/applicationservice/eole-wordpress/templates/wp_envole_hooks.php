<?php
/**
 * Plugin Name: Envole hooks
 * Description: Plugin contenant des modifications de l'équipe envole
 *Version: 1.0
 */



function envole_update_core()
{
    //renvoyer "false" permet de poursuivre l'exécution de "get_site_transient" et donc permet à nouveau la mise à jour
        return array();
}
add_action('pre_site_transient_update_core','envole_update_core');

function do_wp_tracking()
{
    /**
     * appel aux pages de conf de CAS
     */
    require_once('CAS-1.3.1/eoleCAS.php');
    require_once('configCAS/cas.inc.php');

    if(eolephpCAS::isAuthenticated())
    {
    	include("/var/www/html/piwik/envoleProfil.php");
		$inclusion = "<script src='https://%%web_url/piwik/envoleTrackeur.js.php?appli=wordpress&profil=".$profil."'></script>";
    }else{
		$inclusion = "<script src='https://%%web_url/piwik/envoleTrackeur.js.php?appli=wordpress&profil=visiteur'></script>";
    }
    echo $inclusion;
}
add_action('wp_head', 'do_wp_tracking');
?>
