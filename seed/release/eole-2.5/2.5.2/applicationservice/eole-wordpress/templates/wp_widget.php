<?
	include_once('../../../wp-load.php');
	require_wp_db();
	include_once('../wpcas.php');
	
	$format=$_GET["format"];
?>

<script type="text/javascript">
	function openOnglet(name, url) {
		parent.$p.show('vmenu','none');
		parent.$('tabscontent').style.marginLeft=0;
		parent.$p.app.tabs.openTempLink(name,url);		
	}
</script>

<style type="text/css" media="screen">
	@import url( <?php bloginfo('stylesheet_url'); ?> );
	
	body {
		text-align: left;
		min-width: 100px;
		overflow:auto;
	}
	
</style>
<?php
			include("/var/www/html/piwik/envoleProfil.php");
			$inclusion = "<script src='https://%%web_url/piwik/envoleTrackeur.js.php?appli=wordpress&profil=".$profil."'></script>";
			echo $inclusion; 
?>
<?php

	if(EolephpCAS::checkAuthentication()&& !is_user_logged_in()){
		// CAS was successful
		$attributs=EolephpCAS::getDetails();

		if(is_array($attributs)){
			//correspondances des statuts
			$profils = array
			(
				"National_1" => "eleve",
				"National_2" => "responsable",
				"National_3" => "professeur",
				"National_6" => "administratif",
				"administrateur" => "administrateur",
				"autre" => "visiteur"
			);

			$login = $attributs['utilisateur']['user'][0];
			$mail = $attributs['utilisateur']['email'][0];
			$cas_profil = $attributs['utilisateur']['profil'][0];

			if(array_key_exists($cas_profil,$profils))
				$profil =  $profils[$cas_profil];
			else
				$profil =  $profils["autre"];

			if ($user = get_userdatabylogin($login)) { 
				wp_set_auth_cookie($user->ID);
				$sql="SELECT * FROM wp_sentry_groups WHERE FIND_IN_SET('".$user->ID."', member_list)";
				$sql .= " AND name!='eleves-et-parents'";
				$res=$wpdb->get_row($sql);

				if(!$res) wpcas_nogroup($login,$profil);
			}
			else {
				if (function_exists('wpcas_nowpuser'))
					wpcas_nowpuser($login,$mail,$profil);
			}
			
			wp_redirect(function_exists('site_url')  ? site_url('/wp-content/plugins/poshwidget/wordpresswidget.php?format='.$format) : '/wp-content/plugins/poshwidget/wordpresswidget.php='.$format);
		}
	}
	
	// Récupération des informations utilisateurs
	$user=wp_get_current_user();
	$user_id=$user->ID;

	// Récupération de la liste des blog
	$blog_list = get_blog_list( 0, 'all' );
	asort($blog_list);
	
	// Pour chaque blog
	$i=0;
	foreach ($blog_list AS $blog) {
		// Si l'utilisateur est membre du blog
		if(is_user_member_of_blog( $user_id, $blog['blog_id'] )) {
			// On switch sur le blog en cours
			switch_to_blog($blog['blog_id']);
			
			// Afficher le titre du site
			if($format=="light") {
				if($i>0) echo "<br>";
				echo "<h1><a onClick=\"openOnglet('Wordpress','".get_bloginfo('url')."');\" style='cursor:pointer;'>".get_bloginfo('name')."</a></h1>";
			}
			
			// On parcourt les 5 derniers article
			$featuredPosts = new WP_Query();
			$featuredPosts->query('showposts=5');
			 
			while ($featuredPosts->have_posts()) : $featuredPosts->the_post(); ?>
				<div class="ItemTitle"><a onClick="openOnglet('Wordpress','<? the_permalink(); ?>');" style="cursor:pointer;"><?php the_title(); ?></a></div>
				<? if($format!="light") { ?>
				<div class="ItemDate"><?php the_time('j F Y') ?></div>
				<div class="ItemContent"><?php the_content(); ?></div>
				<? } ?>
			<? endwhile;
			
			$i=$i+1;
		} 
	}?>
	
