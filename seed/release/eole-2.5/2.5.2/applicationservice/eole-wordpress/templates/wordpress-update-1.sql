\r wordpress
SET character_set_client = utf8;

-- blogs
UPDATE wp_blogs SET domain='%%web_url'; 


-- options
UPDATE wp_options SET option_value='https://%%web_url/wordpress' WHERE option_name='siteurl';
UPDATE wp_options SET option_value='https://%%web_url/wordpress' WHERE option_name='home';
UPDATE wp_options SET option_value='https://%%web_url/wordpress/wp-content/uploads' WHERE option_name='fileupload_url';
UPDATE wp_options SET option_value='admin@%%web_url' WHERE option_name='new_admin_email';

-- site
UPDATE wp_site SET domain='%%web_url';

-- Metas du site
UPDATE wp_sitemeta SET meta_value='admin@%%web_url' WHERE meta_key='admin_email';
UPDATE wp_sitemeta SET meta_value='https://%%web_url/worpdress/' WHERE meta_key='siteurl' AND site_id=1;


-- Metas des users
UPDATE wp_usermeta SET meta_value='%%web_url' WHERE meta_key='source_domain';

-- Admin
UPDATE wp_users SET user_email='admin@%%web_url' WHERE user_login='admin';


