<?php
// ** Réglages MySQL ** //
$mdp="mdpwordpress";


@define('DB_NAME', 'wordpress'); // Le nom de la base de données
@define('DB_USER', 'wordpress'); // Votre identifiant MySQL
define('DB_PASSWORD', $mdp); // ...et votre mot de passe
@define('DB_HOST', '%%adresse_ip_mysql'); // Dans la plupart des cas, vous n'aurez pas à modifier cette ligne
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

// Modifiez chaque KEY en y mettant une phrase unique.  Vous n'avez pas besoin de la mémoriser.
// Elle doit être longue et compliquée.  Vous pouvez aller sur le site http://api.wordpress.org/secret-key/1.1/ 
// afin de générer des phrases uniques pour votre installation. Chaques phrase doit être différente.
define('AUTH_KEY', 'put your unique phrase here'); // Modifier par une phrase unique. 
define('SECURE_AUTH_KEY', 'put your unique phrase here'); // Modifier par une phrase unique. 
define('LOGGED_IN_KEY', 'put your unique phrase here'); // Modifier par une phrase unique. 

// Vous pouvez faire plusieurs installations sur une même base de données, en leur donnant chacune un préfixe unique.
define( 'NONCE_KEY', 'x=E@Lt=o,FnD2u,EOVp]S+?[xyp4w#_4FQ!;xf{7j4-#q<*w(D5vgy6?D.82' );
define( 'AUTH_SALT', './4EAJg(W-):Z_d%kWmR0yWB8CK=%VMF&>a Ue[oNMt-Ky92]bV_DkE!s>8S{Y' );
define( 'SECURE_AUTH_SALT', 'ES^L?lpRq`_lH,A1uy$:)hY$7tkJ?*7~YSh*bYia)lq:<1=1b|;Hck,~=2' );
define( 'LOGGED_IN_SALT', 'aUHw_N.G!~0%mE)Y+~D|})f``U[RRag_E#T6iJ{=~(q466lFkV]BAR(s^&k,W}' );
define( 'NONCE_SALT', '8)28@CQ1vkqaJD/xva/K8%iHdEq{o:JbQP@-CyY;r} 8yR2nE:bdzjVa42Z _f2s' );
$table_prefix  = 'wp_'; // Que des chiffres, lettres ou caractères soulignés, s'il vous plait !

// Modifiez la ligne suivante pour traduire l'administration de WordPress. Il faut que le dossier
// wp-content/languages contienne un fichier .mo correspondant à la langue choisie.
// Par exemple,  installez de.mo dans wp-content/languages et précisez 'de' pour la variable WPLANG,
// si vous souhaitez mettre en place la traduction allemande.
define ('WPLANG', 'fr_FR');
define ('FS_METHOD', 'direct');
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
$base = '/wordpress/';
define('DOMAIN_CURRENT_SITE', '%%web_url');
define('PATH_CURRENT_SITE', '/wordpress/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('FORCE_ADMIN_SSL', false);
$mem_limit = %%php_memory_limit;
define('WP_MEMORY_LIMIT', $mem_limit.'M');


%if %%activer_proxy_client == "oui"
define('WP_PROXY_HOST', '%%proxy_client_adresse');
define('WP_PROXY_PORT', '%%proxy_client_port');
define('WP_PROXY_BYPASS_HOSTS', '%%web_url, localhost');
%end if

// C'est tout, ne touchez pas au reste ! Bloguez bien ! //
if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');
define('WP_ALLOW_MULTISITE', true);
require_once(ABSPATH . 'wp-settings.php');
?>
