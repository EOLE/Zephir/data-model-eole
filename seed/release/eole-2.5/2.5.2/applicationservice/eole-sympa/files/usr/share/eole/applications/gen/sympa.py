#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2011
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# sympa.py
#
# Configuration de base de données mysql de sympa
#
###########################################################################
"""
    Config pour sympa
"""
from os.path import join
from eolesql.db_test import table_exists, test_var
from eolesql.config import dico_creole

REMOVE_BDD_QUERY = "DROP DATABASE IF EXISTS %s"
# /!\ sympa est dans le conteneur mail /!\
SYMPA_TABLEFILENAMES = [join('/', dico_creole.get_creole('container_path_mail'),
                        'usr/share/sympa/bin/create_db.mysql')]

def test():
    """
        test l'existence de la table Sympa
    """
    return test_var('activer_sympa') and not table_exists('sympa', 'user_table')

def pregen_func(db_handler):
    """
        Fonction exécutée avant la génération si le test est bon
    """
    db_handler.delete(REMOVE_BDD_QUERY % ("sympa",))

def postgen_func(db_handler):
    """
        Fonction exécutée après la génération si le test est bon
    """
    # les requêtes sont réalisées depuis le conteneur "mail"
    sympa_host = dico_creole.get_creole('adresse_ip_mail')
    db_handler.simple_query("""GRANT select,insert,update,delete,alter
                               ON sympa.* TO sympa@%s
                               IDENTIFIED BY 'sympa';""" % sympa_host)

conf_dict = dict(filenames=SYMPA_TABLEFILENAMES,
                 test=test,
                 pregen=pregen_func,
                 postgen=postgen_func,
                 )
