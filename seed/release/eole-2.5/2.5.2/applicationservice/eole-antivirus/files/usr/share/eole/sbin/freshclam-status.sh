#!/bin/bash

STATUS=$1

echo "# -*- coding: UTF-8 -*-
DATE=\"`date +%s`\"
STATUS=\"$STATUS\"" > /var/log/clamav/freshclam-status.log
