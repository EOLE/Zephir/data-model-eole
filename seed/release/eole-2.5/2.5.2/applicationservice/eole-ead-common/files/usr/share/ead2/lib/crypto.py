#-*-coding:utf-8-*-
"""
    Fonction de cryptage
"""
try:
    from hashlib import sha1 as sha
except ImportError:
    from sha import sha
from time import time

def create_key(cle1, cle2):
    """
        génère une clé aléatoire
    """
    quote = '%s%s%s' % (int(time()), cle1, cle2)
    return sha(quote).hexdigest()
