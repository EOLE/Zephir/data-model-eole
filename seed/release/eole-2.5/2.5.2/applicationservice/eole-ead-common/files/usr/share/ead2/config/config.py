# -*- coding: UTF-8 -*-

# répertoire de l'application ead
import os.path as osp
from sys import exit
from creole.eosfunc import is_installed
from ConfigParser import ConfigParser

CONFIG_FILE = '/var/lib/eole/config/ead.cfg'

if not osp.isfile(CONFIG_FILE):
    print("Fichier de configuration non existant")
    exit(0)

cfg = ConfigParser(allow_no_value=True)
cfg.read(CONFIG_FILE)

HERE = osp.dirname(__file__)
EAD_DIR = osp.normpath(osp.join(HERE, '..'))

ip_locale = cfg.get('eole', 'adresse_ip_eth0')
AUTH_SERVER_ADDR = cfg.get('ead', 'auth_server_addr')
AUTH_FORM_PORT = cfg.get('ead', 'auth_form_port')
AUTH_FORM_URL = cfg.get('ead', 'auth_form_url')
AUTH_SERVER = cfg.get('ead', 'auth_server')
REVERSEPROXY_PORT = cfg.get('ead', 'reverseproxy_port')
cert_file = cfg.get('ead', 'cert_file')
key_file = cfg.get('ead', 'key_file')

MAGIC_NUMBER_TIMEOUT = cfg.getint('ead', 'magic_number_timeout')
EXPIRED_SESSION_TIMEOUT = cfg.getint('ead', 'expired_session_timeout')
LIGHTSQUID_INSTALLED = cfg.getboolean('lightsquid', 'lightsquid_installed')
LIGHTSQUID_TIMEOUT = cfg.getint('lightsquid', 'lightsquid_timeout')
LIGHTSQUID_PORT = cfg.getint('lightsquid', 'lightsquid_port')
LIGHTSQUID_AUTO = cfg.getboolean('lightsquid', 'lightsquid_auto')
SQUID_AUTH = cfg.getboolean('proxy', "squid_auth")
SQUID2_ACTIVATE = cfg.getboolean('proxy', "squid2_activate")
BACKEND_LISTEN_PORT = cfg.getint('ead', 'backend_listen_port')
#Port d'ecoute du serveur de fichier
EADFILE_LISTEN_PORT = cfg.getint('ead', 'eadfile_listen_port')
EADFILE_URL = cfg.get('ead', 'eadfile_url')
DHCP_PATH = cfg.get('eole', 'container_path_dhcp')
CUPS_ACTIVATE = cfg.getboolean('cups', 'cups_activate')
CUPS_ADDRESS = cfg.get('cups', 'cups_address')
CONTAINER_IP_FICHIER = cfg.get('eole', 'container_ip_fichier')
CONTAINER_PATH_FICHIER = cfg.get('eole', 'container_path_fichier')
CONTAINER_PATH_PROXY = cfg.get('eole', 'container_path_proxy')
NOM_ACADEMIE = cfg.get('eole', 'nom_academie')
LIBELLE_ETAB = cfg.get('eole', 'libelle_etab')
EOLE_MODULE = cfg.get('eole', 'eole_module')
EOLE_VERSION = cfg.get('eole', 'eole_version')
SUFFIXE_DOMAINE_ACADEMIQUE = cfg.get('eole', 'suffixe_domaine_academique')
BAREOS_SD_INSTALLED = cfg.getboolean('backup', 'bareos_sd_installed')
BAREOS_SD_ACTIVATE =  cfg.getboolean('backup', 'bareos_sd_activate')
BAREOS_DIR_ACTIVATE = cfg.getboolean('backup', 'bareos_dir_activate')
BLACKLIST_INSTALLED = cfg.getboolean('proxy', 'blacklist_installed')
ANTIVIRUS_SAMBA = cfg.getboolean('fichier', 'antivirus_samba')
SYNCHRO_AAF = cfg.get('fichier', 'synchro_aaf')
NFS_ACTIVATE = cfg.getboolean('fichier', 'nfs_activate')
CLIENT_LTSP_INSTALLED = cfg.getboolean('fichier', 'client_ltsp_installed')
SMB_MIN_PASSWORD_LENGTH = cfg.get('fichier', 'smb_min_password_length')
SMB_MIN_PASSWORD_CLASS = cfg.get('fichier', 'smb_min_password_class')
# FIXME: utilisation de eth1 sur AmonEcole (#2726)
FICHIER_LINK_INTERFACE = cfg.get('fichier', 'fichier_link_interface')
DOMAIN_FICHIER_LINK_INTERFACE = cfg.get('fichier', 'domain_fichier_link_interface')
DANSGUARDIAN_EAD_FILTRE1 = cfg.get('proxy', 'dansguardian_ead_filtre1')
DANSGUARDIAN_EAD_FILTRE2 = cfg.get('proxy', 'dansguardian_ead_filtre2')
services_name = cfg.get('eole', 'services_name').split()
services_container = cfg.get('eole', 'services_container').split()
SERVICES = set(zip(services_name, services_container))
