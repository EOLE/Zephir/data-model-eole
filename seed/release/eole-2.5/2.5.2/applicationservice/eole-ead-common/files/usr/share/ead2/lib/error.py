# -*- coding: UTF-8 -*-
"""
Module contenant les erreurs de l'ead
"""
from ead2.backend.actions.tools import forbidden_pwd_chars

# Définition des exceptions pour les listes d'objets
class UnknownServer(Exception):
    pass

class ExistingServer(Exception):
    pass

## utilisé dans les actions
class MissingKey(Exception):
    """ Il manque une clé dans un retour de formulaire """
    pass

class MissingValue(Exception):
    """ Une valeur n'est pas renseignée """
    pass

class BadPassword(Exception):
    """ Un mot de passe est mal formé """
    message = "Erreur : le mot de passe ne doit pas contenir d'espace."

class BadLogin(Exception):
    """ Un login est mal formé """
    message = "Erreur : le login ne doit contenir que les caratères de a à z, de 0 à 9 et -_ ."

class BadShareName(Exception):
    """ Un nom de partage est mal formé """
    message = "Erreur : le nom du partage ne doit contenir que les caratères de a à z, de 0 à 9 et -_ ."

class BadGroupName(Exception):
    """ Un nom de groupe est mal formé """
    message = "Erreur : le nom du groupe ne doit contenir que les caratères de a à z, de 0 à 9 et -_ ."

class BadDriveLetter(Exception):
    """ Une lettre de lecteur mal formée """
    message = "Erreur : lettre de lecteur mal formé (ex: k:)."

class BadPath(Exception):
    """ un chemin de fichier mal formé """
    message = "Erreur : le chemin ne doit contenir que les caratères de a à z, de 0 à 9 et -_/ ."

class TemplateError(Exception):
    pass

