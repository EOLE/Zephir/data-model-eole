#!/bin/bash
# creation des branches pour les groupes bureautiques
# 3/01/2012 chemin complet pour repl fic
# source /etc/sysconfig/.site
# source /usr/share/${eq_modele}/param.source

. /usr/bin/ParseDico

ENTITE=`ldapsearch -x ou=Entites|grep ou:|tr -d " "`
if [ "$ENTITE" = "" ]; then
cd /usr/share/ecdl/

eq_serv_domsamba=$(echo $ecdl_smb_workgroup|tr [:lower:] [:upper:])

# ./bin/repl_fic.sh modeles/groupes.ldif import_branches.ldif

cat > import_branches.ldif << EOF
########################################################
#
## groupes.ldif  creation des branches pour les groupes bureautiques
#
## Equipe CETE Mediteranne / PNE systeme
#  Validé le 3/01/2012
#
########################################################
###################################
# definition du domaine Samba
###################################
# Normalement tout est en place sauf si le service
# fournit un sid (la mise en place par net getlocalsid et
# ldapmodify)
dn: ou=Groupes,ou=Population,ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
objectClass: organizationalUnit
ou: local

dn: ou=Amedee,ou=Groupes,ou=Population,ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
objectClass: organizationalUnit
ou: Amedee

dn: ou=Listes,ou=Groupes,ou=Population,ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
objectClass: organizationalUnit
ou: Listes

dn: ou=Entites,ou=Groupes,ou=Population,ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr
objectClass: organizationalUnit
ou: Entites

EOF

ldapmodify -a -v -x -w `cat /etc/sysconfig/.pass` -D uid=SambaAdm.${eq_serv_domsamba},ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://$adresse_ip_ldap_maitre:$ecdl_ldap_port/ -f import_branches.ldif
else
echo creation deja faite
fi

