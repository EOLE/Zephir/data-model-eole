#!/bin/bash
#test replication compte
. /usr/share/ecdl/param.source
ldapsearch -x -h ldapsmb.ac.melanie2.i2 -D uid=SambaAdm.${eq_serv_domsamba},ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -w `cat /etc/sysconfig/.pass` -b ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr  uid=$1 sambaNTPassword > r1
ldapsearch -x -h ldapma -D uid=SambaAdm.${eq_serv_domsamba},ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -w `cat /etc/sysconfig/.pass` -b ou=${eq_serv_domsamba},ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr  uid=$1 sambaNTPassword > r2
diff r1 r2
