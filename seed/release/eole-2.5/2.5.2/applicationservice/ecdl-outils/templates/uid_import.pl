#!/usr/bin/perl
# generation du fichier d import des uidnumber pour les comptes bureautiques amedee
# en entree : fichier des comptes - uid,uidnumber (si null, prend le uidnumber le plus eleve)
# en sortie : fichier des comptes pris en compte - uid, uidnumber
#           : fichier anomalie - compte amedee inexistant
# variables environnement utilises : DOM nom de domaine, SID SID du domaine
use strict;
  use Net::LDAP;

  my $dom;
  my $sid;
  my $mesg;
  my $rid;
  my $luid;
  my $id;
  my $uidn;
  my $x;
  my @mot;
  my @dom;
  my $p=@ARGV[0];
  my $posix;
  my $uidnumber = 9200001;
  my $smb;
#  my $ldap = Net::LDAP->new("ldapsmb.ac.melanie2.i2", port => 389, version => 3)
#     or die "erreur LDAP: Can't contact master ldap server ($@)";
  my %idsmb;
  my %idposix;
  my %idn; # table utilisateur ldap
my %table;  # table utilisateur en entree
my $idmax = 9000000;
my %tabidn; # table indexe uidnumber en entree

open (OUTLDIF, ">/var/log/import-amedee/import.ldif");
open (OUTFILE, ">report_uid.txt");
if ($ENV{'DOM'}) 
 { $dom = $ENV{'DOM'};
 }
 else
 { print "ERR DOM\n";
 exit;}

if ($ENV{'SID'})
 { $sid = $ENV{'SID'};}
  else { print "ERR SID\n"; exit;}
print OUTFILE "$dom $sid\n";
#
# lecture du fichier entree
print OUTFILE "============= Fichier en entree ==============\n";
 while (<>) {
  chomp;
  my ($luid, $lidnumber) = split /,/;
  if ($lidnumber == '') { $lidnumber = 0; }
  if ($idmax < $lidnumber) { $idmax = $lidnumber;}
  $table{$luid} = [] unless exists $table{$luid};
  $table{$luid} = $lidnumber;
  if (exists $tabidn{$lidnumber})
    {
    if (not($lidnumber == 0)) {print OUTFILE "Conflit :  $luid meme $lidnumber que $tabidn{$lidnumber}\n";}
    }
  else
    {
    $tabidn{$lidnumber} = [] unless exists $tabidn{$lidnumber};   
    $tabidn{$lidnumber} = $luid;
    }
  }
#print "Max : $idmax \n";
$idmax = $idmax + 1;
#print "Lecture des uid \n";

print OUTFILE  "============== Comptes Amedee =================\n";

#
#Lecture des uid amedee
# objectClass = Person
# 
my $ldap = Net::LDAP->new("ldaps://ldapsmb.ac.melanie2.i2", port => 636, version => 3)
     or die "erreur LDAP: Can't contact master ldap server ($@)";
######## $table rempli avec uid et uidnumber 
  $mesg = $ldap->bind(  );
  $mesg->code && die $mesg->error;
  $mesg = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(objectClass=Person)',
    deref => 'always',
#    attrs=> ['sambaSID', 'uidNumber'])
    attrs=> ['objectClass','uid', 'uidNumber'])
  ;
  $mesg->code && die $mesg->error;
  my $max = $mesg->count; 

#print "#  nombre: $max \n";
  my @entries = $mesg->entries;

for( my $index = 0 ; $index < $max ; $index++) 
{
     my $entry = $mesg->entry($index);
     my $dn = $entry->dn; # Obtain DN of this entry
     my $uid = $entry->get_value( 'uid' );
     my $tidnumber = $entry->get_value( 'uidNumber' );
     if ($idmax < $tidnumber) { $idmax = $tidnumber;}
#print "$dn \n";
print OUTFILE  "Compte ldap $uid: ";
$idposix{$uid} = 0;
$idsmb{$uid} = 0;
$idn{$uid} = $dn;
my @attrs = $entry->attributes; # Obtain attributes for this entry.
my $attr = $entry->get_value( "objectClass", asref => 1 );
  foreach my $value ( @$attr )
      {
       if ($value eq 'sambaSamAccount') { $idsmb{$uid} = 1;}
       if ($value eq 'posixAccount') {$idposix{$uid} = 1;}
      }
# test existant iodnumber dans fichier entree
  if ($table{$uid} )
  {
   print OUTFILE  "$tidnumber en ";
   print OUTFILE  "$table{$uid}";
   print OUTFILE "\n";
  }
  else
  {
   print OUTFILE  "=$tidnumber+ ";
   my $ident = 0;
     if ($tidnumber == "undef")
     {
     print OUTFILE "nouveau compte bureautique\n";
     $ident = 2;    
     }
     else
     {
     foreach my $nid (sort keys %table)
     {
      $ident = 0;
      if ( $table{$nid} == $tidnumber )
        { my $cherid = $nid ;
          $ident = 1;
 print OUTFILE  "en conflit avec $cherid \n"; 
        } 
     }
     }
     if ( $ident == 0 )
     {
     print OUTFILE  "inchange\n";
     }
  }

}
$idmax = $idmax + 1;

print OUTFILE  "============ Resultat du traitement ===================\n";
#
# Generation du fichier ldif
#
#
foreach my $uid (sort keys %table)
{
# print "#==================================================\n";
# print "#  $uid \n";
#print "# $idsmb{$uid} $idposix{$uid} \n";
if ($idn{$uid} )
{
 print OUTLDIF "dn:$idn{$uid} \n";
 print OUTLDIF "changetype: modify\n";
  print OUTFILE  "$uid,";
  if ( $table{$uid} == 0 ) {
     $table{$uid} = $idmax ;
     $idmax = $idmax+1;
     }
  print OUTFILE  "$table{$uid}";
  print OUTFILE "\n";

 $uidnumber = $table{$uid};
 $rid=$uidnumber-1000;
 if  ( $idposix{$uid} == 0 )
    {
    # ajout classe posix
	print OUTLDIF "add: objectClass\nobjectClass: posixAccount\n-\nadd: gidNumber\ngidNumber: 513\n-\nadd: uidNumber\n";
	print OUTLDIF "uidNumber: $uidnumber\n";
	print OUTLDIF "-\nadd: loginShell\nloginShell: /bin/false\n-\nadd: homeDirectory\n";
	print OUTLDIF "homeDirectory: /bureautique/individuel/$uid\n-\n";
    }
 else
    {
    #modif classe posix uidnumber ...
	print OUTLDIF "replace: gidNumber\ngidNumber: 513\n-\nreplace: uidNumber\n";
	print OUTLDIF "uidNumber: $uidnumber\n-\n";
	print OUTLDIF "replace: loginShell\nloginShell: /bin/false\n-\nreplace: homeDirectory\n";
	print OUTLDIF "homeDirectory: /bureautique/individuel/$uid\n-\n";
    }

 if ( $idsmb{$uid} == 0)
    {
    # Ajout classe samba
	print OUTLDIF "add: objectClass\nobjectClass: sambaSamAccount\n-\nadd: sambaAcctFlags\nsambaAcctFlags: [UX]\n-\n";
	print OUTLDIF "add: sambaPwdMustChange\nsambaPwdMustChange: 2147483647\n-\nadd: sambaNTPassword\n";
	print OUTLDIF "sambaNTPassword: **NoLogin**\n-\nadd: SambaSID\n";
	print OUTLDIF "sambaSID: $sid-$rid\n-\nadd: sambaDomainName\n";
	print OUTLDIF "sambaDomainName: $dom\n-\nadd: sambaPwdLastSet\nsambaPwdLastSet: 1250240808\n-\n";
	print OUTLDIF "replace: mineqPasswordDoitChanger\nmineqPasswordDoitChanger: Vous devez changer de mot de passe\n\n";
    }
   else
   {
   # Modif SID samba
	print OUTLDIF "replace: SambaSID\nsambaSID: $sid-$rid\n\n";
  }
}
else
{
print OUTFILE "$uid inexistant\n";
}
}
print OUTFILE  "idmax = $idmax\n";
close OUTFILE;
close OUTLDIF;
