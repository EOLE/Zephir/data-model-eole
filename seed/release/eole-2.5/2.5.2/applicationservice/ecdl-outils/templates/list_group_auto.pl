#!/usr/bin/perl 
# version 19/07/11
# creation dans groupes/entites
# zone info : dn + niveau complet
#
use strict;
  use Net::LDAP;

  my $dom="TEST";
  my $sid;
  my $mesg;
  my $ga_modif;
  my $groupsmb;
  my $mcount;
  my $rid;
  my $uidn;
  my $p=@ARGV[0];
  my %th_ga;
  my %th_la;
  my $posix;
  my $ligne = "";
  my $smb;
  my $ldap = Net::LDAP->new("ldaps://ldapsmb.ac.melanie2.i2", port => 636, version => 3)
     or die "erreur LDAP: Can't contact master ldap server ($@)";
  my %table;  # table listes auto
  my %tabgba;  # table groupes auto
  $mesg = $ldap->bind(  );
  $mesg->code && die $mesg->error;
$dom=`grep workgroup /etc/samba/smb.conf|tr -d " "|cut -d "=" -f 2`;
chomp $dom;
# print "DOM=$dom\n";

#if ($ENV{'SID'})
# { $sid = $ENV{'SID'};}
#  else { print "ERR SID\n"; exit;}

my $mdom = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(objectClass=sambaDomain)',
    deref => 'never',
    attrs=> ['sambaSID']) ;
  $mdom->code && die $mdom->error;
  my $cdentry = $mdom->entry(0);
  my $sid =  $cdentry->get_value( 'sambaSID' );
#print "$sid\n";
open (OUTLDIF, ">out");
open (OUTLDIFM, ">outm");

  $mesg = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(&(info=GESTION: AUTO/ListesSU)(objectclass=mineqMelListe))',
    deref => 'always',
    attrs=> ['mail','memberUid']) ;
  $mesg->code && die $mesg->error;
  my $max = $mesg->count; 

  $mcount = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(cn=zcompteur)',
    deref => 'never',
    attrs=> ['gidNumber']) ;
  $mcount->code && die $mcount->error;

  $groupsmb = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(&(objectclass=sambaGroupMapping)(objectclass=posixGroup))',
    deref => 'never',
    attrs=> ['cn','memberUid']) ;
  $groupsmb->code && die $mesg->error;

  my $gmax = $groupsmb->count; 
  my $gcount = $mcount->count; 
  my @entries = $mesg->entries;
  my @gentries = $groupsmb->entries;
  my @entcount = $mcount->entries;
  my $centry = $mcount->entry(0);
  my $gidnum =  $centry->get_value( 'gidNumber' );
  my $gidinit = $gidnum;
  my $modif_g = 0;
#  print "gidnumber = $gidnum \n";
#  print "Gidmax = $gcount\n";

# Lecture Table des listes auto
#print "#  nombre: $max \n";
for( my $index = 0 ; $index < $max ; $index++) 
  {
     my $entry = $mesg->entry($index);
     my $dn = $entry->dn; # Obtain DN of this entry
#   print "$dn\n";
     my $gid = $entry->get_value( 'mail' );
    $gid =~ tr/"\100"/":"/;
    my ($mgid,$ad) = split /:/,$gid;
    my ($ag,$gad) = split /Agents./,$mgid;
  $gad =~ tr /A-Z/a-z/ ;

  $table{$gad} = [] unless exists $table{$gad};
  $table{$gad} = $index;
    my @attrs = $entry->attributes; # Obtain attributes for this entry.
    my $attr = $entry->get_value( "memberUid", asref => 1 );
    foreach my $value ( @$attr )
      {
#     print "$value ";
      }
#    print "\n";
  }

# Lecture Table des groupes auto existants
#print "Liste des groupes existants\n";
#print "nombre: $gmax \n";
for( my $index = 0 ; $index < $gmax ; $index++) 
  {
    my $gentry = $groupsmb->entry($index);
    my $gid = $gentry->get_value( 'cn' );
     my $dn = $gentry->dn; # Obtain DN of this entry
#   print "$dn\n";
#    print OUTLDIF "$gid: ";
#    print "$gid : ";
  $tabgba{$gid} = [] unless exists $tabgba{$gid};
  $tabgba{$gid} = $index;
    my @attrs = $gentry->attributes; # Obtain attributes for this entry.
    my $attr = $gentry->get_value( "memberUid", asref => 1 );
    foreach my $value ( @$attr )
      {
#print  "$value ";
      }
#    print "\n";
  }

#    print "\n\n";
# Traitement  Table des listes auto
for( my $index = 0 ; $index < $max ; $index++) 
  {
     my $entry = $mesg->entry($index);
     my $dnn = $entry->dn; # Obtain DN of this entry
     my ($leftn,$dn) = split /,/,$dnn;
     $leftn = $leftn.",";
     my ($dn1,$dn) = split /$leftn/,$dnn;
     my $gid = $entry->get_value( 'mail' );
#print "mail = $gid \n";
    $gid =~ tr/"\100"/":"/;
    my ($mgid,$ad) = split /:/,$gid;
    my ($ag,$gad) = split /Agents./,$mgid;
    $gad =~ tr /A-Z/a-z/ ;
    if ( index($gad,".") == -1 )
    {
#    print "ignore $gad : nom du service\n";
    }
    else
    {
  my @values = split('\.', $gad);
  @values = reverse @values;
 my $gad = join('.', @values);

#      print "membre liste auto  $gad: ";
      my @attrs = $entry->attributes; # Obtain attributes for this entry.
      my $attr = $entry->get_value( "memberUid", asref => 1 );
      foreach my $value ( @$attr )
      {
      $th_la{$value} = [] unless exists $th_la{$value};
#      print  "$value ";
      }
#      print "\n";
    if (exists $tabgba{$gad})
      {
      $ga_modif = 0;
      my $ix = $tabgba{$gad};
#      print "$gad exite deja : modif $ix\n";
       my $gentry = $groupsmb->entry($ix);
       my $ga_id = $gentry->get_value( 'cn' );
       my $ga_dn = $gentry->dn; # Obtain DN of this entry
#
      $ligne = $ligne."dn:".$ga_dn."\nchangetype: modify\n";
      $ligne = $ligne."replace: info\n";
      $ligne = $ligne."info:  SAMBA.gb.auto.entite.dn: $dn\n";
      $ligne = $ligne."-\n";
      $ligne = $ligne."add: info\n";
      $ligne = $ligne."info:  SAMBA.gb.auto.entite.niveau: complet\n";
      $ligne = $ligne."\n";
	 $modif_g = 1;
      }
      else
      {
#       print "creation de $gad $gidnum \n";
       $rid=$gidnum-1000;
       my @attrs = $entry->attributes; # Obtain attributes for this entry.
       my $attr = $entry->get_value( "memberUid", asref => 1 );
       print OUTLDIF "dn: cn=$gad,ou=Entites,ou=Groupes,ou=Population,ou=$dom,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr\n";
       print OUTLDIF "objectClass: posixGroup\n";
       print OUTLDIF "objectClass: sambaGroupMapping\n";
       print OUTLDIF "objectClass: mineqGroup\n";
       print OUTLDIF "objectClass: top\n";
       print OUTLDIF "cn: $gad\n";
       print OUTLDIF "sn: $gad\n";
       print OUTLDIF "gidNumber: $gidnum\n";
       print OUTLDIF "description: $gad\n";
       print OUTLDIF "displayName: $gad\n";
       print OUTLDIF "info: SAMBA.gb.auto.entite.dn: $dn\n";
       print OUTLDIF "info: SAMBA.gb.auto.entite.niveau: complet\n";
       print OUTLDIF "sambaSID: $sid-$rid\n";
       foreach my $value ( @$attr )
         {
         print OUTLDIF "memberUid: $value\n";
         }
       print OUTLDIF "sambaGroupType: 2\n\n";
       $gidnum = $gidnum + 1;
      }
  }
  }
if ( $modif_g == 1 )
{
print OUTLDIFM $ligne;
} 
# MAJ compteur gidnumber
if (not( $gidnum == $gidinit))
{
print OUTLDIFM "dn: uid=zcompteur,ou=$dom,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr\n";
print OUTLDIFM "changetype: modify\n";
print OUTLDIFM "replace: gidNumber\n";
print OUTLDIFM "gidNumber: $gidnum\n";
print "Compteur gidnumber = $gidnum\n";
}
my $file = '/etc/sysconfig/.pass';
open(INFO, $file);              # Ouvre le fichier
my $ligne = <INFO>;
chomp $ligne;
my $pass = $ligne;
`ldapadd  -v -c -x -w $pass -D uid=SambaAdm.$dom,ou=$dom,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://ldapma-01.csac.melanie2.i2:636/ -f out`;

`ldapmodify -a -v -x -w $pass -D uid=SambaAdm.$dom,ou=$dom,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr -H ldaps://ldapma-01.csac.melanie2.i2:636/ -f outm`;
#my $hote = `hostname`;
#print "\n$hote";

close OUTLDIF;
close OUTLDIFM;
close INFO;
