#!/bin/bash
cd /bureautique/partage_bal/
. /usr/share/ecdl/param.source
DOSSIER_PARTAGE=/bureautique/dossiers
DOM=$eq_serv_domsamba+
echo echo $DOM
echo cd $DOSSIER_PARTAGE
for i in *
 do  echo echo dossier: $i
 echo mkdir $i '2>/dev/null'
 for j in `cat $i`
 do 
 IDENT=`echo $j|cut -d ":" -f1`
 PERM=`echo $j|cut -d ":" -f2`
 case $PERM in
 [EC])
 ACL="rwx"
 ;;
 L)
 ACL="r-x" 
 ;;
 G)
 ACL="rwx"
 echo chown $DOM$IDENT $i
 ;;
 esac
 echo setfacl -R -m u:$DOM$IDENT:$ACL $i
 echo setfacl -R -d -m u:$DOM$IDENT:$ACL $i
 done
 echo
 done
