#!/bin/bash
. /etc/sysconfig/.site
DOM=`grep workgroup /etc/samba/smb.conf|tr -d " "|cut -d = -f2|tr [:lower:] [:upper:]`
SID=`ldapsearch -x "(&(sambaDomainName=$DOM)(obJectClass=sambaDomain))"|grep sambaSID|tr -d " "|cut -d ":" -f2`
[ ! -d "/var/log/import-amedee" ] &&  mkdir /var/log/import-amedee/ 2> /dev/null
# cp /usr/share/${eq_modele}/modeles/import-amedee /etc/apache2/sites-available/
# a2ensite import-amedee
# service apache2 reload
export DOM SID
/usr/share/ecdl/bin/uid_import.pl $1

