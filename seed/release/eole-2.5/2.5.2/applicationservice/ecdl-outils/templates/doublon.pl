#!/usr/bin/perl 
# generation du fichier d import des uidnumber pour les comptes bureautiques amedee
# en entree : fichier des comptes - uid,uidnumber (si null, prend le uidnumber le plus eleve)
# en sortie : fichier des comptes pris en compte - uid, uidnumber
#           : fichier anomalie - compte amedee inexistant
# variables environnement utilises : DOM nom de domaine, SID SID du domaine
use strict;
  use Net::LDAP;

  my $dom;
  my $sid;
  my $mesg;
  my $rid;
  my $luid;
  my $id;
  my $uidn;
  my $x;
  my @mot;
  my $p=@ARGV[0];
  my $posix;
  my $uidnumber = 21922;
  my $smb;
#  my $ldap = Net::LDAP->new("ldapsmb.ac.melanie2.i2", port => 389, version => 3)
#     or die "erreur LDAP: Can't contact master ldap server ($@)";
  my %idsmb;
  my %idposix;
  my %idn;
my %table;
my $idmax = 0;

print "Liste des uidnumber en doublon\n";
open (OUTFILE, ">report_uid.txt");
if ($ENV{'DOM'}) 
 { $dom = $ENV{'DOM'};
 }
 else
 { print "ERR DOM\n";
 exit;}

if ($ENV{'SID'})
 { $sid = $ENV{'SID'};}
  else { print "ERR SID\n"; exit;}
#print OUTFILE "$dom $sid\n";
#
# lecture du fichier entree


#
#Lecture des uid amedee
# objectClass = Person
# 
my $ldap = Net::LDAP->new("ldaps://ldapsmb.ac.melanie2.i2", port => 636, version => 3)
     or die "erreur LDAP: Can't contact master ldap server ($@)";
######## $table rempli avec uid et uidnumber 
  $mesg = $ldap->bind(  );
  $mesg->code && die $mesg->error;
  $mesg = $ldap->search(
    base   => 'ou='.$dom.',ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr',
    scope  => 'sub',
    filter => '(objectClass=Person)',
    deref => 'always',
#    attrs=> ['sambaSID', 'uidNumber'])
    attrs=> ['objectClass','uid','uidNumber'])
  ;
  $mesg->code && die $mesg->error;
  my $max = $mesg->count; 

#print "#  nombre: $max \n";
  my @entries = $mesg->entries;

for( my $index = 0 ; $index < $max ; $index++) 
  {
     my $entry = $mesg->entry($index);
     my $dn = $entry->dn; # Obtain DN of this entry
     my $uid = $entry->get_value( 'uid' );
     my $uidnum = $entry->get_value( 'uidNumber' );
#print "$dn \n";
$idposix{$uid} = $uidnum;
$idsmb{$uid} = 0;
$idn{$uid} = $dn;
my @attrs = $entry->attributes; # Obtain attributes for this entry.
my $attr = $entry->get_value( "objectClass", asref => 1 );
  foreach my $value ( @$attr )
      {
       if ($value eq 'sambaSamAccount') { $idsmb{$uid} = 1;}
      }
#print "$uid = $uidnum\n";
   }
#
# Generation du fichier ldif
#
#
@mot = sort values %idposix;
my $idc = 720;
for my $val ( @mot )
 {
  if ( $idc == $val )
     {
     print "- $val : ";
      foreach my $id (sort keys %idposix)
      {
      if ( $idposix { $id} == $val)
      {
      print "$id ";
      }
     }
     print "\n";
    }
  else
  {  $idc = $val; }
 }
close OUTFILE;
