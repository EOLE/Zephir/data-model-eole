#!/usr/bin/perl -T
#############################################################################
#                                                                           #
# This script was initially developed by Anstat Pty Ltd for internal use    #
# and has kindly been made available to the Open Source community for       #
# redistribution and further development under the terms of the             #
# GNU General Public License: http://www.gnu.org/licenses/gpl.html          #
#                                                                           #
#############################################################################
#                                                                           #
# This script is supplied 'as-is', in the hope that it will be useful, but  #
# neither Anstat Pty Ltd nor the authors make any warranties or guarantees  #
# as to its correct operation, including its intended function.             #
#                                                                           #
# Or in other words:                                                        #
#       Test it yourself, and make sure it works for YOU.                   #
#                                                                           #
#############################################################################
# Author: George Hansper               e-mail:  Name.Surname@anstat.com.au  #
#############################################################################

$ENV{PATH}="/sbin:/usr/sbin:/bin:/usr/bin";

use strict vars;
use vars qw( $omreport %opt);

%opt = (
         'omreport'          => undef,    # omreport path
       );



sub find_omreport {
    # If user has specified path to omreport
    if (defined $opt{omreport} and -x $opt{omreport}) {
        $omreport = qq{"$opt{omreport}"};
        return;
    }

    # Possible full paths for omreport
    my @omreport_paths
      = (
         '/opt/dell/srvadmin/bin/omreport',              # default on Linux with OMSA >= 6.2.0
         '/usr/bin/omreport',                            # default on Linux with OMSA < 6.2.0
         '/opt/dell/srvadmin/oma/bin/omreport.sh',       # alternate on Linux
         '/opt/dell/srvadmin/oma/bin/omreport',          # alternate on Linux
         'C:\Program Files (x86)\Dell\SysMgt\oma\bin\omreport.exe', # default on Windows x64
         'C:\Program Files\Dell\SysMgt\oma\bin\omreport.exe',       # default on Windows x32
         'c:\progra~1\dell\sysmgt\oma\bin\omreport.exe', # 8bit legacy default on Windows x32
         'c:\progra~2\dell\sysmgt\oma\bin\omreport.exe', # 8bit legacy default on Windows x64
        );

    # Find the one to use
  OMREPORT_PATH:
    foreach my $bin (@omreport_paths) {
        if (-x $bin) {
            $omreport = qq{"$bin"};
            last OMREPORT_PATH;
        }
    }

    # Exit with status=UNKNOWN if OM is not installed, or we don't
    # have permission to execute the binary
    if (!defined $omreport) {
        print "ERROR: Dell OpenManage Server Administrator (OMSA) is not installed\n";
        exit 2;
    }
    return;
}

$omreport = undef;
find_omreport();

my $rcsid = '$Id: check_dell_sensors.pl,v 1.2 2006/10/08 22:43:40 georgeh Exp georgeh $';
my $rcslog = '
  $Log: check_dell_sensors.pl,v $
  Revision 1.2  2006/10/08 22:43:40  georgeh
  Added line to clobber BASH_ENV for taint checks (simplifies testing on the command line).

  Revision 1.1  2005/12/19 04:52:45  georgeh
  Initial revision

';

# Taint checks may fail due to the following...
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

if ( @ARGV > 0 ) {
	if( $ARGV[0] eq "-V" ) {
		print STDERR "$rcsid\n";
	} else {
		print STDERR "Nagios plugin for checking the status of Dell chassis sensors using 'omreport'\n";
		print STDERR "Requires Dell Open Manage to be installed.\n";
		print STDERR "\n";
		print STDERR "Usage:\n";
		print STDERR "\t$0 [-V]\n";
		print STDERR "\t\t-V ... print version information\n";
		print STDERR "\n";
	}
}

my ($ok,$tag,$value,$alarm);
my (%alarm,%sensor);

open(OMREPORT,"$omreport chassis|") || do { print "omreport: " . $! ."\n"; exit 2};

$ok="OK";
while ( <OMREPORT> ) {
	if ( $_ !~ /:/ ) {
		next;
	}
	chomp;
	($value,$tag) = split /\s*:\s*/, "$_", 2;
	if ( "$tag" =~ /^(COMPONENT|SEVERITY|)$/ ) {
		next;
	}
	if ( "$value" !~ m/Ok/i ) {
		$alarm = 1;
		$ok="ALARM: ";
		$alarm{$tag} = $alarm;
	} else {
		$alarm = 0;
	}
	($value,) = split /\s+/, "$value", 2;
	$sensor{$tag} = $value . ($alarm?"**":"");
}

print "$ok";
print join ", ", ( sort ( keys %alarm ));
print " --";
foreach $tag ( sort ( keys %sensor )) {
	print " $tag=$sensor{$tag};";
}
print "\n";

if ( $ok eq "OK" ) {
	exit 0;
} else {
	exit 2;
}


