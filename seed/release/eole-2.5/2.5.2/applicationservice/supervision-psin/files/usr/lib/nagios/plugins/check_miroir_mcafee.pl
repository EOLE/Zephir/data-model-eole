#! /usr/bin/perl
#use strict;
use Time::Local;
use Getopt::Long;
#print "Repert=$Repert";

#verif prsence miroir
# Modif PMA 26/03/2015 Prise en compte r epertoire different ddt et autre


if (-e "/usr/share/eole-antivir2/bin/eole-antivir2.j") {
	my $fic="ok";
}else{
    print "EOLE-SER1-C001 Check Miroir MCAFEE CRITIQUE : Miroir NON INSTALLE\n";
    exit 2;
}
my $flag_cron="OK";
my $VerifCron=`grep eole-antivir2.j /etc/cron.d/eole-antivir2`;
if ($VerifCron =~ /eole-antivir2.j/) {
    $flag_cron=substr($VerifCron,0,5);
}else{
    print "EOLE-SER1-C002 Check Miroir MCAFEE CRITIQUE : Crontab Mise a jour Miroir vide\n";
    exit 2;
}
my $Repert="";
my $RepertXml="";
my $repert=`grep 'Anonymous ' /etc/proftpd/conf.d/miroir-mcafee.conf`;
($Repert) = $repert =~ /Anonymous (.*)\>/;
if (-e "$Repert/miroir-mcafee/SiteStat.xml" ) {
	$fic="ok";
    $RepertXml=$Repert."/miroir-mcafee";
}elsif(-e "$Repert/miroir-mcafee-ddt/SiteStat.xml") {
    $fic="ok";
    $RepertXml=$Repert."/miroir-mcafee-ddt";
}else{
    print "EOLE-SER1-C003 Check Miroir MCAFEE : Miroir NON Encore synchronise $Repert/miroir-mcafee(-ddt)/SiteStat.xml \n";
    exit 2;
}

getoptions();

my $LibPer="";
unless ($tps_c) {
	#$tps_c=172800;
	$tps_c=259200;  #72H  50H=180000
	$LibPer=">72 Heures";
}else{
	$LibPer="> $tps_c secondes";
}

my $tps_w=$tps_c;

my $EtatFichier="";
my $CodeEtatFichier="";
my $CodeEtatStatut="";
my $EtatDate="";
my $CodeEtatDate="";

my $CodeEtatFTP="0";
#rechecherche source mAJ
my $source=`grep 'miroir-av' /usr/share/eole-antivir2/bin/eole-antivir2.j`;
(my $SrcMirroir) = $source =~ /miroir-av(.*) \>/;
$SrcMirroir="miroir-av".$SrcMirroir;


#detection miroir local par FR
our $LocMiroir="miroir";

my $LocIP="";
my $action="";
my $action2="";
#print "execution de cat /etc/eole/release \n";
$action=`cat /etc/eole/release`;


if($action =~ /esbl/)
{
    #print "execution de ifconfig eth0 | grep 'inet ad'\n";
    $action2=`/sbin/ifconfig eth0 | grep 'inet ad'`;
    #print "resultat : $action2 \n";
}
if($action =~ /amon/)
{
    my $nbint=`cat /etc/network/interfaces | grep 'address' | wc -l`;
    if($nbint > 2)
    {
        $action2=`ifconfig eth2 | grep 'inet ad'`;
    }
    else
    {
        $action2=`ifconfig eth1 | grep 'inet ad'`;
    }
}

($LocIP) = $action2 =~ /dr:(.*) Bcast/;
#print "execution de sudo host -t CNAME $LocIP\n";
$action=`sudo /usr/bin/host -t CNAME $LocIP`;
#print "Resultat $action\n";


if($action =~ /miroir/)
{
    #print "recup miroir\n";
    ($LocMiroir) = $action =~ /pointer (.*)/;
    #print "$LocMiroir\n";
}
else
{
    $LocMiroir="<font color='blue'><b>Nom DNS NOK</b></font>";
}
#fin partie ajoute par FR


my $check="";
if ($opt_v) {
    	print "Verification connexion ftp /usr/lib/nagios/plugins/check_ftp -v -H $SrcMirroir \n";
	$check="/usr/lib/nagios/plugins/check_ftp -v -t 20 -H $SrcMirroir";
}else{
	$check="/usr/lib/nagios/plugins/check_ftp.pl -t 20 -H $SrcMirroir";
}
my $result=`$check`;
if ($opt_v) {
    print $result;
}
if( $result =~ /OK/) {
    $SrcMirroir=$SrcMirroir." (Reponse OK)";
}else{
    if( $result =~ /invalide/) {
        $SrcMirroir=$SrcMirroir." (EOLE-SER1-C004 PAS de Reponse DNS)";
    }else{
        $SrcMirroir=$SrcMirroir." (EOLE-SER1-C004 PAS de Reponse FTP)";
    }
    $CodeEtatFTP="2";
}

my $check="/usr/lib/nagios/plugins/check_file_age -w $tps_w -c $tps_c -f $RepertXml/SiteStat.xml";
my $result=`$check`;
if ($opt_v) {
    print "Verification date fichier SiteStat\n";
    print $result;
    print `ls -ali $RepertXml/SiteStat.xml`;
    print `sudo tail -n 20 /var/log/miroir-mcafee/miroir-mcafee.log`;
}
if( $result =~ /OK/) {
       ($EtatFichier) = $result =~ /is (.*) seconds/;
       $CodeEtatFichier="0";
}else{
       ($EtatFichier) = $result =~ /is (.*) seconds/;
       $CodeEtatFichier="2";
}
my $der_access=time()-$EtatFichier;
my $datemajfic=`date -d\@$der_access`;
chomp($datemajfic);
#print "$datemaj\n";
VerifSign();
#if($CodeEtatStatut eq "2") {
if($CodeEtatDate eq "2") {
    ##print "Relance Synchro Force ";
    #if ($opt_v) {
    #    print "sudo /usr/share/eole-antivir2/bin/eole-antivir2.j";
    #}
    #my $Relance=`sudo /usr/share/eole-antivir2/bin/eole-antivir2.j`;
    #$CodeEtatDate="0";
    #VerifSign();
}

#verif proftpd
my $EtatFTP="";
#my $check="/usr/lib/nagios/plugins/check_ftp -H localhost";
my $check="ps -edf | grep ftp";
my $result=`$check`;
if( $result =~ /accepting connections/) {
       ($EtatFTP) = " Ecoute FTP OK";
       #$CodeEtatFTP="0";
}else{
        #tentative de relance
       my $relancetfp=`sudo /etc/init.d/proftpd restart`;
       sleep(5);
       my $result2=`$check`;
       if( $result2 =~ /accepting connections/) {
            ($EtatFTP) = " Ecoute FTP OK (Relance Force)";
       }else{
            ($EtatFTP) = " EOLE-SER1-C004 Ecoute FTP CRITIQUE proftpd relance force";
             $CodeEtatFTP="2";
       }
}


if($CodeEtatFichier eq "2") {
    print "Check Miroir MCAFEE CRITIQUE Local $LocMiroir ($EtatFTP) Source $SrcMirroir CRON:$flag_cron Date Fichier:$datemajfic PROBLEME REPLICATION $EtatDate\n";
    exit 2;
}
if($CodeEtatDate eq "2") {
    print "Check Miroir MCAFEE CRITIQUE Local $LocMiroir ($EtatFTP) Source $SrcMirroir CRON:$flag_cron Date Fichier:$datemajfic OK $EtatDate\n";
    exit 2;
}

if($CodeEtatFTP eq "2") {
    print "Check Miroir MCAFEE CRITIQUE Local $LocMiroir ($EtatFTP) Source $SrcMirroir CRON:$flag_cron PROFTPD Date Fichier:$datemajfic OK $EtatDate\n";
    exit 2;
}
print "Check Miroir MCAFEE  OK  Local $LocMiroir ($EtatFTP) Source $SrcMirroir CRON:$flag_cron Date Fichier:$datemajfic $EtatDate\n";
exit 0;


#------------------------------------------------------------------

sub VerifSign() {
$CodeEtatDate="";
$result=`grep Status $RepertXml/SiteStat.xml`;
#print "$result\n";
if ($opt_v) {
    print "Verification fichier SiteStat grep Status $RepertXml/SiteStat.xml\n";
    print $result;
}
my ($DateSign) = $result =~ /CatalogVersion=\"(.*)\">/;
if($DateSign eq "") { $DateSign="19700101000000";}
my $an=substr($DateSign,0,4);
my $mois=substr($DateSign,4,2);
my $j=substr($DateSign,6,2);
my $h=substr($DateSign,8,2);
my $m=substr($DateSign,10,2);
my $s=substr($DateSign,12,2);
my $datecalc=$j.".".$mois.".".$an." ".$h.":".$m.":".$s;
my ($mday,$mon,$year,$hour,$min,$sec) = split(/[\s.:]+/, $datecalc);
my $time = timelocal($sec,$min,$hour,$mday,$mon-1,$year);
#print $time,"\n",scalar localtime $time;
$DateSign=$time;
my $DateSignH=`date -d\@$DateSign`;
chomp($DateSignH);

$DateLimit=time()-$tps_c;
#print "DateSign=$DateSign DateLimit=$DateLimit";
if ($DateSign<$DateLimit) {
    $EtatDate= "Date Signatures $DateSignH DEPASSEE $LibPer";
    $CodeEtatDate="2";
}else{
    $EtatDate= "Date Signatures $DateSignH OK";
}
if($result =~ /Disabled/) {
    $EtatDate= " EOLE-SER1-C005 MIROIR STATUT DESACTIVE Date Signatures $DateSignH DEPASSEE $LibPer";
    $CodeEtatDate="2";
    $CodeEtatStatut="2";
}
}

sub getoptions {
    Getopt::Long::Configure('bundling');
    GetOptions(
       'h|help'            => \$opt_help,
       'c|tpscritique=s'     => \$tps_c,
       'v|verbose'     => \$opt_v,
    ) or do {
        print_usage();
        exit();
        };
    if($opt_help) {
        print_usage();
        exit();
    }
    sub print_usage {
      print <<EOB

Usage:
      check_miroir_macfee.pl [-c|--tpscritique secondes] [-v verbose]

EOB
    }
}

