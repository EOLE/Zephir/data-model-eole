echo Configuration modprop ...
verif=`grep -i ipmi_si /etc/modules |wc -l`
if [ $verif -eq "0" ]; then
	sudo modprobe ipmi_si
	sudo modprobe ipmi_devintf
	sudo modprobe ipmi_watchdog
	sudo sed -i '$aipmi_si' /etc/modules
	sudo sed -i '$aimpi_devintf' /etc/modules
	sudo sed -i '$aipmi_watchdog' /etc/modules
fi


echo Installation cl� HP ...
sudo apt-key add GPG-KEY-ProLiantSupportPack
verif=`grep -i hp /etc/apt/sources.list|wc -l`
if [ $verif -eq "0" ]; then
	sudo sed -i '$adeb http://debmiroir-02.ac.centre-serveur.i2/hp/PSP/ubuntu/ lucid/current non-free' /etc/apt/sources.list
fi
echo Installation des paquets necessaires ...
sudo apt-get update
sudo apt-get install hp-snmp-agents hpsmh hp-smh-templates hpacucli cpqacuxe
#suppression du paquet hp-health pour les Gen8
sudo apt-get remove hp-health
echo Installation terminee , test avec ./check_hparray -s 99 et ./check_hpasm --ignore-dimms
