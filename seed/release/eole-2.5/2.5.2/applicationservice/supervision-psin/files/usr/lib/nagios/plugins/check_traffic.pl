#! /usr/bin/perl -w
###################################################################
# Oreon is developped with GPL Licence 2.0 
#
# GPL License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
#
# Developped by : Julien Mathis - Romain Le Merlus 
#                 Christophe Coraboeuf - Sugumaran Mathavarajan
#
###################################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
#    For information : contact@merethis.com
####################################################################
#
# Script init
#

use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
use FindBin;
use lib "$FindBin::Bin";
use lib "/usr/lib/nagios/plugins";
use utils qw($TIMEOUT %ERRORS &print_revision &support);
if (eval "require centreon" ) {
    use centreon qw(get_parameters);
    use vars qw(%centreon);
    %centreon=get_parameters();
} else {
	print "Unable to load centreon perl module\n";
    exit $ERRORS{'UNKNOWN'};
}
use vars qw($PROGNAME);
use Getopt::Long;
use vars qw($opt_V $opt_h $opt_P $opt_64bits $opt_v $opt_C $opt_b $opt_k $opt_u $opt_p $opt_H $opt_D $opt_i $opt_n $opt_w $opt_c $opt_s $opt_T $opt_r $opt_d $opt_U $opt_E);

# Plugin var init
my $SNMPWALK     = "/usr/bin/snmpwalk";
my $SNMPGET      = "/usr/bin/snmpget";
my($proc, $proc_run, @test, $row, @laste_values, $last_check_time, $last_in_bits, $last_out_bits, @last_values, $update_time, $db_file, $in_traffic, $out_traffic, $in_usage, $out_usage, $statusR);

$PROGNAME = "$0";
sub print_help ();
sub print_usage ();

#print "verif option\n";
Getopt::Long::Configure('bundling');
Getopt::Long::Configure( "pass_through" );
GetOptions
    ("h"   => \$opt_h, "help"         => \$opt_h,
     "u=s"   => \$opt_u, "username=s" => \$opt_u,
     "p=s"   => \$opt_p, "password=s" => \$opt_p,
     "P=s"   => \$opt_P, "--snmp-port=s" => \$opt_P,
     "k=s"   => \$opt_k, "key=s"      => \$opt_k,
     "s"   => \$opt_s, "show"         => \$opt_s,
     "V"   => \$opt_V, "version"      => \$opt_V,
     "i=s" => \$opt_i, "interface=s"  => \$opt_i,
     "64-bits" => \$opt_64bits,
     "n"   => \$opt_n, "name"         => \$opt_n,
     "v=s" => \$opt_v, "snmp=s"       => \$opt_v,
     "C=s" => \$opt_C, "community=s"  => \$opt_C,
     "b=s" => \$opt_b, "bps=s"        => \$opt_b,
     "d=f" => \$opt_d, "debit=f"    => \$opt_d,
     "w=s" => \$opt_w, "warning=s"    => \$opt_w,
     "c=s" => \$opt_c, "critical=s"   => \$opt_c,
     "T=s" => \$opt_T, "DebitLien=s"	  => \$opt_T,
	 "U"   => \$opt_U, "tauxcpu"	  => \$opt_U,
     "E"   => \$opt_E, "ErreurIN"	  => \$opt_E,
     "H=s" => \$opt_H, "hostname=s"   => \$opt_H);


#print "verif option\n";
if ($opt_V) {
    print_revision($PROGNAME,'$Revision: 1.2 $');
  	exit $ERRORS{'OK'};
}

if ($opt_h) {
    print_help();
    exit $ERRORS{'OK'};
	Getopt::Long::Configure('bundling');
}

##################################################
#####      Verify Options
##
if (!$opt_H) {
	print_usage();
	exit $ERRORS{'OK'};
}
my $snmp = "1";
if ($opt_v && $opt_v =~ /^[0-9]$/) {
	$snmp = $opt_v;
}

if ($snmp eq "3") {
if (!$opt_u) {
print "Option -u (--username) is required for snmpV3\n";
exit $ERRORS{'OK'};
}
if (!$opt_p && !$opt_k) {
print "Option -k (--key) or -p (--password) is required for snmpV3\n";
exit $ERRORS{'OK'};
}elsif ($opt_p && $opt_k) {
print "Only option -k (--key) or -p (--password) is needed for snmpV3\n";
exit $ERRORS{'OK'};
}
}

if ($opt_n && !$opt_i) {
    print "Option -n (--name) need option -i (--interface)\n";
    exit $ERRORS{'UNKNOWN'};
}

if (!$opt_C) {
$opt_C = "public";
}

if (!$opt_i) {
$opt_i = 2;
}

if (!$opt_b) {
$opt_b = 95;
}
if (!defined($opt_P)) {
	$opt_P = 161;
}
if ($opt_b =~ /([0-9]+)/) {
my $bps = $1;
}
my $critical = 95;
if ($opt_c && $opt_c =~ /[0-9]+/) {
$critical = $opt_c;
}
my $warning = 80;
if ($opt_w && $opt_w =~ /[0-9]+/) {
$warning = $opt_w;
}
my $interface = 0;
if ($opt_i =~ /([0-9]+)/ && !$opt_n){
    $interface = $1;
} elsif (!$opt_n) {
    print "Unknown -i number expected... or it doesn't exist, try another interface - number\n";
    exit $ERRORS{'UNKNOWN'};
}

if ($critical <= $warning){
    print "(--crit) must be superior to (--warn)";
    print_usage();
    exit $ERRORS{'OK'};
}
if ($opt_64bits && $snmp !~ /2/) {
        print "Error : Usage : SNMP v2 is required with option --64-bits\n";
        exit $ERRORS{'UNKNOWN'};
}
if (defined ($opt_64bits)) {
      if (eval "require bigint") {
        use bigint;
      } else  { print "ERROR : Need bigint module for 64 bit counters\n"; exit $ERRORS{"UNKNOWN"}}
}



#################################################
#####            Plugin snmp requests
##
my $OID_DESC =$centreon{MIB2}{IF_DESC};
#my $OID_OPERSTATUS =$centreon{MIB2}{IF_OPERSTATUS};
my @operstatus = ("up","down","testing", "unknown", "dormant", "notPresent", "lowerLayerDown");




#exit 0;


#print $OID_SPEED;
# Get desctiption table
my $speed_card_th = "Inconnu";
if ($opt_s) {
    #my $result = $session->get_table(Baseoid => $OID_DESC);
	#my $result = `$SNMPWALK -v 1 10.234.15.218 -c metl $OID_DESC`;
	$_= `$SNMPWALK -t 3 -v 1 $opt_H -c $opt_C $OID_DESC`;
	print $_;
	if (!$_) {
	    printf("ERROR: Affichage liste Interface impossible\n");
	    #$session->close;
	    exit $ERRORS{'CRITICAL'};
	}else{
			$_= `$SNMPWALK -t 3 -v 1 $opt_H -c $opt_C $centreon{MIB2}{IF_SPEED}`;
			print $_;
	}
	
	exit $ERRORS{'OK'};
}


if ($opt_U) {
	#print "Recherche CPU";
	#exit $ERRORS{'OK'};
	my $uptime ;
	my $cpu;
	my $pIndex;
	my $result;
	my $OID_CPU = $centreon{MIB2}{IF_CPU};
	my $OID_UPTIME = $centreon{MIB2}{UPTIME_OTHER};
    #my $result = $session->get_table(Baseoid => $OID_DESC);
	#my $result = `$SNMPWALK -v 1 10.234.15.218 -c metl $OID_DESC`;
	my @Result = `$SNMPGET -t 3 -v 1 $opt_H -c $opt_C $OID_CPU $OID_UPTIME`;
	my      $Instance;
    local   $_;
    local   @_;
    local   %_      = ();
	if (!@Result) {
			printf("ERROR: Collectue CPU impossible \n");
			exit $ERRORS{'CRITICAL'};
			}
	for( @Result ) {
        #warn   "114: \$_='$_'\n";
        @_      = split ':';
        $Instance= shift @_;
        $_      = join(':',@_);
		if ($_ =~ m/sysUpTimeInstance/) { 
			m/.+Timeticks:(.+)$/;
			$uptime=$1;
		}
		if ($_ =~ m/enterprises/) { 
			m/.+INTEGER: (.+)$/;
			#m/(\d*)\s$/;
			#print $_;
			$cpu = $1;
		}
	  @{$pIndex}{ $_, $Instance }     = ( $Instance, $_, );
    }

print "Routeur en service depuis ".$uptime." CPU collect� : ".$cpu." %";
print "|cpu=".$cpu."\n";
exit $ERRORS{'OK'};
}






#getting interface using its name instead of its oid index
my $tps1 = time();

#	$_ = `$SNMPWALK -t 5 -v 1 $opt_H -c $opt_C $OID_DESC|grep $opt_i\$`;
#	my $tps = time()-$tps1;
#		if (!$_) {
#			$_ = `$SNMPWALK -t 3 -v 1 $opt_H -c $opt_C $OID_DESC| grep A`;
#			if (!$_) {
#				printf("ERROR: Impossible acceder a la liste des interfaces\n");
#			}else{
#				$_ =~ s/\n//s;
#				#$_ =~ s/\n+/\n/s;
#				printf("ERROR: Interface $opt_i innexistant ($tps s)  Dispo:$_\n");
#			}
#			exit $ERRORS{'CRITICAL'};
#		}
#	m/.+ifDescr\.(.+) =/;
#	#print $_;
#	$interface = $1;
	

	my $ListeI="";
	my $pIndex;
	my $result;
    my      @Result = `$SNMPWALK -t 4 -v 1 $opt_H -c $opt_C $OID_DESC`;
    my $tps = time()-$tps1;
	my      $Instance;
    local   $_;
    local   @_;
    local   %_      = ();
#    if(! defined $Result[0] ) {
#		$_ = `$SNMPWALK -t 3 -v 1 $opt_H -c $opt_C $OID_DESC| grep A`;
#		if (!$_) {
#			printf("ERROR: Impossible acceder a la liste des interfaces\n");
#		}else{
#			$_ =~ s/\n//s;
#			#$_ =~ s/\n+/\n/s;
#			printf("ERROR: Interface $opt_i innexistant ($tps s)  Dispo:$_\n");
#		}
#		exit $ERRORS{'CRITICAL'};
#	}
    for( @Result ) {
        #warn   "114: \$_='$_'\n";
        @_      = split ':';
        $Instance= shift @_;
        $_      = join(':',@_);
		if ($_ =~ m/$opt_i$/) { 
			
			m/.+ifDescr\.(.+) =/;
			$interface = $1;
		}else{
			m/.+STRING:(.+)$/;
			$ListeI=$ListeI." : ".$1;
		}
        @{$pIndex}{ $_, $Instance }     = ( $Instance, $_, );
    }

if($interface==0) {
#$_ = `$SNMPWALK -t 3 -v 1 $opt_H -c $opt_C $OID_DESC| grep A`;
		if ($ListeI eq "") {
			printf("ERROR: Impossible acceder a la liste des interfaces\n");
		}else{
			#$_ =~ s/\n//s;
			#$_ =~ s/\n+/\n/s;
			printf("ERROR: Interface $opt_i innexistant ($tps s)  Dispo:$ListeI\n");
		}
		exit $ERRORS{'CRITICAL'};
}



my ($OID_IN, $OID_OUT, $OID_SPEED, $OID_OPERSTATUS, $OID_IF_IN_ERROR);
if ($opt_64bits) {
    $OID_IN =$centreon{MIB2}{IF_IN_OCTET_64_BITS}.".".$interface;
	$OID_OUT = $centreon{MIB2}{IF_OUT_OCTET_64_BITS}.".".$interface;
	$OID_SPEED = $centreon{MIB2}{IF_SPEED_64_BITS}.".".$interface;
}else {
    $OID_IN =$centreon{MIB2}{IF_IN_OCTET}.".".$interface;
	$OID_OUT = $centreon{MIB2}{IF_OUT_OCTET}.".".$interface;
	$OID_SPEED = $centreon{MIB2}{IF_SPEED}.".".$interface;
	$OID_OPERSTATUS = $centreon{MIB2}{IF_OPERSTATUS}.".".$interface;
	$OID_IF_IN_ERROR = $centreon{MIB2}{IF_IN_ERROR}.".".$interface;
}





if ($opt_E) {
	#print "Recherche CPU";
	#exit $ERRORS{'OK'};
	my $nberrorin ;

	#my $pIndex;
	#my $result;
    #my $result = $session->get_table(Baseoid => $OID_DESC);
	#my $result = `$SNMPWALK -v 1 10.234.15.218 -c metl $OID_DESC`;
	@Result = `$SNMPGET -t 3 -v 1 $opt_H -c $opt_C $OID_IF_IN_ERROR`;
	#my      $Instance;
    #local   $_;
    #local   @_;
    #local   %_      = ();
	if (!@Result) {
			printf("ERROR: Collecte taux erreur IN impossible \n");
			exit $ERRORS{'CRITICAL'};
			}
	for( @Result ) {
        #warn   "114: \$_='$_'\n";
        @_      = split ':';
        $Instance= shift @_;
        $_      = join(':',@_);
		if ($_ =~ m/ifInErrors/) { 
			m/(\d*)\s$/;
			#m/.+: (.+)$/;
			$nberrorin=$1;
		}
		
	  @{$pIndex}{ $_, $Instance }     = ( $Instance, $_, );
    }

print "Verification des Erreurs IN  Taux collect� : ".$nberrorin." ";
print "|errorin=".$nberrorin."\n";
exit $ERRORS{'OK'};
}





# Si interface n'est pas UP , erreur
		#if (!$_) {
	    #printf("ERROR: Recherche status interface $opt_i impossible \n");
	    #exit $ERRORS{'CRITICAL'};
		#}
my $erreur_status=0;	
my $operstatus;
my $in_bits;
my $out_bits;
my $speed_card;

	@Result = `$SNMPGET -t 3 -v 1 $opt_H -c $opt_C $OID_OPERSTATUS $OID_IN $OID_OUT $OID_SPEED`;
		if (!@Result) {
			printf("ERROR: Recherche status interface $opt_i impossible \n");
			exit $ERRORS{'CRITICAL'};
			}
	for( @Result ) {
        #warn   "114: \$_='$_'\n";
        @_      = split ':';
        $Instance= shift @_;
        $_      = join(':',@_);
		if ($_ =~ m/ifOperStatus/) { 
			m/.+INTEGER: (.+)\(/;
			$operstatus=$1;
		}
		if ($_ =~ m/ifInOctets/) { 
			m/(\d*)\s$/;
			#print $_;
			$in_bits = $1;
		}
		if ($_ =~ m/ifOutOctets/) { 
			m/(\d*)\s$/;
			#print $_;
			$out_bits = $1;
		}
		if ($_ =~ m/ifSpeed/) { 
			m/(\d*)\s$/;
			#print $_;
			$speed_card_th = $1;
		}
        @{$pIndex}{ $_, $Instance }     = ( $Instance, $_, );
    }
	

if ($operstatus eq "down" ) {
	$erreur_status=1;
    }

$in_bits =  $in_bits * 8;

$out_bits =  $out_bits * 8;

if (defined($opt_T) && $opt_T != 0){
	#$speed_card = $opt_T * 1000000;
	$speed_card = $opt_T * 1000 ;
} else {
	$speed_card = $speed_card_th;
}

my $aff_speed_card=$speed_card/1000;

#print "Interface:".$opt_i."(".$interface.") - Statut:".$operstatus. "Debit Th:".$speed_card_th." Debit demande:".$speed_card." debit in=".$in_bits." debit out=".$out_bits."\n";


#############################################
#####          Plugin return code
##

$last_in_bits = 0;
$last_out_bits  = 0;
my $last_nb_trafic_nul = 0;

my $flg_created = 0;

if (-e "/var/lib/centreon/centplugins/traffic_if".$interface."_".$opt_H) {
    open(FILE,"<"."/var/lib/centreon/centplugins/traffic_if".$interface."_".$opt_H);
    while($row = <FILE>){
		@last_values = split(":",$row);
		$last_check_time = $last_values[0];
		$last_in_bits = $last_values[1];
		$last_out_bits = $last_values[2];
		$last_nb_trafic_nul = $last_values[3];
		$flg_created = 1;
    }
    close(FILE);
} else {
    $flg_created = 0;
}

my $nb_trafic_nul = 0;
if ($in_bits + $out_bits < 1000) { $nb_trafic_nul=$last_nb_trafic_nul+1;}

$update_time = time();

unless (open(FILE,">"."/var/lib/centreon/centplugins/traffic_if".$interface."_".$opt_H)){
    print "Check mod for temporary file : /var/lib/centreon/centplugins/traffic_if".$interface."_".$opt_H. " !\n";
    exit $ERRORS{"UNKNOWN"};
}
print FILE "$update_time:$in_bits:$out_bits:$nb_trafic_nul";
close(FILE);

if ($flg_created == 0){
    print "First execution : Buffer in creation.... \n";
    exit($ERRORS{"UNKNOWN"});
}


## Bandwith = IN + OUT / Delta(T) = 6 Mb/s
## (100 * Bandwith) / (2(si full duplex) * Ispeed)
## Count must round at 4294967296 
##

#verification du trafic 


if (($in_bits - $last_in_bits != 0) && defined($last_in_bits)) {
	my $total = 0;
	if ($in_bits - $last_in_bits < 0){
		$total = 4294967296 * 8 - $last_in_bits + $in_bits;
	} else {
		$total = $in_bits - $last_in_bits;
	}
	my $diff = time() - $last_check_time;
	if ($diff == 0){$diff = 1;}
    my $pct_in_traffic = $in_traffic = abs($total / $diff);
} else {
    $in_traffic = 0;
} 

if ($out_bits - $last_out_bits != 0 && defined($last_out_bits)) {
    my $total = 0;
    if ($out_bits - $last_out_bits < 0){
		$total = 4294967296 * 8 - $last_out_bits + $out_bits;
    } else {
		$total = $out_bits - $last_out_bits;
    }
    my $diff =  time() - $last_check_time;
    if ($diff == 0){$diff = 1;}
    my $pct_out_traffic = $out_traffic = abs($total / $diff);
} else {
    $out_traffic = 0;
}

if ( $speed_card != 0 ) {
    $in_usage = sprintf("%.1f",($in_traffic * 100) / $speed_card);
    $out_usage = sprintf("%.1f",($out_traffic * 100) / $speed_card);
}

my $in_prefix = "";
my $out_prefix = "";

my $in_perfparse_traffic = $in_traffic;
my $out_perfparse_traffic = $out_traffic;



if ($in_traffic > 1000) {
    $in_traffic = $in_traffic / 1000;
    $in_prefix = "k";
    if($in_traffic > 1000){
		$in_traffic = $in_traffic / 1000;
		$in_prefix = "M";
    }
    if($in_traffic > 1000){
		$in_traffic = $in_traffic / 1000;
		$in_prefix = "G";
    }
}

if ($out_traffic > 1000){
    $out_traffic = $out_traffic / 1000;
    $out_prefix = "k";
    if ($out_traffic > 1000){
		$out_traffic = $out_traffic / 1000;
		$out_prefix = "M";
	}
    if ($out_traffic > 1000){
		$out_traffic = $out_traffic / 1000;
		$out_prefix = "G";
    }
}

#si pas pas de traffic : Erreur
if( $in_traffic==0 and $out_traffic <100) {
	$erreur_status=99;
}

my $in_bits_unit = "";
$in_bits = $in_bits/1048576;
if ($in_bits > 1000){
    $in_bits = $in_bits / 1000;
    $in_bits_unit = "G";
} else { 
    $in_bits_unit = "M";
}

my $out_bits_unit = "";
$out_bits = $out_bits/1048576;
if ($out_bits > 1000){
    $out_bits = $out_bits / 1000;
    $out_bits_unit = "G";
} else {
    $out_bits_unit = "M";
}


if ( $speed_card == 0 ) {
    print "CRITICAL: Interface speed equal 0! Interface must be down.|traffic_in=0B/s traffic_out=0B/s\n";
    exit($ERRORS{"CRITICAL"});
}

#####################################
#####        Display result
##


my $in_perfparse_traffic_str = sprintf("%.1f",abs($in_perfparse_traffic));
my $out_perfparse_traffic_str = sprintf("%.1f",abs($out_perfparse_traffic));

$in_perfparse_traffic_str =~ s/\./,/g;
$out_perfparse_traffic_str =~ s/\./,/g;

my $status = "OK";
my $libstatus = "OK";

if ($opt_i =~ /Dialer/) {
$warning=0;
$critical=0;
$statusR = "OK (RNIS INACTIF)";
}

if(($in_usage > $warning) or ($out_usage > $warning)){
	$libstatus="ELEVE";
	$status = "WARNING";
	if ($opt_i =~ /Dialer/) { $statusR="WARNING (RNIS ACTIF)";}
}
if (($in_usage > $critical) or ($out_usage > $critical)){
	$status = "CRITICAL";
	if($critical<10) {
		$libstatus="ANORMAL";
	}else{
		$libstatus="SATURE";
	}
	if ($opt_i =~ /Dialer/) { $statusR="CRITIQUE (RNIS ACTIF)";}
}

if($erreur_status==1) {
	$status="CRITICAL";
	$libstatus="LIEN DOWN";
}

my $aff_speed_card_th="Inconnu";
if ($speed_card_th eq "Inconnu"){
	$aff_speed_card_th=$speed_card_th;
}else{
	$aff_speed_card_th=$speed_card_th/1000;
}
if ($opt_i =~ /Dialer/) {
	printf("RNIS ".$statusR." Debit:".$aff_speed_card." kb/s Traffic ($opt_i) In : %.2f ".$in_prefix."b/s (".$in_usage." %%), Out : %.2f ".$out_prefix."b/s (".$out_usage." %%) - ", $in_traffic, $out_traffic);
	printf("Total RX Bits In : %.2f ".$in_bits_unit."B, Out : %.2f ".$out_bits_unit."b", $in_bits, $out_bits);
	printf("|traffic_in=".$in_perfparse_traffic_str."Bits/s traffic_out=".$out_perfparse_traffic_str."Bits/s\n");
	exit($ERRORS{$status});
}

#si trafic nul detecte plus de 6 fois ( environ 30 ')
if($nb_trafic_nul > 6 and $critical>10) {
	$status="CRITICAL";
	$libstatus="NUL PAS DE TRAFIC DETECTE";
}
if($erreur_status==99) {
	$status="CRITICAL";
	$libstatus="LIEN DOWN (PAS DE TRAFIC)";
}



#printf($status);
printf("Traffic $libstatus ($opt_i) Debit:".$aff_speed_card." kb/s In : %.2f ".$in_prefix."b/s (".$in_usage." %%), Out : %.2f ".$out_prefix."b/s (".$out_usage." %%) - ", $in_traffic, $out_traffic);
printf("Total RX Bits In : %.2f ".$in_bits_unit."B, Out : %.2f ".$out_bits_unit."b", $in_bits, $out_bits);
printf("|traffic_in=".$in_perfparse_traffic_str."Bits/s traffic_out=".$out_perfparse_traffic_str."Bits/s \n");
exit($ERRORS{$status});


#print "Interface:".$opt_i."(".$interface.") - Statut:".$operstatus. "Debit Th:".$speed_card_th." Debit demande:".$speed_card."\n";
#exit;

sub print_usage () {
    print "\nUsage:\n";
    print "$PROGNAME\n";
    print "   -H (--hostname)   Hostname to query - (required)\n";
    print "   -C (--community)  SNMP read community (defaults to public,\n";
    print "                     used with SNMP v1 and v2c\n";
    print "   -v (--snmp_version)  1 for SNMP v1 (default)\n";
    print "                        2 for SNMP v2c\n";
    print "   -s (--show)       Describes all interfaces number (debug mode)\n";
    print "   -i (--interface)  Set the interface number (2 by default)\n";
    print "   -n (--name)       Allows to use interface name with option -d instead of interface oid index\n";
    print "                     (ex: -i \"eth0\" -n, -i \"VMware Virtual Ethernet Adapter for VMnet8\" -n\n";
    print "                     (choose an unique expression for each interface)\n";
    print "   -w (--warn)       Signal strength at which a warning message will be generated\n";
    print "                     (default 80)\n";
    print "   -c (--crit)       Signal strength at which a critical message will be generated\n";
    print "   -T                Max Banwidth \n";
    print "   -V (--version)    Plugin version\n";
    print "   -r                Regexp Match Mode\n";
	print "   -CPU              Collecte du CPU\n";
	print "   -E                Collecte erreur IN\n";
    print "   -h (--help)       usage help\n";
}

sub print_help () {
    print "##############################################\n";
    print "#    Copyright (c) 2004-2007 Centreon        #\n";
    print "#    Bugs to http://bugs.oreon-project.org/  #\n";
    print "##############################################\n";
    print_usage();
    print "\n";
}
