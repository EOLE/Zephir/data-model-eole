#!/usr/bin/perl
#
my $host=$ARGV[0];

if($host eq "" or $host eq "help" or $host eq "-h" or $host eq "--help") {
	print "synthaxe : check_dns.pl HOST\n";
	exit 0;
}

my $i=0;
my @dns;
my $resultat;
my $status="";
my $CodeSortie=0;
my $tpscrit=5;
my $tpswarn=3;
my $temps=0;
my $perf="";
my $NbCrit=0;

open(FIC,"/etc/resolv.conf") || die ("Erreur d'ouverture de resolv.conf") ;

while (<FIC>) {
	$dns[$i]=$_;
	$dns[$i] =~ s/ //g;
	$dns[$i] =~ s/nameserver//g;
	$dns[$i] =~ s/\n//g;
	#print $dns[$i]; 
	$i++;
}
close(FIC);

while ($k<$i) {
	if ($dns[$k] ne "" and $dns[$k] !~ "search" and  $dns[$k] !~ "#") {
		#print $dns[$k];
		#print "/usr/local/nagios/libexec/check_dns -H $host -s $dns[$k] -c 5";
		$status=`/usr/lib/nagios/plugins/check_dns -H $host -s $dns[$k] -c $tpscrit`;
		#print $status;
		if ($status =~ "timed out") {
			$resultat=$resultat."EOLE-SR-C001 DNS $dns[$k] CRITIQUE (Timeout) ";
			#$CodeSortie=2;
			$NbCrit++;
			$perf="time=0s";
		}elsif($status =~ "CRITIQUE") {
			($temps) = $status =~ /time=(.*)s/;
			$perf="time=$temps\s";
			$resultat=$resultat."EOLE-SR-C001 DNS $dns[$k] CRITIQUE (Temps $temps > $tpscrit s) ";
			$NbCrit++;
			#$CodeSortie=2;
		
		}elsif($status =~ "DNS OK") {
			($temps) = $status =~ /time=(.*)s/;
			$perf="time=$temps\s";
			$resultat=$resultat."DNS $dns[$k] OK ($temps s) ";
			#$CodeSortie=0;

		}
	}
	$k++;
}
if($NbCrit>0) { $CodeSortie=1;}
if($NbCrit>1) { $CodeSortie=2;}

print "Check $resultat |$perf\n";
exit($CodeSortie);

