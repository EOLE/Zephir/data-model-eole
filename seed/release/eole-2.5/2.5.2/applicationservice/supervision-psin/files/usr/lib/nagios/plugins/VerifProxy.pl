#!/usr/bin/perl
#
$version = "1.2";

my $host=`hostname`;



#pour sortie Nagios
my %STATUS_CODE = (  'UNKNOWN'  => '-1',
                     'OK'       => '0',
                     'WARNING'  => '1',
                     'CRITICAL' => '2'   );

my $Globalstatus="OK";

#Recup plateforme de sortie
my $PF="Inconnue";
if( -e "/etc/squid/squid.conf") {
        $verif=`sudo grep 'pfrie-std.proxy' /etc/squid/squid.conf`;
	if($verif =~ /pfrie-std.proxy/) { 
		$PF="RIE";
	}else{
		$PF="RIE";
	}
}
if( -e "/etc/squid3/squid.conf") {
        $verif=`sudo grep 'pfrie-std.proxy' /etc/squid3/common-squid2.conf`;
	if($verif =~ /pfrie-std.proxy/) { 
		$PF="RIE";
	}else{
		$PF="RIE";
	}
}



#-------------------------
#Phase 1: reseau IP Local
#-------------------------

my $status=`/usr/lib/nagios/plugins/monitoring-cete.pl -c /usr/lib/nagios/plugins/cete-config-intra -p1 -d1`;
#print $status;
if ($status =~ /OK/) {
	$status1="OK";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus="Reseau Local OK (".$resu." ms) ";
}elsif ($status =~ /WARNING/) {
	$status1="WARN";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus="Reseau Local WARNING (".$resu." ms) ";
      	$Globalstatus="WARN";
}else {
	$status1="NOT1";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus="ERREUR Reseau Local (".$resu." ms) ";
        $Globalstatus="NOK";
}


#-------------------------
#Phase 2: reseau IP Distant
#-------------------------
$resu="";
my $status=`/usr/lib/nagios/plugins/monitoring-cete.pl -c /usr/lib/nagios/plugins/cete-config-intra -p2 -d2`;
#print $status;
if ($status =~ /OK/) {
	$status2="OK";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus=$libstatus."Reseau Distant OK (".$resu." ms) ";
}elsif ($status =~ /WARNING/) {
	$status2="WARN";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus="Reseau Distant WARNING (".$resu." ms) ";
      	$Globalstatus="WARN";
}else {
	$status2="NOT2";
	($resu) = $status =~ /RTA \= (.*) ms/;
	$libstatus=$libstatus."ERREUR Reseau Distant (".$resu." ms) ";
      $Globalstatus="NOK";
}

#print $libstatus;

#-------------------------
#Phase 3: Internet Google
#-------------------------
#my $type="PLATEFORME MOREA";
my $type="";
#$status=`grep proxy  /usr/lib/nagios/plugins/config-proxy.xml`;
#if($status =~ /100.78/) {
#    $type="PLATEFORME RIE";
#}
$resu="";
$status=`/usr/lib/nagios/plugins/monitoring-cete.pl -c /usr/lib/nagios/plugins/cete-config-proxy -p5 -d5`;
#print $status;
if ($status =~ /OK/) {
	$status3="OK";
	($resu) = $status =~ /succes en (.*) seconds/;
	$libstatus=$libstatus."Internet (Google) OK (".$resu." ms) ";
}else {
	$status3="NOT3";
	$libstatus=$libstatus."ERREUR Internet (Google) (".$resu." ms) ";
      $Globalstatus="NOK";
}

#-------------------------------------
#Verif process squid
#------------------------------------
$resu="";
my $status=`/usr/lib/nagios/plugins/monitoring-cete.pl -c /usr/lib/nagios/plugins/cete-config-intra -p4 -d4`;
#print $status;
if ($status =~ /OK/) {
       $status4="OK";
      ($resu) = $status =~ /PROCS OK: (.*) process/;
      $libstatus=$libstatus."Process Squid OK (".$resu." ) ";
}else {
      $status4="NOT4";
      $Globalstatus="NOK";
      $libstatus=$libstatus."ERREUR Process Squid (".$resu." ) ";
}


#-------------------------------------
#Verif Trafic
#------------------------------------
#
my $perf="";
my $status=`/usr/lib/nagios/plugins/check_traffic_interface.sh eth1`;
if($status =~ /non ACTIF/) {
    $status=`/usr/lib/nagios/plugins/check_traffic_interface.sh eth2`;
}
#print $status;
if ($status =~ /OK/) {
       $status5="OK";
      ($resu) = $status =~ /soit (.*) In:/;
      $resu =~ s/ //;
      ($resu2) = $status =~ /In:.* soit (.*) Erreurs In/;
      $resu2 =~ s/ //;
      if ($resu eq "") {
        $resu="Indisponible";
        $libstatus=$libstatus."Traffic Indisponible ";
      }else{
         $perf = "Trafic_Out=".$resu." Trafic_In=".$resu2;
        $libstatus=$libstatus."Traffic OK (Out:".$resu." In:".$resu2." ) ";
     }
}else{
      $status5="NOT5";
      $Globalstatus="NOK";
      ($resu) = $status =~ /soit (.*) In:/;
      $resu =~ s/ //;
      ($resu2) = $status =~ /In:.* (.*) Erreur In/;
      $resu2 =~ s/ //;
      if ($resu eq "") {
        $resu="Indisponible";
        $perf="";
        $libstatus=$libstatus."Traffic Indisponible ";
      }else{
         $perf = "Trafic_Out=".$resu." Trafic_In=".$resu2;
        $libstatus=$libstatus."Traffic CRITIQUE (Out:".$resu." In:".$resu2." ) ";
      }
 }

#-------------------------------------
#Verif log
#------------------------------------

$resu="";
my $ficlogsquid = "/var/log/rsyslog/local/squid/squid7.info.log";
my $statusL="";
if (-e $ficlogsquid) {
    #print "**NV**";
    $statusL=`/usr/lib/nagios/plugins/check_file_age -f /var/log/rsyslog/local/squid/squid1.info.log -w 200000 -c 216000`;
}else{
    $statusL=`/usr/lib/nagios/plugins/check_file_age -f /var/log/squid/access.log -w 200000 -c 216000`;
}
#my $statusL=`/usr/lib/nagios/plugins/monitoring-cete.pl -c /usr/lib/nagios/plugins/cete-config-proxy -p99 -d99`;
#my $statusL=`/usr/lib/nagios/plugins/check_file_age -f /var/log/rsyslog/local/squid/squid7.info.log -w 200000 -c 216000`;
#print $statusL;
my $ActiviteProxy="YES";
if ($statusL =~ /OK/) {
       $status6="OK";
      ($resu) = $statusL =~ /and (.*) bytes/;
      #$libstatus=$libstatus."LOG OK (".$resu." ) ";
      if ($resu == 0 ) {
      	$libstatus=$libstatus."PAS ACTIVITE PROXY (".$resu." ) ";
      }elsif ($resu > 1800000000 ) {
      	$libstatus=$libstatus."LOG CRITIQUE Taille > 1,8 Go (".$resu." ) ";
       $status6="NOT5";
       $Globalstatus="NOK";
      }else{
      	$libstatus=$libstatus."LOG OK (".$resu." ) ";
      }
}else {
      $status6="NOT5";
      ($resu) = $statusL =~ /and (.*) bytes/;
      if ($resu == 0)  {
      	$Globalstatus="OK";
        $ActiviteProxy="NO";
        $libstatus=$libstatus."PAS ACTIVITE PROXY ( access.log vide ) ";
      }else{
        $Globalstatus="NOK";
        $libstatus=$libstatus."ERREUR LOG access.log (".$resu." ) ";
      }
}

#$libstatus=$libstatus."LOG NON VERIFIE ";
#-------------------------------------
#Verif DansGuardian
#------------------------------------
$resu="";
#my $status=`dansguardian -c /etc/dansguardian/dansguardian0/dansguardian.conf -s`;
my $status=`/usr/lib/nagios/plugins/check_procs -C dansguardian -c 1:200`;
#print $status;
if ($status =~ /OK/) {
       $status7="OK";
      ($resu) = $status =~ /OK: (.*) processus/;
      $libstatus=$libstatus."DANSGUARDIAN OK (".$resu." process ) ";
}else {
      if ($ActiviteProxy eq "YES") {
          $Globalstatus="WARN";
      }
      $status7="NOT7";
      $libstatus=$libstatus."ERREUR DANSGUARDIAN (Pas de process) ";
}
$libstatus="PFS : ".$PF." ".$libstatus;
#Verif cache squid
#$resu="";
#my $status=`/usr/local/nagios/libexec/check_squid.pl -H localhost`;
#if ($status =~ /OK/) {
#       $status7="OK";
#      ($resu) = $status =~ /(.*)\)/ ;
#      $libstatus=$libstatus." ".$resu."  ";
#}else {
#      $Globalstatus="NOK";
#      $status7="NOT7";
#      ($resu) = $status =~ /(.*)\)/ ;
#      $libstatus=$libstatus." ERREUR Cache Squid (".$resu." ) ";
#}




if ($Globalstatus eq "OK") {
      $libstatus="PROXY OK - ".$libstatus;
} elsif ($Globalstatus eq "NOK") {
      $libstatus="EOLE-PRO-C001 PROXY ERREUR - ".$libstatus;
} else {
      $libstatus="EOLE-PRO-W001 PROXY WARNING - ".$libstatus;
}

print $libstatus." ".$type."|".$perf."\n";






#print "status=$Globalstatus";
if ($Globalstatus eq "OK") {
	exit($STATUS_CODE{"OK"});
} elsif ($Globalstatus eq "NOK") {
	exit($STATUS_CODE{"CRITICAL"});
} else {
	exit($STATUS_CODE{"WARNING"});
}
exit();





