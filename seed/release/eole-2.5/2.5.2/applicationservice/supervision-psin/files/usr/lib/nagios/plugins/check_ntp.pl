#! /usr/bin/perl

BEGIN {
	if ($0 =~ m/^(.*?)[\/\\]([^\/\\]+)$/) {
		$runtimedir = $1;
		$PROGNAME = $2;
	}
}

require 5.004;
use POSIX;
use strict;
use Getopt::Long;
use vars qw($opt_V $opt_h $opt_H $opt_w $opt_c $verbose $PROGNAME);
use lib $main::runtimedir;
use utils qw($TIMEOUT %ERRORS &print_revision &support);

sub print_help ();
sub print_usage ();

$ENV{'PATH'}='';
$ENV{'BASH_ENV'}='';
$ENV{'ENV'}='';

Getopt::Long::Configure('bundling');
GetOptions
	("V"   => \$opt_V, "version"    => \$opt_V,
	 "h"   => \$opt_h, "help"       => \$opt_h,
	 "v" => \$verbose, "verbose"  => \$verbose,
	 "w=s" => \$opt_w, "warning=s"  => \$opt_w,
	 "c=s" => \$opt_c, "critical=s" => \$opt_c,
	 "H=s" => \$opt_H, "hostname=s" => \$opt_H);
my $opt_v=$verbose;
if ($opt_V) {
	print_revision($PROGNAME,'$Revision: 1.12 $ ');
	exit $ERRORS{'OK'};
}

if ($opt_h) {
	print_help();
	exit $ERRORS{'OK'};
}

$opt_H = shift unless ($opt_H);
my $host = $1 if ($opt_H && $opt_H =~ m/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+|[a-zA-Z][-a-zA-Z0-9]+(\.[a-zA-Z][-a-zA-Z0-9]+)*)$/);

my $hostlocal="";
my $recupserveur=`/bin/grep '^server' /etc/ntp.conf`;
chomp($recupserveur);
my @serv=split(/ /,$recupserveur);
if($serv[1] ne "") {
    $hostlocal=$serv[1];
}else{
    $hostlocal="ntp.i2";
}
#print "host=".$serv[1]."*";


unless ($host) {
	print_usage();
	exit $ERRORS{'UNKNOWN'};
}

($opt_w) || ($opt_w = shift) || ($opt_w = 60);
my $warning = $1 if ($opt_w =~ /([0-9]+)/);

($opt_c) || ($opt_c = shift) || ($opt_c = 120);
my $critical = $1 if ($opt_c =~ /([0-9]+)/);

my $answer = undef;
my $offset = undef;
my $msg; # first line of output to print if format is invalid

my $state = $ERRORS{'CRITICAL'};
my $ntpdate_error = $ERRORS{'OK'};
my $dispersion_error = $ERRORS{'UNKNOWN'};

my $key = undef;

# Just in case of problems, let's not hang NetSaint
$SIG{'ALRM'} = sub {
	print ("ERROR EOLE-SYS-C004 : No response from ntp server (alarm)\n");
	exit $ERRORS{"CRITICAL"};
};
alarm(40);


###
###
### First, check ntpdate
###
###


#MAIN
Verif();
my $rel1="";
my $flag_relance="";
if($state > 1) {
    if($opt_v) { print "Relance ntp : Arret ntpd \n"};
    $rel1=`/usr/bin/sudo /etc/init.d/ntp stop`;
    sleep(5);
    if($opt_v) { print "Relance ntp : lancement ntpdate sur $host \n"};
    $rel1=`/usr/bin/sudo ntpdate $host`;
    sleep(5);
    if($opt_v) { print "Relance ntp : Relance ntpd \n"};
    $rel1=`/usr/bin/sudo /etc/init.d/ntp start`;
    sleep(5);
    $flag_relance=" Relance NTP effectuee";
    $key = undef;
    $answer = undef;
    $state = $ERRORS{'CRITICAL'};
    Verif();
}

foreach $key (keys %ERRORS) {
	if ($state==$ERRORS{$key}) {
		print ("NTP $key: $answer $flag_relance\n");
		last;
	}
}
exit $state;



sub Verif () {
    if($opt_v) { print "Test /usr/sbin/ntpdate -q $host \n"};
if (!open (NTPDATE, "/usr/sbin/ntpdate -q $host 2>&1 |")) {
	print " Erreur EOLE-SYS-C004 Could not open ntpdate\n";
	exit $ERRORS{"CRITICAL"};
}
my $err_ntp="";
# soak up remaining output; check for error
while (<NTPDATE>) {
    if($opt_v) { print "VERIFICATION RETOUR NTPDATE\n"};

	if (/no server suitable for synchronization found/) { $err_ntp="1";}
	if (/delay 0.00000/) { $err_ntp="1";}
	if ($err_ntp eq "1") {
        if($opt_v) { print "Erreur EOLE-SYS-C004 no server suitable for synchronization found ou offset indefini\n"};
		$ntpdate_error = $ERRORS{"CRITICAL"};
        #test avec hostlocal
        close(NTPDATE);
        $host=$hostlocal;
        if($opt_v) { print "Nouvelle tentative ntpdate sur $host \n"};
        $ntpdate_error = $ERRORS{"OK"};
        if (!open (NTPDATE, "/usr/sbin/ntpdate -q $host 2>&1 |")) {
	        print "Erreur EOLE-SYS-C004 Could not open ntpdate\n";
	        exit $ERRORS{"CRITICAL"};
        }
        while (<NTPDATE>) {
	        if (/Network is down/) {
                if($opt_v) { print "Pas de reseau\n"};
		        $ntpdate_error = $ERRORS{"CRITICAL"};
                 }
	        if (/no server suitable for synchronization found/) {
               		 if($opt_v) { print "no server suitable for synchronization found\n"};
		        $ntpdate_error = $ERRORS{"CRITICAL"};
           	 }
            if($opt_v) { print "VERIFICATION 2 RETOUR NTPDATE\n"};
        	print if ($verbose);
	        $msg = $_ unless ($msg);
	        if ($_ =~ /Network is down/) {
                if($opt_v) { print "Pas de reseau\n"};
		        print ("EOLE-SYS-C004 NTP CRITIQUE : Pas de reseau pour $host Host Conf Locale : $hostlocal \n");
	            exit $ERRORS{"CRITICAL"};
             }
		    print "RESULTAT: $_ Offset=$2\n" if ($verbose);
	        if (/(offset|adjust)\s+([-.\d]+)/i) {
		        $offset = $2;
		        print "RESULTAT: $_ Offset=$2\n" if ($verbose);
		        last;
        	}
        }
     }
    if($opt_v) { print "VERIFICATION 2 RETOUR NTPDATE\n"};
	print if ($verbose);
	$msg = $_ unless ($msg);
		print "RESULTAT: $_ Offset=$2\n" if ($verbose);
	if (/(offset|adjust)\s+([-.\d]+)/i) {
		$offset = $2;
		print "RESULTAT: $_ Offset=$2\n" if ($verbose);
		last;
	}
}


close(NTPDATE);
#print "Retour ntpdate:$?\n" if ($verbose);
# only declare an error if we also get a non-zero return code from ntpdate
#$ntpdate_error = ($? >> 8) || $ntpdate_error;
#$ntpdate_error = $? ;
print "Retour ntpdate:$ntpdate_error\n" if ($verbose);
$dispersion_error=0;
# An offset of 0.000000 with an error is probably bogus. Actually,
# it's probably always bogus, but let's be paranoid here.
if ($ntpdate_error && $offset && ($offset == 0)) { undef $offset;}

if ($ntpdate_error > $ERRORS{'OK'}) {
	$state = $ntpdate_error;
	$answer = "Server for ntp probably down";
     if($opt_v) { print "state=$state answer=$answer offset=$offset\n"; }
	if (defined($offset) && abs($offset) > $critical) {
		$state = $ERRORS{'CRITICAL'};
		$answer = "EOLE-SYS-C004 Erreur detectee et difference de temps $offset seconds superieur a +/- $critical sec host:$host host conf local : $hostlocal";
	} elsif (defined($offset) && abs($offset) > $warning) {
		$answer = "EOLE-SYS-W004 Erreur detecetee et difference de temps $offset seconds superieur a  +/- $warning sec host:$host host conf local : $hostlocal";
	}

} elsif ($dispersion_error > $ERRORS{'OK'}) {
	$state = $dispersion_error;
	$answer = "Dispersion too high\n";
	if (defined($offset) && abs($offset) > $critical) {
		$state = $ERRORS{'CRITICAL'};
		$answer = "EOLE_SYS-C004 Dispersion error and time difference $offset seconds greater than +/- $critical sec host:$host host conf local : $hostlocal";
	} elsif (defined($offset) && abs($offset) > $warning) {
		$answer = "EOLE-SYS-W004 Dispersion error and time difference $offset seconds greater than +/- $warning sec host:$host host conf local : $hostlocal";
	}

} else { # no errors from ntpdate or xntpdc
	if (defined $offset) {
		if (abs($offset) > $critical) {
			$state = $ERRORS{'CRITICAL'};
			$answer = "EOLE-SYS-C004 Difference de temps $offset seconds superieur a +/- $critical sec host:$host host conf local : $hostlocal";
		} elsif (abs($offset) > $warning) {
			$state = $ERRORS{'WARNING'};
			$answer = "EOLE-SYS-W004 Difference de temps $offset seconds superieur a +/- $warning sec host:$host host conf local : $hostlocal";
		} elsif (abs($offset) <= $warning) {
			$state = $ERRORS{'OK'};
			$answer = "Difference de temps de $offset seconds host:$host host conf local : $hostlocal";
		}
	} else { # no offset defined
		$state = $ERRORS{'CRITICAL'};
		$answer = "EOLE-SYS-C004 Invalid format returned from ntpdate ($msg) host:$host host conf local : $hostlocal";
	}
}
}



sub print_usage () {
	print "Usage: $PROGNAME -H <host> [-w <warn>] [-c <crit>]\n";
}

sub print_help () {
	print_revision($PROGNAME,'$Revision: 1.12 $');
	print "Copyright (c) 2000 Bo Kersey/Karl DeBisschop\n";
	print "\n";
	print_usage();
	print "\n";
	print "<warn> = Clock offset in seconds at which a warning message will be generated.\n	Defaults to 60.\n";
	print "<crit> = Clock offset in seconds at which a critical message will be generated.\n	Defaults to 120.\n\n";
	support();
}

