#!/bin/bash


function quoi {
echo "Ce script va installer un noyau 4.4.0-97 et reconfigurer le serveur pour spécifier de booter sur ce noyau"
echo "Tapez (i) pour installer ce noyau ET reconfigurer le serveur,"
echo "Tapez (r) pour installer ce noyau SANS reconfigurer le serveur"
echo "Tapez (q) pour quitter"
echo ""
read -p "Choix [i]:" X
}

function inst_noyau {
Query-Auto -i -S debmiroir-02.ac.centre-serveur.i2 -U debmiroir-02.ac.centre-serveur.i2 -V debmiroir-02.ac.centre-serveur.i2
inst=$(apt-eole install linux-image-4.4.0-97-generic)
inst=$?
inst1=$(apt-eole install linux-image-extra-4.4.0-97-generic)
inst1=$?
if [ $inst = "0" ] || [ $inst1 = "0" ]; then
	if ! [ -e "/usr/share/eole/noyau/local" ]; then
		echo "4.4.0-97-generic" >/usr/share/eole/noyau/local
	else
		echo "il y a déjà un fichier /usr/share/eole/noyau/local : le supprimer et relancer le traitement"
		exit 1
	fi
else
echo "soucis à l'installation des paquets ! "
exit 1
fi
}

quoi

if [ "$X" = "" ] ; then
   X="i"
fi

if [ "$X" = "q" ] ; then
   echo "Abandon du traitement"
   exit 0
fi

if [ "$X" = "r" ] ; then
   inst_noyau
   echo "le noyau est installé, il faut planifier un reconfigure et un reboot pour que le changement soit pris en compte"
   exit 0
fi

if [ "$X" = "i" ] ; then
   inst_noyau
   reconfigure
   echo "le noyau est installé et le serveur reconfiguré, il faut planifier un reboot pour que le changement soit pris en compte"
   exit 0 
fi

if ! [ "$X" = "i" ] || ! [ "$X" = "r" ] || ! [ "$X" = "q" ]; then
	echo "il faut taper i,r ou q : abandon du traitement"
	exit 1
fi





