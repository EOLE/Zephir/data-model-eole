verif=`sudo lshw -C system | grep produit`
vers=`cat /etc/eole/release |grep EOLE_VERSION`
echo Version Eole : $vers Materiel: $verif
read -p "Appuyer sur une touche pour continuer ..."

echo Configuration modprop ...
verif=`grep -i ipmi_si /etc/modules |wc -l`
if [ $verif -eq "0" ]; then
	sudo modprobe ipmi_si
	sudo modprobe ipmi_devintf
	sudo modprobe ipmi_watchdog
	sudo sed -i '$aipmi_si' /etc/modules
	sudo sed -i '$aimpi_devintf' /etc/modules
	sudo sed -i '$aipmi_watchdog' /etc/modules
fi


echo Installation cl� HP ...
wget http://psin.supervision.i2/supervision/psin/fichiers/GPG-KEY-mcp
sudo apt-key add GPG-KEY-mcp
verif=`sudo rm -f /etc/apt/sources.list.d/hp.list`
verif=`sudo  touch /etc/apt/sources.list.d/hp.list`
verif=`sudo chmod 777 /etc/apt/sources.list.d/hp.list`
verif=`grep -i 2.5 /etc/eole/release |wc -l`
verif2=`grep -i 2.4 /etc/eole/release |wc -l`
if [ $verif -eq "2" ]; then
	sudo echo 'deb http://deb2.eole.i2/hp/mcp/ubuntu trusty/current non-free' >/etc/apt/sources.list.d/hp.list
elif  [ $verif2 -eq "2" ]; then
	sudo echo 'deb http://deb2.eole.i2/hp/mcp/ubuntu precise/current non-free' >/etc/apt/sources.list.d/hp.list
else
    echo Version eole incompatible : uniquement 2.4 et 2.5
    exit
fi

echo Installation des paquets necessaires ...
sudo apt-get update
sudo apt-get install binutils lib32gcc1 libc6-i386
echo Installation des paquets HP ...
sudo apt-get install hp-health hpssacli

echo Installation terminee , test avec ./check_hparray -s 99 et ./check_hpasm --ignore-dimms
