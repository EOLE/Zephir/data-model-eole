#!/usr/bin/perl
# version 1.0 10/04/2012
# P Malossane

#Verification si onduleur present ( nut)
#print "ssh -i /usr/local/nagios/.ssh/id_rsa_eole $IP 'sudo grep MONITOR /etc/nut/hosts.conf'";
my $SUps="Non";
if( -e "/etc/nut/hosts.conf") {
	my $Ups=`sudo grep ^MONITOR /etc/nut/hosts.conf`;

	if($Ups =~ /MONITOR/) {
		($NomUps) = $Ups =~ /MONITOR (.*)\@/;
		($IpUps) = $Ups =~ /\@(.*) \"/;
		$SUps="Oui";
	}
}
#$NomUps="bb";
#print "Ups:".$SUps." nom=".$NomUps." ipups=".$IpUps."\n";
if( $SUps eq "Oui") {
	$CheckUps=`/usr/lib/nagios/plugins/check_ups -H $IpUps -u $NomUps -t 30`;
	if($CheckUps =~ /UPS OK/) {
		print $CheckUps;
		exit 0;
	}elsif($CheckUps =~ /WARNING/) {
		print $CheckUps;
		exit 1;
	}elsif($CheckUps =~ /expiration/) {
	    print "ONDULEUR NON JOIGNABLE (Timeout)\n";  #ne pas modifier
	    exit 0;
	}else{
		print $CheckUps;
		exit 2;
	}
}else{
	print "ONDULEUR NON DETECTE\n";  #ne pas modifier
	exit 0;

}

