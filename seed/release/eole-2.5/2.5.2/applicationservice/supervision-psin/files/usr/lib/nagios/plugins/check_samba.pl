#! /usr/bin/perl
#param base de donn�es oracle
#require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
#use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
my $CheckSamba="";
#my $VerifSamba="";
if( -e "/usr/lib/nagios/plugins/ParamCheck.pl") {
   require "/usr/lib/nagios/plugins/ParamCheck.pl";
   #print $VerifSamba;
   if($VerifSamba eq "No") {
            print "Supervision SAMBA non effectuee volontairement (paramcheck)\n";
            exit 0;
   }
   #$CheckSamba=$VerifSamba;
}
use lib "/usr/lib/nagios/plugins";
use utils qw($TIMEOUT %ERRORS );
use Getopt::Long;
use vars qw($opt_h $opt_v $opt_s $opt_d );
sub print_help ();

#print "verif option\n";
GetOptions
    ("h"   => \$opt_h, "--help"         => \$opt_h,
     "s=s"   => \$opt_s, "--serveur=s" => \$opt_s,
     "v" => \$opt_v, "--verbose"       => \$opt_v);

#print "verif option\n";
if (!$opt_s) {
	print "Usage: check_samba.pl -s IP_SERVEUR [-v verbose] \n";
	exit 0;
}
if ($opt_h) {
	print "Usage: check_samba.pl -s IP_SERVEUR [-v verbose] \n";
	exit 0;
}
my $verif2="";
my $debug="-d 1";
if($opt_v) {$debug="-d 4";}

if($opt_v) {print "Verification serveur ".$opt_s."\n";}
if($opt_v) {print "-----------------------------------\n";}
my $CodeSortie="0";
my $librelance="";
my $serveur="";
my $nbsession=` ps -edf | grep smbd | wc -l`;
chomp($nbsession);
my $verif=`sudo /usr/bin/smbclient $debug -U guest -N -L $opt_s 2>&1`;
if($opt_v) {print $verif."\n";}
if($verif =~ /login successful/ or $verif =~ /session request ok/ or $verif =~ /IPC\$/) {
	($serveur) = $verif =~ /Server=(.*)/;
	print "SMB OK Version Serveur : $serveur Nombre de session:$nbsession $verif | nbsession=$nbsession\n";
        exit 0;
}else{

	$verif2=`sudo /usr/bin/smbclient $debug -U guest -N -L localhost 2>&1| grep Anony`;
	if($verif2 =~ /login successful/ or $verif2 =~ /session request ok/ or $verif2 =~ /EEEEechanges/) {
        #($serveur) = $verif2 =~ /Server=(.*)/;
		print "EOLE-SR-C002 SMB OK en localhost mais CRITIQUE en IP Version Serveur : $serveur Nombre de session:$nbsession $verif $verif2| nbsession=$nbsession\n";
       		 exit 1;
	}else{

		if($nbsession eq "0") {
			my $relance=`sudo /etc/init.d/smbd restart`;
        		$librelance=" Relance service samba effectuee";
		}else{
 			if($nbsession eq "1") {
       		         	my $relance=`sudo /etc/init.d/smbd restart`;
       		         	$librelance=" Relance service samba effectuee";
			}
		}
	}
	print "EOLE-SR-C003 SMB CRITICAL Acces partage Samba impossible  Nombre de sessions:$nbsession $librelance $verif $verif2| nbsession=$nbsession \n";
        exit 2;
}
exit 0;

