#!/bin/bash

IPT='sudo /sbin/iptables'
GREP='/bin/grep'
AWK='/usr/bin/awk'
EXPR='/usr/bin/expr'
WC='/usr/bin/wc'

STAT=0
OUTPUT=''
NBCHAINS=`expr $($IPT -nvL | $GREP 'Chain' | $WC -l)`

if [ $NBCHAINS -eq 0 ] ; then
	OUTPUT="EOLE-IPT-C001 IPTABLES CRITIQUE $NBCHAINS chains!  "
	STAT=2
else
	OUTPUT="IPTABLES OK $NBCHAINS chains  "
fi

#echo $OUTPUT
#exit $STAT

CHAINS=`$IPT -nvL | $GREP 'Chain' | $AWK '{ print $2 }'`
for CHAIN in $CHAINS ; do
		CNT=`expr $($IPT -S $CHAIN | $WC -l) '-' 1`
		OUTPUT="${OUTPUT}$CHAIN $CNT rules  "
done

#verif rules / chaine
#for CHAIN in $CHAINS ; do
#	if [ "$CHAIN" != 'FORWARD' ] && [ "$CHAIN" != 'OUTPUT' ] && [ `$EXPR substr $CHAIN 1 4` != "LOG_" ] ; then
#		CNT=`expr $($IPT -S $CHAIN | $WC -l) '-' 1`
#		if [ $CNT -eq 0 ] ; then
#			OUTPUT="${OUTPUT}ERROR $CHAIN $CNT rules!\n"
#			STAT=2
#		else
#			OUTPUT="${OUTPUT}OK $CHAIN $CNT rules\n"
#		fi
#	fi
#done

echo $OUTPUT

exit $STAT

