#!/usr/bin/perl -w
#

# Plugin directory / home of utils.pm.
use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS &print_revision &support &usage);
use Getopt::Long qw(:config no_ignore_case bundling);
use File::Basename;

use strict;

# Path to installed clamd binary.
my $iptable_cmd  = "sudo iptables";
my $warn_val = 1;  # Default -w arg
my $crit_val = 2;  # Default  -c arg
my $help_val = 0;  # Off unless -h arg
my $debug_val = 0;  # Off unless -v arg


sub show_help() {
    print <<END;

Usage: check_iptables_state [-v <verbose>]  [-h]

-h, --help
-v, --verbose
END
}

GetOptions (
    "h" => \$help_val, "help" => \$help_val,
    "v" => \$debug_val, "verbose" => \$debug_val,
);

if ($help_val != 0) {
    &show_help;
    exit 0;
}

my ($total,$diff,$update_time,$row, @last_values, $last_time,$last_VolIntIn,$last_VolIntOut,$last_VolInt1Out, $last_VolInt2Out, $last_VolInt3Out, $last_VolInt4Out, $last_VolInt1In, $last_VolInt2In, $last_VolInt3In, $last_VolInt4In );
my ($Tindic1,$Tindic2,$Tindic3,$Tindic4,$Tindic5);

#recup volume internet out et in
chomp(my $VolInt1Out = `$iptable_cmd -nvL |grep "172.24.24.225        0.0" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt1Out=".$VolInt1Out."\n";}
chomp(my $VolInt1In = `$iptable_cmd -nvL |grep "0.0.0.0/0            172.24.24.225" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt1In=".$VolInt1In."\n";}
chomp(my $VolInt2Out = `$iptable_cmd -nvL |grep "172.24.24.226        0.0" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt2Out=".$VolInt2Out."\n";}
chomp(my $VolInt2In = `$iptable_cmd -nvL |grep "0.0.0.0/0            172.24.24.226" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt2In=".$VolInt1In."\n";}
chomp(my $VolInt3Out = `$iptable_cmd -nvL |grep "172.24.24.227        0.0" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt3Out=".$VolInt3Out."\n";}
chomp(my $VolInt3In = `$iptable_cmd -nvL |grep "0.0.0.0/0            172.24.24.227" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt3In=".$VolInt3In."\n";}
chomp(my $VolInt4Out = `$iptable_cmd -nvL |grep "172.24.24.228        0.0" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt4Out=".$VolInt4Out."\n";}
chomp(my $VolInt4In = `$iptable_cmd -nvL |grep "0.0.0.0/0            172.24.24.228" |awk -F' ' '{print \$2}'`);
if ($debug_val != 0) {print "VolInt4In=".$VolInt4In."\n";}


my $tracerecup="$VolInt1Out:$VolInt2Out:$VolInt3Out:$VolInt4Out:$VolInt1In:$VolInt2In:$VolInt3In:$VolInt4In";


$VolInt1Out  =~ s/K/000/g;
$VolInt2Out  =~ s/K/000/g;
$VolInt3Out  =~ s/K/000/g;
$VolInt4Out  =~ s/K/000/g;
$VolInt1In  =~ s/K/000/g;
$VolInt2In  =~ s/K/000/g;
$VolInt3In  =~ s/K/000/g;
$VolInt4In  =~ s/K/000/g;
$VolInt1Out  =~ s/M/000000/g;
$VolInt2Out  =~ s/M/000000/g;
$VolInt3Out  =~ s/M/000000/g;
$VolInt4Out  =~ s/M/000000/g;
$VolInt1In  =~ s/M/000000/g;
$VolInt2In  =~ s/M/000000/g;
$VolInt3In  =~ s/M/000000/g;
$VolInt4In  =~ s/M/000000/g;
$VolInt1Out  =~ s/G/000000000/g;
$VolInt2Out  =~ s/G/000000000/g;
$VolInt3Out  =~ s/G/000000000/g;
$VolInt4Out  =~ s/G/000000000/g;
$VolInt1In  =~ s/G/000000000/g;
$VolInt2In  =~ s/G/000000000/g;
$VolInt3In  =~ s/G/000000000/g;
$VolInt4In  =~ s/G/000000000/g;

my @Resultat;

if ($VolInt1In eq ""){$VolInt1In=0;}
if ($VolInt2In eq ""){$VolInt2In=0;}
if ($VolInt3In eq ""){$VolInt3In=0;}
if ($VolInt4In eq ""){$VolInt4In=0;}
if ($VolInt1Out eq ""){$VolInt1Out=0;}
if ($VolInt2Out eq ""){$VolInt2Out=0;}
if ($VolInt3Out eq ""){$VolInt3Out=0;}
if ($VolInt4Out eq ""){$VolInt4Out=0;}
my $VolIntIn=$VolInt1In+$VolInt2In+$VolInt3In+$VolInt4In;
my $VolIntOut=$VolInt1Out+$VolInt2Out+$VolInt3Out+$VolInt4Out;
$Resultat[0]=$VolIntIn;
$Resultat[1]=$VolInt1In;
$Resultat[2]=$VolInt2In;
$Resultat[3]=$VolInt3In;
$Resultat[4]=$VolInt4In;
$Resultat[5]=$VolIntOut;
$Resultat[6]=$VolInt1Out;
$Resultat[7]=$VolInt2Out;
$Resultat[8]=$VolInt3Out;
$Resultat[9]=$VolInt4Out;
my @perf;
$perf[0]="Total-In";
$perf[1]="WW225-In";
$perf[2]="WW226-In";
$perf[3]="WW227-In";
$perf[4]="WW228-In";
$perf[5]="Total-Out";
$perf[6]="WW225-Out";
$perf[7]="WW226-Out";
$perf[8]="WW227-Out";
$perf[9]="WW228-Out";

if ($debug_val != 0) { print "Volume Total Out/In :".$VolIntOut."/".$VolIntIn." WW225 Out/In : ".$VolInt1Out."/".$VolInt1In." Volume WW226 Out/In : ".$VolInt2Out."/".$VolInt2In." Volume WW227 Out/In : ".$VolInt3Out."/".$VolInt3In." Volume WW228 Out/In : ".$VolInt4Out."/".$VolInt4In;}

my $flg_created = 0;

if (-e "/tmp/indic_surf.log") {
    open(FILE,"<"."/tmp/indic_surf.log");
    while($row = <FILE>){
       @last_values = split(":",$row);
       $last_time = $last_values[0];
       $last_VolIntOut = $last_values[1];
       $last_VolIntIn = $last_values[2];
       $last_VolInt1Out = $last_values[3];
       $last_VolInt1In = $last_values[4];
       $last_VolInt2Out = $last_values[5];
       $last_VolInt2In = $last_values[6];
       $last_VolInt3Out = $last_values[7];
       $last_VolInt3In = $last_values[8];
       $last_VolInt4Out = $last_values[9];
       $last_VolInt4In = $last_values[10];
       $flg_created = 1;
    }
    close(FILE);
} else {
    $flg_created = 0;
}
my @last_Resultat;
$last_Resultat[0]=$last_VolIntIn;
$last_Resultat[1]=$last_VolInt1In;
$last_Resultat[2]=$last_VolInt2In;
$last_Resultat[3]=$last_VolInt3In;
$last_Resultat[4]=$last_VolInt4In;
$last_Resultat[5]=$last_VolIntOut;
$last_Resultat[6]=$last_VolInt1Out;
$last_Resultat[7]=$last_VolInt2Out;
$last_Resultat[8]=$last_VolInt3Out;
$last_Resultat[9]=$last_VolInt4Out;


$update_time = time();
unless (open(FILE,">"."/tmp/indic_surf.log")){
    print "Check mod for temporary file : /tmp/indic_surf.log !\n";
   exit $ERRORS{"UNKNOWN"};
}
print FILE "$update_time:$VolIntOut:$VolIntIn:$VolInt1Out:$VolInt1In:$VolInt2Out:$VolInt2In:$VolInt3Out:$VolInt3In:$VolInt4Out:$VolInt4In";
close(FILE);
unless (open(FILE,">"."/tmp/hist_indic_surf.log")){
    print "Check mod for temporary file : /tmp/hist_indic_surf.log !\n";
   exit $ERRORS{"UNKNOWN"};
}
print FILE "$update_time:$tracerecup\n";
print FILE "$update_time:$VolIntOut:$VolIntIn:$VolInt1Out:$VolInt1In:$VolInt2Out:$VolInt2In:$VolInt3Out:$VolInt3In:$VolInt4Out:$VolInt4In\n";
close(FILE);

if ($flg_created == 0){
   print "First execution : Buffer in creation.... \n";
   exit($ERRORS{"UNKNOWN"});
}
my @Tindic;
my @Tindicb;
my @Unite;
my @liste=("VolIntIn","VolInt1In","VolInt2In","VolInt3In","VolInt4In","VolIntOut","VolInt1Out","VolInt2Out","VolInt3Out","VolInt4Out");
my $i=0;
$diff = time() - $last_time;
if ($diff == 0){$diff = 1;}
my $message="TRAFIC SORTIE INTERNET :";
my $perfparse="|";
for($i=0; $i<10; $i++) {
   if ($debug_val != 0) {print "Indicateur $liste[$i] : $Resultat[$i] last= $last_Resultat[$i]\n";}
   if (($Resultat[$i] - $last_Resultat[$i] != 0) && defined($last_Resultat[$i])) {
	$Tindic[$i] = 0;
	if ($Resultat[$i] - $last_Resultat[$i] < 0){
	   $total = $Resultat[$i];
	} else {
	   $total = $Resultat[$i] - $last_Resultat[$i];
	}
	#if ($debug_val != 0) {print "total=$total diff=$diff\n";}
	#my $pct_out_traffic = $Tindic1 = abs($total / $diff);
	$Tindic[$i] = abs($total / $diff)*8;   #en bits/s
	$Tindic[$i] = int($Tindic[$i]);
   } else {
       $Tindic[$i] = 0;
   }
   $Unite[$i]="bits/s";
   $Tindicb[$i]=$Tindic[$i];
   if($Tindic[$i]>1000) {
	   $Unite[$i]="kbits/s";
   	   $Tindic[$i]=$Tindic[$i]/1000;
   }
   $message = $message." $perf[$i] : $Tindic[$i] $Unite[$i] ";
   $perfparse = $perfparse." ".$perf[$i]."=".$Tindicb[$i];
}


#print "Resultat:$stat0,$stat1,$stat2,$stat3,$stat4,$stat5,$stat6 \n";
print "$message$perfparse\n";
exit 0;
