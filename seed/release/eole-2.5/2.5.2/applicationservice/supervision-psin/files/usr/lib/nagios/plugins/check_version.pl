#!/usr/bin/perl -w
#
# Plugin directory / home of utils.pm.
#use lib "/usr/local/libexec/nagios";
#use utils qw(%ERRORS &print_revision &support &usage);
use Getopt::Long qw(:config no_ignore_case bundling);

use strict;
my $Message="";
my $test;
my $fic;
my $version_val = "1";
my $type= "ecdl";
my $versplugins = "0.0";
my $verseole = "0.0";
my $vers_eole = "0.0";
my $vers_ecdl = "";
my $Globalstatus = 0;
my $vers_distrib="0.0";
my $versdistrib="0.0";
#pour sortie Nagios
my %STATUS_CODE = (  'UNKNOWN'  => '-1',
                     'OK'       => '0',
                     'WARNING'  => '1',
                     'CRITICAL' => '2'   );

GetOptions (
    "v=s" => \$version_val, "version=i" => \$version_val,
    "t=s" => \$type, "type=i" => \$type
);

if($type eq "") { $type="ecdl";}

if($type eq "ecdl") {
	#version ecdl
	$test = `grep VER /etc/sysconfig/.version`;
	$vers_ecdl = $test =~ /VER=(.+)$/;
	$vers_ecdl="Version ECDL :$1";
}

#------------------------
#Verification version des plugins
#------------------------
$test = `grep Version /usr/lib/nagios/plugins/VersionPlugins.txt`;
chomp($test);
#print "\nRESULTAT1: $test\n";
$versplugins = $test =~ /Version=(.+)$/;
$versplugins=$1;
if($type eq "essl") {
	if ( -e "/etc/eole/version" ) {
		$test = `cat /etc/eole/version`;
	}else{
		$test="Inconnue";
	}

}else{
	if (-e "/usr/share/$type/bin/exxl_version") {
		$test = `/usr/share/$type/bin/exxl_version`;
	}else{
		$test = "Inconnue";
	}
}
chomp($test);
#print "\nRESULTAT1: $test\n";
$verseole = $test;

if($type eq "esbl") {
	$test=`dpkg -l | grep conf-esbl`;
	if($test =~ /eole23/) {
		$verseole = "2.3.15";
	}elsif($test =~ /eole20/) {
		$verseole = "2.3.13";
	}elsif($test =~ /eole19/) {
		$verseole = "2.3.11";
	}elsif($test =~ /eole17/) {
		$verseole="2.3.9";
	}else{
		$test=`dpkg -l | grep eole-esbl-all`;
		if($test =~ /2.4.1/) {
			$verseole = "2.4.1";
		}else{
			$verseole="*** V1 Non suportee ***";
		}
	}
}elsif($type eq "ecdl") {
	$test=`dpkg -l | grep conf-ecdl`;
	if($test =~ /eole16/) {
		$verseole = "2.3.15";
	}elsif($test =~ /eole13/) {
		$verseole = "2.3.13";
	}elsif($test =~ /eole12/) {
		$verseole = "2.3.11";
	}elsif($test =~ /eole9/) {
		$verseole="2.3.9";
	}else{
		$test=`dpkg -l | grep eole-ecdl-all`;
		if($test =~ /2.4.1/) {
			$verseole = "2.4.1";
		}else{
			$verseole="*** V1 Non suportee ***";
		}
	}
}else{
	$test=`dpkg -l | grep eole-common`;
	if($test =~ /eole162/) {
		$verseole = "2.3.15"
	}elsif($test =~ /eole159/) {
		$verseole = "2.3.13"
	}elsif($test =~ /eole101/) {
		$verseole = "2.3.11";
	}elsif($test =~ /eole99/) {
		$verseole="2.3.9";
	}else{
	    $test=`dpkg -l | grep eole-amon-all`;
        if($test =~ /2.4.1/) {
		    $verseole="2.4.1";
        }else{
		    $verseole="*** <2.3.9 ***";
        }
	}
}

if ($versplugins eq $version_val) {
        $Globalstatus=0;
        #chomp($test);
        $Message = "OK : Version Plugins OK ".$version_val." ".$vers_ecdl." Version EOLE : ".$verseole;
}else{
        $Globalstatus=1;
        $Message = "CRITIQUE : Version Plugins Non a jour ".$versplugins."/".$version_val." Version EOLE : ".$verseole." . Lancer une re�installation ";
}


#Verif Version Eole et distrib
my $ficrech="/usr/lib/nagios/plugins/version_eole.txt";
if ( -e $ficrech) {
   	$fic = `grep VERSION_EOLE /usr/lib/nagios/plugins/version_eole.txt`;
	chomp($fic);
	$vers_eole = $fic =~ /VERSION_EOLE=(.+)$/;
	$vers_eole = $1;
	$vers_eole = $verseole;
	if($vers_eole ne $verseole) {
        	#$Globalstatus=1;
        	#$Message = "CRITIQUE : Nouvelle Version EOLE DETECTEE ".$verseole."/".$vers_eole." . Lancer une re�installation ";
	}
}else{
	$fic=`echo 'VERSION_EOLE=$verseole' >/usr/lib/nagios/plugins/version_eole.txt`;
}

if ( -e "/usr/lib/nagios/plugins/version_distrib.txt") {
	$fic = `grep DISTRIB_RELEASE /etc/lsb-release`;
	chomp($fic);
	$versdistrib = $fic =~ /DISTRIB_RELEASE=(.+)$/;
	$versdistrib = $1;
   	$fic = `grep VERSION_DISTRIB /usr/lib/nagios/plugins/version_distrib.txt`;
	$vers_distrib = $fic =~ /VERSION_DISTRIB=(.+)$/;
	$vers_distrib = $1;
    $vers_distrib=$versdistrib;
	if($vers_distrib ne $versdistrib) {
        	$Globalstatus=1;
        	$Message = "CRITIQUE : Nouvelle Version DISTRIB DETECTEE ".$versdistrib."/".$vers_distrib." . Lancer une re�installation ";
	}
}else{
	$fic = `grep DISTRIB_RELEASE /etc/lsb-release`;
	chomp($fic);
	$versdistrib = $fic =~ /DISTRIB_RELEASE=(.+)$/;
	$versdistrib = $1;
	$fic=`echo 'VERSION_DISTRIB=$versdistrib' >/usr/lib/nagios/plugins/version_distrib.txt`;
}
my $messcron="";
my $types="";
my $product="";
my $vendor="";
$vendor=`sudo rm -f /tmp/materiel.log`;
#if($type ne  "essl") {
	#recup type de serveur pour inf
	 my $TimeOut = 20;
	eval {
                local $SIG{ALRM} = sub { die "timeout" };
                alarm $TimeOut;
		$product=`sudo lshw -C system >/tmp/materiel.log`;
	};
alarm 0;
	#chomp($product);
	$vendor = `sudo grep vendor /tmp/materiel.log`;
        chomp($vendor);
	$vendor =~ s/\n/ /g;
	$types = `sudo grep product /tmp/materiel.log`;
        chomp($types);
	$types =~ s/\n/ /g;
#}
if($type eq  "ecdl") {
	#verif crontab
	my $action=`sudo crontab -l -u root | grep statut | wc -l`;
	#print "Nombre de ligne :$action\n";
	if ($action =~ /1/) {
      		$messcron="CRON OK";
	}else{
      		$messcron="ERREUR CRON 0 ou 2 lignes statut ldap";
     		 $Globalstatus=1;
	}
}

my $zeph=`enregistrement_zephir -c`;
chomp($zeph);
my $IdZeph="Non Enregistre";
if($zeph =~ /Serveur enregis/) {
	($IdZeph)=$zeph=~ /\((.*)\)/;
}
if ($Globalstatus == 0) {
        print $Message." $messcron  Version Distrib : $versdistrib Materiel:$vendor $types ZEPHIR $IdZeph\n";
        exit($STATUS_CODE{"OK"});
} else {
        #print $Message."\n";
        print $Message." $messcron  Version Distrib : $versdistrib Materiel:$vendor $types ZEPHIR $IdZeph\n";
        exit($STATUS_CODE{"CRITICAL"});
}
exit();

