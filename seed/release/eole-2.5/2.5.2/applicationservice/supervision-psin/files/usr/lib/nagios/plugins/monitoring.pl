#!/usr/bin/perl 
#
# PSIN P.Malossane Monitoring application avec WEBINJECT
#Version 2.0

$version = "2.0";
use Getopt::Long;
use Net::SMTP;
use Sys::Hostname;
use LWP;
use HTTP::Request::Common;

use XML::Simple;
use Time::HiRes 'time','sleep';
#use DateTime;
getoptions();
my $date = scalar localtime;
$plugin="/usr/local/nagios/libexec";
if($opt_test) {$plugin="/usr/local/nagios/libexec/dev"};
unless ($idtest) {$idtest= 0;}
unless ($host) {
	$host="centreon-p1";
}else{
	$plugin="/usr/lib/nagios/plugins"; # distant sur eole
}
#Sonde pardefaut;
#my $host=hostname;
my $sonde=hostname;
#$host="centreon-p1";


unless ($opt_base) {
	$opt_base = 1;
}else{
	$opt_base = 0;
}



unless ($premiertest) {$premiertest = 1;}
#ancienne version
if($premiertest == 6 ) { $premiertest=1;}


my $heuremail=0;
# si VerifReseau=1 dans fichier config alors on fait un check_traceroute avant sortie

#pour sortie Nagios
my %STATUS_CODE = (  'UNKNOWN'  => '-1',
                     'OK'       => '0',
                     'WARNING'  => '1',
                     'CRITICAL' => '2'   );


unless ($configfile) { # Ausun fichier de config passe en ligne de commande
    $configfile = "cete-config-base";
}
require $configfile || die "Impossible d'ouvrir le fichier de configuration $configfile";
#Param cerbere
#require ("$plugin/paramCERBERE.pl") || die "Impossible d'ouvrir le fichier de configuration paramCERBERE.pl";
$test2=$test;
$test2=~s/ //g;

$TBI_FILE_SQL = "$plugin/".$application."_".$test2;

if($idtest >0) { #site distant
	#$host=hostname;
	#$plugin="/usr/lib/nagios/plugins";
	$opt_base=0;
	$monitoring="2";
	$TBI_FILE_SQL = "$plugin/".$application."_".$idtest;
}

if ($monitoring eq "1" and $opt_base == 1) {
	#$opt_base=1;
	#param base de donn�es oracle
	require ("$plugin/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
}

 
@Phase; @Message; @Time; @Resultat; @contexte; @TempsPage; @TaillePage;
$Ok = "green";
$Warning = "yellow";
$Critical = "red";
$Unknown = "unknown";
$Errscan = "";
$Serveur = "";
$ServeurApp = "";
$Indic1="0";
$Indic2="0";
$Maintenance_active="0";
$libelle = "";
$UrlTest="";
$Erreur="";
$perf="";
$MessageDNS="";
$PhaseDNS="";
$TimeDNS=0;
$TimeTCP=0;
$IPRP="";
$ErreurBdd="0";
$LibErreurBdd="";
our ($currentdatetime, $totalruntime, $startruntimer, $endruntimer);
$startruntimer=0;
$endruntimer=0;

$Globalstatus = "$Critical";

#seuil temps reponse par defaut si non definie
unless ($tps_warn) {$tps_warn = 7;}
unless ($tps_crit) {$tps_crit = 12;}
unless ($Maint) {$Maint="";}
unless ($Contremesure) {$Contremesure="yes";}
unless ($TpsContremesure) {$TpsContremesure=30;}
#$tps_warn=7;
#$tps_crit=12;
unless ($VerifReseau) {$VerifReseau="0";}

unless ($NomPerf) {$nperf="time";
}else{
$nperf=$NomPerf;
}
unless ($ResultPerf) {$rperf="timetemptot";
}else{
$rperf=$ResultPerf;
}
unless ($UnitePerf) {$uperf="s";
}else{
$uperf=$UnitePerf;
}
$taille=0;
$debit=0;
$motrech="";
$testnum = 0;
$NbTest = 1;
$timetemptot = 0;
$heurerelance=0;
use POSIX qw(strftime);
#    $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;
$now_string = strftime "%d-%m-%Y %H:%M:%S", localtime;
$now_string2 = strftime "%d/%m/%Y", localtime;
$datetest = $now_string;
$datetest2 = $now_string2;
$datetestT = time;
$applibb=$dnsatester;
$aj = strftime "%d/%m/%Y %H:%M:%S", localtime;

#unless ($portail) {$portail = 1;}

#insert acai avec valeur � null
$datemesureI=$datetest;
$Erreur="ACCES IMPOSSIBLE (timeout Nagios) ";
#insert_acai();
#insert_travail();
#$Erreur="";

#------------------------
#Verification acces  
#------------------------ 
if($debug) { print "Verification Sortie :\n";} 
if( $dnsatester =~ /i2/ or $dnsatester =~ /ader/) {
	if($debug) { print "$plugin/check_http_curl.pl -u intra.portail.i2 -r\n";} 
	$TestSortie = `$plugin/check_http_curl.pl -u intra.portail.i2 -r`;
}else{
	if($debug) { print "$plugin/check_http_curl.pl -u google.fr -r\n";} 
	$TestSortie = `$plugin/check_http_curl.pl -u www.google.fr -r`;
}
if($debug) { print "$TestSortie\n";} 
if ($TestSortie =~ /CRITIQUE/) {
	if( $dnsatester =~ /i2/) {
		#$TestSortie = `$plugin/check_http_curl.pl -u cerbere.application.i2 -r`;
		$TestSortie = `$plugin/check_http_curl.pl -u 10.167.150.110 -r`;
	}else{
		$TestSortie = `$plugin/check_http_curl.pl -u fr.yahoo.com -r`;
	}
	if ($TestSortie =~ /CRITIQUE/) {
		print "Verification $application $test2 ABANDONNEE : Acces impossible a Internet (google) . Verifier le lien de sortie - ($application-$test)| time=0;0;;0 t1=0s t2=0s t3=0s t4=0s t5=0s t6=0s t7=0s t8=0s  \n";	
		exit 2;
	}else{
		if($debug) { print "Test Sortie OK $TestSortie\n";}
	}
}



#------------------------
#Resolution DNS et test port tcp 80 
#------------------------ 
 if($debug) { print "test DNS :";} 
#ATTENTION, si la resolution est active dans le cache, il se peut qu'un probleme DNS ne soit pas detecte.
# videe le cache DNS
my	$TraitD	= '-' x 60;
#$VideDns = `rndc flush`;
$TimeDNS=0;
$TimeTCP=0;
my $IPRP="";
$MessageDNS = "CRITIQUE";
 if($debug) { print "$plugin/check_dns -H $dnsatestea -t 5r\n";} 
 if($debug) { print "$plugin/check_http_curl.pl -u $dnsatester";}
#$dns = `$plugin/check_dns -H $dnsatester -t 5`;
$dns = `$plugin/check_http_curl.pl -u $dnsatester`;
#print $dns;
 if($debug) { print "$dns\n";} 
if ($dns =~ /DNS OK/) { 
	$PhaseDNS = "$Ok"; 
	$MessageDNS = "OK ";
	#($TimeDNS) = $dns =~ /DNS OK: (.*) second/; 
	($TimeDNS) = $dns =~ /tdns=(.*) ttcp/; 
	($TimeTCP) = $dns =~ /ttcp=(.*) ttotal/;
	#($IPRP) = $dns =~ /renvoie (.*)\|/;
}
if ($dns =~ /CRITICAL/) { $PhaseDNS = "$Critical"; $MessageDNS = "Erreur resolution dns";};
$TimeDNS=~ s/,/./; 
#$TimeTCP=0;
if($TimeDNS eq "") { $TimeDNS=0;}
if($TimeTCP eq "") { $TimeTCP=0;}
#print "TIMEDNS=$TimeDNS TCP=$TimeTCP";
#$MessageDNS="Check DNS ".$MessageDNS." en ".$TimeDNS. " s (".$IPRP.") ";
$MessageDNS="Check DNS ".$MessageDNS." en ".$TimeDNS. " s ";
$Erreur .= $MessageDNS;
print $MessageDNS;
$test2=$test;
$test2=~s/ //g;
my $TBI_FILE_DEBUG = "$plugin/".$application."_".$test2;
        open(FILEDEBUG,">".$TBI_FILE_DEBUG.".log") or die "Can't open $TBI_FILE_DEBUG for writing: $!";
        print FILEDEBUG "Trace check ".$application." (".$test.") au ".$now_string."\n";
	print FILEDEBUG $TraitD."\nCheck DNS\n".$TraitD."\nDNS :".$dns."\n";


if($opt_tcp) {
	$MessageTCP=" ";
}else{
	 #if($debug) { print "Calcul temps tcp\n";} 
	 #if($debug) { print "Commande de check : $plugin/check_tcp -H $dnsatester -p 80\n";} 
	#$tcpcon=`$plugin/check_tcp -H $dnsatester -p 80`;
 	#if($debug) { print "Resultat : $tcpcon\n";} 
	#if ($tcpcon =~ /TCP OK/) { $PhaseTCP = "$Ok"; $MessageTCP = "OK ";($TimeTCP) = $tcpcon =~ /TCP OK - (.*) second/; };
	#if ($tcpcon =~ /CRITIQUE/) { $PhaseTCP = "$Critical"; $MessageTCP = "Erreur connexion tcp port 80";};
	#$MessageTCP="Check TCP ".$MessageTCP." en ".$TimeTCP. " s ";
	$MessageTCP="TCP en ".$TimeTCP. " s ";
	$Erreur .= $MessageTCP;
	#print FILEDEBUG $TraitD."\nCheck TCP Connection\n".$TraitD."\nTCP PORT 80 :".$tcpcon."\n\n";
	#$TimeTCP=~ s/,/./; 
	#print $TimeTCP;
}
if($debug) { print "Temps DNS= $TimeDNS Temps TCP=$TimeTCP\n";}
$datetestT = time;
if($opt_base == 1 ) {
	#insert_travail();
}
$Erreur="";


if ($premiertest < 99) {
#----------------------------------
#test application   
#----------------------------------

$testnum++;  #1

#print "passage $testnum";
 if($debug) { print "passage $testnum\n";} 


#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;
print FILEDEBUG "Commande de check :".$testphase."\n";
 if($debug) { print "Commande de check : $testphase\n";} 

my $TimeOut = 90;

eval {
        local $SIG{ALRM} = sub { die "timeout" };
        alarm $TimeOut; 

	$Resultatwebinject = `$testphase`;
};
alarm 0;
if($@ and $@=~ /timeout/) {
	cree_fichier_timeout();
	$LibErreurBdd="";
	if($ErreurBdd eq "1") {
		$LibErreurBdd="PROBLEME MAJ BASE DE DONNEES ACAI";
	}	
	print "Verification $application $test2 CRITIQUE TimeOut $TimeOut s $LibErreurBdd | time=0;0;;0 t1=0s t2=0s t3=0s t4=0s t5=0s t6=0s t7=0s t8=0s \n";	
	exit 2;
}
 if($debug) { print "Resultat : $Resultatwebinject\n";} 

#Contremesure
if ($Resultatwebinject =~ /WebInject CRITICAL/ and $Contremesure eq "yes") {
	print "CM ";
	sleep($TpsContremesure);
	eval {
        	local $SIG{ALRM} = sub { die "timeout" };
        	alarm $TimeOut; 

		$Resultatwebinject = `$testphase`;
	};
	alarm 0;
	if($@ and $@=~ /timeout/) {
		cree_fichier_timeout();
		$LibErreurBdd="";
		if($ErreurBdd eq "1") {
			$LibErreurBdd="PROBLEME MAJ BASE DE DONNEES ACAI";
		}	
		print "Verification $application $test2 CRITIQUE TimeOut $TimeOut s $LibErreurBdd | time=0;0;;0 t1=0s t2=0s t3=0s t4=0s t5=0s t6=0s t7=0s t8=0s \n";	
		exit 2;
	}	
}


 if($debug) { print "Resultat Contremesure : $Resultatwebinject\n";} 
$NbTest=0;
if ($Resultatwebinject =~ /WebInject OK/) { 
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/; 
	#print "timetemp=$timetemp";
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/; 
	($Serveur) = $Resultatwebinject =~ /Serveur:(.*) ServeurApp:/; 
	($ServeurApp) = $Resultatwebinject =~ /ServeurApp:(.*) ID1/; 
	($Indic1) = $Resultatwebinject =~ /ID1=(.*) ID2/; 
	($Indic2) = $Resultatwebinject =~ /ID2=(.*) \|/; 
	($debit) = $Resultatwebinject =~ /Debit:(.*) kb/; 
	($motrech) = $Resultatwebinject =~ /MOT:(.*)- taille/; 
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/; 
	#($Libelle) = $Resultatwebinject =~ /(.*)taille/; 
	if($nombredetests >=1) { ($TempsPage[1]) = $Resultatwebinject =~ /T0=(.*) P0/; }
	if($nombredetests >=2) { ($TempsPage[2]) = $Resultatwebinject =~ /T1=(.*) P1/; }
    if($nombredetests >=3) { ($TempsPage[3]) = $Resultatwebinject =~ /T2=(.*) P2/; }	
	if($nombredetests >=4) { ($TempsPage[4]) = $Resultatwebinject =~ /T3=(.*) P3/; }	
	if($nombredetests >=5) { ($TempsPage[5]) = $Resultatwebinject =~ /T4=(.*) P4/; }	
	if($nombredetests >=6) { ($TempsPage[6]) = $Resultatwebinject =~ /T5=(.*) P5/; }
	if($nombredetests >=7) { ($TempsPage[7]) = $Resultatwebinject =~ /T6=(.*) P6/; }	
	if($nombredetests >=8) { ($TempsPage[8]) = $Resultatwebinject =~ /T7=(.*) P7/; }
	if($nombredetests >=9) { ($TempsPage[9]) = $Resultatwebinject =~ /T8=(.*) P8/; }
	if($nombredetests >=10) { ($TempsPage[10]) = $Resultatwebinject =~ /T9=(.*) P9/; }
	if($nombredetests >1) { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*)- T1/; } else { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*) -/; }
	if($nombredetests >2) { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*)- T2/; } else { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*) -/; }
    if($nombredetests >3) { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*)- T3/; } else { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*) -/; }	
	if($nombredetests >4) { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*)- T4/; } else { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*) -/; }
	if($nombredetests >5) { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*)- T5/; } else { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*) -/; }
	if($nombredetests >6) { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*)- T6/; } else { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*) -/; }
	if($nombredetests >7) { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*)- T7/; } else { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*) -/; }
	if($nombredetests >8) { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*)- T8/; } else { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*) -/; }
	if($nombredetests >9) { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*)- T9/; } else { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*) -/; }
	if($nombredetests >10) { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; } else { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; }
	for ($i = $testnum; $i <= $nombredetests; $i++) {
		$NbTest++;
		$Time[$i] = $TempsPage[$i];
		#Verification seuils
		if ($timetemp >= $tps_crit ) {
			$Phase[$i] = "$Critical";
			$libelle = "DEGRADE Seuil Maximum $tps_crit s depasse : $timetemp secondes - Taille : $TaillePage[$i] Octets"; 
		}elsif ($timetemp >= $tps_warn ) {
			$Phase[$i] = "$Warning";
			$libelle = "WARNING Seuil warning $tps_warn s depasse :  $timetemp secondes - Taille : $TaillePage[$i] Octets"; 
		}else{
			$Phase[$i] = "$Ok";
			$libelle = "OK Page accede avec succes en $Time[$i] secondes - Taille : $TaillePage[$i] Octets"; 
		}
		#$Message[$i] = "$Resultatwebinject";
		#$Time[$i] = $timetemp;
		#$libelle = "OK Page accede avec succes en $Time[$i] secondes - Taille : $TaillePage[$i] Octets"; 
		$Message[$i] = $libelle;
		#print "LIBELLE=".$libelle."\n";
		#$perf=$perf." t$i=$Time[$i]s p$i=$TaillePage[$i]o";
		$perf=$perf." t$i=$Time[$i]s";
		}
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) { 
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/; 
	#print "timetemp=$timetemp";
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/; 
	($Serveur) = $Resultatwebinject =~ /Serveur:(.*) ServeurApp:/; 
	($ServeurApp) = $Resultatwebinject =~ /ServeurApp:(.*) ID1/; 
	($Indic1) = $Resultatwebinject =~ /ID1=(.*) ID2/; 
	($Indic2) = $Resultatwebinject =~ /ID2=(.*) \|/; 
	($debit) = $Resultatwebinject =~ /Debit:(.*) kb/; 
	($motrech) = $Resultatwebinject =~ /MOT:(.*)- taille/; 
	($Errscan) = $Resultatwebinject =~ /Err=(.*)Mess=/; 
	#($Libelle) = $Resultatwebinject =~ /(.*)taille/; 
	if($nombredetests >=1) { ($TempsPage[1]) = $Resultatwebinject =~ /T0=(.*) P0/; }
	if($nombredetests >=2) { ($TempsPage[2]) = $Resultatwebinject =~ /T1=(.*) P1/; }
    if($nombredetests >=3) { ($TempsPage[3]) = $Resultatwebinject =~ /T2=(.*) P2/; }	
	if($nombredetests >=4) { ($TempsPage[4]) = $Resultatwebinject =~ /T3=(.*) P3/; }	
	if($nombredetests >=5) { ($TempsPage[5]) = $Resultatwebinject =~ /T4=(.*) P4/; }	
	if($nombredetests >=6) { ($TempsPage[6]) = $Resultatwebinject =~ /T5=(.*) P5/; }
	if($nombredetests >=7) { ($TempsPage[7]) = $Resultatwebinject =~ /T6=(.*) P6/; }	
	if($nombredetests >=8) { ($TempsPage[8]) = $Resultatwebinject =~ /T7=(.*) P7/; }
	if($nombredetests >=9) { ($TempsPage[9]) = $Resultatwebinject =~ /T8=(.*) P8/; }
	if($nombredetests >=10) { ($TempsPage[10]) = $Resultatwebinject =~ /T9=(.*) P9/; }
	if($nombredetests >1) { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*)- T1/; } else { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*) -/; }
	if($nombredetests >2) { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*)- T2/; } else { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*) -/; }
    if($nombredetests >3) { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*)- T3/; } else { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*) -/; }	
	if($nombredetests >4) { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*)- T4/; } else { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*) -/; }
	if($nombredetests >5) { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*)- T5/; } else { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*) -/; }
	if($nombredetests >6) { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*)- T6/; } else { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*) -/; }
	if($nombredetests >7) { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*)- T7/; } else { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*) -/; }
	if($nombredetests >8) { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*)- T8/; } else { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*) -/; }
	if($nombredetests >9) { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*)- T9/; } else { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*) -/; }
	if($nombredetests >10) { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; } else { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; }
	for ($i = $testnum; $i <= $nombredetests; $i++) {
		$NbTest++;
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
		$Time[$i] = $TempsPage[$i];
		$libelle = "WARNING Page accede avec succes en $Time[$i] secondes - Taille : $TaillePage[$i] Octets"; 
		$Message[$i] = $libelle;
		#print $libelle;
		#$perf=$perf." t$i=$Time[$i]s p$i=$TaillePage[$i]o";
		$perf=$perf." t$i=$Time[$i]s";
		}
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
	($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
	($taille) = 0; 
	($Serveur) = $Resultatwebinject =~ /Serveur:(.*) ServeurApp:/; 
	($ServeurApp) = $Resultatwebinject =~ /ServeurApp:(.*) ID1/; 
	($Indic1) = $Resultatwebinject =~ /ID1=(.*) ID2/; 
	($Indic2) = $Resultatwebinject =~ /ID2=(.*) \|/; 
	($debit) = 0; 
	($motrech) = $Resultatwebinject =~ /MOT:(.*)- taille/; 
	($Errscan) = $Resultatwebinject =~ /Err=(.*)Mess=/; 
	if ($Resultatwebinject =~ /(MAINTENANCE)/) {
		$Errscan="MAINTENANCE APPLICATION EN COURS ".$Errscan;
		$Maintenance_active="1";
	} 
	if ($Resultatwebinject =~ /(HTTP Code)/) { 
		#print "ERREUR HTTP CODE";
		$timetemp=0;
		if($nombredetests >=1) { ($TempsPage[1]) = 0; }
		if($nombredetests >=2) { ($TempsPage[2]) = 0; }
    		if($nombredetests >=3) { ($TempsPage[3]) = 0; }	
		if($nombredetests >=4) { ($TempsPage[4]) = 0; }	
		if($nombredetests >=5) { ($TempsPage[5])= 0; }	
		if($nombredetests >=6) { ($TempsPage[6])= 0; }
		if($nombredetests >=7) { ($TempsPage[7])= 0; }	
		if($nombredetests >=8) { ($TempsPage[8])= 0; }
		if($nombredetests >=9) { ($TempsPage[9])= 0; }
		if($nombredetests >=10) { ($TempsPage[10])= 0; }
		if($nombredetests >1) { ($TaillePage[1]) = 0; }
		if($nombredetests >2) { ($TaillePage[2]) = 0; }
    		if($nombredetests >3) { ($TaillePage[3]) = 0; }	
		if($nombredetests >4) { ($TaillePage[4]) = 0; }
		if($nombredetests >5) { ($TaillePage[5]) = 0; }
		if($nombredetests >6) { ($TaillePage[6]) = 0; }
		if($nombredetests >7) { ($TaillePage[7]) = 0; }
		if($nombredetests >8) { ($TaillePage[8]) = 0; }
		if($nombredetests >9) { ($TaillePage[9]) = 0; }
		if($nombredetests >10) { ($TaillePage[10]) = 0; }
	}else{
	#($timetemp) = $Resultatwebinject =~ /\|time=(.*) T/;
		$timetemp=0;
		if($nombredetests >=1) { ($TempsPage[6]) = $Resultatwebinject =~ /T0=(.*) P0/;$timetemp=$timetemp+$TempsPage[1]; }
		if($nombredetests >=2) { ($TempsPage[7]) = $Resultatwebinject =~ /T1=(.*) P1/;$timetemp=$timetemp+$TempsPage[2]; }
    		if($nombredetests >=3) { ($TempsPage[8]) = $Resultatwebinject =~ /T2=(.*) P2/;$timetemp=$timetemp+$TempsPage[3]; }	
		if($nombredetests >=4) { ($TempsPage[9]) = $Resultatwebinject =~ /T3=(.*) P3/;$timetemp=$timetemp+$TempsPage[4]; }	
		if($nombredetests >=5) { ($TempsPage[10]) = $Resultatwebinject =~ /T4=(.*) P4/;$timetemp=$timetemp+$TempsPage[5]; }	
		if($nombredetests >=6) { ($TempsPage[11]) = $Resultatwebinject =~ /T5=(.*) P5/;$timetemp=$timetemp+$TempsPage[6]; }
		if($nombredetests >=7) { ($TempsPage[12]) = $Resultatwebinject =~ /T6=(.*) P6/;$timetemp=$timetemp+$TempsPage[7]; }	
		if($nombredetests >=8) { ($TempsPage[13]) = $Resultatwebinject =~ /T7=(.*) P7/;$timetemp=$timetemp+$TempsPage[8]; }
		if($nombredetests >=9) { ($TempsPage[14]) = $Resultatwebinject =~ /T8=(.*) P8/;$timetemp=$timetemp+$TempsPage[9]; }
		if($nombredetests >=10) { ($TempsPage[15]) = $Resultatwebinject =~ /T9=(.*) P9/;$timetemp=$timetemp+$TempsPage[10]; }
		if($nombredetests >1) { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*)- T1/; } else { ($TaillePage[1]) = $Resultatwebinject =~ /P0=(.*) -/; }
		if($nombredetests >2) { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*)- T2/; } else { ($TaillePage[2]) = $Resultatwebinject =~ /P1=(.*) -/; }
    		if($nombredetests >3) { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*)- T3/; } else { ($TaillePage[3]) = $Resultatwebinject =~ /P2=(.*) -/; }	
		if($nombredetests >4) { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*)- T4/; } else { ($TaillePage[4]) = $Resultatwebinject =~ /P3=(.*) -/; }
		if($nombredetests >5) { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*)- T5/; } else { ($TaillePage[5]) = $Resultatwebinject =~ /P4=(.*) -/; }
		if($nombredetests >6) { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*)- T6/; } else { ($TaillePage[6]) = $Resultatwebinject =~ /P5=(.*) -/; }
		if($nombredetests >7) { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*)- T7/; } else { ($TaillePage[7]) = $Resultatwebinject =~ /P6=(.*) -/; }
		if($nombredetests >8) { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*)- T8/; } else { ($TaillePage[8]) = $Resultatwebinject =~ /P7=(.*) -/; }
		if($nombredetests >9) { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*)- T9/; } else { ($TaillePage[9]) = $Resultatwebinject =~ /P8=(.*) -/; }
		if($nombredetests >10) { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; } else { ($TaillePage[10]) = $Resultatwebinject =~ /P9=(.*) -/; }
	}
	#for ($i = $testnum; $i <= $nombredetests; $i++) {
	while ($testnum <= $nombredetests) {
		$NbTest++;
		if ($testnum < $phasecritique) {
			$Phase[$testnum] = "$Ok";
			$Message[$testnum] = "Phase $testnum passe avec succes";
		}
		elsif ($testnum == $phasecritique) {
			$Phase[$testnum] = "$Critical";
			$Message[$testnum] = "$Resultatwebinject";
			$Time[$testnum] = $TempsPage[$testnum];
			$libelle = "CRITICAL $Errscan "; 
			$Message[$testnum] = $libelle;
			#print "LIBELLE :".$libelle." **********";
			#$perf=$perf." t$i=$Time[$i]s p$i=$TaillePage[$i]o";
			$perf=$perf." t$testnum=$Time[$testnum]s";
		}
		elsif ($testnum > $phasecritique) {
			$Phase[$testnum] = "$Unknown";
			$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
		}
		$testnum++;
	}
}	
$timetemptot=$timetemp;

}  # fin testnum <> 99 et 100

#---------------------------------------------------------------------------------------------------------------
#Phase 99: Traffic Proxy (phase ind�pendante lanc�e seule avec option -p99 -d99 et r�serv�e supervision locale 
#---------------------------------------------------------------------------------------------------------------
$testnum = 99;
if ($premiertest == 99) {

	$trafic = `$plugin/check_graph_traffic_metl.pl -H $proxy -C public -i 2 `;
	#print "\nRESULTAT PING LOCAL: $trafic\n";
	if ($trafic =~ /WARNING TRAFIC IN/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /WARNING TRAFIC OUT/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /CRITICAL TRAFIC IN/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /CRITICAL TRAFIC OUT/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /No response/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = 0;}
	else { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "TRAFFIC OK ".$trafic;($Time[$testnum]) = $trafic =~ /Traffic (.*)\-/;}
	$Message[$testnum]="Trafic Proxy local : ".$Message[$testnum];
	$timetemptot=$Time[$testnum];
	#warn "message=$Message[$testnum] temps=$timetemptot";	
	preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}


#---------------------------------------------------------------------------------------------------------------
#Phase 100: iVerification IGC 
#---------------------------------------------------------------------------------------------------------------

$testnum = 100;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
        $Testname[$testnum]="IGC Verification CRL";
        $currentdatetime = localtime time;  #get current date and time for results report
        $startruntimer = time();
        #print $startruntimer;
        print FILEDEBUG "Commande de check : /usr/local/nagios/libexec/crl-checkDate.sh\n";
        $CheckIGC = `/usr/local/nagios/libexec/crl-checkDate.sh`;
        #print "\nRESULTAT CHECK IGC: $CheckIGC\n";
        $endruntimer = time();
        print FILEDEBUG $CheckIGC;
        #print $endruntimer;
        $totalruntime = (int(1000 * ($endruntimer - $startruntimer)) / 1000);
        if ($CheckIGC =~ /CRITIQUE/) {
                $Phase[$testnum] = "$Critical";
                 $Message[$testnum] = "ERREUR CRL INVALIDE - $CheckIGC";
                 $Time[$testnum] = $totalruntime;
                 $Erreur=$Message[$testnum];
        }
        else {
                $Phase[$testnum] = "$Ok";
                $Message[$testnum] = "$CheckIGC";
                $Time[$testnum] = $totalruntime;
        }
        #$Message[$testnum]="Verification IGC ".$Message[$testnum];
        #$Erreur=$Message[$testnum];
        $timetemptot=$Time[$testnum];
        $Time[6]=$timetemptot; #tempspage1
        #warn "message=$Message[$testnum] temps=$timetemptot";
        preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}



#----------------------
#Traitement particulier
#----------------------

if($traitpart) {
traitpart();
exit();
}

#------------------
#Edition du rapport
#------------------

sendreport();
#Fin du Programme.



#------------
#Sub routines
#------------

#------------------------------------------------------------------
sub preparereport {
	my ($phasenum,$phaseresult,$phasemessage) = @_;
	#print " DEBUG: Phase $phasenum: $phaseresult - $phasemessage\n";
	if ($phaseresult eq $Critical) {# Ce test a �chou�
		for ($i = ($phasenum + 1); $i <= $nombredetests; $i++) {
			$Phase[$i] = $Unknown;
			$Message[$i] = "Test non effectue car un test precedent a echoue.";
		}
		sendreport();	
	}
}


#------------------------------------------------------------------
sub sendreport {
	$Globalstatus = "$Ok";
	if($NbTest == 1) {$perf="";}

	my	$Trait	= '-' x 60;
	print FILEDEBUG " ".$Trait."\nResultat des tests:\n".$Trait."\n";
	if ($typerapport ne "Nagios") {
		print	"\n",
			"Resultats des tests:\n",
			$Trait,"\n";
	}
	for ($i = $premiertest; $i <= $NbTest; $i++) {
		if ($Phase[$i]) {
			if (($Phase[$i] eq "$Warning") and ($Globalstatus ne "$Critical")) {
				$Globalstatus = "$Warning";
				$Erreur="$Message[$i]";
			}
			if ($Phase[$i] eq "$Critical") { $Globalstatus = "$Critical";
			chomp($Message[$i]);
			$Erreur="Phase $i ($Testname[$i]): $Phase[$i]: $Message[$i]";
			}
			#$Time[$i] contient le temps de traitement
			#print "Avant :$Message[$i]\n";
			chomp($Message[$i]);
			#print "Apres :$Message[$i]\n";
			if ($typerapport ne "Nagios") {
				print	"Phase $i ($Testname[$i]): $Phase[$i]: $Message[$i]\n";
			}
			print FILEDEBUG "Phase :".$i."(".$Testname[$i].") : ".$Phase[$i]." : ".$Message[$i]."\n";
		}
		else { #La Phase n'a pas ete effetu�
			#$Erreur="Phase $i ($Testname[$i]): $Unknown: Phase non definie";
			if ($typerapport ne "Nagios") {	
				print	"Phase $i ($Testname[$i]): $Unknown: Phase non definie\n";
			}
		}
	}

	cree_fichier();
	if ($monitoring eq "4") {  # 0 = rien 1=maj base 2=creation fichier resultat html
		create_acai();
	}elsif ($monitoring eq "1" and $opt_base == 1) {
		insert_acai();		
	}

	if($ErreurBdd eq "1") {
		$Globalstatus="red";
		$LibErreurBdd="PROBLEME MAJ BASE DE DONNEES ACAI";
	}	
	
	if ($typerapport ne "Nagios") {
		print	$Trait,"\n",
		"GLOBAL_STATUS: $Globalstatus\n",
		"Temps Page : $timetemptot secondes\n";
		print FILEDEBUG "-".$trait."\n GLOBAL STATUS :".$Globalstatus."\n Temps Page : ".$timetemptot." secondes\n";
	} else {
		if ($Serveur ne "") { $LibServeur="Serveur:$Serveur";}
		if ($ServeurApp ne "") { $LibServeurApp="ServeurApp:$ServeurApp";}
		if ($Globalstatus eq "green") {
			print "Verification OK  $LibErreurBdd - ($application - $test) All $NbTest tests passed successfully in $timetemptot secondes $motrech Debit:$debit kb/s $LibServeur $LibServeurApp Sonde:$sonde |$nperf=$$rperf$uperf;30;;0$perf\n";
		} elsif ($Globalstatus eq "red") {
			print "Verification CRITICAL $LibErreurBdd - ($application-$test) $Erreur $motrech Debit:$debit kb/s $LibServeur $LibServeurApp Sonde:$sonde|$nperf=$$rperf$uperf;30;;0\n";	
			#print "WebInject CRITICAL - $Erreur |time=$timetemptot;30;;0\n";	
		} else {
			print "Verification WARNING $LibErreurBdd - ($application-$test) $motrech Debit:$debit kb/s $LibServeur $LibServeurApp $Erreur Sonde:$sonde|$nperf=$$rperf$uperf;30;;0\n";	
			#print "WebInject WARNING - $Erreur |time=$timetemptot;30;;0\n";
		}
	}
	

#if ($monitoring eq "4") {  # 0 = rien 1=maj base 2=creation fichier resultat html
#	create_acai();
#}elsif ($monitoring eq "1" and $opt_base == 1) {
#	insert_acai();		
#}

#if($idtest >0) {
#	cree_fichier();		
#}


#print "status=$Globalstatus";
close(FILEDEBUG);


if ($VerifReseau eq "1" and $Globalstatus ne "green") {
	#my @parametre = ("$plugin/check_reseau.pl", "-H","$dnsatester","-r","-T","30") ;
#exec (@parametre) ;
$reseau = `/bin/echo ----------------------------------------------------  >>/tmp/Reseau_$application`;
$reseau = `/bin/echo MESURE $application $test  - $dnsatester du $date >>/tmp/Reseau_$application`;
$reseau = `/usr/bin/traceroute -I -w 2 -m 15 $dnsatester >>/tmp/Reseau_$application`;
	#print "TEST RESEAU ANDI";
	#exec("$plugin/check_reseau.pl","-H 10.167.150.57 -r -T 30 -v >>/tmp/Reseau_$application") or print "Tests r�seau impossible";
}

if ($Globalstatus eq "green") {
	exit($STATUS_CODE{"OK"});
} elsif ($Globalstatus eq "red") {
	exit($STATUS_CODE{"CRITICAL"});
} else {
	exit($STATUS_CODE{"WARNING"});
}
exit();
}

#------------------------------------------------------------------
sub getoptions {
    Getopt::Long::Configure('bundling');
    GetOptions(
       'V|version'         => \$opt_version,
       'v|debug'         => \$debug,
       'h|help'            => \$opt_help,
       'p|premiertest=s'     => \$premiertest,
       'd|derniertest=s'     => \$derniertest,
	   'i|idtest=s'         => \$idtest,
       'c|config=s'          => \$configfile,
       's|HostName=s'         => \$host,
       'N|notcpcon'          => \$opt_tcp,
	   'B|nobase'          => \$opt_base,
	   't|test'          => \$opt_test,
       'D|detail'          => \$opt_detail,
    ) or do {
        print_usage();
        exit();
        };
    if($opt_version) {
        print "monitoring version $version.\n";
        exit();
    }
    if($opt_help) {
        print_usage();
        exit();
    }
    sub print_usage {
      print <<EOB

Usage:
      monitoring-cete.pl [-p|--premiertest NUM] [-d|--derniertest NUM2] [-c|--config CONFIGFILE] [-D (avec detail ph 6a9)
      monitoring-cete.pl --version|-v
      

EOB
    }
}




sub cree_fichier_timeout {
 my      $ReqSQL;

$Erreur =" TimeOut $TimeOut s";
if ($portail eq "0") {
	$v_portail=0;
} elsif ($portail eq "2") {
	$v_portail=2;
}else {
	$v_portail=1;
}
if($Indic1 eq "") {
	$Indic1="0";
}	
if($Indic2 eq "") {
	$Indic2="0";
}	

open(FILESQL,">>".$TBI_FILE_SQL.".pl") or die "Can't open $TBI_FILE_SQL for writing: $!";

#print FILESQL $ReqSQL."\n";
#print FILESQL "my      \$ReqSQL;\n";
$UrlTest=$Testname[0]."#".$Testname[1]."#".$Testname[2]."#".$Testname[3]."#".$Testname[4]."#".$Testname[5]."#".$Testname[6]."#".$Testname[7]."#".$Testname[8]."#".$Testname[9]."#".$Testname[10];
$UrlTest =~ s/\'/ /g;

my $tlimit;
#on garde 3 mois soit 90 jours (7776000)  ou 2 mois 5184000 (60j) ou 1 mois 2678400 (31 j)
# 40 jours
$tlimit = time-3456000 ;
#warn $tlimit;
#warn time;
#$timetemp =~ tr/./,/;
#$host="centreon-p1";
	#delete temps reel
	$ReqSQLTRD	= "DELETE FROM qds.acai_tr WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLTRD;
	# Insert temps reel
	$ReqSQLTRI	= "INSERT INTO qds.acai_tr VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$applibb','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
		#warn	$ReqSQLTRI;
	#print FILESQL "Trace check ".$application." (".$test.") au ".$now_string."\n";
	print FILESQL $ReqSQLTRI;
	#delete
	$ReqSQLAD	= "DELETE FROM qds.acai WHERE APPLICATION='$application' AND TEST='$test' AND DATE_MESURE_T<$tlimit AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLAD;
	# Insert
	$ReqSQLAI	= "INSERT INTO qds.acai VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$applibb','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLAI;

 close(FILESQL);

if ($monitoring eq "1") {
	system("perl /usr/local/nagios/libexec/InsertAcaiSondeV2.pl $TBI_FILE_SQL.pl");
	my $CodeSortie=$?;
	#print "CodeSortie=".$CodeSortie;
	if (-e $TBI_FILE_SQL.".pl") {
        	#print "ERREUR au Chargement des mesures \n";
		$ExitCode       = 1;
                $ErreurBdd="1";
        	#system("rm $TBI_FILE_SQL.pl");
        } #else {
        	#print "Chargement des mesures OK\n";
        #print "Erreur traitement des donn�es remont�es par le serveur distant";
	#}
}
# ------------------------------------------------------------------------------
}



#------------------------------------------------------------------
#
#
#
#------------------------------------------------------------------

sub cree_fichier {
 my      $ReqSQL;

if ($cont eq "") { $cont=$Errscan;}
$cont=s/#/\\#/g ;
$taille=substr($taille,0,10);
$taille =~ s/\"//g ;
$taille =~ s/\'//g ;
$taille =~ s/\,//g ;
$taille =~ s/\;//g ;
$taille =~ s/\{//g ;
$taille =~ s/\}//g ;
$Erreur =~ s/\"//g ;
$Erreur =~ s/\'//g ;
$Erreur =~ s/\,//g ;
$Erreur =~ s/\;//g ;
$Erreur =~ s/\{//g ;
$Erreur =~ s/\}//g ;

$Indic1=substr($Indic1,0,20);
$Indic1 =~ s/\"//g ;
$Indic1 =~ s/\'//g ;
$Indic1 =~ s/\,//g ;
$Indic1 =~ s/\;//g ;
$Indic1 =~ s/\{//g ;
$Indic1 =~ s/\}//g ;
$Indic2=substr($Indic2,0,20);
$Indic2 =~ s/\"//g ;
$Indic2 =~ s/\'//g ;
$Indic2 =~ s/\,//g ;
$Indic2 =~ s/\;//g ;
$Indic2 =~ s/\{//g ;
$Indic2 =~ s/\}//g ;
$ServeurApp=substr($ServeurApp,0,30);
$ServeurApp =~ s/\"//g ;
$ServeurApp =~ s/\'//g ;
$ServeurApp =~ s/\,//g ;
$ServeurApp =~ s/\;//g ;
$ServeurApp =~ s/\{//g ;
$ServeurApp =~ s/\}//g ;
$Serveur=substr($Serveur,0,30);
$Serveur =~ s/\"//g ;
$Serveur =~ s/\'//g ;
$Serveur =~ s/\,//g ;
$Serveur =~ s/\;//g ;
$Serveur =~ s/\{//g ;
$Serveur =~ s/\}//g ;
if ($portail eq "0") {
	$v_portail=0;
} elsif ($portail eq "2") {
	$v_portail=2;
}else {
	$v_portail=1;
}
if($Indic1 eq "") {
	$Indic1="0";
}	
if($Indic2 eq "") {
	$Indic2="0";
}	

open(FILESQL,">>".$TBI_FILE_SQL.".pl") or die "Can't open $TBI_FILE_SQL for writing: $!";

#print FILESQL $ReqSQL."\n";
#print FILESQL "my      \$ReqSQL;\n";
$UrlTest=$Testname[0]."#".$Testname[1]."#".$Testname[2]."#".$Testname[3]."#".$Testname[4]."#".$Testname[5]."#".$Testname[6]."#".$Testname[7]."#".$Testname[8]."#".$Testname[9]."#".$Testname[10];
$UrlTest =~ s/\'/ /g;

my $tlimit;
#on garde 3 mois soit 90 jours (7776000)  ou 2 mois 5184000 (60j) ou 1 mois 2678400 (31 j)
# 40 jours
$tlimit = time-3456000 ;
#warn $tlimit;
#warn time;
#$timetemp =~ tr/./,/;
#$host="centreon-p1";
	#delete temps reel
	$ReqSQLTRD	= "DELETE FROM qds.acai_tr WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLTRD;
	# Insert temps reel
	$ReqSQLTRI	= "INSERT INTO qds.acai_tr VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$applibb','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
		#warn	$ReqSQLTRI;
	#print FILESQL "Trace check ".$application." (".$test.") au ".$now_string."\n";
	print FILESQL $ReqSQLTRI;
	#delete
	$ReqSQLAD	= "DELETE FROM qds.acai WHERE APPLICATION='$application' AND TEST='$test' AND DATE_MESURE_T<$tlimit AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLAD;
	# Insert
	$ReqSQLAI	= "INSERT INTO qds.acai VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$applibb','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
	#warn	$ReqSQL;
	print FILESQL $ReqSQLAI;

 close(FILESQL);

# ------------------------------------------------------------------------------
}


# ------------------------------------------------------------------------------
sub insert_travail {
$Erreur=substr($Erreur,0,500);
$Erreur =~ s/\"//g;
$Erreur =~ s/\'//g;

if (($monitoring eq "1")&&($application ne "IGC")) {

#use	strict;
#use	Getopt::Long;
#use	DBI;
#use DBD::Oracle;     
# ---------------------------------------------------------
#		PROGRAMME
# ---------------------------------------------------------
my      $dbConn;
        my      $dbCmd;
        my      $ReqSQL;
        my @MsgERR;
        my $Instance;
        my $Username;
        my $Password;
        my $ExitCode;




if ($portail eq "0") {
	$v_portail=0;
} elsif ($portail eq "2") {
	$v_portail=2;
}else {
	$v_portail=1;
}
#warn "v_portail=$v_portail \n";


my $tlimit;
#on garde 3 mois soit 90 jours (7776000)  ou 2 mois 5184000 (60j) ou 1 mois 2678400 (31 j)
# 40 jours
$tlimit = time-3456000 ;
my $nb=0;
#warn $tlimit;
#warn time;
#$timetemp =~ tr/./,/;

# ------------------------------	Connexion DBI
#warn	"Attempting to Connect: $Instance\n";
#if	(! ($dbConn = DBI->connect( $Instance, $Username, $Password ) ) )    #mysql
if	(! ($dbConn = DBI->connect("dbi:Oracle:host=$oracle_server;port=$oracle_port;sid=$oracle_sid", $oracle_user, $oracle_password ) ) )
{
	push @MsgERR, "Connection failed: $DBI::errstr";
        $ErreurBdd="1";
	exit	1;
}
#warn	"Mise � jour base Connect_OK: \n";

#warn	"REQ='$ReqSQL'\nChamps=(",join(';',@{$dbCmd->{NAME}}),")\n";


# ------------------------------	MAJ
	#warn	"MAJ de la base Oracle:\n";


		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "select count(*) from qds.acai_travail where APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
			#warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}
while (@categories = $dbCmd->fetchrow_array)
{
	($nb) = @categories;
#warn "nb=$nb\n";
}
#  

	if ($nb >0) {

		#Si  enreg existe alors timeout , on recupere
		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "DELETE FROM qds.acai_tr WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
			#warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}
		
		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "INSERT INTO qds.acai_tr SELECT * FROM qds.acai_travail WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
			#warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}
		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "INSERT INTO qds.acai SELECT * FROM qds.acai_travail WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
			#warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}
		#delete temps reel
		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "DELETE FROM qds.acai_travail WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
			#warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}
	}
		# Insert temps reel
		#warn	join("Err:\n",$Erreur),"\n"; 
		@MsgERR	= ('_MAJ_ERRORS_');
			$ReqSQL	= "INSERT INTO qds.acai_travail VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$dnsatester','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'0','0','0','$ServeurApp')\n";
		#	warn	$ReqSQL;
			$dbCmd	= $dbConn->prepare($ReqSQL);
			push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
		if	($#{MsgERR})
		{
			warn	join("\n",@MsgERR),"\n";
			$ExitCode	= 1;
                	$ErreurBdd="1";
		}




	$dbConn->disconnect;
	# ------------------------------------------------------------------------------
	}
}

sub insert_acai {

if ($monitoring eq "1") {
	system("perl /usr/local/nagios/libexec/InsertAcaiSondeV2.pl $TBI_FILE_SQL.pl");
	my $CodeSortie=$?;
	#print "CodeSortie=".$CodeSortie;
	if (-e $TBI_FILE_SQL.".pl") {
        	#print "ERREUR au Chargement des mesures \n";
		$ExitCode       = 1;
                $ErreurBdd="1";
        	#system("rm $TBI_FILE_SQL.pl");
        } #else {
        	#print "Chargement des mesures OK\n";
        #print "Erreur traitement des donn�es remont�es par le serveur distant";
	#}
}
}


sub insert_acai2 {

if ($monitoring eq "1") {
	$Erreur=substr($Erreur,0,500);
	$Erreur =~ s/\"//g;
	$Erreur =~ s/\'//g;

	if ($application eq "IGC") {
		#use	DBI;
		#use DBD::Oracle;     
	} #
	# ---------------------------------------------------------
	#		PROGRAMME
	# ---------------------------------------------------------
	my      $dbConn;
        my      $dbCmd;
        my      $ReqSQL;
        my @MsgERR;
        my $Instance;
        my $Username;
        my $Password;
        my $ExitCode;

	if ($cont eq "") { $cont=$Errscan;}

	if ($portail eq "0") {
		$v_portail=0;
	} elsif ($portail eq "2") {
		$v_portail=2;
	}else {
		$v_portail=1;
	}
	if($Indic1 eq "") {
		$Indic1="0";
	}	
	if($Indic2 eq "") {
		$Indic2="0";
	}	

	#on limite indic � 10c
	$Indic1= substr $Indic1,0,10;
	$Indic2= substr $Indic2,0,10;

	#warn "v_portail=$v_portail \n";

	#champ UrlTest 
	$UrlTest=$Testname[0]."#".$Testname[1]."#".$Testname[2]."#".$Testname[3]."#".$Testname[4]."#".$Testname[5]."#".$Testname[6]."#".$Testname[7]."#".$Testname[8]."#".$Testname[9]."#".$Testname[10];
	$UrlTest =~ s/\'/ /g;

	my $tlimit;
	#on garde 3 mois soit 90 jours (7776000)  ou 2 mois 5184000 (60j) ou 1 mois 2678400 (31 j)
	# 40 jours
	$tlimit = time-3456000 ;

	# ------------------------------	Connexion DBI
	#warn	"Attempting to Connect: $Instance\n";
	#if	(! ($dbConn = DBI->connect( $Instance, $Username, $Password ) ) )    #mysql
	if	(! ($dbConn = DBI->connect("dbi:Oracle:host=$oracle_server;port=$oracle_port;sid=$oracle_sid", $oracle_user, $oracle_password ) ) )
	{
		push @MsgERR, "Connect failed: $DBI::errstr";
		exit	1;
	}
	#warn	"Mise � jour base Connect_OK: \n";

	#delete temps reel
	@MsgERR	= ('_MAJ_ERRORS_');
	$ReqSQL	= "DELETE FROM qds.acai_travail WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	$dbCmd	= $dbConn->prepare($ReqSQL);
	push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
	if	($#{MsgERR})
	{
		warn	join("\n",@MsgERR),"\n";
		$ExitCode	= 1;
		$ErreurBdd="1";
	}
	@MsgERR	= ('_MAJ_ERRORS_');
	$ReqSQL	= "DELETE FROM qds.acai_tr WHERE APPLICATION='$application' AND TEST='$test' AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	$dbCmd	= $dbConn->prepare($ReqSQL);
	push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
	if	($#{MsgERR})
	{
		warn	join("\n",@MsgERR),"\n";
		$ExitCode	= 1;
		$ErreurBdd="1";
	}

	# Insert temps reel
	#warn	join("Err:\n",$Erreur),"\n"; 
	@MsgERR	= ('_MAJ_ERRORS_');
	$ReqSQL	= "INSERT INTO qds.acai_tr VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$dnsatester','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
		#warn	$ReqSQL;
	$dbCmd	= $dbConn->prepare($ReqSQL);
	push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
	if	($#{MsgERR})
	{
		warn	join("\n",@MsgERR),"\n";
		$ExitCode	= 1;
		$ErreurBdd="1";
	}

	#delete
	@MsgERR	= ('_MAJ_ERRORS_');
	$ReqSQL	= "DELETE FROM qds.acai WHERE APPLICATION='$application' AND TEST='$test' AND DATE_MESURE_T<$tlimit AND SERVEUR='$host'\n";
	#warn	$ReqSQL;
	$dbCmd	= $dbConn->prepare($ReqSQL);
	push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
	if	($#{MsgERR})
	{
		warn	join("\n",@MsgERR),"\n";
		$ExitCode	= 1;
		$ErreurBdd="1";
	}

	# Insert
	@MsgERR	= ('_MAJ_ERRORS_');
	$ReqSQL	= "INSERT INTO qds.acai VALUES('$application','$datetest','$Globalstatus',$timetemptot,$datetestT,'$test','$dnsatester','$host',$heuremail,'$sonde','$cont',$heurerelance,$v_portail,$tps_warn,$tps_crit,'$taille','$Time[1]','$Time[2]','$Time[3]','$Time[4]','$Time[5]','$Time[6]','$Time[7]','$Time[8]','$Time[9]','$Time[10]','$UrlTest','$Erreur','$Maint','$Serveur',$TimeDNS,$TimeTCP,'$Indic1','$Indic2','$Maintenance_active','$ServeurApp')\n";
	#warn	$ReqSQL;
	print FILEDEBUG $ReqSQL."\n";
	$dbCmd	= $dbConn->prepare($ReqSQL);
	push	@MsgERR, $ReqSQL	if (! $dbCmd->execute() );
	if	($#{MsgERR})
	{
		warn	join("\n",@MsgERR),"\n";
		$ExitCode	= 1;
		$ErreurBdd="1";
	}
	$dbConn->disconnect;
}


# ------------------------------------------------------------------------------
}


#------------------------------------------------------------------
sub envoi_mail {
#$host=`hostname`;
#$host="centreon-p1";
if ($host =~ /bb4/) {
my @adr;
my $adr;
my $i;
# envoi du mail
 my $smtp;
 $serveur_smtp="smtp.melanie2.i2";
 $emeteur="psin\@cete\-lyon.i2";
 #$message="Bonjour,\nAlertes detect�es sur les contextes ASTEC\n\nVeuillez consulter la page http://www.bison-fute.equipement.gouv.fr/asteccli/servlet/clientleger?supervise=supervise&process=AstecBase pour plus de d�tail";

 $smtp = Net::SMTP->new($serveur_smtp, Hello => 'cnxsmtp', Debug => 1) or exit 1;
 $smtp->mail($emeteur) or exit 1;
 
 @adr=split(",",$mailto);
 for $i(0..$#adr){
     $smtp->to($adr[$i]) or exit 1;
 }
 #$smtp->to($mailto) or exit 1;
 $smtp->data() or exit 1;
 $smtp->datasend("From: $emeteur\n") or exit 1;
 $smtp->datasend("To: $mailto\n") or exit 1;
 $smtp->datasend("Subject: $sujet\n") or exit 1;
 $smtp->datasend("\n") or exit 1;
 $smtp->datasend("$messageent") or exit 1;
 $smtp->datasend("\n") or exit 1;
 $smtp->datasend("$message") or exit 1;
 $smtp->datasend("\n") or exit 1;
 $smtp->datasend("$messagecompl") or exit 1;
 $smtp->datasend("\n\n") or exit 1;
 $smtp->datasend("$messageaide") or exit 1;
 $smtp->dataend() or exit 1;
 $smtp->quit or exit 1;
}
}

#------------------------------------------------------------------
sub create_acai {
#warn "Cr�ation fichier html";


my $TBI_FILE = "/var/www/html/acai/vue_".$application."_".$test;
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
  		print FILE "<table border='0' cellspacing='0' width='100%' bgcolor='#DEF2FE'>\n";
		print FILE "<tr><td align='left' width='27%' valign='top'><font color='#003399' size='2' face='Verdana'>\n";
     	print FILE "$application   $test  </font></td>\n";
		if ($Globalstatus eq "red" ) {
    		print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='$Erreur'></td>\n";
		} else {
    		print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_green.gif' width='14' height='14' alt='$Erreur' ></td>\n";	
		}

		if ($timetemptot >= $tps_crit ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='Temps sup�rieur � $tps_crit s'></td>\n";
		}
		elsif ($timetemptot >= $tps_warn ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_orange.gif' width='14' height='14' alt='Temps sup�rieur � $tps_warn s'></td>\n";
		}
		elsif ($timetemptot <= 0 ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='Acces impossible'></td>\n";
		}
		else {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_green.gif' width='14' height='14' alt='Seuil Warning : $tps_warn s Critique : $tps_crit s'></td>\n";	
		}

		print FILE "<td align='left' width='23%' valign='top'><font face='Verdana' size='2'>$timetemptot s&nbsp;le&nbsp; $datetest</font></td></tr>\n";
   		print FILE "</table>\n";
		print FILE "</body>\n";	
	    print FILE "</html>\n";
        close(FILE);	
		
}

#------------------------------------------------------------------
sub traitpart {
#traitement particulier
}

				
