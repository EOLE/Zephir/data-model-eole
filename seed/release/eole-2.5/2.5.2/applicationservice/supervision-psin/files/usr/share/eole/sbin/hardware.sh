#!/bin/bash


##################################################################################################
##
## fonctions
##

detection_materiel()
{
	echo "INFO => detection_materiel" >> $scripts/$fichierErreur
	#TODO : améliorer la détection matérielle ...
	EchoBleu "Identification materielle en cours..."
	marque=$(lshw -class system |grep -i 'poweredge')
	marque=$?
	if [ $marque = 0 ]
	then
	EchoBleu "Materiel Dell detecte"
		return 10
	fi
	
	marque=$(lshw -class system |grep -i 'proliant'|grep -v "ML110 G5")
	marque=$?
	if [ $marque = 0 ]
	then
	EchoBleu "Materiel HP detecte"
		return 20
	fi	
}

depot_hp()
{
	echo "INFO => depot HP" >> $scripts/$fichierErreur
	EchoBleu "Recuperation de la cle de depot"
	if [ ! -f $hp_key ]
	then
	recup=$(wget http://${serveur_maj%% *}/$depots_key/$hp_key)
	recup=$?
		if [ $recup != 0 ]
		then
			echo "ERR => recuperation de la cle de depot HP" >> $scripts/$fichierErreur
			EchoRouge "Probleme d'acces miroir"
			return 1
		fi
	fi

	EchoBleu "Installation de la cle de depot"	
	echo "INFO => keyring" >> $scripts/$fichierErreur
	cle=$(apt-key add $hp_key)
	cle=$?
	if [ $cle != 0 ]
	then
		echo "ERR => incorporation de la cle dans le keyring" >> $scripts/$fichierErreur
		EchoRouge "Probleme d'integration cle GPG"
		return 1
	fi
	
	EchoBleu "Mise en place du depot"
	echo "INFO => sources.list" >> $scripts/$fichierErreur
	echo 'deb http://'${serveur_maj%% *}$hp_path' lucid/8.70 non-free' > /etc/apt/sources.list.d/hp.list
	
	EchoBleu "Mise a jour de la liste des paquets"
	maj=$(apt-get -qq update)
	maj=$?
	if [ $maj != 0 ]
	then
		echo "ERR => mise a jour des paquets" >> $scripts/$fichierErreur
		EchoRouge "Update en erreur"
		return 1
	fi
}

paquets_hp()
{
	echo "INFO => paquets HP" >> $scripts/$fichierErreur
	modprobe ipmi_si
	modprobe ipmi_devintf
	modprobe ipmi_watchdog

	sed -i '$aipmi_si' /etc/modules
	sed -i '$aimpi_devintf' /etc/modules
	sed -i '$aipmi_watchdog' /etc/modules
	
	installation=$(apt-eole install hp-health hp-snmp-agents hpsmh hp-smh-templates hpacucli cpqacuxe)
	installation=$?
	if [ $installation != 0 ]
	then
		echo "ERR => installation des paquets" >> $scripts/$fichierErreur
		EchoRouge "l'installation des paquets HP a echoue"
	else
		EchoBleu "L'interface Web d'HPSMH est accessible via https://$adresse_ip_eth0:2381/"
	fi
}


depot_dell()
{
	echo "INFO => depot Dell" >> $scripts/$fichierErreur
	EchoBleu "Recuperation de la cle de depot"
	if [ ! -f $dell_key ]
	then
		recup=$(wget http://${serveur_maj%% *}/$depots_key/$dell_key)
		recup=$?
		if [ $recup != 0 ]
		then
			echo "ERR => recuperation de la cle de depot Dell" >> $scripts/$fichierErreur
			EchoRouge "Probleme d'acces miroir"
			return 1
		fi
	fi
	
	EchoBleu "Installation de la cle de depot"
	echo "INFO => keyring" >> $scripts/$fichierErreur
	cle=$(apt-key add $dell_key)
	cle=$?
	if [ $cle != 0 ]
	then
		echo "ERR => incorporation de la cle dans le keyring" >> $scripts/$fichierErreur
		EchoRouge "Probleme d'integration cle GPG"
		return 1
	fi
	
	EchoBleu "Mise en place du dépot"
	echo "INFO => sources.list" >> $scripts/$fichierErreur	
	echo 'deb http://'${serveur_maj%% *}$dell_path' /' > /etc/apt/sources.list.d/dell.list

	EchoBleu "Mise a jour de la liste des paquets"
	maj=$(apt-get -qq update)
	maj=$?
	if [ $maj != 0 ]
	then
		echo "ERR => mise a jour des paquets" >> $scripts/$fichierErreur
		EchoRouge "Update en erreur"
		return 1
	fi
}

paquets_dell()
{
	echo "INFO => paquets HP" >> $scripts/$fichierErreur
	installation=$(apt-eole install srvadmin-all)
	installation=$?
	if [ $installation != 0 ]
	then
		echo "ERR => installation des paquets" >> $scripts/$fichierErreur
		EchoRouge "l'installation des paquets Dell a echoue"
	else
		EchoBleu "Demarrage de dataeng"
		service dataeng start
		EchoBleu "Demarrage de l'interface Web d'OMSA: accessible via https://$adresse_ip_eth0:1311/"
		service dsm_om_connsvc start
	fi
}

##
##################################################################################################




#################################################
## main
installation_materiel()
{
	echo "INFO => lancement de detection_materiel"  >> $scripts/$fichierErreur
	detection_materiel

	case $? in

		10 )
		## matériel de type Dell
		echo "Materiel Dell detecte" >> $scripts/$fichierEnvoi

		echo "INFO => lancement de depot_dell" >> $scripts/$fichierErreur
		depot=$(depot_dell)
		depot=$?
		if [ $depot != 0 ]
		then
			EchoRouge "Probleme de depot Dell"
			return 1
		else
			EchoVert "Depot Dell OK"
		
			echo "INFO => lancement de paquets_dell" >> $scripts/$fichierErreur
			paquets=$(paquets_dell)
			paquets=$?
			if [ $paquets != 0 ]
			then
				echo "ERR => paquets_dell" >> $scripts/$fichierErreur
				EchoRouge "Probleme d'installation des paquets"
				return 1
			else
				EchoVert "Paquets de supervision installes"
			fi
			return 0
		fi
		;;

		20 )
		## matériel de type HP
		echo "Materiel HP detecte" >> $scripts/$fichierEnvoi
		echo "INFO => lancement de depot_hp" >> $scripts/$fichierErreur
		depot=$(depot_hp)
		depot=$?
		if [ $depot != 0 ]
		then
			EchoRouge "Probleme de depot HP"
			return 1
		else
			EchoVert "Depot HP OK"
		
			echo "INFO => lancement de paquets_hp" >> $scripts/$fichierErreur
			paquets=$(paquets_hp)
			paquets=$?
			if [ $paquets != 0 ]
			then
				echo "ERR => paquets_hp" >> $scripts/$fichierErreur
				EchoRouge "Probleme d'installation des paquets"
				return 1
			else
				EchoVert "Paquets de supervision installes"
				return 0
			fi
		fi
		;;

		* )
		## matériel inconnu
		echo "Materiel inconnu" >> $scripts/$fichierEnvoi
		EchoOrange "Votre materiel n'a pas identifie comme etant un HP ou un Dell."
		EchoOrange "Merci de contacter le PSIN si vous estimez que ce n'est pas normal."
		;;

	esac
}
