#!/usr/bin/perl
#
#$plugins='/usr/local/nagios/libexec';
$plugins='/usr/lib/nagios/plugins';
my $debug=$ARGV[0];
my $debug2=$ARGV[1];
my $veole="";
my $VersEole="2.3";
if( -e "/etc/eole/release") {
	$veole=`cat /etc/eole/release`;
	if($veole =~ /2.4/ ) { $VersEole="2.4";}
	if($veole =~ /2.5/ ) { $VersEole="2.5";}
	if($veole =~ /2.6/ ) { $VersEole="2.6";}
}

my $type=`sudo uname -a`;
my $arch="32B";
if($type =~ /x86_64/) { $arch="64B";}

#controle local a suppimer
# $NoCehck="-b fs=all"; exemple pour power suply
if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
	require "/usr/lib/nagios/plugins/NoCheckDell.pl";
}

if($NoCheck eq "NON") {
    if(!$option =~ /NOLSHW/) {
	    my $status=`sudo lshw -C system | grep produ`;
	    chomp($status);
	    if($status =~ /PowerEdge 840/ or $status =~ /PowerEdge 800/ or $status =~ /PowerEdge 830/ or $status =~ /PowerEdge 2600/ or $status =~ /PowerEdge 2800/) {  $status=$status." (vieux materiel)";}
    }else{
        $status="Impossible de detecter le materiel";
    }
	print "Eole:$VersEole $arch $status Pas de Check Materiel . Paquet non Installable $psin\n";
	exit 0;
}
#my $status=`$plugins/check_openmanage.pl -e -s -p -b intr=all -b ctrl_fw=all -b ctrl_driver=all -b volt=all  -t 60 $debug $debug2 `;
my $status=`$plugins/check_openmanage.pl -e -s -p -b bp=all -b ctrl_fw=all -b intr=all -b bat_charge=all -b volt=all $NoCheck -t 60 $debug $debug2 `;
#print "$plugins/check_openmanage.pl -e -s -p -b intr=all -b volt=all $NoCheck -t 60 $debug $debug2 ";
my $verif="";
#print $status;
if ($status =~ /No controllers found/) {
        $status=`$plugins/check_openmanage.pl -e --no-storage -s -p -b bp=all -b intr=all -b bat_charge=all -b volt=all $NoCheck -t 60 $debug $debug2`;
}elsif ($status =~ /out of/) {
        $status=`$plugins/check_openmanage.pl -e -s -p -b intr=all -b ctrl_fw=all -b ctrl_driver=all -b volt=all -b bat_charge=all $NoCheck -t 60 $debug $debug2`;
}
my $CodeSortie=$?;

if($NoCheck =~ /no-storage/) {
    $status=~ s/storage/storage (pas de carte RAID detectee FORCE)/;
}elsif($psin ne "") {
     $status=~ s/storage/storage (pas de carte RAID detectee $psin)/;
}else{
    if($status=~ /storage/) {
	if($VersEole =~ /2.3/) {
    		$verif=`sudo lshw -short | grep MegaRAID`;
    		if($verif =~ /MegaRAID/) {
    			$status=~ s/storage/storage (pas de carte RAID detectee auto ANORMAL)/;
    		}else{
    			$status=~ s/storage/storage (pas de carte RAID detectee auto VERIFICATION OK)/;
   		 }
	}else{
		if($arch eq "32B") {
    			$status=~ s/storage/storage (Pas de version 32B)/;
		}else{
    			$verif=`sudo lshw -short | grep MegaRAID`;
    			if($verif =~ /MegaRAID/) {
    				$status=~ s/storage/storage (pas de carte RAID detectee auto ANORMAL)/;
    			}else{
    				$status=~ s/storage/storage (pas de carte RAID detectee auto VERIFICATION OK)/;
   			 }
		}
	}
    }
}
if($status =~ /PowerEdge 840/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}
if($status =~ /PowerEdge 830/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}
if($status =~ /PowerEdge 800/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}
if($status =~ /PowerEdge 2800/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}
if($status =~ /PowerEdge 2600/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}
if($status =~ /PowerEdge 2700/) {  $status=~ s/RAID detectee/RAID detectee vieux materiel/;}

#print $status;
#print "Erreur:$CodeSortie";

#si object not found aon lance la proc�dure dell orphans
if ($status =~ /object not found/) {
	my $repare=`$plugins/dell_orphans`;
	#my $relance=`sudo /etc/init.d/dataeng restart`;
	chomp($status);
	$status=$status." Purge Orphans realisee et relance openmanage\n";
}

#si log ESM full on le vide
if ($status =~ /ESM log is/) {
	my $repare=`sudo /opt/dell/srvadmin/sbin/omconfig system esmlog action=clear`;
	#my $relance=`sudo /etc/init.d/dataeng restart`;
	chomp($status);
	$status=$status." Purge LOG ESM realisee et relance openmanage\n";
}
if($status =~ /Physical Disk/ ) {
        if($status =~ /is Foreign/ or $status =~ /is Failed/ ) {
            $status=~ s/UNKNOWN/CRITICAL/g;
            $CodeSortie=2;
        }
}
my $VersOM="??";
my $vom=`dpkg -l | grep srvadmin-base`;
if($vom =~ /8./ ) { $VersOM="8.";}
if($vom =~ /6.5/ ) { $VersOM="6.5";}
if($vom =~ /7.4/ ) { $VersOM="7.4";}
if($vom =~ /7.1/ ) { $VersOM="7.1";}
if($vom =~ /7.0/ ) { $VersOM="7.0";}
if($vom =~ /7.2/ ) { $VersOM="7.2";}

$status="Eole:$VersEole $arch OM:$VersOM ".$status;
if($status =~ /ERROR\: Dell OpenManage Server Administrator/) {
        print "EOLE-MAT1-C003 ".$status;
        exit 2;
}

if($status =~ /ANORMAL/) { $status =~ s/OK/CRITICAL/;}

if($status =~ /UNKNOWN/) {
	print "EOLE-MAT1-C001 ".$status;
        $CodeSortie=2;
}elsif($status =~ /CRITICAL/) {
        print "EOLE-MAT1-C002 ".$status;
        $CodeSortie=2;
}elsif($status =~ /WARNING/) {
        print "EOLE-MAT1-W002 ".$status;
        $CodeSortie=1;
}else{
	print $status;
        $CodeSortie=0;
}

if ($CodeSortie == 0) {
        exit(0);
} elsif($CodeSortie == 256 ) {
        exit(1);
} elsif($CodeSortie == 1 ) {
        exit(1);
} else {
        #print "Erreur traitement des donn�es remont�es par le serveur distant";
        exit(2);
}

