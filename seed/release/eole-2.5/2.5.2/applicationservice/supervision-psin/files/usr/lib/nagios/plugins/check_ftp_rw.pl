#! /usr/bin/perl -w
# $Id: check_ftp_rw,v 1.0 2011/07/06 14:17:10 root Exp root $

#
# Checks an ftp site
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# you should have received a copy of the GNU General Public License
# along with this program (or with Nagios);  if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA

use strict;
use Getopt::Long;
use Net::FTP;
#use Net::FTPSSL;
use vars qw($PROGNAME);
use lib "/usr/lib/nagios/plugins" ;
use utils qw (%ERRORS &print_revision &support);

sub print_help ();
sub print_usage ();
my $debugftp=0;
my ($opt_host, $opt_user, $opt_pass, $opt_dir, $opt_file, $opt_v, $opt_h, $opt_write, $opt_ssl) =
   ("",        "",        "",        "",       "");
my $result;
my $util="";
my $pass="";
my $outstring="";

$PROGNAME="check_ftp_rw";


GetOptions(
	"version"	=> \$opt_v,
	"help"		=> \$opt_h,
	"host=s"	=> \$opt_host,
	"user=s"	=> \$opt_user,
	"password=s"	=> \$opt_pass,
	"dir=s"		=> \$opt_dir,
	"file=s"	=> \$opt_file,
	"write"		=> \$opt_write,
	"ssl"		=> \$opt_ssl,
	);

if ($opt_v) {
	#print_revision($PROGNAME, '$Id: check_hgsc_ftp,v 1.1 2007/01/02 16:11:33 root Exp root $');
	#exit $ERRORS{'OK'};
}

if ($opt_h) {
	print_help();
	exit $ERRORS{'OK'};
}

unless ($opt_host) {
	print "must supply hostname with --host\n";
	print_usage();
	exit $ERRORS{'UNKNOWN'};
}

#
my $con=`sudo echo 'localhost:5432:geoide:geoide:geoide' >/home/nagios/.pgpass`;
$con=`sudo chmod 0600 ~/.pgpass`;
$con=`psql -h localhost -p 5432 geoide geoide -c "select * from xparams" -a`;
#print $con;
if($con =~ /Login ftp/) {
    ($util) = $con =~ /centralLogin (.*)Login/;
}else{
    ($util) = $con =~ /centralLogin (.*)/;
}
$util =~ s/ //g;
$util =~ s/\|//g;
if($con =~ /Mot/) {
    ($pass) = $con =~ /centralPwd (.*)Mot/;
}else{
    ($pass) = $con =~ /centralPwd (.*)/;
}
$pass =~ s/ //g;
$pass =~ s/\|//g;
if($opt_v) { print "Check FTP Ecriture util=".$util." pass=".$pass."\n";}

#recup adl
my $adl="";
if (-e "/usr/lib/nagios/plugins/adl.sql") {
    $con=`psql -h localhost -p 5432 geoide geoide -f /usr/lib/nagios/plugins/adl.sql | grep @`;
    #print $con;
    $adl=($con);
    $adl =~ s/\\n//;
    $adl =~ s/\n//;
}
$con=`sudo rm -f ~/.pgpass`;

$result = 'OK';

if($opt_v) { $debugftp=1;}
my $ftp;
if ($opt_ssl) {
    $ftp = Net::FTP -> new($opt_host,
				Debug => $debugftp,
                #Encryption => EXP_CRYPT,
				Croak => 0,
				Passive => 1 #must be in passive mode to work
    );
} else {
    $ftp = Net::FTP -> new($opt_host,
				Debug => $debugftp,
				Passive => 1 #must be in passive mode to work
    );
}

unless ($ftp) {
    $result="CRITICAL";
    print "Check FTP Entrepot Central Cannot connect to host: $opt_host ADL:$adl\n";
    if($opt_v) { print "\nConnexion CRITIQUE  to $opt_host";}
    exit $ERRORS{$result};
} else {
    $outstring .="Connected to $opt_host";
    if($opt_v) { print "\nConnected to $opt_host";}
}

$opt_user ||= $util;
$opt_pass ||= $pass;

unless ($ftp->login($opt_user, $opt_pass)){
	$result="CRITICAL";
	print "Check FTP Entrepot Central Cannot login as user $opt_user ADL:$adl\n";
        if($opt_v) { print "\nloggin CRITIQUE as $opt_user";}
	exit $ERRORS{$result};
} else {
	$outstring .=", logged in as $opt_user";
    	if($opt_v) { print "\nlogged in as $opt_user";}
}

if ($opt_dir ne ""){
	unless ($ftp->cwd($opt_dir)){
		$result="WARNING";
		print "Check FTP Entrepot Central Cannot chdir to $opt_dir ADL:$adl\n";
    		if($opt_v) { print "\nChdir CRITIQUE sur $opt_dir";}
		exit $ERRORS{$result};
	} else {
		$outstring .=", chdir'ed to $opt_dir";
    		if($opt_v) { print "\nchdir OK to $opt_dir";}
	}
}

if ($opt_file ne ""){
	unless ($ftp->get($opt_file, "/dev/null")){
		$result="WARNING";
		print "Check FTP Entrepot Central Cannot get file: $opt_file ADL:$adl\n";
    		if($opt_v) { print "\nDownload CRITIQUE $opt_file";}
		exit $ERRORS{$result};
	} else {
		$outstring .=", downloaded $opt_file";
    		if($opt_v) { print "\nDownload OK $opt_file";}
	}
}

my $remote_filename = "__check_ftp.nagios";
my $memfile = "check_ftp Nagios plugin's test file.\n";
if ($opt_write){
	open(file_handle, "<", \$memfile);
#	unless ($remote_filename = $ftp->put_unique(*file_handle, "__check_ftp.nagios")){
	unless ($ftp->put(\*file_handle, $remote_filename)){
		$result="CRITICAL";
        #print "Cannot put file: $remote_filename\n";
		print "\nCheck FTP Entrepot Central Envoi impossible de fichier QUOTA certainement atteind ADL:$adl\n";
    		if($opt_v) { print "\nTest Ecriture CRITIQUE Quota?";}
		exit $ERRORS{$result};
	} else {
		$outstring .=", test ecriture OK QUOTA OK";
    		if($opt_v) { print "\nTest Ecriture OK";}
	}
	close(file_handle);
	if ($remote_filename ne ""){
	    $ftp->delete($remote_filename);
	}
}

print "Check FTP Entrepot Central ",$result," ",$outstring, " ADL:$adl\n";
exit $ERRORS{$result};

sub print_usage () {
	print "Usage:\n";
	print "  $PROGNAME --host <host> [--user <user>][--password <password>][--dir <dir>][--file <file>][--write][--ssl]\n";
	print "  $PROGNAME [--help]\n";
	print "  $PROGNAME [--version]\n";
}

sub print_help () {
	print_revision($PROGNAME, '$Id: check_hgsc_ftp,v 1.1 2007/01/02 16:11:33 root Exp root $');
	print "Copyright (c) 2007 Paul Archer\n\n";
	print_usage();
	print "\n";
	print "  --host		host to check\n";
	print "  --user		username to use (uses 'anonymous' if user not given)\n";
	print "  --password		password to use (uses 'nagios' if password not given)\n";
	print "  --dir			cd to this directory (stays in base directory otherwise)\n";
	print "  --file		file to retrieve (can be absolute path, or relative to 'dir' (or / if no 'dir' given))\n";
	print "  --write	write random file (writes relative to 'dir' (or / if no 'dir' given))\n";
	print "  --ssl		connect using FTPS instead of plain FTP\n";
	print "\n";
	print "Will return CRITICAL if host cannot be contacted or logged into.\n";
	print "Will return WARNING if specified directory or file isn't accessible/uploadable.\n";
	print "\n";
	support();
}
