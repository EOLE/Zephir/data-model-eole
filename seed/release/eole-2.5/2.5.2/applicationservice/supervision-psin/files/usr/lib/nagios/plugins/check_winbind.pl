#!/usr/bin/perl
#
$version = "2.0";


use Time::HiRes 'time','sleep';
my $plugin="/usr/lib/nagios/plugins";

#pour sortie Nagios
my %STATUS_CODE = (  'UNKNOWN'  => '-1',
                     'OK'       => '0',
                     'WARNING'  => '1',
                     'CRITICAL' => '2'   );


my $Message = "";
my $test2="";
my $TimeOut=10;

#------------------------
#Phase 0: Verification process winbind   
#------------------------
my $NbProcess=0;
$test = `$plugin/check_procs -a winbind -w 1:10 -c 1:10`;
chomp($test);
#print "\nRESULTAT1: $test\n";
if ($test =~ /PROCS OK/) { 
	$Globalstatus=0;
	#chomp($test);
	$Message = $Message." ".$test;
}else{
	#($NbProcess) = $test =~ /PROCS CRITIQUE: (.*) processus/;
	$Globalstatus=1;
	#$Message = $Message." Check process:$NbProcess CRITIQUE ";
	$Message = $Message." ".$test;
}


#------------------------
#Phase 1: Test ping  
#------------------------
my $TimeOut = 10;
eval {
        local $SIG{ALRM} = sub { die "timeout" };
        alarm $TimeOut;

	$test = `wbinfo -p`;
	#print "\nRESULTAT1: $test\n";
	if ($test =~ /succeeded/) { 
		$Globalstatus=$Globalstatus;
		$Message = $Message." Check reponse OK";
	}else{
		$Globalstatus=1;
		$Message = $Message." Check reponse CRITIQUE WINBIND ARRETE";
	}
};
if ($@) {
	$arr=`killall -9 wbinfo`;
	$arr2=`sudo killall -9 winbindd`;
	$arr1=`sudo /etc/init.d/winbind restart`;
        $Globalstatus=2;
        #$Message = $Message." TimeOut Check reponse winbindd ";
        $Message = $Message." TimeOut Check reponse winbindd Relance Automatique forcee ";
	print "WINBIND CRITIQUE ".$Message."\n";
	exit($STATUS_CODE{"CRITICAL"});
}
alarm 0;

#------------------------
#Phase 2 : Test users  
#------------------------
$TimeOut = 40;
#$test = `wbinfo -u`;
eval {
        local $SIG{ALRM} = sub { die "timeout" };
        alarm $TimeOut;
	my $tpsd= time();	
	$test = `getent group`;
	#print "\nRESULTAT1: $test\n";
	my $tpsf= time();	
 	my $tps= (int(1000 * ($tpsf-$tpsd)) / 1000);
	if ($test =~ /admin./) {
		$Globalstatus=$Globalstatus;
		$Message = $Message." Check Users OK temps:".$tps." s | time=".$tps."s";
	}else{
		$Globalstatus=1;
		$Message = $Message." Check Users CRITIQUE | time=0s";
	}
};
if ($@) {
	#$arr=`killall wbinfo`;
        $Globalstatus=2;
        $Message = $Message." TimeOut Verification Users ";
	print "WINBIND WARNING ".$Message."\n";
	exit($STATUS_CODE{"WARNING"});
}
alarm 0;
#------------------------
#Phase 3: Test secret  
#------------------------

#$test = `wbinfo -t >/dev/null 2>&1`;
#print "\nRESULTAT1: $test\n";
#if ($test =~ /secret/) { 
#	$Globalstatus=$Globalstatus;
#	$Message = $Message." Check Secret OK";
#}else{
#	$Globalstatus=1;
#	$Message = $Message." Check Secret CRITIQUE";
#}

#print "status=$Globalstatus";

if ($Globalstatus == 0) {
	print "WINDBIND OK ".$Message."\n";
	exit($STATUS_CODE{"OK"});
} elsif ($Globalstatus == 1) {
	print "WINBIND CRITIQUE ".$Message."\n";
	exit($STATUS_CODE{"CRITICAL"});
} else {
	print "WINBIND WARNING ".$Message."\n";
	exit($STATUS_CODE{"WARNING"});
}
exit();



