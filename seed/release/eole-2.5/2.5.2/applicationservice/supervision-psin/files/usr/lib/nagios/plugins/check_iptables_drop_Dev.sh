#!/bin/bash
# PSIN Verification des drop par interface
# Revision:
# - 03/06/2010 alerte uniquement sur eth0 et eth1

VERSION=0.2

#variables 
# taux critique alerte

TAUXCRIT=100 # par minute

function init() {

	NBDROP=`sudo cat /var/log/ulog/syslogemu.log |grep DROP | wc -l`
	NBDROPETH0=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth0)' | wc -l`
	NBDROPETH1=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth1)' | wc -l`
	NBDROPETH2=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth2)' | wc -l`
	NBDROPETH3=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth3)' | wc -l`
	DATE=`date +%s`

	# sauve new data
	echo $NBDROP > /tmp/iptables_nb_drop
	echo $NBDROPETH0 > /tmp/iptables_nb_drop_eth0
	echo $NBDROPETH1 > /tmp/iptables_nb_drop_eth1
	echo $NBDROPETH2 > /tmp/iptables_nb_drop_eth2
	echo $NBDROPETH3 > /tmp/iptables_nb_drop_eth3
	date +%s > /tmp/iptables_time_drop

	echo ""
	echo "Premiere Initialisation des donnees complete. Nombre de drop $NBDROP"
	echo ""
exit 0

}


function check_drop() {

	#TAUXCRIT=100 # par minute

	# sauve anciennes donnes
	OLD_NBDROP=`cat /tmp/iptables_nb_drop`
	OLD_NBDROPETH0=`cat /tmp/iptables_nb_drop_eth0`
	OLD_NBDROPETH1=`cat /tmp/iptables_nb_drop_eth1`
	OLD_NBDROPETH2=`cat /tmp/iptables_nb_drop_eth2`
	OLD_NBDROPETH3=`cat /tmp/iptables_nb_drop_eth3`
	LAST=`cat /tmp/iptables_time_drop`

	NBDROP=`sudo cat /var/log/ulog/syslogemu.log |grep DROP | wc -l`
	NBDROPETH0=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth0)' | wc -l`
	NBDROPETH1=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth1)' | wc -l`
	NBDROPETH2=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth2)' | wc -l`
	NBDROPETH3=`sudo cat /var/log/ulog/syslogemu.log | egrep '(DROP.+IN=eth3)' | wc -l`
	DATE=`date +%s`


	# sauve nouvelles donnees
	echo $NBDROP > /tmp/iptables_nb_drop
	echo $NBDROPETH0 > /tmp/iptables_nb_drop_eth0
	echo $NBDROPETH1 > /tmp/iptables_nb_drop_eth1
	echo $NBDROPETH2 > /tmp/iptables_nb_drop_eth2
	echo $NBDROPETH3 > /tmp/iptables_nb_drop_eth3
	date +%s > /tmp/iptables_time_drop

	DIFF_NBDROP=`expr ${NBDROP} - ${OLD_NBDROP}`
	DIFF_NBDROPETH0=`expr ${NBDROPETH0} - ${OLD_NBDROPETH0}`
	DIFF_NBDROPETH1=`expr ${NBDROPETH1} - ${OLD_NBDROPETH1}`
	DIFF_NBDROPETH2=`expr ${NBDROPETH2} - ${OLD_NBDROPETH2}`
	DIFF_NBDROPETH3=`expr ${NBDROPETH3} - ${OLD_NBDROPETH3}`
	DIFF_AGES=`expr ${DATE} - ${LAST}`
	DIFF_AGE=`expr ${DATE} - ${LAST}`
	DIFF_AGE=`expr ${DIFF_AGE} / 60`   #en minute

	#if [ $DIFF_NBDROP -lt 0 ]; then DIFF_NBDROP=0; fi
	if [ $DIFF_AGE -eq 0 ]; then DIFF_AGE=1; fi

	DEBITDROP=`expr ${DIFF_NBDROP} / $DIFF_AGE`
	DEBITDROPETH0=`expr ${DIFF_NBDROPETH0} / $DIFF_AGE`
	DEBITDROPETH1=`expr ${DIFF_NBDROPETH1} / $DIFF_AGE`
	DEBITDROPCRIT=`expr ${DEBITDROPETH0} + ${DEBITDROPETH1}`

	if [ $TAUXCRIT -lt $DEBITDROPETH0 ]; then
		ERREUR=DROP INTERFACE eth0 $DEBITDROPETH0 / minute
	fi
	if [ $TAUXCRIT -lt $DEBITDROPETH1 ]; then
		ERREUR=$ERREUR DROP INTERFACE eth1 $DEBITDROPETH1 / minute
	fi

	
	if [ $TAUXCRIT -lt $DEBITDROPCRIT]; then
#		echo "CHECK IPTABLES CRITIQUE Nombre de drop : $DEBITDROPCRIT /minute $ERREUR  Total:$NBDROP Sup:$DIFF_NBDROP ETH0:$DIFF_NBDROPETH0 ETH1:$DIFF_NBDROPETH1 ETH2:$DIFF_NBDROPETH2 ETH3:$DIFF_NBDROPETH3| drop=$DIFF_NBDROP drop_eth0=$DIFF_NBDROPETH0 drop_eth1=$DIFF_NBDROPETH1 drop_eth2=$DIFF_NBDROPETH2 drop_eth3=$DIFF_NBDROPETH3"
		exit 1;
	fi

#	if [ $TAUXCRIT -lt $DEBITDROP ]; then
#		echo "CHECK IPTABLES CRITIQUE Nombre de drop : $DEBITDROP /minute  Total:$NBDROP Sup:$DIFF_NBDROP ETH0:$DIFF_NBDROPETH0 ETH1:$DIFF_NBDROPETH1 ETH2:$DIFF_NBDROPETH2 ETH3:$DIFF_NBDROPETH3| drop=$DIFF_NBDROP drop_eth0=$DIFF_NBDROPETH0 drop_eth1=$DIFF_NBDROPETH1 drop_eth2=$DIFF_NBDROPETH2 drop_eth3=$DIFF_NBDROPETH3"
#		exit 1
#	fi


	echo "CHECK IPTABLES OK  Nombre de drop : $DEBITDROP /minute Total:$NBDROP Sup:$DIFF_NBDROP temps:$DIFF_AGES s ETH0:$DIFF_NBDROPETH0 ETH1:$DIFF_NBDROPETH1 ETH2:$DIFF_NBDROPETH2 ETH3:$DIFF_NBDROPETH3 | drop=$DIFF_NBDROP drop_eth0=$DIFF_NBDROPETH0 drop_eth1=$DIFF_NBDROPETH1 drop_eth2=$DIFF_NBDROPETH2 drop_eth3=$DIFF_NBDROPETH3"
	exit 0
}



if [ ! -e /tmp/iptables_nb_drop ]; then
init;
fi
check_drop
esac


