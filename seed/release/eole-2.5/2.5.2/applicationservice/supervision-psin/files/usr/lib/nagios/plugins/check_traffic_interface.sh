#!/bin/bash
# Maintainer Sebastian Clanzett 07/2006   <Sebastian.Clanzett@gmx.de>
# Check Interface Traffic Plugin for Linux: Version 0.6

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Revision:
# - 01/08 2006 > added: monitoring traffic exceeds
# - 23/08 2006 > added: Performance data (written to temp_files in /tmp/)
# - 24/08 2006 > added: init-Mode: must be started once for each interface

VERSION=0.6

function show_usage(){
	echo ""
	echo "    Plugin for checking interface traffic based on proc-Filesystem"
	echo "    It now needs a first time initilisation to create the first performance-data by issuing:"
	echo "    $0 init <INTERFACE>"
	echo ""
	echo "    Usage: $0 <INTERFACE> <--- shows Traffic"
	echo "    Usage: $0 maxtx <MAX-TRANCEIVED-BYTES> <INTERFACE> <--- checks Traffic"
	echo ""
	echo "    Example: $0 maxtx 120000000 eth0    <----- critical when TX-Bytes greater than 12000000 Bytes"
	echo ""
	echo "    Performance Data Output: DIFF_TX(RX) <- difference between now and last check(bytes) LAST <- last check before xy seconds"
	echo ""
	echo ""
	echo "    Version: $VERSION"
	echo ""

}



function check_traffic() {
	INT=$1
	AWK=`which awk`

	MAXTX=$2

	TX=`cat /proc/net/dev | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $10 } else { print $9 } }'`


	if [ $MAXTX -lt $TX ]; then
		DIFF=`expr $TX - $MAXTX`
		kDIFF=`expr $DIFF / 1024`
		mDIFF=`expr $kDIFF / 1024`
		echo "EOLE-SR-C008 CRITICAL: Traffic limit has exceeded by $kDIFF KBytes or $mDIFF MB!"

	else
		echo "Traffic for $INT is OK. Max-Traffic: $MAXTX"
	fi


}

function init() {

	INT=$1
	AWK=`which awk`
	#RX=`cat /proc/net/dev | grep $INT | $AWK '{ if ($1 == "$INT:") { print $2 } else { split($1,feld,":");print feld[3]} }'`
	RX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $1 $2 $3 $4 $5 } else { print $2 } }'`
    #echo $RX
	TX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $10 } else { print $9 } }'`
	ERX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $3 }'`
	ETX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $12 } else { print $11 } }'`
    if let $RX 2>/dev/null
	then
		verif="OK"
	else
		echo "Interface $INT non ACTIF\n"
		exit 0;
		RX=0
		ERX=0
		TX=0
		ETX=0
	fi
	DATE=`date +%s`

	# save new data
	echo $TX > /tmp/tx_$INT
	echo $RX > /tmp/rx_$INT
	echo $ERX > /tmp/erx_$INT
	echo $ETX > /tmp/etx_$INT
	date +%s > /tmp/last_$INT

	echo ""
	echo "First time initialisation for $INT completed. TX=$TX RX=$RX ETX=$ETX ERX=$ERX"
	echo ""
exit 0

}


function show_traffic() {

	INT=$1
	AWK=`which awk`
	TAUXCRIT=10

	# save old data
	OLD_TX=`cat /tmp/tx_$INT`
	OLD_RX=`cat /tmp/rx_$INT`
	OLD_ETX=`cat /tmp/etx_$INT`
	OLD_ERX=`cat /tmp/erx_$INT`
	LAST=`cat /tmp/last_$INT`

	ERX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $3 }'`
	RX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $2 } else { split($1,feld,":");print feld[2]} }'`
if [ $INT = "eth0" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth0\: | /usr/bin/awk '{ if ($1 == "eth0:") { print $2 } else {  split($1,feld,":");print feld[2] } }'`
elif [ $INT = "eth1" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth1\: | /usr/bin/awk '{ if ($1 == "eth1:") { print $2 } else {  split($1,feld,":");print feld[2] } }'`
elif [ $INT = "eth2" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth2\: | /usr/bin/awk '{ if ($1 == "eth2:") { print $2 } else {  split($1,feld,":");print feld[2] } }'`
elif [ $INT = "eth3" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth3\: | /usr/bin/awk '{ if ($1 == "eth3:") { print $2 } else { split($1,feld,":");print feld[2]} }'`
fi
	expr $RX + 0 1>/dev/null 2>&1
	coderetour=$?
	#echo "Code Retour RX:$coderetour"

	if [ $coderetour -eq 1 ]; then
		#echo "Reclacul"
		RX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $2}'`
		ERX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $4 }'`
		coderetour=$?
		if [ $coderetour -eq 1 ]; then
			RX=0
			ERX=0
		fi
	fi
	ETX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $12 }'`
	TX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ if ($1 == "$INT:") { print $10 } else { print $9 } }'`
if [ $INT = "eth0" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth0\: | /usr/bin/awk '{ if ($1 == "eth0:") { print $10 } else { print $9 } }'`
elif [ $INT = "eth1" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth1\: | /usr/bin/awk '{ if ($1 == "eth1:") { print $10 } else { print $9 } }'`
elif [ $INT = "eth2" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth2\: | /usr/bin/awk '{ if ($1 == "eth2:") { print $10 } else { print $9 } }'`
elif [ $INT = "eth3" ]; then
    TX=`cat /proc/net/dev | grep -v rename | grep eth3\: | /usr/bin/awk '{ if ($1 == "eth3:") { print $10 } else { print $9 } }'`
fi
#echo "TX=$TX "

	expr $TX + 0 1>/dev/null 2>&1
	coderetour=$?
	if [ $coderetour -eq 1 ]; then
		#echo "Reclacul"
		TX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $10}'`
		ETX=`cat /proc/net/dev | grep -v rename | grep $INT\: | $AWK '{ print $12 }'`
		coderetour=$?
		if [ $coderetour -eq 1 ]; then
			TX=0
			ETX=0
		fi
	fi

	DATE=`date +%s`
	if let $RX 2>/dev/null
	then
		verif="OK"
	else
		RX=0
		ERX=0
		TX=0
		ETX=0
	fi


	# save new data
	echo $TX > /tmp/tx_$INT
	echo $RX > /tmp/rx_$INT
	echo $ETX > /tmp/etx_$INT
	echo $ERX > /tmp/erx_$INT
	date +%s > /tmp/last_$INT
	#echo "Valeurs trouvess for $INT  TX=$TX RX=$RX ETX=$ETX ERX=$ERX\n"

	DIFF_TX=`expr ${TX} - ${OLD_TX}`
	DIFF_RX=`expr ${RX} - ${OLD_RX}`
	DIFF_ETX=`expr ${ETX} - ${OLD_ETX}`
	DIFF_ERX=`expr ${ERX} - ${OLD_ERX}`
	DIFF_AGE=`expr ${DATE} - ${LAST}`
	#echo "Difference trouvees en bits $INT  DIFF_TX=$DIFF_TX DIFF_RX=$DIFF_RX "

	if [ $DIFF_TX -lt 0 ]; then DIFF_TX=$(( $DIFF_TX + 4294967295 )); fi
	if [ $DIFF_RX -lt 0 ]; then DIFF_RX=$(( $DIFF_RX + 4294967295 )); fi

	#passage en Kbits

	PDIFF_TX=`expr ${DIFF_TX} \* 8`
	DIFF_TXK=`expr ${DIFF_TX} / 128`  #au lieu de 1024
	PDIFF_RX=`expr ${DIFF_RX} \* 8`
	DIFF_RXK=`expr ${DIFF_RX} / 128`
	#TX=`expr ${TX} * 8`
	TX=`expr ${TX} / 128`
	#RX=`expr ${RX} * 8`
	RX=`expr ${RX} / 128`
	DEBITRXK=`expr ${DIFF_RXK} / $DIFF_AGE`
	DEBITTXK=`expr ${DIFF_TXK} / $DIFF_AGE`
	DEBITRX=`expr ${PDIFF_RX} / $DIFF_AGE`
	DEBITTX=`expr ${PDIFF_TX} / $DIFF_AGE`
	#echo "Difference trouvees en bit/s $INT  DIFF_TX=$DIFF_TX DIFF_RX=$DIFF_RX "
#IPINT=`sudo ifquery $INT | grep address | awk '{print $2}'`
IPINT=`sudo ifconfig $INT | grep inet | awk '{print $2}'| sed "s/adr://g"`
#DIFF_TX=$DIFF_TX&"Bits/s"

	if [ $TAUXCRIT -lt $DIFF_ETX ]; then
		echo "EOLE-SR-C009 CHECK INTERFACE $INT IP:$IPINT : CRITIQUE (Erreurs Out)  TRAFIC Out: $TX KBits soit $DEBITTXK Kbits/s  In: $RX Kbits soit $DEBITRXK Kbits/s Erreurs In : $DIFF_ERX Erreurs Out : $DIFF_ETX |traffic_in=$DEBITRX""Bits/s traffic_out=$DEBITTX""Bits/s"
		exit 1
	fi
	if [ $TAUXCRIT -lt $DIFF_ERX ]; then
		echo "EOLE-SR-C010 CHECK INTERFACE $INT IP:$IPINT : CRITIQUE (Erreurs In)  TRAFIC Out: $TX Kbits soit $DEBITTXK Kbits/s  In: $RX Kbits soit $DEBITRXK Kbits/s Erreurs In : $DIFF_ERX Erreurs Out : $DIFF_ETX|  traffic_in=$DEBITRX""Bits/s traffic_out=$DEBITTX""Bits/s"
		exit 1
	fi


	echo "CHECK INTERFACE $INT IP:$IPINT : OK  TRAFIC Out: $TX Kbits soit $DEBITTXK Kbits/s  In: $RX Kbits soit $DEBITRXK Kbits/s Erreurs In : $DIFF_ERX Erreurs Out : $DIFF_ETX|  traffic_in=$DEBITRX""Bits/s traffic_out=$DEBITTX""Bits/s"
	exit 0
}


if [ -z "$1" ]; then
	show_usage
	exit 2
fi



case $1 in
	--help)
		show_usage
		exit 0
		;;
	eth*)
		INT=$1
		if [ ! -e /tmp/tx_$INT ]; then
			init $INT;
		fi
		#droits
		sudo chmod 777 /tmp/tx_$INT
		sudo chmod 777 /tmp/rx_$INT
		sudo chmod 777 /tmp/erx_$INT
		sudo chmod 777 /tmp/etx_$INT
		sudo chmod 777 /tmp/last_$INT
		show_traffic $INT
		;;
	maxtx)
		INT=$3
		MAXTX=$2
		check_traffic $INT $MAXTX
		;;
	init)
		INT=$2
		init $INT
		;;
	*)
		show_usage
		exit 1
		;;
esac


