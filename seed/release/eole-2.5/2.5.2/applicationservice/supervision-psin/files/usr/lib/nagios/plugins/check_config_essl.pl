#!/usr/bin/perl 
#

use Getopt::Long;
use Sys::Hostname;
use POSIX qw(strftime);
#param base de donn�es oracle
require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";

use Time::HiRes 'time','sleep';
my $date = scalar localtime;
#my $host=`hostname`;
my $host=hostname;

my $IP=$ARGV[0];
my $HostName=$ARGV[1];
if($IP eq "") {
	print "Usage : check_config_essl.pl IP_du_Serveur Nom_du_Serveur\n";
	exit 0;
}

if($HostName eq "") {
	print "Usage : check_config_essl.pl IP_du_Serveur Nom_du_Serveur\n";
	exit 0;
}
my $Nom=`ssh -i /usr/local/nagios/.ssh/id_rsa_eole $IP "hostname"`;
chomp($Nom);
my $Service=`ssh -i /usr/local/nagios/.ssh/id_rsa_eole $IP "/usr/lib/nagios/plugins/check_procs -C clamd"`;
my $G2="0";
if ($Service =~ /0 processus/) {
	$G2="0";
}else{
	$G2="1";
}
$G2="0"; # on ne traite pas clamav
$Service=`ssh -i /usr/local/nagios/.ssh/id_rsa_eole $IP "/usr/lib/nagios/plugins/check_file_age -w 86400 -c 86500 -f /var/kaspersky/bases/mir-kav/index/u0607g.xml"`;
my $G3="0";
if ($Service =~ /FILE_AGE OK/) {
	$G3="1";
}else{
	$G3="0";
}

my $Sdell="Non";
my $Dell=`ssh -i /usr/local/nagios/.ssh/id_rsa_eole $IP "/usr/lib/nagios/plugins/check_openmanage.pl -s "`;
if ($Dell =~ /install/) {
	$Sdell="Non";
}elsif($Dell =~ /Error! No temperature probes/) {
        $Sdell="Non";
}else{
	$Sdell="Oui";
}



use     DBI;

# ---------------------------------------------------------
#               PROGRAMME
# ---------------------------------------------------------
my      $dbConn;
        my      $dbCmd;
        my      $ReqSQL;
        my @MsgERR;
        my $Instance;
        my $Username;
        my $Password;
        my $ExitCode;
        $Instance="dbi:mysql:database=centreon;host=$mysql_serveur";
        $Username=$mysql_user;
        $Password=$mysql_password;

my $HostId=0;
my $NbH=0;
my $CodeSortie=0;
my $Erreur="";
my $ErreurDell="";

# ------------------------------        Connexion DBI
#warn   "Attempting to Connect: $Instance\n";
if      ( ($dbConn = DBI->connect( $Instance, $Username, $Password ) ) )
{
#warn   "Mise � jour base Connect_OK: \n";

         #Recup host_id

         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select host_id from host where host_name like '".$HostName."\%' \n";
         #               warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($HostId) = @categories;
           }

         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select count(*) from hostgroup_relation where hostgroup_hg_id=434 and host_host_id=".$HostId." \n";
                      #  warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($NbH) = @categories;
           }


	if( $NbH == 0 ) {
		if($Erreur eq "") { $Erreur=" Le serveur n'est pas dans le ou les groupes "; }
		$Erreur=$Erreur." ESSL";
		$CodeSortie=2;
	}

	if( $G2 eq "1") {
         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select count(*) from hostgroup_relation where hostgroup_hg_id=446 and host_host_id=".$HostId." \n";
                      #  warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($NbH) = @categories;
           }
	if( $NbH == 0 ) {
		if($Erreur eq "") { $Erreur=" Le serveur n'est pas dans le ou les groupes "; }
		$Erreur=$Erreur." ESSL-CLAMAV";
		$CodeSortie=2;
	}
	}

	if( $G3 eq "1") {
         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select count(*) from hostgroup_relation where hostgroup_hg_id=464 and host_host_id=".$HostId." \n";
                       # warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($NbH) = @categories;
           }
	if( $NbH == 0 ) {
		if($Erreur eq "") { $Erreur=" Le serveur n'est pas dans le ou les groupes "; }
		$Erreur=$Erreur." ESSL-KAV";
		$CodeSortie=2;
	}
	}

	
	if($Sdell eq "Oui") {
         #Recup host_object id

         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select host_object_id from ndo.nagios_hosts where display_name like '".$HostName."\%' \n";
         #               warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($HostId) = @categories;
           }
         @MsgERR = ('_MAJ_ERRORS_');
        $ReqSQL = "select count(*) from ndo.nagios_services where display_name like '\%materiel\%' and host_object_id=".$HostId." \n";
                      #  warn    $ReqSQL;
         $dbCmd = $dbConn->prepare($ReqSQL);
         push    @MsgERR, $ReqSQL        if (! $dbCmd->execute() );
         if      ($#{MsgERR})
           {
                warn    join("\n",@MsgERR),"\n";
               $ExitCode       = 1;
           }
          while (@categories = $dbCmd->fetchrow_array)
           {
             ($NbH) = @categories;
           }


	if( $NbH == 0 ) {
		$ErreurDell=" Service Check DELL NON ACTIF";
		$CodeSortie=2;
	}

	}


        $dbConn->disconnect;
}else{
        $Erreur="!!!! ATTENTION !!!!!!!!!!Erreur de Connexion � la base de donn�es";
}

if( $CodeSortie == 0 ) {
	print "Check Config Serveur $HostName ( $Nom ) OK \n";
	exit 0;
}else{
	print "Check Config Serveur $HostName ( $Nom ) CRITIQUE $Erreur $ErreurDell \n";
	exit 2;

}

