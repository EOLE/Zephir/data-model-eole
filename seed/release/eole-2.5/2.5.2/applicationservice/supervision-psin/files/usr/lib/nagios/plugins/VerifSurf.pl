#!/usr/bin/perl
#

use Getopt::Long;
use Sys::Hostname;
use POSIX qw(strftime);
use Term::ANSIColor qw(:constants);
    #print BOLD, GREEN, "This text is in bold blue.\n", RESET;

use Time::HiRes 'time','sleep';
my $date = scalar localtime;
my $User=$ARGV[0];
if($User eq "") {
	print "Nom de l'utilisateur � verifier ? ";
	chomp(my $Reponse= <STDIN>);
	#print $Reponse;
	if($Reponse eq "") {
        	print "Usage : VerifSurf.pl nom_utilisateur\n";
        	exit 0;
	}else{
		$User=$Reponse;
	}
}
$Host=`hostname`;
$PROXY="localhost";
$now_string = strftime "%d-%m-%Y %H:%M:%S", localtime;
print "--------------------------------------------------------------------\n";
print BOLD, YELLOW, "$now_string INFO surf user:$User\n",RESET;
print "--------------------------------------------------------------------\n";
print "\n";
print "Presence extraction Amande  : ";
my $Nom=`sudo grep $User /etc/squid/pwd-squid`;
chomp($Nom);
if( $Nom =~ /$User/) {
	print BOLD, GREEN, " OK ", RESET;
}else{
	print BOLD, RED, " User Non Detecte (Mot de passe non chang� recement ou depuis moins de 2 heures) ", RESET;
}
print $Nom;
print "\n";
print "\n";
my $profildef="";
print "Recuperation du profile de l'utilisateur\n";
my $profil=`sudo grep $User /etc/dansguardian/dansguardian0/common/filtergroupslist`;
chomp($profil);
if($profil =~ /filter2/) {
	$profil="Profil INTERDIT : Aucune nagivation autorisee";
	$UserPsin="local.psin-interdit";
	$profildef="interdit";
}elsif($profil =~ /filter3/) {
	$profil="Profil INITIAL : Il faut valider les CGU";
	$UserPsin="local.psin-standard";
	$profildef="standard";
}elsif($profil =~ /filter4/) {
	$profil="Profil STANDARD";
	$UserPsin="local.psin-standard";
	$profildef="standard";
}elsif($profil =~ /filter5/) {
	$profil="Profil VIP : Tout Autorise";
	$UserPsin="local.psin-vip";
	$profildef="vip";
}else{
	$profil="Profil INCONNU : $profil";
	$UserPsin="local.psin-standard";
	$profildef="standard";
}
print $profil;
print "\n";

print "Test de connexion : User(U) Psin(P) Quitter(Q)? ";
chomp(my $Reponse= <STDIN>);
#print $Reponse;
if($Reponse eq "U") {
	print "Password $User ?";
	chomp(my $Pass= <STDIN>);
	print "URL a tester ?";
	chomp(my $URL= <STDIN>);
	my $test=`/usr/lib/nagios/plugins/check_surf_auth.pl -u $URL -x $PROXY:3128 -o $profildef -A $User -p $Pass -e 'e'`;
	print $test;
}elsif($Reponse eq "P") {
	print "Password $UserPsin ?";
	chomp(my $Pass= <STDIN>);
	print "URL a tester ?";
	chomp(my $URL= <STDIN>);
	my $test=`/usr/lib/nagios/plugins/check_surf_auth.pl -u $URL -x $PROXY:3128 -o $profildef -A $UserPsin -p $Pass -e 'e'`;
	print $test;

}else{
    exit 0;
}
