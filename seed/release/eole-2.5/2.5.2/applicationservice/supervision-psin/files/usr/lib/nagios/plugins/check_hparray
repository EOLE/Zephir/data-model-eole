#! /bin/sh
######################################################################
# Name: check_hparray
# By: Copyright (C) 2007 Magnus Glantz
# Credits to: andreiw
######################################################################
# Licence: GPL 2.0
######################################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
######################################################################
# Description:
#
# A Nagios plugin that checks HP Proliant hardware raid via the HPACUCLI tool.
#
# HPACUCLI needs administrator rights.
# add this line to /etc/sudoers
#
# nagios      ALL=NOPASSWD: /usr/sbin/hpacucli
######################################################################

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROGNAME=`basename $0`
PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
REVISION=`echo '$Revision: 1.0 $' | sed -e 's/[^0-9.]//g'`
HPACUCLI=/usr/sbin/hpssacli
if [ -f /usr/sbin/hpacucli ]; then
	HPACUCLI=/usr/sbin/hpacucli
else
	if [ -f /usr/sbin/hpssacli ]; then
		HPACUCLI=/usr/sbin/hpssacli
	else
	   echo "Outils HP non installes\n"
           exit 1
	fi
fi
disk_nv="0:0:0:0:0"
if [ -f /usr/lib/nagios/plugins/NoCheckHpDisk.txt ]; then
        disk_nv=`grep -i I /usr/lib/nagios/plugins/NoCheckHpDisk.txt`
    fi
#echo $disk_nv
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4

print_usage() {
        echo ""
        echo "Usage: $PROGNAME -s <slot-number> ou auto"
        echo "Usage: $PROGNAME [-h | --help]"
        echo "Usage: $PROGNAME [-d | --debug]"
        echo "Usage: $PROGNAME [-V | --version]"
        echo ""
        echo " NOTE:"
        echo ""
        echo " HPACUCLI needs administrator rights."
        echo " add this line to /etc/sudoers"
        echo " nagios      ALL=NOPASSWD: /usr/sbin/hpacucli"
        echo ""
}

print_help() {
        print_revision $PROGNAME $REVISION
        echo ""
        print_usage
        echo ""
        echo "This plugin checks hardware status for HP Proliant servers using HPACUCLI utility."
        echo ""
        support
        exit 0
}

if [ $# -lt 1 ]; then
    print_usage
    exit $STATE_UNKNOWN
fi

check_raid()
{
        #print $check;
        raid_ok=`echo $check|grep -i ok|wc -l`
        raid_warning=`echo $check|grep -i rebuild|wc -l`
        raid_warning2=`echo $check|grep -i 'Predictive Failure'|wc -l`
        #echo $raid_warning2
        raid_critical_1=`echo $check|grep -i failed|grep -v $disk_nv|wc -l`
        raid_critical_2=`echo $check|grep -i recovery|wc -l`
        raid_upgrade=`echo $check|grep -i 'UPGRADE REQUIRED'|wc -l`

        err_check=`expr $raid_ok + $raid_warning + $raid_warning2 + $raid_critical_1 + $raid_critical_2`

        if [ $err_check -eq "0" ]; then
            if [ $raid_upgrade -eq "1" ]; then
                checkm=`echo $check|sed -e '/^$/ d'`
                echo "EOLE-MAT2-C001 $PROGNAME PAS DE VERIFICATION. $checkm"
                exit 0
            fi
                checkm=`echo $check|sed -e '/^$/ d'`
                echo "EOLE-MAT2-C001 $PROGNAME Error. $checkm"
                exit 2
        fi

        if [ $raid_ok -ge "1" ]; then
                exit_status=$STATE_OK
        fi
        if [ $raid_warning -ge "1" ]; then
                exit_status=$STATE_WARNING
        fi
        if [ $raid_warning2 -ge "1" ]; then
                exit_status=$STATE_WARNING
        fi
        if [ $raid_critical_1 -ge "1" ]; then
                exit_status=$STATE_CRITICAL
        fi
        if [ $raid_critical_2 -ge "1" ]; then
                exit_status=$STATE_CRITICAL
        fi

        if [ $exit_status -eq "0" ]; then
                msg_ok=`echo $check|grep -i ok`
                echo "RAID OK - ($msg_ok)"
                exit $exit_status
        elif [ $exit_status -eq "1" ]; then
                msg_warning=`echo $check|grep -i rebuild`
                msg_warning2=`echo $check|grep -i 'Predictive Failure'`
                echo "EOLE_MAT2-W002 RAID WARNING - ($msg_warning $msg_warning2)"
                exit $exit_status
        elif [ $exit_status -eq "2" ]; then
                msg_critical1=`echo $check|grep -i failed`
                msg_critical2=`echo $check|grep -i recovery`
                echo "EOLE-MAT2-C002 RAID CRITICAL - ($msg_critical1 $msg_critical2)"
                exit $exit_status
        fi
}


case "$1" in
        --help)
                print_help
                exit 0
                ;;
        -h)
                print_help
                exit 0
                ;;
        --version)
                print_revision $PROGNAME $REVISION
                exit 0
                ;;
        -V)
                print_revision $PROGNAME $REVISION
                exit 0
                ;;
        -d)
       			slot=`sudo -u root $HPACUCLI controller all show status | grep Slot`
                echo $slot
			#echo $slot
			#echo $slot | awk -F' ' '( $5 == "Slot" ) {print $6}' | read var1 var2 var3 var4 var5 var6 var7
			var1=`echo ${slot}|cut -d' ' -f6`;
			#echo "var1=$var1"
			if [ -z $var1 ]; then
                verif=`lspci|grep -i 'Smart Array'|wc -l`
                if [ $verif -ge "1" ]; then
				    echo "EOLE-MAT2-C001 Pas de controlleur detecte ANORMAL"
                		exit 2
                else
				    echo "EOLE-MAT2-C001 Pas de controlleur detecte VERIFICATION PSIN OK"
                		exit 0
                fi
			else
				check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
                echo $check
				check=`sudo -u root $HPACUCLI controller slot=$var1 pd all show`
                echo $check
        			slot_err=`echo $check|grep -i "not have any logical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
                        echo "EOLE-MAT2-C001 Pas de disques logique detectes controleur $var1"
                        exit 0;
                    fi
        			slot_err=`echo $check|grep -i "not have any physical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
                        echo "EOLE-MAT2-C001 Pas de disques physiques detectes controleur $var1"
                        exit 0;
                    fi
        			slot_err=`echo $check|grep -i Error|wc -l`
        			if [ $slot_err -ge "1" ]; then
					var1=`echo ${slot}|cut -d' ' -f12`;
					check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
				fi
			fi
		check_raid
        ;;
        -s)
       			slot=`sudo -u root $HPACUCLI controller all show status | grep Slot`
                #echo $slot
			#echo $slot
			#echo $slot | awk -F' ' '( $5 == "Slot" ) {print $6}' | read var1 var2 var3 var4 var5 var6 var7
			##
            #print "sudo -u root /usr/sbin/hpacucli controller all show status | grep Slot";
            var1=`echo ${slot}|cut -d' ' -f6`;
            var0=`echo ${slot}|cut -d' ' -f6`;
            #exit;
			if [ -z $var1 ]; then
				echo "EOLE-MAT2-C001 Pas de controlleur detecte"
                		exit 0
			else
				#check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
                #echo $check
				#echo "sudo -u root $HPACUCLI controller slot=$var1 pd all show"
				check=`sudo -u root $HPACUCLI controller slot=$var1 pd all show`
                #echo $check
        			slot_err=`echo $check|grep -i "not have any logical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
				        var1=`echo ${slot}|cut -d' ' -f12`;
                         if [ -z $var1 ]; then
                            var1=$var0
                        fi
                        check=`sudo -u root $HPACUCLI controller slot=$var1 pd all show`
        			    slot_err=`echo $check|grep -i "not have any logical drives"|wc -l`
        			    if [ $slot_err -ge "1" ]; then
                            echo "EOLE-MAT2-C001 Pas de disques logique detectes controleur $var1"
                            exit 0;
                        fi
                    fi
        			slot_err=`echo $check|grep -i "not have any physical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
				        var1=`echo ${slot}|cut -d' ' -f12`;
                         if [ -z $var1 ]; then
                            var1=$var0
                        fi
                        check=`sudo -u root $HPACUCLI controller slot=$var1 pd all show`
        			    slot_err=`echo $check|grep -i "not have any physical drives"|wc -l`

        			    if [ $slot_err -ge "1" ]; then
                            echo "EOLE-MAT2-C001 Pas de disques physiques detectes controleur $var1"
                            exit 0;
                        fi
                    fi
        			slot_err=`echo $check|grep -i Error | grep -v 'write errors'|wc -l`
        			if [ $slot_err -ge "1" ]; then
                        lib_err=`echo $check|grep -i Error`
                        echo "EOLE-MAT2-C001 Erreur detectee $lib_err\n";
                        exit 2;
					    #var1=`echo ${slot}|cut -d' ' -f12`;
                        #echo "Recherche sur slot $var1";
					    #check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
				    fi
			fi
		check_raid
        ;;
        -ss)
		if [ $2 -ne "99" ]; then
                	check=`sudo -u root $HPACUCLI controller slot=$2 ld all show`
                else
       			slot=`sudo -u root $HPACUCLI controller all show status | grep Slot`
			#echo $slot
			#echo $slot | awk -F' ' '( $5 == "Slot" ) {print $6}' | read var1 var2 var3 var4 var5 var6 var7
			var1=`echo ${slot}|cut -d' ' -f6`;
			#echo "var1=$var1"
			if [ -z $var1 ]; then
				echo "EOLE-MAT2-C001 Pas de controlleur detecte"
                		exit 0
			else
				check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
        			slot_err=`echo $check|grep -i "not have any logical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
                        echo "EOLE-MAT2-C001 Pas de disques logique detectes controleur $var1"
                        exit 0;
                    fi
        			slot_err=`echo $check|grep -i "not have any physical drives"|wc -l`
        			if [ $slot_err -ge "1" ]; then
                        echo "EOLE-MAT2-C001 Pas de disques physiques detectes controleur $var1"
                        exit 0;
                    fi
        			slot_err=`echo $check|grep -i Error|wc -l`
        			if [ $slot_err -ge "1" ]; then
					var1=`echo ${slot}|cut -d' ' -f12`;
					check=`sudo -u root $HPACUCLI controller slot=$var1 ld all show`
				fi
			fi
		fi
		check_raid
                ;;
        *)
                print_usage
                ;;
esac
