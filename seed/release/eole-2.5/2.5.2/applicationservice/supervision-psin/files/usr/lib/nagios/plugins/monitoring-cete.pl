#!/usr/bin/perl
#
#Ce script a �t� developp� par TEAMLOG sous Licence GPL
#Il a pour objet de tester un service applicatif de bout en bout et se
#base sur le logiciel webinject pour la partie scriptage Web.
#
#Il effectue plusieurs Tests numerotes de 1 a 9 qui sont :
#
# Phase 1) Reseau IP Local: Test d'un ping local (utilisation de la passerelle par defaut)
# Phase 2) Reseau IP Distant: Test d'un ping distant: deux adresse du Ministere seront utilis�s
#          Le ping distant sera declar� non fonctionnel si les deux adresses ne r�ponde pas.
# Phase 3) Resolution DNS: Test de resolution d'une url via le DNS
# Phase 4) Acces direct au web: Test d'acc�s a un (ou plusieurs site web generaus ex: google)
# Phase 5) Validation du proxy: Test d'acc�s au memes serveurs web que pr�c�dament mais via le proxy
# Phase 6) Acces a l'application X: On essaye d'acceder a la page d'accueil de l'application
# Phase 7) Authentification Cerbere: On verifie si on peux s'authentifier sur cerbere
# Phase 8) Navigation dans l'application X: On affiche une page de l'application autre que l'accueil
# Phase 9) Base de donnes Oracle: On effectue une requete qui interoge la base Oracle.
#
#Si un des test echoue, le process renvoi un message d'erreur indiquant quelle phase n'a pas pu etre men�e a bien.

$version = "1.2";
use Getopt::Long;
use Net::SMTP;

use LWP;
use HTTP::Request::Common;

use XML::Simple;
use Time::HiRes 'time','sleep';

getoptions();

my $host=`hostname`;

#pour sortie Nagios
my %STATUS_CODE = (  'UNKNOWN'  => '-1',
                     'OK'       => '0',
                     'WARNING'  => '1',
                     'CRITICAL' => '2'   );


unless ($configfile) { # Ausun fichier de config passe en ligne de commande
    $configfile = "cete-config-base";
}
require $configfile || die "Impossible d'ouvrir le fichier de configuration $configfile";

@Phase; @Message; @Time; @Resultat; @contexte; @TempsPage; @TaillePage;
$Ok = "green";
$Warning = "yellow";
$Critical = "red";
$Unknown = "unknown";
$Errscan = "";
$libelle = "";


#seuil temps reponse par defaut si non definie
unless ($tps_warn) {$tps_warn = 7;}
unless ($tps_crit) {$tps_crit = 12;}
#$tps_warn=7;
#$tps_crit=12;
$taille=0;
$testnum = 0;
$timetemptot = 0;
$heurerelance=0;
use POSIX qw(strftime);
#    $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;
$now_string = strftime "%d-%m-%Y %H:%M:%S", localtime;
$now_string2 = strftime "%d/%m/%Y", localtime;
$datetest = $now_string;
$datetest2 = $now_string2;
$datetestT = time;
$aj = strftime "%d/%m/%Y %H:%M:%S", localtime;
unless ($premiertest) {$premiertest = 1;}
unless ($derniertest) {$derniertest = $nombredetests;}
#unless ($portail) {$portail = 1;}

#-------------------------
#Phase 1: reseau IP Local
#-------------------------

$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#recuperation de l'adresse de la passerelle par defaut
$route = `/sbin/route | grep default`;
($passerelle) = $route =~ /(\d+\.\d+\.\d+\.\d+)/;
#print "\nRoute: $route\n\n";
#print "\nPasserelle: $passerelle\n\n";

unless ($passerelle) {
	#$route = `/sbin/route | grep localnet`;
	$route = `/sbin/route | grep 10.0.0.0`;
	($passerelle) = substr($route,0,30) =~ /10.0.0.0.*(\d+\.\d+\.\d+\.\d+)*/;
    @result = split(" ",$route);
    $passerelle = $result[1];
#print "\nroute: $route \npasserelle : $passerelle\n\n";
	unless ($passerelle) {
		$Phase[$testnum] = "$Critical";
		$Message[$testnum] = "Impossible de determiner la passerelle par defaut via 'ip route'";
	}else {
		$ping = `$plugin/check_ping -H $passerelle -w $pingwarningtimeout,30% -c $pingcriticaltimeout,60%`;
		#print "\nRESULTAT PING LOCAL: $ping\n";
		if ($ping =~ /PING OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "$ping"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
		elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping local fonctionne mais depasse le timeout d'alerte ($pingwarningtimeout ms)"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
		elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping local a depass� le timeout critique ($pingcriticaltimeout ms)"}
		else { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Impossible d'utiliser la fonction nagios check_ping"};
	}
	}
else {
	$ping = `$plugin/check_ping -H $passerelle -w $pingwarningtimeout,30% -c $pingcriticaltimeout,60%`;
	#print "\nRESULTAT PING LOCAL: $ping\n";
	if ($ping =~ /PING OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "$ping"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
	elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping local fonctionne mais depasse le timeout d'alerte ($pingwarningtimeout ms)"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
	elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping local a depass� le timeout critique ($pingcriticaltimeout ms)"}
	else { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Impossible d'utiliser la fonction nagios check_ping"};
}
$Message[$testnum]="R�seau Local (".$passerelle.") : ".$Message[$testnum];
$timetemptot=$Time[$testnum];
#warn "message=$Message[$testnum] temps=$timetemptot";
preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}


#---------------------------
#Phase 2: reseau IP Distant
#---------------------------

$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
$ping = `$plugin/check_ping -H $pingdistant1 -w $pingwarningtimeout,30% -c $pingcriticaltimeout,60%`;
#print "\nRESULTAT PING DISTANT1: $ping\n";
if ($ping =~ /PING OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = $ping; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping distant fonctionne mais depasse le timeout d'alerte ($pingwarningtimeout ms)"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
else { #Le ping distant du premier serveur a echoue, on teste le deuxieme
	$ping = `./check_ping -H $pingdistant2 -w $pingwarningtimeout,30% -c $pingcriticaltimeout,60%`;
	#print "\nRESULTAT PING DISTANT2: $ping\n";
	if ($ping =~ /PING OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = $ping; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
	elsif ($ping =~ /PING WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "Le ping distant fonctionne mais depasse le timeout d'alerte ($pingwarningtimeout ms)"; ($Time[$testnum]) = $ping =~ /RTA \= (.*) ms/;}
	elsif ($ping =~ /PING CRITICAL/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "Le ping distant a depass� le timeout critique ($pingcriticaltimeout ms)"}
        }
$Message[$testnum]="Acces r�seau distant : ".$Message[$testnum];
$timetemptot=$Time[$testnum];
preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}



#------------------------
#Phase 3: Resolution DNS
#------------------------

#ATTENTION, si la resolution est active dans le cache, il se peut qu'un probleme DNS ne soit pas detecte.
$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
$VideDns = `rndc flush`;
$dns = `$plugin/check_dns -H $dnsatester -t 5`;
#print "\nRESULTAT DNS: $dns\n";
if ($dns =~ /DNS OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Resolution de $dnsatester: $dns";($Time[$testnum]) = $dns =~ /DNS OK: (.*) second/; };
if ($dns =~ /CRITICAL/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "Le test de resolution DNS echou�"};
$Message[$testnum]="Test DNS : ".$Message[$testnum];
$timetemptot=$Time[$testnum];
preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}


#----------------------------------
#Phase 4: verification proces proxy
#----------------------------------

#On utilise le plugin check_snmp_process et squid comme serveru proxy

$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
$process = `$plugin/check_procs -a squid -w 1:70 -c 1:90`;

#if ($process =~ /process matching/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Verification process squid: $process";($Time[$testnum]) = $process =~ /(.*) processes /;}
if ($process =~ /PROCS OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Verification process squid: $process";($Time[$testnum]) = $process =~ /(.*) processes /;}
if ($process =~ /CRITICAL/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "Pas de process squid sur le serveur proxy";}
if ($process =~ /CRITIQUE/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "Pas de process squid sur le serveur proxy";}
if ($process =~ /No response/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$process"; ($Time[$testnum]) = 0;}

$Message[$testnum]="Test Process Proxy : ".$Message[$testnum];
$timetemptot=$Time[$testnum];
preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}



#-----------------------------
#Phase 5: Validation du proxy
#-----------------------------

#On utilise webinject sur un service connu (google) avec un fichier de configuration
#qui specifie le proxy a utiliser.

$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#$Resultat[$testnum] = `/usr/bin/perl webinject.pl --config config-proxy.xml google.xml`;
$Resultat[$testnum] = `$testphase5`;

if ($Resultat[$testnum] =~ /WebInject OK/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "$Resultat[$testnum]"; ($timetemp) = $Resultat[$testnum] =~ /succes en (.*) seconds/; $Time[$testnum] = $timetemp;};
if ($Resultat[$testnum] =~ /WebInject WARNING/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "$Resultat[$testnum]"};
if ($Resultat[$testnum] =~ /WebInject CRITICAL/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$Resultat[$testnum]"};
$Message[$testnum]="Test PROXY LOCAL (Google) : ".$Message[$testnum];
$timetemptot=$Time[$testnum];
preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}


if($opt_detail) {

#----------------------------------
#Phase 6
#----------------------------------

$testnum++;
#warn "Traitement phase tesnum=$testnum";
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;
$Resultatwebinject = `$testphase6`;

if ($Resultatwebinject =~ /WebInject OK/) {
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/;
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/;
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/;
	$i = $testnum;
		$Phase[$i] = "$Ok";
		$Message[$i] = "$Resultatwebinject";
		$Time[$i] = $timetemp;
		$timetemptot=$timetemp;
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) {
		($timetemp) = $Resultatwebinject =~ /time=(.{1}\..{3})/;
		$i = $testnum;
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
		($timetemp) = $Resultatwebinject =~ /time=(.{1}\..{3})/;
		($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
		$i = $testnum;
		$Time[$i] = $timetemp;
		$timetemptot=$timetemp;
		#$timetemptot=0;
		$Phase[$i] = "$Critical";
		$Message[$i] = "$Resultatwebinject";

	#	if ($testnum < $phasecritique) {
	#		$Phase[$testnum] = "$Ok";
	#		$Message[$testnum] = "Phase $testnum passe avec succes";
	#		}
	#	elsif ($testnum == $phasecritique) {
	#		$Phase[$testnum] = "$Critical";
	#		$Message[$testnum] = "$Resultatwebinject";
	#		}
	#	elsif ($testnum > $phasecritique) {
	#		$Phase[$testnum] = "$Unknown";
	#		$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
	#		}
	#	$testnum++;
		}
}



#----------------------------------
#Phase 7
#----------------------------------

$testnum++;
#warn "Traitement phase tesnum=$testnum";
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;
$Resultatwebinject = `$testphase7`;
#warn $Resultatwebinject;

if ($Resultatwebinject =~ /WebInject OK/) {
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/;
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/;
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/;
	$i = $testnum;
		$Phase[$i] = "$Ok";
		$Message[$i] = "$Resultatwebinject";
		$Time[$i] = $timetemp;
		$timetemptot=$timetemp;
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) {
	$i = $testnum;
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
	($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
	$i = $testnum;
	$timetemptot=0;
		$Phase[$i] = "$Critical";
		$Message[$i] = "$Resultatwebinject";

	#	if ($testnum < $phasecritique) {
	#		$Phase[$testnum] = "$Ok";
	#		$Message[$testnum] = "Phase $testnum passe avec succes";
	#		}
	#	elsif ($testnum == $phasecritique) {
	#		$Phase[$testnum] = "$Critical";
	#		$Message[$testnum] = "$Resultatwebinject";
	#		}
	#	elsif ($testnum > $phasecritique) {
	#		$Phase[$testnum] = "$Unknown";
	#		$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
	#		}
	#	$testnum++;
		}
}

#----------------------------------
#Phase 8
#----------------------------------

$testnum++;
#warn "Traitement phase tesnum=$testnum";
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;
$Resultatwebinject = `$testphase8`;
#warn $Resultatwebinject;
if ($Resultatwebinject =~ /WebInject OK/) {
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/;
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/;
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/;
	$i = $testnum;
		$Phase[$i] = "$Ok";
		$Message[$i] = "$Resultatwebinject";
		$Time[$i] = $timetemp;
		$timetemptot=$timetemp;
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) {
	$i = $testnum;
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
	($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
	$i = $testnum;
	$timetemptot=0;
		$Phase[$i] = "$Critical";
		$Message[$i] = "$Resultatwebinject";

	#	if ($testnum < $phasecritique) {
	#		$Phase[$testnum] = "$Ok";
	#		$Message[$testnum] = "Phase $testnum passe avec succes";
	#		}
	#	elsif ($testnum == $phasecritique) {
	#		$Phase[$testnum] = "$Critical";
	#		$Message[$testnum] = "$Resultatwebinject";
	#		}
	#	elsif ($testnum > $phasecritique) {
	#		$Phase[$testnum] = "$Unknown";
	#		$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
	#		}
	#	$testnum++;
		}
}


#----------------------------------
#Phase 9
#----------------------------------

$testnum++;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#warn "Traitement phase tesnum=$testnum";
#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;
$Resultatwebinject = `$testphase9`;

if ($Resultatwebinject =~ /WebInject OK/) {
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/;
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/;
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/;
	$i = $testnum;
		$Phase[$i] = "$Ok";
		$Message[$i] = "$Resultatwebinject";
		$Time[$i] = $timetemp;
		$timetemptot=$timetemp;
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) {
	$i = $testnum;
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
	($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
	$i = $testnum;
	$timetemptot=0;
		$Phase[$i] = "$Critical";
		$Message[$i] = "$Resultatwebinject";

	#	if ($testnum < $phasecritique) {
	#		$Phase[$testnum] = "$Ok";
	#		$Message[$testnum] = "Phase $testnum passe avec succes";
	#		}
	#	elsif ($testnum == $phasecritique) {
	#		$Phase[$testnum] = "$Critical";
	#		$Message[$testnum] = "$Resultatwebinject";
	#		}
	#	elsif ($testnum > $phasecritique) {
	#		$Phase[$testnum] = "$Unknown";
	#		$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
	#		}
	#	$testnum++;
		}
}
}
else {
#----------------------------------
#Phase 6 a 9: Les dernieres phases
#----------------------------------

$testnum++;

#print "passage $testnum";

if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {
#$Resultatwebinject = `/usr/bin/perl webinject.pl --config config-proxy.xml dapcete.xml`;

$Resultatwebinject = `$testphase6a9`;

if ($Resultatwebinject =~ /WebInject OK/) {
	($timetemp) = $Resultatwebinject =~ /succes en (.*) seconds/;
	($taille) = $Resultatwebinject =~ /page : (.*) Octets/;
	($Errscan) = $Resultatwebinject =~ /Err=(.*)-/;
	#($Libelle) = $Resultatwebinject =~ /(.*)taille/;
	if($nombredetests >=6) { ($TempsPage[6]) = $Resultatwebinject =~ /T0=(.*)P0/; }
	if($nombredetests >=7) { ($TempsPage[7]) = $Resultatwebinject =~ /T1=(.*)P1/; }
    if($nombredetests >=8) { ($TempsPage[8]) = $Resultatwebinject =~ /T2=(.*)P2/; }
	if($nombredetests >=9) { ($TempsPage[9]) = $Resultatwebinject =~ /T3=(.*)P3/; }
	if($nombredetests >=10) { ($TempsPage[10]) = $Resultatwebinject =~ /T4=(.*)P4/; }
	if($nombredetests >=11) { ($TempsPage[11]) = $Resultatwebinject =~ /T5=(.*)P5/;	 }
	if($nombredetests >=12) { ($TempsPage[12]) = $Resultatwebinject =~ /T6=(.*)P6/; }
	if($nombredetests >=13) { ($TempsPage[13]) = $Resultatwebinject =~ /T7=(.*)P7/;	 }
	if($nombredetests >=14) { ($TempsPage[14]) = $Resultatwebinject =~ /T8=(.*)P8/;	 }
	if($nombredetests >=15) { ($TempsPage[15]) = $Resultatwebinject =~ /T9=(.*)P9/;	 }
	if($nombredetests >6) { ($TaillePage[6]) = $Resultatwebinject =~ /P0=(.*)- T1/; } else { ($TaillePage[6]) = $Resultatwebinject =~ /P0=(.*) -/; }
	if($nombredetests >7) { ($TaillePage[7]) = $Resultatwebinject =~ /P1=(.*)- T2/; } else { ($TaillePage[7]) = $Resultatwebinject =~ /P1=(.*) -/; }
    if($nombredetests >8) { ($TaillePage[8]) = $Resultatwebinject =~ /P2=(.*)- T3/; } else { ($TaillePage[8]) = $Resultatwebinject =~ /P2=(.*) -/; }
	if($nombredetests >9) { ($TaillePage[9]) = $Resultatwebinject =~ /P3=(.*)- T4/; } else { ($TaillePage[9]) = $Resultatwebinject =~ /P3=(.*) -/; }
	if($nombredetests >10) { ($TaillePage[10]) = $Resultatwebinject =~ /P4=(.*)- T5/; } else { ($TaillePage[10]) = $Resultatwebinject =~ /P4=(.*) -/; }
	if($nombredetests >11) { ($TaillePage[11]) = $Resultatwebinject =~ /P5=(.*)- T6/; } else { ($TaillePage[11]) = $Resultatwebinject =~ /P5=(.*) -/; }
	if($nombredetests >12) { ($TaillePage[12]) = $Resultatwebinject =~ /P6=(.*)- T7/; } else { ($TaillePage[12]) = $Resultatwebinject =~ /P6=(.*) -/; }
	if($nombredetests >13) { ($TaillePage[13]) = $Resultatwebinject =~ /P7=(.*)- T8/; } else { ($TaillePage[13]) = $Resultatwebinject =~ /P7=(.*) -/; }
	if($nombredetests >14) { ($TaillePage[14]) = $Resultatwebinject =~ /P8=(.*)- T9/; } else { ($TaillePage[14]) = $Resultatwebinject =~ /P8=(.*) -/; }
	if($nombredetests >15) { ($TaillePage[15]) = $Resultatwebinject =~ /P9=(.*) -/; } else { ($TaillePage[15]) = $Resultatwebinject =~ /P9=(.*) -/; }
	for ($i = $testnum; $i <= $nombredetests; $i++) {
		$Phase[$i] = "$Ok";
		$Message[$i] = "$Resultatwebinject";
		#$Time[$i] = $timetemp;
		$Time[$i] = $TempsPage[$i];
		$libelle = "OK Page accede avec succes en $Time[$i] secondes - Taille : $TaillePage[$i] Ko";
		$Message[$i] = $libelle;
		#print $libelle;
		}
	}
elsif ($Resultatwebinject =~ /WebInject WARNING/) {
	for ($i = $testnum; $i <= $nombredetests; $i++) {
		$Phase[$i] = "$Warning";
		$Message[$i] = "$Resultatwebinject";
		}
	}
elsif ($Resultatwebinject =~ /WebInject CRITICAL/) {
	($phasecritique) = $Resultatwebinject =~ /Phase (.*) :/;
	$timetemp=0;
	while ($testnum <= $nombredetests) {
		if ($testnum < $phasecritique) {
			$Phase[$testnum] = "$Ok";
			$Message[$testnum] = "Phase $testnum passe avec succes";
		}
		elsif ($testnum == $phasecritique) {
			$Phase[$testnum] = "$Critical";
			$Message[$testnum] = "$Resultatwebinject";
		}
		elsif ($testnum > $phasecritique) {
			$Phase[$testnum] = "$Unknown";
			$Message[$testnum] = "Test non effectue car un test precedent a echoue.";
		}
		$testnum++;
	}
}
$timetemptot=$timetemp;
}
#$timetemptot=$timetemp;
}


#---------------------------------------------------------------------------------------------------------------
#Phase 99: Traffic Proxy (phase ind�pendante lanc�e seule avec option -p99 -d99 et r�serv�e supervision locale
#---------------------------------------------------------------------------------------------------------------

$testnum = 99;
if (($premiertest <= $testnum) and ($testnum <= $derniertest)) {

	$trafic = `$plugin/check_graph_traffic_metl.pl -H localhost -C public -i 2 `;
	#print "\nRESULTAT PING LOCAL: $trafic\n";
	if ($trafic =~ /WARNING TRAFIC IN/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /WARNING TRAFIC OUT/) { $Phase[$testnum] = "$Warning"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /CRITICAL TRAFIC IN/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /CRITICAL TRAFIC OUT/) { $Phase[$testnum] = "$Critical"; $Message[$testnum] = "$trafic"; ($Time[$testnum]) = $trafic =~ /Traffic (.*)\./;}
	elsif ($trafic =~ /No response/) { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "Pas de reponse SNMP"; ($Time[$testnum]) = 0;}
	else { $Phase[$testnum] = "$Ok"; $Message[$testnum] = "TRAFFIC OK ".$trafic;($Time[$testnum]) = $trafic =~ /Traffic (.*)\-/;}
	$Message[$testnum]="Trafic Proxy local : ".$Message[$testnum];
	$timetemptot=$Time[$testnum];
	#warn "message=$Message[$testnum] temps=$timetemptot";
	preparereport($testnum, $Phase[$testnum], $Message[$testnum]);
}



#----------------------
#Traitement particulier
#----------------------

if($traitpart) {
traitpart();
exit();
}

#------------------
#Edition du rapport
#------------------

sendreport();
#Fin du Programme.



#------------
#Sub routines
#------------

#------------------------------------------------------------------
sub preparereport {
	my ($phasenum,$phaseresult,$phasemessage) = @_;
	#print " DEBUG: Phase $phasenum: $phaseresult - $phasemessage\n";
	if ($phaseresult eq $Critical) {# Ce test a �chou�
		for ($i = ($phasenum + 1); $i <= $nombredetests; $i++) {
			$Phase[$i] = $Unknown;
			$Message[$i] = "Test non effectue car un test precedent a echoue.";
			}
		sendreport();
		}
	}

#------------------------------------------------------------------
sub sendreport {
	$Globalstatus = "$Ok";

	my	$Trait	= '-' x 60;
	if ($typerapport ne "Nagios") {
		print	"\n",
			"Resultats des tests:\n",
			$Trait,"\n";
	}
	for ($i = $premiertest; $i <= $derniertest; $i++) {
		if ($Phase[$i]) {
			if (($Phase[$i] eq "$Warning") and ($Globalstatus ne "$Critical")) {
				$Globalstatus = "$Warning";
			}
			if ($Phase[$i] eq "$Critical") { $Globalstatus = "$Critical"; }
			chomp($Message[$i]);
			$Erreur="Phase $i ($Testname[$i]): $Phase[$i]: $Message[$i]";
			#$Time[$i] contient le temps de traitement
			if ($typerapport ne "Nagios") {
				print	"Phase $i ($Testname[$i]): $Phase[$i]: $Message[$i]\n";
			}
		}
		else { #La Phase n'a pas ete effetu�
			$Erreur="Phase $i ($Testname[$i]): $Phase[$i]: $Message[$i]";
			if ($typerapport ne "Nagios") {
				print	"Phase $i ($Testname[$i]): $Unknown: Phase non definie\n";
			}
		}
	}

	if ($typerapport ne "Nagios") {
		print	$Trait,"\n",
		"GLOBAL_STATUS: $Globalstatus\n",
		"Temps Page : $timetemptot secondes\n";
	} else {
		if ($Globalstatus eq "green") {
			#print "WebInject OK - All tests passed successfully in $timetemptot secondes |time=$timetemptot;30;;0\n";
			#print "OK ",$Message[$derniertest]," |time=",$timetemptot,";30;;0\n";
			print "OK ".$Message[$derniertest]," |time=",$timetemptot,";30;;0\n";
		} elsif ($Globalstatus eq "red") {
			print "CRITIQUE ",$Message[$derniertest]," |time=",$timetemptot,";30;;0\n";
			#print "WebInject CRITICAL - $Erreur |time=$timetemptot;30;;0\n";
		} else {
			print "WARNING ",$Message[$derniertest]," |time=",$timetemptot,";30;;0\n";
			#print "WebInject WARNING - $Erreur |time=$timetemptot;30;;0\n";
		}
	}


$host=`hostname`;
if ($host =~ /bb4/) {
	#insert_acai();
	$heuremail=0;
	if($mailto) {
		prepare_mail();
	}

	$mailtob="";
    insert_acai();
}
else {
	if ($monitoring eq "2") {
		#create_acai();
	}
}
#print "status=$Globalstatus";
if ($Globalstatus eq "green") {
	exit($STATUS_CODE{"OK"});
} elsif ($Globalstatus eq "red") {
	exit($STATUS_CODE{"CRITICAL"});
} else {
	exit($STATUS_CODE{"WARNING"});
}
exit();
}

#------------------------------------------------------------------
sub getoptions {
    Getopt::Long::Configure('bundling');
    GetOptions(
       'v|version'         => \$opt_version,
       'h|help'            => \$opt_help,
       'p|premiertest=s'     => \$premiertest,
       'd|derniertest=s'     => \$derniertest,
       'c|config=s'          => \$configfile,
       'D|detail'          => \$opt_detail,
    ) or do {
        print_usage();
        exit();
        };
    if($opt_version) {
        print "monitoring-cete version $version.\n";
        exit();
    }
    if($opt_help) {
        print_usage();
        exit();
    }
    sub print_usage {
      print <<EOB

Usage:
      monitoring-cete.pl [-p|--premiertest NUM] [-d|--derniertest NUM2] [-c|--config CONFIGFILE] [-D (avec detail ph 6a9)
      monitoring-cete.pl --version|-v


EOB
    }
}

#------------------------------------------------------------------
#
#
#
#------------------------------------------------------------------
sub insert_acai {

if ($monitoring eq "1") {

}
}



#------------------------------------------------------------------
sub create_acai {
#warn "Cr�ation fichier html";

#my $TBI_FILE = "/var/www/html/ent_vue_acai";
#        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
#        print FILE "<html>\n";
#		print FILE "<head>\n";
#		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
#		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
#		print FILE "</head>\n";
#		print FILE "<body>\n";
#		print FILE "<table border='0' width='100%'>\n";
#		print FILE "<tr><td valign='top' bgcolor='#FF854A' width='80%'>&nbsp;<font face='Arial' size='4'>Supervision des applications nationales - Vue Locale</font></td></tr>\n";
#		print FILE "<tr><td valign='top' width='80%'><p align='left'>&nbsp;<br>\n";
# 		print FILE "<table border='0' cellspacing='0' width='100%' bgcolor='#DEF2FE'><tr>\n";
#    	print FILE "<td align='left' width='27%' valign='top'>&nbsp;</td>\n";
#    	print FILE "<td align='center' width='4%' valign='top' bgcolor='#66CCFF'><font face='Verdana' size='2'><b>Etat</b></font></td>\n";
#    	print FILE "<td align='center' width='4%' valign='top' bgcolor='#66CCFF'><font face='Verdana' size='2'><b>Tps</b></font></td>\n";
#    	print FILE "<td align='left' width='23%' valign='top' bgcolor='#66CCFF'><font face='Verdana' size='2'><b>Derni�re Mesure</font></b></td></tr>\n";
#  		print FILE "</table></td></tr></table>\n";
#		print FILE "</body>\n";
#	    print FILE "</html>\n";
#       close(FILE);


if ($derniertest == 1){
		my $TBI_FILE = "/var/www/html/acai/vue_1rl";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		print FILE "Proxy <a href='https://172.20.210.10:10000/' target='_blank'>$proxy</a> : Derni�re mesure le $datetest<br>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>RESEAU LOCAL : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[1]'> ($Time[1] ms)\n";
		} else {
		print FILE "<font size='2' face='Verdana'>RESEAU LOCAL : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[1]'> ($Time[1] ms)\n";
		}
		print FILE "</body>\n";
	    print FILE "</html>\n";
        close(FILE);
} elsif ($derniertest == 2){
		my $TBI_FILE = "/var/www/html/acai/vue_2rd";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>RESEAU DISTANT : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[2]'> ($Time[2] ms)\n";
		} else {
		print FILE "<font size='2' face='Verdana'>RESEAU DISTANT : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[2]'> ($Time[2] ms)\n";
		}
		print FILE "</body>\n";
	    print FILE "</html>\n";
        close(FILE);
} elsif ($derniertest == 3){
		my $TBI_FILE = "/var/www/html/acai/vue_3dns";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>DNS : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[3]'> ($Time[3] ms)\n";
		} else {
		print FILE "<font size='2' face='Verdana'>DNS : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[3]'> ($Time[3] ms)\n";
		}
		print FILE "</body>\n";
	    print FILE "</html>\n";
        close(FILE);
} elsif ($derniertest == 4){
		my $TBI_FILE = "/var/www/html/acai/vue_5proxy";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>Process Proxy : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[4]'> ($Time[4] process)\n";
		} else {
		print FILE "<font size='2' face='Verdana'>Process Proxy : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[4]'> ($Time[4] process)\n";
		}
		print FILE "</body>\n";
	    print FILE "</html>\n";
        close(FILE);
} elsif ($derniertest == 5){
		my $TBI_FILE = "/var/www/html/acai/vue_4proxy";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>PROXY LOCAL (Acces Internet) : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[5]'> ($Time[5] ms)\n";
		} else {
		print FILE "<font size='2' face='Verdana'>PROXY LOCAL (Acces Internet) : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[5]'> ($Time[5] ms)\n";
		}
		print FILE "<br></body>\n";
	    print FILE "</html>\n";
        close(FILE);
} elsif ($derniertest == 99){
		my $TBI_FILE = "/var/www/html/acai/vue_6trafic";
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
		if ($Globalstatus eq "red" ) {
		print FILE "<font size='2' face='Verdana'>TRAFIC PROXY LOCAL : <img border='0' src='r_red.gif' width='14' height='14' alt='$Message[99]'> ($Time[99] )\n";
		} elsif ($Globalstatus eq "yellow" ) {
		print FILE "<font size='2' face='Verdana'>TRAFIC PROXY LOCAL : <img border='0' src='r_orange.gif' width='14' height='14' alt='$Message[99]'> ($Time[99] )\n";
		} else {
		print FILE "<font size='2' face='Verdana'>TRAFIC PROXY LOCAL : <img border='0' src='r_green.gif' width='14' height='14' alt='$Message[99]'> ($Time[99] )\n";
		}
		print FILE "</body>\n";
	    print FILE "<br><br></html>\n";
        close(FILE);
} else {
my $TBI_FILE = "/var/www/html/acai/vue_".$application."_".$test;
        open(FILE,">".$TBI_FILE.".html") or die "Can't open $TBI_FILE for writing: $!";
        print FILE "<html>\n";
		print FILE "<head>\n";
		print FILE "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>\n";
		print FILE "<title>Supervision Applications Acai Vue Locale</title>\n";
		print FILE "</head>\n";
		print FILE "<body>\n";
  		print FILE "<table border='0' cellspacing='0' width='100%' bgcolor='#DEF2FE'>\n";
		print FILE "<tr><td align='left' width='27%' valign='top'><font color='#003399' size='2' face='Verdana'>\n";
     	print FILE "$application   $test  </font></td>\n";
		if ($Globalstatus eq "red" ) {
    		print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='$Erreur'></td>\n";
		} else {
    		print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_green.gif' width='14' height='14' alt='$Erreur' ></td>\n";
		}

		if ($timetemptot >= $tps_crit ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='Temps sup�rieur � $tps_crit s'></td>\n";
		}
		elsif ($timetemptot >= $tps_warn ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_orange.gif' width='14' height='14' alt='Temps sup�rieur � $tps_warn s'></td>\n";
		}
		elsif ($timetemptot <= 0 ) {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_red.gif' width='14' height='14' alt='Acces impossible'></td>\n";
		}
		else {
			print FILE "<td align='center' width='4%' valign='top'><img border='0' src='r_green.gif' width='14' height='14' alt='Seuil Warning : $tps_warn s Critique : $tps_crit s'></td>\n";
		}

		print FILE "<td align='left' width='23%' valign='top'><font face='Verdana' size='2'>$timetemptot s&nbsp;le&nbsp; $datetest</font></td></tr>\n";
   		print FILE "</table>\n";
		print FILE "</body>\n";
	    print FILE "</html>\n";
        close(FILE);

}
}

exit();


