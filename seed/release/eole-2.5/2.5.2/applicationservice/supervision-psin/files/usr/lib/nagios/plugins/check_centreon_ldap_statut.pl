#!/usr/bin/perl -w
####################### check_ldap.pl #######################
# Version : 1.0
# Date : 28 Oct 2010
# Author  : PSIN P.Malossane
# Licence : GPL - http://www.fsf.org/licenses/gpl.txt
#############################################################
#
# help : ./check_ldap_statut
# lecture du resultat du check_ldap_statut sur le serveur pilot� par le crontab
#lecture du fichier mode_ldap.log
#alimentatiion de centreon

use strict;
use Getopt::Long;

# Nagios specific

use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS $TIMEOUT);
use Time::HiRes 'time','sleep';
#my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);
use POSIX qw(strftime);
#    $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;


my $filelocal="/usr/lib/nagios/plugins/mode_ldap.log";
#my $filelocal="/usr/local/nagios/libexec/mode_ldap.log";
my $l;
my $ligne;
my $CodeSortie=0;
my $message="";

my $testprocess=`sudo crontab -l -u root | grep ldap_statut | wc -l`;
if(!$testprocess =~ /1/) {
	print "EOLE-LDAP-C001 Recherche Statut CRITIQUE : ATTENTION Aucune ou Plusieurs lignes check_ldap_statut dans le crontab \n";
	exit 2;

}
#verification age fichier
my $testfic=`/usr/lib/nagios/plugins/check_file_age -w 600 -c 600 -f $filelocal`;
if($testfic =~ /CRITICAL/) {
	print "EOLE-LDAP-C001 Recherche Statut CRITIQUE : Pas de controle - fichier mode_ldap.log non mise a jour . verifier le crontab ou process ecdl_bascule_ldap en cours\n";
	exit 2;
}

	#lecture du fichier
	open(DESCR,"<$filelocal");
	while( defined( $l = <DESCR> ) )
	{
   		chomp $l;
		$ligne=$.;
		if($ligne==1) {
			$CodeSortie=$l;
		}
		if($ligne==2) {
			$message=$l."\n";
		}
   		#print "$. : $l\n";
	}

	close(DESCR);

#print "CodeSortie=".$CodeSortie." message=".$message;
print $message;
exit $CodeSortie;


#if($CodeSortie==2) { exit $ERRORS{"CRITICAL"};}
#if($CodeSortie==1) { exit $ERRORS{"WARNING"};}
#if($CodeSortie==0) { exit $ERRORS{"OK"};}
#if($CodeSortieL==0) { exit $ERRORS{"OK"};}
1
