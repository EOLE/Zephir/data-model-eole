#! /usr/bin/perl
#use strict;
#Check fonctionnement GEOIDE
#V 0.1  29/01/2014  PSIN P.Malossane
#Verification partage samba GB_*
#Verification des droits (script fournià
#Verification des ACL (script fourni)
#Verification des logs des drontab
#
use Getopt::Long;
use vars qw($opt_V $opt_h $opt_v $opt_id $opt_t $opt_c $opt_ia $opt_err1 $opt_err2 $opt_err3 $opt_il $opt_ip);
#my $opt=$ARGV[0];
Getopt::Long::Configure('bundling');
Getopt::Long::Configure( "pass_through" );
GetOptions
    ("h"   => \$opt_h, "help"         => \$opt_h,
     "v"   => \$opt_v, "verbose" => \$opt_v,
     "t"   => \$opt_t, "test" => \$opt_t,
     "c"   => \$opt_c, "check" => \$opt_c,
     "m"   => \$opt_m, "check" => \$opt_m,
     "a"   => \$opt_ia, "ignore-acl" => \$opt_ia,
     "p"   => \$opt_ip, "ignore-partage" => \$opt_ip,
     "l"   => \$opt_il, "ignore-log" => \$opt_il,
     "e=s"   => \$opt_err1, "err1=s" => \$opt_err1,
     "g=s"   => \$opt_err2, "err1=s" => \$opt_err2,
     "f=s"   => \$opt_err3, "err1=s" => \$opt_err3,
     "i"   => \$opt_id, "ignore-droits" => \$opt_id);

if($opt_h) {
    print "check_geoide.pl -v pour verbose -t pour test  installation\n";
    print "options complementaire :\n";
    print "-i pour ignorer le tests sur les droits\n";
    print "-a pour ignorer le tests sur les ACL\n";
    print "-p pour ignorer le tests sur les partages\n";
    print "-e texte pour erreur a rechercher dans replication.log (defaut=error)\n";
    print "-g texte pour erreur a rechercher dans gb_echanges.log (defaut=error)\n";
    print "-f texte pour erreur a rechercher dans gp_verif.log (defaut=error)\n";
    print "-c pour check installation et version paquets GEOIDE\n";
    print "-m pour check modere : warning si fichier log inexistant \n";
    print "-l pour ignoer le check des logs \n";
    exit 0;
}
$CheckGeo="yes";
if( -e "/usr/lib/nagios/plugins/ParamCheck.pl") {
            require "/usr/lib/nagios/plugins/ParamCheck.pl";
            $CheckGeo=$ParamGeobase;
}
   my $module="Paquets Installes : ";
   my @verif=`dpkg -l | grep eole-geo`;
   foreach $item (@verif) {
       #print $item."\n";
        @detail=split(/\s+/,$item);
        #print $detail[1]." (".$detail[2].")\n";
        $module=$module." ".$detail[1]." (".$detail[2].")";

    }
    #print $module;
    if( -e "/usr/bin/ParseDico") {
    	$verif=`sudo /usr/bin/ParseDico --list geoide_base_replic_activated`;
    }else{
	$verif=`CreoleGet geoide_base_replic_activated non`;
    }
    if($verif =~ /oui/) {
        #print " Replication:activee";
        $module=$module." Replication:activee";
    }else{
        #print " Replication:NON ACTIVEE";
        $module=$module." Replication:NON ACTIVEE";
    }
    if( -e "/usr/bin/ParseDico") {
    	$verif=`sudo /usr/bin/ParseDico --list activer_geoide_echanges`;
    }else{
	$verif=`CreoleGet activer_geoide_echanges non`;
    }
    
    if($verif =~ /oui/) {
        #print " Echanges:activee";
        $module=$module." Echanges:activee";
    }else{
        #print " Echanges:NON ACTIVEE";
        $module=$module." Echanges:NON ACTIVEE";
    }
    #print a"\n";


#verif iinstallation geobase

my $VerifP2=`/usr/lib/nagios/plugins/check_geoidebase.pl -H 127.0.0.1 -t`;
if($VerifP2 =~ /GEOBASE INSTALL OK/) {
	print "ATTENTION Le palier 2 est installe ou en cours d'installation\n";	
	exit 2;
}

if (-e "/etc/samba/includes/smb-geoide.conf" and -e "/var/log/geobase" and $module =~ /eole-geo-ide-base/) {
	my $fic="ok";
	if($opt_t) {
		print "GEOBASE INSTALL OK\n";
		exit 0;
	}
}else{
    print "Check GEOBASE : GEOBASE NON INSTALLE ou Incomplet $module\n";
    exit 1;
}


if($opt_c) {
    print $module."\n";
    exit 0;
}
if(!$opt_err1) { $opt_err1="error";}
if(!$opt_err2) { $opt_err2="error";}
if(!$opt_err3) { $opt_err3="error";}
unless($opt_id) {
    $opt_id="oui";
}else{
    $opt_id="non";
}
unless($opt_ia) {
    $opt_ia="oui";
}else{
    $opt_ia="non";
}
unless($opt_il) {
    $opt_il="oui";
}else{
    $opt_il="non";
}
unless($opt_ip) {
    $opt_ip="oui";
}else{
    $opt_ip="non";
}
#Initialisation
unless( -f "/var/log/geobase/replication.old") { system("sudo touch /var/log/geobase/replication.old");}
unless( -f "/var/log/geobase/gb_verif.old") { system("sudo touch /var/log/geobase/gb_verif.old");}
unless( -f "/var/log/geobase/gb_echange.old") { system("sudo touch /var/log/geobase/gb_echange.old");}
my $EtatPartage="";
my $EtatDroits="";
my $EtatACL="";
my $EtatLog="";
my $CodeRetour="0";
#verification partage
$EtatPartage="Verification Partage ignoree";
if($opt_ip eq "oui") {
	if($opt_v) { print "lecture des partages reseau...\n";}
	if($opt_v) { print "smbclient -N -L localhost\n";}
	my $check='smbclient -N -L "localhost" 2>/dev/null';
	my $result=`$check`;
	if( ($result =~ /GB_ADM/) && ($result =~ /GB_CONS/) && ($result =~ /GB_PROD/) && ($result =~ /GB_REF/)) {
      		 $EtatPartage="Partage OK";
	}else{
      		 $EtatPartage="Partage CRITIQUE";
      		 if( !($result =~ /GB_ADM/) ) { $EtatPartage=$EtatPartage." GP_ADM manquant ";}
      		 if( !($result =~ /GB_CONS/) ) { $EtatPartage=$EtatPartage." GP_CONS manquant ";}
       		if( !($result =~ /GB_PROD/) ) { $EtatPartage=$EtatPartage." GP_PROD manquant ";}
       		if( !($result =~ /GB_REF/) ) { $EtatPartage=$EtatPartage." GP_REF manquant ";}
       		$CodeRetour="2";
	}
}
#verification des droits
$EtatDroits="";
if($opt_id eq "oui") {
    if($opt_v) { print "Verification persmissions :sudo /usr/lib/geobase/check_geobase_perms.sh -a\n";}
    $check="sudo /usr/lib/geobase/check_geobase_perms.sh -a";
    $result=`$check`;
    if($opt_v) { print $result."\n";}
    if( ($result =~ /accessible/) || ($result =~ /accessible/)) {
        $check="sudo /usr/lib/geobase/check_geobase_perms.sh -a >/usr/lib/nagios/plugins/probleme_droits.log";
        $result=`$check`;
       $EtatDroits="Droits CRITIQUES (voir fichier probleme_droits.log)";
       $CodeRetour="2";
    }else{
       $EtatDroits="Droits OK";
    }
}
#verification des ACL
$EtatACL="";
if($opt_ia eq "oui") {
    if($opt_v) { print "Verification ACL : /usr/lib/geobase/list_geobase_acl.sh\n";}
    if (! -e "/usr/lib/nagios/plugins/liste_acl_geobase.ref") {
        print "initialisation acl ...";
        $check="sudo /usr/lib/geobase/list_geobase_acl.sh >/usr/lib/nagios/plugins/liste_acl_geobase.ref 2>/dev/null";
        $result=`$check`;
    }else{
        $check="sudo /usr/lib/geobase/list_geobase_acl.sh >/usr/lib/nagios/plugins/liste_acl_geobase.log 2>/dev/null";
        $result=`$check`;
    }
    $check="sudo diff /usr/lib/nagios/plugins/liste_acl_geobase.log /usr/lib/nagios/plugins/liste_acl_geobase.ref";
    $result=`$check`;
    if( ($result =~ /---/) ) {
        $check="sudo diff /usr/lib/nagios/plugins/liste_acl_geobase.log /usr/lib/nagios/plugins/liste_acl_geobase.ref >/usr/lib/nagios/probleme_acl.log";
        $result=`$check`;
       $EtatACL="ACL CRITIQUES (Voir fichier probleme_acl.log)";
       $CodeRetour="2";
    }else{
       $EtatACL="ACL OK";
    }
}else{
	$effacl=`rm -f /usr/lib/nagios/plugins/liste_acl_geobase.ref`;
}
#traiteùdes logs
$EtatLog="Verification LOG IGNOREE";
if($opt_il eq "oui") {
   if( -e "/usr/bin/ParseDico") {
     $verif=`sudo /usr/bin/ParseDico --list geoide_base_replic_activated`;
   }else{
     $verif=`CreoleGet geoide_base_replic_activated non`;
   }
#print $verif;
  if($verif =~ /oui/) {
        if (! -e "/var/log/geobase/replication.log") {
            $veriflog=`sudo /usr/lib/nagios/plugins/check_log -F /var/log/geobase/replication.log -O /var/log/geobase/replication.old -q $opt_err1`;
            if($veriflog =~ /Log check ok/) {
                $EtatLog="Log Replication OK";
            }else{
                $EtatLog="Log Replication CRITIQUE $opt_err1 trouve (Voir /var/log/geobase/replication.log)";
                $CodeRetour="2";
            }
        }else{
		if($opt_m) {
                	$EtatLog="Log Replication DEGRADE Pas de fichier /var/log/geobase/replication.log";
                	$CodeRetour="1";
		}else{
                	$EtatLog="Log Replication CRITIQUE Pas de fichier /var/log/geobase/replication.log";
			$CodeRetour="2";
		}
        }
}else{
    $EtatLog="Log Replication non actif";
}
if( -e "/usr/bin/ParseDico") {
  $verif=`sudo /usr/bin/ParseDico --list activer_geoide_echanges`;
}else{
  $verif=`CreoleGet activer_geoide_echanges non`;
}
#print $verif;
if($verif =~ /oui/) {
        if (! -e "/var/log/geobase/gb_echange.log") {
            $veriflog=`sudo /usr/lib/nagios/plugins/check_log -F /var/log/geobase/gb_echange.log -O /var/log/geobase/gb_echange.old -q $opt_err2`;
            if($veriflog =~ /Log check ok/) {
                $EtatLog=$EtatLog." Log Echanges OK";
            }else{
                $EtatLog=$EtatLog." Log Echanges CRITIQUE $opt_err2 trouve (Voir /var/log/geobase/gb_echange.log)";
                $CodeRetour="2";
            }
        }else{
		if($opt_m) {
                	$EtatLog="Log Echanges DEGRADE Pas de fichier /var/log/geobase/gb_echange.log";
                	$CodeRetour="1";
		}else{
                	$EtatLog="Log Echanges CRITIQUE Pas de fichier /var/log/geobase/gb_echange.log";
			$CodeRetour="2";
		}
        }
}else{
    $EtatLog=$EtatLog." Log Echange non actif";
}
if( -e "/usr/bin/ParseDico") {
  $verif=`sudo /usr/bin/ParseDico --list geoide_base_verif_activated`;
}else{
  $verif=`CreoleGet geoide_base_verif_activated non`;
}
#print $verif;
if($verif =~ /oui/) {
        unless( -f "/var/log/geobase/gb_verif.log") { system("sudo touch /var/log/geobase/gb_verif.log");}
        $veriflog=`sudo /usr/lib/nagios/plugins/check_log -F /var/log/geobase/gb_verif.log -O /var/log/geobase/gb_verif.old -q $opt_err3`;
        if($veriflog =~ /Log check ok/) {
            $EtatLog=$EtatLog." Log Verification OK";
        }else{
            $EtatLog=$EtatLog." Log Verification CRITIQUE $opt_err3 trouve (Voir /var/log/geobase/gb_verif.log)";
            $CodeRetour="2";
        }
}else{
    $EtatLog=$EtatLog." Log Verification non actif";
}
}

#Verif connexion entrepot central
#print "Test acces entrepot national (app-ftp17.sen.centre-serveur.i2)\n";
my $EtatServ=`/usr/lib/nagios/plugins/check_ftp.pl -H app-ftp17.sen.centre-serveur.i2 -v`;
if($EtatServ =~ /OK/) {
        $EtatServ="Connexion Entrepot central OK";
}else{
        $EtatServ="Connexion Entrepot central CRITIQUE";
        $CodeRetour="2";
}
my $EtatServ2=`/usr/lib/nagios/plugins/check_http -a 'geoide:1!kLp%98' -H supplements.geoide-distribution.application.i2 -t 30`;
if($EtatServ2 =~ /OK/) {
        $EtatServ2="Connexion Referentiel Distribution OK";
}else{
        $EtatServ2="Connexion Referentiel Distribution CRITIQUE";
        $CodeRetour="2";
}

#Retour
#print "checkgeo=".$CheckGeo;
if($CodeRetour eq "2") {
    if($CheckGeo eq "No") {
        print "Supervision GEOBASE non effectuee volontairement\n";
        exit 0;
    }else{
        print "Check Module GEOBASE CRITIQUE $EtatPartage $EtatDroits $EtatACL $EtatLog $module $EtatServ $EtatServ2\n";
        exit 2;
    }
}

if($CodeRetour eq "1") {
    if($CheckGeo eq "No") {
        print "Supervision GEOBASE non effectuee volontairement\n";
        exit 0;
    }else{
        print "Check Module GEOBASE DEGRADE $EtatPartage $EtatDroits $EtatACL $EtatLog $module $EtatServ $EtatServ2\n";
         exit 1;
    }
}
if($CheckGeoeq eq "No") {
    $suparam=`sed -i 's/^\$ParamGeobase="yes"/\$ParamGeobase="no"/' ParamCheck.pl`;
}
print "Check Module GEOBASE OK $EtatPartage $EtatDroits $EtatACL $EtatLog $module $EtatServ $EtatServ2\n";
exit 0;


