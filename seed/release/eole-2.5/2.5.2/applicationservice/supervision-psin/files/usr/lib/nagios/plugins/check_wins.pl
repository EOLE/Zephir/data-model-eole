#!/usr/bin/perl -w
## check_wins.pl Verification de l'interrogation wins � partir du serveur 
## Auteur PSIN PM
##

use strict;

$ENV{'PATH'}='';
$ENV{'BASH_ENV'}='';
$ENV{'ENV'}='';


if ($#ARGV+1 !=2) {
	usage();
	exit 0;
}


my $serveur_wins=$ARGV[0];
my $domaine=$ARGV[1];

my $status=`/usr/bin/nmblookup -R -U $serveur_wins $domaine#1b`;
#print $status;
if ($status =~ /$domaine<1b>/) {
        print "Check serveur wins OK $status \n";
	exit 0;
}elsif ($status =~ /WARNING/) {
        print "Check serveur wins WARNING \n";
	exit 1;
}else {
        #($resu) = $status =~ /RTA \= (.*) ms/;
        print "Check serveur wins CRITIQUE : $status \n";
	exit 2;
}


sub usage {
	print <<EOL
 check_wins.pl <IP serveur WINS> <domaine> 
	
EOL

}


exit 0;
