#!/usr/bin/perl -w
####################### check_ldap.pl #######################
# Version : 1.0
# Date : 24 Jul 2007 
# Author  : De Bodt Lieven (Lieven.DeBodt at gmail.com)
# Licence : GPL - http://www.fsf.org/licenses/gpl.txt
#############################################################
#
# help : ./check_ldap.pl -h

use strict;
use Getopt::Long;

# Nagios specific

use lib "/usr/local/nagios/libexec";
use utils qw(%ERRORS $TIMEOUT);
use Time::HiRes 'time','sleep';
#my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);

# Globals
#my $debug = 2;
#use vars qw($PROGNAME);
#use vars qw($opt_w $opt_c);


my $Version='1.0';
my $Name=$0;
my $latency;
#my $critical=5;
#my $warning=3;
my $starttimer;
my $endtimer;
my $o_search="numEntries: 1";

my $o_host =		undef; 		# hostname 
my $o_login=		undef;		# LDAP login
my $o_help=		undef; 		# wan't some help ?
my $opt_w=		undef; 		# wan't some help ?
my $opt_c=		undef; 		# wan't some help ?
my $opt_v;

# functions

sub show_versioninfo { print "$Name version : $Version\n"; }

sub print_usage {
    print "Usage: $Name -H <host> -l <uid recherche> [-m <mot cle (numEntries: 1 par defaut)>] [ -v (verbose)] [ -w <temps warning> ( 3 s par defaut)] [ -c <temps critique> ( 5 s par defaut)] \n";
}

# Get the alarm signal (just in case ldap timout screws up)
$SIG{'ALRM'} = sub {
     print ("ERROR: Alarm signal (Nagios time-out)\n");
     exit $ERRORS{"CRITICAL"};
};

sub help {
    print "Usage: $Name -H <host> -l <uid recherche> [-m <mot cle (numEntries: 1 par defaut)>] [ -v (verbose)] [ -w <temps warning> ( 3 s par defaut)] [ -c <temps critique> ( 5 s par defaut)] \n";
}

sub check_options {
    Getopt::Long::Configure ("bundling");
    GetOptions(
        'h'     => \$o_help,    	'help'        	=> \$o_help,
        'H:s'   => \$o_host,		'hostname:s'	=> \$o_host,
        'l:s'   => \$o_login,           'login:s'       => \$o_login,
        'm:s'   => \$o_search,          'mot:s'         => \$o_search,
        'w=s'   => \$opt_w,		'warn=s'     => \$opt_w,
        'c=s'   => \$opt_c,		'crit=s'    => \$opt_c,
        'v'     => \$opt_v,             'verbose'       => \$opt_v
    );
    if (defined ($o_help)) { help(); exit $ERRORS{"UNKNOWN"}};
    # Check compulsory attributes
    if ( !defined($o_host) ) { print_usage(); exit $ERRORS{"UNKNOWN"}};
}

########## MAIN #######

check_options();
#($opt_c) || ($opt_c = shift) || ($opt_c =5);
#my $critical = $1 if ($opt_c =~ /([0-9]+)/);

#($opt_w) || ($opt_w = shift) || ($opt_w =3);
#my $warning = $1 if ($opt_w =~ /([0-9]+)/);
my $critical = 5;
#if ($opt_c && $opt_c =~ /[0-9]+/) {
if ($opt_c ) {
$critical = $opt_c;
}
my $warning = 3;
if ($opt_w && $opt_w =~ /[0-9]+/) {
$warning = $opt_w;
}


if ($critical <= $warning){
    print "(-c) doit etre superieur a (-w)";
    print_usage();
    exit $ERRORS{'OK'};
}


#print "critiac=$critical warning=$warning";

my $ldap;
 $starttimer = time();
$ldap=`ldapsearch -h $o_host -x uid=$o_login`;
$endtimer = time();
$latency = (int(1000 * ($endtimer - $starttimer)) / 1000);
if ($opt_v) {
	print $ldap ;
}
#chomp @results;
if ($ldap =~ /$o_search/) {
	if ($latency > $critical) {
	    print "CRITICAL! UID $o_login TROUVE sur $o_host mais Temps critique  $latency s > $critical s ($o_search)|time=$latency\n";
	    exit $ERRORS{"CRITICAL"};
	}elsif($latency > $warning) {
	    print "WARNING! UID $o_login TROUVE sur $o_host mais Temps warning $latency s  > $warning s ($o_search)|time=$latency\n";
	    exit $ERRORS{"WARNING"};
	}else{
	    print "OK! UID $o_login TROUVE sur $o_host en $latency s ($o_search)|time=$latency\n";
	    exit $ERRORS{"OK"};
	}
}else{
	print "CRITICAL ! UID $o_login NON TROUVE sur $o_host en $latency s|time=$latency\n";
    	exit $ERRORS{"CRITICAL"};
}

