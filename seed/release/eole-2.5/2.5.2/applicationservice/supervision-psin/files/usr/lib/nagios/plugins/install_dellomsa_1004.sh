#!/bin/bash
cd /tmp
wget ftp://debmiroir-01.ac.centre-serveur.i2/ubuntu/pool/universe/g/gcc-3.3/libstdc++5_3.3.6-15ubuntu6_i386.deb
sudo dpkg -i --force-depends libstdc++5_3.3.6-15ubuntu6_i386.deb

wget ftp://debmiroir-01.ac.centre-serveur.i2/ubuntu/pool/main/p/procmail/procmail_3.22-16ubuntu3_i386.deb
sudo dpkg -i procmail_3.22-16ubuntu3_i386.deb

wget ftp://debmiroir-01.ac.centre-serveur.i2/ubuntu/pool/main/o/openipmi/libopenipmi0_2.0.13-0ubuntu3_i386.deb
sudo dpkg -i libopenipmi0_2.0.13-0ubuntu3_i386.deb

wget ftp://debmiroir-01.ac.centre-serveur.i2/ubuntu/pool/main/o/openipmi/openipmi_2.0.13-0ubuntu3_i386.deb
sudo dpkg -i openipmi_2.0.13-0ubuntu3_i386.deb

wget ftp://debmiroir-01.ac.centre-serveur.i2/ubuntu/pool/main/r/rpm/rpm_4.4.2.1-1ubuntu6_i386.deb
sudo dpkg -i --force-depends rpm_4.4.2.1-1ubuntu6_i386.deb

wget ftp://debmiroir-02.ac.centre-serveur.i2/mirror/ftp.sara.nl/pub/sara-omsa/dists/dell/sara/binary-i386/dellomsa_5.5.0-5_i386.deb
sudo  dpkg -i --force-depends dellomsa_5.5.0-5_i386.deb

cd /usr/lib/nagios/plugins

sudo /etc/init.d/dataeng restart

exit
