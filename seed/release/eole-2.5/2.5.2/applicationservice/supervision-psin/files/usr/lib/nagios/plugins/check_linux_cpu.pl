#! /usr/bin/perl -w
#use strict;
my $EtatLoad="";
my $CodeEtatLoad="";
my $EtatProcess="";
my $CodeEtatProcess="0";
my $creef="";
my $Perf="";
my $NbCpu=`ls /sys/devices/system/cpu | grep cpu | grep -v cpufreq | grep -v cpuidle | wc -l`;
chomp($NbCpu);
my $check="";
if($NbCpu =~ /1/) {
    $check="/usr/lib/nagios/plugins/check_load -w 2.5,2.5,2.5 -c 3,5,10";
}else{
    $check="/usr/lib/nagios/plugins/check_load -w 1.2,1.1,1 -c 2,5,10 -r ";
}
my $result=`$check`;
if( $result =~ /OK/) {
       ($EtatLoad) = $result =~ /\- (.*)\|/;
       $CodeEtatLoad="0";
}elsif( $result =~ /WARNING/) {
       ($EtatLoad) = $result =~ /\- (.*)\|/;
       $CodeEtatLoad="1";
}else{
       ($EtatLoad) = $result =~ /\- (.*)\|/;
       $CodeEtatLoad="2";
}
($Perf) = $result =~ /\|(.*)/;
#print "DEBUG: result=$result EtatLoad=$EtatLoad";
my $NomProcess="";
my $NomProcess2="";
my $DureeErr="";
my $ValCpu=0;
my $ErrCpu="";
if($CodeEtatLoad eq "0") {
    $check="/usr/lib/nagios/plugins/check_top_process.sh -c 98.0 -w 90.0";
    $result=`$check`;
       ($ValCpu) = $result =~ /CPU=(.*)%/;

}else{
$check="/usr/lib/nagios/plugins/check_top_process.sh -c 98.0 -w 90.0";
$result=`$check`;
#print $result;
if( $result =~ /CRITIQUE/ or $result =~ /WARNING/) {
       ($ValCpu) = $result =~ /CPU=(.*)%/;
       ($NomProcess) = $result =~ /command=(.*) user/;
       sleep(10);
       my $result2=`$check`;
       if( $result2 =~ /CRITIQUE/ or $result2 =~ /WARNING/) {
     	    ($ValCpu) = $result2 =~ /CPU=(.*)%/;
            sleep(10);
            my $result3=`$check`;
#print $result3;
            if( $result3 =~ /CRITIQUE/) {
	            $ErrCpu=$result3;
       		   ($ValCpu) = $result3 =~ /CPU=(.*)%/;
                   $CodeEtatProcess="2";
                   $EtatProcess=$result3;
            }else{
                   $CodeEtatProcess="0";
                   $EtatProcess=$result3;
       		  if( $result3 =~ /WARNING/) {
	              $ErrCpu=$result3;
                  ($ValCpu) = $result3 =~ /CPU=(.*)%/;
                   $CodeEtatProcess="1";

              }
       		  if( $result3 =~ /OK/) {($ValCpu) = $result3 =~ /CPU=(.*)%/;}
            }
       }else{
            $CodeEtatProcess="0";
            $EtatProcess=$result2;
       	    if( $result2 =~ /WARNING/) {($ValCpu) = $result2 =~ /CPU=(.*)%/;}
       	    if( $result2 =~ /OK/) {($ValCpu) = $result2 =~ /CPU=(.*)%/;}
       }
 }else{
      $CodeEtatProcess="0";
     $EtatProcess=$result;
     if( $result =~ /WARNING/) {($ValCpu) = $result =~ /CPU=(.*)%/;}
     if( $result =~ /OK/) {($ValCpu) = $result =~ /CPU=(.*)%/;}
 }
#print "Codeetatprocess=".$CodeEtatProcess;
}
if($CodeEtatLoad eq "2" or $CodeEtatProcess eq "2") {
       $checkf=`/usr/lib/nagios/plugins/check_file_age /usr/lib/nagios/plugins/_alertecpu.log -w 7600 -c 7601`;
       #print $checkf;
    if($checkf =~ /CRITICAL/) {
		$DureeErr=" ( duree > 2 heure)";
	}elsif($checkf =~ /not found/) {
        $creef=`echo $result > /usr/lib/nagios/plugins/_alertecpu.log`;
		$CodeEtatLoad="1";
		$CodeEtatProcess="1";
		$DureeErr=" ( duree < 2 heure)";
    }else{
		$CodeEtatLoad="1";
		$CodeEtatProcess="1";
		$DureeErr=" ( duree < 2 heure)";
	}
}else{
       $checkf=`rm -f /usr/lib/nagios/plugins/_alertecpu.log`;
}

if($CodeEtatLoad eq "2") {
    if($ErrCpu eq "") { $ErrCpu="Plusieurs process concernes";}
    print "Check CPU CRITIQUE EOLE-SYS-C001$DureeErr ($NbCpu CPU) LOAD:$EtatLoad ".$ErrCpu."|$Perf\n";
    exit 2;
}
if($CodeEtatProcess eq "2") {
    if($ErrCpu eq "") { $ErrCpu="Plusieurs process concernes";}
    print "Check CPU CRITIQUE EOLE-SYS-C001 $DureeErr ($NbCpu CPU) LOAD OK :$EtatLoad ".$ErrCpu."|$Perf \n";
    exit 2;
}

if($CodeEtatLoad eq "1" or $CodeEtatProcess eq "1") {
    if($ErrCpu eq "") { $ErrCpu="Plusieurs process concernes";}
    print "Check CPU WARNING EOLE-SYS-W001 $DureeErr ($NbCpu CPU) LOAD:$EtatLoad ".$ErrCpu."|$Perf\n";
    exit 1;
}

#		$creef=`echo $result > /usr/lib/nagios/plugins/_alertepu.log`;
print "Check CPU NORMAL ($NbCpu CPU) LOAD:$EtatLoad |$Perf\n";
exit 0;
