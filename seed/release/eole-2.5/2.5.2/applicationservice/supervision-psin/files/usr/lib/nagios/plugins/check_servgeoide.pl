#!/usr/bin/perl -w
#
print "Test acces Application catalogue (carto.geo-ide.application)\n";
my $etat=`/usr/lib/nagios/plugins/check_http_curl.pl -u carto.geo-ide.application.i2`;
if($etat =~ /OK/) {
    print "CARTO:OK ".$etat;
}else{
    print `/usr/lib/nagios/plugins/check_http_curl.pl -u carto.geo-ide.application.i2 -v`;
    print "\n\n------------------ tcptraceroute --------------------------\n";
    print `tcptraceroute carto.geo-ide.application.i2`;
    print "\n-----------------------------------------------------------\n";
}
print "Test acces entrepot national (app-ftp17.sen.centre-serveur.i2)\n";
#$etat=`/usr/lib/nagios/plugins/check_ftp.pl -H app-ftp17.sen.centre-serveur.i2 -u geoide -v`;
$etat=`/usr/lib/nagios/plugins/check_ftp_rw.pl --host app-ftp17.sen.centre-serveur.i2 --write -v`;
if($etat =~ /OK/) {
    print "ENT:OK ".$etat;
}else{
    print $etat;
    print "\n\n------------------ tcptraceroute --------------------------\n";
    print `sudo tcptraceroute app-ftp17.sen.centre-serveur.i2 21`;
    print "\n-----------------------------------------------------------\n";
}

print "Test acces geoide Distribution (geoide-distribution.application.i2)\n";
$etat=`/usr/lib/nagios/plugins/check_http -a 'geoide:1!kLp%98' -H supplements.geoide-distribution.application.i2`;
if($etat =~ /OK/) {
    print "DIST:OK ".$etat;
}else{
    print `/usr/lib/nagios/plugins/check_http -H geoide-distribution.application.i2 -e 401 -v`;
    print "\n\n------------------ tcptraceroute --------------------------\n";
    print `tcptraceroute geoide-distribution.application.i2`;
    print "\n-----------------------------------------------------------\n";
}

print "Test acces serveur de gabarit (geostandards.geo-ide.application.i2)\n";
$etat=`/usr/lib/nagios/plugins/check_http_curl.pl -u geostandards.geo-ide.application.i2`;
if($etat =~ /OK/) {
    print "GAB:OK ".$etat;
}else{
    print `/usr/lib/nagios/plugins/check_http_curl.pl -u geostandards.geo-ide.application.i2 -v`;
    print "\n\n------------------ tcptraceroute --------------------------\n";
    print `tcptraceroute geostandards.geo-ide.application.i2`;
    print "\n-----------------------------------------------------------\n";
}
exit;
