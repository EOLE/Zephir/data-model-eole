#!/bin/sh

#Check anon access to SMB for nagios.
# Dave Love <fx@gnu.org>, 2005-07-15

REVISION=1.0
PROGPATH=`echo $0 | /bin/sed -e 's,[\\/][^\\/][^\\/]*$,,'`

. $PROGPATH/utils.sh

usage () {
    echo "\
Nagios plugin to check if (anonymous) access to SMB on host works.

Usage:
  $PROGNAME -H <host>
  $PROGNAME --help
  $PROGNAME --version
"
}

help () {
    print_revision $PROGNAME $REVISION
    echo; usage; echo; support
}

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    usage
    exit $STATE_UNKNOWN
fi

while test -n "$1"; do
    case "$1" in
	--help | -h)
	    help
	    exit $STATE_OK;;
	--version | -V)
	    print_revision $PROGNAME $REVISION
	    exit $STATE_OK;;
	-H)
	    shift
	    host=$1;;
	*)
	    usage; exit $STATE_UNKNOWN;;
    esac
    shift
done

host=`grep 'address' /etc/network/interfaces | /usr/bin/awk '{ print $2 }'`
#datecd=`grep DATECD /etc/sysconfig/.site`
#nbsession=`/usr/bin/smbstatus -p | wc -l`
librelance=""
nbsession=` ps -edf | grep smbd | wc -l`
stdout=`/usr/bin/smbclient -U guest -N -L "$host" 2>&1`

if [ $? -eq 0 ]; then
    header=`echo "$stdout" | grep Server= | head -n 1`
    echo "SMB OK $header Version Serveur : $BASH_VERSION Nombre de session:$nbsession | nbsession=$nbsession"
    exit $STATE_OK
else
    err=`echo "$stdout" | head -n 1`
    if [ $nbsession -eq "1" ] || [ $nbsession -eq "0" ]
    then
    	relance=`sudo /etc/init.d/smbd restart`
    	librelance=" Relance service samba effectuee";
    fi

    echo "SMB CRITICAL Acces partage Samba impossible: $err Version Serveur : $BASH_VERSION Nombre de sessions:$nbsession $librelance| nbsession=$nbsession "
    exit $STATE_CRITICAL
fi
