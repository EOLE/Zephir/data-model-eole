#!/bin/bash

cd /usr/lib/nagios/plugins

echo "Suppression des anciens paquets dell ..."
echo "Appuyer la touche <Entré pour continuer..."
read touche
sudo apt-get --purge remove dellomsa
sudo apt-get --purge remove srvadmin-all
sudo apt-get --purge autoremove
echo "Installation des nouveaux paquets dell ..."
echo "Appuyer la touche <Entré pour continuer..."
read touche
sudo apt-get install srvadmin-all
echo "Demarrage des services dell..."
echo "Appuyer la touche <Entré pour continuer..."
read touche

sudo /etc/init.d/dataeng start
echo "Verification du check ..."
echo "Appuyer la touche <Entré pour continuer..."
read touche

/usr/lib/nagios/plugins/check_dell.pl
echo "Appuyer la touche <Entré pour continuer..."
read touche
#sudo /etc/init.d/dataeng restart

exit

