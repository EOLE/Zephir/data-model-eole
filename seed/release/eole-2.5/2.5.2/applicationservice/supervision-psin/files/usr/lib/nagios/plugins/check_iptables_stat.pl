#!/usr/bin/perl -w
#

# Plugin directory / home of utils.pm.
use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS &print_revision &support &usage);
use Getopt::Long qw(:config no_ignore_case bundling);
use File::Basename;
use Net::DNS;

use strict;

# Path to installed clamd binary.
my $iptable_cmd  = "sudo iptables";
my $warn_val = 1;  # Default -w arg
my $crit_val = 2;  # Default  -c arg
my $help_val = 0;  # Off unless -h arg
my $debug_val = 0;  # Off unless -h arg
my $chaine = "mor-lan";
my $indicateur = "DROP ";

#verif installation
my $verif=`sudo tail -n 10 /root/scripts/accounting.sh`;
if($verif =~ /non activ/) {
        print "Accounting non active sur le zephir\n";
        exit 1;
}
$verif=`sudo ls /root/scripts/`;
#print $verif;
if($verif =~ /affiche_account.sh/) {
    my $verif2=`sudo /root/scripts/affiche_account.sh`;
    if($verif2=~ /flux/) {
        my $instacc="";
    }else{
        my $instacc=`sudo /root/scripts/accounting.sh`;
        print "Installation des regles - ";
        #print "Accounting non installe\n";
        #exit 0;
    }
}else{
        print "Accounting non installe\n";
        exit 0;
}



sub show_help() {
    print <<END;

Usage: check_iptables_stat.pl
-v, --verbose
END
}

GetOptions (
    "w=i" => \$warn_val, "warning=i" => \$warn_val,
    "c=i" => \$crit_val, "critical=i" => \$crit_val,
    "h" => \$help_val, "help" => \$help_val,
    "v" => \$debug_val, "verbose" => \$debug_val,
    "C=s" => \$chaine, "chaine=s" => \$chaine,
    "I=s" => \$indicateur, "indicateur=s" => \$indicateur
);

if ($help_val != 0) {
    &show_help;
    exit 0;
}

my ($total,$diff,$update_time,$row, @last_values, $last_time,$last_VolIntOut, $last_VolIntIn, $last_VolM2Out, $last_VolM2In, $last_VolKAKOut,$last_VolKAKIn,$last_VolSUSOut,$last_VolSUSIn,$last_VolDNSOut,$last_VolDNSIn);
my ($last_VolLdapIn,$last_VolLdapOut,$last_VolTotIn,$last_VolTotOut);
my ($Tindic1,$Tindic2,$Tindic3,$Tindic4,$Tindic5);

#recup volume internet out et in
#chomp(my $VolIntIn = `$iptable_cmd -nvL mor-lan -x |grep "ACC-flux_in-proxies " |awk -F' ' '{print \$2}'`);
#chomp(my $VolIntOut = `$iptable_cmd -nvL lan-mor -x |grep "ACC-flux_out-proxies " |awk -F' ' '{print \$2}'`);
my @Resultat;
chomp(my $VolIntIn = `$iptable_cmd -nvL |grep "ACC-flux_in-proxies "|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolIntOut = `$iptable_cmd -nvL |grep "ACC-flux_out-proxies "|tail -n 1 |awk -F' ' '{print \$2}'`);

chomp(my $VolM2In = `$iptable_cmd -nvL |grep "ACC-flux_in-melanie2 "|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolM2Out = `$iptable_cmd -nvL |grep "ACC-flux_out-melanie2 "|tail -n 1 |awk -F' ' '{print \$2}'`);

chomp(my $VolKAKIn = `$iptable_cmd -nvL |grep "ACC-ports_PF_in-mcafee-clients"|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolKAKOut = `$iptable_cmd -nvL |grep "ACC-ports_PF_out-mcafee-clients"|tail -n 1 |awk -F' ' '{print \$2}'`);

chomp(my $VolSUSIn = `$iptable_cmd -nvL |grep "ACC-flux_in-wsus "|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolSUSOut = `$iptable_cmd -nvL |grep "ACC-flux_out-wsus "|tail -n 1 |awk -F' ' '{print \$2}'`);

chomp(my $VolLdapIn = `$iptable_cmd -nvL |grep "ACC-ports_PF_in-melanie2-ldap"|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolLdapOut = `$iptable_cmd -nvL |grep "ACC-ports_PF_out-melanie2-ldap"|tail -n 1 |awk -F' ' '{print \$2}'`);

chomp(my $VolTotIn = `$iptable_cmd -nvL |grep "ACC-flux_in-morea " | grep "flux_essl_IN"|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolTotOut = `$iptable_cmd -nvL |grep "ACC-flux_out-morea " | grep "flux_essl_OUT"|tail -n 1 |awk -F' ' '{print \$2}'`);
chomp(my $VolDNSIn = `$iptable_cmd -nvL |grep "ACC-flux_in-dnsnat "| tail -n 1 |awk -F' ' '{print \$1}'`);
chomp(my $VolDNSOut = `$iptable_cmd -nvL |grep "ACC-flux_out-dnsnat "| tail -n 1 |awk -F' ' '{print \$1}'`);


if ($debug_val != 0) { print "Volume releve brut Total Out/In ".$VolTotOut."/".$VolTotIn." Internet Out/In :".$VolIntOut."/".$VolIntIn." Volume Melanie2 Out/In : ".$VolM2Out."/".$VolM2In." Volume MCAFEE Out/In : ".$VolKAKOut."/".$VolKAKIn." Volume SUS Out/In : ".$VolSUSOut."/".$VolSUSIn." Volume LDAP Out/In :".$VolLdapOut."/".$VolLdapIn."\n";}
#exit;
#if ($VolIntIn =~ /M/ or $VolIntOut =~ /M/ or $VolM2Int =~ /M/ or $VolM2Out =~ /M/ or $VolKAKIn =~ /M/ or $VolKAKOut =~ /M/ or $VolSUSIn =~ /M/ or $VolSUSOut =~ /M/ or $VolLdapIn =~ /M/ or $VolLdapOut =~ /M/) {
if ($VolTotIn =~ /G/ ) {
        my $raz=`sudo iptables -Z`;
        my $update_time2 = time();
        unless (open(FILE,">"."/tmp/indic_iptable.log")){
         print "Check mod for temporary file : /tmp/indic_iptable.log !\n";
         exit $ERRORS{"UNKNOWN"};
        }
        print FILE "$update_time2:0:0:0:0:0:0:0:0:0:0:0:0";
        close(FILE);
        sleep(30);
        chomp($VolTotIn = `$iptable_cmd -nvL |grep "ACC-flux_in-morea "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolTotOut = `$iptable_cmd -nvL |grep "ACC-flux_out-morea "|tail -n 1 |awk -F' ' '{print \$2}'`);

        chomp($VolIntIn = `$iptable_cmd -nvL |grep "ACC-flux_in-proxies "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolIntOut = `$iptable_cmd -nvL |grep "ACC-flux_out-proxies "|tail -n 1 |awk -F' ' '{print \$2}'`);

        chomp($VolM2In = `$iptable_cmd -nvL |grep "ACC-flux_in-melanie2 "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolM2Out = `$iptable_cmd -nvL |grep "ACC-flux_out-melanie2 "|tail -n 1 |awk -F' ' '{print \$2}'`);

        chomp($VolKAKIn = `$iptable_cmd -nvL |grep "ACC-flux_in-mcafee "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolKAKOut = `$iptable_cmd -nvL |grep "ACC-flux_out-mcafee "|tail -n 1 |awk -F' ' '{print \$2}'`);

        chomp($VolSUSIn = `$iptable_cmd -nvL |grep "ACC-flux_in-wsus "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolSUSOut = `$iptable_cmd -nvL |grep "ACC-flux_out-wsus "|tail -n 1 |awk -F' ' '{print \$2}'`);

        chomp($VolLdapIn = `$iptable_cmd -nvL |grep "ACC-flux_in-ldap "|tail -n 1 |awk -F' ' '{print \$2}'`);
        chomp($VolLdapOut = `$iptable_cmd -nvL |grep "ACC-flux_out-ldap "|tail -n 1 |awk -F' ' '{print \$2}'`);
	chomp($VolDNSIn = `$iptable_cmd -nvL |grep "ACC-flux_in-dnsnat "| tail -n 1 |awk -F' ' '{print \$1}'`);
	chomp($VolDNSOut = `$iptable_cmd -nvL |grep "ACC-flux_out-dnsnat "| tail -n 1 |awk -F' ' '{print \$1}'`);
        if ($debug_val != 0) { print "Volume releve brut apres RAZ Total Out/In :".$VolTotOut."/".$VolTotIn." Internet Out/In :".$VolIntOut."/".$VolIntIn." Volume Melanie2 Out/In : ".$VolM2Out."/".$VolM2In." Volume MCAFEE Out/In : ".$VolKAKOut."/".$VolKAKIn." Volume SUS Out/In : ".$VolSUSOut."/".$VolSUSIn." Volume LDAP Out/In :".$VolLdapOut."/".$VolLdapIn."\n";}
}

if ($VolTotOut eq ""){$VolTotOut=0;}
if ($VolTotOut =~ /K/){$VolTotOut=~ s/K/000/;}
if ($VolTotOut =~ /M/){$VolTotOut=~ s/M/000000/;}
if ($VolTotOut =~ /G/){$VolTotOut=~ s/G/000000000/;}
if ($VolTotIn eq ""){$VolTotIn=0;}
if ($VolTotIn =~ /K/){$VolTotIn=~ s/K/000/;}
if ($VolTotIn =~ /M/){$VolTotIn=~ s/M/000000/;}
if ($VolTotIn =~ /G/){$VolTotIn=~ s/G/000000000/;}
if ($VolIntIn eq ""){$VolIntIn=0;}
if ($VolIntIn =~ /K/){$VolIntIn=~ s/K/000/;}
if ($VolIntIn =~ /M/){$VolIntIn=~ s/M/000000/;}
if ($VolIntIn =~ /G/){$VolIntIn=~ s/G/000000000/;}
if ($VolM2In eq ""){$VolM2In=0;}
if ($VolM2In =~ /K/){$VolM2In=~ s/K/000/;}
if ($VolM2In =~ /M/){$VolM2In=~ s/M/000000/;}
if ($VolM2In =~ /G/){$VolM2In=~ s/G/000000000/;}
if ($VolKAKIn eq ""){$VolKAKIn=0;}
if ($VolKAKIn =~ /K/){$VolKAKIn=~ s/K/000/;}
if ($VolKAKIn =~ /M/){$VolKAKIn=~ s/M/000000/;}
if ($VolKAKIn =~ /G/){$VolKAKIn=~ s/G/000000000/;}
if ($VolSUSIn eq ""){$VolSUSIn=0;}
if ($VolSUSIn =~ /K/){$VolSUSIn=~ s/K/000/;}
if ($VolSUSIn =~ /M/){$VolSUSIn=~ s/M/000000/;}
if ($VolSUSIn =~ /G/){$VolSUSIn=~ s/G/000000000/;}
if ($VolLdapIn eq ""){$VolLdapIn=0;}
if ($VolLdapIn =~ /K/){$VolLdapIn=~ s/K/000/;}
if ($VolLdapIn =~ /M/){$VolLdapIn=~ s/M/000000/;}
if ($VolLdapIn =~ /G/){$VolLdapIn=~ s/G/000000000/;}
if ($VolIntOut eq ""){$VolIntOut=0;}
if ($VolIntOut =~ /K/){$VolIntOut=~ s/K/000/;}
if ($VolIntOut =~ /M/){$VolIntOut=~ s/M/000000/;}
if ($VolIntOut =~ /G/){$VolIntOut=~ s/G/000000000/;}
if ($VolM2Out eq ""){$VolM2Out=0;}
if ($VolM2Out =~ /K/){$VolM2Out=~ s/K/000/;}
if ($VolM2Out =~ /M/){$VolM2Out=~ s/M/000000/;}
if ($VolM2Out =~ /G/){$VolM2Out=~ s/G/000000000/;}
if ($VolKAKOut eq ""){$VolKAKOut=0;}
if ($VolKAKOut =~ /K/){$VolKAKOut=~ s/K/000/;}
if ($VolKAKOut =~ /M/){$VolKAKOut=~ s/M/000000/;}
if ($VolKAKOut =~ /G/){$VolKAKOut=~ s/G/000000000/;}
if ($VolSUSOut eq ""){$VolSUSOut=0;}
if ($VolSUSOut =~ /K/){$VolSUSOut=~ s/K/000/;}
if ($VolSUSOut =~ /M/){$VolSUSOut=~ s/M/000000/;}
if ($VolSUSOut =~ /G/){$VolSUSOut=~ s/G/000000000/;}
if ($VolLdapOut eq ""){$VolLdapOut=0;}
if ($VolLdapOut =~ /K/){$VolLdapOut=~ s/K/000/;}
if ($VolLdapOut =~ /M/){$VolLdapOut=~ s/M/000000/;}
if ($VolLdapOut =~ /G/){$VolLdapOut=~ s/G/000000000/;}
if ($VolDNSOut eq ""){$VolDNSOut=0;}
if ($VolDNSOut =~ /K/){$VolDNSOut=~ s/K/000/;}
if ($VolDNSOut =~ /M/){$VolDNSOut=~ s/M/000000/;}
if ($VolDNSOut =~ /G/){$VolDNSOut=~ s/G/000000000/;}
if ($VolDNSIn eq ""){$VolDNSIn=0;}
if ($VolDNSIn =~ /K/){$VolDNSIn=~ s/K/000/;}
if ($VolDNSIn =~ /M/){$VolDNSIn=~ s/M/000000/;}
if ($VolDNSIn =~ /G/){$VolDNSIn=~ s/G/000000000/;}

$Resultat[0]=$VolIntIn;
$Resultat[1]=$VolM2In;
$Resultat[2]=$VolKAKIn;
$Resultat[3]=$VolSUSIn;
$Resultat[4]=$VolIntOut;
$Resultat[5]=$VolM2Out;
$Resultat[6]=$VolKAKOut;
$Resultat[7]=$VolSUSOut;
$Resultat[8]=$VolLdapOut;
$Resultat[9]=$VolLdapIn;
$Resultat[10]=$VolTotIn;
$Resultat[11]=$VolTotOut;
$Resultat[12]=$VolDNSOut;
$Resultat[13]=$VolDNSIn;
my @perf;
$perf[0]="Internet-In";
$perf[1]="Melanie2-In";
$perf[2]="MCAFEE-In";
$perf[3]="WSUS-In";
$perf[4]="Internet-Out";
$perf[5]="Melanie2-Out";
$perf[6]="MCAFEE-Out";
$perf[7]="WSUS-Out";
$perf[8]="LDAP-Out";
$perf[9]="LDAP-In";
$perf[10]="TOTAL-In";
$perf[11]="TOTAL-Out";
$perf[12]="DNS-Out";
$perf[13]="DNS-In";

if ($debug_val != 0) { print "Volume releve corrige Total Out/In: ".$VolTotOut."/".$VolTotIn." Internet Out/In :".$VolIntOut."/".$VolIntIn." Volume Melanie2 Out/In : ".$VolM2Out."/".$VolM2In." Volume MCAFEE Out/In : ".$VolKAKOut."/".$VolKAKIn." Volume SUS Out/In : ".$VolSUSOut."/".$VolSUSIn." Volume LDAP Out/In :".$VolLdapOut."/".$VolLdapIn."\n";}

my $flg_created = 0;

if (-e "/tmp/indic_iptable.log") {
    open(FILE,"<"."/tmp/indic_iptable.log");
    while($row = <FILE>){
       @last_values = split(":",$row);
       $last_time = $last_values[0];
       $last_VolIntOut = $last_values[1];
       $last_VolIntIn = $last_values[2];
       $last_VolM2Out = $last_values[3];
       $last_VolM2In = $last_values[4];
       $last_VolKAKOut = $last_values[5];
       $last_VolKAKIn = $last_values[6];
       $last_VolSUSOut = $last_values[7];
       $last_VolSUSIn = $last_values[8];
       $last_VolLdapOut = $last_values[9];
       $last_VolLdapIn = $last_values[10];
       $last_VolTotIn = $last_values[11];
       $last_VolTotOut = $last_values[12];
       $last_VolDNSOut = $last_values[13];
       $last_VolDNSIn = $last_values[14];
       $flg_created = 1;
    }
    close(FILE);
} else {
    $flg_created = 0;
}
my @last_Resultat;
$last_Resultat[0]=$last_VolIntIn;
$last_Resultat[1]=$last_VolM2In;
$last_Resultat[2]=$last_VolKAKIn;
$last_Resultat[3]=$last_VolSUSIn;
$last_Resultat[4]=$last_VolIntOut;
$last_Resultat[5]=$last_VolM2Out;
$last_Resultat[6]=$last_VolKAKOut;
$last_Resultat[7]=$last_VolSUSOut;
$last_Resultat[8]=$last_VolLdapOut;
$last_Resultat[9]=$last_VolLdapIn;
$last_Resultat[10]=$last_VolTotIn;
$last_Resultat[11]=$last_VolTotOut;
$last_Resultat[12]=$last_VolDNSOut;
$last_Resultat[13]=$last_VolDNSIn;


$update_time = time();
unless (open(FILE,">"."/tmp/indic_iptable.log")){
    print "Check mod for temporary file : /tmp/indic_iptable.log !\n";
   exit $ERRORS{"UNKNOWN"};
}
print FILE "$update_time:$VolIntOut:$VolIntIn:$VolM2Out:$VolM2In:$VolKAKOut:$VolKAKIn:$VolSUSOut:$VolSUSIn:$VolLdapOut:$VolLdapIn:$VolTotIn:$VolTotOut:$VolDNSOut:$VolDNSIn";
close(FILE);
if ($flg_created == 0){
   print "initialisation des compteurs (premiere utilisation ou remise a zero des compteurs ).... \n";
   exit($ERRORS{"UNKNOWN"});
}
my @Tindic;
my @Tindicb;
my @Unite;
my @liste=("VolIntIn","VolM2In","VolKAKIn","VolSUSIn","VolIntOut","VolM2Out","VolKAKOut","VolSUSOut","VolLdapOut","VolLdapIn","VolTotIn","VolTotOut","VolDNSOut","VolDNSIn");
my $i=0;
$diff = time() - $last_time;
if ($diff == 0){$diff = 1;}
my $message="TRAFIC ESSL RIE :";
my $perfparse="|";
for($i=0; $i<14; $i++) {
    $total=0;
    #print "Indicateur $liste[$i] : $Resultat[$i]\n";
   if (($Resultat[$i] - $last_Resultat[$i] != 0) && defined($last_Resultat[$i])) {
	$Tindic[$i] = 0;
	if ($Resultat[$i] - $last_Resultat[$i] < 0){
	   $total = $Resultat[$i];
	} else {
	   $total = $Resultat[$i] - $last_Resultat[$i];
	}
	#my $pct_out_traffic = $Tindic1 = abs($total / $diff);
	$Tindic[$i] = abs($total / $diff)*8;   #en bits/s
	$Tindic[$i] = int($Tindic[$i]);
   } else {
       $Tindic[$i] = 0;
   }
   $Unite[$i]="bits/s";
   $Tindicb[$i]=$Tindic[$i];
   if($Tindic[$i]>1000) {
	   $Unite[$i]="kbits/s";
   	   $Tindic[$i]=$Tindic[$i]/1000;
   }
    if ($debug_val != 0) {print $liste[$i].": releve: ".$Resultat[$i]."  last: ".$last_Resultat[$i]." resultat: ".$total." debit:".$Tindic[$i]."\n";}
   $message = $message." $perf[$i] : $Tindic[$i] $Unite[$i] ";
   $perfparse = $perfparse." ".$perf[$i]."=".$Tindicb[$i];
}


#print "Resultat:$stat0,$stat1,$stat2,$stat3,$stat4,$stat5,$stat6 \n";
print "$message$perfparse\n";
exit 0;
