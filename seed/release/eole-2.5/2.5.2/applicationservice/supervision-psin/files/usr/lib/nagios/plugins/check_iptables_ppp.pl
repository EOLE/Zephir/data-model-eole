#!/usr/bin/perl -w
#

# Plugin directory / home of utils.pm.
use lib "/usr/lib/nagios/plugins";
#use utils qw(%ERRORS &print_revision &support &usage);
use Getopt::Long qw(:config no_ignore_case bundling);
#use File::Basename;
#use Net::DNS;

use strict;

# Path to installed clamd binary.
my $iptable_cmd  = "sudo iptables";
my $debug_val = 0;  # Off unless -h arg
my $chaine = "mor-lan";
my $help_val=0;

sub show_help() {
    print <<END;

Usage: check_iptables_ppp [-v] [-h]
-h, --help
-v, --verbose
END
}

GetOptions (
    "h" => \$help_val, "help" => \$help_val,
    "v" => \$debug_val, "verbose" => \$debug_val
);

if ($help_val != 0) {
    &show_help;
    exit 0;
}
my $CodeSortie=2;
my $debug="";
if ($debug_val != 0) { $debug="-v";}
my $message4="";
if ($debug_val != 0) { print "\nTEST 1 : VERIFICATION LINK ETH1 ET CONFIGURATION IP\n";}
if ($debug_val != 0) { print "Commande: sudo ip link show eth1\n\n";}
chomp(my $Result4 = `sudo ip link show eth1`);
if ($debug_val != 0) { print $Result4."\n";}
if($Result4 =~ /NO-CARRIER/){
    $message4=" ETH1 NON CONNECTE";
    $CodeSortie=2;
}else{
    $message4=" ETH1 OK";
}
if ($debug_val != 0) { print "Commande: ifconfig eth1\n\n";}
chomp(my $Result41 = `ifconfig eth1`);
if ($debug_val != 0) { print $Result4."\n";}
if($Result41 =~ /10.9.0.1/){
	$message4=$message4." (IP OK)";
}else{
    	print "Check Tunnel PPP CRITIQUE : CONFIGURATION IP ETH1 NON VALIDE $Result41\n";
	exit 2;
}


#recup nombre de drop internet out et in
if ($debug_val != 0) { print "\nTEST 2 : REGLES NAT IPTABLES\n";}
if ($debug_val != 0) { print "Commande:".$iptable_cmd." -nvL -t nat\n\n";}
chomp(my $Result = `$iptable_cmd -nvL -t nat`);

if ($debug_val != 0) { print $Result."\n";}
if ($Result eq ""){
    print "Check Iptables Tunnel PPP CRITIQUE : Pas de reponse\n";
    exit 2;
}
my $message1=" Regles NAT non trouvees";
if ($Result =~ /POSTROUTING/ and $Result =~ /ACCEPT/ and $Result =~ /84.55/ and $Result=~ /89.91/ and $Result =~ /212.194/ ) {
    $CodeSortie=0;
    $message1=" Regles NAT en service";
}
#Verifications

#test reponse boitier distant
my $message2="";
my $TimeRep=0;
#chomp(my $Result2 = `sudo nmap -sP 10.9.0.2`);
#if($Result2 =~ /1 host up/){
#chomp(my $Result2 = `/usr/lib/nagios/plugins/check_udp -H 212.194.217.153 -p 500 -s ls -e ''`);
if ($debug_val != 0) { print "\nTEST 3 : VERIFICATION ACCES COOLECTEUR EXPRIMM PORT UDP 500\n";}
if ($debug_val != 0) { print "Commande: sudo nmap $debug -p 500 -sU -PN 212.194.217.153\n\n";}
chomp(my $Result2 = `sudo nmap $debug -p 500 -sU -PN 212.194.217.153`);
if ($debug_val != 0) { print $Result2."\n";}
if($Result2 =~ /500\/udp open/){
    $message2=" Acces Collecteur VPN OK";
     ($TimeRep) = $Result2 =~ /scanned in (.*) seconds/;
}else{
    $CodeSortie=2;
    $message2=" Acces Collecteur VPN CRITIQUE";
}


my $message3="";
if ($debug_val != 0) { print "\nTEST 4 : VERIFICATION CONNEXION TCP PORT 822 BOITIER ARKOON\n";}
if ($debug_val != 0) { print "Commande: sudo tcpcheck 5 10.9.0.2:822\n\n";}
chomp(my $Result3 = `sudo tcpcheck 5 10.9.0.2:822`);
if ($debug_val != 0) { print $Result3."\n";}
if($Result3 =~ /is alive/){
    $message3=" Connexion Arkoon port 822 OK";
}else{
    $CodeSortie=2;
    $message3=" Connexion Arkoon port 822 CRITIQUE";
}




if ($debug_val != 0) { 
	print "\nTEST 5 : IKE-SCAN\n";
	print "Commande: sudo ike-scan $debug 10.9.0.1\n\n";
	my $message5="";
	chomp(my $Result5 = `sudo ike-scan $debug 10.9.0.1`);
	print $Result5;
	print "\n";
	if($Result5 =~ /1 returned handshake/){
    		$message5=" Test IKE-SCAN OK";
	}else{
   		#$CodeSortie=0;
    		$message5=" Test IKE-SCAN CRITIQUE";
	}
	print $message5;
}

if ($debug_val != 0) { print "\n";}

if($CodeSortie == 0 ) {
    print "Check Tunnel PPP OK : $message4 $message1 $message2 ($TimeRep s) $message3\n";
}else{
    print "Check Tunnel PPP CRITIQUE : $message4 $message1 $message2 ($TimeRep s) $message3\n";
}
exit $CodeSortie;
