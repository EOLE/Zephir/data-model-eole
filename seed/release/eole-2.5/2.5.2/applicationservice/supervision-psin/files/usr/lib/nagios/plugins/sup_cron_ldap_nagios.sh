#!/bin/bash

echo "Suppression check ldap statut dans le cron nagios"
cat > /tmp/cron_zzz << EOF

EOF
crontab -l | grep -v '\(tdbbackup\|check_ldap_statut.pl\)'  >> /tmp/cron_zzz
sort /tmp/cron_zzz| uniq > /tmp/cron_u
crontab /tmp/cron_u
rm /tmp/cron_zzz /tmp/cron_u
exit 0
