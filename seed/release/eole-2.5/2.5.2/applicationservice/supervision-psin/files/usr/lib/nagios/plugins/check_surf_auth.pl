#! /usr/bin/perl -w
#param base de donn�es oracle
#require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
#use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
use lib "/usr/lib/nagios/plugins";
use utils qw($TIMEOUT %ERRORS );
use Getopt::Long;
use vars qw($opt_h $opt_v $opt_u $opt_x $opt_m $opt_c $opt_w $opt_r $opt_U $opt_P $opt_p );
$opt_h="";
my($debug, $check, $perf, $CodeHttp, $Tdns, $Ttcp, $Ttotal, $Taille, $Url,$Proxy);

my $ChaineAleat="";
my $message="";
@c=("A".."Z","a".."z",0..9);
$ChaineAleat= join("",@c[map{rand @c}(1..8)]);


sub print_help ();

#print "verif option\n";
GetOptions
    ("h"   => \$opt_h, "--help"         => \$opt_h,
     "u=s"   => \$opt_u, "--url=s" => \$opt_u,
     "m=s"   => \$opt_m, "--timeout=s" => \$opt_m,
     "c=s"   => \$opt_c, "--critique=s" => \$opt_c,
     "w=s"   => \$opt_w, "--warning=s" => \$opt_w,
     "x=s"   => \$opt_x, "--proxy=s" => \$opt_x,
     "v" => \$opt_v, "--verbose"       => \$opt_v,
     "N" => \$opt_n, "--authen"       => \$opt_n,
     "A=s" => \$opt_A, "--user=s"       => \$opt_A,
     "p=s" => \$opt_p, "--password=s"       => \$opt_p,
     "o=s" => \$opt_o, "--profil=s"       => \$opt_o,
     "e=s" => \$opt_e, "-chaine=s"       => \$opt_e,
     "r" => \$opt_r, "--no-redirect"       => \$opt_r);


#print "verif option\n";
$debug="";
if ($opt_v) {
    $debug=" -v --trace /usr/local/nagios/libexec/tracesurf.log --trace-time";
    $debug=" -v";
}
if (!$opt_A) {
	print_help();
	#print "Usage: check_surf.pl -A user -p password -N [-v]\n";
	exit 0;
}
if (!$opt_p) {
	print_help();
	#print "Usage: check_surf.pl -A user -p password -N [-v]\n";
	exit 0;
}

##################################################
#####      Verify Options
##
if (!$opt_u ) {
print "Option -u (--url) est obligatoire\n";
print_help();
exit $ERRORS{'OK'};
}
$Url=$opt_u;
$opt_u="$opt_u";

if(!$opt_x) {
  $opt_x="";
  $Proxy="Aucun";
}else{
  $Proxy=$opt_x;
  $opt_x="-x http://$opt_x";
}

if (!$opt_o) {
	$opt_o = "standard";  #profil defaut
}
if (!$opt_e) {
	if($opt_o eq "vip") {
		$opt_e = "YouTube";
	}else{
		$opt_e = " propos de Google";  #ichaine defaut
	}
}
if (!$opt_m) {
	$opt_m = 40;  #timeout defaut
}
if (!$opt_c) {
	$opt_c = 30;  #timeout defaut
}
if (!$opt_w) {
	$opt_w = 20;  #timeout defaut
}
my $uagent="-A Mozilla/5.0";
$perf="-w 'Perfdata:%{http_code}:%{time_namelookup}:%{time_connect}:%{time_total}:%{size_download}:%{url_effective}'";



#$check="curl --silent --no-sessionid -N -L -m $opt_m $debug $opt_x $opt_u $perf $uagent -o curl.log";
#$check="curl --retry 1 --proxy-ntlm --no-sessionid -N -m $opt_m -U $domaine\\$opt_A $debug $opt_x $opt_u $perf $uagent -o curl.log ";
#$check="curl --silent --retry 1  -H 'Pragma: no-cache' -N --proxy-ntlm -m $opt_m -U '$domaine\\$opt_A:$opt_p' $opt_x $opt_u $perf -v 1>>/dev/null 2>>/dev/null ";
#$check="curl --silent --retry 1  -H 'Pragma: no-cache' -N --proxy-ntlm -m $opt_m -U '$domaine\\$opt_A:$opt_p' $opt_x $opt_u $perf -v -o curl.log ";
my $Message = "SURF OK";
$check="curl --silent --retry 1  -H 'Pragma: no-cache' -N --proxy-digest -m $opt_m --proxy-user $opt_A:$opt_p $opt_x $opt_u $perf -v ";
$checkvip="curl --silent --retry 1  -H 'Pragma: no-cache' -N --proxy-digest -m $opt_m --proxy-user $opt_A:$opt_p $opt_x 'www.youtube.com' $perf -v ";
#$checksta="curl --silent --retry 1  -H 'Pragma: no-cache' -N --proxy-digest -m $opt_m --proxy-user $opt_A:$opt_p $opt_x 'www.google.com' $perf -v ";

if($opt_v) {
    print "Fichier log : /tmp/".$ChaineAleat.".log\n";
    print $check;
}
#exit;
$result=`$check 1>>/tmp/$ChaineAleat.log  2>&1`;
my $CodeRetour=$? >>8;
$result=`less /tmp/$ChaineAleat.log`;
#printf "child exited with value %d\n", $? >> 8;
#print $result;
if($opt_v) {print "RESULTAT:$result";}

#verification retour
my $CodeSortie=0;
if($result =~ "Cache Access Denied") {
		$Message="Surf Internet : Authentification CRITIQUE user ($opt_A) ou password invalide \n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
}
if($opt_o eq "standard") {
	if($result =~ $opt_e) {
		$Message="Authentification OK Acces Internet OK";
	}else{
		$Message="SURF CRITIQUE : Authentification OK mais acces Internet impossible ($opt_u) Chaine $opt_e non trouvee\n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}
	#verif non acces VIP
	$result2=`$checkvip 1>>/tmp/$ChaineAleat.2.log  2>&1`;
	$result2=`less /tmp/$ChaineAleat.2.log`;
	print $result2;
    if(!$result2 =~ "Page inaccessible") {
		$Message="Surf Internet : ATTENTION Acces VIP possible pour un profil standard user ($opt_A)\n ";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}
}elsif($opt_o eq "vip") {
	if($result =~ $opt_e) {
		$Message="Authentification OK Acces Internet OK";
	}elsif($result =~ "interdit") {
		$Message="SURF CRITIQUE : Acces Interdit pour un profil VIP (user $opt_A) URL: $opt_u\n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}else{
		$Message="SURF CRITIQUE : Authentification OK mais acces Internet impossible ($opt_u) Chaine $opt_e non trouvee\n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}

}elsif($opt_o eq "interdit") {
	if($result =~ "Profil interdit") {
		$Message=" Verification profil Interdit OK PAS ACCES (user $opt_A)";
	}else{
		$Message="SURF CRITIQUE : Verification profil Interdit Critique (user $opt_A) Acces autorise $opt_u\n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}

}elsif($opt_o eq "novip") {
	if($result =~ "Page inaccessible") {
		$Message=" Verification Acces profil standard sur un site VIP OK PAS ACCES (user $opt_A)";
	}else{
		$Message="SURF CRITIQUE : Acces a un site VIP avec le profil standard (user $opt_A) Acces autorise $opt_u\n";
		print $Message;
		$CodeSortie=2;
        $result=`rm -f /tmp/$ChaineAleat.log`;
        $result=`rm -f /tmp/$ChaineAleat.2.log`;
		exit 2;
	}
}else{
	print "SURF CRITIQUE : PROFIL $opt_o NON RECONNU . Verfication abandonnee Proxy:$Proxy Code Erreur:$CodeRetour\n";
    $result=`rm -f /tmp/$ChaineAleat.log`;
    $result=`rm -f /tmp/$ChaineAleat.2.log`;
	exit 2;

}
#exit;
my $MessageDNS = "DNS OK";
#if ($result =~ /Couldn't resolve host/) {
$result=`tail -n 1 /tmp/$ChaineAleat.log`;
my @Compteurs=split(":",$result);
$CodeHttp=$Compteurs[1];
$Tdns=$Compteurs[2];
$Tdns =~ s/,/./;
$Ttcp=$Compteurs[3];
$Ttcp =~ s/,/./;
$Ttotal=$Compteurs[4];
$Ttotal =~ s/,/./;
$Taille=$Compteurs[5];
#print "CodeHttp=$CodeHttp";
#if($CodeHttp eq "302" or $CodeHttp eq "301" and $opt_r ) {
if($CodeHttp eq "302" or $CodeHttp eq "301" ) {
	$CodeHttp="200";
}
#}
if ($CodeRetour eq "6") {
        $MessageDNS = "Erreur DNS";
	$CodeHttp="Erreur DNS";
}elsif($CodeRetour eq "7") {
	$CodeHttp = "Connexion au serveur Impossible";
}
if($CodeHttp eq "000") { $CodeHttp="Connexion TimeOut ou refusee";}
#for ($i=0;$i<=$#Compteurs;$i++)

#verif retour

$result=`rm -f /tmp/$ChaineAleat.log`;
$result=`rm -f /tmp/$ChaineAleat.2.log`;

# { print ("$Compteurs[$i]\n");
#}
if($CodeHttp ne "200") {
	print "SURF CRITIQUE Code HTTP retour : $CodeHttp Proxy:$Proxy Code Erreur:$CodeRetour Url:$Url\n";
	exit 2;
}

if($Ttotal >= $opt_c ) {
	print "SURF CRITIQUE $Message Temps total $Ttotal > $opt_c s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy Url:$Url|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 2;
}
if($Ttotal >= $opt_w ) {
	print "SURF DEGRADE $Message Temps total $Ttotal > $opt_w s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy Url:$Url|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 1;
}else{
	print "SURF OK $Message Temps total : $Ttotal s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy Url:$Url|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 0;

}
sub print_help () {
    print "\nUsage:\n";
    print "   -u (--url)   URL a tester\n";
    print "   -v (--verbose)       debug file /usr/local/nagios/libexec/tracesurf.log\n";
    print "   -m   Timeout en seconde (30 par defaut)\n";
    print "   -x (--proxy)      indiquer le proxy hote:port\n";
    print "   -c (--critique)      temps critique en secondes defaut 25\n";
    print "   -w (--warning)      temps warning en secondes defaut 15\n";
    print "   -A (--user)      user\n";
    print "   -p (--password)      password\n";
    print "   -o (--profil)      profil (standard,vip,interdit) defaut=standard\n";
    print "   -e (--chaine)      chaine recherchee dans la page\n";
}
exit 0;
