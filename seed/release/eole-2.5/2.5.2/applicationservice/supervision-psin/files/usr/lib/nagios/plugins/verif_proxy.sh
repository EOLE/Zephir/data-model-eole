#!/bin/bash

#test reseau local
#echo "V�rification proxy local"

plugin="/usr/lib/nagios/plugins"

function Sortie() 
{
echo $Err
#echo "code sortie : $Cerr / $array_status1"
exit $Cerr
}	

# reseau local
#$plugin/monitoring-cete.pl -c cete-config-intra -p1 -d1
status=($($plugin/monitor.pl -c $plugin/cete-config-intra -p1 -d1))
#echo $status
#if [[ "$status" =~ 'OK' ]]
if [[ "$status" == 'OK' ]]
then
	array_status1="OK"
else
	array_status1="NOT1"
fi
#test reseau distant
#$plugin/monitoring-cete.pl -c cete-config-intra -p2 -d2
status=($($plugin/monitor.pl -c $plugin/cete-config-intra -p2 -d2))
#echo $status
if [[ "$status" == 'OK' ]]
then
	array_status2="OK"
else
	array_status2="NOT2"
fi
#test proxy local (google)
#$plugin/monitoring-cete.pl -c cete-config-proxy -p5 -d5
status=($($plugin/monitor.pl -c $plugin/cete-config-proxy -p5 -d5))
#echo $status
if [[ "$status" == 'OK' ]]
then
	array_status3="OK"
else
	array_status3="NOT3"
fi
#verif process proxy
#$plugin/monitoring-cete.pl -c cete-config-intra -p4 -d4
status=($plugin/monitor.pl -c $plugin/cete-config-intra -p4 -d4)
echo $status
if [[ "$status" == 'OK' ]]
then
	array_status4="OK"
else
	array_status4="NOT4"
fi
#verif traffic proxy
#$plugin/monitoring-cete.pl -c cete-config-proxy -p99 -d99
status=($($plugin/monitor.pl -c $plugin/cete-config-proxy -p99 -d99))
#echo $status
if [[ "$status" == 'OK' ]]
then
	array_status5="OK"
else
	array_status5="NOT5"
fi
#echo "$array_status1"
if [[ "$array_status1" == 'NOT1' ]]
then
	Err="ERREUR R�seau Local"
	Cerr=2
	Sortie 0
fi
if [[ "$array_status2" == 'NOT2' ]]
then
	Err="Erreur R�seau Distant"
	Cerr=2
	Sortie 0
fi
if [[ "$array_status3" == 'NOT3' ]]
then
	Err="Erreur Proxy local (Google)"
	Cerr=2
	Sortie 0
fi
if [[ "$array_status4" == 'NOT4' ]]
then
	Err="Erreur Process Squid"
	Cerr=2
	Sortie 0
fi	
if [[ "$array_status5" == 'NOT5' ]]
then
	Err="Erreur Trafic Proxy"
	Cerr=2
	Sortie 0
else
	Err="Verification Proxy OK"
	Cerr=0
	Sortie 0
fi	
	

exit 3

