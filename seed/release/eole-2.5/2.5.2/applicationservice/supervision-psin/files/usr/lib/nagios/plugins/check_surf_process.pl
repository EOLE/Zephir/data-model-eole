#! /usr/bin/perl -w
#param base de donn�es oracle
#require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
#use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
use lib "/usr/lib/nagios/plugins";
use lib "/usr/local/nagios/libexec";
use utils qw($TIMEOUT %ERRORS );
use Getopt::Long;
use vars qw( $opt_v );

my $mess4="";
my $mess5="";
my $tps1=0;
my $tps2=0;
sub print_help ();

#print "verif option\n";
GetOptions
    ("h"   => \$opt_h, "--help"         => \$opt_h,
     "v" => \$opt_v, "--verbose"       => \$opt_v);


#print "verif option\n";
if ($opt_h) {
	print "Usage: check_surf.pl [-v verbose] \n";
	exit 0;
}

#Verification activation surf auth et pro
if($opt_v) {print "Verification activation\n";}
if($opt_v) {print "-----------------------\n";}
my $verif="non";
if( -e "/usr/bin/ParseDico") {
  $verif=`/usr/bin/ParseDico --list activer_squid_auth`;
}else{
  $verif=`CreoleGet activer_squid_auth non`;
}
if($verif =~ /"non"/) {
	print "Surf Authentifie NON ACTIVER \n";
	if($opt_v) {print " NON ACTIVER\n";}
	exit 1;
}
my $surfpro="non";
if( -e "/usr/bin/ParseDico") {
  $verif=`/usr/bin/ParseDico --list activer_proxy_pro`;
}else{
  $verif=`CreoleGet activer_proxy_pro non`;
}

if($verif =~ /oui/) {
	$surfpro="oui";
}
if($opt_v) {print " Activation SURF PRO : $surfpro\n";}



if($opt_v) {print "Verification process squid\n";}
if($opt_v) {print "--------------------------\n";}
my $CodeSortie="0";
my $Etat1="Process SQUID CRITIQUE";
my $Etat2="Process DANSGUADIAN CRITIQUE";
my $Etat3="Proicess Authentification Digest CRITIQUE";
my $Etat4="Fichier Password SQUID CRITIQUE > 30 minutes ou Taille < 10000";
my $Etat5="Fichier Filtrage CRITIQUE > 30 minutes ou taille < 1000000 ";
$verif=`/usr/lib/nagios/plugins/check_procs -C squid -u proxy -w 1:1 -c 1:1`;
if($opt_v) {print $verif;}
$tps1 = $verif =~ /: (.*) processus/;
if($verif =~ /PROCS OK/) {
	$Etat1="Process SQUID OK (".$tps1." process)";
}else{
	$CodeSortie="2";
}

if($opt_v) {print "\nVerification process dansguardian\n";}
if($opt_v) {print "-----------------------------------\n";}
$verif=`/usr/lib/nagios/plugins/check_procs -C dansguardian  -w 7:180 -c 7:200`;
if($opt_v) {print $verif;}
$tps1 = $verif =~ /: (.*) processus/;
if($verif =~ /PROCS OK/) {
	$Etat2=" Process dansguardian OK (".$tps1." process)";
}else{
        $CodeSortie="2";
}

if($opt_v) {print "\nVerification process digest_pw_auth\n";}
if($opt_v) {print "-------------------------------------\n";}
$verif=`/usr/lib/nagios/plugins/check_procs -C digest_pw_auth  -w 5:10 -c 5:12`;
if($opt_v) {print $verif;}
$tps1 = $verif =~ /: (.*) processus/;
if($verif =~ /PROCS OK/) {
	$Etat3=" Process Digest OK (".$tps1." process)";
}else{
        $CodeSortie="2";
}

if($opt_v) {print "\nVerification Fichier pwd-squid\n";}
if($opt_v) {print "--------------------------------\n";}
$verif=`/usr/lib/nagios/plugins/check_file_age  -f /etc/squid/pwd-squid -w 1800 -c 1900 -C 10000`;
if($opt_v) {print $verif;}
if($verif =~ /FILE_AGE OK/) {
	$Etat4=" Fichier Password Squid OK";
}else{
        $mess4=$verif =~ /CRITICAL: (.*)/;
        $CodeSortie="2";
}

if($opt_v) {print "\nVerification Fichier filtergroupslist\n";}
if($opt_v) {print "---------------------------------------\n";}
$verif=`/usr/lib/nagios/plugins/check_file_age -f /etc/dansguardian/dansguardian0/common/filtergroupslist -w 1800 -c 1900 -C 1000000`;
if($opt_v) {print $verif;}
if($verif =~ /FILE_AGE OK/) {
	$Etat5=" Fichier Filtrage OK";
    $mess5="";
}else{
        $mess5=$verif =~ /CRITICAL: (.*)/;
        $CodeSortie="2";
}
$verif=`grep -c "/etc/dansguardian/dansguardian0/common/filtergroupslist" /etc/dansguardian/dansguardian0/dansguardian.conf`;
if($verif =~ /1/){
        $Etat5=$Etat5." Conf OK";
}else{
        $Etat5=$Etat5." ATTENTION Conf MODIFIEE";
        $CodeSortie="2";
}
if($opt_v) {print "\nRESULTAT\n";}
if($opt_v) {print "---------\n";}

if($CodeSortie eq "0") {
	print "PROCESS SURF OK $Etat1 $Etat2 $Etat3 $Etat4 $Etat5 SurfPro:$surfpro\n";
    exit 0;
}else{
	print "PROCESS SURF CRITIQUE $Etat1 $Etat2 $Etat3 $Etat4 $Etat5 $mess4 $mess5 SurfPro:$surfpro\n";
    exit 2;
}
exit 0;
