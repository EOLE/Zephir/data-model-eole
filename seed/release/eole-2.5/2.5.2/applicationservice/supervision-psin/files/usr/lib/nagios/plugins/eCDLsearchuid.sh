#! /bin/sh

if [ $# -ne 1 ]; then
	echo "usage : $0 uid"
	exit 1
fi
SEARCHUID=$1

. /usr/bin/ParseDico

BASE="ou=$ecdl_smb_workgroup,ou=domaines,ou=Samba,ou=applications,ou=ressources,dc=equipement,dc=gouv,dc=fr"
CONNECT_DN="uid=SambaAdm.$ecdl_smb_workgroup,$BASE"

ldapsearch -x -H ldaps://ldapsmb.ac.melanie2.i2 -D "$CONNECT_DN" -w "$(cat /etc/sysconfig/.pass)" -b "$BASE" -s sub -a always "(uid=$SEARCHUID)" dn

