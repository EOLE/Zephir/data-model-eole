#!/usr/bin/perl
#
# PSIN P.Malossane 
use Time::HiRes "time","sleep";
use POSIX qw(strftime);

my $datetest = strftime "%d%m%Y %H:%M:%S", localtime;
print "----------------------------------------------"."\n";
print "Diagnostic Materiel du $datetest\n";
print "----------------------------------------------\n";
my $result=`omreport chassis info`;
print "$result\n";
$result=`omreport system operatingsystem`;
print "$result\n";
$result=`omreport chassis hwperformance`;
print "$result\n";
print "----------------------------------------------\n";
print " Informations Chassis \n";
print "----------------------------------------------\n";
$result=`omreport chassis bios`;
print "$result\n";
$result=`omreport chassis nics`;
print "$result\n";
$result=`omreport chassis processors`;   
print "$result\n";  
$result=`omreport chassis memory`;
print "$result\n";
$result=`omreport chassis fans`;
print "$result\n";
$result=`omreport chassis pwrsupplies`;
print "$result\n";
$result=`omreport chassis temps`;
print "$result\n";
$result=`omreport chassis volts`;
print "$result\n";
$result=`omreport chassis batteries`;
print "$result\n";

print "----------------------------------------------\n";
print "Information storage\n";
print "----------------------------------------------\n";

$result=`omreport storage controller`;
print "$result\n";
$result=`omreport storage vdisk`;
print "$result\n";
$result=`omreport storage pdisk controller=0`;
print "$result\n";
$result=`omreport storage pdisk controller=1`;
print "$result\n";

$result=`omreport storage battery`;
print "$result\n";

print "----------------------------------------------\n";
print "Log Alertes ESM\n";
print "----------------------------------------------\n";

$result=`omreport system alertlog`;
print "$result\n";
$result=`omreport system esmlog`;
print "$result\n";
print "----------------------------------------------\n";
print "Fin du diagnostic\n";
print "----------------------------------------------\n";
