#!/bin/bash

. /usr/lib/eole/ihm.sh

if [ "$(CreoleGet module_type)" = "amon" ];then

EchoRouge "***************************************************************"
EchoRouge "***             Gestion du traffic par ports                ***"
EchoRouge "***************************************************************"
 if [ "$(CreoleGet type_amon)" = "2zones-essl" ];then
EchoOrange "********** Flux Proxy/DNS/Anti-virus **************************"
EchoOrange "** Output **"
iptables -vnL OUTPUT | grep ports_essl_OUT
iptables -vnL ports_essl_OUT | grep ACC-ports_out
EchoOrange "** Input **"
iptables -vnL INPUT | grep ports_essl_IN
iptables -vnL ports_essl_IN | grep ACC-ports_in
echo -e "\n"

  if [ "$(CreoleGet variante_type)" = "eSSL Internet" ];then
EchoOrange "********** Flux Proxy/DNS/Anti-virus Internet *********************************"
EchoOrange "** Output **"
iptables -vnL ports_essl_OUT | grep ACC-ports_INTERNET_out
EchoOrange "** Input **"
iptables -vnL ports_essl_IN | grep ACC-ports_INTERNET_in
echo -e "\n"
  fi
 else
EchoOrange "********** Flux Proxy/DNS/Anti-virus **************************"
EchoOrange "** Output **"
iptables -vnL OUTPUT | grep ports_essl_OUT
iptables -vnL ports_essl_OUT | grep ACC-ports_out
echo -e "\n"
iptables -vnL OUTPUT | grep ports_essl_lan_OUT
iptables -vnL ports_essl_lan_OUT | grep ACC-ports_out_LAN
EchoOrange "** Input **"
iptables -vnL INPUT | grep ports_essl_IN
iptables -vnL ports_essl_IN | grep ACC-ports_in
echo -e "\n"
iptables -vnL INPUT | grep ports_essl_lan_IN
iptables -vnL ports_essl_lan_IN | grep ACC-ports_in_LAN
echo -e "\n"

EchoOrange "********** Flux Pare-feu ***************************************"
EchoOrange "** Output **"
iptables -vnL FORWARD | grep ports_forward_OUT
iptables -vnL ports_forward_OUT | grep ACC-ports_PF_out
EchoOrange "** Input **"
iptables -vnL FORWARD | grep ports_forward_IN
iptables -vnL ports_forward_IN | grep ACC-ports_PF_in
echo -e "\n"

  if [ "$(CreoleGet variante_type)" = "eSSL Internet" ];then
EchoOrange "********** Flux Flux Proxy/DNS/Anti-virus Internet *************"
EchoOrange "** Output **"
iptables -vnL ports_essl_OUT | grep ACC-ports_INTERNET_out
EchoOrange "** Input **"
iptables -vnL ports_essl_IN | grep ACC-ports_INTERNET_in
echo -e "\n"
EchoOrange "********** Flux Pare-feu Internet ******************************"
EchoOrange "** Output **"
iptables -vnL ports_forward_OUT | grep ACC-ports_PF_INTERNET_out
EchoOrange "** Input **"
iptables -vnL ports_forward_IN | grep ACC-ports_PF_INTERNET_in
echo -e "\n"
  fi
 fi

echo -e "\n\n"
EchoRouge "****************************************************************"
EchoRouge "***       Gestion du traffic par serveurs nationaux          ***"
EchoRouge "****************************************************************"
EchoOrange "********** Flux Proxy/DNS/Anti-virus *************"
EchoOrange "** Output **"
iptables -vnL OUTPUT | grep flux_essl_OUT
iptables -vnL flux_essl_OUT | grep ACC_out
EchoOrange "** Input **"
iptables -vnL INPUT | grep flux_essl_IN
iptables -vnL flux_essl_IN | grep ACC_in
echo -e "\n"

 if [ "$(CreoleGet type_amon)" != "2zones-essl" ];then
EchoOrange "********** Flux Pare-feu ***************************************"
EchoOrange "** Output **"
iptables -vnL FORWARD | grep flux_forward_OUT
iptables -vnL flux_forward_OUT | grep ACC-flux_out
EchoOrange "** Input **"
iptables -vnL FORWARD | grep flux_forward_IN
iptables -vnL flux_forward_IN | grep ACC-flux_in
 fi

else

EchoRouge "***************************************************************"
EchoRouge "***             Gestion du traffic par ports                ***"
EchoRouge "***************************************************************"
EchoOrange "** Output **"
iptables -vnL OUTPUT | grep ports_OUT
iptables -vnL ports_OUT | grep ACC-ports_out
EchoOrange "** Input **"
iptables -vnL INPUT | grep ports_IN
iptables -vnL ports_IN | grep ACC-ports_in
echo -e "\n"

EchoRouge "****************************************************************"
EchoRouge "***       Gestion du traffic par serveurs nationaux          ***"
EchoRouge "****************************************************************"
EchoOrange "** Output **"
iptables -vnL OUTPUT | grep flux_OUT
iptables -vnL flux_OUT | grep ACC-flux_out
EchoOrange "** Input **"
iptables -vnL INPUT | grep flux_IN
iptables -vnL flux_IN | grep ACC-flux_in
echo -e "\n"

fi
