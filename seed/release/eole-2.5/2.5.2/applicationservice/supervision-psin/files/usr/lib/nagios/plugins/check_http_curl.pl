#! /usr/bin/perl -w
#param base de donn�es oracle
#require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
#use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
use lib "/usr/lib/nagios/plugins";
use utils qw($TIMEOUT %ERRORS );
use Getopt::Long;
use vars qw($opt_h $opt_v $opt_u $opt_x $opt_m $opt_c $opt_w );
$opt_h="";
my($debug, $check, $perf, $CodeHttp, $Tdns, $Ttcp, $Ttotal, $Taille, $Url,$Proxy);

my $ChaineAleat="";
@c=("A".."Z","a".."z",0..9);
$ChaineAleat= join("",@c[map{rand @c}(1..8)]);


sub print_help ();

#print "verif option\n";
GetOptions
    ("h"   => \$opt_h, "--help"         => \$opt_h,
     "u=s"   => \$opt_u, "--url=s" => \$opt_u,
     "m=s"   => \$opt_m, "--timeout=s" => \$opt_m,
     "c=s"   => \$opt_c, "--critique=s" => \$opt_c,
     "w=s"   => \$opt_w, "--warning=s" => \$opt_w,
     "x=s"   => \$opt_x, "--proxy=s" => \$opt_x,
     "v" => \$opt_v, "--verbose"       => \$opt_v,
     "r" => \$opt_r, "--no-redirect"       => \$opt_r);


#print "verif option\n";
$debug="";
if ($opt_v) {
    $debug=" -v --trace /usr/lib/nagios/plugins/tracecurel.log --trace-time";
    $debug=" -v";
}

$opt_u =~ s/ChaineAleat/$ChaineAleat/;

#if ($opt_h) {
#    print_help();
#    exit $ERRORS{'OK'};
#	Getopt::Long::Configure('bundling');
#}

##################################################
#####      Verify Options
##
if (!$opt_u ) {
print "Option -u (--url) est obligatoire\n";
print_help();
exit $ERRORS{'OK'};
}
$Url=$opt_u;
$opt_u="--url $opt_u";

if(!$opt_x) {
  $opt_x="";
  $Proxy="Aucun";
}else{
  $Proxy=$opt_x;
  $opt_x="--proxy $opt_x:8080";
}

if (!$opt_m) {
	$opt_m = 40;  #timeout defaut
}

if (!$opt_c) {
	$opt_c = 30;  #timeout defaut
}
if (!$opt_w) {
	$opt_w = 15;  #timeout defaut
}
$uagent="-A Mozilla/5.0";
$perf="-w 'Perfdata:%{http_code}:%{time_namelookup}:%{time_connect}:%{time_total}:%{size_download}:%{url_effective}'"; 

#$check="curl --silent --no-sessionid -N -L -m $opt_m $debug $opt_x $opt_u $perf $uagent -o curl.log"; 
$check="curl --retry 1 --silent --no-sessionid -N -m $opt_m $debug $opt_x $opt_u $perf $uagent -o curl.log "; 

if($opt_v) {print $check;}
my $result=`$check`;
my $CodeRetour=$? >>8;
#print $CodeRetour;
#printf "child exited with value %d\n", $? >> 8;
#print $result;
#exit;
my $MessageDNS = "DNS OK";
#if ($result =~ /Couldn't resolve host/) {
my @Compteurs=split(":",$result);
$CodeHttp=$Compteurs[1];
$Tdns=$Compteurs[2];
$Tdns =~ s/,/./;
$Ttcp=$Compteurs[3];
$Ttcp =~ s/,/./;
$Ttotal=$Compteurs[4];
$Ttotal =~ s/,/./;
$Taille=$Compteurs[5];
#print "CodeHttp=$CodeHttp";
if($CodeHttp eq "302" or $CodeHttp eq "301" and $opt_r ) {
	$CodeHttp="200";
}
#si code redirect 302 on suit la redirection
if($CodeHttp eq "302" or $CodeHttp eq "301" ) {
	$opt_u="--url '".$Compteurs[8].":".$Compteurs[9]."'";
	$Url=$Compteurs[8].":".$Compteurs[9];
	$check="curl --silent --no-sessionid -N -m $opt_m $debug $opt_x $opt_u $perf $uagent -o curl.log "; 
	if($opt_v) {print $check;}
	$result=`$check`;
	@Compteurs=split(":",$result);
	$CodeHttp=$Compteurs[1];
	$Tdns=$Compteurs[2];
	$Tdns =~ s/,/./;
	$Ttcp=$Compteurs[3];
	$Ttcp =~ s/,/./;
	$Ttotal=$Compteurs[4];
	$Ttotal =~ s/,/./;
	$Taille=$Compteurs[5];
}
if ($CodeRetour eq "6") {
        $MessageDNS = "Erreur DNS";
	$CodeHttp="Erreur DNS";
}elsif($CodeRetour eq "7") {
	$CodeHttp = "Connexion au serveur Impossible";
}
if($CodeHttp eq "000") { $CodeHttp="Connexion TimeOut ou refusee";}
#for ($i=0;$i<=$#Compteurs;$i++)
# { print ("$Compteurs[$i]\n");
#}
if ($opt_v) { print $result."\n";}
if($CodeHttp ne "200") {
	print "Check URL $Url CRITIQUE $MessageDNS Code HTTP retour : $CodeHttp Proxy:$Proxy Code Erreur:$CodeRetour|tdns=0,ttcp=0,ttotal=0\n";
	exit 2;
}

if($Ttotal >= $opt_c ) {
	print "Check URL $Url CRITIQUE $MessageDNS Temps total $Ttotal > $opt_c s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 2;
}
if($Ttotal >= $opt_w ) {
	print "Check URL $Url DEGRADE $MessageDNS Temps total $Ttotal > $opt_w s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 1;
}else{
	print "Check URL $Url OK $MessageDNS Temps total : $Ttotal s (dns=$Tdns , tcp=$Ttcp , Taille:$Taille) Proxy:$Proxy|tdns=$Tdns,ttcp=$Ttcp,ttotal=$Ttotal\n";
	exit 0;

}
sub print_help () {
    print "\nUsage:\n";
    print "   -u (--url)   URL a tester\n";
    print "   -v (--verbose)       debug file /usr/lib/nagios/plugins/tracecurl.log\n";
    print "   -m   Timeout en seconde (30 par defaut)\n";
    print "   -x (--proxy)      indiquer le proxy hote:port\n";
    print "   -c (--critique)      temps critique en secondes defaut 25\n";
    print "   -w (--warning)      temps warning en secondes defaut 15\n";
}
exit 0
