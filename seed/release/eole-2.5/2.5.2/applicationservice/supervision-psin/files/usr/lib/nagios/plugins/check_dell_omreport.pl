#!/usr/bin/perl -T
#############################################################################
#                                                                           #
# This script was initially developed by Anstat Pty Ltd for internal use    #
# and has kindly been made available to the Open Source community for       #
# redistribution and further development under the terms of the             #
# GNU General Public License: http://www.gnu.org/licenses/gpl.html          #
#                                                                           #
#############################################################################
#                                                                           #
# This script is supplied 'as-is', in the hope that it will be useful, but  #
# neither Anstat Pty Ltd nor the authors make any warranties or guarantees  #
# as to its correct operation, including its intended function.             #
#                                                                           #
# Or in other words:                                                        #
#       Test it yourself, and make sure it works for YOU.                   #
#                                                                           #
#############################################################################
# Author: George Hansper               e-mail:  Name.Surname@anstat.com.au  #
#############################################################################

$ENV{PATH}="/sbin:/usr/sbin:/bin:/usr/bin";

use strict vars;
use vars qw( $omreport %opt);

%opt = (
         'omreport'          => undef,    # omreport path
       );



sub find_omreport {
    # If user has specified path to omreport
    if (defined $opt{omreport} and -x $opt{omreport}) {
        $omreport = qq{"$opt{omreport}"};
        return;
    }

    # Possible full paths for omreport
    my @omreport_paths
      = (
         '/opt/dell/srvadmin/bin/omreport',              # default on Linux with OMSA >= 6.2.0
         '/usr/bin/omreport',                            # default on Linux with OMSA < 6.2.0
         '/opt/dell/srvadmin/oma/bin/omreport.sh',       # alternate on Linux
         '/opt/dell/srvadmin/oma/bin/omreport',          # alternate on Linux
         'C:\Program Files (x86)\Dell\SysMgt\oma\bin\omreport.exe', # default on Windows x64
         'C:\Program Files\Dell\SysMgt\oma\bin\omreport.exe',       # default on Windows x32
         'c:\progra~1\dell\sysmgt\oma\bin\omreport.exe', # 8bit legacy default on Windows x32
         'c:\progra~2\dell\sysmgt\oma\bin\omreport.exe', # 8bit legacy default on Windows x64
        );

    # Find the one to use
  OMREPORT_PATH:
    foreach my $bin (@omreport_paths) {
        if (-x $bin) {
            $omreport = qq{"$bin"};
            last OMREPORT_PATH;
        }
    }

    # Exit with status=UNKNOWN if OM is not installed, or we don't
    # have permission to execute the binary
    if (!defined $omreport) {
        print "ERROR: Dell OpenManage Server Administrator (OMSA) is not installed\n";
        exit 2;
    }
    return;
}

$omreport = undef;
find_omreport();

my $rcsid = '$Id: check_dell_omreport.pl,v 1.3 2007/07/13 02:01:43 georgeh Exp georgeh $';
my $rcslog = '
  $Log: check_dell_omreport.pl,v $
  Revision 1.3  2007/07/13 02:01:43  georgeh
  Added "charging" to battery states which result in Warning instead of critical

  Revision 1.2  2007/01/11 02:58:05  georgeh
  Added "rebuild" to disk states which result in Warning instead of critical

  Revision 1.1  2006/10/08 22:44:26  georgeh
  Initial revision

  Revision 1.1  2005/12/19 04:56:37  georgeh
  Initial revision

';

# Taint checks may fail due to the following...
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

if ( @ARGV > 0 ) {
	if( $ARGV[0] eq "-V" ) {
		print STDERR "$rcsid\n";
	} else {
		print STDERR "Nagios plugin for checking the status of a Dell RAID array using 'omreport'\n";
		print STDERR "Requires Dell Open Manage v4 to be installed.\n";
		print STDERR "\n";
		print STDERR "Usage:\n";
		print STDERR "\t$0 [-V]\n";
		print STDERR "\t\t-V ... print version information\n";
		print STDERR "\n";
	}
}

my $cmd;

my $ctrlr_flds;
my $controller;
my $ctrlr_ndx;
my $cn;
my $vdisk_flds;
my $vdisk;
my $vdisk_ndx;
my $vn;
my $adisk_flds;
my $adisk;
my $adisk_ndx;
my $an;
my $batt_flds;
my $battery;
my $batt_ndx;
my $bn;
my $state;
my $progress;
my $crit=0;
my $warn=0;
my $ok=0;
my $message="";

sub runcmd( $ ) {
	my (@tag,@results,@values);
	my %fields;
	my $i;
	$ENV{BASH_ENV}=undef;
	open(CMD,"$cmd|")
		|| do { print "omreport: " . $! ."\n"; exit 2};
	while ( <CMD> ) {
		chomp;
		if( $_ =~ /^ID;/i ) {
			@tag=(split /;/, "$_" );
			for ($i=0 ; $i <  @tag ; $i++  ) {
				$fields{$tag[$i]} =$i;
			}
			# print $_ . "\n";
		} elsif ( @tag != 0 && $_ =~ /;/ ) {
			@values = (split /;/, "$_" );
			push @results, [ @values ];
			# print $_ . "\n";
		}
		# print "\t$_\n" ;
	}
	close(CMD);
	return (\%fields,\@results);
}

$cmd= "$omreport storage controller -fmt ssv";
($ctrlr_flds,$controller) = &runcmd($cmd);
# print STDERR "Controller: state=$ctrlr_flds->{Status}  $controller->[0][$ctrlr_flds->{ID}]   Status:  $controller->[0][$ctrlr_flds->{State}]\n";
# print ( join " , ", %ctrlr_flds ) , "\n";
if ( @{$controller} == 0 ) {
	#$warn++;
	$message .= " Pas de Controlleur RAID";
}
for ( $ctrlr_ndx=0; $ctrlr_ndx < @{$controller} ; $ctrlr_ndx++ ) {
#print "Traitement controleur $ctrlr_ndx";
# foreach $ctrlr ( @{$controller} ) {
	# print ( join " X ", @{$ctrlr} );
	# print "\n";
	$_ = $controller->[$ctrlr_ndx][ $ctrlr_flds->{ID} ];
	/([0-9]+)/m;
	$cn = $1;
	$state = "$controller->[$ctrlr_ndx][ $ctrlr_flds->{Status} ]/$controller->[$ctrlr_ndx][ $ctrlr_flds->{State} ]";
#print " statut:".$state;
	
	$message .= " Controller$cn=$state [";
	if( $state =~ /^ok\/ready/i or $state =~ /Not Appli/i ) {
		$ok++;
	} elsif ( $state =~ /degrad/i or $state =~ /Not Appli/i ) {
		$ok++;
	} else {
		$ok++;
	}
	$cmd= "$omreport storage battery controller=$cn -fmt ssv";
	($batt_flds,$battery) = &runcmd($cmd);
	for ( $batt_ndx=0; $batt_ndx < @{$battery} ; $batt_ndx++ ) {
		# print "bbb $battery->[$batt_ndx][0]\n";
		$_ = $battery->[$batt_ndx][ $batt_flds->{ID} ];
		if ( /No Batteries found/mi ) {
			last;
		}
		/([0-9:]+)/m;
		$bn = $1;
		$state = "$battery->[$batt_ndx][ $batt_flds->{Status} ]/$battery->[$batt_ndx][ $batt_flds->{State} ]";
		$message.=" Battery$cn=$state";
		if( $state =~ /^ok\/ready/i ) {
			$ok++;
		} elsif ( $state =~ /degrad|charging/i ) {
			$warn++;
		} else {
			$crit++;
			$message .= "**";
		}
		# print STDERR "battery=$bn Status=$battery->[$batt_ndx][$batt_flds->{Status}] State=$battery->[$batt_ndx][$batt_flds->{State}]\n";
	}
	# print STDERR "controller=$cn State=$controller->[$ctrlr_ndx][ $ctrlr_flds->{State} ]\n";
	$cmd= "$omreport storage vdisk controller=$cn -fmt ssv";
	($vdisk_flds,$vdisk) = &runcmd($cmd);
	for ( $vdisk_ndx=0; $vdisk_ndx < @{$vdisk} ; $vdisk_ndx++ ) {
		$_ = $vdisk->[$vdisk_ndx][ $vdisk_flds->{ID} ];
		/([0-9]+)/m;
		$vn = $1;
		# print ( join " , ", %{$vdisk_flds} );
		# print "\n";
		$state = "$vdisk->[$vdisk_ndx][$vdisk_flds->{Status}]/$vdisk->[$vdisk_ndx][$vdisk_flds->{State}]";
		$progress = " $vdisk->[$vdisk_ndx][$vdisk_flds->{Progress}]";
		if ( $progress =~ /not appl/i ) {
			$progress = "";
		}
		$message .= " Vdisk$vn=$state$progress [";
		if( $state =~ /^ok\/ready/i ) {
			$ok++;
		} elsif ($state =~ /(degrad|regen)/i ) {
			$warn++;
		} else {
			$crit++;
		}
		# print STDERR "vdisk=$vn State=$vdisk->[$vdisk_ndx][$vdisk_flds->{State}]\n";

		$cmd= "$omreport storage adisk vdisk=$vn controller=$cn -fmt ssv";
		($adisk_flds,$adisk) = &runcmd($cmd);
		for ( $adisk_ndx=0; $adisk_ndx < @{$adisk} ; $adisk_ndx++ ) {
			# print ( join " , ", %{$adisk_flds} );
			# print "\n";
			# print ( join " , ", @{$adisk->[$adisk_ndx]} );
			# print "\n";
			$_ = $adisk->[$adisk_ndx][ $adisk_flds->{ID} ];
			/([0-9:]+)/m;
			$an = $1;
			$state = "$adisk->[$adisk_ndx][$adisk_flds->{Status}]/$adisk->[$adisk_ndx][$adisk_flds->{State}]";
			$progress = " $adisk->[$adisk_ndx][$adisk_flds->{Progress}]";
			if ( $progress =~ /not appl/i ) {
				$progress = "";
			}
			$message .= " $an=$state$progress";
			if( $state =~ /^ok\/(ready|online)/i ) {
				$ok++;
			} elsif ($state =~ /(degrad|regen|rebuild)/i ) {
				$warn++;
			} else {
				$crit++;
				$message .= "**";
			}
			# print STDERR "adisk=$an Status=$adisk->[$adisk_ndx][$adisk_flds->{Status}] State=$adisk->[$adisk_ndx][$adisk_flds->{State}]\n";
		}
		$message .= " ]";
	}
	$message .= " ] ";
}

if ( $crit != 0 ) {
	print "CRITICAL:$message\n";
	exit(2);
} elsif ( $warn != 0 ) {
	print "WARNING:$message\n";
	exit(1);
} else {
	print "OK:$message\n";
	exit(0);
}

