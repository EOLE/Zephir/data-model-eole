###################################################################
# Oreon is developped with GPL Licence 2.0 
#
# GPL License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
#
# Developped by : Julien Mathis - Romain Le Merlus 
#                 Mathavarajan Sugumaran
#
###################################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
#    For information : contact@merethis.com
####################################################################
#
# Plugin init
#
package	centreon;

use Exporter   ();
use FindBin qw($Bin);
use lib "$FindBin::Bin";
use lib "/usr/lib/nagios/plugins";

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
use utils qw($TIMEOUT %ERRORS &print_revision &support);

if (eval "require Config::IniFiles" ) {
	use Config::IniFiles;
} else {
	print "Unable to load Config::IniFiles\n";
    exit $ERRORS{'UNKNOWN'};
}

#pas sur essl
### RRDTOOL Module
#use lib qw(/usr/lib/perl5 ../lib/perl);
#if (eval "require RRDs" ) {
#	use RRDs;
#} else {
#	print "Unable to load RRDs perl module\n";
#    exit $ERRORS{'UNKNOWN'};
#}

# On d�fini une version pour les v�rifications
#$VERSION = do { my @r = (q$Revision: XXX $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(get_parameters);
our @EXPORT = @EXPORT_OK;

my $params_file = "centreon.conf";
my @ds = ("a","b","c","d","e","f","g","h","i","j","k","l");

###############################################################################
#  Get all parameters from the ini file
###############################################################################
sub get_parameters	{
	$params_file = "/usr/lib/nagios/plugins/$params_file";
	unless (-e $params_file)	{
		print "Unknown - In centreon.pm :: $params_file :: $!\n";
        exit $ERRORS{'UNKNOWN'};
    }
    my %centreon;
    tie %centreon, 'Config::IniFiles', ( -file => $params_file );
    return %centreon;
}

1;

__END__

=head1 NAME

centreon - shared module for Oreon plugins

=head1 SYNOPSIS

  use centreon;
  centreon::get_parameters()

=head1 DESCRIPTION

=head2 Functions


=head1 AUTHOR

Mathieu Chateau E<lt>mathieu.chateau@lsafrance.comE<gt>
Christophe Coraboeuf E<lt>ccoraboeuf@oreon-project.orgE<gt>

=cut




