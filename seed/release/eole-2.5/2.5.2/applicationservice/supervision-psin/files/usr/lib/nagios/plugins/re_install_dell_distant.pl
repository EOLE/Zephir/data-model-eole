#!/usr/bin/perl
use Getopt::Long;
use Sys::Hostname;
use POSIX qw(strftime);
use Term::ANSIColor qw(:constants);

our $versiondell;
our $VerifT300;
my $veole="";
my $VersEole="2.3";
if( -e "/etc/eole/release") {
        $veole=`cat /etc/eole/release`;
        if($veole =~ /2.4/ ) { $VersEole="2.4";}
        if($veole =~ /2.5/ ) { $VersEole="2.5";}
        if($veole =~ /2.6/ ) { $VersEole="2.6";}
}

my $VerForce=$ARGV[0];
if($VerForce eq "") { $VerForce="0";}

my $type=`sudo uname -a`;
my $arch="32B";
if($type =~ /x86_64/) { $arch="64B";}
my $action="";
my $Version="";
   $cmd="sudo sed -i -e 's/NON/OUI/' /usr/lib/nagios/plugins/NoCheckDell.pl";
   system $cmd;

my $status=`sudo lshw -C system | grep produ`;
chomp($status);
print "$status\n";
if($status =~ /T300/ and $VersEole eq "2.3") {
   	$cmd="sudo sed -i -e 's/OUI/NON/' /usr/lib/nagios/plugins/NoCheckDell.pl";
    	if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
		$cmd=`echo '\$psin="T300 REBOOT VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
   	}else{
		$cmd=`cp /usr/lib/nagios/plugins/NoCheckDell.pl.exemple /usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$psin="T300 REBOOT VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
  	 }
    print BOLD, YELLOW, "Pas installation en 2.3 sur les T300\n",RESET;
	exit;
}

if($status =~ /T300/) {
		print BOLD, YELLOW, "--> T300 : Avez vous modifier le rc.local pour eviter le reboot O/N: ",RESET;
        	chomp($VerifT300= <STDIN>);
		if($VerifT300 eq "O") {
			print "OK";
		}else{
			print "Sortie de l'installation - Modier le rc.local avant de relancer l'installation\n";
			exit 0;
		} 
}

if($VersEole =~ /2.3/) {
	if($VerForce eq "0") {
		print BOLD, YELLOW, "--> Indiquer la version openmanage a installer 65 ou 71 : ",RESET;
        	chomp($versiondell= <STDIN>);
	}else{
		$versiondell=$VerForce;
	}
	if($versiondell eq "") { $versiondell="65";}
	$action=`sudo rm -f /etc/apt/sources.list.d/dell.list`;
	$action=`sudo touch /etc/apt/sources.list.d/dell.list`;
	$action=`sudo chmod 777 /etc/apt/sources.list.d/dell.list`;
	if($versiondelli eq "65") {
		#$action=`sudo cp /tmp/dell.list.65 /etc/apt/sources.list.d/dell.list`;
		$action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/omsa_6.5 / >/etc/apt/sources.list.d/dell.list`;
		$Version="6.5";
	}elsif($versiondell eq "71") {
		$action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/ubuntu lucid openmanage/710 >/etc/apt/sources.list.d/dell.list`;
		#$action=`sudo cp /tmp/dell.list.71 /etc/apt/sources.list.d/dell.list`;
		$Version="7.1";
	}else{
		print "Version openmanage a installer incorrecte\n";
		exit 0;
	}
	$action=`wget http://debmiroir-02.ac.centre-serveur.i2/gpg/cle_omsa_6.5.gpg`;
	$action=`sudo apt-key add cle_omsa_6.5.gpg`;
	$action=`sudo apt-key add /usr/lib/nagios/plugins/cle_dell.txt`;
}
if($VersEole =~ /2.4/ or $VersEole =~ /2.5/) {
	$action=`sudo rm -f /etc/apt/sources.list.d/dell.list`;
	$action=`sudo touch /etc/apt/sources.list.d/dell.list`;
	$action=`sudo chmod 777 /etc/apt/sources.list.d/dell.list`;
	if($arch eq "64B") {
		$Version="7.4";
		#$action=`sudo cp /tmp/dell.list.74 /etc/apt/sources.list.d/dell.list`;
        $actionbis=`sudo grep trusty /etc/apt/sources.list`;
        if($actionbis =~ /trusty/) {
		    $action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/ubuntu trusty openmanage/740 >/etc/apt/sources.list.d/dell.list`;
        }else{
		    $action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/ubuntu precise openmanage/740 >/etc/apt/sources.list.d/dell.list`;
 	    }
     }else{
		#$action=`sudo cp /tmp/dell.list.65 /etc/apt/sources.list.d/dell.list`;
		$action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/omsa_6.5 / >/etc/apt/sources.list.d/dell.list`;
		$Version="6.5";
	}
	$action=`wget http://debmiroir-02.ac.centre-serveur.i2/gpg/cle_omsa_6.5.gpg`;
	$action=`sudo apt-key add cle_omsa_6.5.gpg`;
	$action=`sudo apt-key add /usr/lib/nagios/plugins/cle_dell.txt`;
}

$cmd="sudo apt-get update";
system $cmd;
print BOLD, YELLOW, "Installation de la version $Version Eole $VersEole Architecure $arch\n",RESET;
print BOLD, YELLOW, "Suppression des anciens paquets dell ...\n",RESET;
sleep(5);
$cmd="sudo apt-get -y --purge remove dellomsa";
system $cmd;
$cmd="sudo apt-get -y --purge remove srvadmin-all";
system $cmd;
$cmd="sudo apt-get -y --purge remove srvadmin-base srvadmin-omcommon srvadmin-storageservices";
system $cmd;
$cmd="sudo apt-get -y --purge autoremove";
system $cmd;
print BOLD, YELLOW, "Installation des nouveaux paquets dell ...\n",RESET;
sleep(5);
$cmd="sudo apt-get -y install srvadmin-all";
system $cmd;
$cmd="sudo sed -i -e 's/\\x00release_date\\x00/\\x00version\\x00\\x00\\x00\\x00\\x00\\x00/' /opt/dell/srvadmin/lib/libstorelib.so.4";
system $cmd;
$cmd="sudo rm -f /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
system $cmd;
$cmd="sudo touch /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
system $cmd;
$cmd="sudo chmod 777 /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
system $cmd;
$cmd="sudo apt-get -f install";
system $cmd;
print BOLD, YELLOW, "Relance des services dell ...\n",RESET;
#sleep(5);
$cmd="sudo /etc/init.d/dataeng restart";
system $cmd;

print BOLD, YELLOW, "Test du plugin ...\n",RESET;
#sleep(5);
$cmd=`/usr/lib/nagios/plugins/check_dell.pl`;
print "$cmd\n";
if($cmd =~ /ERROR: Dell OpenManage Server Administrator/ and $Version eq "6.5") {
    	if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
   	}else{
		$cmd=`cp /usr/lib/nagios/plugins/NoCheckDell.pl.exemple /usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
  	 }
}
if($cmd =~ /ERROR: Dell OpenManage Server Administrator/ and $Version ne "6.5") {
    print BOLD, YELLOW, "Erreur detectee dans installation standard , tentative version simplifiee\n",RESET;
    sleep(5);
    print BOLD, YELLOW, "Installation des nouveaux paquets dell ...\n",RESET;
   $cmd="sudo apt-get -y install srvadmin-base srvadmin-omcommon srvadmin-storageservices";
   system $cmd;
   $cmd="sudo sed -i -e 's/\\x00release_date\\x00/\\x00version\\x00\\x00\\x00\\x00\\x00\\x00/' /opt/dell/srvadmin/lib/libstorelib.so.4";
   system $cmd;
    print BOLD, YELLOW, "Relance des services dell ...\n",RESET;
#sleep(5);
   $cmd="sudo /etc/init.d/dataeng restart";
   system $cmd;

   print BOLD, YELLOW, "Test du plugin ...\n",RESET;
   #sleep(5);
   $cmd=`/usr/lib/nagios/plugins/check_dell.pl`;
   print "$cmd\n";
   exit;
}

if($cmd =~ /ERROR: Dell OpenManage Server Administrator/ and $Version ne "6.5") {
    print BOLD, YELLOW, "Erreur detectee dans installation simplifiee , tentative installation 6.5\n",RESET;
    sleep(5);
    $action=`sudo rm -f /etc/apt/sources.list.d/dell.list`;
    $action=`sudo touch /etc/apt/sources.list.d/dell.list`;
    $action=`sudo chmod 777 /etc/apt/sources.list.d/dell.list`;
    $action=`sudo echo deb http://debmiroir-02.ac.centre-serveur.i2/dell/omsa_6.5 / >/etc/apt/sources.list.d/dell.list`;
    $Version="6.5";
    $cmd="sudo apt-get update";
    system $cmd;
    $cmd="sudo apt-get -y install srvadmin-all";
    system $cmd;
    $cmd="sudo rm -f /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
    system $cmd;
    $cmd="sudo touch /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
    system $cmd;
    $cmd="sudo chmod 777 /opt/dell/srvadmin/lib/srvadmin-idrac/register-rac-components.sh";
    system $cmd;
    $cmd="sudo apt-get -f install";
    system $cmd;
    print BOLD, YELLOW, "Relance des services dell ...\n",RESET;
    #sleep(5);
    $cmd="sudo /etc/init.d/dataeng restart";
    system $cmd;

    print BOLD, YELLOW, "Test du plugin ...\n",RESET;
    #sleep(5);
    $cmd=`/usr/lib/nagios/plugins/check_dell.pl`;
    print "$cmd\n";
    if($cmd =~ /ERROR: Dell OpenManage Server Administrator/) {
    	if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
   	}else{
		$cmd=`cp /usr/lib/nagios/plugins/NoCheckDell.pl.exemple /usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
  	 }
   }else{
    	if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
		$cmd=`echo '\$psin="V 6.5 FORCE PB installation Paquets";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
   	}else{
		$cmd=`cp /usr/lib/nagios/plugins/NoCheckDell.pl.exemple /usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$psin="V 6.5 FORCE PB installation Paquets";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
  	 }

  }

}

if($cmd =~ /object not found/ and $Version eq "6.5") {
    	if( -e "/usr/lib/nagios/plugins/NoCheckDell.pl") {
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
   	}else{
		$cmd=`cp /usr/lib/nagios/plugins/NoCheckDell.pl.exemple /usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$psin="VERIFICATION PSIN OK";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
		$cmd=`echo '\$NoCheck="NON";'>>/usr/lib/nagios/plugins/NoCheckDell.pl`;
  	 }
}
exit 0;

