#!/usr/bin/perl -w
####################### check_ldap.pl #######################
# Version : 1.2
# Date : 31 Mars 2011
# rajout log
# Author  : PSIN P.Malossane
# Licence : GPL - http://www.fsf.org/licenses/gpl.txt
#############################################################
#
# help : ./check_ldap_statut
#Verifie le statut de la connexion ldap central
#si pas de reponse au bout de 45 secondes on bascule en local
# si le central est oerationnel au bout de 30 minutes on bascule sur le central
# si mode autre que central ou local , on ne fait rien
#Version 1.01 28/01/2011
#Ajout du controle bascule en cours Si ecdl_bascule tourne on sort 
#Version 1.22 26/05/2011
#Modification gestion log
#Version 1.30 30/05/2011
#Gestion mode mixte/LOCAL  par defaut  option -d central pour gestion central/local


use strict;
use Getopt::Long;

# Nagios specific

use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS $TIMEOUT);
use Time::HiRes 'time','sleep';
#my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);
use POSIX qw(strftime);

# Globals

my $Version='1.30';
my $Name=$0;
my $o_search="numEntries: 1";
my $o_domaine="ou=organisation,dc=equipement,dc=gouv,dc=fr";
my $CodeSortie = 0;
my $latency;
my $wlog;
my $critical=20;
my $warning=15;
my $starttimer;
my $endtimer;
my $message;
my $dureefic=0;
my $dureeficF=0;
my $opt_d="mixte";         
my $opt_v;
my $opt_c;
my $opt_l;
my $opt_s;
my $opt_m;
my $o_help;

my $filelocal="/usr/lib/nagios/plugins/bascule.loc";
my $filelocalF="/usr/lib/nagios/plugins/basculeF.loc";
my $filetracebasc="/usr/lib/nagios/plugins/journalbascule.log";
my $filelog="/usr/lib/nagios/plugins/logldap.log";
my $filelog2="/usr/lib/nagios/plugins/logldap2.log";
my $filelogh="/usr/lib/nagios/plugins/logldaphistory.log";
#Time out commande ldapsearch 
my $TimeOut=12;
my $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
my $datelog = strftime "%d-%m-%Y %H:%M:%S", localtime;
my $heurelog = strftime "%H:%M", localtime;
#my $minute_aleatoire = int(rand(10));

# functions

sub help {
    print "Usage: check_ldap_statut.pl avec options :\n";
    print " -d mode de fonctionnement normal souhait� (mixte par defaut) \n";
    print " -v (verbose) \n";
    print " -s liste le mode ldap actuel\n";
    print " -c force le mode LDAP en ldap CENTRAL \n";
    print " -l force le mode LDAP en ldap LOCAL pour 8 heures maximum \n";
    print " -m force le mode ldap en mode Mixte \n";
    exit 0;
}

sub liste_mode {
	print "Mode LDAP actuel :\n";
	my $ldapmode=`/usr/share/ecdl/bin/ecdl_bascule_ldap -s`;
	print $ldapmode;
exit 0;
}

sub purge_log {
	print "Purge du fichier de log :\n";
        $datelog = strftime "%d-%m-%Y %H:%M:%S ", localtime;
        open(DESCRL,">>$filelog");
        print (DESCRL "### $datelog : Purge du fichier de log\n");
        close(DESCRL);
	#my $purge=`rm -f $filelogh`;
	my $purge=`tail -n +12000 $filelog >> $filelogh`;
	#$purge=`cp $filelog $filelogh`;
	$purge=`tail -n 12000 $filelog >$filelog2`;
	$purge=`mv $filelog2 $filelog`;
	$purge=`tail -c 10M $filelogh >$filelog2`;
	$purge=`mv $filelog2 $filelogh`;
}

sub ecrit_log {
	$wlog=$_[0];
        $datelog = strftime "%d-%m-%Y %H:%M:%S ", localtime;
        open(DESCRL,">>$filelog");
        print (DESCRL "### $datelog : $wlog\n");
        close(DESCRL);
}

sub bascule_force_mixte {
	print "Bascule FORCEE sur le ldap mixte \n";
	my $ldapmode=`/usr/share/ecdl/bin/ecdl_bascule_ldap -m`;
        my $ldapSup=`rm -rf /usr/lib/nagios/plugins/bascule.loc`;
        my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
	print $ldapmode;
        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
        open(DESCR,">>$filetracebasc");
        print (DESCR "$datebascule : BASCULE FORCEE MANUELLEMENT SUR MODE LDAP MIXTE\n");
        close(DESCR);
exit 0;
}

sub bascule_force_central {
	print "Bascule FORCEE sur le ldap central \n";
	my $ldapmode=`/usr/share/ecdl/bin/ecdl_bascule_ldap -c`;
        my $ldapSup=`rm -rf /usr/lib/nagios/plugins/bascule.loc`;
        my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
	print $ldapmode;
        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
        open(DESCR,">>$filetracebasc");
        print (DESCR "$datebascule : BASCULE FORCEE MANUELLEMENT SUR LDAP CENTRAL\n");
        close(DESCR);
exit 0;
}


sub bascule_force_locale {
	print "Bascule FORCEE sur le ldap LOCAL \n";
	my $ldapmode=`/usr/share/ecdl/bin/ecdl_bascule_ldap -l`;
        my $ldapSup=`rm -rf /usr/lib/nagios/plugins/bascule.loc`;
        open(DESCR,">$filelocalF");
        close(DESCR);
	print $ldapmode;
        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
        open(DESCR,">>$filetracebasc");
        print "$datebascule : BASCULE FORCEE MANUELLEMENT SUR LDAP LOCAL POUR 8 HEURES\n";
        print (DESCR "$datebascule : BASCULE FORCEE MANUELLEMENT SUR LDAP LOCAL POUR 8 HEURES\n");
        close(DESCR);
exit 0;
}

sub verif_central {
my ($debug) = @_;
my $resu;
my $o_host="ldapsmb.ac.melanie2.i2";
my $o_login="admin*";
my $o_domaine;
my $o_search="homeDirectory";

#recup domaine dans $o_domaine et force les elments recherches
my $domaine=`grep "ldap suffix" /etc/samba/smb.conf`;
($resu) = $domaine =~ /ldap suffix \= (.*)$/;
#print $resu;
if ( !defined($resu) ) {
        $message="ERREUR RECHERCHE LDAP : Domaine introuvable dans configuration Samba\n";
	ecrit_log($message);
        $CodeSortie=2;
        #exit $ERRORS{"CRITICAL"};
}

$o_domaine=$resu;
#$o_login="admin."$o_domaine;



my $ldap;
my $ldaploc;
 $starttimer = time();

eval {
    local $SIG{ALRM} = sub { die "timeout" };
    alarm $TimeOut;
	ecrit_log("/usr/bin/ldapsearch -H ldaps://$o_host -x uid=$o_login -b $o_domaine -v");
	$ldap=`/usr/bin/ldapsearch -H ldaps://$o_host -x uid=$o_login -b $o_domaine -v -d $debug`;
};
#ecrit_log("ldap=$ldap");

if ($@) {
		ecrit_log($ldap);
		$ldap=" *** TIMEOUT $TimeOut s ***";
		ecrit_log($ldap);
        	my $arr=`/usr/bin/killall -9 ldapsearch`;
		ecrit_log("process=$arr");
}
alarm 0;
$endtimer = time();
$latency = (int(1000 * ($endtimer - $starttimer)) / 1000);
if ($opt_v ) {
        print "/usr/bin/ldapsearch -H ldaps://$o_host -x uid=$o_login -b $o_domaine -v\n";
        print $ldap ;
}
#chomp @results;
#verifi existence uid
if ($ldap =~ /numEntries/) {
        #verifi mot cle
        my $ETAT="LDAP Central OK";
        if ($ldap =~ /$o_search/) {
                if ($latency > $critical) {
                        $message="LDAP CENTRAL $ETAT Temps CRITIQUE! UID $o_login TROUVE sur $o_host mais Temps critique  $latency s > $critical s \n";
                        $CodeSortie=2;
			ecrit_log($message);
                        #exit $ERRORS{"CRITICAL"};
                }else{
                        $message="LDAP CENTRAL $ETAT  UID $o_login TROUVE sur $o_host en $latency s \n";
                        $CodeSortie=0;
                        #exit $ERRORS{"OK"};
                }

        }else{
                $message= "LDAP CENTRAL CRITICAL! UID $o_login TROUVE sur $o_host mais Mot cle $o_search NON TROUVE  \n";
                $CodeSortie=2;
		ecrit_log("ldap=$ldap");
		ecrit_log($message);

                #exit $ERRORS{"CRITICAL"};

        }
}else{

        $message="LDAP Central CRITIQUE Pas de reponse\n";
	ecrit_log("ldap=$ldap");
	ecrit_log($message);
        $CodeSortie=2;
}
}
#fin fonction Verif_central



sub bascule_locale() {
        #bascule sur le ldap local
        print "Bascule automatique sur le serveur LOCAL\n";
        my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
        my $ldapBL;
        $ldapBL=`/usr/share/ecdl/bin/ecdl_bascule_ldap -l`;
        #creation du fichier bascule.loc
        open(DESCR,">$filelocal");
        close(DESCR);
        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
        open(DESCR,">>$filetracebasc");
        print (DESCR "$datebascule : BASCULE AUTO SUR LDAP LOCAL\n");
        close(DESCR);

        $message="CRITIQUE LDAP CENTRAL Critique - BASCULE Automatique effectuee sur le LOCAL\n";
	ecrit_log($message);
        $CodeSortie=2;
}

sub bascule_central() {
        #bascule sur le ldap central
        print "Bascule automatique sur le serveur CENTRAL\n";
        my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
        my $ldapCENT;
        #LE DELAIS DE BASCULE sur le central est de 20 minutes + minute_aleatoire
	my $timeattente=1200;
	#$timeattente=1200+(60*$minute_aleatoire);
        my $duree=`/usr/lib/nagios/plugins/check_file_age /usr/lib/nagios/plugins/bascule.loc -w 5000 -c 8000`;
        #si le fichier n'existe pas , bascule force ,, on le cree
        if ($duree =~ /File not found/) {
                $dureefic=300;
                open(DESCR,">$filelocal");
                close(DESCR);
        }else{
                ($dureefic) = $duree =~ /is (.*) seconds/;
                #print $dureefic;
        }
        if($dureefic > $timeattente) {
                verif_central(0);
                if($CodeSortie!=2) {
                        #bascule central
                        $ldapCENT=`/usr/share/ecdl/bin/ecdl_bascule_ldap -c`;
                        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
                        open(DESCR,">>$filetracebasc");
                        print (DESCR "$datebascule : BASCULE AUTO SUR LDAP CENTRAL\n");
                        close(DESCR);
                        my $ldapSup=`rm -rf /usr/lib/nagios/plugins/bascule.loc`;
                        $message="OK LDAP CENTRAL en service apres bascule - BASCULE Automatique effectuee sur le CENTRAL\n";
						print $message;
                }else{
                        $message="CRITIQUE  LDAP LOCAL en service - BASCULE sur CENTRAL impossible apres ".int($dureefic/60)." minutes LDAP CENTRAL NE REPOND PAS \n";
                        print "CRITIQUE  LDAP LOCAL en service - BASCULE sur CENTRAL impossible apres ".int($dureefic/60)." minutes \n";
                        open(DESCR,">$filelocal");
						close(DESCR);
				}
        }else{
                $CodeSortie=2;
                $message="CRITIQUE LDAP LOCAL en service . Attente de ".int(($timeattente-$dureefic)/60)." minutes pour bascule Automatique sur LDAP CENTRAL\n";
                print "ATTENTE BASCULE sur CENTRAL : ".int(($timeattente-$dureefic)/60)." minutes \n";
        }
}

sub bascule_mixte() {
        #bascule mode mixte
        print "Bascule automatique en mode mixte\n";
        my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
        my $ldapCENT;
        #LE DELAIS DE BASCULE sur le central est de 20 minutes + minute_aleatoire
	my $timeattente=1200;
	#$timeattente=1200+(60*$minute_aleatoire);
        my $duree=`/usr/lib/nagios/plugins/check_file_age /usr/lib/nagios/plugins/bascule.loc -w 5000 -c 8000`;
        #si le fichier n'existe pas , bascule force ,, on le cree
        if ($duree =~ /File not found/) {
                $dureefic=300;
                open(DESCR,">$filelocal");
                close(DESCR);
        }else{
                ($dureefic) = $duree =~ /is (.*) seconds/;
                #print $dureefic;
        }
        if($dureefic > $timeattente) {
                verif_central(0);
                if($CodeSortie!=2) {
                        #bascule central
                        $ldapCENT=`/usr/share/ecdl/bin/ecdl_bascule_ldap -m`;
                        $datebascule = strftime "%d-%m-%Y %H:%M:%S", localtime;
                        open(DESCR,">>$filetracebasc");
                        print (DESCR "$datebascule : BASCULE AUTO EN MODE MIXTE\n");
                        close(DESCR);
                        my $ldapSup=`rm -rf /usr/lib/nagios/plugins/bascule.loc`;
                        $message="OK LDAP en service apres bascule - BASCULE Automatique effectuee en mode MIXTE\n";
						print $message;
                }else{
                        $message="CRITIQUE  LDAP LOCAL en service - BASCULE ien MODE MIXTE impossible apres ".int($dureefic/60)." minutes LDAP CENTRAL NE REPOND PAS \n";
                        print "CRITIQUE  LDAP LOCAL en service - BASCULE en MODE MIXTE impossible apres ".int($dureefic/60)." minutes \n";
                        open(DESCR,">$filelocal");
						close(DESCR);
				}
        }else{
                $CodeSortie=2;
                $message="CRITIQUE LDAP LOCAL en service . Attente de ".int(($timeattente-$dureefic)/60)." minutes pour bascule Automatique en mode MIXTE\n";
                print "ATTENTE BASCULE ien mode MIXTE de ".int(($timeattente-$dureefic)/60)." minutes \n";
        }
}

sub sortie() {
        my $filetrace="/usr/lib/nagios/plugins/mode_ldap.log";
        open(DESCR,">$filetrace");
        print (DESCR "$CodeSortie\n");
        print (DESCR "$message");
        close(DESCR);
	ecrit_log("**** RESULTAT : $message");
        exit 0;
};

sub sortieD() {
        open(DESCR,">>$filetracebasc");
        print (DESCR "$CodeSortie ");
        print (DESCR "$datebascule $message \n");
        close(DESCR);
	ecrit_log("**** RESULTAT : $message");
        exit 0;
};

sub check_options {
    Getopt::Long::Configure ("bundling");
    GetOptions(
        'h'     => \$o_help,            'help'          => \$o_help,
        'l'   => \$opt_l,           'local'       => \$opt_l,
        'm'   => \$opt_m,          'mixte'         => \$opt_m,
        'd=s'   => \$opt_d,        'modedefaut=s'     => \$opt_d,
        'c'   => \$opt_c,             'central'    => \$opt_c,
        's'   => \$opt_s,             'liste'    => \$opt_s,
        'v'     => \$opt_v,             'verbose'       => \$opt_v
    );
    if (defined ($o_help)) { help(); exit 0};
    if (defined ($opt_c)) { bascule_force_central();exit 0};
    if (defined ($opt_l)) { bascule_force_locale();exit 0};
    if (defined ($opt_m)) { bascule_mixte();exit 0};
    if (defined ($opt_s)) { liste_mode();exit 0};
}


########## MAIN #######
#print $heurelog;
if ( $heurelog eq "23:01" or $heurelog eq "23:02" ) { purge_log(); }
ecrit_log("###############################################################");
ecrit_log("######## Nouvelle Verification ################################");
ecrit_log("###############################################################");
check_options();

#Verification que le script ecdl_bascule ne tourne pas
my $VerifBascule;
$VerifBascule=`/bin/ps -C ecdl_bascule_ldap`;
if ($VerifBascule =~ /ecdl_bascule/) {
	print "Operation de bascule en cours . Verification annulee\n";	
	$message= "Operation de bascule en cours . Verification annulee\n";	
	ecrit_log($message);
	$CodeSortie=0;
	#exit 0;
	sortieD();
}
# Verif si bascule local force
	#my $timeattenteF=28800;
	my $timeattenteF=3000;
	#$timeattente=1200+(60*$minute_aleatoire);
        my $dureeF=`/usr/lib/nagios/plugins/check_file_age /usr/lib/nagios/plugins/basculeF.loc -w 5000 -c 8000`;
        #si le fichier n'existe pas , bascule force ,, on le cree
        if ($dureeF =~ /File not found/) {
		$dureeficF=300;
        }else{
                ($dureeficF) = $dureeF =~ /is (.*) seconds/;
                if($dureeficF > $timeattenteF) {
        		open(DESCR,">>$filetracebasc");
        		print (DESCR "$datebascule FIN BASCULE LOCAL FORCEE DETECTEE - REPRISE PROCESSUS NORMAL\n");
        		close(DESCR);
        		$message="DEGRADE  FIN BASCULE LOCAL FORCEE DETECTEE - REPRISE PROCESSUS NORMAL\n";
			ecrit_log($message);
        		my $ldapSupF=`rm -rf /usr/lib/nagios/plugins/basculeF.loc`;
			$CodeSortie=1;
			sortie();
        		#exit 0;
		}else{
        		open(DESCR,">>$filetracebasc");
        		print (DESCR "$datebascule ARRET CONTROLE : MODE LOCAL FORCEE DETECTE ( Reste ".int(($timeattenteF-$dureeficF)/60)." minutes) \n");
        		close(DESCR);
        		$message="DEGRADE ARRET CONTROLE : MODE LOCAL FORCEE DETECTE ( Reste ".int(($timeattenteF-$dureeficF)/60)." minutes)\n";
			ecrit_log($message);
			$CodeSortie=1;
			sortie();
			
		}
	}
#recherche le mode actuel
my $ldapmode;
my $relance=10;
$ldapmode=`/usr/share/ecdl/bin/ecdl_bascule_ldap -s`;
ecrit_log($ldapmode);
print "ldapmod=$ldapmode\n";
print "opt_d=$opt_d";
if ($ldapmode =~ /$opt_d/) {
        print "Mode $opt_d\n";
        print "Interrogation LDAP national...\n";
        verif_central(0);
        print $message;
        #print $CodeSortie;
        if($CodeSortie ==2) {
                #on attends 30 s puis  nouvelle tentative
                print "Attente de ".$relance." secondes avant nouvelle tentative \n";
                sleep($relance);
                print "Interrogation annuaire national..Tentative 2/3\n";
                ecrit_log("Interrogation annuaire national..Tentative 2/3\n");
                verif_central(1);
                if($CodeSortie==2) {
                        #on attends 30 s puis  nouvelle tentative
                        print "Attente de ".$relance." secondes avant nouvelle tentative (3/3)\n";
                        sleep($relance);
                	print "Interrogation annuaire national..Tentative 3/3\n";
                	ecrit_log("Interrogation annuaire national..Tentative 3/3\n");
                        verif_central(1);
                        if($CodeSortie==2) {
                                #on attends 30 s puis  nouvelle tentative
                                
				#print "Attente de ".$relance." secondes avant derniere tentative\n";
                                #sleep($relance);
                                #print "Interrogation annuaire national..Tentative 3/3\n";
                                #verif_central(1);
                                #if($CodeSortie==2) {
                                #        #bascule local
	                        #             bascule_locale();
				#}
				bascule_locale();
			}
		}	
	}


}elsif($ldapmode =~ /local/) {
		#verif central
		#si OK on lance l'attente de 30 minutes avant bascule sur le central
        print "Mode local\n";
        print "Verification LDAP national...\n";

		verif_central(0);
		 if($CodeSortie!=2) {
			#bascule_central();
			bascule_$opt_d();
		 }else{
			open(DESCR,">$filelocal");
			close(DESCR);
			$message="CRITIQUE Mode LDAP LOCAL - LDAP CENTRAL NE REPOND PAS - Lancement process de bascule impossible\n";
			print $message;
			$CodeSortie=2;
		 }
}elsif($ldapmode =~ /test/) {
		$message="WARNING Mode LDAP Test . Pas de verification effectuee\n";
		$CodeSortie=0;

}elsif($ldapmode =~ /mixte/) {
		$message="WARNING Mode LDAP Mixte . Bascule FORCEE en Central\n";
		bascule_force_central();
		$CodeSortie=1;
}elsif($ldapmode =~ /central/) {
		$message="WARNING Mode LDAP CENTRAL . Bascule FORCEE en iMode Mixte\n";
		bascule_force_mixte();
		$CodeSortie=1;
}

sortie();
exit 0;
