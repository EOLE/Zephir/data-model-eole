#!/usr/bin/perl

# $Id: check_traceroute.pl,v 1.1 2005/01/27 10:34:16 stanleyhopcroft Exp $

# $Log: check_traceroute.pl,v $
# Revision 1.1  2005/01/27 10:34:16  stanleyhopcroft
# Jon Meek's check_traceroute for Mon hacked by YT for Nagios. Prob pretty weak
#

use strict ;
use lib "/usr/lib/nagios/plugins";
use vars qw(%ERRORS $TIMEOUT) ;
use utils qw(%ERRORS  $TIMEOUT &print_revision &support &usage) ;
use Time::HiRes 'time','sleep';
sub print_help ();
sub print_usage ();

use POSIX qw(strftime);
my $datetest = strftime "%d%m%Y %H:%M:%S", localtime;


$ENV{'PATH'}='/bin:/usr/bin:/usr/sbin';

my $PROGNAME			= 'check_traceroute' ;
										# delay units are millisecs.
my $MAX_INTERHOP_DELAY	= 200 ;
my $MAX_HOPS 			= 30 ;
my $PORTDEF                = 636 ;

use Getopt::Std;

use vars qw($opt_H $opt_N $opt_r $opt_R $opt_T $opt_d $opt_h $opt_i $opt_v $opt_V $opt_p) ;

getopts('p:i:H:N:R:T:dhrvV');
										# H, N, R, T, and i take parms, others are flags

do { print_help ; exit $ERRORS{'OK'}; }
	if $opt_h ;


										# Set default timeout in seconds
my $TIMEOUT=60;
my $TimeOut			= $opt_T || $TIMEOUT;
my $port			= $opt_p || $PORTDEF;

my $max_hops 		= $opt_N || $MAX_HOPS ;

my $TRACEROUTE		= 'sudo tcptraceroute';

my $TargetHost		= $opt_H ;

print_help
	unless $TargetHost ;

my ($route, $pid, $rta_list, $tempstot, $perf) = ( '', '', '', 0, '' );
if($opt_v ) { print "$TRACEROUTE $TargetHost $port \n";}
my $startruntimer = time();

$SIG{ALRM} = sub { die "timeout" };

eval {

	alarm($TimeOut);
										# XXXX Discarding STDERR _should_ reduce the risk
										# of unexpected output but consequently, results for
										# non existent hosts are stupid. However, why would you
										# specify a route to a NX host, other than a typo ...
    if($opt_v ) {
	    $route = `$TRACEROUTE $TargetHost 636 `;
    }else{
        $route = `$TRACEROUTE $TargetHost 636 2>/dev/null`;
    }
};
#print "Temps total ".$tempstot;

alarm(0);
my $k="";
if ( $@ and $@ =~ /timeout/ ) {
		$k=`sudo killall -9 tcptraceroute`;
        print "Check TCPTRACEROUTE port 636 sur $TargetHost CRITIQUE : TIMEOUT $TimeOut s \n";
	open(FILE,">>"."/usr/lib/nagios/plugins/TraceRoutesLdaps.log");
	print FILE "\n-----ERREUR Test du $datetest -------\n";
	print FILE "\nERREUR TIMEOUT $TimeOut s\n";
	print FILE $route;
	close(FILE);
        exit 2;

}

my $endruntimer = time();
my $totalruntime = (int(1000 * ($endruntimer - $startruntimer)) / 1000);

chop($route) ;

print "$route\n"
	if $opt_v;
if($route =~ /open/) {
        print "Check TCPTRACEROUTE port 636 sur $TargetHost OK ( port ouvert) en  $totalruntime s |temps=$totalruntime\n";
        exit 0;
    }
open(FILE,">>"."/usr/lib/nagios/plugins/TraceRoutesLdaps.log");
print FILE "\n-----ERREUR  Test du $datetest -------\n";
print FILE $route;
close(FILE);
if($route =~ /close/) {
    print "Check TCPTRACEROUTE port 636 sur $TargetHost CRITIQUE (port ferme) en  $totalruntime s \n$route |temps=0\n";
}else{
    print "Check TCPTRACEROUTE port 636 sur $TargetHost CRITIQUE  \n$route |temps=0\n";
}
exit 2;



sub print_usage () {
	print "Usage: $PROGNAME  [ -T timeout -v -h ] -H <host> \n";
}

sub print_help () {
print_usage();
	print "
-h
   Help
-H
   Host.
-v
   Greater verbosity.
-T
   Maximum time (seconds) to wait for the traceroute command to complete. Defaults to $TIMEOUT seconds.

";
	support();
}
