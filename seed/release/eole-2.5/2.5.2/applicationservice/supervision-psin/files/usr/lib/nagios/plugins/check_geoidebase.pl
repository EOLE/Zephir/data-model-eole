#!/usr/bin/perl
#
# V 1.0 initiale 25/01/2014
# V 1.01  06/10/2014 Suppresion temporaire alerte sur replication centrale

#use lib "/usr/local/libexec/nagios";
#use utils qw(%ERRORS &print_revision &support &usage);
#

if( -e "/usr/lib/nagios/plugins/ParamCheck.pl") {
     require "/usr/lib/nagios/plugins/ParamCheck.pl";
    if($ParamGeobase eq "No") {
        print "Supervision GEOBASE non effectuee volontairement\n";
     exit 0;
    }
}

use Getopt::Long qw(:config no_ignore_case bundling);

use strict;
my $Message="";
my $debug="";
my $verif="";
my $version="";
my $IP="";

GetOptions (
    "v" => \$debug, "--debug" => \$debug,
    "t" => \$verif, "--test" => \$verif,
    "H=s" => \$IP, "ip=i" => \$IP
);


if($IP eq "") {
	print "Usage : check_geoidebase.pl -H IP_SERVEUR [-v (verbose)]  [-t (test install geoidebase)]\n";
    exit;
}

#le matin relance service geo-ide_base-php
#my $purge = `/usr/lib/nagios/plugins/check_file_age -f /tmp/diaggeoide.log -c 18000 -w 18000`;
#if($purge =~ /CRITICAL/i) {
 #   print "Relance service geo-ide-base-php ";
#    $purge = `sudo /etc/init.d/geo-ide-base-php restart`;
#}

my $purge=`sudo rm -f /tmp/diaggeoide.log`;
$purge=`sudo rm -f /tmp/erreur_geoide.log`;
$purge=`sudo rm -f home/nagios/.pgpass`;
$purge=`sudo rm -f /usr/lib/nagios/plugins/DiagGeoIde.log`;
#verif iinstallation geobase
#
#
 my $paq=`dpkg -l | grep geo-ide-base-php`;
if($debug) {print "Paquets installes :\n $paq";}
 my $var1=`sudo /usr/bin/ParseDico --list activer_geoide_base 2>/dev/null`;
if($debug) {print "Variable activer_geoide_base:\n $var1";}
 my $var2=`sudo /usr/bin/ParseDico --list inst_pallier12 2>/dev/null`;
if($debug) {print "Variable inst_pallier12 :\n $var2";}
my $module="";
if ($var1 =~ /Mise en place/) {
	$module=$module." Variable activer_geoide_base OK";
}else{
	$module=$module." Variable activer_geoide_base NON PRESENTE OU INCORRECTE (".chomp($var1).")";
}

if ($var2 =~ /oui/ or $var2 =~ /non/) {
    $var2="oui";
	$module=$module." Variable inst_pallier12 OK ";
}else{
	$module=$module." Variable inst_pallier12 NON PRESENTE OU INCORRECTE (".chomp($var2).")";
}

if ($paq =~ /geo-ide-base-php/) {
	$module=$module." Paquet geo-ide-base-php OK";
}else{
	$module=$module." Paquet geo-ide-base-php NON INSTALLE";
}
if ($var1 =~ /Mise en place/ and $var2 =~ /oui/ and $paq =~ /geo-ide-base-php/) {
        if($verif) {
                my  $testbase ="";
            #my $testbase=`/usr/lib/nagios/plugins/check_ftp_rw.pl --host app-ftp17.sen.centre-serveur.i2 1>/tmp/base.log 2>&1`;
                $testbase=`grep FATAL /tmp/base.log`;
                if($testbase =~ /FATAL/) {
                    print "Check GEOBASE : GEOBASE NON INSTALLE ou Incomplet \n";
                }else{
                     print "GEOBASE INSTALL OK\n";
                 }
                exit 0;
        }
}else{

    #verif palier 0
    $paq=`sudo dpkg -l | grep eole-geo`;
    if (-e "/etc/samba/includes/smb-geoide.conf" and -e "/var/log/geobase" and $paq =~ /eole-geo-ide-base/) {
        print "GEOBASE PALIER 0 INSTALLE\n";
        exit 0;
    }

    print "EOLE-GEO-C001 Check GEOBASE : GEOBASE NON INSTALLE ou Incomplet $module\n";
    exit 1;
}

if($debug) {print "Verifications installation :\n $module";}

my $CodeSortie=0;
my $NbErr=0;
my $LibErreur="";
my @Nli;
my $lid=0;
my $lif=0;
my $cmd="";
my $erreur="";
$purge=`sudo rm -f /tmp/erreur_geoide.log`;
$purge=`sudo rm -f /tmp/diaggeoide.log`;
$purge=`sudo rm -f /usr/lib/nagios/plugins/DiagGeoIde.log`;
#------------------------
#recup page diagnostic
##------------------------

if($debug) {print "\nCheck Page Diagnostics...\nsudo wget http://$IP/geo-ide-base/index.php/admin/checkSystem -o /tmp/diaggeoide.log -O /usr/lib/nagios/plugins/DiagGeoIde.log\n";}
my $Recup=`sudo wget http://$IP/geo-ide-base/index.php/admin/checkSystem -o /tmp/diaggeoide.log -O /usr/lib/nagios/plugins/DiagGeoIde.log`;

my $test = `grep -iE "Connection refuse\|Connexion refus\|ERREUR\|404" /tmp/diaggeoide.log`;
if($debug) {print $test."\n";}
chomp($test);
if($debug) {print "test=***".$test."***\n";}
if($test =~ / 404 / or $test =~ /ERREUR/ or $test =~ /Connexion refus/ or $test =~ /Connection refus/) {
    print "EOLE-GEO-C002 Diagnostic GeoIdeBase CRITIQUE : Erreur acces Page de diagnostic .Verifier la configuration $test\n";
    exit 2;
}
$version=`grep Version /usr/lib/nagios/plugins/DiagGeoIde.log`;
chomp($version);
($version)=$version =~ /Version (.*) du/;
if($debug) {print "Version : $version\n";}
$test = `grep erreur /usr/lib/nagios/plugins/DiagGeoIde.log`;
chomp($test);
if($test =~ /erreur/) {

    my $NbErreur = $test =~ /\<b>(.+)erreur/;
    $NbErr=$1;
    my $li=`grep -ni "Ko" /usr/lib/nagios/plugins/DiagGeoIde.log >/tmp/erreur_geoide.log`;
    open(FIC,"/tmp/erreur_geoide.log");
    while (<FIC>)
     {
           @Nli=split(/:/,$_);
           #print "Ligne numero $Nli[0]\n";
           $lid=$Nli[0]+2;
           $lif=$Nli[0]+3;
           #print "debut:$lid fin:$lif\n";
            $cmd="sed -n '".$lid.",".$lif."p'";
            $erreur=`$cmd /usr/lib/nagios/plugins/DiagGeoIde.log`;
            $erreur =~ s/\<br>//g;
            $erreur =~ s/\<br\/>//g;
            $erreur =~ s/\<tr>//g;
            $erreur =~ s/\<td>//g;
            $erreur =~ s/\<b>//g;
            $erreur =~ s/\<\/b>//g;
            $erreur =~ s/\<\/td>//g;
            $erreur =~ s/\<\/tr>//g;
            $erreur =~ s/\<\\n//g;
            $erreur =~ s/   //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            $erreur =~ s/  //g;
            #if($erreur =~ /plication vers l'entrep/ and $erreur =~ /central/ and $version =~ /5890M/) {
            if($erreur =~ /Sauvegarde de la base de don/) {
                $NbErr=$NbErr-1;
	   #traitement temporaire GEO REPEROIRE acces
	    }elsif( $erreur =~ /pertoire/) {
                $LibErreur=$LibErreur." Erreur acces Geo Repertoire ignoree ";
                if($CodeSortie==0) { $CodeSortie=1;}
            }else{
                $LibErreur=$LibErreur." ".$erreur;
                $CodeSortie=2;
            }

           #
    }
    close(FIC);
    $Message="Diagnostic GeoIdeBase CRITIQUE : $NbErr erreur(s) detectee(s) $LibErreur";
}

if($debug) {print "Nombre erreur : $NbErr Erreur:$LibErreur\n";}
if($debug) {print "Code Sortie apres page verif :".$CodeSortie."\n"; }
#verif proces resque
#
my $Message2="";
my $Message3="";
my $Message4="";
if($debug) {print "Check process resque...\n";}
my $VerifProcesResque=`/usr/lib/nagios/plugins/check_procs -w 5:10 -c 5:10 -a resque`;
if($debug) {print $VerifProcesResque."\n";}
if($VerifProcesResque =~ /OK/) {
    $Message2="Process resque OK";
}else{
    $Message2="Process resque CRITIQUE ".chomp($VerifProcesResque);
    $CodeSortie=2;
}

if($debug) {print "Code Sortie apres check process resque :".$CodeSortie."\n"; }
if($debug) {
	print "$Message2\n";
	my $listprocess=`ps -edf | grep resque`;
	print $listprocess;
}
if($debug) {print "Check Etat du Quota : test ecriture ftp...\n";}
my $VerifQuota=`/usr/lib/nagios/plugins/check_ftp_rw.pl --host app-ftp17.sen.centre-serveur.i2 --write`;
if($VerifQuota =~ /QUOTA OK/) {
    $Message3="Etat du QUOTA OK";
}else{
    $Message3="QUOTA CRITIQUE Ecriture bloquee".chomp($VerifQuota);
    $CodeSortie=2;
}
if($debug) {print "$Message3\n";}
#Verif partage samba
if($debug) {print "Check partage Samba...\n";}
my $sess=`smbstatus -S 2>/dev/null| grep GB_ | wc -l`;
chomp($sess);
#my $EtatPartage=`sudo smbclient -L localhost -P 2>/dev/null`;
my $EtatPartage=`/usr/lib/nagios/plugins/check_samba.pl -s $IP 2>/dev/null`;
if($debug) {print "Resultat partage samba :\n $EtatPartage \n";}
if($EtatPartage =~ /GB_ADM/ and $EtatPartage =~ /GB_PROD/ ) {
	if($EtatPartage =~ /GB_CONS/ and $EtatPartage =~ /GB_REF/) {
    		$Message4="Partage Samba OK ($sess sessions)";
            #$CodeSortie=0;
	}else{
    		$Message4="Partage Samba incomplet(GB_CONS ou GB_REF inexistant) ($sess sessions)";
    		if($CodeSortie eq "0" ) {$CodeSortie=1;}
	}
}else{
    $Message4="Partage Samba CRITIQUE (GB_ADM ou GB_PROD inexistant) ($sess sessions)";
    $CodeSortie=2;
}


if($debug) {print "$EtatPartage\n";}
if($debug) {print "$Message4\n";}
#print "\nRESULTAT1: $test\n";
$Message="Diagnostic GeoIdeBase OK - Aucune erreur $Message2 $Message3 $Message4 Version:$version";
if($CodeSortie eq "2") {
    $Message="EOLE-GEO-C003 Diagnostic GeoIdeBase CRITIQUE : $NbErr erreur(s) detectee(s) $Message2 $Message3 $Message4 Version:$version\n$LibErreur";
}
if($CodeSortie eq "1") {
    $Message="EOLE-GEO-W003 Diagnostic GeoIdeBase DEGRADE : $NbErr erreur(s) detectee(s) $Message2 $Message3 $Message4 Version:$version\n$LibErreur";
}
print $Message."\n";

if($debug) {
    #$Recup=`less /usr/lib/nagios/plugins/DiagGeoIde.log`;
    #print "\n--- Resultat Page Diagnostics ---\n";
    #print $Recup;
}
exit($CodeSortie);
