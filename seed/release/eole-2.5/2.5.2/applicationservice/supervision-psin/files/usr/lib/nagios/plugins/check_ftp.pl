#!/usr/bin/perl -w
#
# check FTP connections
#
# Copyright (c) 2005 Holger Weiss <holger@CIS.FU-Berlin.DE>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

use strict;
use Socket;
use Getopt::Long;
use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS $TIMEOUT &print_revision &support);
use vars qw($PROGNAME $PORT $CRIT $WARN $opt_H $opt_P $opt_V $opt_c $opt_h
            $opt_p $opt_t $opt_u $opt_v $opt_w);

sub talk_ftp ($$$$$$$);
sub die_crit ($);
sub die_warn ($);
sub die_unknown ($);
sub print_usage ();
sub print_help ();
my ($ftp_port, $warning, $critical, $timeout, $response);

$PROGNAME = "check_ftp";
$PORT = 21;
$WARN = $TIMEOUT;
$CRIT = $TIMEOUT;
$SIG{'ALRM'} = sub { die_unknown("Timeout"); };
$ENV{'PATH'}='';
$ENV{'ENV'}='';

Getopt::Long::Configure("bundling");
if (!GetOptions("V"   => \$opt_V, "version"    => \$opt_V,
                "h"   => \$opt_h, "help"       => \$opt_h,
                "v+"  => \$opt_v, "verbose+"   => \$opt_v,
                "H=s" => \$opt_H, "hostname=s" => \$opt_H,
                "c=i" => \$opt_c, "critical=i" => \$opt_c,
                "P=i" => \$opt_P, "port=i"     => \$opt_P,
                "p=s" => \$opt_p, "password=s" => \$opt_p,
                "t=i" => \$opt_t, "timeout=i"  => \$opt_t,
                "u=s" => \$opt_u, "username=s" => \$opt_u,
                "w=i" => \$opt_w, "warning=i"  => \$opt_w)) {
	print "FTP UNKNOWN - Error processing command line options\n";
	print_usage();
	exit $ERRORS{'UNKNOWN'};
}
if ($opt_V) {
	print_revision($PROGNAME,'$Revision: 1.4 $ ');
	exit $ERRORS{'OK'};
}
if ($opt_h) {
	print_help();
	exit $ERRORS{'OK'};
}
unless ($opt_H) {
	print "FTP UNKNOWN - No target host specified\n";
	print_usage();
	exit $ERRORS{'UNKNOWN'};
}
$critical = $opt_c ? $opt_c : $CRIT;
$warning = $opt_w ? $opt_w : $critical;
if ($warning > $critical) {
	print "FTP UNKNOWN - Warning larger than critical threshold\n";
	print_usage();
	exit $ERRORS{'UNKNOWN'};
}
$ftp_port = $opt_P ? $opt_P : getservbyname("ftp", "tcp");
$ftp_port = $PORT unless defined $ftp_port;
$timeout = $opt_t ? $opt_t : $TIMEOUT;
alarm($timeout);
$response = talk_ftp($opt_H, $ftp_port, $warning, $critical, $opt_v, $opt_u, $opt_p);
print "FTP OK - $response\n";
exit $ERRORS{'OK'};

sub talk_ftp ($$$$$$$) {
	my ($host, $port, $warn, $crit, $verbose, $user, $pass) = @_;
	my ($buffer, $duration, $iaddr, $paddr, $start, $str);

	$iaddr = inet_aton($host)
	    || die_unknown("Invalid hostname/address: $host");
	$paddr = sockaddr_in($port, $iaddr);
	$SIG{'ALRM'} = sub { die_crit("Socket timeout"); };
	socket(SOCK, PF_INET, SOCK_STREAM, getprotobyname('tcp'))
	    || die_unknown("Socket error: $!");
	$start = time;
	connect(SOCK, $paddr) || die_crit("Error connecting to $host: $!");
	do {
		if (($buffer = <SOCK>)) {
			$buffer =~ s/[\r\n]//g;
		} else {
			die_crit("Unexpected EOF from server");
		}
	} until ($buffer =~ /^[2-9]\d\d /);
	if ($buffer !~ /^220/) {
		my $error = "Protocol error during welcome";

		$error .= " - Server said: $buffer" if $buffer;
		die_crit($error);
	}
	($str = $buffer) =~ s/\d\d\d // unless length $buffer < 5;
	if ($user) {
		defined(send(SOCK, "user $user\r\n", 0))
		    || die_unknown("Error sending user request to $host: $!");
		do {
			($buffer = <SOCK>) =~ s/[\r\n]//g;
		} until ($buffer =~ /^[2-9]\d\d /);
		if ($buffer !~ /^331/) {
			my $error = "User not accepted";

			$error .= " - Server said: $buffer" if $buffer;
			die_warn($error);
		}
	}
	if ($pass) {
		defined(send(SOCK, "pass $pass\r\n", 0))
		    || die_unknown("Error sending pass request to $host: $!");
		do {
			($buffer = <SOCK>) =~ s/[\r\n]//g;
		} until ($buffer =~ /^[2-9]\d\d /);
		if ($buffer !~ /^230/) {
			my $error = "Login failed";

			$error .= " - Server said: $buffer" if $buffer;
			die_warn($error);
		}
		($str = $buffer) =~ s/\d\d\d // unless length $buffer < 5;
	}
	defined(send(SOCK, "quit\r\n", 0))
	    || die_unknown("Error sending quit request to $host: $!");
	do {
		($buffer = <SOCK>) =~ s/[\r\n]//g;
	} until ($buffer =~ /^[2-9]\d\d /);
	if ($buffer !~ /^221/) {
		my $error = "Protocol error during quit";

		$error .= " - Server said: $buffer" if $buffer;
		die_crit($error);
	}
	close SOCK || die_unknown("Cannot close connection: $!");
	$SIG{'ALRM'} = sub { die_unknown("Timeout"); };
	$duration = time - $start;
	die_crit("$duration sec. response time") if $duration >= $crit;
	die_warn("$duration sec. response time") if $duration >= $warn;
	$str .= " - $duration sec. response time" if $verbose;
	return $str;
}

sub die_unknown ($) {
	printf "FTP UNKNOWN - %s\n", shift;
	exit $ERRORS{'UNKNOWN'};
}

sub die_warn ($) {
	printf "FTP WARNING - %s\n", shift;
	exit $ERRORS{'WARNING'};
}

sub die_crit ($) {
	printf "FTP CRITICAL - %s\n", shift;
	exit $ERRORS{'CRITICAL'};
}

sub print_usage () {
	print "Usage: $PROGNAME -H host [-P port] [-w warn] [-c crit] " .
	      "[-t timeout]\n                       " .
	      "[-u username] [-p password] [-v]\n";
}

sub print_help () {
	print_revision($PROGNAME, '$Revision: 1.4 $');
	print "Copyright (c) 2005 Holger Weiss\n\n";
	print "Check FTP connections with the specified host.\n\n";
	print_usage();
	print <<EOF;

 -H, --hostname=ADDRESS
    Host name or IP Address
 -P, --port=INTEGER
    Port number (default: $PORT)
 -w, --warning=INTEGER
    Response time to result in warning status (seconds)
 -c, --critical=INTEGER
    Response time to result in critical status (seconds)
 -t, --timeout=INTEGER
    Seconds before connection times out (default: $TIMEOUT)
 -u, --username=STRING
    Username for FTP authentication
 -p, --password=STRING
    Password for FTP authentication
 -v, --verbose
    Show details for command-line debugging (Nagios may truncate output)

EOF
	support();
}
