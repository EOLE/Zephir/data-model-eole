#!/bin/bash
#
# script de sauvegarde élémentaire de la configuration firewall
# peut être appelé avec un argument $1 prenant O/N pour la cron
saveFile=$(CreoleGet type_amon)
saveDir="/var/saveFirewall"
saveDate=$(date +%d-%m-%Y-%Hh%M)
localFile="/usr/share/era/modeles/${saveFile}"
delay="0"
argument=${1}

if [ ! -d ${saveDir} ]
then
    mkdir ${saveDir}
fi
echo "Sauvegarde du fichier courant sous ${saveDir}/${saveFile}.${saveDate}.gz"
save=$(tar czf ${saveDir}/${saveFile}.${saveDate}.gz ${localFile}.xml)
if [ ! "${?}" == "0" ]
then
    echo "Erreur d'écriture sous ${saveFile}"
    echo "Vérifiez l'espace disque"
    exit 1
fi

if [ "${argument}" == "N" ] || [ "${argument}" == "n" ]
then
	exit 0
else
    read -p "Voulez-vous sauvegarder sur Zephir (o/n): " rep
    if [ "$rep" == "o" ] || [ "$rep" == "O" ]
    then
        echo "Envoi du fichier courant au zephir"
        envoi=$(/usr/share/zephir/scripts/zephir_client save_files)
        if [ "${?}" == "0" ]
        then
            echo "Terminé"
        else
            echo "Erreur de communication Zephir !"
        fi
    fi
fi
