#!/usr/bin/perl

#use strict;

# Nagios specific

use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS $TIMEOUT);
my $vers=`uname -r`;
if($vers =~ /2.6.24/) {
        print "Version trop ancienne - Pas de verification\n";
        exit 1;
}

my $Param1="";
if( -e "/usr/lib/nagios/plugins/ParamCheck.pl") {
            require "/usr/lib/nagios/plugins/ParamCheck.pl";
            #print "$ParamRepLdap ";
            $Param1=$ParamRepLdap;
}
#print $Param1;
my $droits=`sudo chmod 777 /tmp/process.log`;
#my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);
my $CodeSortie="0";
# verification fraicheur fichier ldif et taille >0
my $ficldap="/var/tmp/ldap.ldif";
if (-e  "/home/data/tmp/ldap.ldif") {
     $ficldap="/home/data/tmp/ldap.ldif";
}
my $fraicheur=`/usr/lib/nagios/plugins/check_file_age -f $ficldap -w 5400 -c 6000 -C 1000`;

    chomp($fraicheur);
if($fraicheur =~ /OK/) {
	$CodeSortie="0";
}elsif($fraicheur =~ /WARNING/) {
	$CodeSortie="1";
}else{
	$CodeSortie="2";
}

#Verification integrite du gfichier
my $message="";
my $resu="";
my $domaine=`grep "workgroup =" /etc/samba/smb.conf`;
($resu) = $domaine =~ /workgroup \= (.*)$/;
#print "domaine trouvé ".$resu;
if ( !defined($resu) ) {
        $message="EOLE-LDAP-C002 ERREUR : Domaine introuvable dans configuration Samba\n";
        $CodeSortie="2";
        #exit $ERRORS{"CRITICAL"};
}
my $o_domaine=$resu;
my $MessageI="";
my $process="";
my $creefic="";
my $flag="0",
my $i=0;
my $entree="NC";
while ( $i < 60 ) {
	$process=`ps -edf > /tmp/process.log`;
	if( `egrep /usr/share/ecdl/bin/secours_ldap /tmp/process.log`) {
		$i++;
	}else{
	   $flag="1";
	   $i=60;
	}
}
$process=`uname -a`;
if($process =~ /2.6.24/) {
        $flag="0";
}
my $fic="/data/tmp/bj";
if(! -e $fic) {
    $creefic=`sudo echo 145969 >/usr/lib/nagios/plugins/bj`;
    $creefic=`sudo cp -f /usr/lib/nagios/plugins/bj /data/tmp/bj`;
	$flag="0";
}
if($Param1 eq "No") {
	$flag="0";
}

if($flag eq "1") {
	my $Integr=`cat $ficldap| openssl enc -d -aes256 -kfile /data/tmp/bj | grep -i $o_domaine`;
	if($Integr =~ /$o_domaine/i) {
		$MessageI="Integrite OK";
	}else{
		$MessageI="EOLE-LDAP-C005 ERREUR:Le fichier LDIF ne correspond pas au domaine $o_domaine";
		$CodeSortie="2";
	}

	#recup nombre entrees
	$entree=`cat $ficldap| openssl enc -d -aes256 -kfile /data/tmp/bj | grep -c uidNumber`;
	chomp($entree);
}else{
	$MessageI=" Integrite non verifiee. Secours_ldap en cours";
}
if($CodeSortie eq "0") {
	print "REPLICATION LDAP OK $fraicheur $MessageI Nombre UID domaine $o_domaine : $entree\n";
	exit 0;
}elsif($CodeSortie eq "1") {
	print "EOLE-LDAP-W006 REPLICATION LDAP DEGRADE $fraicheur $MessageI Nombre UID domaine $o_domaine : $entree\n";
	exit 1;
}else{
	print "EOLE-LDAP-C006 REPLICATION LDAP CRITIQUE $fraicheur $message $MessageI Nombre UID domaine $o_domaine : $entree\n";
	exit 2;
}
exit 0;

