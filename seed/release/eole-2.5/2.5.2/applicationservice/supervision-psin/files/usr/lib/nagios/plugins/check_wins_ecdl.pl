#!/usr/bin/perl -w
####################### check_wins_ecdl.pl #######################
# Version : 1.00
# Date : 31 Mars 2012
# rajout log
# Author  : PSIN P.Malossane
#############################################################
#
#Verifie une interrogation wins et recupere le masterbroser
#

use strict;
use Getopt::Long;

# Nagios specific

use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS $TIMEOUT);
use Time::HiRes 'time','sleep';
#my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);
use POSIX qw(strftime);

# Globals
my $debug="0";
if (defined $ARGV[0]) { $debug = "-v";}

#recuperation du domaine et du serveur wins dans smb.conf


my $critical=20;
my $warning=15;
my $starttimer;
my $endtimer;
my $message;

#Time out commande ldapsearch
my $TimeOut=60;
my $datelog = strftime "%d-%m-%Y %H:%M:%S", localtime;
my $heurelog = strftime "%H:%M", localtime;
#my $minute_aleatoire = int(rand(10));
my $serveur_wins = "";
my $ServeurWins ="";
my $ServeurWins1 ="";
my $ServeurWins2 ="";
my $ServeurWins3 ="";


my $CodeSortie=0;
my $resu;
my $latency = 0;
my $MasterBroser;
my $MasterBroserName="";
if($debug eq "-v") { print "Recherche du domaine ...";}
#recup domaine dans $o_domaine et force les elments recherches
my $domaine=`grep "workgroup =" /etc/samba/smb.conf`;
($resu) = $domaine =~ /workgroup \= (.*)$/;
#print "domaine trouvé ".$resu;
if($debug eq "-v") { print " domaine trouve : $domaine \n";}
if ( !defined($resu) ) {
        $message="EOLE-LDAP-C002 ERREUR RECHERCHE DOMAINE LDAP : Domaine introuvable dans configuration Samba\n";
	print $message;
        $CodeSortie=2;
        exit 2;
}
$domaine=$resu;

if($debug eq "-v") { print "Recherche des serveurs WINS ...";}
$serveur_wins=`grep "wins server =" /etc/samba/smb.conf`;
if ($serveur_wins =~ /\#/) {
        $message="EOLE-SR-C004 CHECK WINS : Serveur WINS non Actif dans la configuration Samba\n";
	print $message;
        $CodeSortie=2;
        exit 0;
}
chomp($serveur_wins);
$serveur_wins =~ s/wins server \= /winsserver=/g;
$serveur_wins =~ s/wins server \=/winsserver=/g;
$serveur_wins =~ s/wins server\=/winsserver=/g;
$serveur_wins =~ s/wins server\= /winsserver=/g;
($resu) = $serveur_wins =~ /winsserver\=(.*)/;
#print "serveur wins ".$serveur_wins;

if ( !defined($resu) ) {
        $message="EOLE-SR-C004 CHECK WINS : Aucun serveur WINS declare dans la configuration Samba\n";
	print $message;
        $CodeSortie=2;
        exit 0;
}
if($resu =~ /1/) {
	$serveur_wins=$resu;
}else{
        $message="EOLE-SR-C004 CHECK WINS : Aucun serveur WINS declare dans la configuration Samba\n";
	print $message;
        $CodeSortie=0;
        exit 0;
}


if($debug eq "-v") { print " Serveurs trouve : $serveur_wins\n";}
if(length($serveur_wins) > 15) {
	$serveur_wins=~ s/;//g;
	my @ServeursWins = split(' ',$serveur_wins);
    #my $NbWins=scalar(keys %ServeursWins);
    #print $NbWins;
	$ServeurWins1=$ServeursWins[0];
	$ServeurWins2=$ServeursWins[1];
	$ServeurWins3=$ServeursWins[2];
}else{
	$ServeurWins1=$serveur_wins;
	$ServeurWins2="";
	$ServeurWins3="";
}

my $recup;
my $recupMB;
my $recupMB2;
my $recupMBN;
my $flag_err1="0";
my $flag_err="0";
my $flag_warn1="0";
my $flag_err2="0";
my $flag_err3="0";
my $flag_warn="0";
my $flag_warn2="0";
my $flag_warn3="0";
 $starttimer = time();

eval {
    local $SIG{ALRM} = sub { die "timeout" };
    alarm $TimeOut;
    # test avec le premier WINS
    if($debug eq "-v") {
        # test du serveur wins
      	print "WINS 1 : ".$ServeurWins1."\n";
	}
    for (my $i = 0; $i <= 5; $i++) {
 	    $starttimer = time();
        if($debug eq "-v") { print " Interrogation $i/6 serveur wins $ServeurWins1 ...\n";}
        #if($debug eq "-v") {$recup=`nmblookup -U $ServeurWins1 -R '$domaine' -d 2`;}
		if(($i eq "1")||($i eq "3")||($i eq "5")) {
            if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins1 -R '$domaine' -d 2\n";}
		    $recup=`nmblookup -U $ServeurWins1 -R '$domaine' -d 2`;
		}else{
		    if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins1 -R '$domaine#1B' -d 2\n";}
		    $recup=`nmblookup -U $ServeurWins1 -R '$domaine#1B' -d 2`;
	    }
		if($debug eq "-v") { print " Retour :$recup\n";}
        if($debug eq "-v") { print " On cherche positive et $domaine\n";}
        if ($recup =~ /positive/ and $recup =~ /$domaine/) {
	        if($debug eq "-v") { print "i=".$i."\n"; }
    	    $i=6;
	        $flag_err1="0";
			#break;
        }else{
            $flag_err1="1";
        }
        sleep(2);
	if($debug eq "-v") { print "Flag_err1 : ".$flag_err1."\n";}
    }

    if($flag_err1 eq "1"){
        $flag_err="1";
	$ServeurWins=$ServeurWins1. " CRITIQUE";
    }else{
	$ServeurWins=$ServeurWins1. " OK";
	}
    if($debug eq "-v") { print " **** Etat avant ServeurWins 2 =$ServeurWins\n";}

    # test avec le deuxieme WINS
    if (!defined $ServeurWins2) {$ServeurWins2="";}
	if($ServeurWins2 ne "") {
        if($debug eq "-v") {
            # test du serveur wins
            print "WINS 2 : ".$ServeurWins2."\n";
        }
        for (my $i = 0; $i <= 5; $i++) {
            if($debug eq "-v") { print " Interrogation $i/6 serveur wins $ServeurWins2 ...\n";}
            if($debug eq "-v") {$recup=`nmblookup -U $ServeurWins2-R '$domaine#1B' -d 2`;}
            if($debug eq "-v") { print " Retour :$recup\n";}
            if($debug eq "-v") { print " On cherche positive et $domaine\n";}
            if(($i eq "1")||($i eq "3")||($i eq "5")) {
                if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins2 -R '$domaine' -d 2\n";}
		        $recup=`nmblookup -U $ServeurWins2 -R '$domaine' -d 2`;
            }else{
		        if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins2 -R '$domaine#1B' -d 2\n";}
                $recup=`nmblookup -U $ServeurWins2 -R '$domaine#1B' -d 2`;
            }
			if ($recup =~ /positive/ and $recup =~ /$domaine/) {
			    if($debug eq "-v") { print "i=".$i."\n";}
                $i=6;
                $flag_err2="0";
				#break;
			}else{
                $flag_err2="1";
            }
	    sleep(2);
	    if($debug eq "-v") { print "Flag_err2 : ".$flag_err2."\n";}
	}

        if($flag_err2 eq "1"){
            $flag_err="1";
            $ServeurWins=$ServeurWins." ".$ServeurWins2. " CRITIQUE";
        }else{
		$ServeurWins=$ServeurWins." ".$ServeurWins2. " OK";
	}
    }

    if($debug eq "-v") { print " **** Etat avant ServeurWins 3 =$ServeurWins\n";}

    # test avec le troisieme WINS
    if (!defined $ServeurWins3) {$ServeurWins3="";}
	if($ServeurWins3 ne "") {
        if($debug eq "-v") {
            # test du serveur wins
            print "WINS 3 : ".$ServeurWins3."\n";
        }
        for (my $i = 0; $i <= 5; $i++) {
            if($debug eq "-v") { print " Interrogation $i/6 serveur wins $ServeurWins3 ...\n";}
            if($debug eq "-v") {$recup=`nmblookup -U $ServeurWins3 -R '$domaine#1B' -d 2`;}
            if($debug eq "-v") { print " Retour :$recup\n";}
            if($debug eq "-v") { print " On cherche positive et $domaine\n";}
            if(($i eq "1")||($i eq "3")||($i eq "5")) {
		        if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins3 -R '$domaine' -d 2\n";}
                $recup=`nmblookup -U $ServeurWins3 -R '$domaine' -d 2`;
            }else{
		        if($debug eq "-v") {print "Commande : nmblookup -U $ServeurWins3 -R '$domaine#1B' -d 2\n";}
                $recup=`nmblookup -U $ServeurWins3 -R '$domaine#1B' -d 2`;
            }
            if($debug eq "-v") { print " Retour :$recup\n";}
			if ($recup =~ /positive/ and $recup =~ /$domaine/) {
			    if($debug eq "-v") { print "i=".$i."\n";}
				$i=6;
                $flag_err3="0";
				#break;
			}else{
                $flag_err3="1";
            }
	    sleep(2);
            if($debug eq "-v") { print "Flag_err2 : ".$flag_err2."\n";}
	}

        if($flag_err3 eq "1"){
            $flag_err="1";
            $ServeurWins=$ServeurWins." ".$ServeurWins3. " CRITIQUE";
        }else{
	        $ServeurWins=$ServeurWins." ".$ServeurWins3. " OK";
	}
    }
    if($debug eq "-v") { print " **** Etat a la fin des trois tests =$ServeurWins\n";}

};


#ecrit_log("ldap=$ldap");

if ($@) {
		$message=" EOLE-SR-C005 *** INTERROGATION WINS TIMEOUT $TimeOut s ***";
        	my $arr=`/usr/bin/killall -9 nmblookup`;
		print $message;
		exit 0;
		#exit 2;
}
alarm 0;
$endtimer = time();
$latency = (int(1000 * ($endtimer - $starttimer)) / 1000);

$MasterBroserName="INCONNU";
#if ($recup =~ /positive/ and $recup =~ /$domaine\<00\>/) {
if ($flag_err eq "0") {
        #verifi mot cle
       #recup masterbroserdd
	$recupMB=`nmblookup -U $ServeurWins1 -R '$domaine#1B'`;
	 #print $recupMB;
	if ($recupMB =~ /$domaine\<1b\>/) {
		($MasterBroser) = $recupMB =~ /(.*) $domaine\<1b\>/;
		$recupMBN=`nmblookup -A $MasterBroser`;
		#print "\nrecupMBN=$recupMBN";
		if($recupMBN =~ /\<00\>/) {
			($MasterBroserName) = $recupMBN =~ /(.*) \<00\>/;
			$MasterBroserName =~ s/\s+//g;
		}elsif($recupMBN =~ /\<1d\>/) {
			($MasterBroserName) = $recupMBN =~ /(.*) \<1d\>/;
			$MasterBroserName =~ s/\s+//g;
		}else{
			$MasterBroserName="INCONNU";
		}
	}else{
		$recupMB2=`nmblookup -U $ServeurWins2 -R '$domaine#1B'`;
		if ($recupMB2 =~ /$domaine\<1b\>/) {
			($MasterBroser) = $recupMB2 =~ /(.*) $domaine\<1b\>/;
			$recupMBN=`nmblookup -A $MasterBroser`;
			if($recupMBN =~ /\<00\>/) {
				($MasterBroserName) = $recupMBN =~ /(.*) \<00\>/;
				$MasterBroserName =~ s/\s+//g;
			}else{
				$MasterBroserName="INCONNU";
			}
		}else{
			$MasterBroserName = "INCONNU";
			$MasterBroser="";
		}
	}


    if ($latency > $critical) {
         $message="EOLE-SR-C006 CHECK WINS $ServeurWins ($domaine) mais Temps CRITIQUE  $latency s > $critical s MasterBrowser:$MasterBroser ($MasterBroserName) \n";
         $CodeSortie=2;
             #exit $ERRORS{"CRITICAL"};
    }elsif ($latency > $warning) {
         $message="EOLE-SR-W006 CHECK WINS $ServeurWins ($domaine) mais Temps degrade  $latency s > $warning s MasterBrowser:$MasterBroser  ($MasterBroserName)\n";
         $CodeSortie=1;
    }else{
         $message="CHECK WINS $ServeurWins ($domaine) en $latency s MasterBrowser:$MasterBroser  ($MasterBroserName)\n";
	     if($flag_warn eq "1") {
             	$message="EOLE-SR-C006 CHECK WINS $ServeurWins CRITIQUE MAIS MASTERBROWSER OK (broadcast)  ($domaine) en $latency s MasterBrowser:$MasterBroser  ($MasterBroserName)\n";
        		$CodeSortie=1;
	     }else{
             	$message="CHECK WINS $ServeurWins ($domaine) en $latency s MasterBrowser:$MasterBroser  ($MasterBroserName)\n";
             	$CodeSortie=0;
             	#exit $ERRORS{"OK"};
       	}
	}
}else{

        $message="EOLE-SR-C007 CHECK WINS CRITIQUE ($ServeurWins) domaine $domaine \n";
	$CodeSortie=0;
        #$CodeSortie=2;
}


print $message;
exit 0;
#exit $CodeSortie;
