#! /usr/bin/perl -w
#param base de donn�es oracle
#require ("/usr/local/nagios/libexec/paramDB.pl") || die "Impossible d'ouvrir le fichier de configuration paramDB.pl";
#use strict;
#use Net::SNMP qw(:snmp oid_lex_sort);
use lib "/usr/lib/nagios/plugins";
use utils qw($TIMEOUT %ERRORS );
use Getopt::Long;
use vars qw($opt_h $opt_v $opt_s );


sub print_help ();

#print "verif option\n";
GetOptions
    ("h"   => \$opt_h, "--help"         => \$opt_h,
     "s=s"   => \$opt_s, "--serveur=s" => \$opt_s,
     "v" => \$opt_v, "--verbose"       => \$opt_v);

#print "verif option\n";
if (!$opt_s) {
	print "Usage: check_surf.pl -s IP_SERVEUR [-v verbose]\n";
	exit 0;
}
if ($opt_h) {
	print "Usage: check_surf.pl -s IP_SERVEUR [-v verbose]\n";
	exit 0;
}


if($opt_v) {print "Verification serveur ".$opt_s."\n";}
my $CodeSortie="0";
my $Etat1="Navigation Profil Standard CRITIQUE";
my $Etat2="Profil VIP CRITIQUE (pas acces)";
my $Etat3="Profil NON VIP CRITIQUE (Acces autorise)";
my $Etat4="Profil Interdit CRITIQUE (Acces autorise)";
my $Etat5="SURF PRO CRITIQUE ";

#test acces standard
if($opt_v) {print "Verification profil Standard\n";}
my $verif=`ssh -o ConnectTimeout=15 -i /usr/local/nagios/.ssh/id_rsa_eole $opt_s "/usr/lib/nagios/plugins/check_surf_auth.pl  -u www.google.fr -x localhost:3128 -A local.psin-standard -p PsinSurf#"`;
if($opt_v) {print $verif;} 
if($verif =~ /SURF OK/) {
	$Etat1="Navigation Profil Standard OK";
}else{
	$CodeSortie="2";
}

if($opt_v) {print "Verification profil VIP : acces a youtube\n";}
$verif=`ssh -o ConnectTimeout=15 -i /usr/local/nagios/.ssh/id_rsa_eole $opt_s "/usr/lib/nagios/plugins/check_surf_auth.pl -u www.youtube.com -x localhost:3128 -A local.psin-vip -p PsinSurf# -o vip"`;
if($opt_v) {print $verif;} 
if($verif =~ /SURF OK/) {
	$Etat2=" Profil VIP OK";
}else{
        $CodeSortie="2";
}

if($opt_v) {print "Verification profil NON VIP : acces a youtube interdit\n";}
$verif=`ssh -o ConnectTimeout=15 -i /usr/local/nagios/.ssh/id_rsa_eole $opt_s "/usr/lib/nagios/plugins/check_surf_auth.pl -u www.youtube.com -x localhost:3128 -A local.psin-standard -p PsinSurf# -o novip"`;
if($opt_v) {print $verif;} 
if($verif =~ /SURF OK/) {
	$Etat3=" Profil NO VIP OK (access refuse)";
}else{
        $CodeSortie="2";
}


if($opt_v) {print "Verification Profil Interdit\n";}
$verif=`ssh -o ConnectTimeout=15 -i /usr/local/nagios/.ssh/id_rsa_eole $opt_s "/usr/lib/nagios/plugins/check_surf_auth.pl -u www.google.fr -x localhost:3128 -A local.psin-interdit -p PsinSurf# -o interdit"`;
if($opt_v) {print $verif;} 
if($verif =~ /SURF OK/) {
	$Etat4=" Profil INTERDIT OK (refuse)";
}else{
        $CodeSortie="2";
}

if($opt_v) {print "Verification Acces surf PRO : acces portail ministere\n";}
$verif=`ssh -o ConnectTimeout=15 -i /usr/local/nagios/.ssh/id_rsa_eole $opt_s "/usr/lib/nagios/plugins/check_surf_auth.pl -u www.developpement-durable.gouv.fr -x localhost:3128 -A local.psin-standard -p PsinSurf# -e 'durable'"`;
if($opt_v) {print $verif;} 
if($verif =~ /SURF OK/) {
	$Etat5=" Navigation SURF PRO OK";
}else{
        $CodeSortie="2";
}
if($CodeSortie eq "0") {
	print "SURF OK $Etat1 $Etat2 $Etat3 $Etat4 $Etat5\n";
	exit 0;
}else{
	print "SURF CRITIQUE $Etat1 $Etat2 $Etat3 $Etat4 $Etat5\n";
        exit 2;
}
exit 0;
