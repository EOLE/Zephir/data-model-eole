#!/usr/bin/perl -w
#

# Plugin directory / home of utils.pm.
use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS &print_revision &support &usage);
use Getopt::Long qw(:config no_ignore_case bundling);
use File::Basename;
use Net::DNS;

use strict;

# Path to installed clamd binary.
my $iptable_cmd  = "sudo iptables";
my $warn_val = 80;  # Default -w arg
my $crit_val = 100;  # Default  -c arg
my $help_val = 0;  # Off unless -h arg
my $debug_val = 0;  # Off unless -h arg
my $chaine = "mor-lan";
my $indicateur = "DROP ";


sub show_help() {
    print <<END;

Usage: check_iptables_state [-C <chaine>] [ -S <indicateur>] [-w <warn>] [-c <crit>]  [-h]

-w, --warning=INTEGER
-c, --critical=INTEGER
-I, --Indicateur a rechercher DROP par defaut
-C, --chaine iptable (mor-lan par defaut)
   Chaine dans laquelle recuperer les statistiques
-h, --help
-v, --verbose
END
}

GetOptions (
    "w=i" => \$warn_val, "warning=i" => \$warn_val,
    "c=i" => \$crit_val, "critical=i" => \$crit_val,
    "h" => \$help_val, "help" => \$help_val,
    "v" => \$debug_val, "verbose" => \$debug_val,
    "C=s" => \$chaine, "chaine=s" => \$chaine,
    "I=s" => \$indicateur, "indicateur=s" => \$indicateur
);

if ($help_val != 0) {
    &show_help;
    exit 0;
}

my ($total,$diff,$update_time,$row, @last_values, $last_time,$last_DropIntOut, $last_DropIntIn);
my ($Tindic1,$Tindic2);


#recup nombre de drop internet out et in
if ($debug_val != 0) { print $iptable_cmd." -nvL mor-lan -x |grep DROP_par_mor-lan |awk -F' ' '{print \$1}'";}
chomp(my $DropIntIn = `$iptable_cmd -nvL |grep DROP_par_ext-mor |awk -F' ' '{print \$1}'`);
chomp(my $DropIntOut = `$iptable_cmd -nvL | grep DROP_par_mor-bas |awk -F' ' '{print \$1}'`);

if ($DropIntIn eq ""){$DropIntIn=0;}
if ($DropIntOut eq ""){$DropIntOut=0;}

if ($debug_val != 0) { print " Nb DROP Out/In : ".$DropIntOut."/".$DropIntIn;}

my $flg_created = 0;

if (-e "/tmp/drop_iptable.log") {
    open(FILE,"<"."/tmp/drop_iptable.log");
    while($row = <FILE>){
       @last_values = split(":",$row);
       $last_time = $last_values[0];
       $last_DropIntOut = $last_values[1];
       $last_DropIntIn = $last_values[2];
       $flg_created = 1;
    }
    close(FILE);
} else {
    $flg_created = 0;
}
$update_time = time();
unless (open(FILE,">"."/tmp/drop_iptable.log")){
    print "Check mod for temporary file : /tmp/drop_iptable.log !\n";
   exit $ERRORS{"UNKNOWN"};
}
print FILE "$update_time:$DropIntOut:$DropIntIn";
close(FILE);
if ($flg_created == 0){
   print "First execution : Buffer in creation.... \n";
   exit($ERRORS{"UNKNOWN"});
}
#calcul intervalle de temps
$diff = (time() - $last_time)/60;  #en minute
if ($diff == 0){$diff = 1;}

if (($DropIntOut - $last_DropIntOut != 0) && defined($last_DropIntOut)) {
	$Tindic1 = 0;
	if ($DropIntOut - $last_DropIntOut < 0){
	   $total = $DropIntOut;
	} else {
	   $total = $DropIntOut - $last_DropIntOut;
	}
	if ($debug_val != 0) {print "total=$total diff=$diff en minutes\n";}
	#my $pct_out_traffic = $Tindic1 = abs($total / $diff);
	$Tindic1 = abs($total / $diff);
	$Tindic1 = int($Tindic1);
} else {
   $Tindic1 = 0;
}
if (($DropIntIn - $last_DropIntIn != 0) && defined($last_DropIntIn)) {
	$Tindic2 = 0;
	if ($DropIntIn - $last_DropIntIn < 0){
	   $total = $DropIntIn;
	} else {
	   $total = $DropIntIn - $last_DropIntIn;
	}
	if ($debug_val != 0) {print "total=$total diff=$diff en minutes\n";}
	#my $pct_out_traffic = $Tindic1 = abs($total / $diff);
	$Tindic2 = abs($total / $diff);
	$Tindic2 = int($Tindic2);
} else {
   $Tindic2 = 0;
}

#Verifications
my $CodeSortie=0;
my $message=" DROP OUT $Tindic1 /min  IN $Tindic2 /min";
my $etat="OK";
if ($Tindic1 > $crit_val){
	$message="EOLE-IPT-C003 Nombre de drop OUT : $Tindic1 /minute > $crit_val";
	$etat="CRITIQUE";
	$CodeSortie=2;
}
if ($Tindic2 > $crit_val){
	$message="EOLE-IPT-C003 Nombre de drop IN : $Tindic2 /minute > $crit_val";
	$etat="CRITIQUE";
	$CodeSortie=2;
}
if ($Tindic1 > $warn_val){
	$message="EOLE-IPT-W003 Nombre de drop OUT : $Tindic1 /minute > $warn_val";
	$etat="DEGRADE";
	$CodeSortie=1;
}
if ($Tindic2 > $warn_val){
	$message="EOLE-IPT-W003 Nombre de drop IN : $Tindic2 /minute > $warn_val";
	$etat="DEGRADE";
	$CodeSortie=1;
}


$message="CHECK DROP IPTABLES Interface RIE ".$etat." ".$message."| nb_drop_out=$Tindic1 nb_drop_in=$Tindic2\n";
print $message;
exit $CodeSortie;
