#!/usr/bin/perl -w
use strict;
use lib "/usr/lib/nagios/plugins" ;
use utils qw($TIMEOUT %ERRORS &print_revision &support);

open INTER, "/usr/bin/wget -q http://80.118.192.105 -O - | cat - |grep adresse | html2text | cut -d' ' -f5 | ";
    $_ = <INTER>;
close INTER;

if (m/212/) {
  #print "CRITICAL - \n";
  print "OK Sortie internet Velizy - ". $_ . "\n";
  exit $ERRORS{'OK'};
}
else {
  print "ATTENTION Sortie internet SECOURS sur Venissieux - " . $_ . "\n";;
  exit $ERRORS{'WARNING'};
}
