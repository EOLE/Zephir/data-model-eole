#!/bin/bash

echo "Mise en place du check ldap statut dans le cron"
cat > /tmp/cron_zzz << EOF
*/2 * * * * /usr/lib/nagios/plugins/check_ldap_statut.pl >/dev/null 2>&1

EOF
sudo crontab -l -u root | grep -v '\(tdbbackup\|check_ldap_statut.pl\)'  >> /tmp/cron_zzz
sort /tmp/cron_zzz| uniq > /tmp/cron_u
sudo crontab -u root /tmp/cron_u

crontab -l -u nagios | grep -v '\(tdbbackup\|check_ldap_statut.pl\)'  > /tmp/cron_zzz
sort /tmp/cron_zzz| uniq > /tmp/cron_u
crontab -u nagios /tmp/cron_u
rm /tmp/cron_zzz /tmp/cron_u
exit 0
