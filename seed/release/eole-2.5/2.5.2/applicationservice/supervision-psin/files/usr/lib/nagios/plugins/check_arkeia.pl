#!/usr/bin/perl
#
$version = "1.2";

$Globalstatus="OK";
#verification installation arkeia
#
#
my $JourLimit=3;
if( -e "/usr/lib/nagios/plugins/ParamCheck.pl") {
        require "/usr/lib/nagios/plugins/ParamCheck.pl";
        #my $JourLimit=$JourLimit2;
        $JourLimit=$JourLimit2;
}
my $TempsLimit=($JourLimit*24*60*60);
#my $TempsLimit=259200;


my $type="";
if ( -e "/opt/arkeia") {
    if ( -e "/opt/arkeia/server/report") {
        $type="serveur";
    }else{
        $type="client";
    }
}else{
    print "ARKEIA Non installee\n";
    exit 0;
}
#print "Type de sauvegarde ".$type;
if($type eq "serveur") {
    TraitServeur();
}else{
    TraitClient();
}

exit;

sub TraitClient {
#-------------------------------------
#Verif process arkeia
#------------------------------------
$resu="";
$perf="";
my $status=`/usr/lib/nagios/plugins/check_procs -a arkwsd -w 1:50 -c 1:60`;
#print $status;
if ($status =~ /OK/) {
       $status4="OK";
      ($resu) = $status =~ /PROCS OK: (.*) process/;
      $libstatus=$libstatus."Process Arkeia OK (".$resu." ) ";
}else {
      $status4="NOT4";
	($resu) = $status =~ /: (.*) process/;
      $Globalstatus="NOK";
      $libstatus=$libstatus."ERREUR Process Arkeia (".$resu." ) ";
}

print $libstatus."|".$perf."\n";
if ($Globalstatus eq "OK") {
        exit($STATUS_CODE{"OK"});
    } elsif ($Globalstatus eq "NOK") {
        exit($STATUS_CODE{"CRITICAL"});
    } else {
        exit($STATUS_CODE{"WARNING"});
    }
exit();
}


sub TraitServeur {
#-------------------------------------
#Verif process arkeia
#------------------------------------

#recup dernier job
$resu="";
my $status=`grep -m 1 JOBID /opt/arkeia/server/report/jobs/bkpmaster.lst`;
$status =~ s/\"//g;
$status =~ s/JOBID//g;
$status =~ s/ //g;
$status =~ s/\t//g;
$status =~ s/\n//g;
my $version=`dpkg -l | grep arkeia `;
chomp($version);
($version_ar) = $version =~ /arkeia (.*) Enter/;
$version_ar =~ s/ //g;

#print $status;
$job=$status;
$resu="";
$perf="";
$flag_lib="1";
my $status=`/usr/lib/nagios/plugins/check_procs -a arkwsd -w 1:50 -c 1:60`;
#print $status;
if ($status =~ /OK/) {
       $status4="OK";
      ($resu) = $status =~ /PROCS OK: (.*) process/;
      $libstatus=$libstatus."Process Arkwsd OK ";
      #verifi arklib si librayry
      $status=`grep -c Library /opt/arkeia/server/report/jobs/bkp$job.lst`;
      if($status =~ /0/) {
            $flag_lib="0";
      }else{
            $status=`/usr/lib/nagios/plugins/check_procs -a arkvlib -w 1:50 -c 1:60`;
             if ($status =~ /OK/) {
                $libstatus=$libstatus." Arklib OK ";
            }else{
                $libstatus=$libstatus." Arklib CRITIQUE ";
                $Globalstatus="NOK";
             }
      }
}else {
      $status4="NOT4";
	($resu) = $status =~ /: (.*) process/;
      $Globalstatus="NOK";
      $libstatus=$libstatus."ERREUR Process Arkeia ";
}

#print "Dernier JOB:".$status;
my $status=`grep -m 1 END_DATE /opt/arkeia/server/report/jobs/bkpmaster.lst`;
$status =~ s/\"//g;
$status =~ s/END_DATE//g;
$status =~ s/\t//g;
use Time::Local;

my $date = '23.10.2011  11:35:00';
my $date = $status;
chomp($date);
#print $date."\n";
my ($year,$mon,$mday,$hour,$min,$sec) = split(/[\s\/:]+/, $date);
my $timeB = timelocal($sec,$min,$hour,$mday,$mon-1,$year);
#print $time,"\n",scalar localtime $time;
#print "Date Dernier JOB:".$timeB;
my $diff=time()-$timeB;
#print " diff=$diff ";
if($diff > $TempsLimit) {
      $libstatus=$libstatus." ERREUR Pas de sauvegarde depuis plus de $JourLimit jours ($date)";
      $Globalstatus="NOK";
}else{
      $libstatus=$libstatus." Derniere sauvegarde $job le $date";
}

# verification tape et driove et verif cahregement librayri ok
$fic="/opt/arkeia/server/report/jobs/bkp".$job.".lst";
if($flag_lib eq "1") {
    #print "fichier:$fic";
    $status=`grep -m 1 I03900004 $fic`;
    ($tape) = $status =~ /tape (.*) in/;
    chomp($tape);
    ($drive) = $status =~ /drive (.*)/;
    chomp($drive);
    #print " tape:$tape drive:$drive\n";

    $status=`grep I03900033 $fic`;
    if($status =~ /successfully loaded/) {
        $libstatus=$libstatus." Chargement librairy $drive OK ($tape)";
    }else{
        $libstatus=$libstatus." ERREUR Chargement librairy $drive ($tape)";
        $Globalstatus="NOK";
    }
}
#verification erreur


$status=`grep errors $fic`;
if($status =~ /errors/) {
    $libstatus=$libstatus." Erreurs sauvegarde detectees  $status";
    $Globalstatus="NOKW";
}


$status=`grep 'End of backup' $fic`;
if($status =~ /End of backup/) {
    $libstatus=$libstatus." Fin OK";
}else{
    $status=`grep 'Fin de la sauvegarde' $fic`;
    if($status =~ /Fin de la sauvegarde/) {
       $libstatus=$libstatus." Fin OK";
    }else{
       $libstatus=$libstatus." Sauvegarde NON TERMINEE";
       $Globalstatus="NOKW";
    }
}

#donnees de perf
#
$status=`grep I02900052 $fic`;
#print $status;
($nbfic) = $status =~ /"(.*)" files/;
($taille) = $status =~ /files, "(.*)" MB,/;
$perf="fic=$nbfic taille=$taille";

if ($Globalstatus eq "OK") {
      $libstatus="Sauvegarde ARKEIA OK - Version $version_ar ".$libstatus;
} elsif ($Globalstatus eq "NOK") {
      $libstatus="Sauvegarde ARKEIA CRITIQUE - Version $version_ar ".$libstatus;
} else {
      $libstatus="Sauvegarde ARKEIA DEGRADEE - Version $version_ar ".$libstatus;
}

print $libstatus."|".$perf."\n";

#print "status=$Globalstatus";
if ($Globalstatus eq "OK") {
	exit 0;
} elsif ($Globalstatus eq "NOK") {
	exit 2;
} else {
	exit 1;
}
exit();

}




