#!/usr/bin/perl
# version 1.1.0 05/2016
# Fabrice REJAUD (prestataire PSIN)
# script execute sur le serveur EOLE


use Sys::Hostname;
use POSIX qw(strftime);
use Term::ANSIColor qw(:constants);
    #print BOLD, GREEN, "This text is in bold blue.\n", RESET;

use Time::HiRes 'time','sleep';
my $date = scalar localtime;
#my $host=`hostname`;
our $sshcmd="ssh -i /usr/local/nagios/.ssh/id_rsa_eole";

our $CodeSortie="0";

our $debug=$ARGV[0];
our $now_string = strftime "%H:%M:%S", localtime;
our $version_plugins_actu="3.1";


#-------------------------------------------- Debut traitement -

#-------------- tronc commun -----------------------------------

#recup version eole
if($debug eq "-v") {
    periode();
    print "-> Recuperation version EOLE...\n";
    print "Cmd : dpkg -l | grep eole-common\n";
}
my $recup=`sudo dpkg -l | grep eole-common`;
our $version_eole="Inconnu";
if($recup =~ /eole16/) {
    $version_eole="2.3.15";
}
elsif($recup =~ /eole15/) {
    $version_eole="2.3.13";
}
elsif($recup =~ /eole10/) {
    $version_eole="2.3.11";
}
elsif($recup =~ /eole99/) {
    $version_eole="2.3.9";
}
else
{
    our $version_eole_tmp=`cat /etc/eole/release`;
    #print $version_eole_tmp;
    ($version_eole) = $version_eole_tmp =~ /EOLE_RELEASE=(.*)/;
}

#id zephir
our $idzephir;
my $recup=`enregistrement_zephir -c`;
($idzephir) = $recup =~ /identifiant : (.*)\)/;

# recup distrib
if($debug eq "-v") {
    periode();
    print "-> Recuperation version distrib...\n";
}
my $recup=`sudo grep DISTRIB_RELEASE /etc/lsb-release`;
our $distrib="";
($distrib) = $recup =~ /DISTRIB_RELEASE=(.*)/;
if($debug eq "-v")
{
    periode();
    print "$distrib\n";
}


#verification version plugin
if($debug eq "-v") {
    periode();
    print "-> Recuperation version plugin...\n";
    print "Cmd : grep Version /usr/lib/nagios/plugins/VersionPlugins.txt\n";
}
our $versplugins = "0.0";
our $versplugins_mess="";
my $test = `grep Version /usr/lib/nagios/plugins/VersionPlugins.txt`;
chomp($test);
our $versplugins_ok=0;
#print "\nRESULTAT1: $test\n";
($versplugins) = $test =~ /Version=(.*)/;
if ($versplugins eq $version_plugins_actu) {
    $versplugins_mess = "Version Plugins OK ".$versplugins;
}else{
    $versplugins_mess = "CRITIQUE : Non a jour ".$versplugins;
    $versplugins_ok=1;
    $CodeSortie=2;
}
if($debug eq "-v")
{
    periode();
    print "$versplugins_mess\n";
}


#recup materiel et vendor
if($debug eq "-v") {
    periode();
    print "-> Recuperation system vendor product...\n";
    print "Cmd : sudo lshw -c system\n";
}
our $materiel="";
our $vendor="";
eval {
    local $SIG{ALRM} = sub { die "timeout" };
    alarm 20;
    $recup=`sudo lshw -c system`;
    ($materiel) = $recup =~ /product: (.*)/;
    if($materiel eq "")
    {
        ($materiel) = $recup =~ /produit: (.*)/;
    }
    if($recup =~ /PowerEdge/) { $vendor="DELL"; }
    elsif($recup =~ /ProLiant/) { $vendor="HP"; }
    else { ($vendor) = $recup =~ /vendor: (.*)/; }
};
alarm 0;

if($@ and $@ =~ /timeout/)
{
    if($debug eq "-v")
    {
         periode();
         print "$vendor - $materiel\n";
    }
}
if($debug eq "-v")
{
    periode();
    print "$vendor - $materiel\n";
}

#recup date install
if($debug eq "-v") {
    periode();
    print "-> Recuperation date install...\n";
    print "Cmd : sudo df / | grep dev | sudo xargs tune2fs -l | grep create\n";
}
my $recup=`sudo df / | grep dev | sudo xargs tune2fs -l | grep create`;
our $install_eole="";
($install_eole) = $recup =~ /Filesystem created:(.*)/;
if($debug eq "-v")
{
    periode();
    print "$install_eole\n";
}

#recup service
our $ups="";
our $squid="";
our $dansguardian="";
our $clamav="";
our $mcafee="";
our $niveau_mcafee="";
our $geoidebase="";
our $geoidedistribution="";
our $service="";

if($debug eq "-v") {
    periode();
    print "-> Detection onduleur...\n";
    print "Cmd : /usr/lib/nagios/plugins/check_onduleur.pl\n";
}
$Service=`/usr/lib/nagios/plugins/check_onduleur.pl`;
our $ups="Oui";
if($Service =~ /ONDULEUR NON DETECTE/) {
    $ups="Non";
}
if($debug eq "-v")
{
    periode();
    print "$ups\n";
}
if($debug eq "-v")
{
    periode();
    print "-> Proc squid...\n";
    print "Cmd : /usr/lib/nagios/plugins/check_procs -C squid\n";
}
$Service=`/usr/lib/nagios/plugins/check_procs -C squid`;
if ($Service =~ /0 processus/) {
    $squid="Non Actif";
}else{
    $squid="Actif";
}
if($debug eq "-v")
{
    periode();
    print "$squid\n";
}
if($debug eq "-v") {
    periode();
    print "-> Proc dansguardian...\n";
    print "Cmd : /usr/lib/nagios/plugins/check_procs -C dansguardian\n";
}
$Service=`/usr/lib/nagios/plugins/check_procs -C dansguardian`;
if ($Service =~ /0 processus/) {
    $dansguardian="Non Actif";
}else{
    $dansguardian="Actif";
}
if($debug eq "-v")
{
    periode();
    print "$dansguardian\n";
}

if($debug eq "-v") {
	periode();
	print "-> Proc clamd...\n";
	print "Cmd : /usr/lib/nagios/plugins/check_procs -C clamd\n";
}
$Service=`/usr/lib/nagios/plugins/check_procs -C clamd`;
if ($Service =~ /0 processus/) {
	$clamav="Non Actif";
}else{
	$clamav="Actif";
}
if($debug eq "-v")
{
	periode();
	print "$clamav\n";
}
if($debug eq "-v") {
	periode();
	print "-> Verification McAfee...\n";
	print "Cmd : /usr/lib/nagios/plugins/check_miroir_mcafee.pl\n";
}
$Service=`/usr/lib/nagios/plugins/check_miroir_mcafee.pl`;
if ($Service =~ /Miroir NON INSTALLE/) {
	$mcafee="Non Actif";
}else{
	$mcafee="Actif";
	if($version_eole =~ /2.3/)
	{
		$Service=`sudo /usr/bin/ParseDico --list | grep miroir'`;
		if($service =~ /niveau_miroir/)
		{
			($niveau_mcafee) = $service =~ /niveau_miroir=\"(.*)\"/;
		}
	}
	else
	{
		$niveau_mcafee=`sudo CreoleGet niveau_miroir'`;
	}
}
if($debug eq "-v")
{
	periode();
	print "$mcafee - $niveau_mcafee\n";
}

if($debug eq "-v") {
	periode();
	print "-> Verification geo-ide-base...\n";
	print "Cmd : /usr/lib/nagios/plugins/check_geoidebase.pl -H 127.0.0.1 -t\n";
}
our $geo="non";
$Service=`/usr/lib/nagios/plugins/check_geoidebase.pl -H 127.0.0.1 -t`;
if ($Service =~ /GEOBASE NON INSTALLE/)
{
	$geoidebase="Non Actif";
}else{
	if($Service =~ /GEOBASE PALIER 0 INSTALLE/)
	{
		$geoidebase="Pallier0 OK";
		$geo="oui";
	}
	else{
		$geoidebase="Pallier2 OK";
		$geo="oui";
	}
}
if($debug eq "-v")
{
	periode();
	print "$geoidebase\n";
}
if($debug eq "-v") {
	periode();
	print "-> Verification geo-ide-distribution...\n";
	print "Cmd : /usr/lib/nagios/plugins/dpkg -l | grep geo-ide\n";
}
$service=`dpkg -l | grep geo-ide`;
if ($Service =~ /GEOBASE NON INSTALLE/)
{
	$geoidedistribution="Non Actif";
}else{
	$geoidedistribution="Actif";
	$geo="oui";
}
if($debug eq "-v")
{
	periode();
	print "$geoidedistribution\n";
}
#recup info geo-ide
our $info_geoide="";
if($geo eq "oui") {
	if($debug eq "-v") {
		periode();
		print "-> Recuperation version paquet geoide sur le serveur\n";
	}
	$info_geoide = $service;
	$info_geoide =~ s/\n/\++/g;
	if($debug eq "-v")
	{
		periode();
		print $info_geoide."\n";
	}
}













if(($version_eole =~ /2.5/) || ($version_eole =~ /2.4/))
{

    if($debug eq "-v") { print "----- Traitement EOLE 2.5.X -----\n"; }

    #---------- Ligne 1 ----------
    # ip zephir ; id zephir ; dernier contact
    if($debug eq "-v") { print "----- Ligne 1 -----\n"; }
    if($debug eq "-v") {
        periode();
        print "-> Recuperation info zephir\n";
    }
    our $zephirmaitre="inconnu";
    my $recup=`sudo cat /usr/lib/python2.7/dist-packages/zephir/zephir_conf/zephir_conf.py`;
    our $idzephir;
    our $zephirmaitre;

    if($recup =~ /adresse_zephir/) {
	    ($zephirmaitre) = $recup =~ /adresse_zephir=\"(.*)\"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$zephirmaitre\n";
    }
    my $recup=`sudo tail -n 1 /var/log/uucp/Stats`;
    our $contactzephir;
    if($recup =~ /zephir/) {
        ($contactzephir) = $recup =~ /zephir \((.*)\) sent/;
    }
    if($debug eq "-v")
    {
        periode();
        print "$contactzephir\n";
    }

    our $ligne1=" Identifiant:$idzephir ; Zephir maitre:$zephirmaitre ; Dernier contact :$contactzephir ;\n";


    #---------- Ligne 2 ----------
    # hostname ; Service ; domaine
    #

    if($debug eq "-v") { print "----- Ligne 2 -----\n"; }
    if($debug eq "-v") {
        periode();
        print "-> Recuperation domaine, hostname, service...\n";
    }
    our $domaine=`sudo CreoleGet smb_workgroup`;
    if($debug eq "-v")
    {
        periode();
        print "$domaine\n";
    }
    our $nom=`sudo CreoleGet nom_machine`;
    if($debug eq "-v")
    {
        periode();
        print "$nom\n";
    }
    our $rne=`sudo CreoleGet numero_etab`;
    if($debug eq "-v")
    {
        periode();
        print "$rne\n";
    }

    our $ligne2=" Serveur:$nom ; Service:$rne ; domaine:$domaine ;\n";

    #---------- Ligne 3 ----------
    # ip zephir ; id zephir ; dernier contact
    if($debug eq "-v") { print "----- Ligne 3 -----\n"; }

    if($debug eq "-v") {
        periode();
        print "-> Recuperation module,variante, debmiroir...\n";
    }
    our $module=`sudo CreoleGet module_type`;
    if($debug eq "-v")
    {
        periode();
        print "$module\n";
    }
    our $variante=`sudo CreoleGet variante_type`;
    if($debug eq "-v")
    {
        periode();
        print "$variante\n";
    }
    our $serveur_maj=`sudo CreoleGet serveur_maj`;
    if($debug eq "-v")
    {
        periode();
        print "$serveur_maj\n";
    }
    #recup proxy
    if($debug eq "-v") {
        periode();
        print "-> Utilisation proxy sur serveur eole...\n";
    }
    our $proxy=`sudo CreoleGet activer_proxy_client`;
    if($debug eq "-v")
    {
        periode();
        print "$proxy\n";
    }

    our $ligne3=" Module:$module ; Variante:$variante ; DebMiroir :$serveur_maj ; Version EOLE :$version_eole ; Distrib :$distrib ; Version plugins :$versplugins_mess ; Utilisation proxy :$proxy ;\n";



    #---------- Ligne 4 ----------
    ## Materiel ; Constructeur ; date install
    #recup materiel et constructeur
    if($debug eq "-v") { print "----- Ligne 4 -----\n"; }

    our $ligne4=" Materiel :$vendor - $materiel ; Date install :$install_eole ;\n";

    #---------- Ligne 5 ----------
    # Conf DNS ; Conf WINS
    if($debug eq "-v") { print "----- Ligne 5 -----\n"; }

    our $smb_wins_support=`sudo CreoleGet smb_wins_support`;
    our $smb_adresse_ip_wins="";
    if($smb_wins_support eq "yes") { $smb_wins_support="oui"; $smb_adresse_ip_wins="127.0.0.1";}
    else { $smb_wins_support="non";$smb_adresse_ip_wins="";}
    if($debug eq "-v")
    {
        periode();
        print "WINS : $smb_wins_support ; Server WINS : $smb_adresse_ip_wins\n";
    }
    our $smb_procede_recherche_nom=`sudo CreoleGet smb_name_resolve_order`;
    if($debug eq "-v")
    {
        periode();
        print "$smb_procede_recherche_nom\n";
    }
    our $adresse_ip_dns=`sudo CreoleGet adresse_ip_dns`;
    if($debug eq "-v")
    {
        periode();
        print "$adresse_ip_dns\n";
    }
    our $activer_cache_dns=`sudo CreoleGet activer_cache_dns`;
    if($debug eq "-v")
    {
        periode();
        print "$activer_cache_dns\n";
    }
    our $smb_domain_master=`sudo CreoleGet smb_domain_master`;
    our $domain_master="non";
    if($smb_domain_master =~ /yes/) { $domain_master="oui";}
    if($debug eq "-v")
    {
        periode();
        print "$domain_master\n";
    }

    our $ligne5=" Conf DNS ( ip_dns:$adresse_ip_dns - cache_dns:$activer_cache_dns) ;\n";
    $ligne5 = $ligne5." Conf WINS ( wins_support:$smb_wins_support - ip_wins:$smb_adresse_ip_wins - smb_recherche:$smb_procede_recherche_nom - domain_master:$domain_master $MessageMaster) ;\n";

    #---------- Ligne 6 ----------
    # Service installe
    if($debug eq "-v") { print "----- Ligne 6 -----\n"; }

	#verification crontab ecdl
	our $ecdl_message="";
	our $ecdl_cron_root="";
	our $ecdl_cron_nagios="";
	our $ecdl_cron_std="";
	if($module =~ /eCDL/)
	{
		my $recuproot=`sudo crontab -l -u root | grep statut | wc -l`;
		if($recuproot == 0) { $ecdl_cron_root="ROOT ABSENT-"; }
		elsif($recuproot == 1) { $ecdl_cron_root="ROOT OK-"; }
		else{ $ecdl_cron_root="ROOT DOUBLE-"; }

		my $recupnagios=`crontab -l | grep statut | wc -l`;
		if($recupnagios == 0) { $ecdl_cron_nagios="NAGIOS OK-"; }
		else{ $ecdl_cron_nagios="NAGIOS DOUBLE-"; }

		my $recupstd=`cat /etc/crontab | grep statut | wc -l`;
		if($recupstd == 0) { $ecdl_cron_std="STD OK-"; }
		else{ $ecdl_cron_std="STD DOUBLE-"; }

		if($ecdl_cron_root =~ /ABSENT/) { $ecdl_message="CRONTAB ECDL ABSENT"; }
		elsif(($ecdl_cron_root =~ /OK/)&&($ecdl_cron_nagios =~ /OK/)&&($ecdl_cron_std =~ /OK/)) { $ecdl_message="CRONTAB ECDL OK"; }
		else{
			$ecdl_message="CRONTAB ECDL EN DOUBLE";
			$ecdl_message.=" (";
			if(($ecdl_cron_root=~/OK/)||($ecdl_cron_root=~/DOUBLE/)) { $ecdl_message.="root"; }
			if($ecdl_cron_nagios=~/DOUBLE/) { $ecdl_message.=" nagios"; }
			if($ecdl_cron_std=~/DOUBLE/) { $ecdl_message.=" /etc/crontab"; }
			$ecdl_message.=")";
		}
	}

    our $ligne6=" Service installe : SQUID:$squid ; DANSGUARDIAN:$dansguardian ; CLAMAV:$clamav ; MCAFEE:$mcafee ; GEO-IDE-BASE:$geoidebase ; GEO-IDE-DISTRIBUTION:$geoidedistribution ; ONDULEUR:$ups ;\n";
    if($geo eq "oui")
    {
    	$ligne6=$ligne6." Paquet GEO-IDE:$info_geoide ;\n";
    }
    if($module =~ /eCDL/)
    {
    	$ligne6=$ligne6." Verif crontab:$ecdl_message ;\n";
    }

    #---------- Ligne 7 ----------
    # Config reseau
    if($debug eq "-v") { print "----- Ligne 7 -----\n"; }
    if($debug eq "-v") {
    	periode();
    	print "-> Recuperation info reseau ...\n";
    }

    our $ligne7;
   	$ligne7=" Config RESEAU ( IP :  - Network :  - Netmask :  ) ;\n";
	if($debug eq "-v")
	{
		periode();
		print "$ligne7\n";
	}
}
else
{

    if($debug eq "-v") { print "----- Traitement EOLE autre -----\n"; }

    #---------- Ligne 1 ----------
    # ip zephir ; id zephir ; dernier contact
    if($debug eq "-v") { print "----- Ligne 1 -----\n"; }
    if($debug eq "-v") {
    	periode();
    	print "-> Recuperation identifiant zephir\n";
    	print "Cmd : sudo cat /usr/lib/python2.6/dist-packages/zephir/zephir_conf/zephir_conf.py\n";
    }
    my $recup=`sudo cat /usr/lib/python2.6/dist-packages/zephir/zephir_conf/zephir_conf.py`;
    our $idzephir;
    our $zephirmaitre;

    if($recup =~ /adresse_zephir/) {
	    ($zephirmaitre) = $recup =~ /adresse_zephir=\"(.*)\"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$zephirmaitre\n";
    }

    my $recup=`enregistrement_zephir -c`;
    ($idzephir) = $recup =~ /identifiant : (.*)\)/;

    my $recup=`sudo tail -n 1 /var/log/uucp/Stats`;
    our $contactzephir;
    if($recup =~ /zephir/) {
    	($contactzephir) = $recup =~ /zephir \((.*)\) sent/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$contactzephir\n";
    }


    our $ligne1=" Identifiant:$idzephir ; Zephir maitre:$zephirmaitre ; Dernier contact :$contactzephir ;\n";


    #---------- Ligne 2 ----------
    # hostname ; Service ; domaine
    if($debug eq "-v") { print "----- Ligne 2 -----\n"; }
    if($debug eq "-v") {
    	periode();
    	print "-> Recuperation domaine, hostname, service...\n";
    	print "Cmd : sudo /usr/bin/ParseDico --list | egrep 'machine|workgroup|etab'\n";
    }
    my $recup=`sudo /usr/bin/ParseDico --list | egrep "machine|workgroup|etab"`;
    our $domaine="Inconnu";
    if($recup =~ /smb_workgroup/) {
    	($domaine) = $recup =~ /smb_workgroup="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$domaine\n";
    }
    our $nom="inconnu";
    if($recup =~ /nom_machine/) {
    	($nom) = $recup =~ /nom_machine="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$nom\n";
    }
    our $rne="inconnu";
    if($recup =~ /numero_etab/) {
    	($rne) = $recup =~ /numero_etab="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$rne\n";
    }

    our $ligne2=" Serveur:$nom ; Service:$rne ; domaine:$domaine ;\n";

    #---------- Ligne 3 ----------
    # Module ; Variante ; Debmiroir ; Version Eole ; Distrib ; Version plugins ; utilisation Proxy
    if($debug eq "-v") { print "----- Ligne 3 -----\n"; }
    #recup module,variante, debmiroir
    if($debug eq "-v") {
    	periode();
    	print "-> Recuperation module,variante, debmiroir...\n";
    	print "Cmd : sudo /usr/bin/ParseDico --list | egrep 'module|serveur_maj|variante'\n";
    }
    my $recup=`sudo /usr/bin/ParseDico --list | egrep 'module|serveur_maj|variante'`;
    our $module="Inconnu";
    if($recup =~ /module/) {
    	($module) = $recup =~ /module_type=\"(.*)\"/;
    }
    if($module eq "")
    {
        $recup = `cat /etc/eole/release`;
        ($module) = $recup =~ /MODULE=(.*)/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$module\n";
    }
    our $variante="Inconnu";
    if($recup =~ /variante/) {
    	($variante) = $recup =~ /variante_type=\"(.*)\"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$variante\n";
    }
    our $serveur_maj="Inconnu";
    if($recup =~ /debmiroir-02/) {
    	$serveur_maj="debmiroir-02";
    }elsif($recup =~ /debmiroir-01/) {
    	$serveur_maj="debmiroir-01";
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$serveur_maj\n";
    }


    #recup proxy
    if($debug eq "-v") {
    	periode();
    	print "-> Utilisation proxy sur serveur eole...\n";
    	print "Cmd : sudo /usr/bin/ParseDico --list proxy\n";
    }
    my $recup=`sudo /usr/bin/ParseDico --list proxy`;
    our $proxy;
    ($proxy) = $recup =~ /activer_proxy_client="(.*)"/;
    if($debug eq "-v")
    {
    	periode();
    	print "$proxy\n";
    }

    our $ligne3=" Module:$module ; Variante:$variante ; DebMiroir :$serveur_maj ; Version EOLE :$version_eole ; Distrib :$distrib ; Version plugins :$versplugins_mess ; Utilisation proxy :$proxy ;\n";

    #---------- Ligne 4 ----------
    # Materiel ; Constructeur ; date install
    if($debug eq "-v") { print "----- Ligne 4 -----\n"; }
    #recup materiel et constructeur

    our $ligne4=" Materiel :$vendor - $materiel ; Date install :$install_eole ;\n";

    #---------- Ligne 5 ----------
    # Conf DNS ; Conf WINS
    if($debug eq "-v") { print "----- Ligne 5 -----\n"; }
    if($debug eq "-v")
    {
    	periode();
    	print "-> Recuperation des variables dico sur le serveur\n";
    }
    my $Recup2=`sudo /usr/bin/ParseDico --list | egrep 'dns|wins|smb|workgroup|domaine|etab'`;
    our $smb_wins_support="oui";
    if($Recup2 =~ /smb_wins_support="no"/) { $smb_wins_support="non";}
    if($debug eq "-v")
    {
    	periode();
    	print "$smb_wins_support\n";
    }
    our $smb_adresse_ip_wins="";
    if($Recup2 =~ /smb_adresse_ip_wins/) {
    	($smb_adresse_ip_wins) = $Recup2 =~ /smb_adresse_ip_wins="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$smb_adresse_ip_wins\n";
    }
    our $smb_procede_recherche_nom="";
    if($Recup2 =~ /smb_procede_recherche_nom/) {
    	($smb_procede_recherche_nom) = $Recup2 =~ /smb_procede_recherche_nom="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$smb_procede_recherche_nom\n";
    }
    our $adresse_ip_dns="";
    if($Recup2 =~ /adresse_ip_dns/) {
    	($adresse_ip_dns) = $Recup2 =~ /adresse_ip_dns="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$adresse_ip_dns\n";
    }
    our $activer_cache_dns="non";
    if($Recup2 =~ /activer_cache_dns/) {
    	($activer_cache_dns) = $Recup2 =~ /activer_cache_dns="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$activer_cache_dns\n";
    }
    our $smb_domain_master="non";
    if($Recup2 =~ /smb_domain_master/) {
    	($smb_domain_master) = $Recup2 =~ /smb_domain_master="(.*)"/;
    }
    if($debug eq "-v")
    {
    	periode();
    	print "$smb_domain_master\n";
    }

    our $ligne5=" Conf DNS ( ip_dns:$adresse_ip_dns - cache_dns:$activer_cache_dns) ;\n";
$ligne5 = $ligne5." Conf WINS ( wins_support:$smb_wins_support - ip_wins:$smb_adresse_ip_wins - smb_recherche:$smb_procede_recherche_nom - domain_master:$smb_domain_master $MessageMaster) ;\n";

    #---------- Ligne 6 ----------
    # Service installe
    if($debug eq "-v") { print "----- Ligne 6 -----\n"; }

	#verification crontab ecdl
	our $ecdl_message="";
	our $ecdl_cron_root="";
	our $ecdl_cron_nagios="";
	our $ecdl_cron_std="";
	if($module eq "eCDL")
	{
		my $recuproot=`sudo crontab -l -u root | grep statut | wc -l`;
		if($recuproot == 0) { $ecdl_cron_root="ROOT ABSENT-"; }
		elsif($recuproot == 1) { $ecdl_cron_root="ROOT OK-"; }
		else{ $ecdl_cron_root="ROOT DOUBLE-"; }

		my $recupnagios=`crontab -l | grep statut | wc -l`;
		if($recupnagios == 0) { $ecdl_cron_nagios="NAGIOS OK-"; }
		else{ $ecdl_cron_nagios="NAGIOS DOUBLE-"; }

		my $recupstd=`cat /etc/crontab | grep statut | wc -l`;
		if($recupstd == 0) { $ecdl_cron_std="STD OK-"; }
		else{ $ecdl_cron_std="STD DOUBLE-"; }

		if($ecdl_cron_root =~ /ABSENT/) { $ecdl_message="CRONTAB ECDL ABSENT"; }
		elsif(($ecdl_cron_root =~ /OK/)&&($ecdl_cron_nagios =~ /OK/)&&($ecdl_cron_std =~ /OK/)) { $ecdl_message="CRONTAB ECDL OK"; }
		else{
			$ecdl_message="CRONTAB ECDL EN DOUBLE";
			$ecdl_message.=" (";
			if(($ecdl_cron_root=~/OK/)||($ecdl_cron_root=~/DOUBLE/)) { $ecdl_message.="root"; }
			if($ecdl_cron_nagios=~/DOUBLE/) { $ecdl_message.=" nagios"; }
			if($ecdl_cron_std=~/DOUBLE/) { $ecdl_message.=" /etc/crontab"; }
			$ecdl_message.=")";
		}
	}

    our $ligne6=" Service installe : SQUID:$squid ; DANSGUARDIAN:$dansguardian ; CLAMAV:$clamav ; MCAFEE:$mcafee ; GEO-IDE-BASE:$geoidebase ; GEO-IDE-DISTRIBUTION:$geoidedistribution ; ONDULEUR:$ups ;\n";
    if($geo eq "oui")
    {
    	$ligne6=$ligne6." Paquet GEO-IDE:$info_geoide ;\n";
    }
    if($module eq "eCDL")
    {
    	$ligne6=$ligne6." Verif crontab:$ecdl_message ;\n";
    }

    #---------- Ligne 7 ----------
    # Config reseau
    if($debug eq "-v") { print "----- Ligne 7 -----\n"; }
    if($debug eq "-v") {
    	periode();
    	print "-> Recuperation info reseau ...\n";
    	print "Cmd : sudo /usr/bin/ParseDico --list | egrep 'eth0|eth1|eth2|eth3|eth4|eth5'\n";
    }

    our $adripeth;
    our $adrneteth;
    our $adrmasketh;
    our $ligne7;

    if(($module =~ /SBL/) || ($module =~ /CDL/))
    {
    	recup_eth(eth0);
    	$ligne7=" Config RESEAU ( IP : $adripeth - Network : $adrneteth - Netmask : $adrmasketh ) ;\n";
    	if($debug eq "-v")
    	{
    		periode();
    		print "$ligne7\n";
    	}
    }
    else
    {
    	$ligne7=" Config RESEAU \n";
    	my $service = `sudo /usr/bin/ParseDico --list | grep 'interfaces'`;
    	my $nbinterface;
	    ($nbinterface) = $service =~ /nombre_interfaces=\"(.*)\"/;

    	if($nbinterface == 2)
    	{
    		#print "2 interfaces\n";
    		recup_eth(eth0);
    		$ligne7.="- Patte eth0 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth1);
    		$ligne7.="- Patte eth1 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    	}
    	if($nbinterface == 3)
    	{
    		#print "3 interfaces\n";
    		recup_eth(eth0);
    		$ligne7.="- Patte eth0 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth1);
    		$ligne7.="- Patte eth1 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth2);
    		$ligne7.="- Patte eth2 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    	}
    	if($nbinterface == 4)
    	{
    		#print "4 interfaces\n";
    		recup_eth(eth0);
    		$ligne7.="- Patte eth0 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth1);
    		$ligne7.="- Patte eth1 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth2);
    		$ligne7.="- Patte eth2 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth3);
    		$ligne7.="- Patte eth3 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    	}
    	if($nbinterface == 5)
    	{
    		#print "4 interfaces\n";
    		recup_eth(eth0);
    		$ligne7.="- Patte eth0 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth1);
    		$ligne7.="- Patte eth1 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth2);
	    	$ligne7.="- Patte eth2 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth3);
    		$ligne7.="- Patte eth3 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth4);
    		$ligne7.="- Patte eth4 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    	}
    	if($nbinterface == 6)
    	{
    		#print "4 interfaces\n";
    		recup_eth(eth0);
    		$ligne7.="- Patte eth0 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth1);
    		$ligne7.="- Patte eth1 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth2);
    		$ligne7.="- Patte eth2 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth3);
    		$ligne7.="- Patte eth3 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth4);
    		$ligne7.="- Patte eth4 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    		recup_eth(eth5);
    		$ligne7.="- Patte eth5 ( IP :$adripeth - Network :$adrneteth - Netmask :$adrmasketh )\n";
    	}

    	if($debug eq "-v")
    	{
    		periode();
    		print "$ligne7\n";
    	}
    }

}


#---------- Affectation des lignes ----------
# ligne 1 : zephir
# ligne 2 : serveur
# ligne 3 : conf eole
# ligne 4 : materiel
# ligne 5 : conf DNS et WINS
# ligne 6 : service install
# ligne 7 : conf reseau


my $lignesortie0 = $ligne1." ; ".$ligne2." ; ".$ligne3." ; ".$ligne4." ; ".$ligne5." ; ".$ligne7." ; ".$ligne6;
my $lignesortie1a = $ligne1." ; ".$ligne2." ; ".$ligne3." ; ".$ligne4." ; ".$ligne5." ; ".$ligne7." ; ".$ligne6;


#---------- Traitement de la sortie ----------

if($CodeSortie eq "0") {
	print $lignesortie0;
	exit 0;
}else{
	if($versplugins_ok eq '1') {
		print $lignesortie1a;
		exit 2;
	}
	else {
		print "Erreur de recuperation information";
		exit 2;
	}
}

exit;


sub recup_eth()
{
	my $eth=$_[0];
	my $service = `sudo /usr/bin/ParseDico --list | grep $eth`;
	if($service =~ /adresse_ip_eth/) { ($adripeth) = $service =~ /adresse_ip_$eth=\"(.*)\"/; }
	if($service =~ /adresse_netmask_eth/) { ($adrmasketh) = $service =~ /adresse_netmask_$eth=\"(.*)\"/; }
	if($service =~ /adresse_network_eth/) { ($adrneteth) = $service =~ /adresse_network_$eth=\"(.*)\"/; }
}

sub periode()
{
	my $now = strftime "%H:%M:%S", localtime;
	print "Heure : $now\n";
}
