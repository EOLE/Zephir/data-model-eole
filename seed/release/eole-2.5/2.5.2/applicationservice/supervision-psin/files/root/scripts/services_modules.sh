#!/bin/bash


##############################################################################
## Applications
##
# récupération des variables
activer_glpi=$(CreoleGet activer_glpi non)
activer_grr=$(CreoleGet activer_grr non)
activer_phpmyadmin=$(CreoleGet activer_phpmyadmin non)
activer_ocsinventory=$(CreoleGet activer_ocsinventory non)
activer_geoide_base=$(CreoleGet activer_geoide_base non)
activer_geoide_base_miroir=$(CreoleGet activer_geoide_base_miroir non)
activer_geoide_distrib=$(CreoleGet activer_geoide_distrib non)

appli_glpi()
{
	if [ $activer_glpi == 'oui' ]
	then
		echo "Application GLPI active" >> $scripts/$fichierEnvoi
	fi
}

appli_grr()
{
	if [ $activer_grr == 'oui' ]
	then
		echo "Application GRR active" >> $scripts/$fichierEnvoi
	fi
}

appli_phpmyadmin()
{
	if [ $activer_phpmyadmin == 'oui' ]
	then
		echo "Application phpMyAdmin active" >> $scripts/$fichierEnvoi
	fi
}

appli_ocs()
{
	if [ $activer_ocsinventory == 'oui' ]
	then
		echo "Application OCS Inventory active" >> $scripts/$fichierEnvoi
	fi
}

appli_geoide_base()
{
	if [ $activer_geoide_base == 'oui' ]
	then
		echo "Application Geoide base active" >> $scripts/$fichierEnvoi
	fi
}

appli_geoide_base_miroir()
{
	if [ $activer_geoide_base_miroir == 'oui' ]
	then
		echo "Application Geoide base miroir active" >> $scripts/$fichierEnvoi
	fi
}

appli_geoide_distrib()
{
	if [ $activer_geoide_distrib == 'oui' ]
	then
		echo "Application Geoide distribution active" >> $scripts/$fichierEnvoi
	fi
}
##############################################################################

##############################################################################
## Informations
##
# récupération des variables
module_type=$(CreoleGet module_type)
nombre_interfaces=$(CreoleGet nombre_interfaces 1)
adresse_ip_eth0=$(CreoleGet adresse_ip_eth0)
adresse_ip_eth1=$(CreoleGet adresse_ip_eth1 1.1.1.1)
adresse_ip_eth2=$(CreoleGet adresse_ip_eth2 2.2.2.2)
adresse_ip_eth3=$(CreoleGet adresse_ip_eth3 3.3.3.3)
adresse_ip_eth4=$(CreoleGet adresse_ip_eth4 4.4.4.4)
type_amon=$(CreoleGet type_amon eSSL)

infos_sys()
{
	echo -e "\nModule: $module_type" >> $scripts/$fichierEnvoi
	echo "Marque: $(dmidecode|grep 'Manufacturer:'|sed -n "1p"|sed 's/.*:\(.*\)/\1/')" >> $scripts/$fichierEnvoi
	echo "Modele: $(dmidecode |grep "Product Name:"|sed -n "1p"|sed 's/.*:\(.*\)/\1/')" >> $scripts/$fichierEnvoi
}
##############################################################################

##############################################################################
## Services
##
# récupération des variables
nom_domaine_local=$(CreoleGet nom_domaine_local)
activer_apache=$(CreoleGet activer_apache non)
activer_clam=$(CreoleGet activer_clam non)
enable_clamd=$(CreoleGet enable_clamd non)
activer_dhcp=$(CreoleGet activer_dhcp non)
activer_miroir=$(CreoleGet activer_miroir non)
niveau_miroir=$(CreoleGet niveau_miroir non)
liste_index_miroir_on=$(CreoleGet liste_index_miroir_on non)
index_miroir=$(CreoleGet index_miroir a)
activer_mysql=$(CreoleGet activer_mysql non)
activer_nut=$(CreoleGet activer_nut non)
nut_ups_name=$(CreoleGet nut_ups_name nut)
nut_ups_driver=$(CreoleGet nut_ups_driver nut)
nut_ups_port=$(CreoleGet nut_ups_port 111)
activer_sso=$(CreoleGet activer_sso non)
activer_accounting=$(CreoleGet activer_accounting non)

srv_apache()
{
	if [ $activer_apache == 'oui' ]
	then
		echo "service Apache actif" >> $scripts/$fichierEnvoi
	fi
}

srv_clamav()
{
	if [ $activer_clam == 'oui' ]
	then
		echo "Antivirus Clamav actif" >> $scripts/$fichierEnvoi
	fi
}
srv_clamav_proxy()
{
	if [ $enable_clamd == 'oui' ]
	then
		echo "Antivirus Clamav actif sur le proxy" >> $scripts/$fichierEnvoi
	fi
}

srv_dhcp()
{
	if [ $activer_dhcp == 'oui' ]
	then
		echo "service DHCP actif" >> $scripts/$fichierEnvoi
	fi
}

srv_mcafee()
{
	if [ "$activer_miroir" == 'oui' ]
	then
		echo "Miroir Mac Afee actif" >> $scripts/$fichierEnvoi
        if [ $module_type == 'amon' ] ; then
            case "$nombre_interfaces" in
             1)
                ip=$adresse_ip_eth0
                ;;
             2)
                ip=$adresse_ip_eth1
                ;;
             3)
                ip=$adresse_ip_eth2
                ;;
             4)
                ip=$adresse_ip_eth2
                ;;
             5)
                ip=$adresse_ip_eth2
                ;;
            esac
        else
            ip=$adresse_ip_eth0
        fi
        if [ $niveau_miroir == '2' ]
        then
            echo "CNAME à créer : miroir-av2."$nom_domaine_local"   "$ip >> $scripts/$fichierEnvoi
        else
            if [ "$liste_index_miroir_on" == 'oui' ]
                then
                    echo "CNAME à créer : miroir-av3"$(CreoleGet liste_index_miroir k)"."$nom_domaine_local"    "$ip >> $scripts/$fichierEnvoi
                else
                    echo "CNAME à créer : miroir-av3"$index_miroir"."$nom_domaine_local"    "$ip >> $scripts/$fichierEnvoi
            fi
        fi
	fi
}

srv_mysql()
{
	if [ $activer_mysql == 'oui' ]
	then
		echo "service MySql actif" >> $scripts/$fichierEnvoi
	fi
}

srv_onduleur()
{
	if [ $activer_nut == 'oui' ]
	then
		echo "service Onduleur actif" >> $scripts/$fichierEnvoi
		echo "Nom: $nut_ups_name, Pilote: $nut_ups_driver, Port: $nut_ups_port" >> $scripts/$fichierEnvoi
	fi
}

srv_reseau()
{
	# informations IP
	case "$nombre_interfaces" in
	1)
		echo "Adresse IP eth0: $adresse_ip_eth0" >> $scripts/$fichierEnvoi
		;;
	2)
		echo "Adresse IP eth0: $adresse_ip_eth0" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth1: $adresse_ip_eth1" >> $scripts/$fichierEnvoi
		;;
	3)
		echo "Adresse IP eth0: $adresse_ip_eth0" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth1: $adresse_ip_eth1" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth2: $adresse_ip_eth2" >> $scripts/$fichierEnvoi
		;;
	4)
		echo "Adresse IP eth0: $adresse_ip_eth0" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth1: $adresse_ip_eth1" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth2: $adresse_ip_eth2" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth3: $adresse_ip_eth3" >> $scripts/$fichierEnvoi
		;;
	5)
		echo "Adresse IP eth0: $adresse_ip_eth0" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth1: $adresse_ip_eth1" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth2: $adresse_ip_eth2" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth3: $adresse_ip_eth3" >> $scripts/$fichierEnvoi
		echo "Adresse IP eth4: $adresse_ip_eth4" >> $scripts/$fichierEnvoi
		;;
	*)
		# spécificité eSSL traitée par la suite
		;;
	esac
}

srv_sso()
{
	if [ $activer_sso == 'distant' ]
	then
		echo "Authentification CAS active" >> $scripts/$fichierEnvoi
	fi
}

srv_account()
{
	if [ $activer_accounting == 'oui' ]
	then
		echo "Accounting actif" >> $scripts/$fichierEnvoi
	fi
}
##############################################################################


######################################################################################################################
## regroupement de services
##
# récupération des variables
nom_machine=$(CreoleGet nom_machine)
numero_etab=$(CreoleGet numero_etab)
variante_type=$(CreoleGet variante_type)
eole_release=$(CreoleGet eole_release 2.4.0)

services_commun()
{
	echo "Un nouveau serveur $nom_machine vient d'etre installe sur le site $numero_etab" >> $scripts/$fichierEnvoi
	echo "ou bien ce serveur - deja supervise - vient d'avoir sa configuration modifiee" >> $scripts/$fichierEnvoi

    # ID zephir
    /usr/bin/enregistrement_zephir -c >> $scripts/$fichierEnvoi
    if [ -f /etc/uucp/sys ]
    then
        echo -e "Enregistré sur le zephir : $(cat /etc/uucp/sys |grep address |grep zephir |sed 's/address//')\n" >> $scripts/$fichierEnvoi
    fi

	# informations IP
	srv_reseau

    # Version Eole
    echo -e "\nVersion Eole : $eole_release" >> $scripts/$fichierEnvoi

	# informations systèmes
	infos_sys

	# onduleurs
	srv_onduleur

	# dépôt Mac Afee
	srv_mcafee

	# serveur DHCP
	srv_dhcp

	# Antivirus
	srv_clamav
}

services_ecdl()
{
	echo -e "\nVariante: $variante_type" >> $scripts/$fichierEnvoi
	# TODO: services ecdl à surveiller
}

services_esbl()
{
	echo -e "\nVariante: $variante_type" >> $scripts/$fichierEnvoi

	case "$variante_type" in
	standard)
		;;

	production)
		;;

    Appli_Web)
		# LAMP
		srv_apache
		srv_mysql

		# cerbere
		srv_sso

		# Applications Web
		appli_glpi
		appli_grr
		appli_ocs
		appli_phpmyadmin
		;;

	geomatique)
		# Geo-Ide
		appli_geoide_base
		appli_geoide_base_miroir
		appli_geoide_distrib
		;;

	esac
}

services_essl()
{
	echo -e "\nVariante: $variante_type" >> $scripts/$fichierEnvoi

	case "$variante_type" in
	eSSL_Std)
		srv_clamav_proxy
		;;

	eSSL_Internet)
		;;

    eSSL_SPC)
		;;

	PPP)
		;;

	esac

    echo -e "\nModele de filtrage: $type_amon" >> $scripts/$fichierEnvoi
    srv_account

}

services_zephir()
{
	echo -e "\nVariante: $variante_type" >> $scripts/$fichierEnvoi
	# TODO: etat sauvegarde locale
	# TODO: etat sauvegarde CSP
}
