#!/usr/bin/env python
#-*- coding: utf-8 -*-

from SafeUTF8ConfigParser import SafeUTF8ConfigParser as ConfigParser
from providers import creole_provider, commandes_provider, zephir_provider, utils
from optparse import OptionParser
from ftplib import FTP
from DNS import dnslookup
from DNS.Base import ServerError
import os
import locale
import time
import re

locale.setlocale(locale.LC_ALL, '')

execution_time = utils.repr_value(time.strftime('%c', time.localtime()))

ip_re = re.compile(r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$')

def is_ip(ip):
    """
    Return True if ip match ip pattern with number below 256
    :param ip: string to test against ip pattern
    :type ip: str
    """
    ip = ip_re.match(ip)
    if ip and len([segment for segment in ip.groups()
                   if int(segment) < 256]) == 4:
        return True
    else:
        return False

def parse_variables(variables_file):
    """
    Parse ini formatted file and return ConfigParser object
    :param variables_file: file path
    :type variables_file: str
    """
    conf_in = ConfigParser()
    conf_in.read(variables_file)

    conf_in.add_section('zephir')
    conf_in.set('zephir', 'idserveur', 'id_serveur')
    conf_in.set('zephir', 'idzephir', 'adresse_zephir')
    return conf_in

def extract_infos(conf_in):
    """
    Return ConfigParser object with Default section
    filled with variables and values extracted
    with providers modules function depending on section
    the variables are in in original ConfigParser object.
    :param conf_in: original configuration with variables in section
                    corresponding to extraction functions.
    :type conf_out: ConfigParser object
    """
    conf_out = ConfigParser()
    conf_out.set('DEFAULT', 'heure_execution_script', execution_time)
    
    creole_items = conf_in.items('creole')
    creole_items = creole_provider.get_values(creole_items).iteritems()
    for variable, value in creole_items:
        conf_out.set('DEFAULT', variable, value)

    command_items = conf_in.items('command')
    command_items = commandes_provider.get_values(command_items).iteritems()
    for variable, value in command_items:
        conf_out.set('DEFAULT', variable, value)

    zephir_items = conf_in.items('zephir')
    zephir_items = zephir_provider.get_values(zephir_items).iteritems()
    for variable, value in zephir_items:
        conf_out.set('DEFAULT', variable, value)

    return conf_out

def write_stats(conf_out):
    """
    Write ini formatted file filled with conf_out variables and values.
    :param conf_out: configuration to write
    :type conf_out: ConfigParser object
    """
    ini_dir = '/tmp'
    id_zephir = conf_out.get('DEFAULT', 'idzephir').lower()
    if not is_ip(id_zephir):
        try:
            ip_zephir = [ip for ip in dnslookup(id_zephir, 'A')
                         if is_ip(ip)]
            ip_zephir = ip_zephir[0]
        except:
            ip_zephir = id_zephir
    else:
        ip_zephir = id_zephir

    ini_name = '-'.join([ip_zephir,
                         conf_out.get('DEFAULT', 'idserveur').lower()])
    ini_file = os.path.join(ini_dir, ini_name)
    with open(ini_file, 'w') as ic:
        conf_out.write(ic)
    return ini_file

def send_stats(stats, dest, folder):
    """
    Issue write ftp command to store file remotely
    :param stats: stats file
    :type stats: str (path)
    :param dest: ftp address
    :type dest: str
    """
    ftp_session = FTP(dest)
    ftp_session.login('anonymous', 'anonymous@eole.i2')
    with open(stats, 'rb') as stats_file:
        ftp_session.storbinary('STOR {0}'.format(os.path.join(folder, 
                                                 os.path.basename(stats))), 
                                                 stats_file)
    ftp_session.quit()

def main():
    """
    Main function: parse variable list,
                   extract information,
                   store locally and
                   send file to ftp anonymously.
    """
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                      help="Fichier contenant la liste des variables")
    parser.add_option("-s", "--sendto", dest="destination",
                      help="Destinataire du fichier")
    parser.add_option("-d", "--folder", dest="folder", default='',
                      help="Dossier de destination du fichier")

    (options, args) = parser.parse_args()
    if options.filename:
        conf_in = parse_variables(options.filename)
        conf_out = extract_infos(conf_in)
        stats = write_stats(conf_out)
    else:
        parser.print_help()
    if options.destination:
        send_stats(stats, options.destination, options.folder)

if __name__ == '__main__':
    main()
