#-*- coding: utf-8 -*-

from ConfigParser import SafeConfigParser, DEFAULTSECT

class SafeUTF8ConfigParser(SafeConfigParser):

    def write(self, fp):
        """Write an .ini-format representation of the configuration state."""
        if self._defaults:
            fp.write("[%s]\n" % DEFAULTSECT)
            for (key, value) in self._defaults.items():
                fp.write("{0} = {1}\n".format(key, value.decode('utf-8').replace('\n', '\n\t').encode('utf8')))
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key == "__name__":
                    continue
                if (value is not None) or (self._optcre == self.OPTCRE):
                    key = " = ".join((key, value.decode('utf-8').replace('\n', '\n\t').encode('utf8')))
                fp.write("%s\n" % (key))
            fp.write("\n")

