#-*- coding:utf-8 -*-
from creole.client import CreoleClient
from utils import repr_value

client = CreoleClient()

def get_values(variables, empty=False):
    """
    Provides values corresponding to variables searching creole dictionnary.
    :param variables: set of variable name and creole variable name (duplicated if empty)
    :type variables: (str, str)
    :param empty: wether to keep variables for creole returns nothing valuable
    :type empty: bool
    """
    values = {}
    for variable in variables:
        value = variable[1]
        if value == '':
            value = variable[0]
        try:
            value = client.get_creole(value, '')
            if empty or value != '':
                values[variable[0]] = repr_value(value)
        except KeyError:
            pass
    return values
