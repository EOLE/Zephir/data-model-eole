#-*- coding: utf-8 -*-
import time
import locale
from itertools import chain, product
import re

DEFAULT_LOCALE = locale.setlocale(locale.LC_ALL, '')

LOCALES = set([DEFAULT_LOCALE, 'C'])

PUNCTUATION = re.compile(r'[,:]$')

UTC_DATE_FORMAT = '%d/%m/%Y %H:%M:%S +0000'

DATE_PATTERNS = {'%Y-%m-%d': ('year', 'month', 'day'),
                 '%x': ('year', 'month', 'day'),
                 '%Y': ('year'),
                 '%m': ('month'),
                 '%B': ('month'),
                 '%b': ('month'),
                 '%d': ('day'),
                 '%X': ('hour')}

DATE_ELS = {'day', 'month', 'year', 'hour'}


def filter_date(value):
    """
    Return value converted to utc date format if value is parsable with
    common time strptime.
    :param value: value we want to test against common time strptime formats.
    :type value: str
    """
    value_els = {i:re.sub(PUNCTUATION, '', v) for i,v in enumerate(value.split())}
    date_els = {}
    for l in LOCALES:
        locale.setlocale(locale.LC_ALL, l)
        for pattern, value_el in product(DATE_PATTERNS, value_els.iteritems()):
            try:
                if time.strptime(value_el[1], pattern):
                    date_els[value_el[0]] = date_els.get(value_el[0],
                                                         {'str':value_el[1]})
                    date_els[value_el[0]].setdefault('elems', {})
                    date_els[value_el[0]]['elems'].update({DATE_PATTERNS[pattern]: pattern})
            except Exception as e:
                continue
        univocals = { date_el for el in date_els.itervalues()
                      for date_el in el['elems'].iterkeys()
                      if len(el['elems']) == 1 }
        filtered_date_els = []
        for el in date_els.itervalues():
            if len(el['elems']) > 1:
                garbage = set(el['elems'].iterkeys()).intersection(univocals)
                for g in garbage:
                    del el['elems'][g]
            if len(el['elems']) == 1:
                filtered_date_els.append((el['elems'].items()[0],
                                          el['str']))
            
        if len(date_els) > 0:
            is_date = { fde[0][0] for fde in filtered_date_els } == DATE_ELS
            if is_date:
                patterns, els = zip(*[(fde[0][1], fde[1])
                                      for fde in filtered_date_els])
                date = time.mktime(time.strptime(' '.join(els), ' '.join(patterns)))
                value = time.strftime(UTC_DATE_FORMAT, time.gmtime(date))
                break
            else:
                date_els = {}

    locale.setlocale(locale.LC_ALL, DEFAULT_LOCALE)
    return value


def repr_value(value):
    """
    Coerce value to stripped string for ConfigParser setting function.
    :param value: value to coerce to string
    :type value: arbitrary type
    """
    if isinstance(value, list):
        value = ', '.join([repr_value(v) for v in value])
    elif isinstance(value, int):
        value = str(value).strip()
    else:
        value = filter_date(value).strip()
    return value


def eole23_version(eole_common_version):
    """
    Return EOLE version depending on eole-common package version
    :param eole_common_version: eole-common package version as returned by dpkg -l command
    :type eole_common_version: str starting with '2.3-eole'
    """
    mapping = {(139,):'2.3.1', 
               (142,):'2.3.2', 
               (143,):'2.3.3', 
               (145,):'2.3.4', 
               (146, 147):'2.3.5', 
               (148,):'2.3.6', 
               (150, 151):'2.3.7', 
               (152, 153):'2.3.8', 
               (154,):'2.3.9', 
               (155,):'2.3.10',
               (156, 157):'2.3.11',
               (158,):'2.3.12',
               (159,):'2.3.13',
               (160,):'2.3.14',
               (161, 162):'2.3.15',
               (163,):'2.3.16'}
    version =  [mapping[m] for m in mapping if int(eole_common_version.replace('2.3-eole','')) in m]
    if len(version) != 0:
        version = version[0]
    else:
        version = ''
    return version
