#-*- coding: utf-8 -*-

from utils import repr_value

try:
    from zephir.zephir_conf import zephir_conf as config
except ImportError:
    print "Ce serveur n'est pas enregistré sur Zéphir"
    config = None

def get_values(variables, empty=False):
    """
    Provides values corresponding to variables importing zephir configuration.
    :param variables: set of variable name and function to execute
    :type variables: (str, str)
    :param empty: wether to keep variables for which zephir configuration contains nothing valuable
    :type empty: bool
    """
    values = {}
    if config is not None:
        for variable in variables:
            value = variable[1]
            try:
                value = repr_value(getattr(config, value))
                if empty or value != '':
                    values[variable[0]] = value
            except AttributeError:
                pass
    else:
        for variable in variables:
            value = raw_input('Fournir la valeur pour la variable {} :'.\
                               format(variable[0]))
            values[variable[0]] = value
    return values

