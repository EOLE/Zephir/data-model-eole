#-*- coding: utf-8 -*-

from pyeole.process import system_out
from utils import repr_value
from subprocess import Popen, PIPE
import shlex

def get_values(variables, empty=False):
    """
    Provides values corresponding to variables executing shell functions.
    :param variables: set of variable name and function to execute
    :type variables: (str, str)
    :param empty: wether to keep variables for which function return nothing valuable
    :type empty: bool
    """
    values = {}
    for variable in variables:
        command = [variable[1]]
        variable = variable[0]
        with open('/dev/null', 'w') as dev_null:
            
            process = Popen(command, stderr=dev_null, stdout=PIPE, shell=True)
            output = process.communicate()[0]
        if empty or output != '':
            values[variable] = repr_value(output)
    return values
