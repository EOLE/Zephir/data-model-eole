# declaration variable
activer_accounting=$(CreoleGet activer_accounting)

%if %%is_defined('activer_accounting')
 %if %%activer_accounting == 'oui'

#!/bin/sh


#####################################################################
## Script d'accounting PSIN
#####################################################################

#########################################
##
## variables globales
##
PROXY_PORTS="80,8080,443,3128"
WEB_PORTS="80,443"
FTP_PORTS="20,21"
LDAP_PORTS="389,636"
POP3_PORTS="110,995"
IMAP_PORTS="143,993"
SMTP_PORTS="25,465"
MCAFEE_EPO="12081"
MCAFEE_PORTS="12443,12445"
SAMBA_PORTS="135,137,138,139,445"
DNS_PORTS="53"
SSH_PORTS="22"

#########################################
#########################################
# déclaration variables des dicos

activer_firewall=$(CreoleGet activer_firewall non)
  %if %%activer_firewall == 'oui'


   %if %%is_defined('serveurs_proxies_nationaux_ip')
serveurs_proxies_nationaux_ip=$(CreoleGet serveurs_proxies_nationaux_ip)
   %end if
   %if %%is_defined('serveurs_proxies_pro_ip')
serveurs_proxies_pro_ip=$(CreoleGet serveurs_proxies_pro_ip)
   %end if
   %if %%is_defined('serveurs_melanie2_nationaux_ip')
serveurs_melanie2_nationaux_ip=$(CreoleGet serveurs_melanie2_nationaux_ip)
   %end if
   %if %%is_defined('serveurs_ldap_nationaux_ip')
serveurs_ldap_nationaux_ip=$(CreoleGet serveurs_ldap_nationaux_ip)
   %end if
   %if %%is_defined('serveurs_mcafee_nationaux_ip')
serveurs_mcafee_nationaux_ip=$(CreoleGet serveurs_mcafee_nationaux_ip)
   %end if
   %if %%is_defined('serveurs_dns_nationaux_ip')
serveurs_dns_nationaux_ip=$(CreoleGet serveurs_dns_nationaux_ip)
   %end if
   %if %%is_defined('serveurs_wsus_nationaux_ip')
serveurs_wsus_nationaux_ip=$(CreoleGet serveurs_wsus_nationaux_ip)
   %end if
#########################################

## suppression de toutes les tables ipset
ipset -X

# proxies nationaux
   %if %%is_defined('serveurs_proxies_nationaux_ip')
    %if not %%is_empty(%%serveurs_proxies_nationaux_ip)
ipset -N proxies iphash --hashsize 32 --probes 1
     %for %%proxy_iter in %%serveurs_proxies_nationaux_ip
ipset -A proxies %%proxy_iter
     %end for
    %end if
   %end if

# proxies pro
   %if %%is_defined('serveurs_proxies_pro_ip')
    %if not %%is_empty(%%serveurs_proxies_pro_ip)
ipset -N proxiespro iphash --hashsize 32 --probes 1
     %for %%proxypro_iter in %%serveurs_proxies_pro_ip
ipset -A proxiespro %%proxypro_iter
     %end for
    %end if
   %end if

# serveurs melanie2
   %if %%is_defined('serveurs_melanie2_nationaux_ip')
    %if not %%is_empty(%%serveurs_melanie2_nationaux_ip)
ipset -N melanie2  iphash --hashsize 32 --probes 1
     %for %%melanie2_iter in %%serveurs_melanie2_nationaux_ip
ipset -A melanie2 %%melanie2_iter
     %end for
    %end if
   %end if

# serveurs LDAP
   %if %%is_defined('serveurs_ldap_nationaux_ip')
    %if not %%is_empty(%%serveurs_ldap_nationaux_ip)
ipset -N ldap iphash --hashsize 32 --probes 1
     %for %%ldap_iter in %%serveurs_ldap_nationaux_ip
ipset -A ldap %%ldap_iter
     %end for
    %end if
   %end if

## serveurs McAfee
   %if %%is_defined('serveurs_mcafee_nationaux_ip')
    %if not %%is_empty(%%serveurs_mcafee_nationaux_ip)
ipset -N mcafee iphash --hashsize 32 --probes 1
     %for %%mcafee_iter in %%serveurs_mcafee_nationaux_ip
ipset -A mcafee %%mcafee_iter
     %end for
    %end if
   %end if

# serveurs dns
   %if %%is_defined('serveurs_dns_nationaux_ip')
    %if not %%is_empty(%%serveurs_dns_nationaux_ip)
ipset -N dns iphash --hashsize 32 --probes 1
     %for %%dns_iter in %%serveurs_dns_nationaux_ip
ipset -A dns %%dns_iter
     %end for
    %end if
   %end if

# serveurs wsus national
   %if %%is_defined('serveurs_wsus_nationaux_ip')
    %if not %%is_empty(%%serveurs_wsus_nationaux_ip)
ipset -N wsus iphash --hashsize 32 --probes 1
     %for %%wsus_iter in %%serveurs_wsus_nationaux_ip
ipset -A wsus %%wsus_iter
     %end for
    %end if
   %end if

module_type=$(CreoleGet module_type amon)
variante_type=$(CreoleGet variante_type essl)
   %if %%module_type == 'amon'
    %if %%is_defined('type_amon')
    type_amon=$(CreoleGet type_amon 2zones-essl)
    %end if
########################################################################
###              Gestion du traffic par ports                        ###
########################################################################
# Declaration des chaines internes 'ports'
/sbin/iptables -N ports_essl_IN
/sbin/iptables -N ports_essl_OUT
/sbin/iptables -N ports_essl_lan_IN
/sbin/iptables -N ports_essl_lan_OUT
# Declaration des chaines traversantes 'ports'
/sbin/iptables -N ports_forward_IN
/sbin/iptables -N ports_forward_OUT

    %if %%type_amon == '2zones-essl'
################### Flux Proxy/DNS/Anti-virus ##########################
# Ports eSSL depuis/vers MOREA
/sbin/iptables -I OUTPUT -o eth1 -m comment --comment "ACC-ports_out-morea" -j ports_essl_OUT
/sbin/iptables -I INPUT -i eth1 -m comment --comment "ACC-ports_in-morea" -j ports_essl_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_in-proxies-http"
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_in-proxies-http"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_out-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_in-ftp"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
/sbin/iptables -A ports_essl_IN -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
#** Protocole SSH **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_in-ssh-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_out-ssh-clients"

     %if %%variante_type == 'eSSL Internet'
################### Flux Proxy/DNS/antivirus Internet ##################################
# Flux eSSL depuis/vers INTERNET
/sbin/iptables -I OUTPUT -o eth0 -m comment --comment "ACC-ports_out-internet" -j ports_essl_OUT
/sbin/iptables -I INPUT -i eth0 -m comment --comment "ACC-ports_in-internet" -j ports_essl_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_INTERNET_in-proxies-http"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_INTERNET_out-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_INTERNET_in-ftp"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_INTERNET_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_out-dns-clients"
/sbin/iptables -A ports_essl_IN -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_out-dns-clients"
     %end if

    %else
################### Flux Proxy/DNS/Anti-virus ##########################
# Ports eSSL LAN <--> MOREA
/sbin/iptables -I OUTPUT -o eth1 -m comment --comment "ACC-ports_out-morea" -j ports_essl_OUT
/sbin/iptables -I INPUT -i eth1 -m comment --comment "ACC-ports_in-morea" -j ports_essl_IN
/sbin/iptables -I OUTPUT -o eth2 -m comment --comment "ACC-ports_out-lan" -j ports_essl_lan_OUT
/sbin/iptables -I INPUT -i eth2 -m comment --comment "ACC-ports_in-lan" -j ports_essl_lan_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_in-proxies-http"
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_in-proxies-http"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_out-proxies-http"
/sbin/iptables -A ports_essl_lan_IN -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_in_LAN-proxies-http"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_out_LAN-proxies-http"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_out_LAN-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_in-ftp"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_out-ftp"
/sbin/iptables -A ports_essl_lan_IN -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_in_LAN-ftp"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_out_LAN-ftp"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_out_LAN-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
/sbin/iptables -A ports_essl_lan_IN -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in_LAN-dns-clients"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_out_LAN-dns-clients"
/sbin/iptables -A ports_essl_IN -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
/sbin/iptables -A ports_essl_lan_IN -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in_LAN-dns-clients"
/sbin/iptables -A ports_essl_lan_OUT -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_out_LAN-dns-clients"
#** Protocole SSH **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $SSH_PORTS -m comment --comment "ACC-ports_in-ssh-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_out-ssh-clients"
/sbin/iptables -A ports_essl_lan_IN -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_in_LAN-ssh-clients"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_out_LAN-ssh-clients"
/sbin/iptables -A ports_essl_lan_OUT -p tcp -m multiport --sport $SSH_PORTS -m comment --comment "ACC-ports_out_LAN-ssh-clients"

###########################################################################
################### Flux Pare-feu #########################################
# Ports pare-feu depuis/vers MOREA
/sbin/iptables -I FORWARD -o eth1 -m comment --comment "ACC-ports_PF_out-morea" -j ports_forward_OUT
/sbin/iptables -I FORWARD -i eth1 -m comment --comment "ACC-ports_PF_in-morea" -j ports_forward_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_PF_in-proxies-http"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_PF_out-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_PF_in-ftp"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_PF_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_PF_in-dns-clients"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_PF_out-dns-clients"
/sbin/iptables -A ports_forward_IN -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_PF_in-dns-clients"
/sbin/iptables -A ports_forward_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_PF_out-dns-clients"
# ** Protocoles Melanie2 **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $POP3_PORTS -m comment --comment "ACC-ports_PF_in-melanie2-pop"
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $SMTP_PORTS -m comment --comment "ACC-ports_PF_in-melanie2-smtp"
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $IMAP_PORTS -m comment --comment "ACC-ports_PF_in-melanie2-imap"
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $LDAP_PORTS -m comment --comment "ACC-ports_PF_in-melanie2-ldap(s)"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $POP3_PORTS -m comment --comment "ACC-ports_PF_out-melanie2-pop"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $SMTP_PORTS -m comment --comment "ACC-ports_PF_out-melanie2-smtp"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $IMAP_PORTS -m comment --comment "ACC-ports_PF_out-melanie2-imap"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $LDAP_PORTS -m comment --comment "ACC-ports_PF_out-melanie2-ldap(s)"
# ** Protocoles  McAfee **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $MCAFEE_PORTS -m comment --comment "ACC-ports_PF_in-mcafee-clients"
/sbin/iptables -A ports_forward_IN -p tcp --sport $MCAFEE_EPO -m comment --comment "ACC-ports_PF_in-mcafee-epo"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $MCAFEE_PORTS -m comment --comment "ACC-ports_PF_out-mcafee-clients"
/sbin/iptables -A ports_forward_OUT -p tcp --dport $MCAFEE_EPO -m comment --comment "ACC-ports_PF_out-mcafee-epo"
# ** Protocoles NetBios **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $SAMBA_PORTS -m comment --comment "ACC-ports_PF_in-samba-clients"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_PF_out-samba-clients"
/sbin/iptables -A ports_forward_IN -p udp -m multiport --sport $SAMBA_PORTS -m comment --comment "ACC-ports_PF_in-samba-clients"
/sbin/iptables -A ports_forward_OUT -p udp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_PF_out-samba-clients"

     %if %%variante_type == 'eSSL Internet'
################### Flux Proxy Internet ##################################
# Flux eSSL depuis/vers INTERNET
/sbin/iptables -I OUTPUT -o eth0 -m comment --comment "ACC-ports_out-internet" -j ports_essl_OUT
/sbin/iptables -I INPUT -i eth0 -m comment --comment "ACC-ports_in-internet" -j ports_essl_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_INTERNET_in-proxies-http"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_INTERNET_out-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_INTERNET_in-ftp"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_INTERNET_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_essl_IN -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_out-dns-clients"
/sbin/iptables -A ports_essl_IN -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_in-dns-clients"
/sbin/iptables -A ports_essl_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_INTERNET_out-dns-clients"

################### Flux Pare-feu Internet #########################################
# Ports pare-feu depuis/vers INTERNET
/sbin/iptables -I FORWARD -o eth0 -m comment --comment "ACC-ports_PF_out-internet" -j ports_forward_OUT
/sbin/iptables -I FORWARD -i eth0 -m comment --comment "ACC-ports_PF_in-internet" -j ports_forward_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_PF_INTERNET_in-proxies-http"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_PF_INTERNET_out-proxies-http"
# ** Protocoles FTP **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $FTP_PORTS -m comment --comment "ACC-ports_PF_INTERNET_in-ftp"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_PF_INTERNET_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_forward_IN -p tcp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_PF_INTERNET_in-dns-clients"
/sbin/iptables -A ports_forward_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_PF_INTERNET_out-dns-clients"
/sbin/iptables -A ports_forward_IN -p udp -m multiport --sport $DNS_PORTS -m comment --comment "ACC-ports_PF_INTERNET_in-dns-clients"
/sbin/iptables -A ports_forward_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_PF_INTERNET_out-dns-clients"
     %end if
    %end if


########################################################################
###            Gestion du traffic par Serveurs Nationaux             ###
########################################################################
#######################################################
# Declaration des chaines traversantes 'flux'
/sbin/iptables -N flux_forward_IN
/sbin/iptables -N flux_forward_OUT
# Declaration des chaines internes 'flux'
/sbin/iptables -N flux_essl_IN
/sbin/iptables -N flux_essl_OUT

################### Flux Proxy/DNS/Anti-virus #############################
# Flux eSSL depuis/vers MOREA
/sbin/iptables -I OUTPUT -o eth1 -m comment --comment "ACC-flux_out-morea" -j flux_essl_OUT
/sbin/iptables -I INPUT -i eth1 -m comment --comment "ACC-flux_in-morea" -j flux_essl_IN

# Flux Morea serveurs proxies nationaux
    %if %%is_defined('serveurs_proxies_nationaux_ip')
     %if not %%is_empty(%%serveurs_proxies_nationaux_ip)
/sbin/iptables -A flux_essl_OUT -m set --match-set  proxies dst -m comment --comment "ACC_out-proxies"
/sbin/iptables -A flux_essl_IN -m set --match-set  proxies src -m comment --comment "ACC_in-proxies"
     %end if
    %end if
# Flux Morea serveurs proxies Surf professionnel
    %if %%is_defined('serveurs_proxies_pro_ip')
     %if not %%is_empty(%%serveurs_proxies_pro_ip)
/sbin/iptables -A flux_essl_OUT -m set --match-set  proxiespro dst -m comment --comment "ACC_out-surfpro"
/sbin/iptables -A flux_essl_IN -m set --match-set  proxiespro src -m comment --comment "ACC_in-surfpro"
     %end if
    %end if
# Flux Morea serveurs McAfee
    %if %%is_defined('serveurs_mcafee_nationaux_ip')
     %if not %%is_empty(%%serveurs_mcafee_nationaux_ip)
/sbin/iptables -A flux_essl_OUT -m set --match-set mcafee dst -m comment --comment "ACC_out-mcafee"
/sbin/iptables -A flux_essl_IN -m set --match-set mcafee src -m comment --comment "ACC_in-mcafee"
     %end if
    %end if
# Flux Morea serveurs dns nationaux
    %if %%is_defined('serveurs_dns_nationaux_ip')
     %if not %%is_empty(%%serveurs_dns_nationaux_ip)
/sbin/iptables -A flux_essl_OUT -m set --match-set dns dst -m comment --comment "ACC_out-dnsnat"
/sbin/iptables -A flux_essl_IN -m set --match-set dns src -m comment --comment "ACC_in-dnsnat"
     %end if
    %end if


    %if %%type_amon != '2zones-essl'
###########################################################################
################### Flux Pare-feu #########################################
# Flux pare-feu depuis/vers MOREA
/sbin/iptables -I FORWARD -o eth1 -m comment --comment "ACC-flux_out-morea" -j flux_forward_OUT
/sbin/iptables -I FORWARD -i eth1 -m comment --comment "ACC-flux_in-morea" -j flux_forward_IN

# Flux Morea serveurs proxies nationaux
     %if %%is_defined('serveurs_proxies_nationaux_ip')
      %if not %%is_empty(%%serveurs_proxies_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set  proxies dst -m comment --comment "ACC-flux_out-proxies"
/sbin/iptables -A flux_forward_IN -m set --match-set  proxies src -m comment --comment "ACC-flux_in-proxies"
      %end if
     %end if
# Flux Morea serveurs proxies Surf professionnel
     %if %%is_defined('serveurs_proxies_pro_ip')
      %if not %%is_empty(%%serveurs_proxies_pro_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set  proxiespro dst -m comment --comment "ACC-flux_out-surfpro"
/sbin/iptables -A flux_forward_IN -m set --match-set  proxiespro src -m comment --comment "ACC-flux_in-surfpro"
      %end if
     %end if
# Flux Morea serveurs messagerie nationaux
     %if %%is_defined('serveurs_melanie2_nationaux_ip')
      %if not %%is_empty(%%serveurs_melanie2_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set melanie2 dst -m comment --comment "ACC-flux_out-melanie2"
/sbin/iptables -A flux_forward_IN -m set --match-set melanie2 src -m comment --comment "ACC-flux_in-melanie2"
      %end if
     %end if
# Flux Morea serveurs LDAP
     %if %%is_defined('serveurs_ldap_nationaux_ip')
      %if not %%is_empty(%%serveurs_ldap_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set ldap dst -m comment --comment "ACC-flux_out-ldap"
/sbin/iptables -A flux_forward_IN -m set --match-set ldap src -m comment --comment "ACC-flux_in-ldap"
     %end if
    %end if
# Flux Morea serveurs McAfee
     %if %%is_defined('serveurs_mcafee_nationaux_ip')
      %if not %%is_empty(%%serveurs_mcafee_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set mcafee dst -m comment --comment "ACC-flux_out-mcafee"
/sbin/iptables -A flux_forward_IN -m set --match-set mcafee src -m comment --comment "ACC-flux_in-mcafee"
      %end if
     %end if
# Flux Morea serveurs dns nationaux
     %if %%is_defined('serveurs_dns_nationaux_ip')
      %if not %%is_empty(%%serveurs_dns_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set dns dst -m comment --comment "ACC-flux_out-dnsnat"
/sbin/iptables -A flux_forward_IN -m set --match-set dns src -m comment --comment "ACC-flux_in-dnsnat"
      %end if
     %end if
# Flux Morea serveurs wsus nationaux
     %if %%is_defined('serveurs_wsus_nationaux_ip')
      %if not %%is_empty(%%serveurs_wsus_nationaux_ip)
/sbin/iptables -A flux_forward_OUT -m set --match-set wsus dst -m comment --comment "ACC-flux_out-wsus"
/sbin/iptables -A flux_forward_IN -m set --match-set wsus src -m comment --comment "ACC-flux_in-wsus"
      %end if
     %end if
    %end if

    %elif %%module_type in ('ESBL', 'eCDL')
########################################################################
###              Gestion du traffic par ports                        ###
########################################################################
# Declaration des chaines internes 'ports'
/sbin/iptables -N ports_IN
/sbin/iptables -N ports_OUT

# Flux globaux entrée/sortie eSBL-eCDL
/sbin/iptables -I OUTPUT -o eth0 -m comment --comment "ACC-ports_out-total" -j ports_OUT
/sbin/iptables -I INPUT -i eth0 -m comment --comment "ACC-ports_in-total" -j ports_IN

# ** Protocoles Proxy **
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $PROXY_PORTS -m comment --comment "ACC-ports_in-proxies"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $PROXY_PORTS -m comment --comment "ACC-ports_out-proxies"
# ** Protocoles WEB **
/sbin/iptables -A ports_IN -p tcp -m multiport --dport $WEB_PORTS -m comment --comment "ACC-ports_in-web"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $WEB_PORTS -m comment --comment "ACC-ports_out-web"
# ** Protocoles FTP **
/sbin/iptables -A ports_IN -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_in-ftp"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $FTP_PORTS -m comment --comment "ACC-ports_out-ftp"
#** Protocole DNS **
/sbin/iptables -A ports_IN -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
/sbin/iptables -A ports_IN -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_in-dns-clients"
/sbin/iptables -A ports_OUT -p udp -m multiport --dport $DNS_PORTS -m comment --comment "ACC-ports_out-dns-clients"
#** Protocole SSH **
/sbin/iptables -A ports_IN -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_in-ssh-clients"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $SSH_PORTS -m comment --comment "ACC-ports_out-ssh-clients"
# ** Protocoles Melanie2 **
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $POP3_PORTS -m comment --comment "ACC-ports_in-melanie2-pop"
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $SMTP_PORTS -m comment --comment "ACC-ports_in-melanie2-smtp"
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $IMAP_PORTS -m comment --comment "ACC-ports_in-melanie2-imap"
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $LDAP_PORTS -m comment --comment "ACC-ports_in-melanie2-ldap(s)"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $POP3_PORTS -m comment --comment "ACC-ports_out-melanie2-pop"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $SMTP_PORTS -m comment --comment "ACC-ports_out-melanie2-smtp"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $IMAP_PORTS -m comment --comment "ACC-ports_out-melanie2-imap"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $LDAP_PORTS -m comment --comment "ACC-ports_out-melanie2-ldap(s)"
# ** Protocoles  McAfee **
/sbin/iptables -A ports_IN -p tcp -m multiport --sport $MCAFEE_PORTS -m comment --comment "ACC-ports_in-mcafee-clients"
/sbin/iptables -A ports_IN -p tcp --sport $MCAFEE_EPO -m comment --comment "ACC-ports_in-mcafee-epo"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $MCAFEE_PORTS -m comment --comment "ACC-ports_out-mcafee-clients"
/sbin/iptables -A ports_OUT -p tcp --dport $MCAFEE_EPO -m comment --comment "ACC-ports_out-mcafee-epo"
# ** Protocoles NetBios **
/sbin/iptables -A ports_IN -p tcp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_in-samba-clients"
/sbin/iptables -A ports_OUT -p tcp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_out-samba-clients"
/sbin/iptables -A ports_IN -p udp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_in-samba-clients"
/sbin/iptables -A ports_OUT -p udp -m multiport --dport $SAMBA_PORTS -m comment --comment "ACC-ports_out-samba-clients"

########################################################################
###            Gestion du traffic par Serveurs Nationaux             ###
########################################################################
#######################################################
# Declaration des chaines internes 'flux'
/sbin/iptables -N flux_IN
/sbin/iptables -N flux_OUT

# Flux depuis/vers serveurs nationaux
/sbin/iptables -I OUTPUT -o eth0 -m comment --comment "ACC-flux_out-total" -j flux_OUT
/sbin/iptables -I INPUT -i eth0 -m comment --comment "ACC-flux_in-total" -j flux_IN

# Flux serveurs proxies nationaux
    %if %%is_defined('serveurs_proxies_nationaux_ip')
     %if not %%is_empty(%%serveurs_proxies_nationaux_ip)
/sbin/iptables -A flux_OUT -m set --match-set  proxies dst -m comment --comment "ACC-flux_out-proxies"
/sbin/iptables -A flux_IN -m set --match-set  proxies src -m comment --comment "ACC-flux_in-proxies"
     %end if
    %end if
#Flux serveurs LDAP
    %if %%is_defined('serveurs_ldap_nationaux_ip')
     %if not %%is_empty(%%serveurs_ldap_nationaux_ip)
/sbin/iptables -A flux_OUT -m set --match-set ldap dst -m comment --comment "ACC-flux_out-ldap"
/sbin/iptables -A flux_IN -m set --match-set ldap src -m comment --comment "ACC-flux_in-ldap"
     %end if
    %end if
# Flux serveurs McAfee
    %if %%is_defined('serveurs_mcafee_nationaux_ip')
     %if not %%is_empty(%%serveurs_mcafee_nationaux_ip)
/sbin/iptables -A flux_OUT -m set --match-set mcafee dst -m comment --comment "ACC-flux_out-mcafee"
/sbin/iptables -A flux_IN -m set --match-set mcafee src -m comment --comment "ACC-flux_in-mcafee"
     %end if
    %end if
#Flux serveurs dns nationaux
    %if %%is_defined('serveurs_dns_nationaux_ip')
     %if not %%is_empty(%%serveurs_dns_nationaux_ip)
/sbin/iptables -A flux_OUT -m set --match-set dns dst -m comment --comment "ACC-flux_out-dnsnat"
/sbin/iptables -A flux_IN -m set --match-set dns src -m comment --comment "ACC-flux_in-dnsnat"
     %end if
    %end if

   %else
# Module inconnu
   %end if
  %else
# Pare-feu non activé
  %end if
##########################################################################
 %else
# accounting non activé
 %end if

%else
# variable d'accounting absente
%end if
