@echo off
echo.

%if %%mode_conteneur_actif == 'non'
set ip-scribe=%%adresse_ip_eth0
%else
set ip-scribe=%%adresse_ip_fichier_link
%end if
set settings-file=\\\%ip-scribe%\wpkg\wpkg-gp.ini
set wpkg-gp-dir="%PROGRAMFILES%\Wpkg-GP\"

IF NOT EXIST "%settings-file%" goto ErrSettings
goto SuiteClient

:ErrSettings
echo ERREUR : "%settings-file%" introuvable
echo Mise a jour impossible !
ping -n 10 127.0.0.1 > nul
goto Fin

:SuiteClient
xcopy /Y %settings-file% %wpkg-gp-dir%%

net stop wpkgserver
net start wpkgserver

echo ***************************************************************************
echo Mise a jour depuis "%settings-file%" terminee.
echo ***************************************************************************
ping -n 2 127.0.0.1 > nul

:Fin
echo.
