@echo off
echo.

%if %%mode_conteneur_actif == 'non'
set ip-scribe=%%adresse_ip_eth0
%else
set ip-scribe=%%adresse_ip_fichier_link
%end if
set settings-file=\\\%ip-scribe%\wpkg\Wpkg-GP.ini

Set RegQry=HKLM\Hardware\Description\System\CentralProcessor\0
REG.exe Query %RegQry% > %TMP%\checkOS.txt
Find /i "x86" < %TMP%\CheckOS.txt > nul
If %ERRORLEVEL% == 0 (
        echo "Installation 32 bits."
        set wpkg-client=\\\%ip-scribe%\wpkg\Wpkg-GP_x86.exe
        goto InstallClient
) ELSE (
        echo "Installation 64 bits."
        set wpkg-client=\\\%ip-scribe%\wpkg\Wpkg-GP_x64.exe
        goto InstallClient
)


:InstallClient
IF NOT EXIST "%wpkg-client%" goto ErrClient

IF NOT EXIST "%settings-file%" goto ErrSettings
goto SuiteClient

:ErrClient
echo ERREUR: "%wpkg-client%" introuvable, installation impossible !
ping -n 10 127.0.0.1 > nul
goto Fin

:ErrSettings
echo ERREUR : "%settings-file%" introuvable, installation impossible !
ping -n 10 127.0.0.1 > nul
goto Fin

:SuiteClient
"%wpkg-client%" /S /INI "%settings-file%"
echo ***************************************************************************
echo Installation de "%wpkg-client%" terminee.
echo ***************************************************************************
ping -n 2 127.0.0.1 > nul


:Fin
echo.
