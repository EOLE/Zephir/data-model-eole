\r gepi
SET character_set_client = utf8;
-- Mise à jour des informations établissements
UPDATE `setting` SET `VALUE`='%%to_sql(%%libelle_etab)' WHERE `NAME`='gepiSchoolName';
UPDATE `setting` SET `VALUE`='admin@%%domaine_messagerie_etab' WHERE `NAME`='gepiAdminAdress';
-- Configuration des modes de connexion/authentification
UPDATE `setting` SET `VALUE`='eleve' WHERE `NAME`='statut_utilisateur_defaut';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='ldap_write_access';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='sso_display_portail';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='sso_hide_logout';
%if %%is_defined('activer_sso') and %%activer_sso != 'non'
-- activation du sso
UPDATE `setting` SET `VALUE`='cas' WHERE `NAME`='auth_sso';
UPDATE `setting` SET `VALUE`='yes' WHERE `NAME`='may_import_user_profile';
UPDATE `setting` SET `VALUE`='yes' WHERE `NAME`='sso_scribe';
-- désactivation des modes d'authentification alternatifs
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='auth_ldap';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='auth_locale';
UPDATE `utilisateurs` SET `auth_mode`='sso' WHERE `login`!='admingepi';
%else
-- désactivation du sso
UPDATE `setting` SET `VALUE`='none' WHERE `NAME`='auth_sso';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='sso_scribe';
UPDATE `setting` SET `VALUE`='no' WHERE `NAME`='may_import_user_profile';
-- activation des modes d'authentification alternatifs
UPDATE `setting` SET `VALUE`='yes' WHERE `NAME`='auth_ldap';
UPDATE `setting` SET `VALUE`='yes' WHERE `NAME`='auth_locale';
UPDATE `utilisateurs` SET `auth_mode` = 'ldap' WHERE `login` != 'admingepi';
%end if
-- correction session_id et phpcas
ALTER TABLE `tempo` CHANGE `num` `num` VARCHAR( 128 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';
