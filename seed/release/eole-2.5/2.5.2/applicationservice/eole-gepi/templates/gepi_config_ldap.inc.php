<?php
#
# Vous devez renommer ce fichier en config_ldap.inc.php dans le repertoire secure pour qu'il soit pris en compte !
#

# Les lignes suivantes sont à modifier selon votre configuration

# adresse de l'annuaire LDAP.
$ldap_host="%%adresse_ip_ldap";     # Exemple : localhost, 192.168.1.1

# port utilisé
$ldap_port="389";

# identifiant et mot de passe dans le cas d'un accès non anonyme
$ldap_login="cn=reader,o=gouv,c=fr";
$ldap_password="motdepasseldap";

# chemin d'accès dans l'annuaire (= BaseDN)
# Exemple pour Eole : "o=gouv,c=fr"
$ldap_base_dn="o=gouv,c=fr";

# Complément de chemin où sont listés les utilisateurs
# Ce paramètre est placé devant le BaseDN lors des requêtes.
$ldap_people_ou = "ou=utilisateurs";

# Les classes de l'entrée LDAP d'un utilisateur. Elles doivent
# être cohérentes avec les attributs utilisés.
$ldap_people_object_classes = array("top","person","inetOrgPerson");

# Différents noms de champs contenant des informations indispensables
# pour Gepi. Si certaines équivalences ne sont pas renseignées, l'information
# ne sera pas importée.
$ldap_champ_login = "uid";
$ldap_champ_prenom = "givenname";
$ldap_champ_nom = "sn";
$ldap_champ_nom_complet = "displayname";    # Si ce champ est renseigné, il sera utilisé en combinaison avec le champ
                                # prénom ou nom pour déterminer l'attribut manquant.
$ldap_champ_email = "mail";
$ldap_champ_civilite = "codecivilite";
$ldap_champ_statut = "";

$ldap_code_civilite_madame = "2";
$ldap_code_civilite_monsieur = "1";
$ldap_code_civilite_mademoiselle = "3";

# Options supplémentaires
# Type de cryptage utilisé pour la génération du mot de passe (accès en écriture) :
$ldap_password_encryption = "crypt"; # clear, crypt, md5, ssha

# Les attributs ci-dessous permettent de déterminer quel
# statut donner à des utilisateurs importés à la volée
# depuis le LDAP.
# Le test est effectué sur la chaîne du DN. Ces attributs
# ne sont donc utiles que dans l'hypothèse où le DN contient
# une information fiable quant au statut de l'utilisateur.
# Remarque : ces paramètres ne sont utilisés que pour l'accès au LDAP
# en lecture (importation). L'accès en écriture ne prend en compte
# qu'un éventuel attribut statut (voir $ldap_champ_statut).
$ldap_chaine_dn_statut_professeur = "";
$ldap_chaine_dn_statut_eleve = "";
$ldap_chaine_dn_statut_responsable = "";
$ldap_chaine_dn_statut_scolarite = "";
$ldap_chaine_dn_statut_cpe = "";

##
# Attributs spécifiques à Scribe NG
$ldap_base_dn_extension = "ou=%%numero_etab,ou=%%nom_academie,ou=education";
%if not %%is_defined('activer_sso') or %%activer_sso == 'non'
$ldap_people_ou = $ldap_people_ou . ',' . $ldap_base_dn_extension;
%end if

?>
