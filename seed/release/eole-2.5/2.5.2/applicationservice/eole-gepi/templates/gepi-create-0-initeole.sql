-- création de la base de donnée
CREATE DATABASE gepi CHARACTER SET utf8 COLLATE utf8_general_ci;

-- création du user de la base
grant all privileges on gepi.* to gepi@%%adresse_ip_web identified by 'gepi';
flush privileges ;

-- connexion à la base
\r gepi


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
