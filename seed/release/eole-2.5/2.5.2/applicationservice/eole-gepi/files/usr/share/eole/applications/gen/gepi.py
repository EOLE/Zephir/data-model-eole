#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2011
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# gepi.py
#
# Création de la base de données mysql de gepi
#
###########################################################################
"""
    Config pour gepi
"""
from eolesql.db_test import db_exists, test_var

GEPI_TABLEFILENAMES = ['/usr/share/eole/mysql/gepi/gen/gepi-create-0-initeole.sql',
                       '/usr/share/eole/mysql/gepi/gen/gepi-create-1-structure.sql',
                       '/usr/share/eole/mysql/gepi/gen/gepi-create-2-data.sql',
                       '/usr/share/eole/mysql/gepi/gen/gepi-create-3-ajouteole.sql']
def test():
    """
        test l'existence de la base gepi
    """
    return test_var('activer_gepi') and not db_exists('gepi')

conf_dict = dict(filenames=GEPI_TABLEFILENAMES,
                 test=test)
