#-*-coding:utf-8-*-
"""
    Update systématique de la base gepi (à chaque reconfigure)
"""
from eolesql.db_test import test_var

DB = "gepi"
FILENAMES = ['/usr/share/eole/mysql/gepi/updates/gepi-update.sql']

def test_response(result=None):
    """
        renvoie True si gepi est activé
    """
    return test_var('activer_gepi')

conf_dict = dict(db=DB,
                 expected_response=test_response,
                 test_active=test_response,
                 filenames=FILENAMES,
                 version='Mise à jour de gepi'
                 )
