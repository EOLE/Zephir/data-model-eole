\r gepi
SET character_set_client = utf8;
SET NAMES 'utf8';
-- Ajout d'un utilisateur admin dans la base de données (si il n'y est pas)
INSERT IGNORE INTO utilisateurs SET login = 'admin', nom = 'Scribe', prenom = 'Administrateur', civilite = 'M.', password = 'ab4f63f9ac65152575886860dde480a1', statut = 'administrateur', etat = 'actif', change_mdp = 'y', auth_mode = 'sso';
INSERT IGNORE INTO `setting` SET `VALUE`='yes', `NAME`='sso_scribe';
