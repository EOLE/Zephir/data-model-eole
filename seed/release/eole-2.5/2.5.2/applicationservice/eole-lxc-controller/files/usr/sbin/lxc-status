#!/usr/bin/python
# -*- coding: utf-8 -*-
#
##########################################################################
# lxc-status
# Copyright © 2014 Pôle de compétences EOLE <eole@ac-dijon.fr>
#
# License CeCILL:
#  * in french: http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
#  * in english http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
##########################################################################

from sys import exit


from creole.containers import is_lxc_started, is_lxc_running
from creole.client import CreoleClient
from creole.i18n import _
from pyeole.ihm import print_red, print_green

from pyeole.i18n import i18n

_ = i18n('eole-common')

def main():
    client = CreoleClient()
    error = False
    for group in client.get_groups():
        if group not in ['root', 'all']:
            container = client.get_group_infos(group)
            if not is_lxc_started(container):
                error = True
                print_red(_('container {0} not started').format(group))
            elif not is_lxc_running(container):
                error = True
                print_red(_('container {0} started but not available').format(group))
            else:
                print_green(_('container {0} available').format(group))

    if error:
        exit(1)

if __name__ == '__main__':
    main()
