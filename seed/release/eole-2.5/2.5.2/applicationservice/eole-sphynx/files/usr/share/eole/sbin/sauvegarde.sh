#!/bin/bash
##########################################
# Sauvegarde EOLE
# - Sphynx -
# LB Dijon 13/01/2006
# bruno boiget <bruno.boiget@ac-dijon.fr>
# samuel morin <samuel.morin@ac-dijon.fr>
##########################################

# color ASCII codes
#
DEFAULT="\e[0m"
BOLD="\e[1m"
BLACK="\e[30m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
VIOLET="\e[35m"
CYAN="\e[36m"
WHITE="\e[37m"

Lock="sauvegarde"
Excl="maj"

day=`date +%d-%m-%Y`
backup_dir=/var/lib/sphynx_backups
tmp_backup=$backup_dir/$day

if [ ! -d $backup_dir ]
then
	mkdir $backup_dir
fi
rm -rf $tmp_backup
mkdir $tmp_backup

# sauvegarde des bases de données ARV et Strongswan
echo -e
echo -e "${CYAN}Sauvegarde en cours, patientez ...${DEFAULT}"

echo -e " - base ARV + Strongswan"
rsync --relative --owner --group --recursive --links --perms --times /etc/ipsec* $tmp_backup
rsync --relative --owner --group --recursive --links --perms --times /var/lib/arv/* $tmp_backup

pushd /etc > /dev/null 2>&1

echo -e " - clés de connexion"
# /root/.ssh
cd /root
tar -cpf $tmp_backup/root_ssh.tar .ssh

echo -e " - configuration eole"
# configuration EOLE et certificats générés
tar -cpf $tmp_backup/conf_eole.tar /etc/eole/config.eol \
/usr/share/eole/creole/dicos /usr/share/eole/creole/patch \
/etc/ssl

# création du fichier de sauvegarde compressé
echo -e "${CYAN}Compression de l'archive...${DEFAULT}"
cd $backup_dir
tar -czf $day.tar.gz $day
echo -e
echo -e "${GREEN}Archive créée : $backup_dir/$day.tar.gz${DEFAULT}"
echo -e

# suppression du répertoire temporaire
rm -rf $tmp_backup
# retour au répertoire d'origine
popd >/dev/null 2>&1
