#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import glob
import subprocess
from os.path import isfile, join
from getpass import getpass

from creole import cert
from creole.client import CreoleClient

from arv.db.initialize import initialize_database, commit_database
from arv.db.edge import (get_credential, get_credentials_auth, get_tmpl_connects,
                         get_tmpl_edges, add_credential_auth, add_tmpl_connect)
from arv.db.node import get_tmpl_nodes, get_tmpl_vertices, add_tmpl_node
from arv.config import sqlite_database, CACert
from arv.lib.util import split_pkcs7

def populate_database():
    zephir = None
    sphynxmodule = None
    amonmodule = None
    conf_eole = CreoleClient().get_creole()
    if conf_eole['activer_haute_dispo'] == 'esclave':
        print "Abandon : Ce serveur est esclave pour la haute disponibilité."
        sys.exit(1)
    if not isfile(sqlite_database):
        print("Abandon : base de données ARV inexistante")
        print("Lancer le script 'init_sphynx' pour initialiser la base.")
        sys.exit()
    if conf_eole['use_pncn'] == 'non':
        print("Le serveur n'est pas configué pour la PKI PNCN")
        sys.exit(1)

    initialize_database()
    #add Toulouse CA chain
    cred_auths_name = []
    for cred_auth in get_credentials_auth():
        cred_auths_name.append(cred_auth.name)
    new_cacerts, certif = split_pkcs7(CACert)
    for new_cacert in new_cacerts:
        new_cacert_name = cert.get_subject(new_cacert)[1]
        if new_cacert_name not in cred_auths_name:
            print("Ajout du certificat AC '{0}'".format(new_cacert_name))
            add_credential_auth(new_cacert)
        else:
            print("Le certificat AC '{0}' est déjà présent dans la base ARV".format(new_cacert_name))
    cred_auth_name = cert.get_subject(new_cacerts[len(new_cacerts)-1])[1]
    cred_auth = get_credential(cred_auth_name)

    #add TmplNode Sphynx and Etablissement
    tmplnode1 = tmplnode2 = None
    for tmpl_node in get_tmpl_nodes():
        if tmpl_node.mimetype == u'sphynx':
            tmplnode1 = tmpl_node
        if tmpl_node.mimetype == u'etablissement':
            tmplnode2 = tmpl_node
    if not tmplnode1:
        tmplnode1 = add_tmpl_node(name=u"Sphynx", mimetype=u'sphynx')
    if not tmplnode2:
        tmplnode2 = add_tmpl_node(name=u"Etablissement", mimetype=u'etablissement')

    #create tmplconnect and tmpledges
    tmplconnects = get_tmpl_connects()
    exist_PNCN_tmplconnect = False
    for tmplconnect in tmplconnects:
        if tmplconnect.cred_auth == cred_auth:
            exist_PNCN_tmplconnect = True
            break
    for tmplconnect in tmplconnects:
        if tmplconnect.cred_auth.name == "RACINE AGRIATES":
            tmplconnect_name = tmplconnect.name.replace('OLD_PKI_', '')
            tmplconnect.name = "OLD_PKI_" + tmplconnect_name
            if not exist_PNCN_tmplconnect:
                PNCN_tmplconnect = add_tmpl_connect(tmplconnect_name,
                                                    tmplconnect.tmpl_node_a,
                                                    tmplconnect.tmpl_node_b,
                                                    cred_auth)
                tmpledges = get_tmpl_edges(tmplconnect)
                for tmpledge in tmpledges:
                    tmpledge_name = tmpledge.name.replace('OLD_PKI_', '')
                    tmpledge.name = "OLD_PKI_" + tmpledge_name
                    PNCN_tmplconnect.add_tmpl_edge(name=tmpledge_name,
                                                   tmpl_vertex_a=tmpledge.tmpl_vertex_a,
                                                   tmpl_vertex_b=tmpledge.tmpl_vertex_b)

    #Restart ARV
    cmd = "/etc/init.d/arv restart".split()
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    if process.wait() != 0:
        print("Le redémarrage du service ARV a échoué")

populate_database()
commit_database()
