#!/usr/bin/python

def calc_smart_test_regex (test):
    from creole.client import CreoleClient
    client = CreoleClient()

    m = calc_smart_test_months(client.get_creole('smartmontools_%s_months' % test))
    d = calc_smart_test_days(client.get_creole('smartmontools_%s_days' % test))
    wd = calc_smart_test_week_days(client.get_creole('smartmontools_%s_week_days' % test))
    h = calc_smart_test_hours(client.get_creole('smartmontools_%s_hours' % test))

    return m + d + wd + h

def calc_smart_test_months(months):
    month = { 'tous':'..',
              'janvier':'01',
              'fevrier':'02',
              'mars':'03',
              'avril':'04',
              'mai':'05',
              'juin':'06',
              'juillet':'07',
              'aout':'08',
              'septembre':'09',
              'octobre':'10',
              'novembre':'11',
              'decembre':'12' }

    if 'tous' in months:
        return '/..'

    mreg =  '|'.join( map(lambda x: month[x], months) )
    if mreg == '':
        return '/..'
    elif mreg.find('|') == -1:
        return '/' + mreg
    else:
        return '/(' + mreg + ')'

def calc_smart_test_days(days):
    if '0' in days:
        return '/..'

    dreg = '|'.join( map(lambda x: str(x).rjust(2,'0') if x < 10 else str(x), days) )
    if dreg == '':
        return '/..'
    elif dreg.find('|') == -1:
        return '/' + dreg
    else:
        return '/(' + dreg + ')'

def calc_smart_test_week_days(week_days):
    week_day = { 'tous':'.',
                 'lundi':'1',
                 'mardi':'2',
                 'mercredi':'3',
                 'jeudi':'4',
                 'vendredi':'5',
                 'samedi':'6',
                 'dimanche':'7'}

    if 'tous' in week_days:
        return '/.'

    wdreg = '|'.join( map(lambda x: week_day[x], week_days) )
    if wdreg == '':
        return '/.'
    elif wdreg.find('|') == -1:
        return '/' + wdreg
    else:
        return '/(' + wdreg + ')'

def calc_smart_test_hours(hours):
    from random import randint
    hreg = '|'.join( map(lambda x: str(x).rjust(2,'0') if x < 10 else str(x), hours) )
    if hreg == '':
        return '/' + str(randint(0,23)).rjust(2,'0')
    elif hreg.find('|') == -1:
        return '/' + hreg
    else:
        return '/(' + hreg + ')'

