# -*- coding: UTF-8 -*-
vareole = "/var/lib/eole/config"
smb_netbios_name = "%%smb_netbios_name"
ldap_serveur = "%%container_ip_annuaire"
ldap_base_dn = "%%ldap_base_dn"
ldap_admin = "cn=admin,%%ldap_base_dn"
master_ip = "%%adresse_ip_br0"
mysql_password = "NOUVEAU_PASSWORD"
mysql_host = "%%adresse_ip_mysql"
activer_eop = "%%getVar('activer_eop', 'non')"
activer_eoe = "%%getVar('activer_eoe', 'non')"
%if %%getVar('activer_ad', 'non') == 'oui'
activer_ad = "oui"
%def %%gen_dc()
%return 'DC='+',DC='.join(%%ad_domain.split('.'))
%end def
# variables supplémentaires pour eole-AD
ad_address = "%%ad_address"
ad_user = "%%ad_user"
ad_base = "%%gen_dc()"
ad_pwdfile = "/root/.eolead"
ad_ldaps = "%%ad_ldaps"
%else
activer_ad = "non"
%end if
debug = False
