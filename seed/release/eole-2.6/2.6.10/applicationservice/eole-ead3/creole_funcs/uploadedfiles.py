# coding: utf-8

"""Lists files located in an upload target directory
"""
from os import listdir
from os.path import join, isfile, splitext, isdir
import json

CONF_FILE='/etc/eole/flask/enabled/ead3fileserver.conf'


def uploaded_files(allowed_extensions=None, default_content=None):
    """Lists files located in an upload target directory
    """
    if isfile(CONF_FILE):
        _DIRECTORY = json.load(open(CONF_FILE, 'r')).get("UPLOAD_FOLDER","")
    else:
        _DIRECTORY = ""

    if _DIRECTORY and isdir(_DIRECTORY):
        list_file = [filename for filename in listdir(_DIRECTORY)
                    if isfile(join(_DIRECTORY, filename)) and (
                    allowed_extensions is None or splitext(filename)[1] in allowed_extensions)]
        if len(list_file) != 0:
            return list_file

    if default_content is None:
        return []
    else:
        return [default_content]
