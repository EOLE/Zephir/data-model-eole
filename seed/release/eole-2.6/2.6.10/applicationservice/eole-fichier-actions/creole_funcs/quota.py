# -*- coding: utf-8 -*-

from pyeole.process import system_out

def quota_directories():
    directories = set()
    code, output, err  = system_out(['quotaon', '-pa'])
    if code != 0:
        for line in output.splitlines():
            try:
                directories.add(line.split()[3])
            except:
                pass
    return list(directories)
