# -*- coding: utf-8 -*-


def pg_valid_object(db_object):
    """
    Raise ValueError if db_object does not conform to pattern
    :param db_object: postgresql database object identification
    :type db_object: unicode
    """
    if len(db_object.split('.')) not in [1, 2, 3]:
        raise ValueError("L'objet doit être identifié par un chemin complet de type database.schema.table")


def pg_valid_perms(perms):
    """
    Raise ValueError if perms contains not allowed values.
    :param perms: list of permissions
    :type perms: unicode
    """
    allowed_perms = set(["SELECT", "INSERT", "UPDATE", "DELETE", "TRUNCATE",
                         "REFERENCES", "TRIGGER", "CREATE", "CONNECT",
                         "TEMPORARY", "TEMP", "EXECUTE", "USAGE", "ALL"])
    perms = set([p.upper() for p in perms.split()])
    if not perms.issubset(allowed_perms):
        raise ValueError("Permissions farfelues.")


def pg_hbalize(perms):
    """
    Return perms as access rules in pg_hba.conf format
    :param perms: permissions information
    :type perms: creole.template.CreoleMaster
    """
    from itertools import product
    source_ip = perms.slave['pg_role_perms_source_ip']
    source_netmask = perms.slave['pg_role_perms_source_netmask']
    role = perms.slave['pg_role_perms_name']
    db = perms.slave['pg_role_perms_object'].split('.')[0]

    db = db if db != "*" else "all"
    if source_ip == '127.0.0.1':
        mode = "local"
        source_ip = None
        source_netmask = None
    else:
        mode = "host"

    parameters = list(product(*[p.split('|') for p in (mode, db, role, source_ip, source_netmask, 'md5')
                  if p is not None]))
    return '\n'.join(['\t'.join(parameter) for parameter in parameters])

def pg_listen_all(perms):
    """
    Return True if perms has ip different of localhost or 127.0.0.1
    :param perms: list of permissions
    :type perms: list of creole.template.CreoleMaster
    """
    local_ip = ['127.0.0.1']
    source_ip = [perm.slave['pg_role_perms_source_ip']
                 for perm in perms
                 if perm.slave['pg_role_perms_source_ip'] not in local_ip]
    return len(source_ip) != 0
