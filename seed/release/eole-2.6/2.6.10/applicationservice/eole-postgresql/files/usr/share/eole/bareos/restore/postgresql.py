#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module postgresql"""
from os.path import join, isfile
from os import listdir
import os
import re
import pwd
import sys
import shlex
import shutil
import tempfile
from subprocess import check_call, check_output
sys.path.append('/usr/share/eole/sbin')
from pyeole.bareosrestore import bareos_restore_one_folder, \
 bareos_restore_one_file, exit_if_running_jobs
from creole.client import CreoleClient

PGSQLSAVDIR = '/home/backup/postgresql'
PGSQL_HOST = CreoleClient().get_creole('container_ip_postgresql')
PGSQL_HOST_OPTION = "-h {}".format(PGSQL_HOST) if PGSQL_HOST != '127.0.0.1' else ""
BACKUP_RE = re.compile(r'(?P<database>.*?).backup'.format(PGSQLSAVDIR))
PGDM_MAGIC = 'PGDM'

def demote(uid, gid):
    """
    Switch user and group
    :param uid: user id
    :type uid: int
    :param gid: group id
    :type gid: int
    """
    def set_ids():
        os.setgid(gid)
        os.setuid(uid)
    return set_ids


def load_file_into_pg_db(filename, clean=False, create=False):
    """
    Load given file in database to restore content.
    :param filename: database dump to restore
    :type filename: str
    :param database: target database
    :type database: str
    :param clean: flag for clean option to drop database before loading
    :type clean: boolean
    :param create: flag for create option to create database before loading
    :type create: boolean
    """
    uid = pwd.getpwnam('postgres').pw_uid
    gid = pwd.getpwnam('postgres').pw_gid
    temp_filename = tempfile.mktemp(dir='/tmp')
    shutil.copy(filename, temp_filename)
    os.chown(temp_filename, uid, gid)
    with open(filename, 'rb') as bin_file:
        magic_number = bin_file.read(4)
    if magic_number == PGDM_MAGIC:
        options = []
        options.append('-c' if clean else '')
        options.append('-C' if create else '')
        options = ' '.join(options)
        cmd = "pg_restore {0} {1}".format(options, temp_filename)
        result = check_output(shlex.split(cmd),
                              preexec_fn=demote(uid, gid))
        with open(temp_filename, 'w') as sql:
            sql.write(result)
    cmd = "psql -f {0}".format(temp_filename)
    try:
        with open(os.devnull) as devnull:
            code = check_call(shlex.split(cmd),
                              stdout=devnull,
                              stderr=devnull,
                              preexec_fn=demote(uid, gid))
    finally:
        os.unlink(temp_filename)


def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """ldap helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)


def pre():
    print "pre postgresql"


def post(databases=None):
    """
    Load backup into database cluster
    :param databases: list of databases to restore (all if none listed)
    :type databases: list
    """
    print "post postgresql"
    if databases is None:
        databases = [backup for backup in listdir(PGSQLSAVDIR)
                     if isfile(join(PGSQLSAVDIR, backup)) and
                     BACKUP_RE.match(backup)]
        databases.sort()
    for backup in [join(PGSQLSAVDIR, database) for database in databases]:
        print "restauration de {0}".format(backup)
        load_file_into_pg_db(backup, clean=True, create=True)


def job(jobid):
    print "Restauration postgressql"
    # restauration des dumps sql
    bareos_restore_one_folder(PGSQLSAVDIR, jobid)

priority = 30
