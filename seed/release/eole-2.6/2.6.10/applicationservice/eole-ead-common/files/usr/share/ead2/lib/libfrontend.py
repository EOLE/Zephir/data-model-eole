#-*-coding: UTF-8-*-
"""
    Librairies utilisées par le frontend ead:
    ServerParser : interface pour les fichiers .ini des serveurs

"""
from pyeole.dict4ini import DictIni
from ead2.lib.error import UnknownServer, ExistingServer
from ead2.config.config import BACKEND_LISTEN_PORT

SERVER_ID_NOT_FOUND = "Le serveur d'id %s n'a pas pu être retrouvé."
SERVER_ID_EXISTS = "Un serveur : \"%s:%s\" est déjà enregistré."

class ServerParser(DictIni):
    """
    utilisé pour parser un fichier servers.ini
    (fichier qui definit les serveurs sur lequel un front-end peut se connecter
    """

    def __init__(self, filename):
        DictIni.__init__(self, filename)
        self._servers = {}

    def add_server(self, url, port, comment, key):
        """
            enregistre un nouveau serveur
        """
        servs = self.get_server()
        for serv in servs.values():
            if serv[0] == url and str(serv[1]) == str(port):
                raise ExistingServer(SERVER_ID_EXISTS % (url, port))
        cle = str(len(servs) + 1)
        servs[cle] = (url, port, comment, key)
        self.save_conf(servs)

    def del_server(self, id_serv):
        """
            supprime un serveur
        """
        servs = self.get_server()
        try:
            del(servs[id_serv])
        except KeyError:
            raise(UnknownServer(SERVER_ID_NOT_FOUND % id_serv))
        server_keys = servs.keys()
        server_keys.sort()
        dico_tmp = {}
        #on va maintenant mettre a jour les indices des serveurs dans le dico
        #si on a les serveurs 1, 2 et 3, et qu'on supprime le 2, on veut que 3 devienne 2
        for key in server_keys:
            cle = str(server_keys.index(key)+1)
            dico_tmp[cle] = servs[key]
        self.save_conf(dico_tmp)

    def get_server(self, id_serv = None):
        """
            renvoie les informations sur un serveur
        """
        if not self._servers:
            for idserv, _config in self.items():
                if idserv.isdigit():
                    url = _config.get('url')
                    port = _config.get('port', "pasdeport")
                    if port.isdigit():
                        port = int(port)
                    else:
                        port = BACKEND_LISTEN_PORT
                    comment = _config.get('comment', "Aucune information")
                    key = _config.get('key')
                    self._servers.setdefault(idserv, (url, port, comment, key))
        if id_serv != None:
            if not self._servers.has_key(id_serv):
                raise(UnknownServer(SERVER_ID_NOT_FOUND % id_serv))
            else:
                return self._servers[id_serv]
        else:
            return self._servers

    def save_conf(self, dico):
        """
            ecrit le fichier de serveurs
            on efface tout pour remettre les bons ids aux serveurs
            ( 1,2,3 , je supprime 2, je veux 1 et 2 )
        """
        if self is not {}:
            for section in self.keys():
                del(self[section])
        for serv_id, config in dico.items():
            url, port, comment, key = config
            self.setdefault(serv_id, {})
            self[serv_id]['url'] = url
            self[serv_id]['port'] = str(port)
            self[serv_id]['comment'] = comment
            self[serv_id]['key'] = key
        try:
            self.save()
            self.read()
            return True
        except:
            return False
