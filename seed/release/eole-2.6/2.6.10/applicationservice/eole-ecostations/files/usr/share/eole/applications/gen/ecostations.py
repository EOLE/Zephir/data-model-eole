#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2017
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# Création de la base de données MySQL pour ecoStations
#
###########################################################################
"""
    Config pour ecoStations
"""
from eolesql.db_test import db_exists, test_var

def test():
    """
        teste l'existence de la bdd ecoStations
    """
    return test_var('activer_ecostations') and not db_exists('ecoStations')

def pregen_func(db_handler):
    """
        Fonction exécutée avant la génération si le test est bon
    """
    db_handler.simple_query("CREATE DATABASE ecoStations;")
    db_handler.simple_query("GRANT ALL PRIVILEGES ON ecoStations.* TO ecostations@localhost IDENTIFIED BY 'ecostations';")
    db_handler.simple_query("FLUSH PRIVILEGES;")

conf_dict = dict(filenames=[],
                 test=test,
                 pregen=pregen_func,
                 )
