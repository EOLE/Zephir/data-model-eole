#!/bin/bash

## on change la politique par defaut du nat
/sbin/iptables -t nat -P PREROUTING ACCEPT
/sbin/iptables -t nat -P POSTROUTING ACCEPT

## Charger le module TIME
modprobe ipt_time

## activer l'anti-spoofing
echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter

## creation de la chaine icmp accept
/sbin/iptables -N icmp-acc

### Creation des chaines zone-zone
/sbin/iptables -t filter -N eth0-eth1
/sbin/iptables -t filter -N eth1-eth0
/sbin/iptables -t filter -N root-eth0
/sbin/iptables -t filter -N eth0-eth0
/sbin/iptables -t filter -A eth0-eth1 -m state --state ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -t filter -A eth1-eth0 -m state --state ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -t filter -A root-eth0 -m state --state ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -t filter -A eth0-eth0 -m state --state ESTABLISHED,RELATED -j ACCEPT

## definition de la chaine root-eth0
/sbin/iptables -A root-eth0 -m state --state NEW -d 0/0 -j ACCEPT
# autoriser les connexions au travers d'ipsec
/sbin/iptables -A root-eth0 -m policy --pol ipsec --proto esp --dir out -j ACCEPT

## definition de la chaine eth0-root
/sbin/iptables -A eth0-root -p esp -j ACCEPT
/sbin/iptables -A eth0-root -m policy --dir in --pol ipsec --proto esp -j ACCEPT

%if %%nombre_interfaces >= '2'
## definition de la chaine eth0-eth1
/sbin/iptables -A FORWARD -i %%nom_zone_eth0 -o %%nom_zone_eth1 -j eth0-eth1
/sbin/iptables -A eth0-eth1 -m policy --dir in --pol ipsec --proto esp -j ACCEPT
/sbin/iptables -A eth0-eth1 -j DROP
## definition de la chaine eth1-eth0
/sbin/iptables -A FORWARD -i %%nom_zone_eth1 -o %%nom_zone_eth0 -j eth1-eth0
/sbin/iptables -A eth1-eth0 -m policy --dir out --pol ipsec --proto esp -j ACCEPT
/sbin/iptables -A eth1-eth0 -j DROP
## definition de la chaine eth1-root
/sbin/iptables -A INPUT -d %%adresse_ip_eth1 -i %%nom_zone_eth1 -j eth1-root
/sbin/iptables -A eth1-root -m policy --dir in --pol ipsec --proto esp -j ACCEPT
%end if

%if %%nombre_interfaces >= '3'
## definition de la chaine eth2-root
 %if %%getVar('activer_haute_dispo', 'non') != "non"
/sbin/iptables -A eth%%corosync_dial_if-root -m pkttype --pkt-type multicast -j ACCEPT
/sbin/iptables -A eth%%corosync_dial_if-root -p udp -m udp --dport %%corosync_mcastport -j ACCEPT
 %end if
%end if

%if %%fw_interetab_accept == "oui"
## definition de la chaine eth0-eth0
# autoriser les connexions inter etablissement au travers d'ipsec
/sbin/iptables -A FORWARD -i %%nom_zone_eth0 -o %%nom_zone_eth0 -j eth0-eth0
 %for %%ip_etab in %%ip_source_etab
  %if %%ip_etab.proto_dest_etab == "tout"
/sbin/iptables -A eth0-eth0 -m policy --pol ipsec --proto esp --dir in -s %%ip_etab/%%ip_etab.netmask_source_etab -d %%ip_etab.ip_dest_etab/%%ip_etab.netmask_dest_etab -j ACCEPT
  %else if %%ip_etab.proto_dest_etab == "icmp"
/sbin/iptables -A eth0-eth0 -m policy --pol ipsec --proto esp --dir in -s %%ip_etab/%%ip_etab.netmask_source_etab -d %%ip_etab.ip_dest_etab/%%ip_etab.netmask_dest_etab -p %%ip_etab.proto_dest_etab -j ACCEPT
  %else
/sbin/iptables -A eth0-eth0 -m policy --pol ipsec --proto esp --dir in -m state --state NEW -s %%ip_etab/%%ip_etab.netmask_source_etab -d %%ip_etab.ip_dest_etab/%%ip_etab.netmask_dest_etab -p %%ip_etab.proto_dest_etab --dport %%ip_etab.port_dest_etab -j ACCEPT
  %end if
 %end for
#pour finir, on interdit le reste
/sbin/iptables -A eth0-eth0 -m policy --pol ipsec --proto esp --dir in -j DROP

 %if %%getVar('val_mtu_interetab', None) != None
#mise en place de la règle pour fixer le mtu intersite
/sbin/iptables -t mangle -F
/sbin/iptables -t mangle -A FORWARD -i %%nom_zone_eth0 -o %%nom_zone_eth0 -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss %%val_mtu_interetab
%%
 %end if
%end if
