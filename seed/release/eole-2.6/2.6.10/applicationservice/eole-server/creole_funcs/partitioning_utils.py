# -*- coding: utf-8 -*-

"""Fonctions relatives à l’ajustement du partitionnement au moment de l’instance
"""
from warnings import warn_explicit
try:
    from tiramisu.error import ValueWarning
except:
    class ValueWarning(Warning):
        def __init__(self, msg, *args, **kwargs):
            super(Warning, self).__init__(msg)
import re
import os
try:
    from pyeole.process import system_out
except:
    pass


def calc_free_PE():
    try:
        cmd = ['vgs', '-o', 'vg_free_count', '--noheadings']
        _, vg_free_pe, _ = system_out(cmd)
        return int(vg_free_pe)
    except (OSError, ValueError):
        return 0


@execute_on_zephir_context
def check_free_space(value, values):
    if None not in values and sum(values) > 100:
        raise ValueError(u'Percentage sum exceeds 100: {}'.format(sum(values)))


# nouveau_zephir : doit remonter les types de fichiers supportés pour les tests-->
def ls_fs_type():
    mkfs_cmds = re.compile(r'mkfs\.(.*)')
    cmd = ['ls', '/sbin/']
    _, cmds, _ = system_out(cmd)
    return mkfs_cmds.findall(cmds)


@execute_on_zephir_context
def is_fs_type(data, partitioning_fs_supported):
    if partitioning_fs_supported and data not in partitioning_fs_supported:
        raise ValueError(u'Les fs supportés sont : {}'.format(', '.join(partitioning_fs_supported)))
# /nouveau_zephir


def enable_lv_creation():
    minimum_PE = 100
    free_PE = calc_free_PE()
    is_instanciated = os.path.isfile('/etc/eole/.instance')
    if free_PE < minimum_PE or is_instanciated:
        return u'non'
    elif free_PE > minimum_PE and not is_instanciated:
        return u'oui'


def get_lv_names():
    """List all logical volume names

    Exclude swap since `lvextend` can not handle it.

    Do not execute on Zéphir.

    :return: the list of logical volume names

    """
    stdout = ''
    try:
        cmd = ['lvs', '-o', 'name', '--noheading', '--sort', 'lv_minor']
        _, stdout, _ = system_out(cmd)
    except OSError:
        return []

    lv_names = stdout.decode('utf-8').split()
    return [ name for name in lv_names if not name.startswith('swap') ]
