#!/bin/bash

arg=$1
[ -z "$arg" ] && arg="full"

[ "$arg" = "reconfigure" ] && echo "Prise en compte des nouveaux certificats Let’s Encrypt :"

/bin/run-parts --exit-on-error -v /usr/share/eole/letsencrypt/hooks-post.d --arg "$arg"
