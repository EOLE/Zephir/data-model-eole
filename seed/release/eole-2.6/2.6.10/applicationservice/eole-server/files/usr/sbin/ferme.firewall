#!/bin/sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.2-14) to ensure that this file is present
# and status_of_proc is working.

VERBOSE=true
CONTAINER=true

usage() {
    cat <<EOF
Usage: $(basename ${0}) [OPTIONS]

Close the firewall on the server and in LXC containers.

Options:
--------

         --no-container       Do not try to close firewall in LXC containers
     -s, --silent             Do not display messages on stdout
         --help               Show this message
EOF
}

# Option management
TEMP=$(getopt -o sh --long silent,no-container,help -- "$@")
test $? = 0 || exit 1
eval set -- "${TEMP}"

while true
do
    case "${1}" in
        # Default options for utilities
        -h|--help)
            usage
            exit 0
            ;;

        # Program options
        -s|--silent)
	    VERBOSE=false
            shift
            ;;

        --no-container)
	    CONTAINER=false
            shift
            ;;

        # End of options
        --)
            shift
            break
            ;;
        *)
            echo "Error: unknown argument '${1}'"
	    exit 1
            ;;
    esac
done

# Compatibility with old command line usage
if [ $# -eq 1 -a "$1" = 'yes' ]
then
    VERBOSE=false
fi

FORTERESSE=/usr/lib/eole/forteresse.sh

if [ -f "${FORTERESSE}" ]
then
    . ${FORTERESSE}
    forteresse_start

    if [ "${CONTAINER}" = 'true' ]
    then
	# lancement de firewall.stop sur chaque conteneur
	close_all_containers
    fi
else
    echo "Impossible d’activer le mode forteresse, serveur instancié ?"
    exit 1
fi

if [ "${VERBOSE}" = 'true' ]
then
    echo "Le serveur est protégé. Pour relancer le pare-feu, exécuter \"CreoleService bastion restart\""
fi
