#!/bin/sh
## ajout du path du repertoire Eole
PYTHONPATH=/usr/share/eole/:$PYTHONPATH
#PATH=$PATH:/usr/share/eole:/usr/sbin:/sbin
PATH=/usr/share/eole:/usr/share/eole/sbin:$PATH
# Eviter les messages d'erreurs en cas de chargement/source multiple
(unset HISTCONTROL &>/dev/null) && HISTCONTROL=ignoredups
(unset HISTFILE &>/dev/null) && HISTFILE=~/.bash_history
(unset HISTSIZE &>/dev/null) && HISTSIZE=1000
(unset HISTFILESIZE &>/dev/null) && HISTFILESIZE=9999
(unset HISTIGNORE &>/dev/null) && HISTIGNORE=""
(unset HISTTIMEFORMAT &>/dev/null) && HISTTIMEFORMAT="[ %d/%m/%Y %H:%M:%S ] "
readonly HISTFILE
readonly HISTSIZE
readonly HISTFILESIZE
readonly HISTIGNORE
readonly HISTCONTROL
readonly HISTTIMEFORMAT
export HISTFILE HISTSIZE HISTFILESIZE HISTIGNORE HISTCONTROL HISTTIMEFORMAT
(unset TMOUT &>/dev/null) && TMOUT=%%bash_tmout
export PYTHONPATH TMOUT

if  [ -z "$EDITOR" ] ; then
        export EDITOR="vi"
fi

[ -f ~/.alias ] && [ -z "$LOAD_SYSTEM_ALIASES" ]
[ -n "$IGNORE_SYSTEM_ALIASES" ]

[ -f "/etc/DIR_COLORS" ] && eval `dircolors --sh /etc/DIR_COLORS`

# default ls options
LS_OPTIONS="-F"

# this should be removed once the bug with ls and multibytes locales is fixed
[ -r /etc/profile.d/lang.sh ] && . /etc/profile.d/lang.sh
case "$LC_ALL$LC_CTYPE" in
    ja*|ko*|zh*) LS_OPTIONS="$LS_OPTIONS --show-control-chars" ;;
    *) if [ "`locale charmap`" = "UTF-8" ]; then
        LS_OPTIONS="$LS_OPTIONS --show-control-chars"
       fi ;;
esac

# emacs doesn't support color
if [ ! "$TERM" = "emacs" ];then
    LS_OPTIONS="$LS_OPTIONS --color=auto"
fi

alias ls="ls $LS_OPTIONS"

alias d="ls"
alias l="ls"       		# classical listing.
alias ll="ls -l"   		# List detailled.
alias la='ls -a'     	# List all.
alias lsd="ls -d */"		# List only the directory.
alias cd..="cd .."
alias s="cd .."
alias p="cd -"
alias du='du -h'
alias df='df -h'

alias md="mkdir"
alias rd="rmdir"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias vi="vim"
alias egrep='egrep --color'
alias fgrep='fgrep --color'
alias grep='grep --color'

alias purge="find \( -name '*.pyc' -o -name '*~' \) -delete"

# Make a filter for less
if [ -x /usr/bin/lesspipe.sh ];then
     export LESSOPEN="|/usr/bin/lesspipe.sh %s"
fi

export LESS=-MM

export PATH
export LANG="fr_FR.UTF-8"
export LC_CTYPE="fr_FR.UTF-8"
export LC_NUMERIC="fr_FR.UTF-8"
export LC_TIME="fr_FR.UTF-8"
export LC_COLLATE="fr_FR.UTF-8"
export LC_MONETARY="fr_FR.UTF-8"
export LC_MESSAGES="fr_FR.UTF-8"
export LC_PAPER="fr_FR.UTF-8"
export LC_NAME="fr_FR.UTF-8"
export LC_ADDRESS="fr_FR.UTF-8"
export LC_TELEPHONE="fr_FR.UTF-8"
export LC_MEASUREMENT="fr_FR.UTF-8"
export LC_IDENTIFICATION="fr_FR.UTF-8"
export LC_ALL="fr_FR.UTF-8"

if [ -n "$BASH" ]
then
    # Bash specific stuffis
    export PS1='\u@\h:\w\$'
    if [ -f /etc/bash.bashrc ]; then
        . /etc/bash.bashrc
    fi
   %if %%activer_bash_completion == 'non'
    complete -r
   %end if
fi
%if %%is_defined('login_graphique')
#si eclair
SDL_AUDIODRIVER='pulse'
if [ `id -u` = 0 ];then
	umask 022
else
	umask 077
fi
%else
umask 022
%end if

export PYTHONSTARTUP=/etc/pythonrc
export PYTHONIOENCODING='UTF8'

#%if %%is_defined('activer_proxy_client') and %%activer_proxy_client == 'oui'
#export http_proxy="http://%%proxy_client_adresse:%%proxy_client_port/"
#export ftp_proxy="http://%%proxy_client_adresse:%%proxy_client_port/"
#%else
#export http_proxy=""
#export ftp_proxy=""
#%end if
