#!/bin/sh

# Do not execute action for disabled service
skip_disabled() {
    if [ -d /run/systemd ] && ! systemctl is-enabled "${1}" 2>&1 /dev/null
    then
        # OK, do not run
        exit 101
    else
        # OK, run
        exit 104
    fi
}

case "${1}-${2}" in
    freeradius-*reload)
        # https://bugs.launchpad.net/ubuntu/+source/freeradius/+bug/1712817
        skip_disabled "${1}"
        ;;
    *)
        # OK, run
	exit 104
	;;
esac
