##############################################################
# sshd_config : fichier de configuration du service ssh
# Pour EOLE
#samuel morin <samuel.morin@ac-dijon.fr>
##############################################################
#OpenBSD: sshd_config

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options change a
# default value.

Port 22
Protocol 2
#Evite les erreurs "WARNING: Not loading blacklisted module ipv6"
AddressFamily inet

Ciphers chacha20-poly1305@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha1-96-etm@openssh.com,hmac-sha1-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-sha1-96,hmac-sha1

#ListenAddress

#ListenAddress ::

# HostKey for protocol version 1
#HostKey /etc/ssh/ssh_host_key
# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Logging
#obsoletes QuietMode and FascistLogging
SyslogFacility AUTH
LogLevel INFO

# Authentication:

LoginGraceTime 30

StrictModes yes

PubkeyAuthentication yes
AuthorizedKeysFile	%h/.ssh/authorized_keys

# rhosts authentication should not be used
#RhostsAuthentication no
# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes
# similar for protocol version 2
HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# RhostsRSAAuthentication and HostbasedAuthentication
IgnoreUserKnownHosts yes
%if not %%is_defined('login_graphique')
AllowGroups %slurp
 %if %%is_defined('activer_web_zephir')
uucp %slurp
 %end if
root %slurp
adm %slurp
 %for %%allow_group in %%ssh_allow_groups
%%allow_group %slurp
 %end for
%end if
%if %%getVar('activer_onenode','non') == 'oui'
 %if %%is_defined('virt_group')
%%virt_group %slurp
 %end if
%end if

%if %%ssh_permit_root == "oui"
PermitRootLogin yes
%else
PermitRootLogin no
%end if

# To disable tunneled clear text passwords, change to no here!
%if %%ssh_allow_passwd == "oui"
PasswordAuthentication yes
%else
PasswordAuthentication no
%end if

PermitEmptyPasswords no
# Change to no to disable s/key passwords
ChallengeResponseAuthentication no
PermitUserEnvironment no
AllowTcpForwarding no
ClientAliveInterval 30
ClientAliveCountMax 10

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes

#AFSTokenPassing no

# Kerberos TGT Passing only works with the AFS kaserver
#KerberosTgtPassing no

# Set this to 'yes' to enable PAM keyboard-interactive authentication
# Warning: enabling this may bypass the setting of 'PasswordAuthentication'
#PAMAuthenticationViaKbdInt yes

X11Forwarding yes
X11DisplayOffset 10
#X11UseLocalhost yes
PrintMotd no
PrintLastLog yes
TCPKeepAlive no
#UseLogin no
UsePrivilegeSeparation yes
Compression delayed

MaxStartups %%ssh_maxstartups
# no default banner path
Banner none
#VerifyReverseMapping no

#Suppress DNS verification
UseDNS no

# Allow client to pass locale environment variables
AcceptEnv LANG LANGUAGE LC_* EDITOR

UsePAM yes
# override default of no subsystems
Subsystem	sftp	/usr/lib/sftp-server

%if %%getVar('ip_machine_esclave', "non") != "non"
Match Address %%ip_machine_esclave
   PermitRootLogin yes
%end if

%if %%getVar('ip_machine_maitre', "non") != "non"
Match Address %%ip_machine_maitre
   PermitRootLogin yes
%end if

