CONTAINER_NAME=addc
CONTAINER_IP=192.0.2.2
CONTAINER_ROOTFS=/var/lib/lxc/$CONTAINER_NAME/rootfs

ConfigureSourcesList() {
    # Configuration des sources.list dans le conteneur addc
    # $1 : adresse du serveur de mise à jour
	grep -iv envole /etc/apt/sources.list > $CONTAINER_ROOTFS/etc/apt/sources.list
	cp /etc/apt/trusted.gpg.d/eole-archive-keyring.gpg $CONTAINER_ROOTFS/etc/apt/trusted.gpg.d/eole-archive-keyring.gpg
	cat > $CONTAINER_ROOTFS/etc/apt/sources.list.d/seth-samba.list <<EOF
deb http://$1/samba samba-4.7 main
EOF

}
