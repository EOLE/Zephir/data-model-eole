#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module mysql"""
from os.path import join
from shutil import copyfile
from eolesql.db_cmd import dump_sql_file2
from glob import glob
from pyeole.bareosrestore import exit_if_running_jobs, bareos_mark_path, \
    bareos_restore
from creole.client import CreoleClient

mysqlsavdir = '/home/backup/sql/'
container_path_mysql = CreoleClient().get_creole('container_path_mysql')
debian_cnf = '/etc/mysql/debian.cnf'
debian_cnf_bak = '/etc/mysql/debian.cnf.bak'

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """ldap helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)

def pre():
    print "pre mysql"

def post():
    print "post mysql"
    mysql_bdd = join(mysqlsavdir, 'mysql.sql')
    for files in glob("{0}/*.sql".format(mysqlsavdir)):
        if files == mysql_bdd:
            continue
        print "restauration de {0}".format(files)
        dump_sql_file2(files, default_file=debian_cnf_bak)
    # bdd mysql restaurée en dernier
    print "restauration de {0}".format(mysql_bdd)
    dump_sql_file2(mysql_bdd, default_file=debian_cnf_bak)

def job(jobid):
    print "Restauration mysql"
    # sauvegarde de l'actuel fichier debian.cnf
    copyfile(container_path_mysql+debian_cnf, container_path_mysql+debian_cnf_bak)
    # marquage du fichier debian.cnf
    marked_paths = bareos_mark_path(container_path_mysql + debian_cnf)
    # marquage du dossier sql
    marked_paths += bareos_mark_path(mysqlsavdir)
    # restauration des fichiers marqués
    bareos_restore(jobid, selection=marked_paths)

priority = 30
