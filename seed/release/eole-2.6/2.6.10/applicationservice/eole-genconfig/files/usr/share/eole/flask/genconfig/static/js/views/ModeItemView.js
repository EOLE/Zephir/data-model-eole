define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'li',
    className: 'tag',
    template: templates.modeItemView,

    ui: {
      link: 'a'
    },

    initialize: function(){
      this.bindTo(this.model, 'change:current', this.render, this);
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id
      });
    },

    templateHelpers: {
      url: function(){
        return '#modes/'+ this.id;
      },

      getMode: function(){
        switch(this.name){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
