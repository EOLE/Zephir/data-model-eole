define(['marionette','vent','templates','strings','views/ModesView','lib/jquery.fileupload'],
function (Marionette,vent,templates,strings,ModesView) {
  "use strict";

  return Marionette.ItemView.extend({

    tagName: 'div',
    className: 'navbar-inner',
    template : templates.header,

    ui: {
      nav: 'ul',
      menu: '#top_menu .top-menu',
      fileinput: 'input[type=file]',
      loading: 'span.loading i.icon-spin'
    },

    events: {
      'click .validateConfig': 'validateConfig',
      'click .discardConfig': 'discardConfig',
      'click .downloadConfig': 'downloadConfig',
      'click .reloadConfig' : 'reloadConfig',
      'click .zephirConfig' : 'zephirConfig',
      'click .quitGenConfig' : 'quitGenConfig',
      'click .wizard': 'wizard',
      'click .legend': 'legend'
    },

    initialize: function(){
      this.bindTo(vent, 'loading:start', this.loadingStart, this);
      this.bindTo(vent, 'loading:stop', this.loadingStop, this);
    },

    onRender: function(){
      this.ui.menu.append(new ModesView(this.options).render().el);
      this.ui.fileinput.fileupload({
        url: app.config.path_prefix+'/upload',
        add: _.bind(function(e, data) {
          Backbone.View.goTo('uploadConfig');
          data.submit();
        }, this),
        done: _.bind(function(fi, response){
          vent.trigger('route:reloadConfig', function(){
            _.each(response.result.errors, function(error){
              vent.trigger('flash', error, 'warning');
            });
            var msg;
            app.config.version = response.result.version;
            if(app.config.version != ''){
              msg = 'La configuration "'+response.result.filename+'" a été correctement importée depuis un fichier en version '+app.config.version+'. ';
              msg+= "<a href=\"#save\" class=\"btn btn-success\" onclick=\"vent.trigger('route:validateConfig'); return false;\">Enregistrer cette configuration</a>";
              //app.config.version = ''
            } else {
              msg = t('import_config_success', response.result.filename)
            }
            vent.trigger('flash', msg, 'success');
          });
        }, this),
        fail: _.bind(function(fi, response){
          if (response.jqXHR.responseText === "")
            if (response.errorThrown !== "")
              response.jqXHR.responseText = response.errorThrown;
            else
              response.jqXHR.responseText = "l'utilisateur n'a pas les permissions suffisantes pour lire le fichier !";
          vent.trigger('flash', "Impossible d'importer le fichier : " + response.jqXHR.responseText, 'error', true);
        }, this)
      });
    },

    serializeData: function(){
      return $.extend(Marionette.View.prototype.serializeData.call(this), {
        title: 'GenConfig', //this.options.title,
        username: this.options.user.get('name'),
        application_mode: this.options.application_mode,
        zephir_mode: this.options.zephir_mode,
        zephir: this.options.user.get('zephir'),
        path_prefix: this.options.path_prefix,
        app_location : this.options.app_location
      });
    },

    validateConfig: function(){
      vent.trigger('route:validateConfig');
    },

    discardConfig: function(){
      vent.trigger('route:discardConfig');
    },

    downloadConfig: function(){
      vent.trigger('route:downloadConfig');
    },

    reloadConfig: function(){
      vent.trigger('route:reloadConfig');
    },

    zephirConfig: function(){
      vent.trigger('route:zephirConfig');
    },

    wizard: function(){
      vent.trigger('route:init');
    },

    legend: function(){
      vent.trigger('route:showLegend');
    },

    loadingStart: function(){
      this.ui.loading.show();
    },

    loadingStop: function(){
      this.ui.loading.hide();
    },

    quitGenConfig: function(){
      if(confirm(t('exit_confirm')))
        $.ajax({
          type: 'POST',
          async: false,
          data: { kill: true },
          url: app.config.path_prefix+"/quit"
        });
    },

    templateHelpers: {
      current_locale: function(){
        var locale = localStorage.getItem('locale');
        switch(locale){
          case 'en-us': return '<i class="icon-locale-en"></i> ' + '<span class="visible-desktop">' + t('english') + '<b class="caret"></b></span>';
          case 'fr-fr': return '<i class="icon-locale-fr"></i> ' + '<span class="visible-desktop">' + t('french') + '<b class="caret"></b></span>';
        }
      }
    }
  });
});
