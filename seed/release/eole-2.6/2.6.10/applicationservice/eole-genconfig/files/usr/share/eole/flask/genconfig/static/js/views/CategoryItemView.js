define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'li',
    template: templates.categoryItemView,

    ui: {
      link: 'a'
    },

    onRender: function(){
      this.ui.link.tooltip();
      this.ui.link.hover(function(){
        var $tooltip = $(this).parent().find('.tooltip');
        $tooltip.css('top',parseInt($tooltip.css('top')) - 22 + 'px')
      });
    },

    templateHelpers: {
      url: function(){
        return '#categories/'+ this.id;
      },
      getMode: function(){
        switch(this.mode){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
