define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'div',
    className: 'modal hide fade small',
    template: templates.legend,

    events: {
      'click .closeDialog' : 'closeDialog'
    },

    initialize: function(){
    },

    closeDialog: function(){
      this.$el.modal('hide');
      this.remove();
    }
  });
});
