define(['marionette'],function(Marionette) {
  'use strict';

  return Marionette.AppRouter.extend({
    appRoutes: {
      'categories/:id'         : 'showCategory', 
      'variables/:id/edit'     : 'editVariable',
      'modes/:id'              : 'setMode',
      'variables/:id/addValue' : 'addValue',
      'validateConfig'         : 'validateConfig',
      'saveConfig'             : 'saveConfig',
      'discardConfig'          : 'discardConfig',
      'downloadConfig'         : 'downloadConfig',
      'uploadConfig'           : 'uploadConfig',
      'set/locale/:locale'     : 'setLocale'
    }
  });
});
