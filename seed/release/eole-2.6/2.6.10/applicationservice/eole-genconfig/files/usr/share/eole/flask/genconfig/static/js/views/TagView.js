define(['vent','marionette','templates','views/VariableRowView','views/LoadingMessageView'],
function(vent,Marionette,templates,VariableRowView,LoadingMessageView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'div',// 'table',
    className: 'row-fluid', //'table table-striped',
    template: templates.tagView,
    itemView: VariableRowView,
    emptyView: LoadingMessageView,
    itemViewContainer: 'tbody',

    initialize: function(){
      this.itemViewOptions = {
        categories: this.options.categories,
        debug: this.options.debug
      };
      this.collection = this.model.variables;
      //this.bindTo(this.collection, 'reset', this.render, this);
      //this.bindTo(this, 'before:render', function(){ this.$('tbody').empty(); }, this);
    }


    /*appendHtml: function(collectionView, itemView){
      var separator = itemView.model.get('separator');
      if(separator) collectionView.$('tbody.variables').append(templates.separatorView({ separator: separator }));
      collectionView.$('tbody.variables').append(itemView.el);
    }*/
 });
});
