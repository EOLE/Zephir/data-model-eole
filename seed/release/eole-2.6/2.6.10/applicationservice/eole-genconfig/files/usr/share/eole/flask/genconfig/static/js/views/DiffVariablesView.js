define(['vent','marionette','templates','views/DiffVariableView'],
function(vent,Marionette,templates,DiffVariableView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'div',
    className: 'container-fluid',
    template: templates.diffVariablesView,
    itemView: DiffVariableView,
    itemViewContainer: 'tbody',
    itemViewOptions: {},
    emptyView: Marionette.ItemView.extend({
      tagName: 'tr',
      template: templates.emptyDiffView,
      serializeData: function(){
        return _.extend(Marionette.ItemView.prototype.serializeData.call(this),{
          zephir: this.options.zephir,
          config: app.config
        });
      }
    }),

    ui: {
      saveBtn: '.saveConfig'
    },

    events: {
      'click .saveConfig'    : 'saveConfig',
      'click .discardConfig' : 'discardConfig',
      'click .localConfig'   : 'localConfig',
      'click .distantConfig' : 'distantConfig',
      'click .backConfig'    : 'backConfig'
    },

    initialize: function(){
      this.itemViewOptions = _.extend({}, this.itemViewOptions, this.options);
      this.collection = this.options.differences;
      this.scrollPos = 0;
      this.bindTo(vent, 'saveScrollPos', this.saveScrollPos, this);
    },

    saveConfig: function(){
      //this.$el.modal('hide');
      if(this.ui.saveBtn.hasClass('disabled')) return;
      this.ui.saveBtn.button('loading');
      $.post(this.options.path_prefix+'/save', _.bind(function(){
        app.config.version = "";
        vent.trigger('route:reloadConfig', function(){
          vent.trigger('flash', t('save_config_success'), 'success');
        });
      }, this))
      .fail(_.bind(function(response){
        this.ui.saveBtn.button('reset');
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
      vent.trigger('route:saveConfig');
    },

    backConfig: function(){
      //this.$el.modal('hide');
      vent.trigger('route:reloadConfig');
    },

    discardConfig: function(){
      //this.$el.modal('hide');
      vent.trigger('route:discardConfig');
    },

    localConfig: function(){
      //this.$el.modal('hide');
      $.post(this.options.path_prefix+'/save', { source: 'local' })
      .done(_.bind(function(){
        app.config.version = "";
        vent.trigger('route:reloadConfig', function(){
          vent.trigger('flash', t('save_config_success'), 'success');
        });
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
      vent.trigger('route:localConfig');
    },

    distantConfig: function(){
      //this.$el.modal('hide');
      $.post(this.options.path_prefix+'/save', { source: 'distant' })
      .done(_.bind(function(){
        app.config.version = "";
        vent.trigger('route:reloadConfig', function(){
          vent.trigger('flash', t('save_config_success'), 'success');
        });
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
      vent.trigger('route:distantConfig');
    },

    saveScrollPos: function(pos){
      this.scrollPos = pos;
    },

    onRender: function(){
      window.scrollTo(0, this.scrollPos);
    },

    serializeData: function(){
      return _.extend(Backbone.Marionette.CompositeView.prototype.serializeData.call(this),{
        zephir: this.options.zephir,
        path_prefix: this.options.path_prefix,
        app_location: this.options.app_location,
        application_mode: this.options.application_mode,
        config: app.config
      });
    }
  });
});
