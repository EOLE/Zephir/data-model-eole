define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    template: templates.loadingMessageView
  });
});
