define(['marionette'],function(Marionette){
  "use strict";

  window.vent = new Marionette.EventAggregator();

  var pending = 0;
  var timer;

  $(window).load(function () {
  }).ajaxComplete(function(){
    pending--;
    clearTimeout(timer);
    if(pending == 0) vent.trigger('loading:stop');
  });

  var s_ajaxListener = new Object();
  s_ajaxListener.tempOpen = XMLHttpRequest.prototype.open;
  s_ajaxListener.tempSend = XMLHttpRequest.prototype.send;
  s_ajaxListener.callback = function () {
    // this.method :the ajax method used
    // this.url    :the url of the requested script (including query string, if any) (urlencoded)
    // this.data   :the data sent, if any ex: foo=bar&a=b (urlencoded)
  }

  XMLHttpRequest.prototype.open = function(a,b) {
    if (!a) var a='';
    if (!b) var b='';
    s_ajaxListener.tempOpen.apply(this, arguments);
    s_ajaxListener.method = a;
    s_ajaxListener.url = b;
    if (a.toLowerCase() == 'get') {
      s_ajaxListener.data = b.split('?');
      s_ajaxListener.data = s_ajaxListener.data[1];
    }
    pending++;
    timer = setTimeout(function(){
      vent.trigger('loading:stop');
    }, 30000);
    if(pending == 1)
      vent.trigger('loading:start');
  }

  XMLHttpRequest.prototype.send = function(a,b) {
    if (!a) var a='';
    if (!b) var b='';
    s_ajaxListener.tempSend.apply(this, arguments);
    if(s_ajaxListener.method.toLowerCase() == 'post')s_ajaxListener.data = a;
    s_ajaxListener.callback();
  }


  return vent;
});
