define(['vent','marionette','templates','collections/Variables','models/Tag','collections/Tags','helpers/util'],
function(vent,Marionette,templates,Variables,Tag,Tags){
  'use strict';

  return Backbone.Model.extend({

    defaults : {
      name : 'Category Name',
      help : "Click to configure variables in this category" // InfoBulle
    },

    initialize: function(){
      this.tags = new Tags([], { categoryid: this.id });
      this.on('change:tags',function() { this.tags.reset(this.get('tags')); }, this);
      this.tags.on('reset',this.adoptAll, this);
      this.tags.on('add',this.adoptOne, this);
      this.tags.reset(this.get('tags'));
    },

    adoptAll:function() {
      this.tags.each(this.adoptOne, this);
    },

    adoptOne:function(tag) {
      tag.set('category', this, { silent: true });
      tag.set('categoryid', this.id, { silent: true});
    },

    toJSON: function(){
      if(this._isSerializing) return this.id || this.cid;
      this._isSerializing = true;
      var json = _.extend(_.clone(this.attributes), {
        //tags: this.tags.toJSON()
      });
      this._isSerializing = false;
      return json;
    }
  });
});
