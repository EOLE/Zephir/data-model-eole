define(['vent','marionette','templates','lib/bootstrap-select'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.Layout.extend({

    tagName: 'div',
    className: 'control-group',
    template: templates.variableItemView,

    ui: {
      input: '.value',
      link: 'a, span',
      msg: '.msg'
    },

    events: {
      'focus .value'      : 'onEditClick',
      'click .calculated' : 'onEditClick'
    },

    initialize: function(){
      this.bindTo(this.model, 'change', this.render, this);
    },

    onRender: function(){
      this.ui.link.tooltip();
      this.ui.input.tooltip();
      this.ui.msg.hide();
      this.$('.selectpicker').selectpicker();
      if(this.model.get('type') == 'choice'){
        var selectTag = this.$('.bootstrap-select').find('button');
        selectTag.attr({ rel:"tooltip", 'data-placement': "top", 'data-toggle': "tooltip", title: this.model.get('help') });
        selectTag.tooltip();
      }
      this.bindTo(this.model, 'error', this.error, this);
      this.bindTo(this.model, 'error:cancel', this.errorCancel, this);
    },

    onEditClick: function(){
      Backbone.View.goTo('variables/'+this.model.id+'/edit');
    },

    error: function(msg){
      this.$el.addClass('error');
      this.ui.msg.show().empty()
        .append($('<i>').addClass('icon-warning-sign'))
      this.ui.input.val(this.model.get('value'));
    },

    errorCancel: function(){
      this.$el.removeClass('error');
      this.ui.msg.empty().hide();
      this.ui.input.val(this.model.get('value'));
    },


    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id,
        debug: this.options.debug,
        model: this.model.toJSON()
      });
    },

    templateHelpers: {
      getValue: function(){
        if(this.value)
          if(this.value != "") return this.value;
          else return "Empty String";
        else if (this.value == 0) return 0
          else return "";
      }
    }
  });
});
