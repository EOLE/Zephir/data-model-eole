define(['marionette','templates','vent',
'views/Header','views/Body','views/Footer','views/FillMandatoriesView','views/DiffVariablesView',
'views/DiscardConfigView','views/LegendView',
'collections/Categories','collections/Modes','collections/Variables','models/User'],
function (Marionette,templates,vent,Header,Body,Footer,
FillMandatoriesView,DiffVariablesView,DiscardConfigView,LegendView,
Categories,Modes,Variables,User){
  "use strict";

  window.app = new Marionette.Application();

  var prmstr = window.location.search.substr(1);
  var prmarr = prmstr.split ("&");
  var params = {};
  for(var i = 0; i < prmarr.length; i++){
    var tmparr = prmarr[i].split("=");
    params[tmparr[0]] = decodeURIComponent((''+tmparr[1]).replace(/\+/g, '%20'));
  }

  app.config = {
    application_mode : /application/.test(window.location.search),
    zephir_mode      : /zephir/.test(window.location.search),
    eole: {
        module : 'Eole',
        version: '2.4'
    },
    zephir: {
        sync: false,
        available: false,
        server_id: null,
        server_ip: null
    },
    location    : window.location.hash,
    prevent_quit: false,
    debug       : false,
    title       : params['title'] ? params['title'] : 'GenConfig',
    path_prefix : location.pathname.replace(/\/+$/, ''), // Prefix des urls de sync, ex: "/genconfig"
    app_location : location.origin, // adresse externe du serveur (utile en cas de reverse_proxy)
  };

  window.location = '#';

  app.addRegions({
    header : '#header',
    body   : '#container',
    footer : '#footer',
    modal  : '#modal'
  });

  app.addInitializer(function(){
    _.extend(app.config, {
      categories : new Categories(),
      modes      : new Modes(),
      mandatories: new Variables(),
      differences: new Variables(),
      user       : new User()
    });
    if(!app.config.application_mode){
        document.title = app.config.title;
    }
    app.body.show(new Body(app.config));
    app.body.currentView.setElement($("#container"));
    app.footer.show(new Footer(app.config));

    app.config.user.fetch({
        success:_.bind(function(model, options){
            app.config.prevent_quit = true;
            app.config.zephir = app.config.user.get('zephir');
            app.header.show(new Header(app.config));
            app.config.modes.fetch({
              success: function(){ vent.trigger('route:getDebug'); },
              error: _.bind(function(collection, response){
                new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
              }, this)
            });
        }, this),
        error: function(model, response) {
            if (!app.config.application_mode || response.statusText != 'INTERNAL SERVER ERROR') {
                new ErrorView({ model: new Backbone.Model(response)}).render().$el.modal('show');
            } else {
                new ErrorView({ model: new Backbone.Model(response), option: app.config}).render().$el.modal('show');
            }
        }
//        error: function(){ window.location.reload(); }
    });

    app.bindTo(vent, 'route:reloadConfig', function(callback){
      app.config.categories.fetch({
        success: _.bind(function(collection){
          if(!app.config.current_category){
            if(/#categories\//.test(app.config.location)){
              Backbone.View.goTo(app.config.location, callback);
              return;
            } else
              app.config.current_category = collection.first().id;
            if (app.config.current_category == 'containers')
              app.config.current_category = 'general';
          }
          Backbone.View.goTo('categories/'+app.config.current_category, callback);
          //vent.trigger('route:showCategory', app.config.current_category, callback);
        }, this),
        error: _.bind(function(collection, response){
          new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
        }, this)
      });
    }, app);

    app.bindTo(vent, 'route:validateConfig', function(callback){
      $.post(app.config.path_prefix+'/validate', {}, _.bind(function(response){
        if(response.status == 'validate'){
          app.config.mandatories.reset(response.variables);
          var test = new FillMandatoriesView(app.config);//.render().$el.modal('show');
          app.body.currentView.main.show(test);
        } else
        if(response.status == 'diff') {
          app.config.differences.reset(response.variables);
          var test = new DiffVariablesView(app.config);//.render().$el.modal('show');
          app.body.currentView.main.show(test);
        }
        if(_.isFunction(callback)) callback();
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
    }, app);

    app.bindTo(vent, 'route:zephirConfig', function(callback){
      $.post(app.config.path_prefix+'/validate', { zephir_sync: true }, _.bind(function(response){
        app.config.categories.fetch({ data: { zephir_sync: false } });
        var msg, msg2;
        if(response.status == 'init'){
          vent.trigger('change:debug');
          vent.trigger('route:reloadConfig');
        } else if (response.status == 'zephir_empty') {
          msg = "La configuration n'existe pas sur le serveur Zéphir (ID: ";
          msg += app.config.zephir.server_id;
          msg += "/ Adresse : " + app.config.zephir.server_ip+")";
          msg2 = "Les modifications apportées à la configuration à partir de ce point seront sauvegardées localement ";
          msg2 += "et simultanément sur le serveur distant.";
          vent.trigger('route:reloadConfig', function(){
            vent.trigger('flash', msg2, 'warning');
            vent.trigger('flash', msg, 'error');
          });
        } else {
          if(response.status == 'validate'){
            app.config.mandatories.reset(response.variables);
            var test = new FillMandatoriesView(_.extend({}, app.config, { zephir: { sync: true }}));//.render().$el.modal('show');
            app.body.currentView.main.show(test);
          } else if(response.status == 'diff') {
            app.config.differences.reset(response.variables);
            var test = new DiffVariablesView(_.extend({}, app.config, { zephir: { sync: true }}));//.render().$el.modal('show');
            app.body.currentView.main.show(test);
          }
          msg = "La configuration actuelle a correctement été importée depuis le serveur Zéphir (ID: ";
          msg += app.config.zephir.server_id;
          msg += "/ Adresse : " + app.config.zephir.server_ip+")";
          msg2 = "Les modifications apportées à la configuration à partir de ce point seront sauvegardées localement ";
          msg2 += "et simultanément sur le serveur distant.";
          vent.trigger('flash', msg2, 'warning');
          vent.trigger('flash', msg, 'info');
          if(_.isFunction(callback)) callback();
        }
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
    }, app);
    app.bindTo(vent, 'route:showLegend', function(){
      new LegendView().render().$el.modal('show');
    });

    app.bindTo(vent, 'route:downloadConfig', function(){
      $.get(app.config.path_prefix+'/download', {}, _.bind(function(response){
        app.config.prevent_quit = false;
        //window.location = app.config.path_prefix+'/download';
        var iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = app.config.path_prefix+'/download';
        if(app.config.application_mode) vent.trigger('flash', 'Le fichier a correctement été exporté dans /var/lib/genconfig/Downloads', 'success');
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
    }, app);

    app.bindTo(vent, 'route:discardConfig', function(){
      new DiscardConfigView(app.config).render().$el.modal('show');
    }, app);

    app.bindTo(vent, 'route:getDebug', function(){
      $.get(app.config.path_prefix+'/getDebug', {}, _.bind(function(response){
        app.config.debug = response.debug;
        app.config.eole = response.eole;
        app.config.version = response.version;
        console.log("response", response);
        var cb = function() {
          if(response.errors){
            _.each(response.errors, function(error){
              vent.trigger('flash', error, 'warning');
            });
          }
        }
        if(app.config.zephir.available){
          vent.trigger('route:zephirConfig', cb);
        } else if(response.init){
          vent.trigger('route:init', cb);
        } else {
          vent.trigger('change:debug');
          vent.trigger('route:reloadConfig', cb);
        }
        if(app.config.version != ''){
          var msg = "La configuration a été correctement chargée depuis un fichier issu d'une version "+app.config.version+". ";
          msg+= "<a href=\"#save\" class=\"btn btn-success\" onclick=\"vent.trigger('route:validateConfig'); return false;\">Enregistrer cette configuration</a>";
          vent.trigger('flash', msg, 'success');
          //app.config.version = ''
        }
      },this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
    }, app);

    app.bindTo(vent, 'route:toggleDebug', function(){
      $.post(app.config.path_prefix+'/setDebug', { debug: !app.config.debug }, _.bind(function(response){
        app.config.debug = response.debug;
        vent.trigger('change:debug');
        vent.trigger('route:reloadConfig');
      }, this))
      .fail(_.bind(function(response){
        new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
      }, this));
    }, app);

  });

  $(window).scroll(function() {
    vent.trigger('saveScrollPos', window.pageYOffset);
  });

  $(window).on('unload', function(e){
    if($.browser.webkit){
      $.ajax({
        type: 'POST',
        url: app.config.path_prefix+"/quit",
        async: false
      })
    }
  });

  $(window).on("pagehide", function(){
  });

  $(window).on('beforeunload', function(e){
    if(app.config.prevent_quit){
      var e = e || window.event || {};
      if($.browser.mozilla){
        var test = confirm(t('exit_confirm'));
        if(test) {
          $.ajax({
            type: 'POST',
            url: app.config.path_prefix+"/quit",
            async: false
          });
          history.go();
        }
        else window.setTimeout(function() {
          window.stop();
          return null;
        }, 1);
      } else {
        return t('exit_confirm');
      }
    } else {
      app.config.prevent_quit = true;
    }
  });

  return app;
});


