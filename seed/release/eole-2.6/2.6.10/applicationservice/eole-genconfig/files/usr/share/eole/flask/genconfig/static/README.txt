Event Handling : 
----------------

- onBlur 		=> Save IF (value != prev_value)
			=> After Save 
				=> Reload Categories
				=> Reload Current Category Variables

- onEnterKeyDown 	=> Save IF (value != prev_value) OR (default_owner) 
			=> Prevent Default 
			=> StopPropagation
			=> After Save 
				=> Reload Categories
				=> Reload Current Category Variables

- onTabKeyDown		=> Save IF (value != pref_value) 
			=> Prevent Default 
			=> StopPropagation
			=> After Save 
				=> Reload Categories
				=> Reload Current Category Variables
				=> Edit Next/Previous Variable
Variables Types : 
-----------------

- String
- Integer
- IP
- Choice
- OpenChoice
- Multi [String|Integer|IP|Choice|OpenChoice]
- Group [Master|Slaves]


Variable View :
---------------

define(['vent','templates','kb'],
function(vent,  templates,  kb){


return Marionette.ItemView.extend({

  tagName: 'div',

  template: templates.variable,
  
  ui: {
    input: 'input[name="value"], 
           select[name="value"]'
  },

  events: {
    'blur    input[name="value"],
             select[name="value"]': 'onBlur',
    'keyup   input[name="value"],
             select[name="value"]': 'onKeyup',
    'keydown input[name="value"],
             select[name="value"]': 'onKeydown'
  }

  modelEvents: {},
  collectionEvents: {},
  callbacks: new Backbone.Marionette.Callbacks(),

  initialize: function(){

  },

  onRender: function(){
    switch(model.getType()){
      case 'string': 	
        this.ui.input.attr({ type: 'text' });
        break;
      case 'int': 	
        this.ui.input.attr({ type: 'number' });
        break;
      case 'ip': 
        this.ui.input.attr({ type: 'text' });
        break;
      case 'choice': 
        this.ui.input.attr({ type: 'text' }).select();
      
      case 'string': this.ui.input.attr({ type: 'text' });
    }
  },

  sync: false,

  onBlur: function(e){
    if(this.model.get('value') != this.ui.input.val() && !this.sync)
      this.save();
  },

  onKeyup: function(e){
  
  },

  onKeydown: function(e){
    switch(e.keyCode){
      case kb.TAB:
        if(this.model.get('value') != this.ui.input.val())
          this.save(callback);
        else
          callback();
        break;
      case kb.ENTER:
        if(this.model.get('value') != this.ui.input.val() || this.model.get('defaut_owner'))
          this.save();
        break;
    }
  },

  save: function(callback){
    this.model.save({ value: this.ui.input.val() },{
      success : _.bind(this.onSuccess),
      error   : _.bind(this.onError)
    });
  },

  onSuccess: function(model, reponse){
   
  },

  onError: function(model, response){
    this.ui.saveBtn.button('reset');
    this.$el.addClass('error');
    this.ui.msg.show().empty()
      .append($('<i>').addClass('icon-warning-sign'))
      .append(' '+response.responseText);
    this.focus();
  },

  templatesHelper: {

  }

});
