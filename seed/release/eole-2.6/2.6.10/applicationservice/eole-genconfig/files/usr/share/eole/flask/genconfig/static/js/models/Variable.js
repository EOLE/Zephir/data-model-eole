define(['vent','collections/Variables'],
function(vent,Variables){
  'use strict';

  return Backbone.Model.extend({

    urlRoot: '/variables',

    defaults: {
      name: null, //'new_variable',
      description: null, //'New Variable',
      mandatory: false,
      multi: false,
      masters: [],
      slaves: [],
      editable: true,
      value: null,
      old_value: null,
      new_value: null,
      type: 'unicode',
      open: false,
      categoryid: null,
      category: null,
      tagid: null,
      tag: null,
      default_owner: true, // Valeur calculée, couleur différente
      owner: null, //'anonymous',
      help: '',            // Infobulle
      auto_freeze: false,  // Valeur à définir une seule fois,
      isSlave: false,
      isMaster: false,
      old_user: null,
      new_user: null,
      warning: null,
      error: null,
      mode: 'normal',
      hidden: false,
      groupid: null,
      group: null,
      index: null
    },

    initialize: function(options){
        this.options = options || {};
        if(typeof Variables === 'undefined') Variables = require('collections/Variables');
        this.slaves = new Variables([], { categoryid: this.get('categoryid') });
        this.masters = new Variables([], { categoryid: this.get('categoryid') });
        this.on('change:slaves',function() { this.slaves.reset(this.get('slaves')); }, this);
        this.on('change:masters',function() { this.masters.reset(this.get('masters')); }, this);
        this.slaves.on('reset',this.adoptAll, this);
        this.masters.on('reset',this.adoptAll, this);
        this.slaves.on('add',this.adoptOne, this);
        this.masters.on('add',this.adoptOne, this);
        this.slaves.reset(this.get('slaves'));
        this.masters.reset(this.get('masters'));
        if(this.get('open')){
          if(!_.contains(this.get('choices'),this.get('value')) && !(this.get('value') instanceof Array) && this.get('value') != null){
            var choices = _.clone(this.get('choices'));
            choices.push(this.get('value'));
            this.set('choices', choices);
          }
        }
        this.on('change:tag', function() {
          this.slaves.each(function(slave){
            slave.set('tag', this.get('tag'));
          }, this);
          this.masters.each(function(master){
            master.set('tag', this.get('tag'));
          }, this);
        });
    },

    adoptAll:function() {
        if(this.slaves.length) {
           this.set('masterid', this.id);
           this.slaves.each(this.adoptOne, this);
        }
        if(this.masters.length) {
           this.masters.each(this.adoptOne, this);
        }
    },

    adoptOne:function(model) {
        model.set('categoryid', this.get('categoryid'));
        model.set('category', this.get('category'));
        model.set('tagid', this.get('tagid'));
        model.set('tag', this.get('tag'));
        model.set('group', this);
    },

    toJSON: function(){
        if(this._isSerializing) return this.id || this.cid;
        this._isSerializing = true;
        var json = _.extend(_.clone(this.attributes), {
          //tag: this.get('tag').toJSON(),
          //category: this.get('category').toJSON()
        });
        this._isSerializing = false;
        return json;
    },

    getType: function(){
      if(this.get('isSlave') || this.get('isMaster')) return this.get('type');
      //else if(this.slaves.length > 0) return 'group';
      else if(this.get('type') == 'group') return 'group';
      else if(this.get('multi')) return 'multi';
      else return this.get('type')
    }
  });
});
