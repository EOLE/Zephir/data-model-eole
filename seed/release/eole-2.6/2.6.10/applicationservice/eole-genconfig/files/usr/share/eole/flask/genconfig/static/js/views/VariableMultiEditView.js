define(['vent','marionette','templates','lib/bootstrap-select','lib/jquery-input-ip-address-control'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName   : 'div',
    className : 'row-fluid',
    template: templates.variableMultiEditView,

    ui: {
      input      : 'input[name=value], select[name=value]',
      saveBtn    : '.btn.save',
      resetBtn   : '.btn.reset',
      msg        : '.msg',
      control    : '.control-group',
      link       : 'a',
      controlBtn : '.btn.btn-control'
    },

    events: {
      'keyup'                   : 'onKeyup',
      'keydown'                 : 'onKeydown',
      'blur input[name=value]'  : 'onBlur',
      'blur select[name=value]' : 'onBlur',
      'mousedown .save'         : 'onMouseDown',
      'mousedown .cancel'       : 'onMouseDown',
      'mousedown .addItem'      : 'onMouseDown',
      'mousedown .removeItem'   : 'onMouseDown',
      'mousedown .reset'        : 'onMouseDown',
      'mousedown .moveItem'     : 'onMouseDown',
      'mouseup'                 : 'onMouseUp',
      'click .save'         : 'onSave',
      'click .cancel'       : 'onCancelClick',
      'click .addItem'      : 'addItem',
      'click .removeItem'   : 'removeItem',
      'click .reset'        : 'resetValue',
      'click .moveItem'     : 'moveItem'
    },

    onSave: function(e){
      e.preventDefault();
      Backbone.View.goTo('/variables/'+this.model.id+'/save');
      this.save();
    },

    onMouseDown: function(e){
      this.freezeBlur = true;
    },

    onMouseUp: function(e){
      this.freezeBlur = false;
    },

    initialize: function(){
      this.freezeBlur = false;
      this.model.set('_value', this.model.get('value'));
      this.bindTo(this.model, 'error', this.error, this);
      this.bindTo(this.model, 'change', this.render, this);
      this.bindTo(this.model, 'edit', this.focus, this);
      this.bindTo(vent, 'route:editVariable', this.editVariable , this);
    },

    onRender: function(){
      this.focus();
      this.ui.msg.hide();
      this.ui.link.tooltip();
      this.ui.input.tooltip();
      if(this.model.get('open')){
        this.$('.selectpicker').attr({ placeholder: this.model.get('value') });
        this.$('.selectpicker').select2({ open_value: true, width: "220px"});
      } else
        this.$('.selectpicker').selectpicker({ size: 6 });
      var empty = (this.ui.input.val() == ''
          ||(this.ui.input.val() == '___.___.___.___'
          && this.model.get('type') == 'ip'));
      this.renderControlBtn(empty);
      if(this.model.get('type') == 'ip')
        this.ui.input.ipAddress();
      if(this.model.get('type') == 'choice'){
        var selectTag = this.$('.bootstrap-select');
        selectTag.attr({ rel:"tooltip", 'data-placement': "top", 'data-toggle': "tooltip", title: this.model.get('help') });
        selectTag.tooltip();
      }
    },

    renderControlBtn: function(save){
      if(save){
        this.ui.controlBtn.removeClass('btn-success addItem').addClass('btn-primary save').attr({
          title: t('save'),
          href: '#/variables/'+this.model.id+'/save'
        }).html($('<i>').addClass('icon-ok'));
      } else {
        this.ui.controlBtn.removeClass('btn-primary save').addClass('btn-success addItem').attr({
          title: t('add_value'),
          href: '#/variables/'+this.model.id+'/addItem'
        }).data({'original-title' : t('add_value') }).html($('<i>').addClass('icon-plus'));
      }
      this.ui.link.tooltip();
    },

    editVariable: function(id){
      if(id == this.model.id) this.focus();
      else this.cancel();
    },

    addItem: function(callback){
      this.ui.input.trigger('makeip');
      var value = this.ui.input.val();
      if(value == "") value = null;
      this.ui.msg.hide();
      if(value){
	      //if(!_.contains(this.model.get('value'), value)){
          var values = _.clone(this.model.get('value'));
	        values.push(value);
	        this.model.save({ 'value': values },{
            success: _.bind(function(){
              this.focus();
              this.model.trigger('error:cancel');
              //this.model.set('_value', this.model.get('value'));
              if(_.isFunction(callback)) callback();
            }, this),
            error: _.bind(function(model, response){
              //this.model.set('value', this.model.get('_value'));
              this.ui.input.val(value);
              this.ui.input.trigger('makeip');
              //this.error(response.responseText);
              this.model.trigger('error', response.responseText);
            }, this)
          });
	      //} else {
          //this.model.trigger('error', t('already_exists'));
          //this.error('Already exists');
        //}
      }
    },

    removeItem: function(e){
      var id = $(e.currentTarget).data('id');
      var values = _.clone(this.model.get('value'));
      values.splice(id, 1);
      this.model.save({ 'value': values },{
        success: _.bind(function(model, response){
          //this.model.set('_value', this.model.get('value'));
        }, this),
        error: _.bind(function(model, response){
          this.model.trigger('error', response.responseText);
          //this.model.set('value', this.model.get('_value'));
        }, this)
      });
    },

    editItem: function(e){
      var $target = $(e.currentTarget)
      var id = $target.data('id');
      var values = _.clone(this.model.get('values'));
      $target.html($('<input>').attr({type: 'text'}));
    },

    moveItem: function(e){
      var id = $(e.currentTarget).data('id');
      var values = _.clone(this.model.get('value'));
      var tmp = values[id];
      values[id] = values[id+1];
      values[id+1] = tmp;
      this.model.save({ 'value': values },{
        success: _.bind(function(model, response){
          //this.model.set('_value', this.model.get('value'));
        }, this),
        error: _.bind(function(model, response){
          this.model.trigger('error', response.responseText);
          //this.model.set('value', this.model.get('_value'));
        }, this)
      });
    },

    resetValue: function(callback){
      this.ui.resetBtn.button('loading');
      this.model.save({ default_owner: true },{
        url: this.model.url()+'/reset',
        success: _.bind(function(model){
          if(this.options.isMandatoryCollection)
            //this.options.mandatories.resetAll(callback);
            vent.trigger('route:validateConfig', callback);
          else
            this.options.categories.fetch({
              success: _.bind(function(collection){
                this.sync = false;
                try {
                  this.model.get('tag').get('category').tags.fetch({ 
                    success: _.bind(function(tags){
                      var bkp_location = window.location.hash;
                      this.onCancelClick();
                      this.ui.saveBtn.button('reset');
                      if(tags.length == 0)
                        Backbone.View.goTo('categories/general');
                      else {
                        Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                        if(_.isFunction(callback)) callback();
                        //else Backbone.View.goTo(bkp_location);
                      }
                    }, this),
                    error: _.bind(function(collection, response){
                      this.model.trigger('error', response.responseText);
                      //new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
                    }, this)
                  });
                } catch(e) {
                  vent.trigger('route:reloadConfig', callback);
                }
              }, this)
            });
        }, this),
        error: _.bind(function(model, response){
          this.model.trigger('error', response.responseText);
          //this.error(response.responseText);
          }, this)
      });
    },

    focus: function(e){
     // _.defer(_.bind(function(){
        this.ui.input.trigger('click');
        this.ui.input.focusNscroll();
        this.ui.input.select();
        if(this.model.get('type') == 'choice'){
          this.$('.bootstrap-select').find('button').focus();
          if(this.$('.dropdown-menu').css('display') == 'none')
            this.$('.bootstrap-select .btn').dropdown('toggle');
          this.ui.input.trigger('change');
        }
        else if(this.model.get('type') == 'ip'){
          this.ui.input.val('___.___.___.___');
        }
     // }, this));
    },

    //onClick: function(e){
    //  e.preventDefault();
    //  return false;
    //},

    //onFocus: function(e){
    //  e.preventDefault();
    //  return false;
    //},

    onBlur: function(e){
      e.preventDefault()
      if(this.ui.input.val() != "" || this.freezeBlur) return;
      this.ui.input.trigger('makeip');
      if(this.model.get('type') == 'ip' && this.ui.input.val() == "___.___.___.___" || this.ui.input.val() == "")
        this.ui.input.val('');
      // Commenté suite à #21029
      //if(!this.sync && this.model.get('value') != this.model.get('_value'))
      //  this.save();
      //else {
      //  this.onCancelClick();
      //  Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
      //}
    },

    onKeyup: function(e){
      var empty = (this.ui.input.val() == ''
        ||(this.ui.input.val() == '___.___.___.___'
        && this.model.get('type') == 'ip'));
      this.renderControlBtn(empty);
      //e.stopPropagation();
      if(e.keyCode == 9)
        e.preventDefault();
      //return false;
    },

    onKeydown: function(e){
      var empty = (this.ui.input.val() == ''
        ||(this.ui.input.val() == '___.___.___.___'
        && this.model.get('type') == 'ip'));
      this.renderControlBtn(empty);
      switch(e.keyCode){
        case 13: // Enter
          e.stopPropagation();
          e.preventDefault();
          //this.ui.input.blur();
          if(this.model.get('type') == 'choice'){
            var focused = _.find(this.$('.bootstrap-select li > a'), function(elt){ return $(elt).is(':focus'); });
            if(focused){
              $(focused).trigger('click');
              this.addItem(_.bind(this.focus, this));
            } else this.save();
          } else
          if(empty) this.save();
          else this.addItem();
          return false;
          break;
        case 9:  // Tab
          e.stopPropagation();
          e.preventDefault();
          this.ui.input.blur();
          var callback = _.bind(function(){
            var links = $('.btn.editBtn');
            var link;
            _.each(links, function(a, i){
              if($(a).attr('href') == '#variables/'+this.model.id+'/edit'){
                var key = i+(e.shiftKey ? -1 : +1);
                if(key >= 0 && key < links.length)
                  link = links[key];
              }
            }, this);
            link = $(link ? link : (e.shiftKey ? links.last() : links.first()));
            Backbone.View.goTo(link.attr('href').substring(1));
          } , this);
          if(empty)
            this.save(callback);
          else {
            if(this.model.get('type') == 'choice')
              callback();
            else
              this.addItem(_.bind(function(){
                this.save(callback);
              }, this));
          }
          return false;
          break;
      }
      //return false;
    },

    save: function(callback){
      Backbone.View.goTo('variables/'+this.model.id+'/save');
      this.ui.saveBtn.button('loading');
      this.ui.msg.hide();

      if(this.model.get('_value') != this.model.get('value')){
        this.model.save({},{
          success: _.bind(function(){
            this.model.trigger('error:cancel');
            if(this.options.isMandatoryCollection)
              //this.options.mandatories.resetAll(callback);
              vent.trigger('route:validateConfig', callback);
            else
              this.options.categories.fetch({
                success: _.bind(function(collection){
                  this.sync = false;
                  try {
                    this.model.get('tag').get('category').tags.fetch({ 
                      success: _.bind(function(tags){
                        var bkp_location = window.location.hash;
                        this.onCancelClick();
                        this.ui.saveBtn.button('reset');
                        if(tags.length == 0)
                          Backbone.View.goTo('categories/general');
                        else {
                          Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                          if(_.isFunction(callback)) callback();
                          else Backbone.View.goTo(bkp_location);
                        }
                      }, this),
                      error: _.bind(function(collection, response){
                        this.model.trigger('error', response.responseText);
                        //new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
                      }, this)
                    });
                  } catch(e) {
                    vent.trigger('route:reloadConfig', callback);
                  }
                }, this)
              });
          }, this),
          error: _.bind(function(model, response){
            //this.error(response.responseText);
            this.model.trigger('error', response.responseText);
          }, this)
        });
      } else {
        this.onCancelClick();
        this.ui.saveBtn.button('reset');
        this.model.set('value', this.model.get('_value'));
        if(_.isFunction(callback)) callback();
      }
    },

    onCancelClick: function(){
      Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
      vent.trigger('route:editVariableCancel');
      this.model.trigger('error:cancel');
      this.ui.control.removeClass('error');
      this.quit();
    },

    cancel: function(){
      //this.model.set('value', this.model.get('_value'));
      this.ui.control.removeClass('error');
      this.model.trigger('error:cancel');
      this.ui.input.val('');
      this.quit();
    },

    quit: function(){
      this.ui.msg.hide();
      this.ui.saveBtn.button('reset');
      this.ui.resetBtn.button('reset');
      if(this.model.get('type') == 'str') this.ui.input.val('');
      if(this.model.get('type') == 'choice') this.ui.input.val(this.model.get('value'));
    },

    error: function(msg){
      this.focus();
      this.ui.saveBtn.button('reset');
      this.ui.resetBtn.button('reset');
      this.ui.control.addClass('error');
      this.ui.msg.show().empty()
        .append($('<i>').addClass('icon-warning-sign'))
	//.append(' '+msg)
      ;
    },

    serializeData: function(){
      //if(this.model.get('_value') != this.model.get('value') && this.model.get('default_owner'))
      //  this.model.set('default_owner', false);
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id
      });
    },

    templateHelpers: {
      selectTag: function(){
        var $select = $('<select>').attr({ name: 'value', id: 'input-'+this.id }).addClass('selectpicker').html(_.map(this.choices, function(choice){
          var $option = $('<option>').attr({ value: choice, id: 'value-'+this.id+'-'+choice.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "-") }).text(choice);
	  if(this.value == choice) $option.attr({ selected: 'selected' });
	  return $option;
        }, this));
	if(this.mandatory) $select.attr({ required: 'required' });
	return $select.wrap('<div></div>').parent().html();
      }
    }
  });
});
