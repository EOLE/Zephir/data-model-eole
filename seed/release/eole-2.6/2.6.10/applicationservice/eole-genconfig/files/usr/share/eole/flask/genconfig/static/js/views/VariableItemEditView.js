define(['vent','marionette','templates','lib/bootstrap-select','lib/bootstrap-select2','lib/jquery-input-ip-address-control'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName   : 'div',
    className : 'control-group',
    template: templates.variableItemEditView,

    ui: {
      input    : 'input[name=value], select[name=value]',
      saveBtn  : '.btn.save',
      resetBtn : '.btn.reset',
      msg      : '.msg',
      link     : 'a'
    },

    events: {
      'keyup'                   : 'onKeyup',
      'keydown'                 : 'onKeydown',
      'blur input[name=value]'  : 'onBlur',
      'blur select[name=value]' : 'onBlur',
      'click .save'             : 'save',
      'click .cancel'           : 'onCancelClick',
      'click .reset'            : 'resetValue',
      'mousedown'               : 'onMouseDown'
    },

    initialize: function(){
      this.bindTo(this.model, 'error', this.error, this);
      this.bindTo(vent, 'route:editVariable', this.editVariable, this);
      this.bindTo(vent, 'route:editVariableCancel', this.cancel, this);
      this.model.set('_value', this.model.get('value'));
    },

    onRender: function(){
      this.ui.link.tooltip();
      this.ui.input.tooltip();
      this.ui.msg.hide();
      if(this.model.get('open')){
        this.$('.selectpicker').attr({ placeholder: this.model.get('value') });
        this.$('.selectpicker').select2({ open_value: true, width: "220px"});
      } else
        this.$('.selectpicker').selectpicker({ size: 6 });
      if(this.model.get('type') == 'ip'){
        $(this.ui.input).ipAddress();
      }
      if(this.model.get('type') == 'choice'){
        var selectTag = this.$('.bootstrap-select').find('button');
        selectTag.attr({ rel:"tooltip", 'data-placement': "top", 'data-toggle': "tooltip", title: this.model.get('help') });
        selectTag.tooltip();
        selectTag.on('click', _.bind(function(){
          setTimeout(function(){
            this.$('ul.dropdown-menu > li[rel=0] > a').focus();
          }, 1);
        }, this));
      }
    },

    editVariable: function(id){
      if(id == this.model.id) this.focus();
      else this.cancel();
    },

    focus: function(e){
      _.defer(_.bind(function(){
        vent.trigger('saveScrollPos', window.pageYOffset);
        //this.ui.input.focusNscroll();
        this.ui.input.select();
        if(this.model.get('type') == 'choice'){
          if(this.model.get('open'))
            this.ui.input.select2('open');
          else {
            this.$('.bootstrap-select').find('button').focus();
            if(this.$('.dropdown-menu').css('display') == 'none')
              this.$('.bootstrap-select .btn').dropdown('toggle');
            this.ui.input.trigger('change');
            this.$('ul.dropdown-menu > li[rel=0] > a').focus();
          }
        }
      }, this));
      return false;
    },

    sync: false,

    onBlur: function(e){
      this.ui.input.trigger('makeip');
      if(this.model.get('type') == 'ip' && this.ui.input.val() == "___.___.___.___")
        this.ui.input.val('');
      if(!this.sync && this.model.get('value') != this.ui.input.val() &&
              (this.ui.input.val() || this.model.get('value')))
        this.save()
      //else if(!this.sync && !this.model.get('open'))
      //  this.onCancelClick();
    },

    onMouseDown: function(e){
      this.sync = true; setTimeout(_.bind(function(){this.sync=false;},this), 200);
    },

    onKeyup: function(e){
 //     e.stopPropagation();
      if(e.keyCode == 9) e.preventDefault();
    },

    onKeydown: function(e){
      switch(e.keyCode){
        case 38: // Up
        case 40: // Down
            break;
        case 13: // Enter
          this.sync = true; setTimeout(_.bind(function(){this.sync=false;},this), 1000);
          e.stopPropagation();
          e.preventDefault();
          this.ui.input.blur();
          if(this.model.get('type') == 'choice'){
            var focused = _.find(this.$('.bootstrap-select li > a'), function(elt){ return $(elt).is(':focus'); });
            if(focused){
              $(focused).trigger('click');
            }
            this.save(_.bind(this.focus, this));
          } else this.save(_.bind(function(){  },this));
          return false;
          break;
        case 9:  // Tab
          this.sync = true; setTimeout(_.bind(function(){this.sync=false;},this), 1000);
          e.stopPropagation();
          e.preventDefault();
          this.ui.input.blur();
          var callback = _.bind(function(){
            var links = $('.btn.editBtn, .btn.addValue');
            var link;
            _.each(links, function(a, i){
              if($(a).attr('href') == '#variables/'+this.model.id+'/edit'){
                var key = i+(e.shiftKey ? -1 : +1);
                if(key >= 0 && key < links.length)
                  link = links[key];
              }
            }, this);
            link = $(link ? link : (e.shiftKey ? links.last() : links.first()));
            if(link.hasClass('addValue')){
              link.focus();
              this.onCancelClick();
            }
            if(link.attr('href'))
              Backbone.View.goTo(link.attr('href').substring(1));
          } , this);
          if((this.model.get('type') != 'ip' || this.ui.input.val() != '___.___.___.___') &&
              this.model.get('value') != this.ui.input.val() &&
              (this.ui.input.val() || this.model.get('value')))
            this.save(callback);
          else callback();
          return false;
          break;
      }
    },

    save: function(callback){
      Backbone.View.goTo('variables/'+this.model.id+'/save');
      var value = this.ui.input.val();
      if(value == "") value = null;
      this.ui.saveBtn.button('loading');
      this.ui.msg.hide();

      if(value != this.model.get('value') || this.model.get('default_owner')){
        this.model.save({
          value: value
        },{
          callback: _.isFunction(callback) ? callback : null,
          success: _.bind(function(model, response){
            if(this.options.isMandatoryCollection)
              //this.options.mandatories.resetAll(callback);
              vent.trigger('route:validateConfig', callback);
            else
              this.options.categories.fetch({
                success: _.bind(function(collection){
                  this.sync = false;
                  try {
                    this.model.get('tag').get('category').tags.fetch({ 
                      success: _.bind(function(tags){
                        var bkp_location = window.location.hash;
                        this.onCancelClick();
                        this.ui.saveBtn.button('reset');
                        if(tags.length == 0)
                          Backbone.View.goTo('categories/general');
                        else {
                          Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                          if(_.isFunction(callback)) callback();
                          else Backbone.View.goTo(bkp_location);
                        }
                      }, this),
                      error: _.bind(function(collection, response){
                        this.model.trigger('error', response.responseText);
                        //new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
                      }, this)
                    });
                  } catch(e) {
                    vent.trigger('route:reloadConfig', callback);
                  }
                }, this),
                error: _.bind(function(model, response){
                    new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
                }, this)
              });
          }, this),
          error: _.bind(function(model, response){
            this.model.trigger('error', response.responseText);
            //this.error(response.responseText);
          }, this)
        });
      } else {
        this.onCancelClick();
        this.ui.saveBtn.button('reset');
        this.model.set('value', this.model.get('_value'));
        if(_.isFunction(callback)) callback();
      }
    },

    resetValue: function(callback){
      this.ui.resetBtn.button('loading');
      this.model.save({},{
        url: this.model.url()+'/reset',
        callback: _.isFunction(callback) ? _.bind(callback, this) : null,
        success: _.bind(function(model, response){
          this.ui.saveBtn.button('reset');
          if(this.options.isMandatoryCollection)
            //this.options.mandatories.resetAll(callback);
            vent.trigger('route:validateConfig', callback);
          else
            this.options.categories.fetch({
              success: _.bind(function(collection){
                this.sync = false;
                try {
                  this.model.get('tag').get('category').tags.fetch({ 
                    success: _.bind(function(tags){
                      var bkp_location = window.location.hash;
                      this.ui.resetBtn.button('reset');
                      this.onCancelClick();
                      if(tags.length == 0)
                        Backbone.View.goTo('categories/general');
                      else {
                        Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                        if(_.isFunction(callback)) callback();
                        else Backbone.View.goTo(bkp_location);
                      }
                    }, this),
                    error: _.bind(function(collection, response){
                      this.model.trigger('error', response.responseText);
                    }, this)
                  });
                } catch(e) {
                  vent.trigger('route:reloadConfig', callback);
                }
              }, this)
            });
        }, this),
        error: _.bind(function(model, response){
          this.model.trigger('error', response.responseText);
          //this.error(response.responseText);
        }, this)
      });
    },

    error: function(msg){
      this.ui.saveBtn.button('reset');
      this.ui.resetBtn.button('reset');
      this.$el.addClass('error');
      this.ui.msg.show().empty()
        .append($('<i>').addClass('icon-warning-sign'))
      this.focus();
    },

    onCancelClick: function(){
      this.model.set('value', this.model.get('_value'));
      this.ui.input.val(this.model.get('value'));
      vent.trigger('route:editVariableCancel');
      Backbone.View.goTo("#variables/"+this.model.id+"/editCancel");
      this.ui.saveBtn.button('reset');
      this.$el.removeClass('error');
      this.ui.msg.empty().hide();
      this.model.trigger('error:cancel');
      //if(this.model.get('type') == 'unicode') this.ui.input.val(this.model.get('value'));
      //if(this.model.get('type') == 'ip') this.ui.input.val(this.model.get('value'));
      //if(this.model.get('type') == 'choice') this.ui.input.val(this.model.get('value'));
      this.cancel();
    },

    cancel: function(id){
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id
      });
    },

    templateHelpers: {
      selectTag: function(){
        var $select = $('<select>').attr({ name: 'value', id: 'value-'+this.id }).addClass('selectpicker save').html(_.map(this.choices, function(choice){
          var $option = $('<option>').attr({ value: choice, id: 'value-'+this.id+'-'+choice.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "-") }).text(choice);
	  if(this.value == choice) $option.attr({ selected: 'selected' });
	  return $option;
        }, this));
        //$select.append($('<option>').attr({ value: '', class: 'null_value' }).text('null'));
	if(this.mandatory) $select.attr({ required: 'required' });
	return $select.wrap('<div></div>').parent().html();
      }
    }
  });
});
