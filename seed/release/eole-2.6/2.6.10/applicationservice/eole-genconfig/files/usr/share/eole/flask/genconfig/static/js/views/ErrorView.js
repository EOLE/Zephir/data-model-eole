define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'div',
    className: 'modal hide fade small',
    template: templates.errorView,

    events: {
      'click .reload'      : 'reload',
      'click .closeDialog' : 'closeDialog'
    },

    initialize: function(){
      var html = this.model.get('responseText').replace(/\n/g, "<br />");
      this.model.set('responseText', html);
    },

    reload: function(){
      this.$el.modal('hide');
      if (this.options == null) {
          window.location.reload();
      } else {
          var url = "/logout?return_url=" + this.options.option.app_location + this.options.option.path_prefix + '/';
          if (this.options.option.application_mode) {
              window.location = url + '%3Fapplication';
          } else {
              window.location = url;
          }
      }
    },

    closeDialog: function(){
      this.$el.modal('hide');
      this.remove();
    }
  });
});
