define(['vent','models/Variable'],
function(vent,Variable){
  'use strict';

  return Backbone.Collection.extend({
    
    model: Variable,

    initialize: function(models, options){
      this.options = options || {};
      this.bind('add', this.adoptOne, this);
      this.bind('reset', this.adoptAll, this);
    },

    adoptAll: function(){
      this.each(this.adoptOne, this);
    },

    adoptOne: function(model){
      model.set('category', app.config.categories.get(model.get('categoryid')));
      //model.set('tag', model.get('category').tags.get(model.get('tagid')));
    },

    get: function(id){
      var model = Backbone.Collection.prototype.get.call(this, id);
      if(model) return model;
      else {
        model = _.find(this.models, function(variable){
          return variable.slaves.get(id);
	});  
	return model ? model.slaves.get(id) : false;
      }              
    },               
                     
    url: function()  {
      return '/categories/'+this.options.categoryid+'/variables';
    },

    resetAll: function(callback){
      this.fetch({ url: '/variables/set/' + _.pluck(this.models,'id').join(';'), success: _.isFunction(callback) ? callback : null });
    },

    next: function(model){
      return this.at(this.indexOf(this.get(model.id)) + 1);
    },

    prev: function(model) {
      return this.at(this.indexOf(this.get(model.id)) - 1);
    }                    
  });                
});                  
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
