define(['vent','marionette','templates','helpers/util'],
function(vent,Marionette,templates,Modes){
  'use strict';

  return Backbone.Model.extend({

    defaults : {
      name    : 'basic',
      level   : 0,
      current : false
    },

    initialize: function() {
      this.set('name', this.id);
    }
  });
});
