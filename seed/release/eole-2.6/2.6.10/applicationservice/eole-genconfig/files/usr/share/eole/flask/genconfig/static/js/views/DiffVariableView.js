define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'tr',
    className: '',
    template: templates.diffVariableView,

    beforeRender: function(){
    },

    onRender: function(){
      this.el = $(this.$el.html());
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        debug: this.options.debug
      });
    },

    templateHelpers: {
      getMode: function(){
        switch(this.mode){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
