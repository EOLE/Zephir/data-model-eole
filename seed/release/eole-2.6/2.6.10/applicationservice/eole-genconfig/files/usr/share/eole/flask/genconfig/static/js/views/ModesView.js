define(['vent','marionette','templates','views/ModeItemView'],
function(vent,Marionette,templates,ModeItemView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'li',
    className: 'dropdown',
    template: templates.modesView,
    itemView: ModeItemView,
    itemViewContainer: '.dropdown-menu',

    events: {
      'click .toggleDebug': 'toggleDebug'
    },

    initialize: function(){
      this.collection = this.options.modes;
      this.bindTo(this.collection, 'reset change', this.render, this);
      this.bindTo(vent, 'route:setMode', this.setMode, this);
      this.bindTo(vent, 'change:debug', this.render, this);
    },

    setMode: function(id){
      if(!this.collection.length) return;
      var current = this.collection.get(id);
      this.collection.each(function(model){
        if(model.id != id) model.set('current', false);
      });
      current.save({ 'current': true },{
        success: _.bind(function(){
          this.options.categories.fetch({
            success: _.bind(function(collection){
              this.render();
              var loc = collection.get(this.options.current_category) ? this.options.current_category : collection.first().id;
              Backbone.View.goTo('categories/'+loc);
            }, this)
          });
        }, this),
        error: _.bind(function(collection, response){
          new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
        }, this)
      
      });
    },

    toggleDebug: function(){
      vent.trigger('route:toggleDebug');
    },

    serializeData: function(){
      var current = this.collection.current();
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        current: current ? current.get('name') : 'None',
        debug: this.options.debug
      });
    },

    templateHelpers: {
      getMode: function(){
        switch(this.current){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
