describe('View :: Sidebar', function() {
 
  beforeEach(function() {
    var flag = false,
        that = this;
 
    require(['models/Category', 'collections/Categories', 'views/Sidebar'], function(Category, Categories, Sidebar) {
      that.categories = new Categories([]);
      that.view = new Sidebar({categories: that.categories});
      that.mockData  = { name: 'category1', help: 'Foo help',  timestamp: new Date().getTime() };
      that.mockData2 = { name: 'category2', help: 'Bar help',  timestamp: new Date().getTime() };
      $('#sandbox').html(that.view.render().el);
      flag = true;
    });
 
    waitsFor(function() {
      return flag;
    });
 
  });
 
  afterEach(function() {
    this.view.remove();
  });
 
  describe('Renders Category', function() {
 
    it('should be empty', function() {
      expect(this.view.ui.nav.children(':not(.nav-header)').length).toEqual(0);
    });
     
    it('should re-render on reset', function() {
      this.categories.reset([this.mockData]);
      expect(this.view.ui.nav.children(':not(.nav-header)').length).toEqual(1);
console.log(this.view.ui.nav); 

      this.categories.reset([this.mockData,this.mockData2]);
      expect(this.view.ui.nav.children(':not(.nav-header)').length).toEqual(2);
console.log(this.view.ui.nav); 
    });
  });
});
