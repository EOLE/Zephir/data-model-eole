define(['vent','marionette','templates',
  'views/VariableItemView' , 'views/VariableItemEditView',
  'views/VariableMultiView', 'views/VariableMultiEditView',
  'views/VariableGroupView'],
function(vent,Marionette,templates,
  VariableItemView,  VariableItemEditView,
  VariableMultiView, VariableMultiEditView,
  VariableGroupView){
  'use strict';

  return Marionette.Layout.extend({

    tagName: 'div',
    className: '',
    template: templates.variableView,

    regions: {
      show: '.show',
      edit: '.edit'
    },

    initialize: function(){
      this.bindTo(vent, 'route:editVariable', this.editVariable, this);
      this.bindTo(vent, 'route:editVariableCancel', this.editVariableCancel, this);
      this.options = _.extend({}, this.options, { model: this.model });

      switch(this.model.getType()){
        case 'multi' :
          if(typeof VariableMultiView === 'undefined') VariableMultiView = require('views/VariableMultiView');
          if(typeof VariableMultiEditView === 'undefined') VariableMultiEditView = require('views/VariableMultiEditView');
	        this.showView = new VariableMultiView(this.options);
	        this.editView = new VariableMultiEditView(this.options);
	        break;
	      case 'group' :
          if(typeof VariableGroupView === 'undefined') VariableGroupView = require('views/VariableGroupView');
	        this.showView = new VariableGroupView(this.options);
	        this.editView = null;//new VariableGroupEditView(this.options);
	        break;
        default:
          if(typeof VariableItemView === 'undefined') VariableItemView = require('views/VariableItemView');
          if(typeof VariableItemEditView === 'undefined') VariableItemEditView = require('views/VariableItemEditView');
	  this.showView = new VariableItemView(this.options);
	  this.editView = new VariableItemEditView(this.options);
      }
    },

    onRender: function(){
      this.$el.removeClass('editing');
      if(this.editView) this.edit.show(this.editView);
      this.show.show(this.showView);
    },

    focus: function(){
     // $('body').scrollTo(this.$el.parent().parent().parent(), 0, { axis: 'y', offset: -80 });
    },

    editVariable: function(id){
      if(this.model.id == id){
        if(this.model.getType() != 'group' && this.model.get('editable'))
          this.$el.addClass('editing');
        this.focus();
      } else this.$el.removeClass('editing');
    },

    editVariableCancel: function(){
      this.$el.removeClass('editing');
    }
  });
});
