define(['vent','marionette','templates','helpers/util'],
function(vent,Marionette,templates,Modes){
  'use strict';

  return Backbone.Model.extend({

    urlRoot: '/user',

    defaults : {
      name    : 'dev user',
      zephir  : {
        registered: false,
        available : false,
        server_id : null,
        server_ip : null
      }
    },

    initialize: function() {
      this.set('name', this.id);
    }
  });
});
