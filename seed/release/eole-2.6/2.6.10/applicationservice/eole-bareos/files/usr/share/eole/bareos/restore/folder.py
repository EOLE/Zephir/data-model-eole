#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Restauration du répertoire passé en argument et de son contenu"""
from pyeole.bareosrestore import bareos_restore_one_folder, exit_if_running_jobs
import sys

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """file helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            if test_jobs:
                exit_if_running_jobs()
            job(option, jobid)

    else:
        print "Il faut spécifier le chemin du répertoire a restaurer"
        sys.exit(1)

def pre():
    print "pre folder"

def post():
    print "post folder"

def job(foldername, jobid):
    print "Restauration du répertoire {0}".format(foldername)
    bareos_restore_one_folder(foldername, jobid)


priority=0
