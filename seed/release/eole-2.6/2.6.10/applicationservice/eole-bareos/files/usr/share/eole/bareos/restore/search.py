#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Liste les fichiers avec le nom passé en argument (attention, mettre '\\*' au lieu de '*' et '\\?' au lien de '?')"""
import sys
from pyeole.process import system_code
from pyeole.service import service_out
from glob import glob
from os import unlink
from pyeole.bareosrestore import exit_if_running_jobs, bareos_search

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """search helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            if test_jobs:
                exit_if_running_jobs()
            job(option, jobid)

    else:
        print "Il faut spécifier le fichier ou répertoire à rechercher"
        sys.exit(1)

def pre():
    print "pre search"

def post():
    print "post search"

def job(filenames, jobid):
    print "Recherche dans la sauvegarde"
    try:
        bareos_search(filenames.split(','))
    except Exception, e:
        print e
        sys.exit(1)
    sys.stdout.write('\n')

priority=0


