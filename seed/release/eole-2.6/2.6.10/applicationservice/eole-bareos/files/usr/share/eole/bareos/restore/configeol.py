#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Restauration du config.eol de bareos (attention il faut savoir ce que l'on fait !)"""
from pyeole.bareosrestore import extract_configeol_files
from sys import exit

def execute(option, opt_str, value, parser, jobid, test_jobs=False):
    """helper
    never test jobs
    """
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            job(option, jobid)
    else:
        print "Il faut spécifier le nom du directeur Bareos"
        exit(1)

def pre():
    print "pre configeol"

def post():
    print "post configeol"

def job(bareos_dir_name, jobid):
    print "Restauration du fichier config.eol"
    try:
        extract_configeol_files(bareos_dir_name=bareos_dir_name)
    except Exception,e:
        print e
        exit(1)

priority=0


