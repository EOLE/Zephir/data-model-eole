#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Liste le contenu du répertoire passé en argument """
from pyeole.bareosrestore import bareos_ls_one_folder, exit_if_running_jobs
import sys

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """file helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            if test_jobs:
                exit_if_running_jobs()
            job(option, jobid)

    else:
        print "Il faut spécifier le répertoire à lister"
        sys.exit(1)

def pre():
    print "pre ls_folder"

def post():
    print "post ls_folder"

def job(foldername, jobid):
    print "Contenu de {0}".format(foldername)
    bareos_ls_one_folder(foldername, jobid)


priority=0

