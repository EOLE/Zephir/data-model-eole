#!/usr/bin/python
# -*- coding: utf-8 -*-
###########################################################################
# EOLE - 2008-2013
# Copyright Pole de Competence EOLE  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
###########################################################################
from os.path import join, isfile

from amon.guardian.config import DANS_LOCAL_SUBDIR, \
    LOCAL_TEMPLATE_DIR, optional_policies, local_deported_files
from amon.guardian.dans_tools import set_filtrage
from creole.client import CreoleClient

def make_local_instance_spec_conf_files(num_zone, indice):
    """
        Mise à jour des fichiers bannedurllist et bannedsitelist
    """
    dirname = DANS_LOCAL_SUBDIR % (num_zone-1, indice)
    ## les fichiers locaux dans lesquels on enlève/ajoute des #
    for f_name in local_deported_files:
        src = join(LOCAL_TEMPLATE_DIR, f_name)
        dst = join(dirname, f_name)
        # mise à jour des fichier pour utiliser les même filtres
        # que ceux des templates (#2764)
        lsrc = file(src).readlines()
        ldst = file(dst).readlines()
        final = []
        for line1 in lsrc:
            current = line1
            for line2 in ldst:
                if line1.endswith(line2):
                    # ligne trouvée avec ou sans # => on l'utilise
                    current = line2
                    break
            final.append(current)
        file(dst, 'w').writelines(final)

client = CreoleClient()
for num_zone in range(1, int(client.get_creole('num_dansguardian_instance'))+1):
    if client.get_creole('dans_instance_{0}_active'.format(num_zone), 'non') == 'oui':
        for policy in optional_policies:
            make_local_instance_spec_conf_files(num_zone, policy)
        set_filtrage(num_zone-1)
