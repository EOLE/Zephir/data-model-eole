#!/bin/bash
#################################################
# enregistrement_domaine.sh
#################################################
. /usr/lib/eole/ihm.sh

TYPE_AUTH=$(CreoleGet type_squid_auth aucun)

integration=1
restart_smb() {
CreoleService winbind stop -c proxy
CreoleService smbd restart -c proxy
CreoleService nmbd restart -c proxy
CreoleService winbind start -c proxy
}

if [ "$TYPE_AUTH" != 'NTLM/KERBEROS' -a "$TYPE_AUTH" != 'NTLM/SMB' ]; then
    EchoRouge "L'authentification du proxy n'est pas de type NTLM"
    exit 1
fi

if [ "$TYPE_AUTH" == 'NTLM/KERBEROS' ];then
    nom_domaine=$(CreoleGet nom_domaine_windows)
else
    nom_domaine=$(CreoleGet nom_domaine_smb)
fi


CreoleRun "/usr/bin/wbinfo -t" proxy &>/dev/null
if [ $? -eq 0 ];then
    echo "Le serveur est déjà intégré à un domaine."
    CreoleRun "/usr/bin/wbinfo -t -D $nom_domaine" proxy
    QUESTION="Relancer l'intégration ?"
    Question_ouinon "$QUESTION" True non warn
    [ $? -ne 0 ] && exit 0
fi

#redemarrage de samba
echo "*** Redémarrage des services pour l'enregistrement au domaine ***"
restart_smb

echo
echo "Entrer le nom de l'administrateur du contrôleur de domaine :"
read user_admin
echo "Entrer le mot de passe de l'administrateur du contrôleur de domaine :"
read -s mdp_admin

if [ $TYPE_AUTH = 'NTLM/KERBEROS' ]; then
    ip_serveur_krb=$(CreoleGet ip_serveur_krb)
    nom_serveur_krb=$(CreoleGet nom_serveur_krb)
    nom_domaine_krb=$(CreoleGet nom_domaine_krb)
    #inscription de la station dans un domaine
    CreoleRun "/usr/bin/net ads join -I $ip_serveur_krb -U $user_admin%'${mdp_admin}' -S $nom_serveur_krb.$nom_domaine_krb" proxy
    if [ "$?" == "0" ]; then
        integration=0
    fi
    echo

elif [ "$TYPE_AUTH" = 'NTLM/SMB' ]; then
    ip_serveur_smb=$(CreoleGet ip_serveur_smb)
    nom_serveur_smb=$(CreoleGet nom_serveur_smb)
    #inscription de la station dans un domaine
    CreoleRun "/usr/bin/net rpc join -I $ip_serveur_smb -U $user_admin%'${mdp_admin}' -S $nom_serveur_smb" proxy
    if [ "$?" == "0" ]; then
        integration=0
    fi
    echo
fi

#redemarrage de samba
echo "*** Redémarrage des services pour confirmer l'enregistrement au domaine ***"
restart_smb

#test de l'intégration
if [ "$integration" == "0" ]; then
    CreoleRun "/usr/bin/wbinfo -t -D $nom_domaine" proxy
    if [ $? -eq 1 ]; then
        EchoRouge "L'intégration au domaine $nom_domaine a échoué"
        exit 1
    else
        EchoVert "L'intégration au domaine $nom_domaine a réussi"
    fi
else
    EchoRouge "L'intégration au domaine $nom_domaine a échoué"
fi

exit 0
