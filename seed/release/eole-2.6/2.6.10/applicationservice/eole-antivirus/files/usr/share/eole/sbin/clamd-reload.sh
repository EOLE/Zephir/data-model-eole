#!/bin/bash
. /usr/lib/eole/ihm.sh

if [ ! -f /etc/eole/clamd-reload.cnf ]; then
    EchoRouge "Reload clam n'est pas configuré"
    exit 1
fi
OPTION=$1
. /etc/eole/clamd-reload.cnf
if [ "$OPTION" = "noreload" ]; then
    RELOAD=1
else
    RELOAD=0
fi

CODE=0
if [ $(CreoleGet mode_conteneur_actif) = "oui" ]; then
    for container in $CLAMD_CONTAINER; do
        container_path=$(CreoleGet container_path_$container)
        if [ ! "$container_path" = "" ]; then
            /usr/bin/rsync -a --delete /var/lib/clamav $container_path/var/lib
        fi
        if [ "$RELOAD" = "0" ]; then
            CreoleRun /usr/bin/clamd-eole $container
            if [ ! "$?" = "0" ]; then
                EchoRouge "Impossible de recharger clamd sur le conteneur $container"
                CODE=2
            else
                EchoVert "Clamd est rechargé sur $container"
            fi
        fi
    done
else
    if [ "$RELOAD" = "0" ]; then
        /usr/bin/clamd-eole
        if [ ! "$?" = "0" ]; then
            EchoRouge "Impossible de recharger clamd"
            CODE=2
        else
            EchoVert "Clamd est rechargé"
        fi
    fi
fi

/usr/share/eole/sbin/freshclam-status.sh "$CODE"

exit $CODE
