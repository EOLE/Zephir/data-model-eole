## Fichier de configuration pour freshclam > 0.90
## Scribe 2.0
## février 207
## Equipe Eole eole@ac-dijon.fr

# Path to the database directory.
# WARNING: It must match clamd.conf's directive!
# Default: hardcoded (depends on installation options)
DatabaseDirectory /var/lib/clamav

# Path to the log file (make sure it has proper permissions)
# Default: disabled
#UpdateLogFile /var/log/clamav/freshclam.log

# Enable verbose logging.
# Default: no
LogVerbose yes

# Use system logger (can work together with UpdateLogFile).
# Default: no
LogSyslog yes

# Specify the type of syslog messages - please refer to 'man syslog'
# for facility names.
# Default: LOG_LOCAL6
#LogFacility LOG_LOCAL6

# By default when started freshclam drops privileges and switches to the
# "clamav" user. This directive allows you to change the database owner.
# Default: clamav (may depend on installation options)
DatabaseOwner clamav

# Initialize supplementary group access (freshclam must be started by root).
# Default: no
AllowSupplementaryGroups yes

# Use DNS to verify virus database version. Freshclam uses DNS TXT records
# to verify database and software versions. With this directive you can change
# the database verification domain.
# Default: enabled, pointing to current.cvd.clamav.net
DNSDatabaseInfo %%clam_dns_database_info

%if %%clam_forcer_mirror_database == 'oui':
  %for %%variable_iter in %%clam_mirror_database
DatabaseMirror %%variable_iter
  %end for
%else
# Uncomment the following line and replace XY with your country
# code. See http://www.iana.org/cctld/cctld-whois.htm for the full list.
DatabaseMirror db.%%{clam_iana}.clamav.net

# database.clamav.net is a round-robin record which points to our most
# reliable mirrors. It's used as a fall back in case db.XY.clamav.net is
# not working. DO NOT TOUCH the following line unless you know what you
# are doing.
DatabaseMirror database.clamav.net
%end if

# How many attempts to make before giving up.
# Default: 3 (per mirror)
MaxAttempts %%clam_max_attempts

# With this option you can control scripted updates. It's highly recommended
# to keep it enabled.
ScriptedUpdates yes

# Number of database checks per day.
# Default: 12 (every two hours)
Checks %%clam_checks

# Proxy settings
# Default: disabled
%if %%activer_proxy_client == 'oui'
HTTPProxyServer %%proxy_client_adresse
HTTPProxyPort %%proxy_client_port
#HTTPProxyUsername myusername
#HTTPProxyPassword mypass
%end if

# Send the RELOAD command to clamd.
# Default: no
#NotifyClamd
# By default it uses the hardcoded configuration file but you can force an
# another one.
#NotifyClamd /etc/clamav/clamd.conf

# Run command after successful database update.
# Default: disabled
%if %%mode_conteneur_actif == 'oui'
OnUpdateExecute sudo /usr/share/eole/sbin/clamd-reload.sh
%else
OnUpdateExecute /usr/share/eole/sbin/freshclam-status.sh "0"
%end if


# Run command when database update process fails.
# Default: disabled
OnErrorExecute /usr/share/eole/sbin/freshclam-status.sh "1"

# Don't fork into background.
# Default: no
#Foreground yes

# Enable debug messages in libclamav.
# Default: no
#Debug yes

# Timeout in seconds when connecting to database server.
# Default: 30
#ConnectTimeout 60

# Timeout in seconds when reading from database server.
# Default: 30
#ReceiveTimeout 60
