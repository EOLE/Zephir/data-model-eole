#!/bin/bash

HOST=`hostname`
DEST=root
case ${1} in

    ONBATT)
        echo "$HOST : The UPS is currently running on battery power!"|mail -s "$HOST : The UPS is currently running on battery power!" UPS $DEST
        ;;
    LOWBATT)
        echo "$HOST : UPS is on battery and has a low battery"|mail -s "$HOST : UPS is on battery and has a low battery" $DEST
	;;
    REPLBATT)
        echo "$HOST : The UPS battery is bad and needs to be replaced"|mail -s "$HOST : The UPS battery is bad and needs to be replaced" $DEST
	;;
    SHUTDOWN)
        echo "$HOST : The system is being shutdown"|mail -s  "$HOST : The system is being shutdown" $DEST 
	;;
    ONLINE)
        echo "$HOST : UPS is back online"|mail -s "$HOST : UPS is back online" $DEST
esac

