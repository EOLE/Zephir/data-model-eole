<?php
##
## database access settings in php format
## automatically generated from /etc/dbconfig-common/phpmyadmin.conf
## by /usr/sbin/dbconfig-generate-include
## Wed, 20 Jul 2011 11:00:21 +0200
##
## by default this file is managed via ucf, so you shouldn't have to
## worry about manual changes being silently discarded.  *however*,
## you'll probably also want to edit the configuration file mentioned
## above too.
##
$dbuser='';
$dbpass='';
$basepath='';
$dbname='phpmyadmin';
$dbserver='%%adresse_ip_mysql';
$dbport='';
$dbtype='mysql';
