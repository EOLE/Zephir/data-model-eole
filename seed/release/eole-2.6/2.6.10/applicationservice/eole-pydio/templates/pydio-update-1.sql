\r pydio
SET character_set_client = utf8;


-- UPGRADE 60 TO 62-- -----------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS 60to62;
DELIMITER |
CREATE PROCEDURE 60to62()
BEGIN
	DECLARE version VARCHAR(100);
	
	SELECT db_build INTO version FROM ajxp_version ORDER BY db_build DESC LIMIT 1;

	IF version = '60' THEN
        DROP TABLE IF EXISTS ajxp_log;
        
        CREATE TABLE IF NOT EXISTS `ajxp_log` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `logdate` datetime DEFAULT NULL,
          `remote_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
          `severity` enum('DEBUG','INFO','NOTICE','WARNING','ERROR') COLLATE utf8_unicode_ci DEFAULT NULL,
          `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `message` text COLLATE utf8_unicode_ci,
          `params` text COLLATE utf8_unicode_ci,
          `repository_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
          `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `dirname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          `basename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `source` (`source`),
          KEY `repository_id` (`repository_id`),
          KEY `logdate` (`logdate`),
          KEY `severity` (`severity`),
          KEY `dirname` (`dirname`),
          KEY `basename` (`basename`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;        
            
        CREATE TABLE IF NOT EXISTS `ajxp_mail_queue` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `url` text COLLATE utf8_unicode_ci NOT NULL,
          `date_event` int(11) NOT NULL,
          `notification_object` longblob NOT NULL,
          `html` int(1) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

        CREATE TABLE IF NOT EXISTS `ajxp_mail_sent` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `url` text COLLATE utf8_unicode_ci NOT NULL,
          `date_event` int(11) NOT NULL,
          `notification_object` longblob NOT NULL,
          `html` int(1) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;            
        
        
        ALTER TABLE ajxp_roles ADD `last_updated` int(11) NOT NULL DEFAULT '0' AFTER serial_role;
        
        -- Passer au build db suivant
        INSERT INTO `ajxp_version` (`db_build`) VALUES (62);
    END IF;
END|
DELIMITER ;

CALL 60to62();
DROP PROCEDURE 60to62;






