<?php
/*
 * Copyright 2007-2013 Charles du Jeu - Abstrium SAS <team (at) pyd.io>
 * This file is part of Pydio.
 *
 * Pydio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pydio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pydio.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The latest code can be found at <http://pyd.io/>.
 *
 * Description : configuration file
 * BASIC REPOSITORY CONFIGURATION.
 * The standard repository will point to the data path (ajaxplorer/data by default), folder "files"
 * Use the GUI to add new repositories.
 *   + Log in as "admin" and open the "Settings" Repository
 */
defined('AJXP_EXEC') or die( 'Access not allowed');

require_once  AJXP_INSTALL_PATH."/conf/EnvoleFilters.php";
AJXP_Controller::registerIncludeHook("vars.filter", "UserFolderftpCas");


%if %%is_defined('smb_netbios_name')
	$REPOSITORIES["ajxp_home"] = array(
		"DISPLAY"               =>       "Home",
		"DRIVER"                =>       "ftpCas",
		"DISPLAY_ID"            =>       "600",
		"AJXP_SLUG"             =>       "Home",
		"DRIVER_OPTIONS"        => array("FTP_HOST"=>"%%pydio_ftp",
	%if %%pydio_ftp != '127.0.0.1'
										 "FTP_SERVICE"=>"ftp://%%pydio_ftp",
	%else
										 "FTP_SERVICE"=>"ftp://%%adresse_ip_eth0",
	%end if
										 "FTP_PORT"=>"21",
										 "TMP_UPLOAD" => "/tmp",
										 "CHARSET"=>"UTF-8",
										 "FIX_PERMISSIONS"=>"user",
										 "PATH"  => USER_FTPCAS_PATH)
	);
%end if

%if %%is_defined('activer_pydio_ftp') and %%activer_pydio_ftp=="oui"
	$REPOSITORIES[2] = array(
		"DISPLAY"               =>       "FTP Distant",
		"DRIVER"                =>       "ftpCas",
		"DISPLAY_ID"            =>       "600",
		"AJXP_SLUG"             =>       "FTP Distant",
		"DRIVER_OPTIONS"        => array("FTP_HOST"=>"%%pydio_repository_ftp",
										 "FTP_SERVICE"=>"ftp://%%pydio_repository_ftp",
										 "FTP_PORT"=>"21",
										 "TMP_UPLOAD" => "/tmp",
										 "CHARSET"=>"UTF-8",
										 "FIX_PERMISSIONS"=>"user",
										 "PATH"  => USER_FTPCAS_PATH)
	);
%end if

%if %%is_defined('activer_pydio_local') and %%activer_pydio_local=="oui"
	$REPOSITORIES[1] = array(
		"DISPLAY"               =>       "Local",
		"DRIVER"                =>       "fs",
		"DISPLAY_ID"            =>       "600",
		"AJXP_SLUG"             =>       "Local",
		"DRIVER_OPTIONS"		=> array("PATH"  => "/home/www-data/var/www/html/pydio"),
	);
%end if

// DO NOT REMOVE THIS!
// USER DASHBOARD
$REPOSITORIES["ajxp_user"] = array(
    "DISPLAY"           =>  "My Dashboard",
    "AJXP_SLUG"         =>  "dashboard",
    "DISPLAY_ID"        =>  "user_dash.title",
    "DESCRIPTION_ID"    =>  "user_dash.desc",
    "DRIVER"            =>  "ajxp_user",
    "DRIVER_OPTIONS"    => array(
        "DEFAULT_RIGHTS" => "rw"
    )   
);



// DO NOT REMOVE THIS!
// SHARE ELEMENTS
$REPOSITORIES["ajxp_shared"] = array(
	"DISPLAY"		=>	"Shared Elements",
	"DISPLAY_ID"		=>	"467",
	"DRIVER"		=>	"ajxp_shared",
	"DRIVER_OPTIONS"=> array(
		"DEFAULT_RIGHTS" => "rw"
	)
);

// ADMIN REPOSITORY
$REPOSITORIES["ajxp_conf"] = array(
	"DISPLAY"		=>	"Settings",
	"DISPLAY_ID"		=>	"165",
	"DRIVER"		=>	"ajxp_conf",
	"DRIVER_OPTIONS"=> array()
);

$REPOSITORIES["fs_template"] = array(
	"DISPLAY"		=>	"Sample Template",
    "DISPLAY_ID"    =>  431,
	"IS_TEMPLATE"	=>  true,
	"DRIVER"		=>	"fs",
	"DRIVER_OPTIONS"=> array(
		"CREATE"		=>	true,
		"RECYCLE_BIN" 	=> 	'recycle_bin',
		"CHMOD_VALUE"   =>  '0600',
		"PAGINATION_THRESHOLD" => 500,
		"PAGINATION_NUMBER" => 200,
        "PURGE_AFTER"       => 0,
        "CHARSET"           => "",
		"META_SOURCES"		=> array(
			"metastore.serial"=> array(
				"METADATA_FILE"	=> ".ajxp_meta",
                "METADATA_FILE_LOCATION" => "infolders"
            ),
            "meta.user"     => array(
				"meta_fields"		=> "tags",
				"meta_labels"		=> "Tags",
                "meta_visibility"   => "hidden"
			),
            "meta.filehasher"   => array(),
            "meta.watch"        => array(),
            "meta.exif"   => array(
                "meta_fields" => "COMPUTED_GPS.GPS_Latitude,COMPUTED_GPS.GPS_Longitude",
                "meta_labels" => "Latitude,Longitude"
            ),
            "index.lucene" => array(
                "index_meta_fields" => "tags"
            )
		)
	),

);
