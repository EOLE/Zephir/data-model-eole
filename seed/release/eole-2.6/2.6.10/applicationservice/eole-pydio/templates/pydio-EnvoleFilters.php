<?php
function UserFolderftpCas(&$value){
    if(AuthService::getLoggedUser() != null){
        $loggedUser = AuthService::getLoggedUser();
        $user_folder = "/home/". substr($loggedUser->getId(), 0, 1) . '/' . $loggedUser->getId() .'/.ftp/';
        $value = str_replace("USER_FTPCAS_PATH", $user_folder, $value);
    }
    else {
        $value = str_replace("USER_FTPCAS_PATH", "", $value);
    }
}
?>
