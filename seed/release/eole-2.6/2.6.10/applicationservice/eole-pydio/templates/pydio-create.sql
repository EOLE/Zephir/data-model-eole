-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Mar 26 Mai 2015 à 10:36
-- Version du serveur: 5.5.43
-- Version de PHP: 5.3.10-1ubuntu3.18
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- création de la base de données
CREATE DATABASE pydio DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE pydio;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `pydio`
--

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_changes`
--

DROP TABLE IF EXISTS `ajxp_changes`;
CREATE TABLE IF NOT EXISTS `ajxp_changes` (
  `seq` int(20) NOT NULL AUTO_INCREMENT,
  `repository_identifier` text COLLATE utf8_unicode_ci NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `type` enum('create','delete','path','content') COLLATE utf8_unicode_ci NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `target` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `node_id` (`node_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_feed`
--

DROP TABLE IF EXISTS `ajxp_feed`;
CREATE TABLE IF NOT EXISTS `ajxp_feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edate` int(11) NOT NULL,
  `etype` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `htype` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `index_path` mediumtext COLLATE utf8_unicode_ci,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `repository_id` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `user_group` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repository_scope` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repository_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `edate` (`edate`,`etype`,`htype`,`user_id`,`repository_id`),
  KEY `index_path` (`index_path`(40))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_groups`
--

DROP TABLE IF EXISTS `ajxp_groups`;
CREATE TABLE IF NOT EXISTS `ajxp_groups` (
  `groupPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `groupLabel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`groupPath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_index`
--

DROP TABLE IF EXISTS `ajxp_index`;
CREATE TABLE IF NOT EXISTS `ajxp_index` (
  `node_id` int(20) NOT NULL AUTO_INCREMENT,
  `node_path` text COLLATE utf8_unicode_ci NOT NULL,
  `bytesize` bigint(20) NOT NULL,
  `md5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `mtime` int(11) NOT NULL,
  `repository_identifier` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Déclencheurs `ajxp_index`
--
DROP TRIGGER IF EXISTS `LOG_INSERT`;
DELIMITER //
CREATE TRIGGER `LOG_INSERT` AFTER INSERT ON `ajxp_index`
 FOR EACH ROW INSERT INTO ajxp_changes (repository_identifier, node_id,source,target,type)
  VALUES (new.repository_identifier, new.node_id, 'NULL', new.node_path, 'create')
//
DELIMITER ;
DROP TRIGGER IF EXISTS `LOG_UPDATE`;
DELIMITER //
CREATE TRIGGER `LOG_UPDATE` AFTER UPDATE ON `ajxp_index`
 FOR EACH ROW INSERT INTO ajxp_changes (repository_identifier, node_id,source,target,type)
  VALUES (new.repository_identifier, new.node_id, old.node_path, new.node_path, CASE old.node_path = new.node_path WHEN true THEN 'content' ELSE 'path' END)
//
DELIMITER ;
DROP TRIGGER IF EXISTS `LOG_DELETE`;
DELIMITER //
CREATE TRIGGER `LOG_DELETE` AFTER DELETE ON `ajxp_index`
 FOR EACH ROW INSERT INTO ajxp_changes (repository_identifier, node_id,source,target,type)
  VALUES (old.repository_identifier, old.node_id, old.node_path, 'NULL', 'delete')
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_log`
--

DROP TABLE IF EXISTS `ajxp_log`;
CREATE TABLE IF NOT EXISTS `ajxp_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logdate` datetime DEFAULT NULL,
  `remote_ip` varchar(45) DEFAULT NULL,
  `severity` enum('DEBUG','INFO','NOTICE','WARNING','ERROR') DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `message` text,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_plugin_configs`
--

DROP TABLE IF EXISTS `ajxp_plugin_configs`;
CREATE TABLE IF NOT EXISTS `ajxp_plugin_configs` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `configs` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ajxp_plugin_configs`
--

INSERT INTO `ajxp_plugin_configs` (`id`, `configs`) VALUES
('action.updater', 0x613a383a7b733a31393a22414a58505f504c5547494e5f454e41424c4544223b623a303b733a31313a225550444154455f53495445223b733a32323a2268747470733a2f2f7079642e696f2f7570646174652f223b733a31343a2250524553455256455f46494c4553223b733a303a22223b733a31373a22454e41424c455f3332345f494d504f5254223b623a303b733a31303a2250524f58595f484f5354223b733a303a22223b733a31303a2250524f58595f55534552223b733a303a22223b733a31303a2250524f58595f50415353223b733a303a22223b733a31343a225550444154455f4348414e4e454c223b733a363a22737461626c65223b7d),
('core.ajaxplorer', 0x613a323a7b733a31373a224150504c49434154494f4e5f5449544c45223b733a353a22507964696f223b733a31363a2244454641554c545f4c414e4755414745223b733a323a226672223b7d),
('core.log', 0x613a333a7b733a31383a224552524f525f544f5f4552524f525f4c4f47223b623a303b733a32303a22435553544f4d5f4845414445525f464f525f4950223b733a303a22223b733a32323a22554e495155455f504c5547494e5f494e5354414e4345223b613a353a7b733a31333a22696e7374616e63655f6e616d65223b733a383a226c6f672e74657874223b733a383a224c4f475f50415448223b733a32373a22414a58505f494e5354414c4c5f504154482f646174612f6c6f6773223b733a31333a224c4f475f46494c455f4e414d45223b733a32313a226c6f675f6461746528276d2d642d7927292e747874223b733a393a224c4f475f43484d4f44223b693a3737303b733a31383a2267726f75705f7377697463685f76616c7565223b733a383a226c6f672e74657874223b7d7d),
('core.mq', 0x613a313a7b733a31383a22554e495155455f4d535f494e5354414e4345223b613a333a7b733a31333a22696e7374616e63655f6e616d65223b733a363a226d712e73716c223b733a31383a2267726f75705f7377697463685f76616c7565223b733a363a226d712e73716c223b733a31303a2253514c5f445249564552223b613a323a7b733a31313a22636f72655f647269766572223b733a343a22636f7265223b733a31383a2267726f75705f7377697463685f76616c7565223b733a343a22636f7265223b7d7d7d),
('core.notifications', 0x613a323a7b733a31313a22555345525f4556454e5453223b623a313b733a32303a22554e495155455f464545445f494e5354414e4345223b613a333a7b733a31333a22696e7374616e63655f6e616d65223b733a383a22666565642e73716c223b733a31383a2267726f75705f7377697463685f76616c7565223b733a383a22666565642e73716c223b733a31303a2253514c5f445249564552223b613a323a7b733a31313a22636f72655f647269766572223b733a343a22636f7265223b733a31383a2267726f75705f7377697463685f76616c7565223b733a343a22636f7265223b7d7d7d),
('gui.ajax', 0x613a313a7b733a32323a22435553544f4d5f57454c434f4d455f4d455353414745223b733a31363a2257656c636f6d6520746f20507964696f223b7d);

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_repo`
--

DROP TABLE IF EXISTS `ajxp_repo`;
CREATE TABLE IF NOT EXISTS `ajxp_repo` (
  `uuid` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `parent_uuid` varchar(33) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `child_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accessType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recycle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcreate` tinyint(1) DEFAULT NULL,
  `writeable` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `isTemplate` tinyint(1) DEFAULT NULL,
  `inferOptionsFromParent` tinyint(1) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `groupPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_repo_options`
--

DROP TABLE IF EXISTS `ajxp_repo_options`;
CREATE TABLE IF NOT EXISTS `ajxp_repo_options` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `val` blob,
  PRIMARY KEY (`oid`),
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=134 ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_roles`
--

DROP TABLE IF EXISTS `ajxp_roles`;
CREATE TABLE IF NOT EXISTS `ajxp_roles` (
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_role` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ajxp_roles`
--

INSERT INTO `ajxp_roles` (`role_id`, `serial_role`) VALUES
('AJXP_USR_/admin', 'O:9:"AJXP_Role":6:{s:12:"\0*\0groupPath";N;s:9:"\0*\0roleId";s:15:"AJXP_USR_/admin";s:7:"\0*\0acls";a:7:{i:1;s:2:"rw";s:9:"ajxp_user";s:2:"rw";s:9:"ajxp_home";s:2:"rw";s:9:"ajxp_conf";s:2:"rw";i:0;s:2:"rw";s:11:"fs_template";s:2:"rw";i:2;s:2:"rw";}s:13:"\0*\0parameters";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:1:{s:9:"core.conf";a:1:{s:17:"USER_DISPLAY_NAME";s:5:"admin";}}}s:10:"\0*\0actions";a:0:{}s:14:"\0*\0autoApplies";a:0:{}}'),
('GUEST', 'O:9:"AJXP_Role":6:{s:12:"\0*\0groupPath";N;s:9:"\0*\0roleId";s:5:"GUEST";s:7:"\0*\0acls";a:0:{}s:13:"\0*\0parameters";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:1:{s:9:"core.conf";a:1:{s:17:"ROLE_DISPLAY_NAME";s:15:"Guest user role";}}}s:10:"\0*\0actions";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:3:{s:9:"access.fs";a:1:{s:5:"purge";s:8:"disabled";}s:10:"meta.watch";a:1:{s:12:"toggle_watch";s:8:"disabled";}s:12:"index.lucene";a:1:{s:5:"index";s:8:"disabled";}}}s:14:"\0*\0autoApplies";a:1:{i:0;s:5:"guest";}}'),
('MINISITE', 'O:9:"AJXP_Role":6:{s:12:"\0*\0groupPath";N;s:9:"\0*\0roleId";s:8:"MINISITE";s:7:"\0*\0acls";a:0:{}s:13:"\0*\0parameters";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:1:{s:9:"core.conf";a:1:{s:17:"ROLE_DISPLAY_NAME";s:14:"Minisite Users";}}}s:10:"\0*\0actions";a:1:{s:22:"AJXP_REPO_SCOPE_SHARED";a:9:{s:9:"access.fs";a:3:{s:9:"ajxp_link";b:0;s:5:"chmod";b:0;s:5:"purge";b:0;}s:10:"meta.watch";a:1:{s:12:"toggle_watch";b:0;}s:11:"conf.serial";a:1:{s:13:"get_bookmarks";b:0;}s:8:"conf.sql";a:1:{s:13:"get_bookmarks";b:0;}s:12:"index.lucene";a:1:{s:5:"index";b:0;}s:12:"action.share";a:6:{s:5:"share";b:0;s:17:"share-edit-shared";b:0;s:22:"share-folder-workspace";b:0;s:19:"share-file-minisite";b:0;s:24:"share-selection-minisite";b:0;s:28:"share-folder-minisite-public";b:0;}s:8:"gui.ajax";a:1:{s:8:"bookmark";b:0;}s:11:"auth.serial";a:1:{s:11:"pass_change";b:0;}s:8:"auth.sql";a:1:{s:11:"pass_change";b:0;}}}s:14:"\0*\0autoApplies";a:0:{}}'),
('MINISITE_NODOWNLOAD', 'O:9:"AJXP_Role":6:{s:12:"\0*\0groupPath";N;s:9:"\0*\0roleId";s:19:"MINISITE_NODOWNLOAD";s:7:"\0*\0acls";a:0:{}s:13:"\0*\0parameters";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:1:{s:9:"core.conf";a:1:{s:17:"ROLE_DISPLAY_NAME";s:28:"Minisite Users - No Download";}}}s:10:"\0*\0actions";a:1:{s:22:"AJXP_REPO_SCOPE_SHARED";a:1:{s:9:"access.fs";a:4:{s:8:"download";b:0;s:14:"download_chunk";b:0;s:16:"prepare_chunk_dl";b:0;s:12:"download_all";b:0;}}}s:14:"\0*\0autoApplies";a:0:{}}'),
('ROOT_ROLE', 'O:9:"AJXP_Role":6:{s:12:"\0*\0groupPath";N;s:9:"\0*\0roleId";s:9:"ROOT_ROLE";s:7:"\0*\0acls";a:3:{i:1;s:2:"rw";s:9:"ajxp_user";s:2:"rw";s:9:"ajxp_home";s:2:"rw";}s:13:"\0*\0parameters";a:1:{s:19:"AJXP_REPO_SCOPE_ALL";a:3:{s:9:"core.conf";a:2:{s:17:"ROLE_DISPLAY_NAME";s:9:"Root Role";s:24:"DEFAULT_START_REPOSITORY";s:9:"ajxp_home";}s:13:"meta.syncable";a:3:{s:13:"REPO_SYNCABLE";s:4:"true";s:23:"OBSERVE_STORAGE_CHANGES";s:5:"false";s:21:"OBSERVE_STORAGE_EVERY";s:1:"5";}s:17:"action.disclaimer";a:1:{s:19:"DISCLAIMER_ACCEPTED";s:2:"no";}}}s:10:"\0*\0actions";a:0:{}s:14:"\0*\0autoApplies";a:2:{i:0;s:8:"standard";i:1;s:5:"admin";}}');

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_simple_store`
--

DROP TABLE IF EXISTS `ajxp_simple_store`;
CREATE TABLE IF NOT EXISTS `ajxp_simple_store` (
  `object_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `store_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `serialized_data` longtext COLLATE utf8_unicode_ci,
  `binary_data` longblob,
  `related_object_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertion_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`object_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_users`
--

DROP TABLE IF EXISTS `ajxp_users`;
CREATE TABLE IF NOT EXISTS `ajxp_users` (
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `groupPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ajxp_users`
--

INSERT INTO `ajxp_users` (`login`, `password`, `groupPath`) VALUES
('admin', 'sha256:1000:J8j5safuTpnVu8Q67JvU6xFJMbqwvWYu:BABIZZomiQsi2MTkxSaF4VDnFePHFX2R', '/');

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_user_bookmarks`
--

DROP TABLE IF EXISTS `ajxp_user_bookmarks`;
CREATE TABLE IF NOT EXISTS `ajxp_user_bookmarks` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `repo_uuid` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_user_prefs`
--

DROP TABLE IF EXISTS `ajxp_user_prefs`;
CREATE TABLE IF NOT EXISTS `ajxp_user_prefs` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `val` blob,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;
INSERT INTO `ajxp_user_prefs` (`rid`, `login`, `name`, `val`) VALUES
(3, 'admin', 'gui_preferences', 0x7b22636f6e74656e745f70616e655f46696c65734c697374223a7b227265706f2d616a78705f636f6e66223a7b22646973706c6179223a226c697374227d7d2c22766572746963616c5f73706c69747465725f53706c6974746572223a7b227265706f2d616a78705f636f6e66223a7b22666f6c646564223a66616c73657d7d7d);

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_user_rights`
--

DROP TABLE IF EXISTS `ajxp_user_rights`;
CREATE TABLE IF NOT EXISTS `ajxp_user_rights` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `repo_uuid` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `rights` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rid`),
  KEY `login` (`login`),
  KEY `repo_uuid` (`repo_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Contenu de la table `ajxp_user_rights`
--

INSERT INTO `ajxp_user_rights` (`rid`, `login`, `repo_uuid`, `rights`) VALUES
(35, 'admin', 'ajxp.admin', '1'),
(36, 'admin', 'ajxp.roles', '$phpserial$a:2:{s:9:"ROOT_ROLE";b:1;s:15:"AJXP_USR_/admin";b:1;}');

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_user_teams`
--

DROP TABLE IF EXISTS `ajxp_user_teams`;
CREATE TABLE IF NOT EXISTS `ajxp_user_teams` (
  `team_id` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ajxp_version`
--

DROP TABLE IF EXISTS `ajxp_version`;
CREATE TABLE IF NOT EXISTS `ajxp_version` (
  `db_build` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ajxp_version`
--

INSERT INTO `ajxp_version` (`db_build`) VALUES
(60);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
