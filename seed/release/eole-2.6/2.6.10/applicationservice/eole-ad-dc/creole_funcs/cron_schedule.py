# -*- coding: utf-8 -*-

def interval_to_cron_schedule(interval):
    """Return cron scheduling information given an interval.
    :param interval: interval between two execution of a command
    :type interval: int
    """
    from random import randint, choice
    interval = int(interval)
    cron_schedule = "{minutes} {hours} {days} {months} {weekdays}"
    minutes = randint(0, 59)
    hours = "*"
    days = '*'
    months = '*'
    weekdays = '0-7'
    if interval < 24 and interval > 1:
        hours = '*/{}'.format(interval)
    elif interval == 24:
        hours = choice([i for i in range(0, 24) if i not in range(6, 19)])
    elif interval > 24:
        hours = choice([i for i in range(0, 24) if i not in range(6, 19)])
        days = '*/{}'.format(interval/24)
    cron_schedule = cron_schedule.format(minutes=minutes,
                                         hours=hours,
                                         days=days,
                                         months=months,
                                         weekdays=weekdays)
    return cron_schedule
