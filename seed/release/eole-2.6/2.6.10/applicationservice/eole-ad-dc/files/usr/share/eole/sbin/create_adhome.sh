#!/bin/bash
login=${1}
homebasedir=${2}
userdir="${homebasedir}/${login}"
[ -d "${userdir}" ] && exit 0
mkdir -p "${userdir}"
setfacl -Rbk "${userdir}"
chmod 700 "${userdir}"
setfacl -m u:${login}:rwx "${userdir}"
setfacl -dm u:${login}:rwx "${userdir}"
