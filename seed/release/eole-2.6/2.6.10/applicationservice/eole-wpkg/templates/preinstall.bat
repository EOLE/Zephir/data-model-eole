rem remet les chemins par defaut avant l'installation
regedit /E %WINDIR%\sauv_menu-dem.reg "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"
regedit /E %WINDIR%\sauv_menu-dem2.reg "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
%if %%mode_conteneur_actif == 'non'
set ip-scribe=%%adresse_ip_eth0
%else
set ip-scribe=%%adresse_ip_fichier_link
%end if
regedit /S "\\\%ip-scribe%\wpkg\bureau-menu_demarrer.reg"
