#!/usr/bin/python

def pwdreader(encoder,reader):
    try:
        mypwdreader=open(reader, "r").readline().rstrip()
        if encoder == "":
            return mypwdreader
        elif encoder == "base64":
            import base64
            return base64.b64encode(mypwdreader)
        
    except:
        return "mot de passe inconnu"
