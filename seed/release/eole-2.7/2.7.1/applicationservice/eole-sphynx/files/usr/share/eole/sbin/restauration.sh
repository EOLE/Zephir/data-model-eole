#!/bin/bash
##########################################
# Restauration EOLE
# - Zephir -
# LB Dijon 13/01/2006
# bruno boiget <bruno.boiget@ac-dijon.fr>
# samuel morin <samuel.morin@ac-dijon.fr>
##########################################

# color ASCII codes
#
DEFAULT="\e[0m"
BOLD="\e[1m"
BLACK="\e[30m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
VIOLET="\e[35m"
CYAN="\e[36m"
WHITE="\e[37m"

Lock="sauvegarde"
Excl="maj"

RepEole=${RepEole="/usr/share/eole"}
activer_haute_dispo=$(CreoleGet activer_haute_dispo)

backup_dir=/var/lib/sphynx_backups


if [ ! -d $backup_dir ]
then
	echo -e "${RED} Erreur, répertoire des sauvegardes non trouvé ${DEFAULT}"
	exit 1
fi
pushd $backup_dir &>/dev/null 2>&1

if [ ! -z $1 ]
then
	archive=$1
        basename=`echo -e $archive | sed -e s/"\.tar\.gz"/""/`
else
	archive=/tmp
fi

while [ ! -f $archive ]
do
	# pas de fichier passé en paramètre
	save_files=`ls *.tar.gz | sed -e s/".tar.gz"/""/`
	echo -e "     Utilitaire de restauration sphynx"
	echo -e
	echo -e "${RED}!!Attention : toutes les modifications effectuées"
	echo -e "  après la sauvegarde restaurée seront perdues!!${DEFAULT}"
	echo -e
	echo -e "${CYAN}liste des sauvegardes présentes :${DEFAULT}"
	echo -e
	echo -e "$save_files"
	echo -e
	read -p "sauvegarde à restaurer (rien pour sortir): " basename
	if [ -z $basename ]
	then
		exit 0
	fi
	if [ -f $basename.tar.gz ]
	then
		archive=$basename.tar.gz
	else
		echo -e "fichier invalide : $basename.tar.gz"
	fi
done

svg_version_file="2.3.10"
echo -e "décompression en cours..."
tar xzf $archive
cd $basename
echo -e "vérification des données..."
if ! ( [ -f var/lib/arv/db/sphynxdb.sqlite ] && ( [ -f etc/ipsec.d/ipsec.db ] && [ -f etc/ipsec.conf ] || [ -f etc/ipsec.conf ] ))
then
    if [ ! -f sphynxdb.sqlite ] && [ ! -f ipsec.db ]
    then
        echo -e "${RED}sauvegarde de la base ARV et de la configuration Strongswan non trouvée${DEFAULT}"
        exit 1
    else
        echo -e "Sauvegarde effectuée par un serveur Sphynx non à jour !!!"
        echo -e "Génération d'un fichier ipsec.conf minimal pour le mode database..."
        echo "config setup
    uniqueids = yes
    cachecrls = yes
    strictcrlpolicy = no
">/etc/ipsec.conf
        svg_version_file="2.3.9"
    fi
fi
if [ ! -f root_ssh.tar ]
then
	echo -e "${RED}sauvegarde des clefs ssh non trouvée${DEFAULT}"
	exit 1
fi

if [ ! -f conf_eole.tar ]
then
	echo -e "${RED}sauvegarde de la configuration du serveur non trouvée${DEFAULT}"
	exit 1
fi

ARV_INIT_SCE="arv"
CURRENT_EOLE_VERSION=$(dpkg -p creole | grep Version)
CURRENT_EOLE_VERSION=$(echo $CURRENT_EOLE_VERSION | sed -e 's/^Version: \([0-9]*\.[0-9]*\.[0-9]\).*/\1/')
if [[ $CURRENT_EOLE_VERSION =~ ^2\.4\..*$ ]]
then
    IPSEC_INIT_SCE="ipsec"
else
    IPSEC_INIT_SCE="strongswan"
fi

tar -C /tmp/ -xpf conf_eole.tar etc/eole/config.eol >/dev/null 2>&1
read FIST_LINE < /tmp/etc/eole/config.eol
echo $FIST_LINE | grep -qe "^\["
if [ $? -eq 0 ]
then
    SVG_EOLE_VERSION="2.3"
else
    echo $FIST_LINE | grep -qe "^{"
    if [[ $? -eq 0 ]]
    then
        VERSION_LINE=$(grep -e "___version___" /tmp/etc/eole/config.eol)
        if [[ $? -eq 0 ]]
        then
            SVG_EOLE_VERSION=$(echo $VERSION_LINE | sed -e 's/^.*___version___":[ ]*"\([0-9]*\.[0-9]*\.[0-9]*\).*$/\1/')
        else
            SVG_EOLE_VERSION="2.4.0"
        fi
    else
        SVG_EOLE_VERSION="INCONNUE"
    fi
fi
echo -e "Restauration d'une sauvegarde $SVG_EOLE_VERSION sur $CURRENT_EOLE_VERSION"

# restauration des données de la base xml
. /usr/lib/eole/ihm.sh
Question_ouinon "Restaurer les bases ARV et la configuration Strongswan (o/n) ?" "True" "non"
if [ $? -eq 0 ];then
    echo -e " - bases ARV + Strongswan"
    if [ "${svg_version_file}" == "2.3.9" ]
    then
        cp -f sphynxdb.sqlite /var/lib/arv/db/
        cp -f ipsec.db /etc/ipsec.d/
    elif [ "${svg_version_file}" == "2.3.10" ]
    then
        rsync --owner --group --recursive --links --perms --times etc/ipsec* /etc/
        rsync --owner --group --recursive --links --perms --times var/lib/arv/* /var/lib/arv/
    fi
    if [ "${activer_haute_dispo}" == "non" ]
    then
        service $IPSEC_INIT_SCE stop
        service $ARV_INIT_SCE stop
        service $ARV_INIT_SCE  start &>/dev/null
        service $IPSEC_INIT_SCE start &>/dev/null
    elif [ "${activer_haute_dispo}" == "maitre" ]
    then
        service strongswan stop
    fi
fi
echo -e

# restauration des répertoires et fichiers importants
# /root/.ssh
rm -rf /root/.ssh
tar -C /root -xpf root_ssh.tar >/dev/null 2>&1
echo -e " - configuration eole"

if [ -f /etc/eole/config.eol ]
then
    mv -f /etc/eole/config.eol /etc/eole/config.eol.bak
fi

echo -e "${CYAN}"
# vérification du dictionnaire
if [ "$SVG_EOLE_VERSION" == "$CURRENT_EOLE_VERSION" ]
then
    if [ -f /etc/eole/config.eol.bak ] && [ -f /etc/eole/.instance ]
    then
        diff -q /etc/eole/config.eol.bak /tmp/etc/eole/config.eol &>/dev/null
        if [ ! $? -eq 0 ]
        then
            echo -e "Le fichier /etc/eole/config.eol est différent de la version restaurée"
            echo -e "Le fichier présent actuellement a été renommé en /etc/eole/config.eol.bak"
            echo -e
            echo -e "  - Renommez éventuellement /etc/eole/config.eol.bak en /etc/eole/config.eol auparavant"
            echo -e "  - Lancez 'reconfigure'"
            echo -e
        else
            echo -e "Lancez 'reconfigure'"
        fi
    else
        echo -e "Après la fin de la restauration :"
        echo -e "  - Lancez 'instance'"
    fi

    # Restauration dicos, patchs, ssl
    rm -rf /usr/share/eole/creole/dicos /usr/share/eole/creole/patch /etc/ssl/*
    tar -C / -xpf conf_eole.tar >/dev/null 2>&1
else
    echo -e "Après la fin de la restauration :"
    echo -e "  - Lancez 'gen_config' et enregistrez la configuration"
    if [ -f /etc/eole/config.eol.bak ] && [ -f /etc/eole/.instance ]
    then
        echo -e "  - Lancez 'reconfigure'"
    else
        echo -e "  - Lancez 'instance'"
        echo -e "    Attention à ne PAS réinitialiser la base ARV pendant l'instance !!!"
    fi
    # Si version EOLE de la sauvegarde différente, restaure config.eol uniquement
    tar -C / -xpf conf_eole.tar etc/eole/config.eol >/dev/null 2>&1
fi
echo -e "${DEFAULT}"

echo -e "Suppression du répertoire temporaire..."
rm -f /tmp/etc/eole/config.eol
rm -rf $backup_dir/$basename
echo -e "${GREEN}Système restauré${DEFAULT}"
echo -e
# retour au répertoire d'origine
popd >/dev/null 2>&1

