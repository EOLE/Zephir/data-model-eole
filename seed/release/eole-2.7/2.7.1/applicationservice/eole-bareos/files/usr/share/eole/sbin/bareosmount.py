#!/usr/bin/python
# -*- coding: UTF-8 -*-

from optparse import OptionParser, OptionGroup
import sys

from pyeole.bareos import test_bareos_support, mount_bareos_support, umount_bareos_support, bareos_active_sd, mount_status_to_str


def main():
    #get options
    parser = OptionParser(usage="Seulement une option \n%prog [option]")
    parser.add_option("--mount", action="store_true", dest="mount",
            default=False, help=u"Monte le support")
    parser.add_option("--umount", action="store_true", dest="umount",
            default=False, help=u"Démonte le support")
    parser.add_option("-t", "--test", action="store_true", dest="test",
            default=False, help=u"Test de la configuration du support")
    parser.add_option("-o", "--owner", action="store_true", dest="chown",
            default=False, help=u"Change le propriétaire du point de montage")
    parser.add_option("-f", "--force", action="store_true", dest="force",
            default=False, help=u"Force l'exécution du test sans vérifier l'activation des services")
    parser.add_option("--volume", dest="volume", default="",
            help=u"Volume utilisé (option utilisée pour le démontage sélectif)")

    (options, args) = parser.parse_args()


    if options.test == False and options.mount == False and options.umount == False:
        parser.print_help()
        sys.exit(1)

    if options.test != False:
        try:
            status, comment = test_bareos_support(options.force, chown=options.chown)
            if status == True:
                print "Test OK"
            else:
                print 'Échec du test de montage :\n', comment
                sys.exit(1)
        except Exception, e:
            print unicode(e)
            sys.exit(1)

    if options.mount != False:
        try:
            status, detail = mount_bareos_support(chown=options.chown)
            if status == True:
                print "Montage OK"
            else:
                print "Échec du montage :\n", mount_status_to_str(detail)
                sys.exit(1)
        except Exception, e:
            print unicode(e)
            sys.exit(1)

    if options.umount != False:
        try:
            ret = umount_bareos_support(options.volume)
            if ret == True:
                print "Démontage OK"
            else:
                print ret
        except Exception, e:
            print unicode(e)
            sys.exit(1)

if __name__ == "__main__":
    if bareos_active_sd():
        main()
    else:
        print "Bareos sd local est désactivé ; le support de sauvegarde n'est pas testé."

