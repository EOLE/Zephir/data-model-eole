#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Restauration du catalogue de bareos (attention il faut savoir ce que l'on fait !)"""
from pyeole.bareosrestore import extract_bareos_files, bareos_restore_dump, \
        restore_file
from creole.loader import load_store
from sys import exit
from creole.client import CreoleClient


client = CreoleClient()


def execute(option, opt_str, value, parser, jobid, test_jobs=False):
    """catalog helper
    never test jobs
    """
    if client.get_creole('module_instancie') == 'non':
        print "Le serveur doit être instancié pour lancer cette commande"
        print "Peut-être voulez-vous utiliser l'option --configeol ?"
        exit(1)
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
        else:
            print "Attention, il ne faut plus spécifier le nom du directeur Bareos"
            exit(1)
    else:
        job(jobid)

def pre():
    print "pre catalog"

def post():
    print "post catalog"

def job(jobid):
    print "Restauration du catalogue"
    try:
        extract_bareos_files()
        bareos_restore_dump()
    except Exception,e:
        print e
        exit(1)

priority=0

