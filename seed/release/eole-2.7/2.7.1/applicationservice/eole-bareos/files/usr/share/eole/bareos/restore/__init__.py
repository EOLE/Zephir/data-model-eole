"""
Chargement des plugins de l'utilitaire de sauvegarde
"""


#on ajoute des '__' pour que lors du chargement des plugins les variables
#propre a l'init ne soit pas prises en compte
import glob as __glob__
import re as __re__
import os as __os__

__os__.chdir('/usr/share/eole/bareos')

__zorg__ = __glob__.glob('restore'+'/[a-z]*.py')

for __p__ in __zorg__:
    __m__ = __re__.search('(?<=/)\w+', __p__)
    exec "import %s" %__m__.group()
