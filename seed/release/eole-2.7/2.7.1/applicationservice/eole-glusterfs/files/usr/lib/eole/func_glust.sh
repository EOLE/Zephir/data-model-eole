#!/bin/bash

function glusterTest {
    hostname=$(CreoleGet glusterfs_servername)
    for remote_name in $(CreoleGet glusterfs_remote_servername); do
        if [ -z $first_host ]; then
            first_host=$remote_name
        fi
        if [[ $remote_name != $hostname ]]; then
            tcpcheck 2 $remote_name:24007 > /dev/null 2>&1
            [[ $? != 0 ]] && echo "le port 24007 n'est pas accessible pour $remote_name" && exit 1
            if [[ $2 != 'remote' ]]; then
                tcpcheck 2 $remote_name:22 > /dev/null 2>&1
                if [[ $? != 0 ]]; then
                    echo "le port 22 (SSH) n'est pas accessible pour $remote_name"
                    echo "Veuillez lancer le script de test './glusterfs_init test' sur $remote_name et valider si le test est ok"
                    read a
                else
                    ssh root@$remote_name "/usr/share/eole/sbin/glusterfs_init test remote"
                    [[ $? != 0 ]] && echo "le test est en erreur sur $remote_name" && exit 1
                fi
            fi
            if [ $first_host = $hostname ]; then
                # if not primary, test only primary
                break
            fi
        fi
    done
    dirname_data=$(CreoleGet glusterfs_dirname_data)
    if [ ! -d "$dirname_data" ]; then
        echo "le répertoire $dirname_data n'existe pas"
        exit 1
    fi
    if [[ $2 = 'remote' && $first_host = $hostname ]]; then
        # we are in master node
        mount_point="$(CreoleGet glusterfs_servername):/$(CreoleGet glusterfs_name)"
        df|grep -q ^"$mount_point "
        if [ ! $? = 0 ]; then
            echo "$mount_point n'est pas monté sur $hostname"
            exit 1
        fi
    fi
    if [[ $2 != 'remote' ]]; then
        echo "L'accès aux différents noeuds est ok"
    fi
}

error(){
    echo "ERREUR : parametres invalides !" >&2
    echo "utilisez l'option -h pour en savoir plus" >&2
    exit 1
}

usage(){
    echo "Usage for test: ./glusterfs_init [options]"
    echo "Usage for install : ./glusterfs_init"
    echo "-h : afficher l'aide"
    echo "test : Tester les serveurs distants"
}


# Pas de paramètre
[[ $1 ]] && [[ $1 != "test" ]] && [[ $1 != '-h' ]] && error

while getopts ":b:h" option; do
    case "$option" in
        test) glusterfs_init test ;;
        :) error ;;
        h) usage ;;
        *) error ;;
    esac
    exit 0
done
