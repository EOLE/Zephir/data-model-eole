#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import argparse
from os import listdir, devnull
from os.path import join
import re
import getpass
from subprocess import check_call
import shlex
import operator
from creole.utils import gen_random


CONF_FILE_RE = re.compile(r'^.*\.yml$')
DEFAULT_ROLE_FILTERS = {'pwd_mode': 'manuelle' ,
                        'dbtype': 'postgres'}


def gen_password():
    """
    Return random password
    """
    return gen_random()


def get_roles(conf_path, role_filters=DEFAULT_ROLE_FILTERS):
    """
    Return list of role matching given filter
    :param conf_path: path of configuration file
    :type conf: str
    :param role_filter: filter with key and value to match
    :type role_filter: dict
    """
    with open(conf_path, 'r') as conf_stream:
        conf = yaml.load(conf_stream)
    if conf is not None and 'additional_roles' in conf:
        roles = [role for role in conf['additional_roles']
                 if reduce(operator.and_, [conf['additional_roles'][role].get(key, False) == value for key, value in role_filters.iteritems()])]
    else:
        roles = []
    return roles


def change_password(role):
    """
    Execute ALTER ROLE sql command for role with generated password or password
    input by user
    :param role: name of the role
    :type role: str
    """
    print "Changement du mot de passe pour {}".format(role)
    if is_validated(raw_input("Générer un mot de passe aléatoire ?")):
        password = gen_password()
    else:
        password = getpass.getpass('Mot de passe pour le role {} :'.format(role))
    sql = "ALTER ROLE {0} WITH PASSWORD '{1}';".format(role, password)
    cmd = 'psql -U postgres -c "{0}" '.format(sql)
    with open(devnull, 'w') as dev_null:
        code = check_call(shlex.split(cmd),
                          stdout=dev_null,
                          stderr=dev_null)
    if code == 0:
        return (role, password)
    else:
        return (role, None)


def is_validated(user_input):
    """
    Return True if user_input is considered positive answer.
    :param user_input: user input
    :type user_input: str
    """
    return user_input.lower() in ['yes', 'oui', 'y', 'o']


def main():
    arg_parser = argparse.ArgumentParser(description='EOLE PostgreSQL role password manager')
    arg_parser.add_argument('-c', '--config', metavar='CONFIG_FILE',
                        default='/etc/eole/eole-db.conf',
                        help='eole database configuration file)')
    arg_parser.add_argument('-d', '--dbdir', metavar='DB_CONFIG_DIR',
                        default='/etc/eole/eole-db.d/',
                        help='eole database configuration directory)')
    arg_parser.add_argument('-r', '--report', action='store_true', default=False,
                            help="afficher les couples role, mot de passe modifiés à la sortie du programme")

    args = arg_parser.parse_args()

    roles = []
    for conf_path in listdir(args.dbdir):
        if CONF_FILE_RE.match(conf_path):
            roles.extend(get_roles(join(args.dbdir, conf_path)))

    passwords = []
    for role in roles:
        if is_validated(raw_input("Voulez-vous changer le mot de passe pour le rôle {} ?".format(role))):
            passwords.append(change_password(role))
    len_pwd = len(passwords)
    print "{0} mot{1} de passe modifié{1}".format(len_pwd, '' if len_pwd < 2 else 's')
    if args.report is True:
        print '\n'.join(['\t'.join(password) for password in passwords])

if __name__ == '__main__':
    main()
