#-*-coding:utf-8-*-
###########################################################################
"""
    Configuration pour la création de la base de données de mon appli
"""
from os.path import join
from eolesql.db_test import db_exists, test_var
from creole.client import CreoleClient

client = CreoleClient()

TABLEFILENAMES = [join('/', client.get_creole('container_path_web'), 'usr/share/dbconfig-common/data/phpmyadmin/install/mysql')]

def test():
    """
        test l'existence de la base de donnée roundcube
    """
    return test_var('activer_phpmyadmin') and not db_exists('phpmyadmin')

def pregen_func(db_handler):
    """
        Fonction exécutée avant la génération si le test est bon
    """
    db_handler.simple_query('create database phpmyadmin')

conf_dict = dict(filenames=TABLEFILENAMES,
                 test=test, database='phpmyadmin',
                 pregen=pregen_func)


