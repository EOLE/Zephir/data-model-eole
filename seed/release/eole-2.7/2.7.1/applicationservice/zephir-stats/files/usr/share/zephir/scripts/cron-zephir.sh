#!/bin/bash
###########################################################################
# Eole NG - 2007
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# cronzephir.sh
#
# script relançant le service z_stats si il est arreté
#
###########################################################################

# ne pas relancer z_stats avant la première installation
is_inst=$(CreoleGet "module_instancie")
[ "$is_inst" = "non" ] && exit 0

/usr/bin/enregistrement_zephir --check >/dev/null
if [ $? -eq 0 ]
then
	# le serveur est enregistré, on vérifie que le service de statistiques fonctionne
	systemctl status z_stats &> /dev/null
	if [ $? != 0 ]
	then
		. /usr/lib/eole/zephir.sh
		# pas de redémarrage au milieu du reconfigure (#3133)
		# test de lock système globaux via CreoleLock
		CreoleLock is_locked --level=system
		if [ $? -ne 0 ]
		then
			CreoleService z_stats restart
			Zephir "MSG" "Service z_stats arreté : redémarrage" "ZEPHIR"
		fi
	fi
fi
exit 0
