# -*- coding: UTF-8 -*-
from ead2.backend.actions.lib.main import Cmd

class Cmd_Zephir(Cmd):
    """
    Action du mode commande
    """
    name = "cmd_update_zephir"
    # propriété de la commande à exécuter
    cmd_template = "/usr/share/zephir/scripts/zephir_client save_files 3"
    cmd_libelle = "Remonter les données locales sur Zéphir"

