
require.config({

  paths : {
    underscore : 'lib/underscore',
    backbone   : 'lib/backbone',
    marionette : 'lib/backbone.marionette',
    jquery     : 'lib/jquery.min',
    bootstrap  : 'lib/bootstrap.min',
    modernizr  : 'lib/modernizr',
    tpl        : 'lib/tpl',
    i18n       : 'lib/i18n'
  },

  locale: localStorage.locale || 'fr-fr',

  shim : {
    'lib/underscore-string'               : ['underscore'],
    'lib/backbone-localStorage'           : ['backbone'],
    'lib/bootstrap-contextmenu'           : ['bootstrap'],
    'lib/bootstrap-typeahead'             : ['bootstrap'],
    'lib/bootstrap-select'                : ['bootstrap'],
    'lib/bootstrap-select2'               : ['bootstrap'],
    'lib/jquery-input-ip-address-control' : ['jquery'],
    'lib/jquery.iframe-transport'         : ['jquery'],
    'lib/jquery.fileupload'               : ['jquery'],
    underscore : {
      exports : '_'
    },
    'lib/underscore-string': {
      deps: ['underscore'],
      init: function(_) {
        _.mixin(_.str.exports());
        return _;
      }
    },
    backbone : {
      exports : 'Backbone',
      deps : ['jquery','underscore']
    },
    marionette : {
      exports : 'Backbone.Marionette',
      deps : ['backbone']
    },
    bootstrap: {
      exports : 'jquery',
      deps : ['jquery']
    },
  },

  deps : ['jquery','underscore']
});

require(['app','backbone','marionette','routers/index','controllers/index','views/ErrorView','templates','strings','helpers/util','lib/underscore-string','bootstrap','modernizr'],
function (app,Backbone,Marionette,Router,Controller,ErrorView,templates,strings,util){
  "use strict";


  /* Scroll to focused element if outside layout */
  $.fn.scrollToThis = function(){
    var x = window.scrollX, y = window.scrollY, offset;
    if(this.css('display') == "none"){
      offset =  this.parents('.view').offset().top;
    } else {
      try {
        offset = this.offset().top;
      } catch (e) {
        console.log('error', e);
        return;
      }
    }
    if(offset != 0)
      if(offset < y+80) { window.scrollTo(x, offset-80); }
      else if(offset > $(window).height()+y-60) { window.scrollTo(x, offset-$(window).height()+60); }
  };

  $.fn.focusNscroll = function(){
    $(this).focus();
    $(this).scrollToThis();
  };

  window.ErrorView = ErrorView;

  /* Browser detection patch */
  jQuery.browser = {};
  jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

  /* Persist Locale in localstorage */
  var locale = localStorage.getItem('locale');
  if(!locale || typeof locale === 'undefined')
    localStorage.setItem('locale', 'fr-fr');

  /* Load translated string */
  _.mixin(_.str.exports());

  /* Translate Method */
  window.t = function(label){
    if(arguments.length > 1) {
      arguments[0] = strings.genconfig[label];
      return _.sprintf.apply(_, arguments);
    }
    return strings.genconfig[label];
  };

  /* Extend the data given to the templates */
  _.extend(Marionette.View.prototype, {
      serializeData: function(){
        var data;
        if (this.model) {
          data = this.model.toJSON();
          data.id = this.model.id;
        } else if (this.collection)
          data = { items: this.collection.toJSON() };
        data = this.mixinTemplateHelpers(data);
        data.t = t;
        data.templates = templates;
        return data;
      },
      mixinTemplateHelpers: function(target){
        target = target || {};
        var templateHelpers = this.templateHelpers;
        if (_.isFunction(templateHelpers)){
          templateHelpers = templateHelpers.call(this);
        }
        return _.extend(target, templateHelpers, util);
      }
  });

  /* Override sync method for url_prefix */
  var sync = function(method, model, options){
    if (!options.url) {
      options.url = app.config.path_prefix + _.result(model, 'url') || urlError();
    } else {
      options.url = app.config.path_prefix + options.url;
    }
    if(!options.error){
      options.complete = function(){ alert('test'); };
      options.error = function(response){
        new ErrorView({
          model: new Backbone.Model(response)
        }).render().$el.modal('show');
      };
    }
    return Backbone.sync.call(this, method, model, options);
  };
  _.extend(Backbone.Model.prototype, { sync: sync });
  _.extend(Backbone.Collection.prototype, { sync: sync });

  /* Start Application */
  app.start();

  /* Init router */
  app.router = new Router({
    controller : Controller
  });

  /* Global route helper method */
  Backbone.View.goTo = function(loc, callback) {
    app.router.navigate(loc, true);
    if(_.isFunction(callback)) callback();
  };

  /* Remember navigation history */
  Backbone.history.start();
});
