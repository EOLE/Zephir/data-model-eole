define(['vent','marionette','models/Category'],
function(vent,marionette,Category){
  'use strict';

  var Categories =  Backbone.Collection.extend({

    model: Category,

    initialize: function(models, options){
      this.options = options || {};
      this.options.mode = 'basic';
    },

    url: function(models){
      return this.options.url ? this.options.url : "/categories";
    },

    setMode: function(mode){
      this.options.mode = mode;
    }
  });
  return Categories;
});
