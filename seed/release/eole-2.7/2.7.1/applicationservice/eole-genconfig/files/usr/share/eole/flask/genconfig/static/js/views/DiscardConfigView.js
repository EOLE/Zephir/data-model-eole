define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.ItemView.extend({

    tagName: 'div',
    className: 'modal hide fade small',
    template: templates.discardConfigView,

    events: {
      'click .discardConfig': 'discardConfig',
      'click .closeModal': 'close'
    },

    discardConfig: function(){
      this.$el.modal('hide');
      $.post(app.config.path_prefix+'/discard', {}, _.bind(function(){
        app.config.version = "";
        vent.trigger('route:reloadConfig', function(){
          vent.trigger('flash', t('discard_config_success'), 'success');
        });
      }, this));
      //vent.trigger('route:saveConfig');
    },

    close: function(){
      this.$el.modal('hide');
    }
  });
});


