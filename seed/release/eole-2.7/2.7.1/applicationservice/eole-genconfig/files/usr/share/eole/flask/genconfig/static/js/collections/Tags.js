define(['vent','marionette','models/Tag'],
function(vent,marionette,Tag){
  'use strict';

  return Backbone.Collection.extend({
 
    model: Tag,
    
    initialize: function(models, options){
      this.options = options || {};
    }, 

    url: function()  {
      return '/categories/'+this.options.categoryid+'/tags';
    },
  });
});
