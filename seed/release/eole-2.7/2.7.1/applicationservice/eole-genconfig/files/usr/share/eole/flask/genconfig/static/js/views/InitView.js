define(['vent','marionette','templates','views/Main','views/LoadingMessageView','collections/Categories'],
function(vent,Marionette,templates,Main,LoadingMessageView,Categories){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'div',
   // className: 'container-fluid',
    template: templates.initView,
    itemView: Main,
    emptyView: LoadingMessageView,
    itemViewContainer: '.categories',

    events: {
      'click .validate'      : 'validate',
    },

    initialize: function(){
      this.options.categories.url = "/init_categories?init=true"
      this.collection = this.options.categories;
      //this.collection = new Categories([],{
      //  url: "/init_categories?init=true"
      //});
      this.itemViewOptions = {
        categories: this.collection,
        debug: this.options.debug
      };
      this.scrollPos = 0;
      this.bindTo(vent, 'saveScrollPos', this.saveScrollPos, this);
      this.options.modes.each(function(mode){ mode.set('current', false); });
      this.options.modes.get('basic').save({ current: true },{
        success: _.bind(function(){
          this.options.categories.fetch({
            success: _.bind(function(model, response){
              //this.collection.fetch({
              //  success: _.bind(function(){
              //    //this.collection.options.url = app.config.path_prefix+"/init_catagories";
              //  }, this)
              //});
              if(_.isFunction(this.options.callback)) this.options.callback();
            }, this)
          });
        }, this)
      });
    },

    validate: function(){
      vent.trigger('route:validateConfig');
    },

    saveScrollPos: function(pos){
      this.scrollPos = pos;
    },

    onRender: function(){
      window.scrollTo(0, this.scrollPos);
    }

  });
});
