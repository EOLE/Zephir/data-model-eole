define(function(){
  "use strict";

  String.prototype.capitalize = function() {
    return (this.charAt(0).toUpperCase() + this.slice(1)).replace(/_/g," ");
  };

  return {
    categoryIcon: function(){
      //var categoryName = this.categoryid || this.name;
      var category = this.category || this;
      return 'icon-' + (category.icon || 'tags');
      /*switch(true){
        case /messagerie/i.test(categoryName): return 'icon-envelope';
        case /reseau/i.test(categoryName): return 'icon-sitemap';
        case /interface/i.test(categoryName): return 'icon-qrcode';
        case /cups/i.test(categoryName): return 'icon-print';
        case /web/i.test(categoryName): return 'icon-globe';
        case /apache/i.test(categoryName): return 'icon-leaf';
        case /sso/i.test(categoryName): return 'icon-group';
        case /ldap/i.test(categoryName): return 'icon-book';
        case /proftpd/i.test(categoryName): return 'icon-briefcase';
        case /ssl/i.test(categoryName): return 'icon-certificate';
        case /^ent/i.test(categoryName): return 'icon-edit-sign';
        case /ssh/i.test(categoryName): return 'icon-terminal';
        case /container/i.test(categoryName): return 'icon-th';
        case /clamav/i.test(categoryName): return 'icon-shield';
        case /firewall/i.test(categoryName): return 'icon-fire';
        case /services/i.test(categoryName): return 'icon-cogs';
        case /systeme/i.test(categoryName): return 'icon-desktop';
        case /samba/i.test(categoryName): return 'icon-windows';
        case /esu/i.test(categoryName): return 'icon-filter';
        case /mysql/i.test(categoryName): return 'icon-hdd';
        case /logs/i.test(categoryName): return 'icon-edit';
        case /ftp/i.test(categoryName): return 'icon-cloud';
        case /dhcp/i.test(categoryName): return 'icon-bolt';
        default: return 'icon-tags';
      }*/
    }
  };
});
