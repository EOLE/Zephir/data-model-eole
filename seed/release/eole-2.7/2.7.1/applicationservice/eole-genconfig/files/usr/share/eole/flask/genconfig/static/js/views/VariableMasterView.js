define(['vent','marionette','templates','views/VariableRowView','views/VariableView'],
function(vent,Marionette,templates,VariableRowView,VariableView){
  'use strict';

  return Marionette.CompositeView.extend({
  
    tagName: 'div',
    className: 'accordion-group',
    template: templates.variableMasterView,
    itemViewContainer: 'tbody.slaves',
    itemViewOptions: {},

    ui: {
      sub: '.sub',
      link: 'a',
      tooltip: '.tooltip_trigger',
      error: '.alert-error',
      warning: '.alert-warning'
    },

    events: {
      'click .removeValue': 'removeValue'
    },

    initialize: function(){
      this.collection = this.model.slaves;
      delete this.options['model'];
      this.itemViewOptions = _.extend({}, this.itemViewOptions, this.options);
      if(typeof VariableRowView === 'undefined') VariableRowView = require('views/VariableRowView');
      if(typeof VariableView === 'undefined') VariableView = require('views/VariableView');
      this.itemView = VariableRowView;
      this.masterView = VariableView;
      this.bindTo(this.model, 'error', this.error, this);
      this.bindTo(this.model, 'error:cancel', this.errorCancel, this);
    },

    onRender: function(){
      this.ui.link.tooltip();
      this.ui.tooltip.tooltip();
      this.ui.error.hide();
      this.ui.sub.html(new this.masterView(_.extend({}, this.options, { model: this.model })).render().el);  
    },

    removeValue: function(){
      var callback = null;
      this.model.save({},{
        url: this.model.get('group').url()+'/removeValue/'+this.model.get('index'),
        success: _.bind(function(model, response){
            this.close();
            if(this.options.isMandatoryCollection)
              this.options.mandatories.resetAll(callback);
            else
              this.options.categories.fetch({
                success: _.bind(function(collection){
                  if(!this.model.get('tag'))
                    vent.trigger('route:reloadConfig', callback);
                  else
                    this.model.get('tag').get('category').tags.fetch({ success: _.bind(function(){ 
                      var bkp_location = window.location.hash;
                      Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                      if(_.isFunction(callback)) callback();
                      else Backbone.View.goTo(bkp_location);
                    }, this) });
                }, this)
              });
        }, this),
        error: _.bind(function(model, response){ 
          new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
        }, this)
      });
    },

    error: function(msg){
      this.ui.error.show();
      this.ui.error.find('.msg').text(msg);
    },

    errorCancel: function(msg){
      this.ui.error.find('.msg').empty();
      this.ui.error.hide();
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        debug: this.options.debug
      });
    },

    templateHelpers: {
      getMode: function(){
        switch(this.mode){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
