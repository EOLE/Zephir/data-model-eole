require.config({

  paths : {
    underscore   : 'lib/underscore',
    backbone     : 'lib/backbone',
    marionette   : 'lib/backbone.marionette',
    jquery       : 'lib/jquery.min',
    bootstrap    : 'lib/bootstrap.min',
    modernizr    : 'lib/modernizr',
    tpl          : 'lib/tpl',
    i18n         : 'lib/i18n',
    jasmine      : 'lib/jasmine',
    jasmine_html : 'lib/jasmine-html',
    spec         : 'spec'
  },

  locale: localStorage.locale || 'fr-fr',

  shim : {
    'lib/underscore-string'               : ['underscore'],    
    'lib/backbone-localStorage'           : ['backbone'],
    'lib/bootstrap-contextmenu'           : ['bootstrap'],
    'lib/bootstrap-typeahead'             : ['bootstrap'],
    'lib/bootstrap-select'                : ['bootstrap'],
    'lib/bootstrap-select2'               : ['bootstrap'],
    'lib/jquery-input-ip-address-control' : ['jquery'],
    underscore : {
      exports : '_'
    },
    'lib/underscore-string': {
      deps: ['underscore'],
      init: function(_) { 
        _.mixin(_.str.exports());
        return _;
      }
    },
    backbone : {
      exports : 'Backbone',
      deps : ['jquery','underscore']
    },
    marionette : {
      exports : 'Backbone.Marionette',
      deps : ['backbone']
    },
    bootstrap: {
      exports : 'jquery',
      deps : ['jquery']
    },
    jasmine: {
      exports: 'jasmine'
    },
    jasmine_html: {
      deps: ['jasmine'],
      exports: 'jasmine'
    }
  },
  
  deps : ['jquery','underscore']
});

require(['app','backbone','marionette','jasmine_html','routers/index','controllers/index','views/ErrorView','templates','strings','lib/underscore-string','bootstrap','modernizr'],
function (app,Backbone,Marionette,jasmine,Router,Controller,ErrorView,templates,strings){
  "use strict";


  app.config.path_prefix = "";

  /* Scroll to focused element if outside layout */
  $.fn.focusNscroll = function(){
    $(this).focus();
    var x = window.scrollX, y = window.scrollY, top;
    if(this.css('display') == "none"){
      top =  this.parents('tr').offset().top;
    } else { 
      this.focus();
      top = this.offset().top;
    }
    if(top < y+80) { window.scrollTo(x, top-80); }
    else if(top > $(window).height()+y-110) { window.scrollTo(x, top-$(window).height()+110); }
  };

  window.ErrorView = ErrorView;

  /* Browser detection patch */
  jQuery.browser = {};
  jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
  jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

  /* Persist Locale in localstorage */
  var locale = localStorage.getItem('locale');
  if(!locale || typeof locale === 'undefined')
    localStorage.setItem('locale', 'fr-fr');

  /* Load translated string - TODO fix rdm "undefined" */
  _.mixin(_.str.exports());

  /* Translate Method */
  window.t = function(label){
    if(arguments.length > 1) { 
      arguments[0] = strings.genconfig[label];
      return _.sprintf.apply(_, arguments);
    }
    return strings.genconfig[label];
  };

  /* Extend the data given to the templates */
  _.extend(Marionette.View.prototype, {
      serializeData: function(){
        var data;
        if (this.model) 
          data = this.model.toJSON(); 
        else if (this.collection)
          data = { items: this.collection.toJSON() };
        data = this.mixinTemplateHelpers(data);
        data.t = t;
        data.templates = templates;
        return data;
      }
  });

  /* Override sync method for url_prefix */
  var sync = function(method, model, options){
    if (!options.url) {
      options.url = app.config.path_prefix + _.result(model, 'url') || urlError();
    } else {
      options.url = app.config.path_prefix + options.url;
    }
    if(!options.error){
      options.error = function(response){
        new ErrorView({
          model: new Backbone.Model(response)
        }).render().$el.modal('show');
      };
    }
    return Backbone.sync.call(this, method, model, options);
  };
  _.extend(Backbone.Model.prototype, { sync: sync });
  _.extend(Backbone.Collection.prototype, { sync: sync });

  /* Start Application */
  app.start();
  
  /* Init router */
  app.router = new Router({
    controller : Controller
  });

  /* Global route helper method */
  Backbone.View.goTo = function(loc) {
    app.router.navigate(loc, true);
  };

  /* Remember navigation history */
  Backbone.history.start();

  var jasmineEnv = jasmine.getEnv();
  jasmineEnv.updateInterval = 1000;
 
  var htmlReporter = new jasmine.HtmlReporter();
 
  jasmineEnv.addReporter(htmlReporter);
 
  jasmineEnv.specFilter = function(spec) {
    return htmlReporter.specFilter(spec);
  };
 
  var specs = [];
 
  specs.push('spec/models/CategorySpec');
  specs.push('spec/views/SidebarSpec');
 
  $(function(){
    require(specs, function(){
      jasmineEnv.execute();
    });
  });
 
});
