define(['vent','marionette','templates','views/VariableRowView'],
function(vent,Marionette,templates,VariableRowView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'div',
    className: 'container-fluid',
    template: templates.fillMandatoriesView,
    itemViewContainer: 'tbody',
    itemViewOptions: {},

    events: {
      'click .validate'      : 'validate',
      'click .backToCategory': 'backToCategory'
    },

    initialize: function(){
      this.el.id = "fillmandatoriesmodal";
      this.collection = this.options.mandatories;
      this.itemViewOptions = _.extend({ breadcrumbs: true, isMandatoryCollection: true }, this.itemViewOptions, this.options);
      if(!VariableRowView) VariableRowView = require('views/VariableRowView');
      this.itemView = VariableRowView;
      this.scrollPos = 0;
      this.bindTo(vent, 'saveScrollPos', this.saveScrollPos, this);
    },

    validate: function(){
      vent.trigger('route:validateConfig');
    },

    backToCategory: function(){
      Backbone.View.goTo("categories/"+this.options.current_category);
    },

    saveScrollPos: function(pos){
      this.scrollPos = pos;
    },

    onRender: function(){
      window.scrollTo(0, this.scrollPos);
    }
  });
});
