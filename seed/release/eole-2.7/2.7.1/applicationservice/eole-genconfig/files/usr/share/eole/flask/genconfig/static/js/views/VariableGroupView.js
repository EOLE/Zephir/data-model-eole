define(['vent','marionette','templates','collections/Variables','views/VariableMasterView'],
function(vent,Marionette,templates,Variables,VariableMasterView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName   : 'div',
    className : 'row-fluid',
    template: templates.variableGroupView,
    itemView: VariableMasterView,
    itemViewContainer: '.accordion',
    itemViewOptions: {},

    ui: {
      accordion: '.accordion',
      input: '.btn.addValue'
    },

    events: {
      'click .addValue'  : 'addValue',
      'keyup .addValue'  : 'onKeyup',
      'keydown .addValue': 'onKeydown',
      'click .toggle'    : 'toggle'
    },

    initialize: function(){
      VariableMasterView = require('views/VariableMasterView');
      this.itemView = VariableMasterView;
      this.collection = this.model.masters;
      //this.bindTo(this.model, 'change:value', this.render, this);
      delete this.options['model'];
      this.itemViewOptions = _.extend({}, this.itemViewOptions, this.options);
    },

    onRender: function(){
      this.ui.input.tooltip();
    },

    onKeyup: function(e){
      if(e.keyCode == 9) e.preventDefault();
    },

    onKeydown: function(e){
      switch(e.keyCode){
        case 38: // Up
        case 40: // Down
            break;
        case 13: // Enter
          break;
        case 9:  // Tab
          e.stopPropagation();
          e.preventDefault();
          var links = $('.btn.editBtn, .btn.addValue');
          var link;
          _.each(links, function(a, i){
            if($(a).attr('href') == '#variables/'+this.model.id+'/addValue'){
              var key = i+(e.shiftKey ? -1 : +1);
              if(key >= 0 && key < links.length)
                link = links[key];
            }
          }, this);
          link = $(link ? link : (e.shiftKey ? links.last() : links.first()));
          if(link.hasClass('addValue'))
            link.focus();
          Backbone.View.goTo(link.attr('href').substring(1));
      }
    },

    addValue: function(){
      this.model.save({},{
        url: this.model.url()+'/addValue',
        success: _.bind(function(model, response){
          var next = model.masters.length - 1;
          Backbone.View.goTo('/variables/'+this.model.id+'_'+next+'/edit');
        }, this),
        error: _.bind(function(model, response){
          new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
        }, this)
      });
    },

    toggle: function(){
      this.ui.accordion.find('.collapse').collapse('toggle');
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id
      });
    },

    templateHelpers: {
      getMode: function(){
        switch(this.mode){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
