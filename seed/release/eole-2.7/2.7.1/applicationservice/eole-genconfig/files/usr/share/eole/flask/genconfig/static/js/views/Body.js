define(['marionette','vent','templates','views/Sidebar','views/Main','views/InitView'],
function (Marionette,vent,templates,Sidebar,Main,InitView){
  "use strict";

  return Marionette.Layout.extend({

    template : templates.body,
    tagName: 'div',
    className: 'row-fluid',

    regions : {
      sidebar : '#sidebar',
      main    : '#main-content'
    },

    ui : {
      sidebar : '#sidebar',
      main : '#main-content'
    },

    initialize : function() {
      this.bindTo(vent, 'route:showCategory', this.showCategory, this);
      this.bindTo(vent, 'route:init', this.init, this);
      this.bindTo(vent, 'flash', this.flash, this);
      this.bindTo(vent, 'toggleSidebar', this.toggleSidebar, this);
      this.sidebarOpened = true;
    },

    onRender : function() {
      this.sidebar.show(new Sidebar(this.options));
      this.sidebar.currentView.setElement(this.ui.sidebar);
    },

    init: function(callback){
      this.options.callback = callback;
      this.main.show(new InitView(this.options));
    },

    toggleSidebar: function(open){
      open = typeof open === 'undefined' ? this.sidebarOpened : open;
      if(open){
        this.ui.main.addClass('closed');
        this.ui.sidebar.addClass('closed');
        this.ui.sidebar.find('.sidebar-toggler').addClass('closed');
        this.ui.sidebar.find('.lead').hide();
      } else {
        this.ui.main.removeClass('closed');
        this.ui.sidebar.removeClass('closed');
        this.ui.sidebar.find('.sidebar-toggler').removeClass('closed');
        this.ui.sidebar.find('.lead').show();
      }
      this.sidebarOpened = !this.sidebarOpened;
    },

    showCategory: function(id, callback){
      this.options.categories.url = "/categories"
      if(!this.options.categories.length) return;
      var category = this.options.categories.get(id);
      if(typeof category === 'undefined'){
        category = this.options.categories.first();
        id = category.id;
        Backbone.View.goTo('/categories/'+id, callback);
        return;
      }
      category.tags.fetch({
        success: _.bind(function(collection, response){
          if(collection.length == 0)
            Backbone.View.goTo('categories/general');
        }, this),
        error: _.bind(function(collection, response){
          new ErrorView({ model: new Backbone.Model(response) }).render().$el.modal('show');
        }, this)
      });
      this.options.current_category = id;
      var options = _.extend({}, this.options, {
        model: category
      });
      this.main.show(new Main(options));
      if(_.isFunction(callback)) callback();
    },

    flash: function(msg, type, remove){
      var icon;
      switch(type){
        case 'success': icon = 'ok'; break;
        case 'info': icon = 'info-sign'; break;
        case 'warning':
        case 'error' : icon = 'warning-sign'; break;
        default: icon = 'info'; break;
      }
      var flash = $('<div>').addClass('alert alert-'+type).html("<i class='icon-"+icon+"'></i> "+msg);
      if (remove === true)
          $('.alert').remove();
      flash.append('<a class="close" data-dismiss="alert" href="#">&times;</a>');
      this.ui.main.prepend(flash); flash.alert();
    }
  });
});
