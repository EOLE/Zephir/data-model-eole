define(['vent','collections/Variables'], function (vent,Variables) {
  "use strict";

  return {
    index: function(){
       vent.trigger('route:index');
    },
    splash: function(){
        
    },
    showCategory: function(id){
      vent.trigger('route:editVariableCancel');
      vent.trigger('route:showCategory', id);
    },
    editVariable: function(id){
      vent.trigger('route:editVariable', id); 
    },
    setMode: function(id){
      vent.trigger('route:setMode', id);
    },
    addValue: function(id){
      vent.trigger('route:addValue', id);
    },
    validateConfig: function(){
    
    },
    saveConfig: function(){

    },
    discardConfig: function(){

    },
    downloadConfig: function(){

    },
    uploadConfig: function(){

    },
    setLocale: function(language){
      var locale = localStorage.getItem('locale');
      if(locale != language) {
        localStorage.setItem('locale', language);
        location.reload();
      }
    }
  };
});
