define(['vent','marionette','templates','views/VariableView'],
function(vent,Marionette,templates,VariableView){
  'use strict';

  return Marionette.Layout.extend({

    tagName: 'tr',
    className: '',
    template: templates.variableRowView,

    ui: {
      tooltip: '.tooltip_trigger',
      error: '.alert-error',
      warning: '.alert-warning',
      resetBtn : '.btn.resetgroup',
      link: 'a'
    },

    events: {
      'click .resetgroup' : 'resetValueGroup'
    },

    regions: {
      view: '.view',
    },

    initialize: function(){
      this.bindTo(this.model, 'error', this.error, this);
      this.bindTo(this.model, 'error:cancel', this.errorCancel, this);
    },

    onRender: function(){
      this.ui.tooltip.tooltip();
      this.ui.link.tooltip();
      this.ui.error.hide();
      if(typeof VariableView === 'undefined') VariableView = require('views/VariableView');
      this.view.show(new VariableView(_.extend(this.options, { model: this.model })));
    },

    error: function(msg){
      this.ui.error.show();
      this.ui.error.find('.msg').text(msg);
    },

    errorCancel: function(msg){
      this.ui.error.find('.msg').empty();
      this.ui.error.hide();
    },

    resetValueGroup: function(callback){
      this.ui.resetBtn.button('loading');
      this.model.save({groupid: this.model.id},{
        url: this.model.url()+'_0/reset',
        callback: _.isFunction(callback) ? _.bind(callback, this) : null,
        success: _.bind(function(model, response){
          if(this.options.isMandatoryCollection)
            //this.options.mandatories.resetAll(callback);
            vent.trigger('route:validateConfig', callback);
          else
            this.options.categories.fetch({
              success: _.bind(function(collection){
                this.sync = false;
                try {
                  this.model.get('tag').get('category').tags.fetch({ 
                    success: _.bind(function(tags){
                      var bkp_location = window.location.hash;
                      this.ui.resetBtn.button('reset');
                      if(tags.length == 0)
                        Backbone.View.goTo('categories/general');
                      else {
                        Backbone.View.goTo('/variables/'+this.model.id+'/editCancel');
                        if(_.isFunction(callback)) callback();
                        else Backbone.View.goTo(bkp_location);
                      }
                    }, this),
                    error: _.bind(function(collection, response){
                      this.model.trigger('error', response.responseText);
                    }, this)
                  });
                } catch(e) {
                  vent.trigger('route:reloadConfig', callback);
                }
              }, this)
            });
        }, this),
        error: _.bind(function(model, response){
          this.model.trigger('error', response.responseText);
          //this.error(response.responseText);
        }, this)
      });
    },


    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        breadcrumbs: this.options.breadcrumbs || false,
        debug: this.options.debug
      });
    },

    templateHelpers: {
      getMode: function(){
        switch(this.mode){
          case 'basic': return 'B'; break;
          case 'normal': return 'N'; break;
          case 'expert': return 'E'; break;
          default: return 'N';
        }
      }
    }
  });
});
