define(['marionette','vent','templates','collections/Categories','views/CategoryItemView'],
function(Marionette,vent,templates,Configs,CategoryItemView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName:   'div',
    className: '',
    template: templates.sidebar,
    itemView: CategoryItemView,
    itemViewContainer: 'ul',

    ui: {
      nav: 'ul'
    },

    events: {
      'click .sidebar-toggler': 'toggleSidebar',
      'click .sidebar-menu a': 'closeDropdown'
    },

    initialize: function(){
      this.collection = this.options.categories;
      this.bindTo(this.collection, 'reset add', this.render, this);
      this.bindTo(vent, 'route:showCategory', this.showCategory, this);
    },

    onRender: function () {

    },

    toggleSidebar: function(){
      vent.trigger('toggleSidebar');
    },

    closeDropdown: function(){
      if(this.$el.hasClass('in')){ console.log(this.$el); this.$el.collapse('hide'); }
    },

    showCategory: function(id){
      this.ui.nav.children(':not(.nav-header)').removeClass('active');
      this.$('a[href$="'+window.location.hash+'"]').parent().addClass('active');
    },

    serializeData: function(){
      return _.extend(Marionette.CompositeView.prototype.serializeData.call(this), {
        config: app.config
      });
    }
  });
});
