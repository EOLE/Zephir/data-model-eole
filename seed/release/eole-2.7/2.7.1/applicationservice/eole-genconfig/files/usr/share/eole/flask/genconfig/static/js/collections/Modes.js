define(['vent','marionette','models/Mode'],
function(vent,marionette,Mode){
  'use strict';

  return Backbone.Collection.extend({

    model: Mode,

    initialize: function(models, options){
      this.options = options || {};
    },

    url: function(models){
      return "/modes";
    },

    comparator: function (mode) {
      return mode.get('level');
    },

    current: function(){
      return this.find(function(mode){
        return mode.get('current');
      });
    }
  });
});
