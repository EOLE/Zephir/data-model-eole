define(['vent','marionette','templates'],
function(vent,Marionette,templates){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName   : 'div',
    className : 'row-fluid',
    template: templates.variableMultiView,

    ui: {
      input: '.multi',
      link: 'a'
    },

    initialize: function(){
      this.bindTo(this.model, 'change', this.render, this);
    },

    onRender: function(){
      this.ui.link.tooltip();
      this.ui.input.tooltip();
    },

    serializeData: function(){
      return _.extend(Marionette.View.prototype.serializeData.call(this), {
        id: this.model.id
      });
    }
  });
});
