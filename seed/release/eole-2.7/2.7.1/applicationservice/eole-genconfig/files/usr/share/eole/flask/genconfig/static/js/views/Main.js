define(['vent','marionette','templates','views/TagView','views/LoadingMessageView'],
function(vent,Marionette,templates,TagView,LoadingMessageView){
  'use strict';

  return Marionette.CompositeView.extend({

    tagName: 'div',
    className: 'container-fluid',
    template: templates.main,
    itemView: TagView,
    emptyView: LoadingMessageView,
    itemViewContainer: '.page',

    initialize: function(){
      //this.options.categories.url = "/categories"
      this.itemViewOptions = {
        categories: this.options.categories,
        debug: this.options.debug
      };
      this.scrollPos = 0;
      //this.loaded = false;
      this.bindTo(vent, 'saveScrollPos', this.saveScrollPos, this);
      //this.bindTo(this.model.tags, 'reset', function(){ this.loaded = true }, this);
      this.bindTo(this.model.tags, 'reset', this.render, this);
      this.collection = this.model.tags;
    },

    saveScrollPos: function(pos){
      this.scrollPos = pos;
    },

    render: function(){
      var r = Marionette.CompositeView.prototype.render.call(this);
      //if(this.loaded) alert('loaded');
      return r;
    },

    onRender: function(){
      window.scrollTo(0, this.scrollPos);
    }
 });
});
