define(function(require){
  "use strict";

  return {
    header                 : require('tpl!templates/header.tmpl'),
    body                   : require('tpl!templates/body.tmpl'),
    footer                 : require('tpl!templates/footer.tmpl'),
    sidebar                : require('tpl!templates/sidebar.tmpl'),
    main                   : require('tpl!templates/main.tmpl'),
    initView               : require('tpl!templates/initView.tmpl'),
    modesView              : require('tpl!templates/modesView.tmpl'),
    modeItemView           : require('tpl!templates/modeItemView.tmpl'),
    categoryItemView       : require('tpl!templates/categoryItemView.tmpl'),
    tagView                : require('tpl!templates/tagView.tmpl'),
    variableView           : require('tpl!templates/variableView.tmpl'),
    variableRowView        : require('tpl!templates/variableRowView.tmpl'),
    variableItemView       : require('tpl!templates/variableItemView.tmpl'),
    variableItemEditView   : require('tpl!templates/variableItemEditView.tmpl'),
    variableMultiView      : require('tpl!templates/variableMultiView.tmpl'),
    variableMultiEditView  : require('tpl!templates/variableMultiEditView.tmpl'),
    variableGroupView      : require('tpl!templates/variableGroupView.tmpl'),
    variableMasterView     : require('tpl!templates/variableMasterView.tmpl'),
    separatorView          : require('tpl!templates/separatorView.tmpl'),
    loadingMessageView     : require('tpl!templates/loadingMessageView.tmpl'),
    fillMandatoriesView    : require('tpl!templates/fillMandatoriesView.tmpl'),
    diffVariablesView      : require('tpl!templates/diffVariablesView.tmpl'),
    diffVariableView       : require('tpl!templates/diffVariableView.tmpl'),
    emptyDiffView          : require('tpl!templates/emptyDiffView.tmpl'),
    discardConfigView      : require('tpl!templates/discardConfigView.tmpl'),
    errorView              : require('tpl!templates/errorView.tmpl'),
    legend                 : require('tpl!templates/legend.tmpl')
  };
});

