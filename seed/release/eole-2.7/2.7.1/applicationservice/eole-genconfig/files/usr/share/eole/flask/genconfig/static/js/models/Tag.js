define(['vent','marionette','templates','collections/Variables','helpers/util'],
function(vent,Marionette,templates,Variables){
  'use strict';

  return Backbone.Model.extend({

    defaults : {
      id: 'Configuration',
      help : "Click to configure variables in this category" // InfoBulle
    },

    initialize: function(){
      this.variables = new Variables([], { categoryid: this.id });
      this.on('change:variables', function(){ this.variables.reset(this.get('variables')); }, this);
      this.variables.on('reset',this.adoptAll, this);
      this.variables.on('add',this.adoptOne, this);
      this.variables.reset(this.get('variables'));
    },

    adoptAll: function() { 
      this.variables.each(this.adoptOne, this);
    },

    adoptOne: function(variable) {
      variable.set('tag', this);
      variable.set('tagid', this.id);
    },

    toJSON: function(){
      if(this._isSerializing) return this.id || this.cid;
      this._isSerializing = true;
      var json = _.extend(_.clone(this.attributes), {
       // variables: this.variables.toJSON(),
        category: this.get('category').toJSON()
      });
      this._isSerializing = false;
      return json;
    }
  });
});
