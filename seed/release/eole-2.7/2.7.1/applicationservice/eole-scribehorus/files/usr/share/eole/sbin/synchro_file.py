#! /usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################################
#
# Eole NG - 2010
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
# eole@ac-dijon.fr
#
###########################################################################
"""
Génération des partages et du fichier smb.conf à partir de l'annuaire
"""
import sys
try:
    # Scribe
    from scribe.eoleshare import Share
    share = Share()
except ImportError:
    # Horus
    from horus import backend as share

if 'norestart' in sys.argv:
    share.synchronize(False)
else:
    share.synchronize(True)
