#!/bin/bash

# Génération des fichiers scripts d'ouverture de session pour les lecteurs réseaux

. /usr/lib/eole/eolead.sh
if [ -f /usr/share/eole/backend/creation-prof.py ]
then
    # Ajout des lecteurs réservés sur Scribe
    SMB_NETBIOS_NAME=$(CreoleGet smb_netbios_name)
    AD_DOMAIN=$(CreoleGet ad_domain)
    ALL_OS_SCRIPT_FILE="$CONTAINER_ROOTFS/home/sysvol/${AD_DOMAIN}/scripts/os/Vista.txt"
    grep -q "lecteur,R:,\\\\\\\\${SMB_NETBIOS_NAME}\\\\icones\\$" $ALL_OS_SCRIPT_FILE > /dev/null 2>&1
    [ $? -ne 0 ] && echo "lecteur,R:,\\\\${SMB_NETBIOS_NAME}\\icones$"  >> $ALL_OS_SCRIPT_FILE

    grep -q "lecteur,S:,\\\\\\\\${SMB_NETBIOS_NAME}\\\\groupes" $ALL_OS_SCRIPT_FILE > /dev/null 2>&1
    [ $? -ne 0 ] && echo "lecteur,S:,\\\\${SMB_NETBIOS_NAME}\\groupes" >> $ALL_OS_SCRIPT_FILE

    grep -q "lecteur,T:,\\\\\\\\${SMB_NETBIOS_NAME}\\\\commun" $ALL_OS_SCRIPT_FILE > /dev/null 2>&1
    [ $? -ne 0 ] && echo "lecteur,T:,\\\\${SMB_NETBIOS_NAME}\\commun" >> $ALL_OS_SCRIPT_FILE

    PROF_GROUP_SCRIPT_FILE="$CONTAINER_ROOTFS/home/sysvol/${AD_DOMAIN}/scripts/groups/professeurs.txt"
    grep -q "lecteur,P:,\\\\\\\\${SMB_NETBIOS_NAME}\\\\professeurs" $PROF_GROUP_SCRIPT_FILE > /dev/null 2<&1
    [ $? -ne 0 ] && echo "lecteur,P:,\\\\${SMB_NETBIOS_NAME}\\professeurs" >> $PROF_GROUP_SCRIPT_FILE
else
    # Ajout des lecteurs réservés sur Horus
    for grp in "minedu" "applidos";do
        # Add logon scripts for drives mapping on HorusAD
        . /usr/lib/eole/eolead.sh
        SMB_NETBIOS_NAME=$(CreoleGet smb_netbios_name)
        AD_DOMAIN=$(CreoleGet ad_domain)
        if [ "${grp}" == "applidos" ]
        then
            LECTEUR="F:"
        else
            LECTEUR="X:"
        fi
        SCRIPT_FILE="$CONTAINER_ROOTFS/home/sysvol/${AD_DOMAIN}/scripts/groups/${grp}.txt"
        grep -q "lecteur,${LECTEUR},\\\\\\\\${SMB_NETBIOS_NAME}\\\\${grp}" $SCRIPT_FILE > /dev/null 2>&1
        [ $? -ne 0 ] && echo "lecteur,${LECTEUR},\\\\${SMB_NETBIOS_NAME}\\${grp}"  >> $SCRIPT_FILE
    done
fi

exit 0
