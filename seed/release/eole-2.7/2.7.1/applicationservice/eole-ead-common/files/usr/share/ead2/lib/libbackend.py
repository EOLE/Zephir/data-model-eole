# -*- coding: UTF-8 -*-
import PAM


class PamAuth():
    """AUTHENTIFICATION PAM
    """
    def __init__(self, service='passwd'):
        self.service = service
        self.auth = PAM.pam()

    def pam_conv(self, auth, query_list, userData):
        """fonction de dialogue avec la lib_pam"""
        resp = []
        for i in range(len(query_list)):
            query, type = query_list[i]
            if type == PAM.PAM_PROMPT_ECHO_ON:
                val = raw_input(query)
                resp.append((val, 0))
            elif type == PAM.PAM_PROMPT_ECHO_OFF:
                resp.append((self.passwd, 0))
            elif type == PAM.PAM_PROMPT_ERROR_MSG or type == PAM.PAM_PROMPT_TEXT_INFO:
                print query
                resp.append(('', 0))
            else:
                return None
        return resp

    def authenticate(self, user, passwd):
        self.auth.start(self.service)
        self.passwd = passwd
        if user != None:
            self.auth.set_item(PAM.PAM_USER, user)
        self.auth.set_item(PAM.PAM_CONV, self.pam_conv)
        try:
            self.auth.authenticate()
            self.auth.acct_mgmt()
            self.passwd = None
        except PAM.error, resp:
            return False
        except:
            print 'Internal error'
            return False
        else:
            return True

from ConfigParser import ConfigParser

class PasswordParser(ConfigParser):
    """
    AUTHENTIFICATION PAR FICHIER DE MOTS DE PASSE
    Utilisé pour parser un fichier passwd.ini
     
    [password]
    user1=password1

    """

    
    def parse_file(self, filename):
        """
        charge les login/mdp depuis un fichier
        """
        self.read(filename)
    
    def get_passwd_dict(self):
        """
        renvoi les couples login/mdp cryptes
        """
        self._passwd = {}
        for login, passwd in self.items('password'):
            self._passwd[login]=passwd

        return self._passwd

    def write_file(self, filename, dict):
        """
        ecrit les valeurs dans le fichier de configuration
        """
        for login, passwd in dict.items():
            self.set('password',login, passwd)
        try:
            fd = file(filename,'w')
            self.write(fd)
            fd.close()
        except:
            return False

        return True


def clean_color_codes(chaine):
    """
    enleve les caracteres de couleur de la chaine (spécifique aux terminaux)
    FIXME: rajouter des couleurs
    """
    win_new_line = chr(13)+chr(10)
    colors = [
    
    "[65G",
    "[1;31m",#rouge
    "[1;32m",#vert
    "[1;33m",#jaune
    "[1;34m",
    "[1;35m",
    "[1;36m",
    "[1;37m",#blanc
    "[0;39m",#retour normal?
    "E", #retour ligne?
    "[70G",
    "[60G",
    "[",  #a garder en dernier pour dernier recourt (on gardera peut etre des caracteres en trop mais ca passera)
    ""  #a garder en dernier pour dernier recourt (on gardera peut etre des caracteres en trop mais ca passera)
    ]
    #pour chaque code de couleur, on le remplace par une chaine vide
    for col in colors:
        chaine = chaine.replace(col,'')
    chaine=chaine.replace(win_new_line,'\n')
    return chaine
