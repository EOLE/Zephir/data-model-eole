# -*- coding: UTF-8 -*-
"""

internationalisation ead 2

"""

import gettext
from ead2.backend.config.config import I18N_DIR, APP_NAME
from os import listdir

LANGUAGE_PACK = {}
languages = [d for d in listdir(I18N_DIR) if d != '.svn']
languages.append('us')
for lang in languages:
    LANGUAGE_PACK[lang] = gettext.translation(
                          domain=APP_NAME,
                          localedir=I18N_DIR,
                          languages=[lang],
                          fallback=True,
                         )

gettext.bindtextdomain (APP_NAME, I18N_DIR)
gettext.textdomain (APP_NAME)
def _(msg, language=None):
    if language == None or not LANGUAGE_PACK.has_key(language):
        return gettext.gettext(msg)
    else:
        return LANGUAGE_PACK[language].gettext(msg)

        
    

#def enable_i18n():
#    gettext.bindtextdomain (APP_NAME, I18N_DIR)
#    gettext.textdomain (APP_NAME)
#    # installons la fonction _() dans les builtins
#    #gettext.install (APP_NAME, I18N_DIR, unicode=1)
#    gettext.install (APP_NAME, I18N_DIR)
#    # si _() n'est pas présent, on la remplace par str
#    if not '_' in globals().keys():
#        globals()['_'] = str

# on lance la commande 
#enable_i18n()


