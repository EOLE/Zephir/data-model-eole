# -*- coding: UTF-8 -*-
"""

internationalisation ead 2

"""

import gettext

from ead2.frontend.config.config import I18N_DIR, APP_NAME


def enable_i18n():
    gettext.bindtextdomain (APP_NAME, I18N_DIR)
    gettext.textdomain (APP_NAME)
    # installons la fonction _() dans les builtins
    gettext.install (APP_NAME, I18N_DIR, unicode=1)
    # si _() n'est pas présent, on la remplace par str
    if not '_' in globals().keys():
        globals()['_'] = str


# on lance la commande 
enable_i18n()
