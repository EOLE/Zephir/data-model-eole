# -*- coding: UTF-8 -*-
import xmlrpclib
from OpenSSL import SSL, crypto
from OpenSSL import crypto
from os.path import join, dirname
from ead2.config.config import cert_file, key_file


# transport sécurisé utilisant un certificat
class TransportEole(xmlrpclib.SafeTransport):

    def make_connection(self, host):
        # create a HTTPS connection object from a host descriptor
        # host may be a string, or a (host, x509-dict) tuple
        import httplib
        localhost, extra_headers, x509 = self.get_host_info(host)
        try:
            HTTPS = httplib.HTTPS
        except AttributeError:
            raise NotImplementedError(
                "your version of httplib doesn't support HTTPS"
                )
        else:
            cx = HTTPS(localhost, None,
                       key_file = key_file,
                       cert_file = cert_file)
            return cx


#################################################
## Factory permettant de créer un contexte SSL ##
#################################################
class ServerContextFactory:

    def __init__(self):
        """
            load the key and cert files in a PKey Object
        """
        self.cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file).read())
        self.key = crypto.load_privatekey(crypto.FILETYPE_PEM, open(key_file).read())

    def getContext(self):
        """
            Create an SSL context.
            Load cert and key files in a context
        """
        ctx = SSL.Context(SSL.TLSv1_METHOD)
        ctx.use_certificate(self.cert)
        ctx.use_privatekey(self.key)
        return ctx
