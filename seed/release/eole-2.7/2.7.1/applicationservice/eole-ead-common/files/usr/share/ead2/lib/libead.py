# -*- coding: UTF-8 -*-
"""
utilitaires ead
"""
from ConfigParser import ConfigParser

class EadKeyParser(ConfigParser):
    """
    Utilisé pour parser un fichier de type key.ini
    (fichier qui definit les clés/adresses ip autorisées)
    """

    def parse_file(self, filename):
        """
        charge les ips/clés depuis un fichier
        """
        self.read(filename)

    def get_key_dict(self):
        """
        renvoi les couples ip/clé
        """
        self._keys = {}
        for ip, key in self.items('keys'):
            self._keys[ip] = key

        return self._keys

    def write_file(self, filename, dict):
        """
        ecrit les valeurs dans le fichier de configuration
        """
        for option in self.options('keys'):
            # on supprime tout
            self.remove_option('keys', option)
        for ip, key in dict.items():
            # on rentre le dico
            self.set('keys', ip, key)
        try:
            # on enregistre
            fd = file(filename, 'w')
            self.write(fd)
            fd.close()
        except:
            return False

        return True

def uni(ch):
    if not isinstance(ch, unicode):
        try:
            return ch.decode('utf-8')
        except:
            try:
                a = ch.decode('iso-8859-1')
                a.encode('utf-8')
                return a
            except:
                return ch
    else:
        return ch

def encode_str(string):
    """ encode une string ou un unicode en utf8 et vérifie la présence de caractères interdits
    """
    if contains_bad_chars(string, chars=["<", ">", '"']):
        raise Exception, "Erreur : caractères interdits dans certains champs de saisie (<, >, \\\" et ')."
    try:
        string = string.encode('utf8')
    except:
        pass
    return string

def contains_bad_chars(content, chars=["<", ">", "'", '"']):
    """ prévient si une chaine contient certains caractères
    """
    for char in chars:
        if char in content:
            return True
    return False

def parcour_list(_list, str_fct=encode_str):
    """
        parcour une liste afin d'appliquer la fonction str_fct
        à toutes les strings présentes
    """
    encoded_list = []
    for element in _list:
        if type(element) == str:
            encoded_list.append(str_fct(element))
        elif type(element) == dict:
            encoded_list.append(parcour_dico(element, str_fct))
        elif type(element) == list:
            encoded_list.append(parcour_list(element, str_fct))
        elif type(element) == unicode:
            encoded_list.append(str_fct(element))
    return encoded_list

def test_bad_chars(content_string):
    return content_string == filter_bad_chars(content_string, chars=[" ", "'", '"'])

def parcour_dico(dico, str_fct=encode_str):
    """
        parcour un dico afin d'appliquer la fonction str_fct
        à toutes les strings présentes
    """
    for key in dico.keys():
        if type(dico[key]) in (str, unicode):
            if key == 'value' and dico.get('name', "vide") in ('password', 'repassword', 'old_password', 'input_password'):
                if contains_bad_chars(dico[key], chars=[" ", "'", '"']):
                    raise Exception, "Erreur : caractères interdits dans le mot de passe."
            # FIX pour #10324
            elif key == 'value' and dico.get('name', "vide") in ('forname', 'name', 'displayName', 'addsite'):
                if contains_bad_chars(dico[key], chars=["<", ">", '"']):
                    raise Exception, "Erreur : caractères interdits dans certains champs de saisie (<, > et \\\")."
                try:
                    # retour de l'encodage #10741
                    dico[key] = dico[key].encode('utf8')
                except:
                    pass
            else:
                dico[key] = str_fct(dico[key])
        elif type(dico[key]) == dict:
            dico[key] = parcour_dico(dico[key], str_fct)
        elif type(dico[key]) == list:
            dico[key] = parcour_list(dico[key], str_fct)
    return dico
