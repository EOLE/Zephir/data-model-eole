#! /usr/bin/env python2
#-*- encoding: utf-8 -*-
import sys
from os.path import isdir, isfile, join
from os import makedirs, listdir, rename, unlink
from shutil import rmtree
from zipfile import ZipFile
from json import dumps
import traceback
from pyeole.lock import acquire, release

from eoleaaf.parseaaf import main as complet_main
from eoleaaf.parsedelta import main as delta_main
from eoleaaf.config import path_aaf_complet
from eoleaaf.util import db
from logging import getLogger

aaflog = getLogger("AAF")

def processfile(filename, level, delete=False):
    """
    :param filename: fullpath of the archive to unzip
    :param level: type of import ('complète' or 'delta')
    :param delete: if True, remove the import file after successfull process
    """
    aaflog.info(20*'*')
    aaflog.info('*** Début du traitement des fichiers AAF (importation {1}) : {0}'.format(filename, level))
    if level == 'complète':
        main = complet_main
    elif level == 'delta':
        main = delta_main
    else:
        sys.exit(1)
    try:
        acquire('importaaf', level="system")
    except Exception as err:
        sys.exit(2)

    # the file must exists
    if not isfile(filename):
        release('importaaf', level='system')
        sys.exit(3)

    try:
        kwargs = {}
        if db.db.user.find_one():
            if level == 'complète':
                kwargs['maj_mode'] = True
        else:
            if level == 'delta':
                release('importaaf', level='system')
                sys.exit(4)
            if level == 'complète':
                kwargs['maj_mode'] = False
    except Exception as err:
        traceback.print_exc()
        release('importaaf', level='system')
        sys.exit(5)

    # purge the folders
    try:
        if isdir(path_aaf_complet):
            rmtree(path_aaf_complet)
        makedirs(path_aaf_complet)
    except Exception as err:
        traceback.print_exc()
        release('importaaf', level='system')
        sys.exit(6)

    # unzip the files
    try:
        zip_ref = ZipFile(filename, 'r')
        zip_ref.extractall(path_aaf_complet)
        zip_ref.close()
        # move the files in path_aaf_complet if there is any subfolder
        for fname in listdir(path_aaf_complet):
            fullname = join(path_aaf_complet, fname)
            if isdir(fullname):
                for subfname in listdir(fullname):
                    rename(join(fullname, subfname), join(path_aaf_complet, subfname))
    except Exception as err:
        traceback.print_exc()
        release('importaaf', level='system')
        sys.exit(7)

    try:
        # imports the files in mongodb
        main(path_aaf_complet, **kwargs)
    except Exception as err:
        traceback.print_exc()
        release('importaaf', level='system')
        sys.exit(8)

    # Delete AAF zip file if success
    if delete:
        unlink(filename)
        aaflog.info('Suppression du fichier {0}'.format(filename))

    aaflog.info('*** Fin du traitement des fichiers AAF')
    aaflog.info(20*'*')
    release('importaaf', level='system')
    sys.exit(0)

if __name__ == "__main__":
    processfile(*sys.argv[1:])