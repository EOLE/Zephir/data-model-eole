@echo off
echo.

%if %%mode_conteneur_actif == 'non'
set ip-scribe=%%adresse_ip_eth0
%else
set ip-scribe=%%adresse_ip_fichier_link
%end if
set settings-file=\\\%ip-scribe%\wpkg\settings.xml
set wpkginst=%PROGRAMFILES%\WPKG\wpkginst.exe

IF NOT EXIST "%wpkginst%" goto ErrClient

IF NOT EXIST "%settings-file%" goto ErrSettings
goto SuiteClient

:ErrClient
echo ERREUR : "%wpkginst%" introuvable
echo Le client WPKG n'est probablement pas installe
echo Mise a jour impossible !
ping -n 10 127.0.0.1 > nul
goto Fin

:ErrSettings
echo ERREUR : "%settings-file%" introuvable
echo Mise a jour impossible !
ping -n 10 127.0.0.1 > nul
goto Fin

:SuiteClient
"%wpkginst%" --SETTINGSFILE=%settings-file%%

echo ***************************************************************************
echo Mise a jour depuis "%settings-file%" terminee.
echo ***************************************************************************
ping -n 2 127.0.0.1 > nul

:Fin
echo.
