#-*-coding:utf-8-*-

FILENAMES = ['/usr/share/eole/mysql/eole-aaf/updates/eoleaaf-update-1.sql']
# existance du champ ENTEleveINE
QUERY = "select * from information_schema.columns where column_name='ENTEleveINE' and table_name='eleve'"

def test_response(result):
    """
    """
    return result == []

conf_dict = dict(db='eoleaaf',
                 condition_query=QUERY,
                 expected_response=test_response,
                 filenames=FILENAMES,
                 version='ENTEleveINE')

