########################################################
#
## dhcpd.conf pour ScribeNg
#
## Equipe Eole eole@ac-dijon.fr
#
## septembre 2007
#
########################################################
#
%def fallback_option(%%struct, %%option, %%fallback_option, %%fallback)
  %if getattr(%%struct, %%option, None) != None
    %return getattr(%%struct, %%option)
  %elif %%getVar(%%fallback_option, None) != None
    %return %%getVar(%%fallback_option)
  %else
    %return %%fallback
  %end if
%end def
%set %%ead3 = %%creole_client.get('dhcpactivation.dhcp_activation.dhcp_activation_ead3', 'non')
%set %%options = ['adresse_netmask_dhcp', 'ip_basse_dhcp', 'ip_haute_dhcp', 'adresse_ip_gw_dhcp', 'interdire_hotes_inconnus', 'adressage_statique', 'dhcp_pool_use_failover', 'fichier_pxe_dhcp']
%set %%overloaded_options = ['nom_domaine_dhcp', 'adresse_ip_dns_dhcp', 'adresse_ip_dns_secondaire_dhcp', 'adresse_ip_wins_primaire_dhcp', 'adresse_ip_wins_secondaire_dhcp', 'adresse_ip_ntp_dhcp', 'dhcp_lease_default', 'dhcp_lease_max', 'domain_wpad_dhcp']
%set %%groups = {}
%set %%pools = {}
%set %%subnets = {}
%for %%range_def in %%adresse_network_dhcp
%silent %%subnets.setdefault((%%str(%%range_def), %%range_def.adresse_netmask_dhcp), [])
%silent %%subnets[(%%str(%%range_def), %%range_def.adresse_netmask_dhcp)].append(%%range_def.nom_plage_dhcp)
%if %%range_def.adressage_statique == 'oui'
%set %%groups[%%range_def.nom_plage_dhcp] = {}
%for %%option in %%options
%if %%hasattr(%%range_def, %%option)
%set %%groups[%%range_def.nom_plage_dhcp][%%option] = %%getattr(%%range_def, %%option)
%end if
%end for
%for %%option in %%overloaded_options
%if %%hasattr(%%range_def, %%option)
%set %%groups[%%range_def.nom_plage_dhcp][%%option] =  %%fallback_option(%%range_def, %%option, 'global_{}'.format(%%option), None)
%end if
%end for
%else
%set %%pools[%%range_def.nom_plage_dhcp] = {}
%for %%option in %%options
%if %%hasattr(%%range_def, %%option)
%set %%pools[%%range_def.nom_plage_dhcp][%%option] = %%getattr(%%range_def, %%option)
%end if
%end for
%for %%option in %%overloaded_options
%if %%hasattr(%%range_def, %%option)
%set %%pools[%%range_def.nom_plage_dhcp][%%option] =  %%fallback_option(%%range_def, %%option, 'global_{}'.format(%%option), None)
%end if
%end for
%end if
%end for
%if not %%is_empty(%%global_dhcp_lease_default)
default-lease-time %%global_dhcp_lease_default;
%end if
%if not %%is_empty(%%global_dhcp_lease_max)
max-lease-time %%global_dhcp_lease_max;
%end if
%if not %%is_empty(%%global_nom_domaine_dhcp)
option domain-name "%%global_nom_domaine_dhcp";
%end if
%if not %%is_empty(%%global_adresse_ip_dns_dhcp)
%set %%dns_ip = %%global_adresse_ip_dns_dhcp
%if not %%is_empty(%%global_adresse_ip_dns_secondaire_dhcp)
%set %%dns_ip = %%dns_ip + ', ' + %%global_adresse_ip_dns_secondaire_dhcp
%end if
option domain-name-servers %%dns_ip;
%end if
%if not %%is_empty(%%global_adresse_ip_wins_primaire_dhcp)
%set %%netbios_ip = %%global_adresse_ip_wins_primaire_dhcp
%if not %%is_empty(%%global_adresse_ip_wins_secondaire_dhcp)
%set %%netbios_ip = %%netbios_ip + ', ' + %%global_adresse_ip_wins_secondaire_dhcp
%end if
option netbios-name-servers %%netbios_ip;
option netbios-dd-server %%netbios_ip;
option netbios-node-type 8;
%end if
%if not %%is_empty(%%global_adresse_ip_ntp_dhcp)
option ntp-servers %%global_adresse_ip_ntp_dhcp;
%end if
%if not %%is_empty(%%global_domain_wpad_dhcp)
option wpad-url code 252 = text;
option wpad-url "http://wpad.%%global_domain_wpad_dhcp/wpad.dat\n";
%end if

%if %%getVar('activer_dhcp_failover', 'non') == 'oui'
#failover
failover peer "%%{dhcp_failover_name}" {
    %%{dhcp_failover_rank};
    address %%{dhcp_failover_local_address};
    port %%{dhcp_failover_port};
    peer address %%{dhcp_failover_peer_address};
    peer port %%{dhcp_failover_peer_port};
    max-response-delay %%{dhcp_failover_response_delay};
    max-unacked-updates %%{dhcp_failover_unacked_updates};
    load balance max seconds %%{dhcp_failover_balance_seconds};
%if %%dhcp_failover_rank == 'primary'
    mclt %%{dhcp_failover_mclt};
    split %%{dhcp_failover_split};
%end if
}
%end if

#ddns-update-style none;
authoritative;

%if %%getVar('activer_omapi', 'non') == 'oui'
# omapi
omapi-port %%{omapi_port};
omapi-key omapi_key;

key omapi_key {
    algorithm hmac-md5;
    secret %%{omapi_secret};
}
%end if

# déclaration des hôtes des plages statiques
%if %%ead3 == 'non'
include "/etc/dhcp/fixed-address/fixed-addresses.txt";
%else
%set %%hosts = %%zip(%%creole_client.get('dhcp.dhcp.id_dhcp.macaddress'), %%creole_client.get('dhcp.dhcp.id_dhcp.hostname'), %%creole_client.get('dhcp.dhcp.id_dhcp.ip'), %%creole_client.get('dhcp.dhcp.id_dhcp.name'), %%creole_client.get('dhcp.dhcp.id_dhcp.dyn'))
%for %%group in %%groups
%set %%grouped_hosts = [h for h in %%hosts if h[3] == %%group]
%if %%grouped_hosts != []
group %%group {
  option subnet-mask %%groups[%%group].get('adresse_netmask_dhcp');
%if %%groups[%%group].get('adresse_ip_gw_dhcp') is not None
  option routers %%groups[%%group].get('adresse_ip_gw_dhcp');
%end if
%if %%groups[%%group].get('nom_domaine_dhcp') is not None
  option domain-name "%%groups[%%group].get('nom_domaine_dhcp')";
%end if
%if %%groups[%%group].get('adresse_ip_dns_dhcp') is not None
  option domain-name-servers %%groups[%%group].get('adresse_ip_dns_dhcp')%slurp
  %if %%groups[%%group].get('adresse_ip_dns_secondaire_dhcp') is not None
, %%groups[%%group].get('adresse_ip_dns_secondaire_dhcp')%slurp
  %end if
;
%end if
%if %%groups[%%group].get('adresse_ip_wins_primaire_dhcp') is not None
  option netbios-name-servers %%groups[%%group].get('adresse_ip_wins_primaire_dhcp')%slurp
  %if %%groups[%%group].get('adresse_ip_wins_secondaire_dhcp') is not None
, %%groups[%%group].get('adresse_ip_wins_secondaire_dhcp')%slurp
  %end if
;
  option netbios-dd-server %%groups[%%group].get('adresse_ip_wins_primaire_dhcp')%slurp
  %if %%groups[%%group].get('adresse_ip_wins_secondaire_dhcp') is not None
, %%groups[%%group].get('adresse_ip_wins_secondaire_dhcp')%slurp
  %end if
;
  option netbios-node-type 8;
%end if
%if %%groups[%%group].get('adresse_ip_ntp_dhcp') is not None
  option ntp-servers %%groups[%%group].get('adresse_ip_ntp_dhcp');
%end if
%if %%activer_tftp == 'oui' and %%groups[%%group].get('fichier_pxe_dhcp') is not None
  next-server %%adresse_ip_tftp;
  filename "%%groups[%%group].get('fichier_pxe_dhcp')";
%end if
%if %%groups[%%group].get('domain_wpad_dhcp') is not None
  option wpad-url "http://wpad.%%groups[%%group]['domain_wpad_dhcp']/wpad.dat\n";
%end if
%if %%groups[%%group].get('dhcp_lease_default') is not None
  default-lease-time %%groups[%%group].get('dhcp_lease_max');
%end if
%if %%groups[%%group].get('dhcp_lease_max') is not None
  max-lease-time %%groups[%%group].get('dhcp_lease_max');
%end if

%for %%host in %%grouped_hosts
  host %%host[1] { hardware ethernet %%host[0]; fixed-address %%host[2]; }
%end for
}
%end if
%end for

# déclaration des hôtes statiques hors plage statique
%for %%host in %%hosts
%if %%host[3] not in %%groups and not %%isinstance(%%host[2], dict)
host %%host[1] { hardware ethernet %%host[0]; fixed-address %%host[2]; }
%end if
%end for

# déclaration des hôtes autorisés dans les plages dynamiques
%for %%host in [h for h in %%hosts if h[4] == 'oui']
host %%host[1] { hardware ethernet %%host[0]; }
%end for

%end if

%if %%adresse_network_eth0 not in %%adresse_network_dhcp
# pas de dhcp sur la zone scribe
subnet %%adresse_network_eth0 netmask %%adresse_netmask_eth0 {
}
%end if

%for %%subnet in %%subnets
%for %%pool in %%subnets[%%subnet]
%if %%pools.get(%%pool) and %%pools[%%pool]['interdire_hotes_inconnus'] == 'oui' and %%pools[%%pool]['adressage_statique'] == 'non'
%if %%ead3 == 'non'
include "/etc/dhcp/host-classes/%%{pools[%%pool]['ip_basse_dhcp']}-%%{pools[%%pool]['ip_haute_dhcp']}.txt";
%else
class "pool-%%{pools[%%pool]['ip_basse_dhcp']}-%%{pools[%%pool]['ip_haute_dhcp']}" {
	match hardware;
}
%set %%filtered_hosts = [h for h in %%hosts if h[3] == %%pool]
%for %%filtered_host in %%filtered_hosts
subclass "pool-%%{pools[%%pool]['ip_basse_dhcp']}-%%{pools[%%pool]['ip_haute_dhcp']}" %%filtered_host[0];
subclass "pool-%%{pools[%%pool]['ip_basse_dhcp']}-%%{pools[%%pool]['ip_haute_dhcp']}" 1:%%filtered_host[0];
%end for
%end if
%end if
%end for
%end for

%for %%subnet in %%subnets
subnet %%subnet[0] netmask %%subnet[1] {
  option subnet-mask %%subnet[1];

    %set %%adresses_ip_gw = []
    %set %%noms_domaine = []
    %set %%adresses_ip_dns = []
    %set %%adresses_ip_wins = []
    %set %%adresses_ip_ntp = []
    %set %%domains_wpad = []
  %for %%pool in [p for p in %%subnets[%%subnet] if p in %%pools and %%pools[p]['adressage_statique'] == 'non']
    %set %%pool_adresse_ip_gw = %%pools[%%pool].get('adresse_ip_gw_dhcp')
	%if %%pool_adresse_ip_gw is not None and %%pool_adresse_ip_gw not in %%adresses_ip_gw
	     %silent %%adresses_ip_gw.append(%%pool_adresse_ip_gw)
	%end if
    %set %%pool_nom_domaine = %%pools[%%pool].get('nom_domaine_dhcp')
	%if %%pool_nom_domaine is not None and %%pool_nom_domaine not in %%noms_domaine
	     %silent %%noms_domaine.append(%%pool_nom_domaine)
	%end if
    %set %%pool_adresse_ip_dns = %%pools[%%pool].get('adresse_ip_dns_dhcp')
	%if %%pool_adresse_ip_dns is not None and %%pool_adresse_ip_dns not in %%adresses_ip_dns
	     %silent %%adresses_ip_dns.append(%%pool_adresse_ip_dns)
	%end if
    %set %%pool_adresse_ip_dns_secondaire = %%pools[%%pool].get('adresse_ip_dns_secondaire_dhcp')
	%if %%pool_adresse_ip_dns_secondaire is not None and %%pool_adresse_ip_dns_secondaire not in %%adresses_ip_dns
	     %silent %%adresses_ip_dns.append(%%pool_adresse_ip_dns_secondaire)
	%end if
    %set %%pool_adresse_ip_wins_primaire = %%pools[%%pool].get('adresse_ip_wins_primaire_dhcp')
	%if %%pool_adresse_ip_wins_primaire is not None and %%pool_adresse_ip_wins_primaire not in %%adresses_ip_wins
	     %silent %%adresses_ip_wins.append(%%pool_adresse_ip_wins_primaire)
	%end if
    %set %%pool_adresse_ip_wins_secondaire = %%pools[%%pool].get('adresse_ip_wins_secondaire_dhcp')
	%if %%pool_adresse_ip_wins_secondaire is not None and %%pool_adresse_ip_wins_secondaire not in %%adresses_ip_wins
	     %silent %%adresses_ip_wins.append(%%pool_adresse_ip_wins_secondaire)
	%end if
    %set %%pool_adresse_ip_ntp = %%pools[%%pool].get('adresse_ip_ntp_dhcp')
	%if %%pool_adresse_ip_ntp is not None and %%pool_adresse_ip_ntp not in %%adresses_ip_ntp
	     %silent %%adresses_ip_ntp.append(%%pool_adresse_ip_ntp)
	%end if
  %end for
  %if %%adresses_ip_gw
  option routers %%custom_join(%%adresses_ip_gw, ', ');
  %end if
  %if %%noms_domaine
  option domain-name "%%noms_domaine[0]";
  %end if
  %if %%adresses_ip_dns
  option domain-name-servers %%custom_join(%%adresses_ip_dns, ', ');
  %end if
  %if %%adresses_ip_wins
  option netbios-name-servers %%custom_join(%%adresses_ip_wins, ', ');
  option netbios-dd-server %%custom_join(%%adresses_ip_wins, ', ');
  option netbios-node-type 8;
  %end if
  %if %%adresses_ip_ntp
  option ntp-servers %%custom_join(%%adresses_ip_ntp, ', ');
  %end if
  %if %%activer_tftp == 'oui'
  next-server %%adresse_ip_tftp;
  filename "%%chemin_fichier_pxe";
  %end if

    # plages d'ip
  %for %%pool in [p for p in %%subnets[%%subnet] if p in %%pools and %%pools[p]['adressage_statique'] == 'non']
  pool {
%if %%pools[%%pool].get('adresse_ip_gw_dhcp') is not None
     option routers %%pools[%%pool].get('adresse_ip_gw_dhcp');
%end if
%if %%pools[%%pool].get('nom_domaine_dhcp') is not None
     option domain-name "%%pools[%%pool].get('nom_domaine_dhcp')";
%end if
%if %%pools[%%pool].get('adresse_ip_dns_dhcp') is not None
     option domain-name-servers %%pools[%%pool].get('adresse_ip_dns_dhcp')%slurp
  %if %%pools[%%pool].get('adresse_ip_dns_secondaire_dhcp') is not None
, %%pools[%%pool].get('adresse_ip_dns_secondaire_dhcp')%slurp
  %end if
;
%end if
%if %%pools[%%pool].get('adresse_ip_wins_primaire_dhcp') is not None
     option netbios-name-servers %%pools[%%pool].get('adresse_ip_wins_primaire_dhcp')%slurp
  %if %%pools[%%pool].get('adresse_ip_wins_secondaire_dhcp') is not None
, %%pools[%%pool].get('adresse_ip_wins_secondaire_dhcp')%slurp
  %end if
;
%end if
%if %%pools[%%pool].get('adresse_ip_ntp_dhcp') is not None
     option ntp-servers %%pools[%%pool].get('adresse_ip_ntp_dhcp');
%end if
%if %%pools[%%pool].get('domain_wpad_dhcp') is not None
     option wpad-url "http://wpad.%%pools[%%pool]['domain_wpad_dhcp']/wpad.dat\n";
%end if
 %if %%pools[%%pool]['dhcp_pool_use_failover'] == 'oui'
     failover peer "%%{dhcp_failover_name}";
 %end if
     range %%pools[%%pool]['ip_basse_dhcp'] %%pools[%%pool]['ip_haute_dhcp'];
 %if %%pools[%%pool]['interdire_hotes_inconnus'] == 'oui'
     allow members of "pool-%%{%%pools[%%pool]['ip_basse_dhcp']}-%%{%%pools[%%pool]['ip_haute_dhcp']}";
 %else
  %for %%restricted_class in [rp for rp in  %%pools if %%pools[rp]['interdire_hotes_inconnus'] == 'oui']
     deny members of "pool-%%{%%pools[%%restricted_class]['ip_basse_dhcp']}-%%{%%pools[%%restricted_class]['ip_haute_dhcp']}";
  %end for
 %end if
 %set %%lease_default = %%pools[%%pool]['dhcp_lease_default']
 %if not %%lease_default == None
     default-lease-time	%%lease_default;
 %end if
 %set %%lease_max = %%pools[%%pool]['dhcp_lease_max']
 %if not %%lease_max == None
     max-lease-time 	%%lease_max;
 %end if
 %if %%activer_tftp == 'oui' and %%pools[%%pool].get('fichier_pxe_dhcp') is not None
     next-server %%adresse_ip_tftp;
     filename "%%pools[%%pool].get('fichier_pxe_dhcp')";
 %end if

    }
%end for
}

%end for
