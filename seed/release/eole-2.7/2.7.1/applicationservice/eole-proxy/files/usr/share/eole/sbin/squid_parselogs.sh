#!/bin/bash

# Génération des statistiques de navigation d'une date
# Par défaut, génération des statistisques du jour
# Paramètres optionnels :
#   date sour la forme AAAAMMDD : 20160229 pour le 29 février 2016
#   --yesterday                 : statistiques de la veille
#   -h ou --help                : affiche l'aide
# exemples :
#  * squid_parselogs.sh             : génération des statistiques de la veille si 'lightsquid_auto'='oui'
#  * squid_parselogs.sh --yesterday : statistiques de la veille
#  * squid_parselogs.sh 20160101    : génération des statistiques du 1er janvier 2016

DATE=""
# parse arguments
for ARG in $@; do
    # Date forcée
    [[ "${ARG}" =~ ^20[0-9]{2}[0-1][0-9][0-3][0-9]$ ]] && DATE="${ARG}"
    # Date de la veille
    [[ "${ARG}" = "--yesterday" ]] && DATE=$(date -d "now 1 days ago" +%Y%m%d)
    # Affichage de l'aide
    if [[ "${ARG}" = "-h" ]] || [[ "${ARG}" = "--help" ]]; then
        echo """Génération des statistiques de navigation d'une date
 Par défaut, génération des statistisques du jour
 Paramètres optionnels :
   date sour la forme AAAAMMDD : 20160229 pour le 29 février 2016
   --yesterday                 : statistiques de la veille
   -h ou --help                : affiche l'aide
 exemples :
  * squid_parselogs.sh          : génération des statistiques de la veille
  * squid_parselogs.sh 20160101 : génération des statistiques du 1er janvier 2016"""
        exit 0
    fi
done

TMP_SQUID_LOG="/var/tmp/squid.log"

# Les journaux de connexions sur 1 jour sont dans 2 fichiers car logrotate à 6h25
if [[ "$DATE" = "" ]];then
    # Pas de paramètre, on prend les traces de navigation du jour
    DATE=$(date +%Y%m%d)
    SECOND_LOG_FILE=""
else
    # date forcée ou paramètre --yesterday
    SECOND_LOG_FILE_CMD="date -d \"$DATE 1 days\" +%Y%m%d"
    SECOND_LOG_FILE="-$(eval $SECOND_LOG_FILE_CMD)"
fi
# Paramètre ou non, le premier fichier est celui du jour de $DATE
FIRST_LOG_FILE="-${DATE}"

# Seul l'utilisateur générant les statistiques à accès au fichier temporaire
touch ${TMP_SQUID_LOG} && chmod 400 ${TMP_SQUID_LOG}

# Prise en compte de la première partie des journaux
[ -f /var/log/rsyslog/local/squid/squid1.info.log${FIRST_LOG_FILE}.lzma ] && \
    lzma -d -c /var/log/rsyslog/local/squid/squid1.info.log${FIRST_LOG_FILE}.lzma >> ${TMP_SQUID_LOG}
[ -f /var/log/rsyslog/local/squid/squid2.info.log${FIRST_LOG_FILE}.lzma ] && \
    lzma -d -c /var/log/rsyslog/local/squid/squid2.info.log${FIRST_LOG_FILE}.lzma >> ${TMP_SQUID_LOG}

# Prise en compte de la seconde partie des journaux
[ -f /var/log/rsyslog/local/squid/squid1.info.log${SECOND_LOG_FILE} ] && \
    cat /var/log/rsyslog/local/squid/squid1.info.log${SECOND_LOG_FILE} >> ${TMP_SQUID_LOG}
[ -f /var/log/rsyslog/local/squid/squid2.info.log${SECOND_LOG_FILE} ] && \
    cat /var/log/rsyslog/local/squid/squid2.info.log${SECOND_LOG_FILE} >> ${TMP_SQUID_LOG}

# Idem précédent, au cas où le fichier soit déjà compressé :
#   nécessaire quand on force la DATE, le fichier est déjà compressé.
[ -f /var/log/rsyslog/local/squid/squid1.info.log${SECOND_LOG_FILE}.lzma ] && \
    lzma -d -c /var/log/rsyslog/local/squid/squid1.info.log${SECOND_LOG_FILE}.lzma >> ${TMP_SQUID_LOG}
[ -f /var/log/rsyslog/local/squid/squid2.info.log${SECOND_LOG_FILE}.lzma ] && \
    lzma -d -c /var/log/rsyslog/local/squid/squid2.info.log${SECOND_LOG_FILE}.lzma >> ${TMP_SQUID_LOG}

# Génération des statistiques
[ -x /usr/share/lightsquid/lightparser.pl ] && \
    /usr/share/lightsquid/lightparser.pl ${DATE}
rm -rf ${TMP_SQUID_LOG}
