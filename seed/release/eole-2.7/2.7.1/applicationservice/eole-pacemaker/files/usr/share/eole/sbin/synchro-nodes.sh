#!/bin/bash

. /usr/lib/eole/ihm.sh

function synchro_files() {
# Synchronisation des fichiers
# Params :
# 1: "maitre"/"esclave"
# 2: nom_machine_locale
# 3: ip_noeud_local
# 4: nom_machine_distante
# 5: ip_noeud_distant

    local HA_PATH="/etc/ha.d/"
    local HA_INITIALIZED="${HA_PATH}.hdinitialized"
    local RSC_FILE="${HA_PATH}.rsc_list"
    tcpcheck 3 $5:22 | grep -q alive
    if [ $? -ne 0 ]
    then
        distant="maitre"
        [ "$1" = maitre ] && distant="esclave"
        EchoOrange "    ERREUR : Noeud $distant injoignable !"
        EchoOrange "             Relancer synchro-nodes.sh quand les 2 serveurs seront instanciés !!!"
        [ -e ${HA_INITIALIZED} ] && rm -f $HA_INITIALIZED
        echo
        exit 0
    fi
    if [ ! -e ${HA_INITIALIZED} ]
    then
        echo "Envoi de la clé RSA sur $4 --> $5"
        ssh-copy-id -i /root/.ssh/id_rsa.pub root@$5
        touch $HA_INITIALIZED
    fi

# On envoie la liste des resources service sur l'esclave
    if [ "$1" = "maitre" ]; then
        if [[ -f $RSC_FILE ]]; then
            /usr/bin/rsync -e ssh --owner --group --recursive --links --perms --times --delete $RSC_FILE $5:$HA_PATH
        else
            echo "Fichier $RSC_FILE inexistant !!!"
            echo "Lancez le script 'appliquer_hautedispo' pour le créer"
            exit 0
        fi
    fi

    # On ne synchronise que si le node est actif
    ACTIVE_NODE=1
    if [ -f $RSC_FILE ]; then
        ACTIVE_NODE=0
        while read sce_rsc; do
            RSC=$(echo $sce_rsc|cut -d" " -f2)
            FIND_PATTERN="^resource ${RSC} is running on: ${2} $"
            crm resource show ${RSC} | grep -qE "${FIND_PATTERN}"
            RC=$?
            [ $RC -ne 0 ] && ACTIVE_NODE=1 && break
        done < $RSC_FILE
    fi
    if [ $ACTIVE_NODE -eq 0 ]
    then
        for fichier in $(CreoleGet synchro_fichiers)
        do
            if [ -d $fichier ]
            then
                # Synchro d'un répertoire
                echo "Synchronisation du répertoire $fichier"
                repertoire=`dirname $fichier`/`basename $fichier`/
                /usr/bin/rsync -e ssh --owner --group --recursive --links --perms --times --delete $repertoire $5:$repertoire
            elif [ -f $fichier ]
            then
                # Synchro d'un fichier
                echo "Synchronisation du fichier $fichier"
                repertoire=`dirname $fichier`/
                /usr/bin/rsync -e ssh --owner --group --recursive --links --perms --times --delete $fichier $5:$repertoire
            else
                echo "Information : $fichier inexistant sur le nœud $1, non synchronisé"
            fi
        done
    fi
}

echo "Synchronisation des nodes ...."
echo

activer_haute_dispo="$(CreoleGet activer_haute_dispo)"
nom_machine_locale="$(CreoleGet nom_machine)"

if [ "$activer_haute_dispo" = "maitre" ]
then
    nom_machine_distante="$(CreoleGet nom_machine_esclave)"
    ip_machine_distante="$(CreoleGet ip_machine_esclave)"
fi

if [ "$activer_haute_dispo" = "esclave" ]
then
    nom_machine_distante="$(CreoleGet nom_machine_maitre)"
    ip_machine_distante="$(CreoleGet ip_machine_maitre)"
fi

if [ "$activer_haute_dispo" != "non" ]
then
    ip_machine_locale="$(CreoleGet "adresse_ip_eth$(CreoleGet corosync_dial_if)")"
    synchro_files $activer_haute_dispo $nom_machine_locale $ip_machine_locale $nom_machine_distante $ip_machine_distante
fi
