#
# configuration ldap pour Eole-2.3
#
# Equipe Eole <eole@ac-dijon.fr>
#

#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
include        /etc/ldap/schema/core.schema
include        /etc/ldap/schema/cosine.schema
include        /etc/ldap/schema/inetorgperson.schema
%if %%ldap_schema == 'eole'
include        /etc/ldap/schema/nis.schema
include        /etc/ldap/schema/eole/samba.schema
include        /etc/ldap/schema/eole/eole.schema
include        /etc/ldap/schema/eole/eoleshare.schema
include        /etc/ldap/schema/eole/radius.schema
include        /etc/ldap/schema/eole/ent.schema
%elif %%ldap_schema == 'sdet'
include        /etc/ldap/schema/eole/ent4.schema
%elif %%ldap_schema == 'zephir'
include        /etc/ldap/schema/openldap.schema
%end if

## Support du TLS
TLSCertificateFile      /etc/ldap/ssl/certs/openldap.crt
TLSCertificateKeyFile   /etc/ldap/ssl/private/openldap.key
TLSCACertificateFile    /etc/ssl/certs/ca.crt
TLSVerifyClient         never
TLSCipherSuite SECURE256:+SIGN-ALL:-VERS-SSL3.0:!AES-128-CBC:!3DES-CBC:!DES-CBC:!ARCFOUR-128:!ARCFOUR-40:!RC2-40:!CAMELLIA-128-CBC:!NULL

# Define global ACLs to disable default read access.

# Do not enable referrals until AFTER you have a working directory
# service AND an understanding of referrals.
#referral    ldap://root.openldap.org

pidfile         /var/run/slapd/slapd.pid
argsfile        /var/run/slapd/slapd.args

# Where the dynamically loaded modules are stored
modulepath      /usr/lib/ldap
moduleload      back_bdb
%if %%ldap_replication == 'oui' or %%ldap_replication_client == 'oui'
moduleload      syncprov
%end if

# Sample security restrictions
#    Require integrity protection (prevent hijacking)
#    Require 112-bit (3DES or better) encryption for updates
#    Require 63-bit encryption for simple bind
# security ssf=1 update_ssf=112 simple_bind=64

# Sample access control policy:
#    Root DSE: allow anyone to read it
#    Subschema (sub)entry DSE: allow anyone to read it
#    Other DSEs:
#        Allow self write access
#        Allow authenticated users read access
#        Allow anonymous users to authenticate
#    Directives needed to implement policy:
# access to dn.base="" by * read
# access to dn.base="cn=Subschema" by * read
# access to *
#    by self write
#    by users read
#    by anonymous auth
#
# if no access controls are present, the default policy
# allows anyone and everyone to read anything but restricts
# updates to rootdn.  (e.g., "access to * by * read")
#
# rootdn can always read and write EVERYTHING!

#######################################################################
# BDB database definitions
#######################################################################

# compatibilite EAD1 et appli PHP
allow   bind_v2

database    bdb
# The base of your directory
suffix        "%%ldap_base_dn"
rootdn        "cn=admin,%%ldap_base_dn"

# Cleartext passwords, especially for the rootdn, should
# be avoid.  See slappasswd(8) and slapd.conf(5) for details.
# Use of strong authentication encouraged.
rootpw {CRYPT}dyJWGdOe6Pgec

# The database directory MUST exist prior to running slapd AND
# should only be accessible by the slapd and slap tools.
# Mode 700 recommended.
directory    /var/lib/ldap

# Indices to maintain
index   objectClass         eq
index   uid,cn,sn           eq,subinitial
%if %%ldap_schema == 'eole'
index   ENTPersonLogin      eq
index   FederationKey       eq
index   ENTEleveStructRattachId eq
# Samba
index   memberuid            eq
index   gidnumber,uidnumber  eq
index   sambaSID             eq,sub
index   sambaPrimaryGroupSID eq
index   sambaDomainName      eq
index   sambaGroupType       eq
index   sambaSIDList         eq
index   uniqueMember         eq
index   sambaShareGroup      eq
# recherches diverses
index   displayName          eq,subinitial
index   description          eq,subinitial
index   type                 eq
index   eleve                eq
%elif %%ldap_schema == 'sdet'
index   ENTEleveMEF          eq
%end if
index   mail                 eq
# réplication
index   entryCSN,entryUUID   eq
index   contextCSN           eq

# Basic ACL
access to attrs=userPassword
        by self write
        by anonymous auth
%if %%getVar('activer_ad', 'non') == 'oui'
        by dn="cn=reader,%%ldap_base_dn" write
%else
        by dn="cn=reader,%%ldap_base_dn" read
%end if
        by * none

%if %%ldap_schema == 'eole'
access to attrs=sambaLMPassword,sambaNTPassword
        by self write
        by anonymous auth
%if %%getVar('activer_ad', 'non') == 'oui'
        by dn="cn=reader,%%ldap_base_dn" write
%else
        by dn="cn=reader,%%ldap_base_dn" read
%end if
        by * none

# ACL pour Scribe
access to attrs=employeeNumber,Ine,dateNaissance,ENTPersonDateNaissance,ENTPersonAdresse,ENTPersonCodePostal,ENTPersonVille,ENTPersonPays,telephoneNumber,mobile,homePhone,eleve,ENTAuxPersRelEleveEleve,ENTEleveStructRattachId
        by self read
        by dn="cn=reader,%%ldap_base_dn" read
        by * none
%end if

# ACL par défaut
access to *
%if %%ldap_restrict_access == 'tous'
        by * read
%else
 %if %%mode_conteneur_actif == 'oui'
        %set %%separator='%'
        by peername.ip=%%adresse_network_br0%%separator%%adresse_netmask_br0 read
 %end if
        by peername.ip=127.0.0.1 read
 %if %%ldap_restrict_access != 'aucun'
        by users read
 %end if
        by * none
%end if

%if %%ldap_replication == 'oui'
# recherches illimitées pour le reader
limits dn.exact="cn=reader,%%ldap_base_dn" size=unlimited time=unlimited
%end if

# logging
loglevel     %%ldap_loglevel

# Specify the maximum number of entries to return from a search operation
sizelimit    %%ldap_sizelimit

# Specify the maximum number of seconds (in  real  time)
# slapd  will  spend  answering  a  search request
timelimit    %%ldap_timelimit

# Controls whether slapd will automatically maintain
# the modifiersName, modifyTimestamp, creatorsName, and createTimestamp attributes for entries.
lastmod      on


#######################################################################
# These slapd.conf options apply to the bdb and hdb backend database
#######################################################################

# Specify the size in entries of the in-memory entry cache
cachesize    %%ldap_cachesize
idlcachesize   %%ldap_cachesize

%if %%is_defined('import_slapadd')
dbnosync
dirtyread
%end if

## dbcachesize est une specifique au backend ldbm => ne pas l'utiliser

%if %%ldap_replication == 'oui'
overlay syncprov
# création d'un checkpoint
# toutes les 100 opérations
# ou toutes les 10 minutes
syncprov-checkpoint 100 10
# nombre maximum de sessions à conserver
syncprov-sessionlog 100
%end if

%if %%ldap_replication_client == 'oui'
include /etc/ldap/replication.conf
%end if
