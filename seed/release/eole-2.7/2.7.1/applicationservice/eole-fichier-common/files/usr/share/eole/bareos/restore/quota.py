#!/usr/bin/env python
"""Module quota"""
from pyeole.process import system_code
from pyeole.bareosrestore import bareos_restore_one_file, exit_if_running_jobs
from fichier.quota import set_quota

quotafile = "/home/backup/samba/sauv_quota.txt"

def execute(option, opt_str, value, parser, jobid, test_jobs=True):
    """ldap helper"""
    if len(parser.rargs) > 0:
        option = parser.rargs[0]
        if option == 'pre':
            pre()
        elif option == 'post':
            post()
    else:
        if test_jobs:
            exit_if_running_jobs()
        job(jobid)

def pre():
    print "pre quota"

def post():
    print "post quota"
    fp = file(quotafile, 'r')
    started = False
    for ligne in fp.readlines():
        if not started:
            if ligne.startswith('------------'):
                started = True
            continue
        # cas plusieurs partitions
        if ligne.startswith('***'):
            started = False
            continue
        elts = ligne.strip().split()
        try:
            user  = elts[0]
            quota = elts[3]
        except:
            continue
        if quota != '0':
            set_quota(user, int(quota)/1024)
    fp.close()

def job(jobid):
    print "Restauration quota"
    bareos_restore_one_file(quotafile, jobid)

priority=40
