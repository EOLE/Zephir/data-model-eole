# -*- coding: utf-8 -*-
%set %%basedn = 'dc=' + %%custom_join(%%ad_domain.split('.'), ',dc=')

eole-workstation:

  ad:

    domain: %%ad_domain
    join_username: eole-workstation-manager
    join_password: %include '/etc/eole/private/eole-workstation-manager.password'

  veyon:

    ldap:

      server: addc.%%ad_domain
      port: 389
      base_dn: %%basedn
      username: %%smb_workgroup\eole-workstation-reader
      password: %include '/etc/eole/private/eole-workstation-reader.password'
