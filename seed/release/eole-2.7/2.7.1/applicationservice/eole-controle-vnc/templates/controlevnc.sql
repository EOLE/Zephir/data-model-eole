## Creation de la base Mysql pour ControleVNC

# Creation base de donnees
CREATE DATABASE controlevnc CHARACTER SET utf8;

# Creation utilisateur
GRANT select,insert,update,delete ON controlevnc.* TO controlevnc@%%adresse_ip_fichier IDENTIFIED BY 'sqpassword';

# Connexion a la base
\r controlevnc

# Creation de la table *log*
CREATE TABLE log (
    ip varchar(16) NOT NULL,
    netbios varchar(15) DEFAULT '',
    os varchar(8) DEFAULT '',
    up boolean DEFAULT False,
    user varchar(32) DEFAULT '',
    display_name varchar(128) DEFAULT '',
    sid varchar(50) DEFAULT '',
    prim_group varchar(32) DEFAULT '',
    mac varchar(18) DEFAULT '',
    PRIMARY KEY (`ip`, `user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="informations sur la machine et l'utilisateur";

CREATE TABLE `group` (
    ip varchar(16) NOT NULL,
    groupname varchar(32) DEFAULT '',
    PRIMARY KEY  (`ip`, `groupname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='groupes utilisateurs';


