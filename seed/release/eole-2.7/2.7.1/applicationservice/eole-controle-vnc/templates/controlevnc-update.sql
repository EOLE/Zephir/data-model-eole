# Connexion a la base
USE controlevnc;

DELIMITER $$

DROP PROCEDURE IF EXISTS drop_index_netbios_if_exists $$
CREATE PROCEDURE drop_index_netbios_if_exists()
BEGIN
 IF((SELECT COUNT(*) AS index_exists
    FROM information_schema.statistics
    WHERE TABLE_SCHEMA = DATABASE() and table_name = 'log' AND index_name = 'netbios') > 0) THEN
   SET @s = 'DROP INDEX `netbios` ON `log`';
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END $$
CALL drop_index_netbios_if_exists $$
DROP PROCEDURE IF EXISTS drop_index_netbios_if_exists $$
DELIMITER ;
