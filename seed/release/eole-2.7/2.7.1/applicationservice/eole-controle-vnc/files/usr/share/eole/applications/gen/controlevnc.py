#-*-coding:utf-8-*-
###########################################################################
# Eole NG - 2015
# Copyright Pole de Competence Eole  (Ministere Education - Academie Dijon)
# Licence CeCill  cf /root/LicenceEole.txt
# eole@ac-dijon.fr
#
# controlevnc.py
#
# Configuration de base de données mysql de controlevnc
#
###########################################################################
"""
    Config pour controlevnc
"""
from eolesql.db_test import db_exists, test_var

TABLEFILENAMES = ["/usr/share/eole/mysql/controlevnc/gen/controlevnc.sql"]

def test():
    """
        test l'existence de la base controlevnc
    """
    return not db_exists('controlevnc')

conf_dict = dict(filenames=TABLEFILENAMES,
                 test=test)

