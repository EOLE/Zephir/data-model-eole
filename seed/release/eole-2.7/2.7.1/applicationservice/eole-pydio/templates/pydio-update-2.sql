\r pydio
SET character_set_client = utf8;

-- Création des trigger
DROP TRIGGER IF EXISTS `mail_queue_go_to_sent`;
DELIMITER //
CREATE TRIGGER `mail_queue_go_to_sent` BEFORE DELETE ON `ajxp_mail_queue`
FOR EACH ROW INSERT INTO ajxp_mail_sent (recipient,url,date_event,notification_object,html) VALUES (old.recipient,old.url,old.date_event,old.notification_object,old.html)
//
DELIMITER ;
        

UPDATE ajxp_plugin_configs SET configs=0x613a383a7b733a31393a22414a58505f504c5547494e5f454e41424c4544223b623a303b733a31313a225550444154455f53495445223b733a32323a2268747470733a2f2f7079642e696f2f7570646174652f223b733a31343a2250524553455256455f46494c4553223b733a303a22223b733a31373a22454e41424c455f3332345f494d504f5254223b623a303b733a31303a2250524f58595f484f5354223b733a303a22223b733a31303a2250524f58595f55534552223b733a303a22223b733a31303a2250524f58595f50415353223b733a303a22223b733a31343a225550444154455f4348414e4e454c223b733a363a22737461626c65223b7d WHERE id="action.updater";
UPDATE ajxp_plugin_configs SET configs=0x613a323a7b733a31373a224150504c49434154494f4e5f5449544c45223b733a353a22507964696f223b733a31363a2244454641554c545f4c414e4755414745223b733a323a226672223b7d WHERE id="core.ajaxplorer";
UPDATE ajxp_plugin_configs SET configs=0x613a333a7b733a31383a224552524f525f544f5f4552524f525f4c4f47223b623a303b733a32303a22435553544f4d5f4845414445525f464f525f4950223b733a303a22223b733a32323a22554e495155455f504c5547494e5f494e5354414e4345223b613a353a7b733a31333a22696e7374616e63655f6e616d65223b733a383a226c6f672e74657874223b733a383a224c4f475f50415448223b733a32373a22414a58505f494e5354414c4c5f504154482f646174612f6c6f6773223b733a31333a224c4f475f46494c455f4e414d45223b733a32313a226c6f675f6461746528276d2d642d7927292e747874223b733a393a224c4f475f43484d4f44223b693a3737303b733a31383a2267726f75705f7377697463685f76616c7565223b733a383a226c6f672e74657874223b7d7d WHERE id="core.log";
UPDATE ajxp_plugin_configs SET configs=0x613a313a7b733a31383a22554e495155455f4d535f494e5354414e4345223b613a333a7b733a31333a22696e7374616e63655f6e616d65223b733a363a226d712e73716c223b733a31383a2267726f75705f7377697463685f76616c7565223b733a363a226d712e73716c223b733a31303a2253514c5f445249564552223b613a323a7b733a31313a22636f72655f647269766572223b733a343a22636f7265223b733a31383a2267726f75705f7377697463685f76616c7565223b733a343a22636f7265223b7d7d7d WHERE id="core.mq";
UPDATE ajxp_plugin_configs SET configs=0x613a323a7b733a31313a22555345525f4556454e5453223b623a303b733a32303a22554e495155455f464545445f494e5354414e4345223b613a333a7b733a31333a22696e7374616e63655f6e616d65223b733a383a22666565642e73716c223b733a31303a2253514c5f445249564552223b613a323a7b733a31313a22636f72655f647269766572223b733a343a22636f7265223b733a31383a2267726f75705f7377697463685f76616c7565223b733a343a22636f7265223b7d733a31383a2267726f75705f7377697463685f76616c7565223b733a383a22666565642e73716c223b7d7d WHERE id="core.notifications";
UPDATE ajxp_plugin_configs SET configs=0x613a313a7b733a32323a22435553544f4d5f57454c434f4d455f4d455353414745223b733a31363a2257656c636f6d6520746f20507964696f223b7d WHERE id="gui.ajax";


-- Ajout 2admin        
%if %%is_defined('activer_pydio') and %%activer_pydio == 'oui'
%if %%is_defined('activer_addadmin') and %%activer_addadmin == 'oui'
-- ------------------------------------------------------------------------------------------------------------------------------------------
-- Pydio = Création admin secondaire

DROP PROCEDURE IF EXISTS AddAdminPydio;
DELIMITER |
CREATE PROCEDURE AddAdminPydio(_adminuser VARCHAR(60))
BEGIN
	DECLARE ad_id INT DEFAULT 0;

-- Création de l'utilisateur si il n'existe pas dans la table des utilisateurs
	SELECT COUNT(`login`) INTO ad_id FROM `ajxp_users` WHERE `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_users` (`login`, `password`, `groupPath`) VALUES (_adminuser, 'sha256:1000:shewUQhP98z13qcyXERCxcWABSQsHS4P:FP546A/uSX1uKWdTQyB3e717PsX1e35Y', '/');
	END IF;

-- Création des préférences par défaut de l'utilisateur si elles n'éxitent pas (utilisateur inexistant avant ce script)
	SELECT COUNT(`login`) INTO ad_id FROM `ajxp_user_prefs` WHERE `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_user_prefs` (`login`, `name`, `val`) VALUES (_adminuser, 'AJXP_WEBDAV_DATA', '$phpserial$a:1:{s:3:\"HA1\";s:32:\"4d27cf16dd08c424f15d1f27e4504f25\";}');
	END IF;

-- Création des rôles associés de l'utilisateur si ils n'éxitent pas (utilisateur inexistant avant ce script)
	SELECT COUNT(`role_id`) INTO ad_id FROM `ajxp_roles` WHERE `role_id`=CONCAT('AJXP_USR_/', _adminuser) COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_roles` (`role_id`, `serial_role`) VALUES (CONCAT('AJXP_USR_/', _adminuser), CONCAT('O:9:\"AJXP_Role\":8:{s:12:\"\0*\0groupPath\";N;s:9:\"\0*\0roleId\";s:17:\"AJXP_USR_/', _adminuser, '\";s:7:\"\0*\0acls\";a:1:{s:9:\"ajxp_conf\";s:2:\"rw\";}s:13:\"\0*\0parameters\";a:0:{}s:10:\"\0*\0actions\";a:0:{}s:14:\"\0*\0autoApplies\";a:0:{}s:8:\"\0*\0masks\";a:0:{}s:14:\"\0*\0lastUpdated\";i:1491557801;}'));
	END IF;
-- Réglage des droits si inexistant et ajout de l'admin si besoin
	SELECT COUNT(`rid`) INTO ad_id FROM `ajxp_user_rights` WHERE `repo_uuid`='ajxp.roles' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_user_rights` (`login`, `repo_uuid`, `rights`)  VALUES (_adminuser, 'ajxp.roles', CONCAT('$phpserial$a:2:{s:9:\"ROOT_ROLE\";b:1;s:17:\"AJXP_USR_/', _adminuser, '\";b:1;}'));
	END IF;

	SELECT COUNT(`rid`) INTO ad_id FROM `ajxp_user_rights` WHERE `repo_uuid`='ajxp.roles.order' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_user_rights` (`login`, `repo_uuid`, `rights`)  VALUES (_adminuser, 'ajxp.roles.order', '$phpserial$a:1:{s:9:\"ROOT_ROLE\";i:2;}');
	END IF;

	SELECT COUNT(`rid`) INTO ad_id FROM `ajxp_user_rights` WHERE `repo_uuid`='ajxp.admin' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_user_rights` (`login`, `repo_uuid`, `rights`)  VALUES (_adminuser, 'ajxp.admin', '1');
	ELSE
		UPDATE `ajxp_user_rights` SET `rights`='1'  WHERE `repo_uuid`='ajxp.admin' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	END IF;

	SELECT COUNT(`rid`) INTO ad_id FROM `ajxp_user_rights` WHERE `repo_uuid`='ajxp.profile' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	IF ad_id = 0 THEN
		INSERT IGNORE INTO `ajxp_user_rights` (`login`, `repo_uuid`, `rights`)  VALUES (_adminuser, 'ajxp.profile', 'admin');
	ELSE
		UPDATE `ajxp_user_rights` SET `rights`='admin'  WHERE `repo_uuid`='ajxp.profile' AND `login`=_adminuser COLLATE utf8_unicode_ci;
	END IF;

END|
DELIMITER ;

CALL AddAdminPydio('%%uid_addadmin');
DROP PROCEDURE AddAdminPydio;

-- Pydio = fin procédure de création admin secondaire
-- ------------------------------------------------------------------------------------------------------------------------------------------
%end if
%end if

