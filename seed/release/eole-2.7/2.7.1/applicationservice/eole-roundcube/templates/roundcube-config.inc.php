<?php

/* Local configuration for Roundcube Webmail */
$config['enable_installer'] = false;

// PEAR database DSN for read/write operations
// format is db_provider://user:password@host/database 
// For examples see http://pear.php.net/manual/en/package.database.mdb2.intro-dsn.php
// currently supported db_providers: mysql, mysqli, pgsql, sqlite, mssql or sqlsrv
$dbpassword="changeme";
$config['dbpassword']					= $dbpassword;
$config['dbport']                       ="3306";
$config['dblogin']						= "roundcube";
$config['dbname'] 						= "roundcube";

%if %%getVar("roundcube_db_mode", 'non') == "externe"
$config['dbhost']                       ="%%roundcube_dbserver";
    %if %%getVar('roundcube_dbport', 'non') != "non"
$config['dbport']                       ="%%roundcube_dbport";
    %end if
%else if %%getVar("roundcube_db_mode", 'non') == "default"
   %set dbhost = %%getVar('edb_host', 'non')
   %if %%dbhost == 'non' and %%mode_conteneur_actif == 'oui':
$config['dbhost']                       ="%%adresse_ip_mysql";
   %else
       %if %%dbhost == 'non'
$config['dbhost']                       ="localhost";
       %else
$config['dbhost']                       ="%%edb_host";
       %end if
   %end if

   %if %%getVar('edb_dbport', 'non') != "non"
$config['dbport']                       ="%%edb_port";
    %end if
%else
$config['dbhost']                       ="%%adresse_ip_mysql";
%end if;

$rcmail_config['db_dsnw'] = 'mysql://'.$config['dblogin'].':'.$config['dbpassword'].'@'.$config['dbhost'].'/'.$config['dbname'];

// ----------------------------------
// IMAP
// ----------------------------------
// The mail host chosen to perform the log-in.
// Leave blank to show a textbox at login, give a list of hosts
// to display a pulldown menu or set one host as string.
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
// Supported replacement variables:
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %s - domain name after the '@' from e-mail address provided at login screen
// For example %n = mail.domain.tld, %t = domain.tld
// WARNING: After hostname change update of mail_host column in users table is
//          required to match old user data records with the new host.
%if %%roundcube_tls == 'non'
$config['default_host'] = '%%adresse_imap';
%else
$config['default_host'] = 'tls://%%adresse_imap';
%end if

// ----------------------------------
// SMTP
// ----------------------------------
// SMTP server host (for sending mails).
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
// If left blank, the PHP mail() function is used
// Supported replacement variables:
// %h - user's IMAP hostname
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %z - IMAP domain (IMAP hostname without the first part)
// For example %n = mail.domain.tld, %t = domain.tld
$config['smtp_server'] = 'tls://%h';

// SMTP port (default is 25; use 587 for STARTTLS or 465 for the
// deprecated SSL over SMTP (aka SMTPS))
$config['smtp_port'] = 25;

// SMTP username (if required) if you use %u as the username Roundcube
// will use the current username for login
$config['smtp_user'] = '%u';

// SMTP password (if required) if you use %p as the password Roundcube
// will use the current user's password for login
$config['smtp_pass'] = '%p';

// SMTP AUTH type (DIGEST-MD5, CRAM-MD5, LOGIN, PLAIN or empty to use
// best server supported one)
$config['smtp_auth_type'] = 'LOGIN';

%if %%getVar('cert_type', 'non') == 'autosigné'
// Don't check SMTP ssl certificate with autosigned certificates
$config['smtp_conn_options'] = array(
	'ssl'         => array(
		'verify_peer'      => false,
		'verify_peer_name' => false,
	),
);
%end if

// provide an URL where a user can get support for this Roundcube installation
// PLEASE DO NOT LINK TO THE ROUNDCUBE.NET WEBSITE HERE!
$config['support_url'] = '';

// use this folder to store log files (must be writeable for apache user)
// This is used by the 'file' log driver.
$config['log_dir'] = 'logs/';

// use this folder to store temp files (must be writeable for apache user)
$config['temp_dir'] = 'temp/';

// X-Frame-Options HTTP header value sent to prevent from Clickjacking.
// Possible values: sameorigin|deny. Set to false in order to disable sending them
$config['x_frame_options'] = false;

// this key is used to encrypt the users imap password which is stored
// in the session record (and the client cookie if remember password is enabled).
// please provide a string of exactly 24 chars.
$config['des_key'] = '1X2pn2KxUNipyL%XNPj+?u06';

// This domain will be used to form e-mail addresses of new users
// Specify an array with 'host' => 'domain' values to support multiple hosts
// Supported replacement variables:
// %h - user's IMAP hostname
// %n - http hostname ($_SERVER['SERVER_NAME'])
// %d - domain (http hostname without the first part)
// %z - IMAP domain (IMAP hostname without the first part)
// For example %n = mail.domain.tld, %t = domain.tld
$config['mail_domain'] = '%%domaine_messagerie_etab';

// Set identities access level:
// 0 - many identities with possibility to edit all params
// 1 - many identities with possibility to edit all params but not email address
// 2 - one identity with possibility to edit all params
// 3 - one identity with possibility to edit all params but not email address
// 4 - one identity with possibility to edit only signature
$config['identities_level'] = 3;

// ----------------------------------
// PLUGINS
// ----------------------------------
// List of active plugins (in plugins/ directory)
// List of active plugins (in plugins/ directory)
%if %%activer_sso != 'non'
$rcmail_config['plugins'] = array('cas_authentication');
%else
$rcmail_config['plugins'] = array();
%end if

array_push($rcmail_config['plugins'],'envole');

%if %%is_defined('activer_piwik') and %%activer_piwik == 'oui'
array_push($rcmail_config['plugins'],'envolepiwik');
%end if

// the default locale setting (leave empty for auto-detection)
// RFC1766 formatted language name like en_US, de_DE, de_CH, fr_FR, pt_BR
$config['language'] = 'fr_FR';

// give this choice of date formats to the user to select from
$config['date_formats'] = array('Y-m-d', 'd-m-Y', 'Y/m/d', 'm/d/Y', 'd/m/Y', 'd.m.Y', 'j.n.Y');

// store draft message is this mailbox
// leave blank if draft messages should not be stored
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['drafts_mbox'] = 'Brouillons';

// store sent message is this mailbox
// leave blank if sent messages should not be stored
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['sent_mbox'] = 'Envoyes';

// move messages to this folder when deleting them
// leave blank if they should be deleted directly
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['trash_mbox'] = 'Poubelle';

// display these folders separately in the mailbox list.
// these folders will also be displayed with localized names
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['default_folders'] = array('INBOX', 'Brouillons', 'Envoyes', 'Junk', 'Poubelle');

// Set the spell checking engine. 'googie' is the default. 'pspell' is also available,
// but requires the Pspell extensions. When using Nox Spell Server, also set 'googie' here.
$config['spellcheck_engine'] = 'pspell';

// In order to enable public ldap search, configure an array like the Verisign
// example further below. if you would like to test, simply uncomment the example.
// Array key must contain only safe characters, ie. a-zA-Z0-9_
%if %%is_defined('activer_client_ldap') and %%activer_client_ldap != 'non'
$config['ldap_public'] = array (
  'Local' => array (
    'name' => "%%libelle_etab",
    'hosts' =>  array (
      0 => '%%adresse_ip_ldap',
      ),
    'port' => 389,
    'use_tls' => false,
    'user_specific' => false,
    'base_dn' => '%%ldap_base_dn',
    'writable' => false,
    'LDAP_Object_Classes' =>  array (
      0 => 'top',
      1 => 'inetOrgPerson',
      ),
    'required_fields' =>  array (
      0 => 'cn',
      1 => 'sn',
      2 => '%%ldap_fill_mail',
      ),
    'LDAP_rdn' => '%%ldap_fill_mail',
    'ldap_version' => 3,
    'search_fields' => array (
      0 => '%%ldap_fill_mail',
      1 => 'cn',
      ),
    'name_field' => 'cn',
    'email_field' => '%%ldap_fill_mail',
    'surname_field' => 'sn',
    'firstname_field' => 'gn',
    'sort' => 'cn',
    'scope' => 'sub',
    'filter' => '(%%ldap_fill_mail=*)',
    'fuzzy_search' => true,
	  'fieldmap' => array(
		// Roundcube   => LDAP
    'name'         => 'cn',
    'middlename'   => 'sn',
		'surname'      => 'ENTPersonProfils',
		'firstname'    => 'gn',
		'jobtitle'     => 'ENTPersonProfils',
		'email'        => 'mail',
    'notes'        => 'description',
  	),      
  ),
);
%end if

// The way how contact names are displayed in the list.
// 0: prefix firstname middlename surname suffix (only if display name is not set)
// 1: firstname middlename surname
// 2: surname firstname middlename
// 3: surname, firstname middlename
$config['addressbook_name_listing'] = 1;

# Fixes: #6652, passage en double quote du libelle, car ce dernier peut contenir des '
// If you are going to use LDAP for individual address books, you will need to 
// set 'user_specific' to true and use the variables to generate the appropriate DNs to access it.
//
// The recommended directory structure for LDAP is to store all the address book entries
// under the users main entry, e.g.:
//
//  o=root
//   ou=people
//    uid=user@domain
//  mail=contact@contactdomain
//
// So the base_dn would be uid=%fu,ou=people,o=root
// The bind_dn would be the same as based_dn or some super user login.
/* 
 * example config for Verisign directory
 *
$rcmail_config['ldap_public']['Verisign'] = array(
  'name'          => 'Verisign.com',
  // Replacement variables supported in host names:
  // %h - user's IMAP hostname
  // %n - hostname ($_SERVER['SERVER_NAME'])
  // %t - hostname without the first part
  // %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
  // %z - IMAP domain (IMAP hostname without the first part)
  // For example %n = mail.domain.tld, %t = domain.tld
  'hosts'         => array('directory.verisign.com'),
  'port'          => 389,
  'use_tls'	      => false,
  'ldap_version'  => 3,       // using LDAPv3
  'network_timeout' => 10,    // The timeout (in seconds) for connect + bind arrempts. This is only supported in PHP >= 5.3.0 with OpenLDAP 2.x
  'user_specific' => false,   // If true the base_dn, bind_dn and bind_pass default to the user's IMAP login.
  // %fu - The full username provided, assumes the username is an email
  //       address, uses the username_domain value if not an email address.
  // %u  - The username prior to the '@'.
  // %d  - The domain name after the '@'.
  // %dc - The domain name hierarchal string e.g. "dc=test,dc=domain,dc=com"
  // %dn - DN found by ldap search when search_filter/search_base_dn are used
  'base_dn'       => '',
  'bind_dn'       => '',
  'bind_pass'     => '',
  // It's possible to bind for an individual address book
  // The login name is used to search for the DN to bind with
  'search_base_dn' => '',
  'search_filter'  => '',   // e.g. '(&(objectClass=posixAccount)(uid=%u))'
  // DN and password to bind as before searching for bind DN, if anonymous search is not allowed
  'search_bind_dn' => '',
  'search_bind_pw' => '',
  // Default for %dn variable if search doesn't return DN value
  'search_dn_default' => '',
  // Optional authentication identifier to be used as SASL authorization proxy
  // bind_dn need to be empty
  'auth_cid'       => '',
  // SASL authentication method (for proxy auth), e.g. DIGEST-MD5
  'auth_method'    => '',
  // Indicates if the addressbook shall be hidden from the list.
  // With this option enabled you can still search/view contacts.
  'hidden'        => false,
  // Indicates if the addressbook shall not list contacts but only allows searching.
  'searchonly'    => false,
  // Indicates if we can write to the LDAP directory or not.
  // If writable is true then these fields need to be populated:
  // LDAP_Object_Classes, required_fields, LDAP_rdn
  'writable'       => false,
  // To create a new contact these are the object classes to specify
  // (or any other classes you wish to use).
  'LDAP_Object_Classes' => array('top', 'inetOrgPerson'),
  // The RDN field that is used for new entries, this field needs
  // to be one of the search_fields, the base of base_dn is appended
  // to the RDN to insert into the LDAP directory.
  'LDAP_rdn'       => 'cn',
  // The required fields needed to build a new contact as required by
  // the object classes (can include additional fields not required by the object classes).
  'required_fields' => array('cn', 'sn', 'mail'),
  'search_fields'   => array('mail', 'cn'),  // fields to search in
  // mapping of contact fields to directory attributes
  //   for every attribute one can specify the number of values (limit) allowed.
  //   default is 1, a wildcard * means unlimited
  'fieldmap' => array(
    // Roundcube  => LDAP:limit
    'name'        => 'cn',
    'surname'     => 'sn',
    'firstname'   => 'givenName',
    'jobtitle'    => 'title',
    'email'       => 'mail:*',
    'phone:home'  => 'homePhone',
    'phone:work'  => 'telephoneNumber',
    'phone:mobile' => 'mobile',
    'phone:pager' => 'pager',
    'street'      => 'street',
    'zipcode'     => 'postalCode',
    'region'      => 'st',
    'locality'    => 'l',
    // if you country is a complex object, you need to configure 'sub_fields' below
    'country'      => 'c',
    'organization' => 'o',
    'department'   => 'ou',
    'jobtitle'     => 'title',
    'notes'        => 'description',
    // these currently don't work:
    // 'phone:workfax' => 'facsimileTelephoneNumber',
    // 'photo'         => 'jpegPhoto',
    // 'manager'       => 'manager',
    // 'assistant'     => 'secretary',
  ),
  // Map of contact sub-objects (attribute name => objectClass(es)), e.g. 'c' => 'country'
  'sub_fields' => array(),
  // Generate values for the following LDAP attributes automatically when creating a new record
  'autovalues' => array(
  // 'uid'  => 'md5(microtime())',               // You may specify PHP code snippets which are then eval'ed 
  // 'mail' => '{givenname}.{sn}@mydomain.com',  // or composite strings with placeholders for existing attributes
  ),
  'sort'          => 'cn',    // The field to sort the listing by.
  'scope'         => 'sub',   // search mode: sub|base|list
  'filter'        => '(objectClass=inetOrgPerson)',      // used for basic listing (if not empty) and will be &'d with search queries. example: status=act
  'fuzzy_search'  => true,    // server allows wildcard search
  'vlv'           => false,   // Enable Virtual List View to more efficiently fetch paginated data (if server supports it)
  'numsub_filter' => '(objectClass=organizationalUnit)',   // with VLV, we also use numSubOrdinates to query the total number of records. Set this filter to get all numSubOrdinates attributes for counting
  'sizelimit'     => '0',     // Enables you to limit the count of entries fetched. Setting this to 0 means no limit.
  'timelimit'     => '0',     // Sets the number of seconds how long is spend on the search. Setting this to 0 means no limit.
  'referrals'     => true|false,  // Sets the LDAP_OPT_REFERRALS option. Mostly used in multi-domain Active Directory setups

  // definition for contact groups (uncomment if no groups are supported)
  // for the groups base_dn, the user replacements %fu, %u, $d and %dc work as for base_dn (see above)
  // if the groups base_dn is empty, the contact base_dn is used for the groups as well
  // -> in this case, assure that groups and contacts are separated due to the concernig filters! 
  'groups'        => array(
    'base_dn'     => '',
    'scope'       => 'sub',   // search mode: sub|base|list
    'filter'      => '(objectClass=groupOfNames)',
    'object_classes' => array("top", "groupOfNames"),
    'member_attr'  => 'member',   // name of the member attribute, e.g. uniqueMember
    'name_attr'    => 'cn',       // attribute to be used as group name
  ),
);
*/// An ordered array of the ids of the addressbooks that should be searched
// when populating address autocomplete fields server-side. ex: array('sql','Verisign');
$config['autocomplete_addressbooks'] = array('sql', 'Local');

// sort contacts by this col (preferably either one of name, firstname, surname)
$config['addressbook_sort_col'] = 'name';

// use this timezone to display date/time
// valid timezone identifers are listed here: php.net/manual/en/timezones.php
// 'auto' will use the browser's timezone settings
%if %%time_zone == "Europe/Paris"
$rcmail_config['timezone'] = 1;
%else
$rcmail_config['timezone'] = 'auto';
%end if

// Encoding of long/non-ascii attachment names:
// 0 - Full RFC 2231 compatible
// 1 - RFC 2047 for 'name' and RFC 2231 for 'filename' parameter (Thunderbird's default)
// 2 - Full 2047 compatible
$config['mime_param_folding'] = 0;

// Default font for composed HTML message.
// Supported values: Andale Mono, Arial, Arial Black, Book Antiqua, Courier New,
// Georgia, Helvetica, Impact, Tahoma, Terminal, Times New Roman, Trebuchet MS, Verdana
$config['default_font'] = '';

// lifetime of message cache
// possible units: s, m, h, d, w
$config['message_cache_lifetime'] = '10d';

