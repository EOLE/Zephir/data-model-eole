
\r roundcube


-- UPGRADE 0.3.1 TO 0.9.1 ------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS 031to091;
DELIMITER |
CREATE PROCEDURE 031to091()
BEGIN
	DECLARE version INT DEFAULT 0;
	DECLARE keyexit INT DEFAULT 0;
	
	SELECT COUNT(*) INTO version FROM information_schema.tables WHERE table_schema = 'roundcube' AND table_name = 'system';

	IF version = 0 THEN
	
		-- Suppression de la table messages
		DROP TABLE `messages`;
		
		-- prerequis
		ALTER TABLE `cache` ENGINE=InnoDB;
		ALTER TABLE `session` ENGINE=InnoDB;
		ALTER TABLE `users` ENGINE=InnoDB;
		ALTER TABLE `contacts` ENGINE=InnoDB;
		ALTER TABLE `identities` ENGINE=InnoDB;

		-- 2009103100.sql
		/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
		SELECT COUNT(*) INTO keyexit FROM information_schema.referential_constraints WHERE constraint_schema = 'roundcube' AND constraint_name='user_id_fk_cache';
		IF keyexit > 0 THEN
			ALTER TABLE `cache` DROP FOREIGN KEY `user_id_fk_cache`;
		END IF;
		
		SELECT COUNT(*) INTO keyexit FROM information_schema.referential_constraints WHERE constraint_schema = 'roundcube' AND constraint_name='user_id_fk_contacts';
		IF keyexit > 0 THEN
			ALTER TABLE `contacts` DROP FOREIGN KEY `user_id_fk_contacts`;
		END IF;

		SELECT COUNT(*) INTO keyexit FROM information_schema.referential_constraints WHERE constraint_schema = 'roundcube' AND constraint_name='user_id_fk_identities';
		IF keyexit > 0 THEN		
			ALTER TABLE `identities` DROP FOREIGN KEY `user_id_fk_identities`;
		END IF;

		ALTER TABLE `cache` ADD CONSTRAINT `user_id_fk_cache` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
		ALTER TABLE `contacts` ADD CONSTRAINT `user_id_fk_contacts` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
		ALTER TABLE `identities` ADD CONSTRAINT `user_id_fk_identities` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

		ALTER TABLE `contacts` ALTER `name` SET DEFAULT '';
		ALTER TABLE `contacts` ALTER `firstname` SET DEFAULT '';
		ALTER TABLE `contacts` ALTER `surname` SET DEFAULT '';

		ALTER TABLE `identities` ADD INDEX `user_identities_index` (`user_id`, `del`);
		ALTER TABLE `identities` ADD `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00' AFTER `user_id`;

		CREATE TABLE `contactgroups` (
		  `contactgroup_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		  `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		  `del` tinyint(1) NOT NULL DEFAULT '0',
		  `name` varchar(128) NOT NULL DEFAULT '',
		  PRIMARY KEY(`contactgroup_id`),
		  CONSTRAINT `user_id_fk_contactgroups` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  INDEX `contactgroups_user_index` (`user_id`,`del`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;

        
        CREATE TABLE `contactgroupmembers` (
          `contactgroup_id` int(10) UNSIGNED DEFAULT '0',
		  `contact_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		  `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		  PRIMARY KEY (`contactgroup_id`, `contact_id`),
		  CONSTRAINT `contactgroup_id_fk_contactgroups` FOREIGN KEY (`contactgroup_id`) REFERENCES `contactgroups`(`contactgroup_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  CONSTRAINT `contact_id_fk_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts`(`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE
		) /*!40000 ENGINE=INNODB */ ;

		/*!40014 SET FOREIGN_KEY_CHECKS=1 */;

		-- 2010042300.sql
		ALTER TABLE `users` CHANGE `last_login` `last_login` datetime DEFAULT NULL;
		UPDATE `users` SET `last_login` = NULL WHERE `last_login` = '1000-01-01 00:00:00';

		-- 2010100600.sql
		ALTER TABLE `users` DROP INDEX `username_index`;
		ALTER TABLE `users` ADD UNIQUE `username` (`username`, `mail_host`);

		ALTER TABLE `contacts` MODIFY `email` varchar(255) NOT NULL;


		-- 2011011200.sql
		ALTER TABLE `contacts` ADD `words` TEXT NULL AFTER `vcard`;
		ALTER TABLE `contacts` CHANGE `vcard` `vcard` LONGTEXT /*!40101 CHARACTER SET utf8 */ NULL DEFAULT NULL;
		ALTER TABLE `contactgroupmembers` ADD INDEX `contactgroupmembers_contact_index` (`contact_id`);

		TRUNCATE TABLE `cache`;
		
		
		-- 2011092800.sql
		/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

		ALTER TABLE `users` CHANGE `alias` `alias` varchar(128) BINARY NOT NULL;
		ALTER TABLE `users` CHANGE `username` `username` varchar(128) BINARY NOT NULL;

		CREATE TABLE `dictionary` (
		  `user_id` int(10) UNSIGNED DEFAULT NULL,
		  `language` varchar(5) NOT NULL,
		  `data` longtext NOT NULL,
		  CONSTRAINT `user_id_fk_dictionary` FOREIGN KEY (`user_id`)	REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  UNIQUE `uniqueness` (`user_id`, `language`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;

		CREATE TABLE `searches` (
		  `search_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		  `type` int(3) NOT NULL DEFAULT '0',
		  `name` varchar(128) NOT NULL,
		  `data` text,
		  PRIMARY KEY(`search_id`),
		  CONSTRAINT `user_id_fk_searches` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		  UNIQUE `uniqueness` (`user_id`, `type`, `name`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;



		CREATE TABLE `cache_index` (
		 `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		 `mailbox` varchar(255) BINARY NOT NULL,
		 `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		 `valid` tinyint(1) NOT NULL DEFAULT '0',
		 `data` longtext NOT NULL,
		 CONSTRAINT `user_id_fk_cache_index` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		 INDEX `changed_index` (`changed`),
		 PRIMARY KEY (`user_id`, `mailbox`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;

		CREATE TABLE `cache_thread` (
		 `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		 `mailbox` varchar(255) BINARY NOT NULL,
		 `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		 `data` longtext NOT NULL,
		 CONSTRAINT `user_id_fk_cache_thread` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		 INDEX `changed_index` (`changed`),
		 PRIMARY KEY (`user_id`, `mailbox`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;

		CREATE TABLE `cache_messages` (
		 `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
		 `mailbox` varchar(255) BINARY NOT NULL,
		 `uid` int(11) UNSIGNED NOT NULL DEFAULT '0',
		 `changed` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		 `data` longtext NOT NULL,
		 `flags` int(11) NOT NULL DEFAULT '0',
		 CONSTRAINT `user_id_fk_cache_messages` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		 INDEX `changed_index` (`changed`),
		 PRIMARY KEY (`user_id`, `mailbox`, `uid`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;

		/*!40014 SET FOREIGN_KEY_CHECKS=1 */;		

		-- 2011111600.sql
		ALTER TABLE `session` CHANGE `sess_id` `sess_id` varchar(128) NOT NULL;
		
		-- 2011121400.sql
		/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

		ALTER TABLE `contacts` DROP FOREIGN KEY `user_id_fk_contacts`;
		ALTER TABLE `contacts` DROP INDEX `user_contacts_index`;
		ALTER TABLE `contacts` MODIFY `email` text NOT NULL;
		ALTER TABLE `contacts` ADD INDEX `user_contacts_index` (`user_id`,`del`);
		ALTER TABLE `contacts` ADD CONSTRAINT `user_id_fk_contacts` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

		ALTER TABLE `cache` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `cache_index` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `cache_thread` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `cache_messages` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `contacts` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `contactgroups` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `contactgroupmembers` ALTER `contact_id` DROP DEFAULT;
		ALTER TABLE `identities` ALTER `user_id` DROP DEFAULT;
		ALTER TABLE `searches` ALTER `user_id` DROP DEFAULT;

		/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
		
		-- 2012080700.sql
		ALTER TABLE `cache` DROP COLUMN `cache_id`;
		ALTER TABLE `users` DROP COLUMN `alias`;
		ALTER TABLE `identities` ADD INDEX `email_identities_index` (`email`, `del`);
		
		-- 2013011000.sql
		CREATE TABLE IF NOT EXISTS `system` (
		 `name` varchar(64) NOT NULL,
		 `value` mediumtext,
		 PRIMARY KEY(`name`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;
		
		INSERT INTO system (`name`, `value`) VALUES ("roundcube-version","2013011700");
				
	END IF;
END|
DELIMITER ;

CALL 031to091();
DROP PROCEDURE 031to091;

-- PLUGIN POP3FETCHER ------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS pop3fetcher;
DELIMITER |
CREATE PROCEDURE pop3fetcher()
BEGIN
        DECLARE version INT DEFAULT 0;

        SELECT COUNT(*) INTO version FROM information_schema.tables WHERE table_schema = 'roundcube' AND table_name = 'pop3fetcher_accounts';

        IF version = 0 THEN
		CREATE TABLE  `roundcube`.`pop3fetcher_accounts` (
		  `pop3fetcher_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `pop3fetcher_email` varchar(128) NOT NULL,
		  `pop3fetcher_username` varchar(128) NOT NULL,
		  `pop3fetcher_password` varchar(128) NOT NULL,
		  `pop3fetcher_serveraddress` varchar(128) NOT NULL,
		  `pop3fetcher_serverport` varchar(128) NOT NULL,
		  `pop3fetcher_ssl` varchar(10) DEFAULT '0',
		  `pop3fetcher_leaveacopyonserver` tinyint(1) DEFAULT '0',
		  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
		  `last_check` int(10) unsigned NOT NULL DEFAULT '0',
		  `last_uidl` varchar(70) DEFAULT NULL,
		  `update_lock` tinyint(1) NOT NULL DEFAULT '0',
		  `pop3fetcher_provider` varchar(128) DEFAULT NULL,
		  `default_folder` varchar(128) DEFAULT NULL,
		  PRIMARY KEY (`pop3fetcher_id`),
		  KEY `user_id_fk_accounts` (`user_id`)
		) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
	END IF;
END|
DELIMITER ;

CALL pop3fetcher();
DROP PROCEDURE pop3fetcher;

-- UPGRADE 0.9.1 TO 1.0.1 ------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS 091to101;
DELIMITER |
CREATE PROCEDURE 091to101()
BEGIN
	DECLARE version INT DEFAULT 0;
	DECLARE keyexit INT DEFAULT 0;
	
	SELECT value INTO version FROM system;

	IF version = '2013011700' THEN

		CREATE TABLE `cache_shared` (
		 `cache_key` varchar(255) /*!40101 CHARACTER SET ascii COLLATE ascii_general_ci */ NOT NULL,
		 `created` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
		 `data` longtext NOT NULL,
		 INDEX `created_index` (`created`),
		 INDEX `cache_key_index` (`cache_key`)
		) /*!40000 ENGINE=INNODB */ /*!40101 CHARACTER SET utf8 COLLATE utf8_general_ci */ ;


		ALTER TABLE `cache` ADD `expires` datetime DEFAULT NULL;
		ALTER TABLE `cache_shared` ADD `expires` datetime DEFAULT NULL;
		ALTER TABLE `cache_index` ADD `expires` datetime DEFAULT NULL;
		ALTER TABLE `cache_thread` ADD `expires` datetime DEFAULT NULL;
		ALTER TABLE `cache_messages` ADD `expires` datetime DEFAULT NULL;

		-- initialize expires column with created/changed date + 7days
		UPDATE `cache` SET `expires` = `created` + interval 604800 second;
		UPDATE `cache_shared` SET `expires` = `created` + interval 604800 second;
		UPDATE `cache_index` SET `expires` = `changed` + interval 604800 second;
		UPDATE `cache_thread` SET `expires` = `changed` + interval 604800 second;
		UPDATE `cache_messages` SET `expires` = `changed` + interval 604800 second;

		ALTER TABLE `cache` DROP INDEX `created_index`;
		ALTER TABLE `cache_shared` DROP INDEX `created_index`;
		ALTER TABLE `cache_index` DROP `changed`;
		ALTER TABLE `cache_thread` DROP `changed`;
		ALTER TABLE `cache_messages` DROP `changed`;

		ALTER TABLE `cache` ADD INDEX `expires_index` (`expires`);
		ALTER TABLE `cache_shared` ADD INDEX `expires_index` (`expires`);
		ALTER TABLE `cache_index` ADD INDEX `expires_index` (`expires`);
		ALTER TABLE `cache_thread` ADD INDEX `expires_index` (`expires`);
		ALTER TABLE `cache_messages` ADD INDEX `expires_index` (`expires`);

		ALTER TABLE `users` CHANGE `preferences` `preferences` longtext;

		UPDATE system SET value = '2014042900' WHERE name = 'roundcube-version';
				
	END IF;
END|
DELIMITER ;

CALL 091to101();
DROP PROCEDURE 091to101;

-- UPGRADE 1.0.1 TO 1.2.0 ------------------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS 101to120;
DELIMITER |
CREATE PROCEDURE 101to120()
BEGIN
	DECLARE version INT DEFAULT 0;
	DECLARE keyexit INT DEFAULT 0;

	SELECT value INTO version FROM system;

	IF version = '2014042900' THEN

		ALTER TABLE users ADD `failed_login` datetime DEFAULT NULL;
		ALTER TABLE users ADD `failed_login_counter` int(10) UNSIGNED DEFAULT NULL;

		UPDATE system SET value = '2016052200' WHERE name = 'roundcube-version';

	END IF;
END|
DELIMITER ;

CALL 101to120();
DROP PROCEDURE 101to120;
